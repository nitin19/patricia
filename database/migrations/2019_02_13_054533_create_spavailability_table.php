<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpavailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spavailability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->enum('openhrs',['1', '0'])->default('1')->comment="1=Open for selected hours,0=Always open";
            $table->enum('day_monday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('monday_start', 191)->nullable();
            $table->string('monday_end', 191)->nullable();
            $table->enum('day_tuesday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('tuesday_start', 191)->nullable();
            $table->string('tuesday_end', 191)->nullable();
            $table->enum('day_wednesday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('wednesday_start', 191)->nullable();
            $table->string('wednesday_end', 191)->nullable();
            $table->enum('day_thursday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('thursday_start', 191)->nullable();
            $table->string('thursday_end', 191)->nullable();
            $table->enum('day_friday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('friday_start', 191)->nullable();
            $table->string('friday_end', 191)->nullable();
            $table->enum('day_saturday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('saturday_start', 191)->nullable();
            $table->string('saturday_end', 191)->nullable();
            $table->enum('day_sunday',['0', '1'])->default('0')->comment="1=Open,0=Close";
            $table->string('sunday_start', 191)->nullable();
            $table->string('sunday_end', 191)->nullable();
            $table->enum('status',['1', '0'])->default('1')->comment="1=Active,0=Deactive";
            $table->integer('isCompleted')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spavailability');
    }
}
