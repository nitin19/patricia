
<!DOCTYPE html>

<head>
  <title>Ativação de Cadastro | NaVizinhança</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="./public/images/icon.png" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/newstyle.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
 <link href="https://fonts.googleapis.com/css?family=Charmonman:400,700" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Charmonman:400,700|Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Pacifico" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
</head>
<body>
<div class="binary-errorpage">
  <div class="container">
    <div class="error-row">
      <div class="row">
        <div class="col-sm-12">
        <div class="thanks-page">
          <img src="/public/images/patricia_logo.png" alt="thanks-logo">
          <h1>Obrigado</h1>
          <h4>Um novo link foi enviado para o seu email.<br> 
          Clique nele para ativar a sua conta.</h4>
          <p style="top:5px;">Atenção: se não receber o seu e-mail em poucos segundos, veja a caixa de lixo eletrônico no seu e-mail.<br> Lembre-se de marcar o conteúdo do NaVizinhança como confiável para ficar sempre por dentro das mensagens recebidas.</p>
          <a href="<?php echo url('home') ?>">Ir para a página inicial</a>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
<style>
.binary-errorpage {
    background-image: url("/public/images/services-01.jpg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
.binary-errorpage:before {
    position: absolute;
    content: "";
    background: rgba(0,0,0,.5);
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
}
.thanks-page img {
    width: 150px;
    height: 80px;
    margin-bottom: 10px;
}
.error-row {
    display: flex;
    align-items: center;
    height: 100vh;
    text-align: center;
    justify-content: center;
}
.thanks-page {
    background-color: rgba(255,255,255,.7);
    padding: 40px;
}
.thanks-page h1 {
    font-family: 'Courgette', cursive;
    
    /*font-size: 100px;
    font-weight: 400;
    margin-bottom: 40px;*/
    color:#333;
}
.thanks-page p {
    font-size: 16px;
    max-width: 550px;
    color: #333;
}
.thanks-page a {
    font-family: 'Lato', sans-serif;
    color: #2854a1;
    font-size: 18px;
    text-decoration: underline;
    margin-top: 20px;
}
</style>
