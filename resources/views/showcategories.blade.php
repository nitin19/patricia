@extends('layouts.default-header')

@section('title', 'Categories')

@section('content')
 <script src="{{ url('/public/admin/js/') }}jquery-1.11.3.min.js"></script>  
 <style>
 .form_section {
    padding: 20px 0;
}
footer {
    margin-top: 20px;
}
th,td {
    text-align: center !important;
}
 </style>
<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">
<div class="form_section">
<div class="row">
<div class="col-sm-12 content_box">
      <div class="media payment_box">
          @include('layouts.flash-message')
        <div class="col-sm-6">
          <div class="media-body button_side">
           <h3>Lista de Serviços <span class="list_clr">({{ $category_count }})</span></h3>
          </div>
        </div>
    </div>
    </div>
</div>
</div>
</div>
<div class="col-sm-12">
 <div class="service_box tablediv"><br>
  <table id="subcategory_table" class="table stripped-table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th> Sr No</th> -->
        <th> Imagem</th>
        <th> Tipo de serviço</th>
        <th> Categoria </th>
        <!--th> Action </th-->
      </tr>
    </thead>
    <tbody>
      <?php $aaa ='1';
      $counting ='1'; ?>
        @foreach($category as $categories)
        <?php
          $cat_image = $categories->cat_image;
              if($cat_image==''){
              $categoryimage = "../public/images/categoryimage/clothing_icon.png";
              }else {
              $categoryimage = "../public/images/categoryimage/".$cat_image;  
              } ?>
      <tr id="{{ $categories->cat_id }}" class="service_table tabledata">
        <!-- <td class="aaa"> {{ $counting++ }}</td> -->
        <td> <img src="{{ $categoryimage }}"> </td>
        <td> {{ $categories->cat_name }} </td>
        <td> {{ $categories->name }} </td>
        <!--td><a href="{{ url('/admin-services/edit') }}/{{ $categories->cat_id }}"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-providers')}}/{{ $categories->main_cat_id }}/{{ $categories->cat_id }}"  data-toggle="tooltip" title="Users"><i class="fa fa-users" style="font-size:16px"></i></a-->
        <!-- <i class="fa fa-users" data-toggle="modal" data-target="#exampleModal_{{ $categories->cat_id }}" style="font-size:16px"></i> -->
        <!--a href="{{url('/admin/subcategory_price/'.$categories->cat_id)}}" style="cursor: pointer;" data-toggle="tooltip" title="Set Pricing"><i class="fa fa-tags" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" style="font-size:16px"></i></a>
        </td-->
      </tr>
      
      @endforeach

      </tbody>
      
<script type="text/javascript">
  jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var catid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "{{url('/')}}/admin-services/category_remove",
          type: 'POST',
          data: {'catid': catid,"_token": "{{ csrf_token() }}" },
          success: function(data) {
            if(data == 1)
            {
              $('#Alert_Success').show();
              jQuery('#'+catid).hide();
              setTimeout(function() {
                $('#Alert_Success').remove(); 
                }, 4000);
            }  
            else
            {
              $('#Alert_Danger').show();
              setTimeout(function() {
                $('#Alert_Danger').remove(); 
                }, 4000);
            } 
          }          
        })      
      }
      else{
        return false;
      }
});
</script>
  </table>

</div>
  </div>

<script type="text/javascript">
jQuery(document).ready(function() {  
jQuery('#subcategory_table').DataTable( {

  "language": {
    "decimal":        "",
    "emptyTable":     "No data available in table",
    "info":           "Mostrando de _START_ a _END_ entre os _TOTAL_ tipos de serviços disponíveis",
     "infoEmpty":      "Showing 0 to 0 of 0 entries",
    "infoFiltered":   "(filtered from _MAX_ total entries)",
    "infoPostFix":    "",
    "thousands":      ",",
    "lengthMenu":     "Mostrar _MENU_ tipos de serviço por página",
    "loadingRecords": "Loading...",
    "processing":     "Processing...",
    "search":         "Procurar:",
    "zeroRecords":    "No matching records found",
    "paginate": {
        "first":      "First",
        "last":       "Last",
        "next":       "Próxima",
        "previous":   "Anterior"
    },
    "aria": {
        "sortAscending":  ": activate to sort column ascending",
        "sortDescending": ": activate to sort column descending"
    }
  }

  //"order": [[ 3, "desc" ]]
  });
});
 </script>

@stop