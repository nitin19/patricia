@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.notify-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane subtabcontent active" id="switch">
            <div class="container">
              <div class="switchrolhead">
                <h6>Escolha abaixo o tipo de usuário para navegação no site<a href="#" data-toggle="tooltip" data-placement="auto" title="Ao alterar o tipo de usuário, você será desconectado e terá que entrar (digitar seu e-mail e senha) novamente para acesso ao site."><img src="{{url('/public/images/question-mark.png')}}" class="question-mark" height="21px;"></a> </h6>
              </div>
              <div class="switch switchrole">
                <form class="switchdashboard" id="switchdashboard" method="post" action="">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label class="col-sm-4"><h4>Tipo de usuário</h4></label>
                    <div class="col-sm-5">
                      <input type="hidden" id="userid" name="current_userId" value="{{ $user_info->id }}">
                      <div class="radioradio">
                        <label class="radio_btn_label">Prestador de serviço
                        <input type="radio" checked="checked" name="role_switch" value="Provider" @if($user_info->user_role=='Provider') checked @endif >
                        <span class="checkmark redme"></span>
                        </label>
                      </div>
                      <div class="radioradio">
                        <label class="radio_btn_label">Contratante
                          <input type="radio" name="role_switch" value="Taker" @if($user_info->user_role=='Taker') checked @endif>
                          <span class="checkmark redme"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-3 changerolebtn">
                      <input class="btn btn-primary change_role" value="Alterar" type="button" >
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script type="text/javascript">

  

  $('[data-toggle="tooltip"]').tooltip();
  /*========switch account========= */
  var base_url = '<?php echo url("/"); ?>';
  $('.change_role').on('click',function(){
    /*if(confirm("Ao alterar o tipo de usuário, você será desconectado e terá que se entrar (digitar seu e-mail e senha) novamente para acesso ao site.")){
       $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
       $('#switchdashboard').submit();
    }
    else{
      return false;
    }*/

    $.confirm({
    title: '',
    content: 'Ao alterar o tipo de usuário, você será desconectado e terá que se entrar (digitar seu e-mail e senha) novamente para acesso ao site.',
    buttons: {
        confirme: function () {
            $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
            $('#switchdashboard').submit();
        },
        Cancelar: function () {
            //return false;
        }
    }
});
   

    //   swal({
    //   title: "Atenção:",
    //   text: "Ao alterar o tipo de usuário, você será desconectado e terá que se entrar (digitar seu e-mail e senha) novamente para acesso ao site.!",
    //   icon: "warning",
    //   buttons: ["Cancelar", "Ok"],
    //   dangerMode: true,
    // })
    // .then((willDelete) => {
    //   if (willDelete) {
    //     swal("Poof! Your role changed!", {
    //       icon: "success",
    //     });
    //      $('#switchdashboard').attr('action','https://www.navizinhanca.com/dashboard/switchdashboard');
    //        $('#switchdashboard').submit();
    //   } else {
    //     // swal("You continue with same role!");
    //     return false;
    //   }
    // });
 });
 
</script>
<style type="text/css">
/*.radio_btn_label .checkmark:after {
  top:5px  !important;
  left:5px !important;
}*/
.checkmark.redme {
    background-color: #ccc !important;
    padding: 8px;
}
/*.radio_btn_label input:checked~.checkmark {
    border: 5px solid #39c;   
}*/
.buttondiv {
    text-align: center;
}
.checkmark {
    position: absolute;
    top: 2px;
    left: 0;
    background-color: transparent !important;
    border-radius: 50%;
    padding: 4px;
}
  .radioradio {
    display: inline-block;
    margin-right: 10px;
}
.checkmark {
    top: 6px;
}
</style>
  @stop
