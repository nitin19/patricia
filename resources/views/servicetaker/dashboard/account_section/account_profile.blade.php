@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.flash-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                 <!--  <li role="presentation"><a href="{{ url('/servicetaker/account/card_details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                  <li role="presentation"><a href="{{ url('/servicetaker/account/notifications')}}" aria-controls="notifications-tabs" role="tab">Notificações</a></li>
                  <li role="presentation"><a href="{{ url('/servicetaker/account/review')}}" aria-controls="closeacc" role="tab">Avaliação do contratante</a></li>
                  <li role="presentation" class="close_account"><a href="{{ url('/servicetaker/account/close')}}" aria-controls="closeacc" role="tab">Encerrar seu cadastro</a></li>
                  
                </ul>
                <div class="tab-content profiletabss">
                  <div role="tabpanel" class="tab-pane active" id="profileinfo">
                    <div class="acccompletion_tabs">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane @if (!Session::has('success')) active @endif" id="profile">
                          <form class="completionform" id="completionform" method="post" action="{{ url('/dashboard/update_account/') }}"  enctype="multipart/form-data">
                          <div class="profileg">
                            <div class="topicon">
                                <a href="http://navizinhanca.com/servicetaker/profile"> <img src="http://navizinhanca.com/public/images/14_dashboard_designicon21.png" alt="dashboard_designi" width="100%" class="img-responsive"></a>
                            </div>
                          {{ csrf_field() }}
                            <div class="row" style="width:100%">
                              <div class="col-sm-4 profileimage">
                                <div class="form-group imagediv" >
                                  <div class="imagebrowse" id="previewdiv">
                                    <?php if($profile_image=='') { ?>
                                    <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="profimg"><?php } else { ?>
                                    <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="profimg">
                                    <?php } ?>
                                    <div class="nopadding imgbtns" style="cursor: inherit!important;">
                                     <!--  <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i> -->
                                    </div>
                                    <div class="prflinput">
                                      <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
                                    </div>
                                  </div>
                                    <h5 class="usrnam">{{ ucfirst($user_info->name) }}</h5>
                                    <h6 class="usreml">{{ $user_info->email }}</h6>
                                </div>
                              </div>
                              <div class="col-sm-8 profileformmainright">
                                <div class="row">
                                  <div class="profileform_right">
                                   
                                      <div class="form-group">
                                        <p class="viewprofile"><span>Nome completo <b class="pull-right">:</b></span> <span class="lastview"> {{ ucfirst($user_info->name) }} </span></p>
                                      </div>
                                       
                                      <div class="form-group">
                                        <p class="viewprofile"> <span>Email<b class="pull-right">:</b></span>  <span class="lastview"> {{ $user_info->email }}</span>
                                            <!-- <input type="hidden" name="user_id" value="{{ $user_info->id }}"> -->
                                        </p>
                                      </div>
                                      <div class="form-group">
                                        <p class="viewprofile"> <span class="">Celular <b class="pull-right">:</b></span>  <span class="lastview"> {{ $user_info->phone }}</span></p>
                                      </div>
                                      <div class="form-group">
                                      <p class="viewprofile">
                                        <span class="">Tipo de usuário <b class="pull-right">:</b></span>  <span class="lastview"> <?php if($user_info->user_role=='Provider') { echo "Prestador de Serviço"; } else { echo "Contratante"; } ?> 
                                        </span>
                                      </p>
                                      </div>
                                       <div class="form-group">
                                      <p class="viewprofile"><span class="">CPF  <b class="pull-right">:</b></span>  <span class="lastview"> {{ $user_info->cpf_id }} </span></p>
                                    </div>
                                      <div class="form-group">
                                      <p class="viewprofile"><span class="">RG  <b class="pull-right">:</b></span>  <span class="lastview"> {{ $user_info->rg }} </span></p>
                                    </div>
                                     
                                     

                                    </div>
                                    <div class="profileform_right">
                                      <!-- <div class="form-group">
                                        <p class="viewprofile"><span class="">Para me conhecer melhor<b class="pull-right">:</b></span>  <span class="lastview"> <php echo $user_info->bio; ?></span>
                                        </p>
                                      </div> -->
                                      <!--  <div class="form-group">
                                        <p class="viewprofile"><span class="">Cursos e certificados<b class="pull-right">:</b></span>  <span class="lastview">{{ $user_info->additional_details }}</span>
                                        </p>
                                      </div> -->
                                      <div class="form-group">
                                      <p class="viewprofile"> <span class="">Endereço <b class="pull-right">:</b></span> <span class="lastview"> <?php echo ucfirst($user_info->address); ?></span>
                                        
                                          
                                        </p>
                                      </div>
                                      

                                      <div class="form-group">
                                        <p class="viewprofile"><span class="">Estado <b class="pull-right">:</b></span>  <span class="lastview"> <?php echo ucfirst($user_info->state); ?> </span></p> 
                                      </div>
                                      <div class="form-group">
                                      <p class="viewprofile"><span class="">País <b class="pull-right">:</b></span> <span class="lastview"> {{ ucfirst($user_info->country) }}
                                          </span>
                                        
                                      </p>
                                    </div>
                                    <div class="form-group">
                                        <p class="viewprofile"><span class="">CEP<b class="pull-right">:</b></span>  <span class="lastview"> <?php echo $user_info->zipcode; ?> </span>
                                        </p>
                                      </div>
 
                                  </div>

                                @if($user_info->user_role =='Provider')
                                <div class="">
                                  <div class="form-group forminput profileform_right radimate">
                                    <p class="viewprofile">
                                      <span class="">Anexar imagem<b class="pull-right">:</b></span> <span class="lastview">
                                        @if(count($user_image) > 0)
                                          @foreach ($user_image as $user_images)
                                           <!--  <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a> -->
                                          <div class="col-sm-3">
                                            <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" style="margin-bottom: 10px !important; width:100%;">
                                          </div>
                                          <!-- </div> -->
                                          @endforeach
                                        @else
                                          <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                                        @endif
                                      </span>
                                    </p>
                                    <label class="col-sm-3"></label>
                                    <!-- <div class="col-sm-9 col-xs-9 new_up_sec nopadding">
                                      <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
                                      <div class="browse_btn" id="browsebtn">
                                        <span>Select Images</span>
                                      </div>
                                    </div> -->
                                  </div>
                                </div>
                                @endif

                                  <!-- <div class="col-sm-12 nextbtn text-right">
                                    <input type="submit" class="form-control" value="Save">
                                  </div> -->
                                </div>
                              </div>
                            </div>
                            </form>
                          </div>
<style>
  .nopadding input {
    width: auto;
  } 
</style>
                         <!--  <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                              <div class="col-sm-12 form-group profileform_right">
                                <form name="complete_service" id="complete_service" class="complete_service" action="{{ url('/accountcomplete/service') }}" method="post">
                                  {{ csrf_field() }}
                                  <?php
                                  $user_cateogry = $user_info->category;
                                  foreach($category as $category_name){
                                    $categrry_name = $category_name->cat_name;
                                    ?>
                                    <div class="col-sm-3 form-group">
                                      <label class="col-sm-11"><?php echo ucfirst($category_name->cat_name); ?></label><div class="profileinput col-sm-1 nopadding"><input type="checkbox" name="cat_name[]" value="<?php echo $category_name->cat_name; ?>" <?php if (strpos($user_cateogry, $categrry_name) !== false) {
                                        echo 'checked';
                                      } ?> data-trigger="focus"></div>
                                    </div>
                                    <?php
                                  }
                                  ?>
                                  <div class="col-sm-12 nextbtn">
                                    <input class="form-control" value="Save" type="submit">
                                  </div>
                                </form>
                              </div>
                            </div> -->
                          </div>

                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style type="text/css">
  figcaption.imgerror {
    width: 65%;
    float: right;
}



</style>
@stop
