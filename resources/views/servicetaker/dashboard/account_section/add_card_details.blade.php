@extends('layouts.default-header')
@section('title',$menu_meta_details['meta_title'])
@section('description', $menu_meta_details['meta_description'])
@section('keywords',$menu_meta_details['meta_keywords'] )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.flash-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                 <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                  <!-- <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/card_details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/notifications')}}" aria-controls="notifications-tabs" role="tab">Notificações</a></li>
                  <li role="presentation"><a href="{{ url('/servicetaker/account/review')}}" aria-controls="closeacc" role="tab">Avaliação do contratante</a></li>
                  <li role="presentation" class="close_account"><a href="{{ url('/servicetaker/account/close')}}" aria-controls="closeacc" role="tab">Encerrar seu cadastro></a></li>
                  
                </ul>
                <div class="tab-content">
                   <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                    </div>
                <div role="tabpanel" class="tab-pane subtabcontent active" id="carddetail">
                  <!-- <div class="alert alert-success alert-block success_show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Account Info Saved Successfully!</strong>
                  </div> -->
                  <div class="tabcontentbg">
                    <form class="carddetailform" id="carddetailform" method="post" action="#">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label class="col-sm-4">Credit/debit card number</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="cardnumber" name="card_num" placeholder="4704 - 1258 - 3698 - 1235" value="{{ $user_info->cardnumber }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4">Name on Card</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="cardname" name="paymentname" placeholder="" value="{{ $user_info->cardname }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4">Expiry Date</label>
                      <div class="col-sm-8">
                        <select class="selectmonth" name="exp_month">
                          <option value="">Select Month</option>
                          <?php
                          $exp_month = $user_info->exp_month;
                          for ($i = 1; $i <= 12; ++$i) {
                            ?>
                            <option value="<?php
                            echo $i;
                            ?>" <?php
                            if ($exp_month == $i) {
                              echo 'selected';
                            }
                            ?>><?php
                            echo $i;
                            ?></option>
                            <?php
                          }
                          ?>
                        </select>
                        <select class="selectyears" name="exp_year">
                          <option value="">Select Years</option>
                          <?php
                          $exp_year = $user_info->exp_year;
                          for ($i = 2018; $i <= 2035; ++$i) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if ($exp_year == $i) {
                              echo 'selected';  } ?>><?php echo $i; ?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Security code (CVV/CVC)</label>
                        <div class="col-sm-8">
                          <input type="text" name="cvv" value="{{ $user_info->cvv }}" id="cvv" class="cvv">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4"></label>
                        <div class="col-sm-8 addcardbtn">
                          <input type="submit" class="btn btn-primary" value="Add Card">
                        </div>
                      </div>
                    </form>
                        </div>
                      </div>
                     
                      
                    </div>
                  </div>
                </div>
              </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop
