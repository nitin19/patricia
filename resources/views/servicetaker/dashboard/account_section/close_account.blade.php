@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.flash-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                 <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                 <!--  <li role="presentation"><a href="{{ url('/servicetaker/account/card_details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                  <li role="presentation"><a href="{{ url('/servicetaker/account/notifications')}}" aria-controls="notifications-tabs" role="tab">Notificações</a></li>
                  <li role="presentation"><a href="{{ url('/servicetaker/account/review')}}" aria-controls="closeacc" role="tab">Avaliação do contratante</a></li>
                  <li role="presentation" class="active close_account"><a href="{{ url('/servicetaker/account/close')}}" aria-controls="closeacc" role="tab">Encerrar seu cadastro</a></li>
                  
                </ul>
                <div class="tab-content profiletabss">
                  
                   <div role="tabpanel" class="tab-pane active" id="closeacc">
                        <!--Close account-->
                        <div class="">
                          <div class="switch subtabcontent">
                            <!-- <div class="alert alert-success alert-block success_request">
                              <button type="button" class="close" data-dismiss="alert">×</button>
                              <strong>Your Request Submited Successfully!</strong>
                            </div> -->
                            <form class="closeaccount" id="closeaccount" method="post" action="">
                                  {{ csrf_field() }}
                              <div class="form-group">
                                  <label class="col-sm-4"><h4>Motivo:</h4></label>
                                <div class="col-sm-8">

                                  <input type="hidden" id="userid" name="current_userId" value="{{ $user_info->id }}">

                                  <label class="radio_btn_label">Sou prestador de serviço e tive pouca demanda por serviços
                                    <input type="radio" checked="checked" class="otherreason" name="close_reason" id="opt1" value="Sou prestador de serviço e tive pouca demanda por serviços">
                                    <span class="checkmark"></span>
                                  </label>

                                  <label class="radio_btn_label" >Sou contratante e não tive sucesso na minha busca por prestadores de serviço
                                    <input type="radio" class="otherreason" name="close_reason" id="opt2" value="Sou contratante e não tive sucesso na minha busca por prestadores de serviço">
                                    <span class="checkmark"></span>
                                  </label>

                                  <label class="radio_btn_label">Site precisa ser melhorado: está com falhas (bugs) e/ou com navegação confusa
                                    <input type="radio" class="otherreason" name="close_reason" id="opt3" value="Site precisa ser melhorado: está com falhas (bugs) e/ou com navegação confusa">
                                    <span class="checkmark"></span>
                                  </label>

                                  <label class="radio_btn_label">Concorrente oferece um serviço melhor
                                    <input type="radio" class="otherreason" name="close_reason" id="opt4" value="Concorrente oferece um serviço melhor">
                                    <span class="checkmark"></span>
                                  </label>

                                   <label class="radio_btn_label"> Outros motivos:
                                    <input type="radio" class="otherreason" name="close_reason" id="opt5" value="">
                                    <textarea class="form-control otherreasonfield" id="otherreasonfield" name="otherreasonfield" required="" style="border: none;opacity: 1;border-bottom: 1px solid;width: 40%;border-radius: 0;box-shadow: none;display:none;"></textarea>
                                    <span class="checkmark"></span>
                                  </label>

                                  <div class="addcardbtn"> 
                                    <input class="btn btn-primary btn-md" value="Enviar" type="submit" id="sub">
                                    <br><br>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      <!--End Close account-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

$('.otherreason').click(function() {
    var OptId = $(this).attr('id');
    if(OptId == 'opt5') {
      if($('#opt5').is(":checked"))  {
        $("#otherreasonfield").show();
        $('#otherreasonfield-error').remove();
        $('#otherreasonfield').removeClass('error');
        var otherfieldVal = $("#otherreasonfield").val();
        if(otherfieldVal!='') {
           $('#opt5').val(vres);
        } else {
           $('#opt5').val('');
        }
      } else {
        $("#otherreasonfield").hide();
        $("#otherreasonfield").val('');
        $('#opt5').val('');
        $('#otherreasonfield-error').remove();
      }
    } else {
      $("#otherreasonfield").hide();
      $("#otherreasonfield").val('');
      $('#opt5').val('');
      $('#otherreasonfield-error').remove();
    }
  });

  $('#otherreasonfield').focusin(function(){
   // var vres = $(this).val();
  });

  $('#otherreasonfield').focusout(function(){ 
    var vres = $(this).val();
    if(vres!='') {
      $('#opt5').val(vres);
    } else {
      $('#opt5').val('');
    }
  });

  // Close account js
  $("#closeaccount").validate({
        rules: {
          otherreasonfield: {
            required: true
          }
        },
        messages: {
          otherreasonfield: {
            required: "Digite o motivo."
          }
        },
        submitHandler: function(form) {
          $.confirm({
          title: '',
          content: 'Você tem certeza que deseja cancelar definitivamente sua conta no NaVizinhança?',
          buttons: {
              SIM: function () {
                   $.ajax({
                    url: "{{ url('/servicetaker/dashboard/closeaccount') }}",
                    type: 'POST',
                    data: $(form).serialize(),
                    success: function(response) {
                      $.notify({
                        message: 'Sua solicitação enviada com sucesso!',
                        },{
                        type: 'success',
                        offset: 
                        {
                          x: 10,
                          y: 130
                        },
                        animate: {
                          enter: 'animated fadeInRight',
                          exit: 'animated fadeOutRight'
                        },
                      });
                      setTimeout(function() {
                        sessionStorage.clear();
                        window.location.href = "{{ url('/login') }}";
                      }, 3000);
                    }
                  
                });
        },
        VOLTAR: function () {
            //return false;
        }
    }
});
}
});
  
</script>
  @stop
