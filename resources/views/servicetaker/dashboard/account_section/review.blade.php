@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
             
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
             
            </ul>
          </div>
        </div>
        <!-- @include('layouts.flash-message') -->
        @include('layouts.notify-message')
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                 <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                  <!-- <li role="presentation"><a href="{{ url('/servicetaker/account/card_details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/notifications')}}" aria-controls="notifications-tabs" role="tab">Notificações</a></li>
                  <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/review')}}" aria-controls="closeacc" role="tab">Avaliação do contratante</a></li>
                  <li role="presentation" class="close_account"><a href="{{ url('/servicetaker/account/close')}}" aria-controls="closeacc" role="tab">Encerrar seu cadastro</a></li>
                 
                </ul>
                <div class="tab-content profiletabss">
                  <div role="tabpanel" class="tab-pane active" id="notifications-tabs">
                    <div class="tabcontentbg">
                      <div class="tablewrapper tablediv">
                            <!--====notication code start--==-->
                            <div class="notificationbox">
                              <!-- <div class="alert alert-success alert-block success_show">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>Notification deleted successfully!</strong>
                              </div>  -->
                              <div class="firstnotification">
                          
                                
                                    <div class="review_section">
                                      @if(count($userreview) > 0)
                                      @foreach($userreview as $feedback)
                                       <?php 
                                             $fmt = datefmt_create(
                                              'pt-BR',
                                              IntlDateFormatter::FULL,
                                              IntlDateFormatter::FULL,
                                              'Brazil/East',
                                              IntlDateFormatter::GREGORIAN,
                                              "dd/MMM/YYYY"  
                                               );
                                                //notification time
                                              $dt = new DateTime($feedback->created_at. "+1 days");
                                              $d = $dt->format('d');
                                              $m = $dt->format('m');
                                              $y = $dt->format('Y');
                                     
                                     
                                              $startdate = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                                      ?>
                                      <div class="main_notificationData main">
                                        
                                        
                                        <div class="notifidata">
                                              
                                          
                                          <div class="media">
                                            <div class="col-md-2 clientimg text-left">
                                              <img class="user_image" src="@if(!empty($feedback->profile_image)) {{ url('/public/images/profileimage/'.$feedback->profile_image) }}@else {{url('/public/images/gravtar.jpeg')}} @endif">
                                            </div>
                                            <div class="col-md-10 ">
                                              <h4 class="user_name ">{{$feedback->name}}
                                                <?php 
                                                  for($i=0; $i<$feedback->rating; $i++){
                                                    echo '<i class="fa fa-star yecolor" aria-hidden="true"></i>';
                                                  }
                                                ?>
                                              <span class="text-right"><img src="{{ url('/')}}/public/admin/images/timer.png" alt="#">{{$startdate}}</span></h4>
                                              
                                              <h5 class="feedback_booking">{{$feedback->category.'/'.$feedback->sub_category }}</h5>
                                                <p class="feedback_message">{{$feedback->message}}</p>
                                                
                                            </div>
                                          </div>
                                          <hr class="notiboxline">
                                        </div>
                                      </div>
                                      
                                     @endforeach
                                     @else
                                     <h2 align="center">Nenhuma avaliação feita até o momento para esse contratante.</h2>
                                     @endif
                                   </div>
                                    

                              </div>
                               
                            </div>
                            <div class="pagination_section "> 
                              {{ $userreview->links() }}
                            </div>
                            <!--===end notification code --==-->
                          </div> 
                        </div>
                      </div> 
                      
                    </div>
                  </div>
                </div>
              </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

  function confirmMessage()
  {
    var retVal = confirm("Are you sure?");
    if( retVal == true ) {
      return true;
    }
    else {
      return false;
    }
  }
  /*================= */
  // $('.success_show').hide();
  $('.deletebutton').click(function(){
    //if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
       var url ="{{ url('/')}}";
      console.log(idd);
    
      $.ajax({
        url: url+"/servicetaker/notifications/destroy",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          $.notify({
                    message: 'Notification deleted successfully!',
                    },{
                    type: 'success',
                    offset: 
                    {
                      x: 10,
                      y: 130
                    },
                    animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
                    },
                  });
          
          jQuery('.main_'+idd).remove();
          
        }            
      });      
    //}
  });
</script>
<style type="text/css">
  .media-body {width: auto;}
  .media {margin-top: 0px;}
  .fa.yecolor {color: #ffcd00;}
</style>
  @stop
