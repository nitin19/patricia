@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        <!-- @include('layouts.flash-message') -->
        @include('layouts.notify-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                 <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" ><a href="{{ url('/servicetaker/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                  <!-- <li role="presentation"><a href="{{ url('/servicetaker/account/card_details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                  <li role="presentation" class="active"><a href="{{ url('/servicetaker/account/notifications')}}" aria-controls="notifications-tabs" role="tab">Notificações</a></li>
                  <li role="presentation"><a href="{{ url('/servicetaker/account/review')}}" aria-controls="closeacc" role="tab">Avaliação do contratante</a></li>
                  <li role="presentation" class="close_account"><a href="{{ url('/servicetaker/account/close')}}" aria-controls="closeacc" role="tab">Encerrar seu cadastro</a></li>
                 
                </ul>
                <div class="tab-content profiletabss">
                  <div role="tabpanel" class="tab-pane active" id="notifications-tabs">
                    <div class="tabcontentbg">
                      <div class="tablewrapper tablediv">
                            <!--====notication code start--==-->
                            <div class="notificationbox">
                              <!-- <div class="alert alert-success alert-block success_show">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>Notification deleted successfully!</strong>
                              </div>  -->
                              <div class="firstnotification">
                             <!--  <php echo "<pre>"; print_r($listresult);die;?> -->
                                @if(count($listresult)>0)
                                   
                                  @foreach($listresult as $allnotifications)
                                    <?php
                                    $booking_message = $allnotifications->booking_message;
                                    $booking_time = $allnotifications->notify_time;
                                    $msg = $allnotifications->notify_message;
                                    $profile_image = $allnotifications->provider_image;
                                    $username = $allnotifications->provider_name;
                                    if($allnotifications->notificaton_type == 'booking') {
                                      if($allnotifications->booking_verified==2) {
                                        $hyperLink =  url('/servicetaker/booking/denied');
                                      } else {
                                        $hyperLink = url('/servicetaker/booking/confirmed');
                                      }
                                    ?>
                                    <div class="main_notificationData main_{{ $allnotifications->notify_id }}">
                                      <div class="notifidata">
                                        <a href="<?php echo $hyperLink;?>" class="btn btn-success join mess" style="background-color: #0c3e64;border: 1px solid #0c3e64;color: #fff;font-size: 12px;padding: 3px 20px;border-radius: 40px;margin-bottom: 10px;">Mensagem recebida</a> 
                                        <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                        <div class="media">
                                          <div class="media-left">
                                            @if($profile_image=='')
                                            <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                            @else
                                            <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                            @endif
                                          </div>
                                          <div class="media-body">
                                      <a href="<?php echo $hyperLink;?>">
                                      <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                      <h3>Mensagem:{{ $msg}}</h3>
                                      </a>
                                          </div>
                                        </div>
                                        <hr class="notiboxline">
                                      </div>
                                    </div>
                                    <?php } elseif($allnotifications->notificaton_type == 'address_approved') { 

                                      if($allnotifications->address_verified==1) {
                                        $hyperLink =  url('/servicetaker/dashboard');
                                      } else {
                                        $hyperLink = url('/servicetaker/dashboard');
                                      }
                                      ?>

                                    <div class="main_notificationData main_{{ $allnotifications->notify_id }}">
                                      <div class="notifidata">
                                        <a href="<?php echo $hyperLink;?>" class="btn btn-success join mess" style="background-color: #0c3e64;border: 1px solid #0c3e64;color: #fff;font-size: 12px;padding: 3px 20px;border-radius: 40px;margin-bottom: 10px;">Mensagem recebida</a> 
                                        <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                        <div class="media">
                                          <div class="media-left">
                                            @if($profile_image=='')
                                            <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                            @else
                                            <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                            @endif
                                          </div>
                                          <div class="media-body">
                                      <a href="<?php echo $hyperLink;?>">
                                      <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                      <h3>Mensagem:{{ $msg}}</h3>
                                      </a>
                                          </div>
                                        </div>
                                        <hr class="notiboxline">
                                      </div>
                                    </div>

                                    <?php } elseif($allnotifications->notificaton_type == 'document') { 

                                      if($allnotifications->document_status==1) {
                                        $hyperLink =  url('/servicetaker/profile');
                                      } else {
                                        $hyperLink = url('/servicetaker/profile');
                                      }
                                      ?>

                                    <div class="main_notificationData main_{{ $allnotifications->notify_id }}">
                                      <div class="notifidata">
                                        <a href="<?php echo $hyperLink;?>" class="btn btn-success join mess" style="background-color: #0c3e64;border: 1px solid #0c3e64;color: #fff;font-size: 12px;padding: 3px 20px;border-radius: 40px;margin-bottom: 10px;">Mensagem recebida</a> 
                                        <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                        <div class="media">
                                          <div class="media-left">
                                            @if($profile_image=='')
                                            <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                            @else
                                            <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                            @endif
                                          </div>
                                          <div class="media-body">
                                      <a href="<?php echo $hyperLink;?>">
                                      <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                      <h3>Mensagem:{{ $msg}}</h3>
                                      </a>
                                          </div>
                                        </div>
                                        <hr class="notiboxline">
                                      </div>
                                    </div>

                                    <?php } ?>
                                  @endforeach
                                @else
                                <h2>Nenhuma notificação encontrada.</h2>
                                @endif

                              </div>
                               
                            </div>
                            <div class="pagination_section "> 
                              {{ $listresult->links() }}
                            </div>
                            <!--===end notification code --==-->
                          </div> 
                        </div>
                      </div> 
                      
                    </div>
                  </div>
                </div>
              </div>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

  function confirmMessage()
  {
    var retVal = confirm("Are you sure?");
    if( retVal == true ) {
      return true;
    }
    else {
      return false;
    }
  }
  /*================= */
  // $('.success_show').hide();
  $('.deletebutton').click(function(){
    //if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
       var url ="{{ url('/')}}";
      console.log(idd);
    
      $.ajax({
        url: url+"/servicetaker/notifications/destroy",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          $.notify({
                    message: 'Notification deleted successfully!',
                    },{
                    type: 'success',
                    offset: 
                    {
                      x: 10,
                      y: 130
                    },
                    animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
                    },
                  });
          
          jQuery('.main_'+idd).remove();
           
        }            
      });      
    //}
  });
</script>
<style type="text/css">
  .media-body {width: auto;}
  .media {margin-top: 0px;}
</style>
  @stop
