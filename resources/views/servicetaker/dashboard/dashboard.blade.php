@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.notify-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="dashboard">
            <div class="istbox">
              <div class="container">
                <div class="row">
                  <div class="col-sm-8 full_box">
                    <div class="profile_box">
                      <div class="row  box-clr">
                        <div class="col-sm-4 text-center">
                          @if(!empty($user_info->profile_image ))
                          <img class="profimg" src="{{url('public/images/profileimage')}}/<?php echo $user_info->profile_image; ?>" alt="dashboardimg" width="100%">
                          @else
                          <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="profimg" alt="dashboardimg" width="100%">
                          @endif
                        </div>
                        <div class="col-sm-8 text_content">
                          <h3>{{ucfirst($user_info->name)}}</h3>
                          <ul class="style-chnage">
                          	<li><span> Email:</span> {{$user_info->email}} </li>
                            <li class=""><span> Endereço completo: </span>{{$user_info->address}}<?php if($user_info->street_number!=''){
                                echo ',';} else { echo '';} ?> {{$user_info->street_number}}<?php if($user_info->complemento!=''){
                                echo ',';} else { echo '';} ?> {{$user_info->complemento}}</li>     
                            <li><span> Celular: </span>{{$user_info->phone}}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="topicon">
                      <?php $url = '/servicetaker/profile'; ?>
                      <a href="{{url($url)}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon21.png" alt="dashboard_designi" width="100%" class="img-responsive"></a>
                    </div> 
                  </div>
                  <div class="col-sm-4 baby_box">
                    <div class="singlepagesidebar">
                      <div class="box_div invite_box">
                        <img class="inviteimg inving" src="{{url('/public/images/14_dashboard_design.icon1.png')}}" alt="design" width="100%">
                        <div class="paid">
                          <h3>Convide um amigo </h3>
                        </div>
                        <form class="form-inline inviteform dashboardinviteform" id="inviteform">
                          <div class="form-group nopadding nopadding copybtn" style="display: block;">

                            <input class="col-xs-9" id="myInput" value="{{url('/')}}/invite-friend/{{$user_info->referal_code}}" type="text" readonly>

                            
                            <input class="btn btn-default col-xs-3 redme" value="Copiar o link" onclick="CopyText()" type="button">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
             <div class="container">  
             
              <div class="singlepagesidebar_line"> </div>
              <div id="map" style="width: 100%; height: 400px; background: #89C4FA; margin-bottom: 20px;" /></div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style type="text/css">
    .singlepagesidebar_line {
      box-shadow: 7px 7px 10px 0 rgba(76, 110, 245, .1);
      float: left;
      padding: 6px;
      width: 100%;
      border-top: 5px solid #39c;
    }
    </style>
    
<script type="text/javascript">
  function CopyText() {
       var copyText = document.getElementById("myInput");
       copyText.select();
       document.execCommand("copy"); 
       $.notify({
          message: 'Copiado!' 
          },{
          type: 'success',
          offset: 
          {
            x: 10,
            y: 130
          },
          animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
          },
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y"></script>

<script  type="text/javascript" charset="UTF-8" >

/* carousel script for user gallery*/
  $('.carousel').carousel({
    interval: 2000,
    cycle:true
  });

  var lat = '<?php echo $latitude_center; ?>';
  var lan = '<?php echo $longitude_center; ?>';
  var center = new google.maps.LatLng(lat, lan);     
  function initMap() {
      // MAP ATTRIBUTES.
      var mapAttr = {
                center: center,
                zoom: 14,
                zoomControl: false,
                gestureHandling: 'none',
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };

      // THE MAP TO DISPLAY.
      var map = new google.maps.Map(document.getElementById("map"), mapAttr);
      var circle = new google.maps.Circle({
                  center: center,
                  map: map,
                  radius: 1000,          // IN METERS.
                  fillColor: 'rgba(0, 128, 0, 0.7)',
                  fillOpacity: 0.5,
                  strokeColor: "rgba(55, 85, 170, 0.6)",
                  strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
              });
  }
  google.maps.event.addDomListener(window, 'load', initMap);
</script>
 @stop
