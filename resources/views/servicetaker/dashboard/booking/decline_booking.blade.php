@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')

<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
             <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation" ><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul> 
          </div>
        </div>
        @include('layouts.flash-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane codeRefer active" id="bookingmsg">
            <div class="bookingmsg_sec">
              <div class="container">
                <div class="bookmsgsec">
                  <!-- <h4>Bookings & Message</h4> -->
                  <div class="bookmsgtab" id="">
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="pending" role="tab" >Pendentes <span class="tkr-count">@if($booking_payment_pending_count>0)({{ $booking_payment_pending_count }})@endif</span></a></li>
                      <li role="presentation" ><a href="{{ url('/servicetaker/booking/confirmed')}}" aria-controls="confirmed" role="tab" >Aceitas <span class="tkr-count">@if($booking_payment_confirm_count>0)({{$booking_payment_confirm_count}}) @endif</span></a></li>
                      <li role="presentation" class="active"><a href="{{ url('/servicetaker/booking/denied')}}" aria-controls="past" role="tab" >Negadas <span class="tkr-count">@if($booking_payment_past_count>0)({{ $booking_payment_past_count }}) @endif</span></a></li>
                    </ul> 
                    <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="past">
                      <div class="tablewrapper tablediv">
                        @if(count($booking_payment_past) > 0)
                        <table class="table-bordered table-striped">
                          <thead class="tableheader">
                            <tr class="headings">
                              <th class="column3">Nome</th>
                              <th class="column4">Mensagem</th>
                              <th class="column3">Categoria</th>
                              <th class="column3">Serviço</th>
                              <th class="column3">Data de início</th>
                              <!-- th class="column2">Data final</th> -->
                              <th class="column4">Valor base</th>
                            </tr>
                          </thead>
                          <tbody class= "data_fill_reject">
                            
                            @foreach($booking_payment_past as $booking_past_pending_info)
                            <?php
                              $bookingid = $booking_past_pending_info->booking_id;
                              $payment_pastbooking_message = $booking_past_pending_info->booking_message;
                              $fmt = datefmt_create(
                                'pt-BR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL,
                                'Brazil/East',
                                IntlDateFormatter::GREGORIAN,
                                "dd/MMM/YYYY"  
                              );
                                //start time
                              $dt = new DateTime($booking_past_pending_info->booking_start_time. "+1 days");
                              $d = $dt->format('d');
                              $m = $dt->format('m');
                              $y = $dt->format('Y');
                                //end time
                              $edt = new DateTime($booking_past_pending_info->booking_end_time. "+1 days");
                              $ed = $edt->format('d');
                              $em = $edt->format('m');
                              $ey = $edt->format('Y');
                           
                            ?>
                              <tr class="tabledata confirmedtable tkr past_{{ $booking_past_pending_info->booking_id }}" >
                                <td class="username column3 pmm">
                                  <a href="{{url('/').'/profile/'.$booking_past_pending_info->profile_id}}"><h4>
                                    <a href="{{url('/').'/profile/'.$booking_past_pending_info->profile_id}}">
                                  @if($booking_past_pending_info->profile_image=='')
                                  <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                  @else
                                  <img src="{{ url('/public/images/') }}/profileimage/<?php echo $booking_past_pending_info->profile_image; ?>">
                                  @endif
                                  <span>{{ substr(ucfirst($booking_past_pending_info->name),0,13) }} @if (strlen($booking_past_pending_info->name)>='13')...@endif</span></a></h4></td>
                                  <td class="userdesc column4" id="msgrow{{$bookingid}}"><p><?php if(strlen($payment_pastbooking_message) > 100){ echo substr($payment_pastbooking_message, 0, 40). '<span class="dots" id="dotmsg'.$bookingid.'">...</span><span class="readmore" id="moremsg'.$bookingid.'">'.substr($payment_pastbooking_message, 100, strlen($payment_pastbooking_message)).'</span><span class="btnmore">Veja mais</span><span class="btnless">Veja menos</span>';}else{ echo $payment_pastbooking_message;} ?></p>
                                  </td>
                                  <td class="column3">{{ $booking_past_pending_info->category }}</td>
                                  <td class="column3">{{ $booking_past_pending_info->sub_category }}</td>
                                  <td class="column3">{{  datefmt_format($fmt, mktime(0,0,0,$m,$d,$y)) }}</td>
                                 <!--  <td class="column2">{{  datefmt_format($fmt, mktime(0,0,0,$em,$ed,$ey)) }}</td> -->
                                  
                                  <?php if($booking_past_pending_info->booking_amount!=''){ ?>
                                  <td class="column4">{{ $booking_past_pending_info->booking_amount }}</td>
                                  <?php } else { ?>
                                  <td class="column4 no_data"></td>
                                  <?php } ?>
                                </tr>
                              @endforeach
                              @else
                              <tr>
                                <h2>Sem propostas negadas para você no momento.</h2>
                              </tr>
                              @endif
                            </tbody>
                          </table>
                          <div class="pagination_section col-sm-6 col-md-6"> 
                            {{ $booking_payment_past->links() }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>   
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function () {
  /*$(".confirmedtable").click(function() {
    var h = $(this).data("href");
    console.log(h);
        window.location = $(this).data("href");
    });*/

  $(".btnmore").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
  });

  $(".btnless").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
  });

});
</script>
<style type="text/css">
  .readmore, .btnless{
    display: none;
  }
  .no_data {min-height: 40px;}
</style>

@stop
