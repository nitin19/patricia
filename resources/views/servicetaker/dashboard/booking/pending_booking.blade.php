@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation" ><a href="{{ url('/servicetaker/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation" class="active"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="bookingmsg" role="tab" > Contatos & Mensagens</a></li>
              <li role="presentation"><a href="{{ url('/servicetaker/account/details')}}" aria-controls="settings" role="tab" >Sua conta</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="{{ url('/servicetaker/switch/role')}}" aria-controls="switch" role="tab" >Mudar de função</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.flash-message')
        <?php $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;  ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane codeRefer active" id="bookingmsg">
            <div class="bookingmsg_sec">
              <div class="container">
                <div class="bookmsgsec">  
                  <!-- <h4>Bookings & Message</h4> -->
                  <div class="bookmsgtab" id="">
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="{{ url('/servicetaker/bookings')}}" aria-controls="pending" role="tab" >Pendentes <span class="tkr-count">@if($booking_payment_pending_count>0)({{ $booking_payment_pending_count }})@endif</span></a></li>
                      <li role="presentation" ><a href="{{ url('/servicetaker/booking/confirmed')}}" aria-controls="confirmed" role="tab" >Aceitas <span class="tkr-count">@if($booking_payment_confirm_count>0)({{$booking_payment_confirm_count}}) @endif</span></a></li>
                      <li role="presentation"><a href="{{ url('/servicetaker/booking/denied')}}" aria-controls="past" role="tab" >Negadas <span class="tkr-count">@if($booking_payment_past_count>0)({{ $booking_payment_past_count }}) @endif</span></a></li>
                    </ul> 
                    <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pending">
                          <div class="tablewrapper tablediv">
                            <div class="alert alert-info alert-dismissible   statusAlerts" style="display:none;">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </div> 
                            @if($booking_payment_pending_count>0)
                            <table class="table-bordered table-striped ">
                              <thead class="tableheader">
                                <tr class="headings">
                                  <th class="column3">Nome</th>
                                  <th class="column5">Mensagem</th>
                                  <th class="column3">Categoria</th>
                                  <th class="column2">Serviço</th>
                                  <th class="column2">Data de início</th>
                                 <!--  <th class="column2">Data final</th> -->
                                  <th class="column5">Valor base</th>
                                </tr>
                              </thead>
                              <tbody class="data_fill_pending">
                                @foreach($booking_payment_pending as $booking_payment_pending_info)
                                <?php
                                $bookingid = $booking_payment_pending_info->booking_id;
                                $payment_pendinguser_book_by = $booking_payment_pending_info->user_id;
                                $payment_pendingbooking_message = $booking_payment_pending_info->booking_message;
                                $payment_pendingbooking_amount = $booking_payment_pending_info->booking_amount;
                                $payment_pendingcategory = $booking_payment_pending_info->category;
                                $fmt = datefmt_create(
                                'pt-BR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL,
                                'Brazil/East',
                                IntlDateFormatter::GREGORIAN,
                                "dd/MMM/YYYY"  
                                );
                                  //start time
                                $dt = new DateTime($booking_payment_pending_info->booking_start_time. "+1 days");
                                $d = $dt->format('d');
                                $m = $dt->format('m');
                                $y = $dt->format('Y');
                                  //end time
                                $edt = new DateTime($booking_payment_pending_info->booking_end_time. "+1 days");
                                $ed = $edt->format('d');
                                $em = $edt->format('m');
                                $ey = $edt->format('Y');
                                $payment_pendingbooking_start_time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                                $payment_pendingbooking_end_time = datefmt_format($fmt, mktime(0,0,0,$em,$ed,$ey)); 
                                $payment_pendingmuser_book_to = $booking_payment_pending_info->profile_id;
                                $paymentuser_info = DB::table('users')
                                  ->where('id', $payment_pendingmuser_book_to)
                                  ->get()->first();
                                 
                              /* echo "<pre>";
                                print_r($paymentuser_info);
                                echo "</pre>";*/
                                //echo  $paymentuser_info->name ?? '-- unset--';
                                $paymentname = data_get($paymentuser_info, 'name');
                                $paymentprofile_image = data_get($paymentuser_info, 'profile_image');
                                //$paymentname = $paymentuser_info->name;
                                //$paymentprofile_image = $paymentuser_info->profile_image;
                                if($paymentprofile_image==''){
                                  $payment_pendinguserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                                }
                                else{
                                  $payment_pendinguserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                                }
                                ?>
                                <tr class="tabledata confirmedtable tkr" id= "pending_{{ $booking_payment_pending_info->booking_id  }}" >
                                  <td class="column3 pmm"><a href="{{url('/').'/profile/'.$booking_payment_pending_info->profile_id}}"><h4>
                                    @if($paymentprofile_image=='')
                                    <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                    @else
                                    <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>">
                                    @endif
                                    <span>{{ substr(ucfirst($paymentname),0,13) }} @if (strlen($paymentname)>='13')...@endif</span></a></h4>
                                  </td>  
                                  <td class="userdesc column5" id="msgrow{{$bookingid}}"><p><?php if(strlen($payment_pendingbooking_message) > 100){ echo substr($payment_pendingbooking_message, 0, 40). '<span class="dots" id="dotmsg'.$bookingid.'">...</span><span class="readmore" id="moremsg'.$bookingid.'">'.substr($payment_pendingbooking_message, 100, strlen($payment_pendingbooking_message)).'</span><span class="btnmore">Veja mais</span><span class="btnless">Veja menos</span>';}else{ echo $payment_pendingbooking_message;} ?></p>
                                  </td>
                                  <td class="column3">{{ $payment_pendingcategory }}</td>
                                  <td class="column2">{{ $booking_payment_pending_info->sub_category }}</td>
                                  <td class="column2">{{ $payment_pendingbooking_start_time }}</td>
                                  <!-- <td class="column2">{{ $payment_pendingbooking_end_time }}</td> -->
                                  <?php if($payment_pendingbooking_amount!=''){ ?>
                                  <td class="column5">{{ $payment_pendingbooking_amount }}</td>
                                  <?php } else { ?>
                                  <td class="column5 no_data"></td>
                                  <?php } ?>
                                  
                                </tr>
                              @endforeach
                              </tbody>
                            </table>
                            @endif
                            <div class="pagination_section col-sm-6 col-md-6"> 
                            {{ $booking_payment_pending->links() }}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function () {
  /*$(".confirmedtable").click(function() {
    var h = $(this).data("href");
    console.log(h);
        window.location = $(this).data("href");
    });*/

  $(".btnmore").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
  });

  $(".btnless").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
  });

});
</script>
<style type="text/css">
  .readmore, .btnless{display: none;}
  .column3 {width: 15% !important;float: left;}
  .no_data {min-height: 40px;}
</style>
@stop
