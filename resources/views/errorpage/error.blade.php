@extends('layouts.default-header')

@section('title', 'Error')

@section('content')

   <div class="col-md-12 dashboard-right">

          <div class="cut-dash-inner">

                 <div class="homepagetable">

                   <div class="row">
                     <div class="col-sm-12">
                  <?php 
                    $errors = DB::table('error_logs')->orderBy('id','DESC')->paginate('10');
                    $count  = count($errors);    
                  ?>
                @if($count > '0')

                    <div class="table-responsive">
                      <table class="table table-striped history-table">
                      <thead style="background-color: #1b9bd8;">
                      <tr>
                        <th class="col13">Error Message</th>
                        <th class="col1">Line</th>
                        <th class="col4">File Name</th>
                        <th class="col2">Error Date</th>
                      </tr>

                    </thead>
                    <tbody>
                    @foreach($errors as $error)
                      <tr>
                        <td  class="col13" data-label="Error message">{{$error->error_message}}</td>
                        <td  class="col1" data-label="Error line number">{{$error->line_number}}</td>
                        <td  class="col4" data-label="Error file name">{{$error->file_name}}</td>
                        <td  class="col2" data-label="Time">{{date('d M Y',strtotime($error->created_at))}}</td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div> 
               @else              
                <div class="table-error-mesg">
                    <p>Yepiee!!!!! No Error found</p>
                    
                </div>
              @endif
              </div>
              <div>{{$errors->render()}}</div>
            </div>
          </div>
     </div>
   </div>
</div>
</div>
</section>

@stop