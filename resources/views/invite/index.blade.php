@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<section class="maincontent searchbarbg">
  @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
  <script>
    $.notify({
      message: 'Favor completar as informações do seu cadastro.' 
      },{
      type: 'danger',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
  </script>
  @endif
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
      <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
          <h1>Convide seus amigos!</h1>
        </div>
      </div>
    </div>
    <div class="invitepagecontent">
        <div class="container">
          <div class="col-sm-12 invitepage_div centered maxmax-width">
              <p>Copie e envie o link abaixo para sua rede de relacionamento ou digite o email de quem você quer indicar no campo seguinte.</p>
              <form class="form-inline inviteform" id="inviteform">
              <div class="form-group col-sm-9 nopadding">
                <input type="text" class="form-control" id="myInput" value="{{ url('/') }}/invite-friend/{{ $user_info->referal_code }}" readonly>
              </div>
              <div class="col-sm-3 nopadding copybtn">
              <input type="button" class="btn btn-primary" id="copy_referal" value="Copiar o link" onClick="CopyText()">
            </div>
            </form>
            <div class="getfrndsec">
                <!-- <h2>Your friends get a &#82;&#36;10 discount</h2>
                <h3>You get &#82;&#36;10 off your next booking!</h3> -->
               <!--  @include('layouts.flash-message') -->
              @include('layouts.notify-message')
              <form class="form-inline getfriendform" id="getfriendform" method="post" action="{{ url('/invite-friend') }}/send">
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="text" class="form-control" id="email" placeholder="Digite o email de quem você quer recomendar" name="email" required>
                  <input type="hidden" name="referal_link" value="{{ url('/') }}/invite-friend/{{ $user_info->referal_code }}">
                </div>
                <div class="sendbtn">
                  <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
            </form>
            <!-- <div class="getbaldiv">
              <h1>&#82;&#36;0,00</h1>
              <h5>Current Cradit Balance to Spend</h5>
              </div> -->
            <div class="invite-img">
               <img src="{{ url('/public/') }}/images/inviteimg.jpg">
            </div>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  function CopyText() {
   var copyText = document.getElementById("myInput");
   copyText.select();
   document.execCommand("copy"); 
   $.notify({
  message: 'Copiado' 
  },{
  type: 'success',
  offset: 
  {
    x: 10,
    y: 130
  },
  animate: {
    enter: 'animated fadeInRight',
    exit: 'animated fadeOutRight'
  },
});
}
 jQuery("#getfriendform").validate({
        rules: {
           email: {
            required: true,
            email: true
          }
        },
        messages: {
          email: {
            required: "Digite o e-mail.",
            email: "E-mail incorreto"
          }
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
</script>

@stop
