@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')

<div class="lock_box">
	<div class="yelloback">
		<div class="col-sm-12">
			<div class="service_box tablediv tablesection">
			<!-- <div class="alert alert-success alert-block success_show">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>Request submitted successfully!</strong>
			</div> -->
			<div class="row ppppppppppppp" style="margin-bottom: 20px;">
				<div class="col-sm-12">
					<div class="media payment_box">
						<!-- <div class="col-sm-10"> -->
							<div class="media-body button_side">
								<h3>Close Request List<span class="list_clr"> ({{ count($close_account_request) }})</span></h3>
							</div>
						<!-- </div> -->
					</div>
				</div>
			</div>
			<div class="row"> 
			<div class="col-sm-12 yyyyyyyyy">
			<table id="close_request" class="table table-borderless heading-name">
			    <thead>
			      <tr>
			        <th class="column1">Sr No.</th>
			        <th class="column2">Name</th>
			        <th class="column3">Email</th>
			        <th class="column3">Reason</th>
			        <th class="column2">Phone </th>
			        <th class="column4">Address</th>
			        <th class="column2">Status</th>
			        <th class="column3">Date of close request</th>
			      </tr>
			    </thead> 
			    <tbody>
				    @if(count($close_account_request)> 0)
				       	<?php $count = 0; ?>
					    @foreach($close_account_request as $close_account_request_info)
					   
				    <tr class="closereq_table tabledata">
				        <td class="column1">{{ ++$count }}</td>
				        <td class="column2">{{ $close_account_request_info->name }}</td>
				        <td class="column3"><span class="painting">{{ $close_account_request_info->email }}</span></td>
				        <td class="column3">{{ ucfirst($close_account_request_info->close_reason) }}</td>
				        <td class="column2">{{ $close_account_request_info->phone }}</td>
				        <td class="column4">{{ $close_account_request_info->address }}</td>
				        <td  class="column2" id="{{$close_account_request_info->userid}}" data-value="{{$close_account_request_info->requestid}}">
				        	@if($close_account_request_info->admin_activity=='0')
				          <select name="action_request" id="action_request_<?php echo $close_account_request_info->userid; ?>" class="select">
				            <option value="0">Pending</option>
				            <option value="1">Accepted</option>
				            <option value="2">Rejected</option>
				            </select>
				            @else
				            <input name="action_request" type="text" readonly="" disabled="true" value="@if($close_account_request_info->admin_activity=='2'){{ 'Rejected'}} @else {{'Accepted'}} @endif" style="width: 80px;text-align: center;">
				            @endif
				        </td>
				        <td class="column3">{{ $close_account_request_info->created_at }}</td>
				    </tr>
			      	@endforeach
			      	@else
			      	<tr><td align="center" colspan="7">No Record Found</td></tr>
			      	@endif
			    </tbody> 
			</table>
			</div> 
		
				<script type="text/javascript">
				    // jQuery('.success_show').hide();
				    jQuery('select').on('change', function() {
				    var action = jQuery(this).val();
				    var parent_id = $(this).closest('td').attr('id');
				    var request_id = $(this).closest('td').attr('data-value');
				    jQuery.ajax({
				          url: 'requestaction',
				          type: 'POST',
				          data: {'action': action,'userid': parent_id, "request_id":request_id, "_token": "{{ csrf_token() }}" },
				          success: function(response) {
				          	$.notify({
						      message: 'Request submitted successfully!',
						      },{
						      type: 'success',
						      offset: 
						      {
						        x: 10,
						        y: 130
						      },
						      animate: {
						        enter: 'animated fadeInRight',
						        exit: 'animated fadeOutRight'
						      },
						    });

				          }            
				          });
				    });
				</script>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#close_request').DataTable();
} );
</script>
@stop
