<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ url('/public') }}/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/style.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/sidebar.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/dashboard.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/animate.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/dropdown.css" rel="stylesheet">
  </head>
  <body>
<nav class="navbar navbar-default navigationbar"> @include('layouts.header-admin') </nav></header>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures wow fadeInLeft">
     @include('layouts.sidebar-admin') </div>
@yield('content')
<footer> @include('layouts.footer-admin') </footer> 
<script src="{{ url('/public') }}/admin/js/jquery.min.js"></script>
<script src="{{ url('/public') }}/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ url('/public') }}/admin/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ url('/public') }}/admin/js/wow.min.js"></script>
<script src="{{ url('/public') }}/admin/js/jquery-1.11.3.min.js"></script>

<script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      alert('OK');
    $(".list-group a").click(function() { 
      $('.list-group a').removeClass('selected');
      $(this).addClass('selected');
    }); 
});
</script>

<script>
jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});
$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
});
});
</script>  
<style type="text/css">   
  .myclass { border:5px solid red;}

@media (max-width: 767px){
}

</style>
  </body>
</html>