@extends('layouts.default-header-admin')

@section('title', 'Edit Service')

@section('content')
<div class="content-wrap">
<div class="yelloback">
<div class="container-fluid">
<div class="seredback">
<div class="row"> 
<div class="col-sm-12 content_box">
<div class="row mb-2">
    <div class="col-sm-8 col-xs-8 category_title">
        <h3> Edit For {{ $category->cat_name }} Service</h3>
    </div>
    <div class="col-sm-4 col-xs-4 createbtn">
        <a href="{{ url('/admin-services') }}" class="btn btn-primary button_blueblue">Back</a>
    </div>
</div>

@include('layouts.flash-message')
 <form class="" id="contactform" method="post" action="{{ url('/admin-services/update') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-6">
    <label>Select an sub category</label>
    <input class="form-control" name="service_name" placeholder="Service Name" type="text"  value="{{ $category->cat_name }}" required >
</div>

<div class="form-group col-md-6">
    <label>Please Select an main category</label>
    <select name="main_cat_id" class="form-control" required>
    @foreach($main_category as $category_id=>$category_value)
        <option <?php if($category_id == $category->main_cat_id){ echo 'selected'; } ?> value="{{$category_id}}">{{$category_value}}</option>
    @endforeach
</select>

</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    @if($category->cat_image!='')
    <img src="{{ url('/public/images/categoryimage/') }}/{{$category->cat_image}}" class="edit_service_img">
    @endif
    <input type="file" name="service_img" id="service_img">
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    <input type="hidden" name="service_id" id="service_id" value="{{ $category->cat_id }}">
<textarea id="comment" placeholder="Description" name="service_description" cols="40" rows="7" class="form-control service_description" style="color: #323334 !important;"  maxlength="250">{{ $category->cat_description }}</textarea>
</div>
</div>  
<div class="row">
    <div class="col-sm-12 nextbtn">
        <button type="submit" class="btn button_blueblue">Update</button>
        <!-- <input class="form-control" value="Save" type="submit"> -->
    </div>   
 </div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>
<script>
    $(document).ready(function(){
        CKEDITOR.replace( 'comment' );
    })
       
</script>
@stop