@extends('layouts.default-header-admin')

@section('title', 'Add Service')

@section('content')
<div class="content-wrap">
 <div class="yelloback">
<div class="container-fluid">
<div class="seredback">  
<div class="row">
<div class="col-sm-12 content_box">
    <div class="row mb-2">
        <div class="col-sm-8 col-xs-8 category_title">
            <h3 >Add Subcategories</h3>
        </div>
        <div class="col-sm-4 col-xs-4 createbtn">
            <a href="{{ url('/admin-services') }}" class="btn btn-primary button_blueblue">Back</a>
        </div>
    </div>

@include('layouts.flash-message')
 <form class="" id="contactform" method="post" action="{{ url('/admin-services/store') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-6">
    <label>Please Enter the service name</label>
    <input class="form-control" name="service_name" placeholder="Service Name" type="text" required >
</div>
<div class="form-group col-md-6">
    <label>Please Select an main category</label>
<select name="main_category_id" class="form-control" required>
     <option value="" selected disabled="disabled">Please Choose One</option>
    @foreach($main_category as $category_id=>$category_value)
        <option value="{{$category_id}}">{{$category_value}}</option>
    @endforeach
</select>

</div>

</div>
<div class="row">

<div class="form-group col-md-6">
    <label>Please Select an Image</label>
<input type="file" name="service_img" id="service_img">
</div>
<div class="form-group col-md-6">
    
</div>
  
</div>
<div class="row">
<div class="form-group col-md-12">
<textarea id="comment" placeholder="Description" name="service_description" cols="40" rows="7" class="form-control service_description" style="color: #323334 !important;"  maxlength="250"></textarea>

</div>
</div>
<div class="form-button">
<button type="submit" class="btn btn-primary button_blueblue">Save</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>

<script>
    $(document).ready(function(){
        CKEDITOR.replace( 'comment' );
    })
       
</script>
@stop