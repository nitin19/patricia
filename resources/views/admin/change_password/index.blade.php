@extends('layouts.default-header-admin')

@section('title', 'Change Password')

@section('content')
<div class="content-wrap">
 
  <div class="yelloback">
    <div class="formdiv loginformdiv ">
      <div class="col-sm-8 col-md-7 col-lg-5 centered"> <div class="message">
          @if(session()->has('success'))
          <div class="alert alert-success">
              {{ session()->get('success') }}
          </div>
          @endif  
            @if(session()->has('error'))
          <div class="alert alert-danger">
              {{ session()->get('error') }}  
          </div>
          @endif
      </div>

        <div class="registerformbg dashboard-cp">
           <?php $id=Auth::user()->id;?>
                        <form class="form-horizontal registerform formarea" method="POST" id="changepassword" action="{{url('/admin-change-password/') }}">
                        {{ csrf_field() }}
                        <!-- <input type="hidden" name="_method" value="PATCH"> -->
                        <h2 class="text-center  flipInY">Change Password</h2> 
                        <p class="text-center  ">Change Password by entering the information below</p>
                        <div class="form-group forminput  ">
                        <input id="oldpassword" type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Old Password" required autofocus>
                       <span toggle="#oldpassword" class="fa fa-fw fa-eye   toggle-password">
                        </div>
                      <!-- <div class="form-group forminput  fadeInLeft">
                        <input id="confirm_oldpassword" type="password" class="form-control" name="confirm_oldpassword" id="confirm_oldpassword" placeholder="Confirm Old Password" required>
                        <span toggle="#confirm_oldpassword" class="fa fa-fw fa-eye   toggle-password">
                        </div> -->

                        <div class="form-group forminput">
                        <input id="newpassword" type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password" required>
                        <span toggle="#newpassword" class="fa fa-fw fa-eye   toggle-password"></span>
                        </div>

                     <div class="form-group forminput">
                     <input type="password" class="form-control" name="newpassword_confirmation" id="newpassword_confirmation" placeholder="Confirm New Password" required>
                     <span toggle="#newpassword_confirmation" class="fa fa-fw fa-eye   toggle-password">
                     </div>
                    <div class="form-group  flipInX">
                    <input type="submit" class="btn btn-primary btn-lg" value="Save" onClick="validatePassword();">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
/*$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*#$%^&]*$/.test(value)
       && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[!@*#$%^&]/.test(value)
       && /\d/.test(value)
});*/
function validatePassword()
{
  var validator = $("#changepassword").validate({
         rules: {
            oldpassword: {  
                required: true,
               // pwcheck: true,
                minlength: 6,
                maxlength: 15  
            },
            // confirm_oldpassword: {
            //     minlength: 6,
            //     maxlength: 15, 
            //     equalTo: "#oldpassword"
            // },
            newpassword: {  
                required: true,
                //pwcheck: true,
                minlength: 6,
                maxlength: 15   
            },
            newpassword_confirmation: {
                minlength: 6,
                maxlength: 15,   
                equalTo: "#newpassword"
            }
         },
         messages: {
            oldpassword: {
                required: "This field is required",
                pwcheck: "Old password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "Old password length minimum 6 character",
                maxlength: "Old password length minimum 15 character"
            },
            // confirm_oldpassword: {
            //     minlength: "Confirm old password length minimum 6 character",
            //     maxlength: "Confirm old password length minimum 15 character",
            //     equalTo: "Confirm old password does not match with old password"
            // },
            newpassword: {
                required: "This field is required",
                pwcheck: "New password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "New password length minimum 6 character",
                maxlength: "New password length minimum 15 character"
            },
            newpassword_confirmation: {
                minlength: "Confirm new password length minimum 6 character",
                maxlength: "Confirm new password length minimum 15 character",
                equalTo: "Confirm new password does not match with new password"
            }
         },
    });
  }
$("document").ready(function(){
    setTimeout(function(){
        $("div.alert").remove();
    }, 5000 ); // 5 secs

});
</script>
<style type="text/css">
  .fa-fw {
    width: 1.28571429em;
    text-align: center;
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
}
.form-group.forminput {position: relative;}
</style>
@endsection