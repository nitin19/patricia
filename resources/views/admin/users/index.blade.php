@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box tablediv tablesection">
  @include('layouts.flash-message')
  <table class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th>Name/Email</th>
        <th>Role</th>
        <th>Category</th>
        <th>Phone </th>
        <th>Address</th>
        <th>Address Approve</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($users as $user_info)
<?php
      $name = $user_info->name;
      $email = $user_info->email;
      $is_active = $user_info->is_active;
?>
<tr class="userstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td>{{ $name }}<br><span class="painting">{{ $email }}</span></td>
      <td>{{ ucfirst($user_info->user_role) }}</td>
      <td>{{ ucfirst($user_info->category) }}</td>
      <td>{{ $user_info->phone }}</td>
      <td>{{ $user_info->address }}</td>
      <td><form method="POST" action="{{ url('/admin-users/approved_address') }}">@if($user_info->address_verified == 1)
          <button class="btn btn-success btn-sm" name="approve_status" type="submit" value="2">Approved</button>
          @elseif($user_info->address_verified == 2)
          <button class="btn btn-danger btn-sm" name="approve_status" type="submit" value="1">Denied</button>
          @else
           <button class="btn btn-success btn-sm" name="approve_status" type="submit" value="1">Approve</button>
            <button class="btn btn-danger btn-sm" name="approve_status" type="submit" value="2">Deny</button>
          @endif
          <input type="hidden" name="user_id" value="{{ $user_info->id }}">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          </form>
      </td>
      <td><a href="{{ url('/admin-users/show') }}/{{ $user_info->id }}"><i class="fa fa-eye" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-users/edit') }}/{{ $user_info->id }}"><i class="fa fa-pencil" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i class="fa fa-trash" style="font-size:16px"></i></a></td>
</tr>
@endforeach
    </tbody>
  </table>
  <div class="pagination-button">

<div class="pagination prev_next">
 {!! $users->render() !!}
</div>
  </div>
</div>
  </div>
 </div>
 </div>
@stop