@extends('layouts.default-header-admin')

@section('title', 'Add Service')

@section('content')
<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">
<div class="form_section">
<div class="col-sm-12 content_box">
<h3>Add Service</h3>
@include('layouts.flash-message')
 <form class="contactform" id="contactform" method="post" action="{{ url('/admin-subservices/store') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-12">
<input class="form-control" name="service_name" placeholder="Service Name" type="text" required>
<input type="hidden" value="{{ $catid }}" name="cat_id">
</div>
</div>
<div class="row">
<div class="form-group col-md-12">
<input type="file" name="service_img" id="service_img">
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
<textarea id="comment" placeholder="Description" name="service_description" cols="40" rows="7" class="form-control service_description" style="color: #323334 !important;" required></textarea>
</div>
</div>
<div class="form-button">
<button type="submit" class="btn btn-box">Send Message</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>
@stop