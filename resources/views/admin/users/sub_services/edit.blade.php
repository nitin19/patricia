@extends('layouts.default-header-admin')

@section('title', 'Edit Service')

@section('content')
 <div class="lock_box">
<div class="contact_box">
<div class="container">
<div class="form_section">
<div class="row">
<div class="col-sm-11 content_box">
<h3> Edit {{ $sub_category->subcat_name }} Service <a href="{{ url('/admin-subservices') }}/{{ $sub_category->cat_id }}" class="back_btn">Back</a></h3>

@include('layouts.flash-message')
 <form class="contactform" id="contactform" method="post" action="{{ url('/admin-subservices/update') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-12">
<input class="form-control" name="service_name" placeholder="Service Name" type="text"  value="{{ $sub_category->subcat_name }}" required>
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    @if($sub_category->subcat_image!='')
    <img src="{{ url('/public/images/categoryimage/') }}/{{$sub_category->subcat_image}}">
    @endif
    <input type="file" name="service_img" id="service_img">
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    <input type="hidden" name="service_id" id="service_id" value="{{ $sub_category->subcat_id }}">
    <input type="hidden" name="cat_id" id="cat_id" value="{{ $sub_category->cat_id }}">
<textarea id="comment" placeholder="Description" name="service_description" cols="40" rows="7" class="form-control service_description" style="color: #323334 !important;" required>{{ $sub_category->subcat_description }}</textarea>
</div>
</div>
<div class="form-button">
<button type="submit" class="btn btn-box">Send Message</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>
@stop