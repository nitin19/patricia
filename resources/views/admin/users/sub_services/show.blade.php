@extends('layouts.default-header-admin')

@section('title', 'Sub Services')

@section('content')
      
<div class="yelloback">
 <div class="lock_box">
<div class="content-set">
  <div class="row">
    <div class="col-sm-12">
      <div class="media payment_box">
          @include('layouts.flash-message')
        <div class="col-sm-9">
                              <div class="media-body button_side">
                               <h3>Setting <span class="list_clr">( {{ $sub_category_count }} Sub Services)</span></h3>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="media-body button_sky bluee">
                              <a href="{{ url('/admin-subservices/create') }}/{{ $catid }}" class="btn btn-primary button_blue">Add Sub Service</a>
                             </div>
                            </div>
                            </div>
    </div>
</div>
</div>
<div class="col-sm-12">
<div class="alert alert-success alert-block success_show">
  <button type="button" class="close" data-dismiss="alert">×</button> 
  <strong>Service deleted successfully!</strong>
</div>  
 <div class="service_box">
  <table class="table table-borderless heading-name">
    <thead>
      <tr>
        <th> Sr No </th>
        <th> Image</th>
        <th> Name</th>
        <th> Category<br>Name</th>
        <th> Description </th>
        <th> Status </th>
        <th> Action </th>
      </tr>
    </thead>
    <tbody>
       <?php $count = 0; ?>
        @foreach($sub_category as $sub_categories)
        <?php
          $subcat_image = $sub_categories->subcat_image;
              if($subcat_image==''){
              $subcategoryimage = "../public/images/categoryimage/clothing_icon.png";

              }else {
              $subcategoryimage = "../public/images/categoryimage/".$subcat_image;  
              }
              $cat_id = $sub_categories->cat_id;
              $category = DB::table('category')
                         ->where('is_active', '1')
                         ->where('is_deleted', '0')
                         ->where('cat_id', $cat_id)
                         ->first(); 
          ?>
      <tr id="{{ $sub_categories->subcat_id }}">
        <td> {{ ++$count }}</td>
        <td> <img src="{{ $subcategoryimage }}"> </td>
        <td> {{ $sub_categories->subcat_name }} </td>
        <td> {{ $category->cat_name }} </td>
        <td> {{ $sub_categories->subcat_description }} </td>
        <td>@if($sub_categories->is_active==1) Active @else Inactive @endif</td>
         <td><a href="{{ url('/admin-subservices/edit') }}/{{ $sub_categories->subcat_id }}"><i class="fa fa-edit" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i class="fa fa-trash" style="font-size:16px"></i></a>
        </td>
      </tr>
      @endforeach
      </tbody>
  </table>
<script type="text/javascript">
  jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var subcat_id = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "../admin-subservices/subcategory_remove",
          type: 'POST',
          data: {'subcat_id': subcat_id,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          jQuery('.success_show').show();
          jQuery('#'+subcat_id).hide();
          }            
          });      
      }
});
</script>  
<div class="pagination-button">
<div class="pagination prev_next">
 {!! $sub_category->render() !!}
</div>
  </div>
</div>
  </div>
 </div>
 </div>
@stop