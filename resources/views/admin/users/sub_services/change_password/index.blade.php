@extends('layouts.default-header')

@section('title', 'Change Password')

@section('content')

@include('layouts.flash-message')

<div class="formsection">
 
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg">
   
                        <form class="form-horizontal registerform formarea" method="POST" id="changepassword" action="{{ url('/changepassword/update') }}">
                            <div class="message">
                                @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                                @endif
                            </div>
                        
                        {{ csrf_field() }}
                        <h2 class="text-center wow flipInY">Change Password</h2> 
                        <p class="text-center wow fadeInLeft">Change Password by entering the information below</p>
                        <div class="form-group forminput wow fadeInLeft">
                        <input id="oldpassword" type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Old Password" required autofocus>
                       <span toggle="#oldpassword" class="fa fa-fw fa-eye field-icon toggle-password">
                        </div>
                      <div class="form-group forminput wow fadeInLeft">
                        <input id="confirm_oldpassword" type="password" class="form-control" name="confirm_oldpassword" id="confirm_oldpassword" placeholder="Confirm Old Password" required>
                        <span toggle="#confirm_oldpassword" class="fa fa-fw fa-eye field-icon toggle-password">
                        </div>

                        <div class="form-group forminput wow fadeInLeft">
                        <input id="newpassword" type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password" required>
                        <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>

                     <div class="form-group forminput wow fadeInLeft">
                     <input type="password" class="form-control" name="newpassword_confirmation" id="newpassword_confirmation" placeholder="Confirm New Password" required>
                     <span toggle="#newpassword_confirmation" class="fa fa-fw fa-eye field-icon toggle-password">
                     </div>
                    <div class="buttondiv wow flipInX">
                    <input type="submit" class="btn" value="Save">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
/*$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*#$%^&]*$/.test(value)
       && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[!@*#$%^&]/.test(value)
       && /\d/.test(value)
});*/
    jQuery('#changepassword').validate({
         rules: {
            oldpassword: {  
                required: true,
               // pwcheck: true,
                minlength: 6,
                maxlength: 15  
            },
            confirm_oldpassword: {
                minlength: 6,
                maxlength: 15, 
                equalTo: "#oldpassword"
            },
            newpassword: {  
                required: true,
                //pwcheck: true,
                minlength: 6,
                maxlength: 15   
            },
            newpassword_confirmation: {
                minlength: 6,
                maxlength: 15,   
                equalTo: "#newpassword"
            }
         },
         messages: {
            oldpassword: {
                required: "This field is required",
                pwcheck: "Old password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "Old password length minimum 6 character",
                maxlength: "Old password length minimum 15 character"
            },
            confirm_oldpassword: {
                minlength: "Confirm old password length minimum 6 character",
                maxlength: "Confirm old password length minimum 15 character",
                equalTo: "Confirm old password does not match with old password"
            },
            newpassword: {
                required: "This field is required",
                pwcheck: "New password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "New password length minimum 6 character",
                maxlength: "New password length minimum 15 character"
            },
            newpassword_confirmation: {
                minlength: "Confirm new password length minimum 6 character",
                maxlength: "Confirm new password length minimum 15 character",
                equalTo: "Confirm new password does not match with new password"
            }
         },
    });
$("document").ready(function(){
    setTimeout(function(){
        $("div.alert").remove();
    }, 5000 ); // 5 secs

});
</script>
@endsection