@extends('layouts.default-header-admin')

@section('title', 'Edit Menu Detail')

@section('content')
<div class="content-wrap">
 <div class="yelloback">
<div class="container-fluid">
<div class="seredback">  
<div class="row">
<div class="col-sm-12 content_box">
    <div class="row">
        <div class="col-sm-8 col-xs-8 category_title">
            <h3 >Edit {{$menu_detail->menu_name}} meta details</h3>
        </div>
        <div class="col-sm-4 col-xs-4 createbtn">
            <a href="{{ url('/admin-menu-details') }}" class="btn btn-primary button_blueblue">Back</a>
        </div>
    </div>

@include('layouts.notify-message')
<form class="metacontactform" id="metacontactform" method="post" action="{{ url('/admin-menu-details/update') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="hidden" value="{{$menu_detail->id}}" name="id">
<div class="row">
<div class="form-group col-md-6">
    <label>Meta Title</label>
<input class="form-control" name="meta_title" placeholder="Enter Title" type="text" required value="{{$menu_detail->meta_title}}">
</div>
<div class="form-group col-md-6">
    <label>Meta Description</label>
<textarea class="form-control" name="meta_description" placeholder="Enter Description" required>{{$menu_detail->meta_description}}</textarea> 
</div>
<div class="form-group col-md-6">
    <label>Meta Keywords</label>
<input class="form-control" name="meta_keywords" placeholder="Enter keywords" type="text" required value="{{$menu_detail->meta_keywords}}">
</div>
<div class="form-group col-md-6">
    <label>Meta Url</label>
<input class="form-control" name="meta_key" placeholder="Enter url" type="text" required value="{{$menu_detail->meta_key}}">
</div>

</div>


<div class="form-button">
<button type="submit" class="btn btn-box pull-right">Update</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #1b9bd8;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>

<script>
    jQuery(document).ready(function($){
        $('#comment').summernote();
        window.setTimeout(function() {
            $(".test21").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
            }, 4000); 
         })   
</script>
@stop