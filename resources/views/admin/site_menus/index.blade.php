@extends('layouts.default-header-admin')

@section('title', 'Menu Details')

@section('content')
<div class="content-wrap">
 <div class="yelloback">
  <div class="container-fluid">
    <div class="tablediv">  
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-12">
          <div class="media payment_box">
            <!-- <div class="col-sm-10"> -->
              <div class="media-body button_side">
                <h3 style="text-align: left;">Menu Details <!-- <a href="{{url('/admin-other-services/create')}}" class="btn btn-success pull-right">Add New </a> --></h3>
              </div>
            <!-- </div> -->
          </div>
        </div>
      </div>
      <div class="row">
        @include('layouts.notify-message')
        <div class="col-sm-12 content_box">
          <div class="row">
            <div class="col-sm-8 col-xs-8 category_title">  
            </div>
          </div>
          <table id="site_menus_table" class="table stripped-table">
            <thead>
              <tr>
                <th> Sr No </th>
                <th> Menu Name </th>
                <th> Meta Title </th>
                <th>Menu Url</th>
                <th>Meta Keywords</th>
                <th> Action </th>
              </tr>
            </thead> 
            <tbody>
              @if(count($meta_details) > 0)
              <?php $count = 0; ?>
              @foreach($meta_details as $meta_detail)
              <tr id="{{ $meta_detail->id }}"  class="service_table tabledata">
                <td class="service_pos" > {{ ++$count }}</td>
                <td> {{ $meta_detail->menu_name }} </td>
                <td> {{ $meta_detail->meta_title }} </td>
                <td> {{ $meta_detail->meta_key }} </td>
                <!-- <td> {{ $meta_detail->meta_description }} </td> -->
                <td> {{ $meta_detail->meta_keywords }} </td> 
                <td><a  data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" ="" href="{{ url('/admin-menu-details/edit') }}/{{ $meta_detail->id }}"><i class="fa fa-edit" style="font-size:16px"></i></a> 
                  <a class="deletebutton" style="cursor: pointer;" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" style="font-size:16px"></i></a> 
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          <input type="hidden" value="{{$meta_details->count()}}" class="record_count">
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$('#site_menus_table').dataTable();
jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
  if(confirm("Are you sure you want to delete this?")){
    var id = jQuery(this).closest('tr').attr('id');
    jQuery.ajax({
      url: "{{url('/')}}/admin-menu-details/remove",
      type: 'POST',
      data: {'id': id,"_token": "{{ csrf_token() }}" },
      success: function(data) {
        if(data == 1)
        {
          $('#Alert_Success').show();
          jQuery('#'+id).hide();
            // window.setTimeout(function() {
            //   $('#Alert_Success').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your meta detail deleted successfully!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          }  
          else
          {
            $('#Alert_Danger').show();
            // window.setTimeout(function() {
            //   $('#Alert_Danger').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your meta  detail  cannot  deleted!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          } 
        }          
      })      
  }
  else{
    return false;
  }
  window.setTimeout(function() {
            // $(".").fadeTo(500, 0).slideUp(500, function(){
              $('test21').remove(); 
            // });
          }, 4000); 
});
</script>
<script> 
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop