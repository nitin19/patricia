@extends('layouts.default-header-admin')

@section('title', 'Edit Testimonial')

@section('content')
<div class="content-wrap">
<div class="yelloback">
<div class="container-fluid">
<div class="seredback">
<div class="row"> 
<div class="col-sm-12 content_box">
<div class="row mb-2">
    <div class="col-sm-8 col-xs-8 category_title">
        <h3> Edit For {{ $testimonial->name }} Testimonial</h3>
    </div>
    <div class="col-sm-4 col-xs-4 createbtn">
        <a href="{{ url('/admin/testimonials') }}" class="btn btn-primary button_blueblue">Back</a>
    </div>
</div>

@include('layouts.flash-message')
 <form class="" id="contactform" method="post" action="{{ url('/admin/testimonials/update') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-6">
    <label>Please Enter the name</label>
    <input class="form-control" name="name" placeholder="Name" value="{{ $testimonial->name }}" type="text" required >
</div>

<div class="form-group col-md-6">
    <label>Please Enter Your Profile</label>
    <input class="form-control" name="profile" placeholder="Profile" value="{{ $testimonial->profile }}" type="text" required >
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    @if($testimonial->profille_pic!='')
    <img src="{{ url('/public/images/testimonialimage/') }}/{{$testimonial->profille_pic}}" class="edit_service_img">
    @endif
    <input type="file" name="testimonial_img" id="testimonial_img">
</div>

</div>
<div class="row">
<div class="form-group col-md-12">
    <input type="hidden" name="testimonial_id" id="testimonial_id" value="{{ $testimonial->id }}">
  <textarea id="comment" placeholder="Description" name="description" cols="40" rows="7" class="form-control description" style="color: #323334 !important;"  maxlength="250">{{ $testimonial->description }}</textarea>
</div>
</div>  
<div class="row">
    <div class="col-sm-12 nextbtn">
        <button type="submit" class="btn button_blueblue">Update</button>
        <!-- <input class="form-control" value="Save" type="submit"> -->
    </div>   
 </div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>
<script>
    $(document).ready(function(){
        CKEDITOR.replace( 'comment' );
    })
       
</script>
@stop