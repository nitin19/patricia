@extends('layouts.default-header-admin')

@section('title', 'Testimonials')

@section('content')
 <script src="{{ url('/public/admin/js/') }}jquery-1.11.3.min.js"></script>     
<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">
<div class="form_section">
<div class="row">
<div class="col-sm-12 content_box">
<div class="alert alert-success" id="Alert_Success" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
     Testimonials deleted successfully!
</div>
  <div class="alert alert-danger" id="Alert_Danger" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
     Sorry you can't delete this Testimonials its already in use!
</div>
      <div class="media payment_box">
          @include('layouts.flash-message')
        <div class="col-sm-6">
          <div class="media-body button_side">
           <h3>Testimonials List<span class="list_clr">( {{ $testimonial_count.' Testimonials' }} )</span></h3>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="media-body button_sky bluee" style="text-align: right;">
          <a href="{{ url('/admin/testimonials/create') }}" class="btn btn-primary button_blueblue">Add Testimonials</a>
         </div>
        </div>
    </div>
    </div>
</div>
</div>
</div>
<div class="col-sm-12">
 <div class="service_box tablediv"><br>
  <table id="subcategory_table" class="table stripped-table table-borderless heading-name">
    <thead>
      <tr>
        <th class="column2"> Sr No </th>
        <th class="column4"> Image </th>
        <th class="column3"> Name</th>
        <th class="column4"> Profile </th>
        <th class="column5"> Description </th>
        <th class="column2"> Action </th>
      </tr>
    </thead>
    <tbody>
      <?php $c = '1'; ?>
        @foreach($testimonial as $testimonials)
        <?php
          $profile_pic = $testimonials->profille_pic;
              if($profile_pic==''){
              $profilepic = "../public/images/testimonialimage/clothing_icon.png";
              }else {
              $profilepic = "../public/images/testimonialimage/".$profile_pic;  
              }
          ?>
      <tr id="{{ $testimonials->id }}" class="service_table tabledata">
        <td class="column2"> {{ $c++ }}</td>
        <td class="column4"> <img src="{{ $profilepic }}"> </td>
        <td class="column3"> {{ $testimonials->name }} </td>
        <td class="column4"> {{ $testimonials->profile }} </td>
        <td class="column5"> {{ $testimonials->description }} </td>
        <td class="column2"><a href="{{ url('/admin/testimonials/edit') }}/{{ $testimonials->id }}"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" style="font-size:16px"></i></a>
        </td>

      </tr>
     
      @endforeach

      </tbody>
      
<script type="text/javascript">
  jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var testimonialid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "{{url('/')}}/admin/testimonials/testimonial_remove",
          type: 'POST',
          data: {'testimonialid': testimonialid,"_token": "{{ csrf_token() }}" },
          success: function(data) {
            if(data == 1)
            {
              $('#Alert_Success').show();
              jQuery('#'+testimonialid).hide();
              setTimeout(function() {
                $('#Alert_Success').remove(); 
                }, 4000);
            }  
            else
            {
              $('#Alert_Danger').show();
              setTimeout(function() {
                $('#Alert_Danger').remove(); 
                }, 4000);
            } 
          }          
        })      
      }
      else{
        return false;
      }
});
</script>
  </table>

</div>
  </div>

<script type="text/javascript">
jQuery(document).ready(function() {  
jQuery('#subcategory_table').DataTable( {
  "order": [[ 3, "desc" ]]
  });
});
 </script>

@stop