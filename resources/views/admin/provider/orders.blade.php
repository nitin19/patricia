@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12">
<div class="orderservice">
  <div class="row">
    <div class="col-sm-12">
      <div class="media payment_box">
       <!--  <div class="col-sm-10"> -->
        <div class="media-body button_side">
          <h3>Orders List<span class="list_clr"> ({{ $order_count }})</span>
            <a href ="{{ url('/admin-orders/excel') }}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a></h3>
        </div>
                            <!-- </div> -->
                            <!-- <div class="col-sm-2"> 
                            </div> -->
      </div>
    </div>
    
  
</div>
</div>

 <div class="service_box tablediv tablesection">
  <table id="order_table" class="table table-borderless heading-name">
    <thead>
      <tr>
        <th class="column4">Service Name</th>
        <th class="column4">Booked By</th>
        <th class="column4">Booking Date</th>
       <!--  <th>No.of Days</th> -->
         <th class="column4">Amount</th>
         <!-- <th>Rating</th> -->
         <th class="column4">Booking Status</th>
      </tr>
    </thead>
    <tbody>
     @foreach($booking as $booking_info)
     <?php
     $users = DB::table('users')
               ->where('id', $booking_info->user_id)
               ->first(); 
               $username = $users->name;
     $booking_start_time = $booking_info->booking_start_time;
     $booking_end_time = $booking_info->booking_end_time;
     $startdate = date("F j, Y", strtotime($booking_start_time));
     $enddate = date("F j, Y", strtotime($booking_end_time));
     $diff = abs(strtotime($booking_end_time) - strtotime($booking_start_time));

     $years = floor($diff / (365*60*60*24));
     $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
     $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
     /* Rating*/
     $booking_id = $booking_info->booking_id;

     $review_count = DB::table('review')
               ->where('job_id', $booking_id)
               ->count();
            
     /* Rating */
            if($review_count>0){
              $review = DB::table('review')
               ->where('job_id', $booking_id)
               ->first(); 
            $stars = $review->stars;
            }
            else {
            $stars = 0;
            }
     $users_count = DB::table('users')
               ->where('id', $booking_info->user_id)
               ->first();           
?>
      <tr class="orders_table tabledata">
       <td class="column4">
        <span class="painting">{{ ucfirst($booking_info->category) }}</span>
        </td>
        <td class="column4">{{ $users->name }}<br>
        <span class="painting">{{ $users->email }}</span></td>
         <td class="column4">{{ $startdate }}</td>
         <!--  <td>{{ $days }} days</td> -->
          <td class="column4">{{ $booking_info->booking_amount }}</td>
          <!-- <td>
           @if($review_count>0) 
           <?php $stars = $review->stars; ?>
            @if($stars>=1)
            <i class="fa fa-star yellow" aria-hidden="true"></i>@endif
             @if($stars>=2)
            <i class="fa fa-star yellow" aria-hidden="true"></i>@endif
             @if($stars>=3)
            <i class="fa fa-star yellow" aria-hidden="true"></i>@endif
             @if($stars>=4)
            <i class="fa fa-star yellow" aria-hidden="true"></i>@endif
             @if($stars==5)
            <i class="fa fa-star yellow" aria-hidden="true"></i>@endif
            @endif
            </td> -->
            <td class="column4"> 
              <form method="POST" action="{{ url('/admin-orders/approved_address') }}">  @if($booking_info->booking_verified == 1)
                <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approved" disabled=""><i class="fa fa-check approv_btn"></i></button>
                @elseif($booking_info->booking_verified == 2)
                <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Denied" disabled=""><i class="fa fa-close approv_btn"></i></button>
                @else
                 <button class="btn btn-success btn-xs btnmar" name="approve_status" type="submit" value="1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve"><i class="fa fa-check approv_btn"></i></button>
                  <button class="btn btn-danger btn-xs btnmar" name="approve_status" type="submit" value="2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deny"><i class="fa fa-close approv_btn"></i></button>
                @endif
                <input type="hidden" name="booking_id" value="{{ $booking_id }}">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
              </form>
            </td>
            </tr> 
            
      @endforeach
    </tbody>
  </table>

</div>
  </div>

 </div>
 </div>
 <script type="text/javascript">
// $(document).ready(function() {
// $('#order_table').DataTable();
// } );
$(document).ready(function() {  
$('#order_table').DataTable();
  });
 </script>
 @stop