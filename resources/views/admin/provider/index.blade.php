@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<style>
#send-email {
    margin: -28px 0 0px -20px;
}
input[type="submit"] {
    margin: -1% 0 8px 42%; 
}
td {
    width: 13%;
}

 

</style>
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box tablediv tablesection">
<div class="row marbot">
  <div class="col-sm-12">
    <div class="media payment_box">
      <!-- <div class="col-sm-10"> -->
      <div class="media-body button_side">
          <h3>Providers List<span class="list_clr"> ({{ $provider_users->count() }})</span>
           <a href ="{{ url('/admin-provider/excel') }}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a>
           <select name="selected_profiles" class="providers-profiles" id="table-filter provider-profiles-status" style="z-index: 9; position: relative;">
            <option value="allproviders">All Providers</option>
            <option value="Completed">Completed Profiles</option>
            <option value="Incompleted">Incompleted Profiles</option> 
           </select>

         
          <form method="POST" action="{{ url('/admin-provider/complete-profile-mail') }}/" id="send-email" style="z-index: 3; position: relative; display:none;">
          {!! csrf_field() !!}
          <input type="hidden" name="recipients" id="recipients" value="">
          <input type="submit" name="send_email" value="Send Email" >
          </form>

          </h3>
      </div>
      @include('layouts.notify-message')
    </div>
  </div>
</div>
  <table id="provider_table" class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th class="column2">Name</th>
        <th class="column4">Email</th>
        <!--<th class="column2">Registration Date</th>-->
        <th class="column2">Access</th>
        <th class="column2">Category</th>
        <th class="column2">Sub-Category</th>
        <th class="column2">Phone </th>
        <th class="column2">Address</th>
        <th class="column2">CEP</th>
        <!--<th class="column1">#</th>-->
        <!-- <th class="column3">Address Approve</th> -->
        <th class="column1">Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($provider_users as $provider_info)
<?php
      $name = $provider_info->name;
      $email = $provider_info->email;
      $created_at = $provider_info->created_at;
      $is_active = $provider_info->is_active;
      $latitude = $provider_info->latitude;
      $longitude = $provider_info->longitude;
      $last_login = $provider_info->last_login;
      $last_login_ip = $provider_info->last_login_ip;
      $login_count = $provider_info->login_count;
      $login_browser = $provider_info->login_browser;
      $login_device = $provider_info->login_device;
      $mainCatInfo = DB::table('main_category')->where('id', '=', $provider_info->cat_id)->pluck('name');
      $subCatInfo = DB::table('category')->where('cat_id', '=', $provider_info->sub_id)->pluck('cat_name');
      $show_status = '';
      if($is_active == '1'){ 
        $show_status = 'Active';
      } else { 
        $show_status = 'Inactive';
      }

      if(count($mainCatInfo) > 0 ) {
          $mainCat_Info = $mainCatInfo;
      }
      if(count($subCatInfo) > 0 ) { 
          $subCatInfo = $subCatInfo;
      }
      $show_tooltip_data = 'Name : ' .$name. '</br> Registration Date : '.$created_at. '</br> Last Login : '.$last_login. '</br> Login Count : '.$login_count.'</br> Profile Status : '.$show_status.'</br> Category : '.$mainCat_Info.'</br> SubCategory : '.$subCatInfo.'</br> Login IP : '.$last_login_ip.'</br> Login Browser : '.$login_browser.'</br> Login Device : '.$login_device.'</br>';
?>
<tr id="{{ $provider_info->id }}" class="puserstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td class="column2" data-toggle="tooltip" data-placement="top"  data-original-title="{{$show_tooltip_data}}" title="" data-html="true">{{ $name }}</td>
      <td class="column4" data-toggle="tooltip" data-placement="top"  data-original-title="{{$show_tooltip_data}}" title="" data-html="true">{{ $email }}</td>
      <!--<td class="column2">{{ $created_at }}</td>-->
      <td class="column2" data-toggle="tooltip" data-placement="top"  data-original-title="{{$show_tooltip_data}}" title="" data-html="true">({{ $provider_info->hd_password }})</td>
      <td class="column2">
        <?php if(count($mainCatInfo) > 0 ) { 
               foreach($mainCatInfo as $mainCat) {
                  echo $mainCat;
         } 
        } ?>
        </td>
      <td class="column2">
        <?php if(count($subCatInfo) > 0 ) { 
               foreach($subCatInfo as $subCat) {
                  echo $subCat;
         } 
        } ?>
      </td>
      <td class="column2">{{ $provider_info->phone }}</td>
      <td class="column2">{{ $provider_info->city }}</td> 
      <td class="column2">{{ $provider_info->zipcode }}</td>
      <!--<td class="column1">{{ $provider_info->id }}</td>--> 
     <!--  <td class="column3"><form method="POST" action="{{ url('/admin-users/approved_address') }}">@if($provider_info->address_verified == 1)
          <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="2" disabled="" style="margin-bottom: 3px;">Approved</button>
          @elseif($provider_info->address_verified == 2)
          <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="1" disabled="" style="margin-bottom: 3px;">Denied</button>
          @else
           <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="1" style="margin-bottom: 3px;">Approve</button>
            <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="2" style="margin-bottom: 3px;">Deny</button>
          @endif
          <input type="hidden" name="user_id" value="{{ $provider_info->id }}">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          </form>
      </td> -->
      <td class="column1 fourIcon"><a href="{{ url('/admin-users/show') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="View" title="" class="fa fa-eye" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-users/edit') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" class="fa fa-pencil" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" title="" class="fa fa-trash" style="font-size:16px"></i>
        <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form></a>

        @if($provider_info->is_active!='1')
        <a href="{{ url('/admin-provider/resendemail') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Resend account activation email" title="" class="fa fa-envelope" style="font-size:16px"></i></a>
        @endif
        <a href="https://maps.google.com/?q={{$latitude}},{{$longitude}}" target="_blank"><i data-toggle="tooltip" data-placement="top"  data-original-title="location" title="" class="fa fa-globe" style="font-size:16px"></i></a>
      </td>
</tr>
@endforeach
    </tbody>
  </table>
  <table id="profile_provider_table" style="display:none;" class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th >Name</th>
        <th >Email</th>
        <th >Registration Date</th>
        <th >Access</th>
        <th >Phone </th>
        <th >Address</th>
        <th >CEP</th>
        <th >#</th>
        <!-- <th class="column3">Address Approve</th> -->
        <!--th >Action</th-->
      </tr>
    </thead>
  </table>

</div>
  </div>



<script type="text/javascript">
  // jQuery('.success_show').hide();
  jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure?")){
      var userid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "/admin/usersdestroy",
          type: 'POST',
          data: {'id': userid,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          // jQuery('.success_show').show();
          // jQuery('.success_show').html(response);
          $.notify({
              message: response,
              },{
              type: 'success',
              offset: 
              {
                x: 10,
                y: 130
              },
              animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
              },
            });
          jQuery('#'+userid).remove();
           // setTimeout(function() {
           //  $('.success_show').remove(); 
           //  }, 2000);
          }            
          });      
      }
      else{
        return false;
      }
});
</script>
<script type="text/javascript">
$(document).ready(function() {
 var table = $('#provider_table').DataTable();
  jQuery('.providers-profiles').on('change', function() {
      var p_status = this.value;
      
      if(p_status == "Incompleted") {
        $('#send-email').show();
      }else {
        $('#send-email').hide();
      }
      //console.log(p_status);
     //$('#provider_table').hide();
     var newtable = $('#profile_provider_table').DataTable();
     table.destroy();
     newtable.destroy();

    
    /*$('#profile_provider_table').DataTable( {
        "ajax": {
            "url": "/admin-provider/incomplete-profile",

        }
        
    } );*/

     jQuery.ajax({
          url: "/admin-provider/incomplete-profile",
          type: 'GET',
          data: {'p_status': p_status,"_token": "{{ csrf_token() }}" },
          success: function(response) {
            $('#provider_table').hide();
            $('#profile_provider_table').show();
            //var data = JSON.parse(response.data);
            console.log(response);
            $('#profile_provider_table').DataTable( {
                data: response,
                columns: [
                            { 'data': 'name' },
                            { 'data': 'email' },
                            { 'data': 'created_at' },
                            { 'data':'hd_password'},
                            { 'data': 'phone' },
                            { 'data': 'city' },
                            { 'data': 'zipcode' },
                            { 'data': 'id' },
                           
                        ]
               
            } );
          }
        });
      /*if(p_status == "Incompleted") {
        window.location.href = "/admin-provider/incomplete-profile";
      } else if(p_status == "Completed") {
        window.location.href = "/admin-provider/complete-profile";
      } else{
        window.location.href = "/admin-provider/";
      }*/
      
    });
    //$('#provider_table').DataTable();
} );
</script>
@stop
