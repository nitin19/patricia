@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12 all-providers">
 <div class="service_box tablediv tablesection">
<div class="row marbot">
  <div class="col-sm-12">
    <div class="media payment_box">
      <!-- <div class="col-sm-10"> -->
      <div class="media-body button_side">
          <h3>Providers List<span class="list_clr"> ({{ $provider_users->count() }})</span>
           <a href ="{{ url('/admin-provider/excel') }}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a>
           <select name="selected_profiles" class="providers-profiles" id="table-filter provider-profiles-status" style="z-index: 9; position: relative;">
            <option value="">Please Select</option>
            <option value="Completed">Completed Profiles</option>
            <option value="Incompleted">Incompleted Profils</option>
            <option value="Providers">All Providers</option>
           </select>

          </h3>
      </div>
      @include('layouts.notify-message')
    </div>
  </div>
</div>
  <table id="provider_table" class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th class="column2">Name</th>
        <th class="column3">Email</th>
        <th class="column2">Registration Date</th>
        <th class="column2">Access</th>
        <th class="column2">Category</th>
        <th class="column2">Sub-Category</th>
        <th class="column2">Phone </th>
        <th class="column1">Address</th>
        <th class="column2">CEP</th>
        <th class="column1">#</th>
        <!-- <th class="column3">Address Approve</th> -->
        <th class="column1">Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($provider_users as $provider_info)
<?php
      $name = $provider_info->name;
      $email = $provider_info->email;
      $created_at = $provider_info->created_at;
      $is_active = $provider_info->is_active;
      $mainCatInfo = DB::table('main_category')->where('id', '=', $provider_info->cat_id)->pluck('name');
      $subCatInfo = DB::table('category')->where('cat_id', '=', $provider_info->sub_id)->pluck('cat_name');
?>
<tr id="{{ $provider_info->id }}" class="userstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td class="column2">{{ $name }}</td>
      <td class="column3">{{ $email }}</td>
      <td class="column2">{{ $created_at }}</td>
      <td class="column2">({{ $provider_info->hd_password }})</td>
      <td class="column2">
        <?php if(count($mainCatInfo) > 0 ) { 
               foreach($mainCatInfo as $mainCat) {
                  echo $mainCat;
         } 
        } ?>
        </td>
      <td class="column2">
        <?php if(count($subCatInfo) > 0 ) { 
               foreach($subCatInfo as $subCat) {
                  echo $subCat;
         } 
        } ?>
      </td>
      <td class="column2">{{ $provider_info->phone }}</td>
      <td class="column1">{{ $provider_info->city }}</td> 
      <td class="column2">{{ $provider_info->zipcode }}</td>
      <td class="column1">{{ $provider_info->id }}</td> 
     <!--  <td class="column3"><form method="POST" action="{{ url('/admin-users/approved_address') }}">@if($provider_info->address_verified == 1)
          <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="2" disabled="" style="margin-bottom: 3px;">Approved</button>
          @elseif($provider_info->address_verified == 2)
          <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="1" disabled="" style="margin-bottom: 3px;">Denied</button>
          @else
           <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="1" style="margin-bottom: 3px;">Approve</button>
            <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="2" style="margin-bottom: 3px;">Deny</button>
          @endif
          <input type="hidden" name="user_id" value="{{ $provider_info->id }}">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          </form>
      </td> -->
      <td class="column1 fourIcon"><a href="{{ url('/admin-users/show') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="View" title="" class="fa fa-eye" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-users/edit') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" class="fa fa-pencil" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" title="" class="fa fa-trash" style="font-size:16px"></i>
        <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form></a>

        @if($provider_info->is_active!='1')
        <a href="{{ url('/admin-provider/resendemail') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Resend account activation email" title="" class="fa fa-envelope" style="font-size:16px"></i></a>
        @endif
        
      </td>
</tr>
@endforeach
    </tbody>
  </table>
  

</div>
  </div>

<div class="incomplete-providers"></div>



<script type="text/javascript">
  // jQuery('.success_show').hide();
  jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure?")){
      var userid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "/admin/usersdestroy",
          type: 'POST',
          data: {'id': userid,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          // jQuery('.success_show').show();
          // jQuery('.success_show').html(response);
          $.notify({
              message: response,
              },{
              type: 'success',
              offset: 
              {
                x: 10,
                y: 130
              },
              animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
              },
            });
          jQuery('#'+userid).remove();
           // setTimeout(function() {
           //  $('.success_show').remove(); 
           //  }, 2000);
          }            
          });      
      }
      else{
        return false;
      }

      
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    jQuery('.providers-profiles').on('change', function() {
      var p_status = this.value;
      if(p_status == "Incompleted") {
        window.location.href = "/admin-provider/incomplete-profile";
      } else if(p_status == "Completed") {
        window.location.href = "/admin-provider/complete-profile";
      } else{
        window.location.href = "/admin-provider";
      }
 
    });
    $('#provider_table').DataTable();
  });
</script>
@stop
