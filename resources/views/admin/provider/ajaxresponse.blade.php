<thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th class="column2">Name</th>
        <th class="column3">Email</th>
        <th class="column2">Registration Date</th>
        <th class="column2">Access</th>
        <th class="column2">Category</th>
        <th class="column2">Sub-Category</th>
        <th class="column2">Phone </th>
        <th class="column1">Address</th>
        <th class="column2">CEP</th>
        <th class="column1">#</th>
        <!-- <th class="column3">Address Approve</th> -->
        <th class="column1">Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($provider_users as $provider_info)
<?php
      $name = $provider_info->name;
      $email = $provider_info->email;
      $created_at = $provider_info->created_at;
      $is_active = $provider_info->is_active;
      $mainCatInfo = DB::table('main_category')->where('id', '=', $provider_info->cat_id)->pluck('name');
      $subCatInfo = DB::table('category')->where('cat_id', '=', $provider_info->sub_id)->pluck('cat_name');
?>
<tr id="{{ $provider_info->id }}" class="userstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td class="column2">{{ $name }}</td>
      <td class="column3">{{ $email }}</td>
      <td class="column2">{{ $created_at }}</td>
      <td class="column2">({{ $provider_info->hd_password }})</td>
      <td class="column2">
        <?php if(count($mainCatInfo) > 0 ) { 
               foreach($mainCatInfo as $mainCat) {
                  echo $mainCat;
         } 
        } ?>
        </td>
      <td class="column2">
        <?php if(count($subCatInfo) > 0 ) { 
               foreach($subCatInfo as $subCat) {
                  echo $subCat;
         } 
        } ?>
      </td>
      <td class="column2">{{ $provider_info->phone }}</td>
      <td class="column1">{{ $provider_info->city }}</td> 
      <td class="column2">{{ $provider_info->zipcode }}</td>
      <td class="column1">{{ $provider_info->id }}</td> 
     <!--  <td class="column3"><form method="POST" action="{{ url('/admin-users/approved_address') }}">@if($provider_info->address_verified == 1)
          <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="2" disabled="" style="margin-bottom: 3px;">Approved</button>
          @elseif($provider_info->address_verified == 2)
          <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="1" disabled="" style="margin-bottom: 3px;">Denied</button>
          @else
           <button class="btn btn-success btn-xs" name="approve_status" type="submit" value="1" style="margin-bottom: 3px;">Approve</button>
            <button class="btn btn-danger btn-xs" name="approve_status" type="submit" value="2" style="margin-bottom: 3px;">Deny</button>
          @endif
          <input type="hidden" name="user_id" value="{{ $provider_info->id }}">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          </form>
      </td> -->
      <td class="column1 fourIcon"><a href="{{ url('/admin-users/show') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="View" title="" class="fa fa-eye" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-users/edit') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" class="fa fa-pencil" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" title="" class="fa fa-trash" style="font-size:16px"></i>
        <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form></a>

        @if($provider_info->is_active!='1')
        <a href="{{ url('/admin-provider/resendemail') }}/{{ $provider_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Resend account activation email" title="" class="fa fa-envelope" style="font-size:16px"></i></a>
        @endif
        
      </td>
</tr>
@endforeach
    </tbody>