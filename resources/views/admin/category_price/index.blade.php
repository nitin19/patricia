@extends('layouts.default-header-admin')
@section('title', 'subcategory_price')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css" />
<!--main container start-->
<div class="yelloback" id="subcatDiv">
<div class="lock_box">
<div class="contact_box">
<div class="form_section">
<div class="col-sm-12 content_box">  
<div class="row" style="margin-bottom: 15px;"> 
  <div class="col-sm-8 col-xs-8 category_title">
    <div class="media-body button_side">
      <h3>Price Query {{ $mainCategoryDetail->name }} / {{ $categoryDetail->cat_name }}</h3>
    </div>  
  </div> 
  <div class="col-sm-4 col-xs-4 createbtn">
    <div class="media-body button_sky bluee">
      <a href="{{url('/admin-services')}}" class="btn button_blueblue">Back</a>
    </div>
  </div>
</div>

<div class="">
  <div style="clear: both;"> </div>
  @include('layouts.flash-message')
  <div class="alert alert-success" id="Alert_Success" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
       Data deleted successfully!
  </div>

  <div style="clear: both;"> </div>
  <div class="alert alert-danger" id="Alert_Danger" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
       Sorry you can't delete this query as its already in use!
  </div>     
  <div class="row">
    <!-- <div class="col-sm-12 blue_bg" > -->
      <form method="post" name="savenewquery" action="{{url('/admin/subcategory_price/save')}}">
          {{csrf_field()}}
          <!-- <div class="col-sm-6"></div> -->
        <div class="col-sm-10" id="addvalue">
         	<div class="form-group add_">
      		  <input class="form-control" placeholder="Enter Your Query" type="text" required="" name="queries" id="queries" value="">
                <input type="hidden" name="cat_id" value="{{$categoryDetail->main_cat_id}}">
                <input type="hidden" name="sub_id" value="{{$categoryDetail->cat_id}}">
                <input type="hidden" name="query_id" id="query_id" value="">
      	  </div>
        </div>
        <div class="col-sm-2">
          <div class="media-body button_sky bluee" style="text-align: right;">
            <!-- <a id="additembtn" class="btn btn-primary button_blue">Add More</a> -->
            <button id="save_btn" class="btn button_blueblue" type="submit">Save</button>
          </div>
        </div> 
      </form> 
    <!-- </div> -->
  </div>
</div>
    	
	<div class="row">
    		<div class="col-sm-12"><br>
	<table id="example" class="table table-striped table-bordered table_style" style="width:100%">
        <thead>
            <tr>
                <th>Query</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($price_queries) > 0)
                @foreach($price_queries as $price_query)
           <tr id="qry_{{$price_query->id}}">
                <td><div class="radio">
                     <label><input type="radio" name="optradio" style="vertical-align: sub;" value="{{$price_query->id}}" @if($price_query->priority=='1') {{'checked'}} @endif> {{$price_query->queries}}</label>
                     </div> </td>
                      <td class="action_style">
         				 <a style="cursor: pointer;" data-id="{{$price_query->id}}" data-value="{{$price_query->queries}}" class="edit"><i data-toggle="tooltip" title="Edit" class="fa fa-edit"></i></a> | 
         				 <a style="cursor: pointer;" id="removetr_btn" onclick="ConfirmDelete(<?php echo $price_query->id;?>)"><i data-toggle="tooltip" title="Delete"class="fa fa-trash"></i></a>
         				 
        		</td>
            </tr>
            @endforeach
            @endif
           </tbody>
   			 </table>
		 </div>
		</div>
  </div>
</div>
		</div>
<!--main container close-->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script>
  jQuery(document).ready(function($) {
    $('#example').dataTable( {
     "ordering": false,	
     "bLengthChange": false,
 	 "info": false,
     "searching": false,
     "bPaginate": false
	});
} );
    </script>
   
   <script>
    $(document).ready(function() {
    $('input[name="optradio"]').on('change',function(){
            var price_queryid = $(this).val();
        $.ajax({
          type:"post",
          url:"{{url('/admin/subcategory_price/changepriority')}}",
          data:{'price_queryid': price_queryid},
          success:function(data){
              //$('#subcat_queries').html(data);
          }
        })
      });

        $('.edit').on('click',function(){
            var price_queryid = $(this).attr('data-id');
            var price_queryvalue = $(this).attr('data-value');
            $('#query_id').val(price_queryid);
            $('#queries').val(price_queryvalue);
        });

      });

function ConfirmDelete(id)
{
  var price_queryid = id;
  var x = confirm("Are you sure you want to delete?");
  if (x) {
    $.ajax({
          type:"post",
          url:"{{url('/admin/subcategory_price/deletesubcat')}}",
          data:{'price_queryid': price_queryid},
          success:function(data){
              if(data==1) {
                $('#qry_'+id).remove();
                $('#Alert_Success').show();
                $('#subcatDiv').animate({
                  scrollTop: 0
                }, 'slow');
                window.setTimeout(function () {
                        $("#Alert_Success").fadeTo(500, 0).slideUp(500, function () {
                            $(this).hide();
                        });
                    }, 3000);

              } else {

              $('#Alert_Danger').show();
              $('#subcatDiv').animate({
                  scrollTop: 0
                }, 'slow');
               window.setTimeout(function () {
                  $("#Alert_Danger").fadeTo(500, 0).slideUp(500, function () {
                    $(this).hide();
                });
            }, 3000);

            }
          }
        })
  } else {
    return false;
  }
}

    </script>
    <style>
    	.contact_box{padding: 10px;}
    	.nav>li>a{padding: 10px 11px !important;}
    	.table_style tr th, .table_style tr td{padding:20px;vertical-align: middle;}
    	.blue_bg{background: #F4F5F7;padding: 20px;}
    	.action_style{color: #42a5f5;}
    	.active{border-left: none;}
    	.button_blue {width: auto;margin: 15px 0px;}
    	.btns_box{margin: 0 auto;float: none;}
      #Alert_Success { opacity: 1 !important; } 
      #Alert_Danger { opacity: 1 !important; } 
      .form_section {
    background-color: #fff;
    padding: 30px;  
    border: 1px solid #ddd;
    border-radius: 5px;
    float:left;
    
} 
    </style>
@stop
