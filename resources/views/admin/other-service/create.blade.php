@extends('layouts.default-header-admin')

@section('title', 'Add Service')

@section('content')
<div class="content-wrap">
 <div class="yelloback">
<div class="container-fluid">
<div class="seredback">  
<div class="row">
<div class="col-sm-12 content_box">
    <div class="row">
        <div class="col-sm-8 col-xs-8 category_title">
            <h3 >Add Other services</h3>
        </div>
        <div class="col-sm-4 col-xs-4 createbtn">
            <a href="{{ url('/admin-other-services') }}" class="btn btn-primary button_blueblue">Back</a>
        </div>
    </div>

@include('layouts.flash-message')
 <form class="contactform" id="contactform" method="post" action="{{ url('/admin-other-services') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
<div class="form-group col-md-6">
<input class="form-control" name="title" placeholder="Enter Title" type="text" required >
</div>

</div>
<div class="row">


</div>
<div class="row">
<div class="form-group col-md-12">
<textarea id="comment" placeholder="Description" name="service_description" cols="40" rows="7" class="form-control service_description" style="color: #323334 !important;"  maxlength="250"></textarea>
<input type="hidden" value="" id="uploaded_file_value">
<input type="file" class="imageshow" name="service_img" id="service_img" style="display:none;">
</div>
</div>
<div class="form-button">
<button type="submit" class="btn btn-box pull-right">Save</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #3c8dbc;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 14px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 4px 17px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>

<script>
    $(document).ready(function(){
        $('#comment').summernote();
    //     var base_url = '<?php echo url("/");?>';
    //     var camera_icon =  base_url+'/public/images/camera.png';
    //     editor = CKEDITOR.replace('comment'); 
    //     // // config.removePlugins = 'Image';
    //     // CKEDITOR.replace( 'comment', {
    //     //     extraPlugins: 'easyimage',
    //     //     removePlugins: 'Image',
    //     //     cloudServices_tokenUrl: 'https://example.com/cs-token-endpoint',
    //     //     cloudServices_uploadUrl: 'https://your-organization-id.cke-cs.com/easyimage/upload/'
    //     // });
    //         editor.addCommand("imageupload", {
    //             exec: function(edt) {
    //                 $("#service_img").click();
    //             }
    //         });
    //         editor.ui.addButton('SuperButton', {
    //             label: "Click me",
    //             command: 'imageupload',
    //             toolbar: 'insert',
    //             icon: camera_icon,
    //             // icon: 'https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwje_OLOtY3hAhWIT30KHbikAOkQjRx6BAgBEAU&url=https%3A%2F%2Finstagramlogins.net%2Ftag%2Fupload-to-instagram-from-mac%2F&psig=AOvVaw0S-9F8psOhJlaQron70k-B&ust=1553057781827755',
    //         });
      
        
    //     CKEDITOR.editorConfig = function( config ) {
    //         config.extraPlugins = 'imageupload';
    //     };
      
        
    //     function readURL(input) {
    //             if (input.files && input.files[0]) {
    //                 var reader = new FileReader();
    //                 var filename = input.files[0]['name'].split('.').pop().toLowerCase();
    //                 if ($.inArray(filename, ['gif','png','jpg','jpeg']) == -1){
    //                     reader.onload = function (e) { 
    //                         $('#uploaded_file_value').val(e.target.result);
    //                         CKEDITOR.instances.comment.insertHtml('<div class="c-doc"><a href="'+e.target.result+'">Doc FIle</a></div>');
    //                     }
    //                 }else{
    //                     reader.onload = function (e) {
    //                     $('#uploaded_file_value').val(e.target.result);
    //                     CKEDITOR.instances.comment.insertHtml('<div class="c-img"><img src="'+e.target.result+'"></div>');
    //                         // alert($('#service_img').val());
    //                     }
    //                 }
    //                 reader.readAsDataURL(input.files[0]);
    //             }
    //         }
    //     $("#service_img").click(function(e){
    //         // e.stopPropagation();
    //         readURL(this); 
    //         // CKEDITOR.instances.comment.insertHtml('<img src="#"></img>');
    //     });
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
    }, 4000);
       
  
})
    
       
</script>
@stop