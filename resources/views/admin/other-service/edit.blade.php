@extends('layouts.default-header-admin')

@section('title', 'Edit Service32324')

@section('content')
<div class="content-wrap">
 <div class="yelloback">
<div class="container-fluid">
<div class="seredback">  
<div class="row">
<div class="col-sm-12 content_box">
    <div class="row">
        <div class="col-sm-8 col-xs-8 category_title">
            <h3 >Add Other services</h3>
        </div>
        <div class="col-sm-4 col-xs-4 createbtn">
            <a href="{{ url('/admin-other-services') }}" class="btn btn-primary button_blueblue">Back</a>
        </div>
    </div>

@include('layouts.notify-message')
<form class="contactform" id="contactform" method="post" action="{{ url('/admin-other-services/update') }}" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="hidden" value="{{$service->id}}" name="other_service_id">
<div class="row">
<div class="form-group col-md-6">
<input class="form-control" name="title" placeholder="Enter Title" type="text" required value="{{$service->title}}">
</div>

</div>
<div class="row">


</div>
<div class="row">
<div class="form-group col-md-12">
<textarea id="comment" placeholder="Description" name="service_description">{{$service->description}}</textarea>
<input type="hidden" value="" id="uploaded_file_value">
<input type="file" class="imageshow" name="service_img" id="service_img" style="display:none;">
</div>
</div>
<div class="form-button">
<button type="submit" class="btn btn-box pull-right">Update</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #1b9bd8;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
</style>

<script>
    jQuery(document).ready(function($){
        $('#comment').summernote();
        window.setTimeout(function() {
            $(".test21").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
            }, 4000); 
         })   
</script>
@stop