@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')

<div class="content-wrap">
  <div class="yelloback">

    <div class="main main-content">
      <div class="container-fluid">
        <div class="business_busness">
          <div class="col-md-12">
            <form class="form-horizontal registerform formarea" method="POST" id="business" action="{{url('/admin/business')}}"  enctype="multipart/form-data">
              {{ csrf_field() }}
              <!-- <input name="_method" type="hidden" value="PATCH"> -->
              <div class="row">
                <div class="col-lg-12">
                  <div class="business_settingtitle">
                    <h1>Business Settings</h1>
                  </div>
                </div>
              </div>
              <div align="center">
                @if (session('message'))
                <div class="alert alert-success">
                  {{ session('message') }}
                </div>
                @endif
              </div>
              <form>
                <div class="row">
                  <div class="col-lg-4 browseinputdiv">
                    <div class="settingdata">
                      <label>Website Header Logo</label>
                      <!--<div class="col-sm-6 nopadding" id="ftrd_browse_img">-->
                      <!--<img src="http://navizinhanca.com/public/admin/images/yellow_logo.png" i="blah" class="img-responsive" style="width: 100px; height: 100px;">
                      <input id="header_logo" type="file" name="header_logo" accept="image/*">-->

                      <div class="upload-btn-wrapperrr">
                        <button class="btnbusness"><img src="{{url('/public/images/admin-assets')}}/{{$business_setting->header_logo}}" class="img-responsive bunessing header-bunessing">
                          <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                          <input type="hidden" value="{{$business_setting->header_logo}}" name="old_header_image">
                        </button>
                        <input type="file" name="header_logo" class="header_logo"/>
                      </div>
                      <!--</div>-->
                    </div>
                  </div>
                  <div class="col-lg-4 browseinputdiv">
                    <div class="settingdata">
                      <label>Website Footer Logo</label>
                      <!-- <div class="col-sm-6 nopadding" id="ftrd_browse_img">-->
                      <!--<img src="http://navizinhanca.com/public/admin/images/yellow_logo.png" i="blah" class="img-responsive" style="width: 100px; height: 100px;">
                      <input id="footer_logo" type="file" name="footer_logo" accept="image/*">-->
                      <div class="upload-btn-wrapperrr">
                        <button class="btnbusness"><img src="{{url('/public/images/admin-assets')}}/{{$business_setting->footer_logo}}" class="img-responsive bunessing footer-bunessing">
                          <p><i class="fa fa-upload" aria-hidden="true"></i></p>
                        </button>
                        <input type="file" name="footer_logo"  class="footer_logo"/>
                        <input type="hidden" value="{{$business_setting->footer_logo}}" name="old_footer_image">
                      </div>
                      <!--</div>-->
                    </div>
                  </div>
                  <div class="col-lg-4 browseinputdiv">
                    <div class="form-group settingdata">
                      <label>Admin Profile</label>

                      <div class="upload-btn-wrapperrr" id="ftrd_browse_img">
                        <button class="btnbusness"><img src="{{url('/public/images/admin-assets')}}/{{$business_setting->admin_profile_picture}}" id="blah" class="img-responsive admin-img bunessing" >
                          <p><i class="fa fa-upload" aria-hidden="true"></i></p> 
                        </button>
                        <input id="header_logo" type="file" name="admin_profile_picture" accept="image/*" class="admin-profile">
                        <input type="hidden" value="{{$business_setting->admin_profile_picture}}" name="old_admin_image">
                      </div>
                      <!-- <div class="col-md-3 nopadding" id="ftrd_browse_img">
                        <img src="{{url('/public/images/admin-assets')}}/{{$business_setting->admin_profile_picture}}" id="blah" class="img-responsive admin-img" style="width: 100px; height: 100px;">
                        <input id="header_logo" type="file" name="admin_profile_picture" accept="image/*" class="admin-profile">
                        <input type="hidden" value="{{$business_setting->admin_profile_picture}}" name="old_admin_image">
                      </div> -->
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label> Business Name*</label>
                      <input id="business_name" type="text" class="form-control valid capttxt" name="name" value="{{$business_setting->name}}" required="" Placeholder="Enter Business Name " required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>E-Mail Address*</label>
                      <input id="business_email" type="business_email" class="form-control" name="email" value="{{$business_setting->email}}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" Placeholder="Enter Business Email" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata">
                      <label> Phone*</label>
                      <input id="phone" type="number" class="form-control" name="phone" value="{{$business_setting->phone}}"   Placeholder="Enter your Phone Number " maxlength="10" required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>Business City*</label>
                      <input id="business_city" type="business_city" class="form-control capttxt" name="city" value="{{$business_setting->city}}"  Placeholder="Enter city " required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label> Business State*</label>
                      <input id="business_state" type="text" class="form-control capttxt" name="state" value="{{$business_setting->state}}" required="" Placeholder="Enter state " required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>Business Country*</label>
                      <input id="business_country" type="business_country" class="form-control capttxt" name="country" value="{{$business_setting->country}}" Placeholder="Enter country " required>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label> Business Address*</label>
                      <input iyellobackd="business_address" type="text" class="form-control capttxt" name="address" value="{{$business_setting->address}}" required="" Placeholder="Enter Address " required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>Business Zipcode*</label>
                      <input id="business_zipcode" type="business_zipcode" class="form-control" name="zipcode" value="{{$business_setting->zipcode}}" Placeholder="Enter Zipcode " required>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label> Twitter Link*</label>
                      <input id="twitter_link" type="text" class="form-control" name="twitter_link" value="{{$business_setting->twitter_link}}" required="" placeholder ="https://twitter.com" required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>Facebook Link*</label>
                      <input id="facebook_link" type="facebook_link" class="form-control" name="facebook_link" value="{{$business_setting->facebook_link}}" placeholder ="https://www.facebook.com/" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label> Google Link*</label>
                      <input id="google_link" type="text" class="form-control" name="google_link" value="{{$business_setting->google_link}}" required="" placeholder="https://google.com" required>
                    </div>
                  </div>
                  <div class="col-md-6 inputdiv">
                    <div class="settingdata busnessmar">
                      <label>Linkedin*</label>
                      <input id="linkedin" type="text" class="form-control" name="linkedin" value="{{$business_setting->linkedin}}" placeholder="https://linkedin.com" required>
                    </div>
                    <div class="btnbussness">
                      <input type="submit" class="btnbus" id="submit" value="Save" onClick="fieldcheck();">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
   
  /* header image preview before upload*/
  function readHeaderURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.header-bunessing').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".header_logo").change(function() {
     // alert('sdfsf');
    readHeaderURL(this);
  });
  /* footer image preview before upload*/
  function readFooterURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.footer-bunessing').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".footer_logo").change(function() {
    readFooterURL(this);
  });
/* admin image preview before upload*/
  function readAdminURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.admin-img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(".admin-profile").change(function() {
    readAdminURL(this);
  });
  // function fieldcheck(e)
  // {
  //   e.preventDefault();
  //   var $email = $('form input[name="email'); //change form to id or containment selector
  //   var re = /^(([^<>()[]\.,;:s@"]+(.[^<>()[]\.,;:s@"]+)*)|(
  //   ".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA
  //   -Z-0-9]+.)+[a-zA-Z]{2,}))$/igm;
  //   if ($email.val() == '' || !re.test($email.val()))
  //   {
  //      alert('Please enter a valid email address.');
  //      return false;
  //   }
    var validator = $("#business").validate({
      rules: {
        business_name: {
          required: true,
        },
        business_email: {
          required: true,
          email: true
        },
        phone: {
          required: true,
        },
        business_city:
        {
          required: true,
        },
        business_state: {
          required: true,
        },
        business_country: {
          required: true
        },
        business_address: {
          required: true
        },
        business_zipcode: {
          required: true
        },
        twitter_link: {
          required: true
        },
        facebook_link: {
          required: true
        },
        google_link: {
          required: true
        },
        linkedin: {
          required: true
        }
      },
      messages: {
        business_name: {
          required: "This field is required"
        },
        business_email: {
          required: "This field is required",
          email: "Enter Valid Email"
        },
        phone: {
          required: "This field is required"
        },
        business_city:
        {
          required: "This field is required"
        },
        business_state: {
          required: "This field is required"
        },
        business_country: {
          required: "This field is required"
        },
        business_address: {
          required: "This field is required"
        },
        business_zipcode: {
          required: "This field is required"
        },
        twitter_link: {
          required: "This field is required"
        },
        facebook_link: {
          required: "This field is required"
        },
        google_link: {
          required:"This field is required"
        },
        linkedin: {
          required: "This field is required"
        }

      },
    });
  // }


</script>
@stop
