@extends('layouts.default-header-admin')

@section('title', 'Profile')

@section('content')



<?php
function humanTiming ($time) {
  $time = time() - $time; 
  $time = ($time<1)? 1 : $time;
  $tokens = array (
    31536000 => 'year',
    2592000 => 'month',
    604800 => 'week',
    86400 => 'day',
    3600 => 'hour',
    60 => 'minute',
    1 => 'second'
    );
  foreach ($tokens as $unit => $text) {
    if ($time < $unit) continue;
    $numberOfUnits = floor($time / $unit);
    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
  }
}
?>
      
<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">

      <div class="row marbot">
        <div class="col-sm-12 content_box">
          <div class="col-sm-6">
             <h3>Mailbox<span class="list_clr"> {{ $conatct_admin_count }} New Message</span></h3>
          </div>
          <div class="col-sm-6 text-right">
            <div class="top_button">
              <Span class="isticontext">1520</Span><a href="#">❮</a>
              <a href="#">❯</a>
            </div>
          </div>
        </div>
      </div>
<div class="clear"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>                          
<script type="text/javascript">
jQuery(document).ready(function(){
  
  $('#customCheck').click(function () {
    if ($(this).prop('checked')) {
        $('.customCheck1').prop('checked', true);
         var valor = [];
          $('.customCheck1').each(function () {
              if (this.checked)
                  valor.push($(this).attr('data-value'));
          });
          document.getElementById("recipients").value = valor.toString();
    } else {
        $('.customCheck1').prop('checked', false);
        document.getElementById("recipients").value = '';
    }
});
$('#customCheck').trigger('change');   
});
</script>                             
                        
                          
                          </div>

  
 
<div class="col-sm-12">
  <div class="alert alert-success" style="float: left;" id="positivemsg" style="">
  <strong>Success!</strong> Message delete successfully.
</div>  
 <div class="service_box">
  @include('layouts.flash-message')
  <table class="table table-borderless mail-table">
    <thead>
      <tr>
        <th class="column5"> <input type="checkbox" class="custom-control-input" id="customCheck" name="example1"> Mail Sent By</th>
        <th class="column7"> Message</th>
        <th class="column8">
<div class="sideicon">
  
  <!-- <a href="#"><i class="fa fa-square-o" aria-hidden="true"></i></a> -->
  <a href="#" class="delete_icon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
  <a data-toggle="modal" data-target="#exampleModal" class="reply_icon"><i class="fa fa-reply" aria-hidden="true"></i></a>
<!-- <a href="#"><i class="fa fa-share" aria-hidden="true"></i></a> -->
  <a href="{{ url('/admin-mails') }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
 
</div>
</th>
      </tr>
    </thead>
    <tbody>
      <?php $count = 0; ?>
      @foreach($conatct_admin as $conatct_admininfo)
      <?php
      $name = $conatct_admininfo->fullname;
      $email = $conatct_admininfo->email;
      $phone_number = $conatct_admininfo->phone_number;
      $message = $conatct_admininfo->message;
      $created_date = $conatct_admininfo->created_date;
      $time = strtotime($created_date);     
     
        ?>
      <tr class="">
        <td class="column5"><input type="checkbox" class="custom-control-input customCheck1"  name="example1" id="{{ $conatct_admininfo->conatct_id }}" value="{{ $conatct_admininfo->conatct_id }}" data-value="{{$email}}"> {{ $name }}<br>
           <span class="painting">{{ $email }}</span>
        </td>
        <td class="column12">{{ $message }}
        </td>
        <td class="column3">{{ humanTiming($time).' ago' }}</td>
      </tr>
      <script type="text/javascript">
        jQuery('#positivemsg').hide();
        jQuery('.delete_icon').click(function(){
        if ($("<?php echo '#'.$conatct_admininfo->conatct_id; ?>").prop('checked')) {
        var contact_id = $("<?php echo '#'.$conatct_admininfo->conatct_id; ?>").val();
        $.ajax({
            url: "{{ url('/admin-mails/destroy/')}}",
            type: 'POST',
            data: {'contact_id': contact_id, "_token": "{{ csrf_token() }}"},
            success: function(response) {
                jQuery('#positivemsg').show();
                location.reload();
            }            
        });
        }
        });
        jQuery('.reply_icon').click(function(){
        if ($("<?php echo '#'.$conatct_admininfo->conatct_id; ?>").prop('checked')) {
        var contact_id = $("<?php echo '#'.$conatct_admininfo->conatct_id; ?>").val();
        // $.ajax({
        //     url: "{{ url('/admin-mails/reply/')}}",
        //     type: 'POST',
        //     data: {'contact_id': contact_id, "_token": "{{ csrf_token() }}"},
        //     success: function(response) {
        //         jQuery('.alert-success').show();
        //     }            
        // });
        }
        });
      </script>
      @endforeach

    </tbody>
  </table>
  <div class="pagination-button">

<div class="pagination prev_next">
 {!! $conatct_admin->render() !!}
</div>
  </div>
</div>
  </div>




<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{url('/sendadminmsg')}}" name="msgform">
      <div class="modal-body">
          {{csrf_field()}}
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipients" name="recipients" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" id="subject" name="subject" >
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message" name="message" required></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send message</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
        $(document).ready(function(){
          var listcnptval = new Array(); 
           $('input[type="checkbox"].customCheck1').click(function(){
                    if($(this).prop("checked") == true){
                     var sltdChk = $(this).attr('data-value');
                   listcnptval.push(sltdChk); 
                    } else if($(this).prop("checked") == false){
                      var sltdChk = $(this).attr('data-value');
                        //listcnptval.pop(sltdChk);
                      var index = listcnptval.indexOf(sltdChk);
                      if (index >= 0) {
                        listcnptval.splice( index, 1 );
                      }
                }
                //alert(listcnptval);
              document.getElementById("recipients").value = listcnptval.toString();
              //document.getElementById('alchkdCntp').title = listcnptval;  
            });


        });
    </script>

@stop