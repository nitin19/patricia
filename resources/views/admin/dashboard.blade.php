@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')

<div class="content-wrap">
  <div class="yelloback">
    <div class="main main-content">
      <div class="container-fluid">
        <div class="row order_box wow fadeInLeft">
          <div class="col-sm-3 ist_box clr effect">
            <a href="{{ url('/admin-orders') }}">
            <div class="col-sm-9">
                <h3 style="color:white;margin: 0;">{{ $order_count }}</h3>
              </div>
              <div class="col-sm-3 text-right">
               <img src="{{ url('/public') }}/admin/images/icon1.png" class="skyimg">
              </div>
              <div class="col-sm-12">
                <hr>
                <h5 style="color:white;margin: 0;">Total Orders</h5>
              </div>
          </a>
          </div>
          
          <div class="col-sm-3 ist_box dangerclr effect">
           <a href="{{ url('/admin-provider') }}"> 
            <div class="col-sm-9">
                <h3 style="color:white;margin: 0;">{{ $provider_users_count }}</h3>
              </div>
              <div class="col-sm-3 text-right">
               <img src="{{ url('/public') }}/admin/images/icon2.png" class="skyimg">
              </div>
              <div class="col-sm-12">
                <hr>
                <h5 style="color:white;margin: 0;">Total Providers</h5>
              </div>
          </a>
          </div>
          <div class="col-sm-3 ist_box warningclr effect">
           <a href="{{ url('/admin-taker') }}"> 
            <div class="col-sm-9">
                <h3 style="color:white;margin: 0;">{{ $taker_users_count }}</h3>
              </div>
              <div class="col-sm-3 text-right">
               <img src="{{ url('/public') }}/admin/images/icon2.png" class="skyimg">
              </div>
              <div class="col-sm-12">
                <hr>
                <h5 style="color:white;margin: 0;">Total Takers</h5>
              </div>

          </a>
          </div>
          <div class="col-sm-3 ist_box greenclr effect">
           <a href="{{ url('/admin-feedback') }}"> 
            <div class="col-sm-9">
                <h3 style="color:white;margin: 0;">{{ $feedback_count }}</h3>
              </div>
              <div class="col-sm-3 text-right">
               <img src="{{ url('/public') }}/admin/images/feedback.png" class="skyimg">
              </div>
              <div class="col-sm-12">
                <hr>
                <h5 style="color:white;margin: 0;">Total Feedbacks</h5>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="tab_imgbox">
        <div class="row three_box wow fadeInLeft">

          <div class="col-sm-4 ta_imgbox wow animated.hinge tabimgbox">
            <nav class="navbar navborder">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand minus nav_nav" href="#">Recently Orders</a>
                </div>
                <!-- <ul class="nav navbar-nav navbar-right">
                  <li><a href="#"><span class="fa fa-minus minus"></span> </a></li>
                  <li><a href="#"><span class="fa fa-times minus"></span> </a></li>
                </ul> -->
              </div>
            </nav>
            <div class="reorder">
            @if($booking->count()>0)
            <?php $count_order = 1; ?>
            @foreach($booking as $booking_info) 
            <div class="media payment_box order_detail">
              <div class="media-body button_side"> 
                <!--<h5>{{ $booking_info->profile_id }}</h5>-->
                  <h5>{{ $booking_info->name }}</h5>
                  <div class="media-body button_si">
                @if(!empty($booking_info->booking_amount))
                <button type="button" class="btn btn-primary btn-xs <?php if($count_order==1) { echo 'btn-warning'; } else if($count_order==2) { echo 'button_sky'; } else if($count_order==3) { echo 'btn-danger'; } ?> else { echo 'btn-success'; } ?>" style="font-size: 10px;margin:5px 0px;">
                {{ $booking_info->booking_amount }}</button>
                @endif
              </div>
                  <h6>{{ substr($booking_info->booking_message, 0, 50) }}</h6>
                  <!--<p>{{ $booking_info->user_id }}</p>-->
              </div>
              
            </div> 
            <?php ++$count_order; ?>
            @endforeach
            @else
            <div class="media payment_box text-center">
              <h5>No Recent Orders</h5>
            </div>
            @endif
          </div>
            <div class="media payment_box">
              <p class="view_content"><a href="{{ url('/admin-orders') }}" class="badge badge-primary view_btn">View All Orders</a></p>
            </div>
            
          </div>

          <div class="col-sm-4 red_tab wow fadeInLeft latestmember_sec tabimgbox">
            <nav class="navbar navborder">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand minus nav_nav" href="#">Latest Providers</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                  <span class="btn btn-danger members">New Members</span>
                </ul>
              </div>
            </nav>
                                   
            <div class="row img_content">
              @foreach($provider_users as $provider_info) 
              <div class="col-sm-4 col-xs-4 text-center custm_cols">
                <?php if($provider_info->profile_image!=''){ ?>
                <img src="{{ url('/public/images/profileimage/') }}/<?php echo $provider_info->profile_image; ?>" class="istimg" width="100%">
                <?php } else { ?>
                <img src="{{ url('/public/') }}/images/prflimg.png" class="istimg" width="100%">
                <?php } ?>
                  <h6>{{ ucfirst($provider_info->name) }}</h6>
                  <small class="small_txt">{{ date('d-m-Y', strtotime($provider_info->created_at)) }}</small>
              </div>
              @endforeach
            </div>
            <p class="red_box"><a href="{{ url('/admin-provider') }}" class="badge badge-primary view_btn">View All Providers</a></p>
          </div>


          <div class="col-sm-4 red_tab wow fadeInLeft latestmember_sec tabimgbox">
            <nav class="navbar navborder">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand minus nav_nav" href="#">Latest Takers</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                  <span class="btn btn-danger members">New Members</span>
                </ul>
              </div> 
            </nav>
                                   
            <div class="row img_content">
              @foreach($taker_users as $taker_info) 
              <div class="col-sm-4 col-xs-4 text-center custm_cols">
                <?php if($taker_info->profile_image!=''){ ?>
                <img src="{{ url('/public/images/profileimage/') }}/<?php echo $taker_info->profile_image; ?>" class="istimg" width="100%">
                <?php } else { ?>
                <img src="{{ url('/public/') }}/images/prflimg.png" class="istimg" width="100%">
                <?php } ?>
                  <h6>{{ ucfirst($taker_info->name) }}</h6>
                  <small class="small_txt">{{ date('d-m-Y', strtotime($taker_info->created_at)) }}</small>
              </div>
              @endforeach
            </div>
            <p class="red_box"><a href="{{ url('/admin-taker') }}" class="badge badge-primary view_btn">View All Takers</a></p>
          </div>
         <!--  <div class="col-sm-4 tab_gray wow fadeInLeft tabimgbox">
            <nav class="navbar navborder">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand minus" href="#">Browser Usage</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="#"><span class="fa fa-ellipsis-h dot"></span> </a></li>
                </ul>
              </div>
            </nav>
            <div class="row nopadding">
              <div class="pending">
                <div class="media slid">
                  <div class="media-body">
                    <img src="{{ url('/public') }}/admin/images/23_admin_dashboard (1) (1).png" class="graphimage" width="100%" alt="istimg">
                  </div>
                  <div class="media-body">
                     <ul class="graph">
                        <li class="ist_line">Pending Payment</li>
                        <li class="appro_line">Aproved Payment</li>
                        <li class="relese_line">Relesed Payment</li>
                        <div class="navigator_chec">
                          <input type="radio" id="defaultRadio" name="example2">
                          <label class="navigator" for="defaultRadio">Navigator</label>
                        </div>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="icon_box">
                  <div class="media payment_box">
                    <div class="media-body down_button">
                      <p>United state of America</p>
                    </div>
                    <div class="media-body button_red down-heading">
                      <span class="down_clr pull-right clickable clrred"><i class="glyphicon glyphicon-chevron-down" aria-hidden="true">12%</i></span>
                    </div>
                  </div>
                  <div class="media payment_box">
                    <div class="media-body down_button">
                      <p>India</p>
                    </div>
                    <div class="media-body button_red down-heading">
                      <span class="down_clr pull-right clickable clrdf"><i class="glyphicon glyphicon-chevron-down" aria-hidden="true">4%</i></span>
                    </div>
                  </div>
                  <div class="media payment_box">
                    <div class="media-body down_button">
                      <p>China</p>
                    </div>
                    <div class="media-body button_red down-heading">
                      <span class="down_clr pull-right clickable clr_yellow"><i class="glyphicon glyphicon-chevron-down" aria-hidden="true">0%</i></span>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
      <br>
      
      @if($closeaccountrequests != '')
      <div class="col-sm-12 tablesection wow fadeInLeft">

        <div class="table_box tablediv tablesection">
            <h4 style="text-align:center;color:#666666;" class="pt-21">Close Requests</h4>
          <table class="table table-borderless">
            <thead>
              <tr>
                <th class="column3">ID</th>
                <th class="column4">User</th>
                <th class="column4">Data</th>
                <th class="column4">Status</th>
                <th class="column5">Reason</th>
              </tr>
            </thead>
            <tbody>
              @if(count($closeaccountrequests)>0)
              <?php $sno=1;?>
              @foreach($closeaccountrequests as $closeaccountrequest)
              <tr class="dashboard_table1 tabledata">
                <td class="column3"><?php echo $sno; ?></td>
                <td class="column4">{{ ucfirst($closeaccountrequest->name) }}</td>
                <td class="column4">{{date('d M Y', strtotime($closeaccountrequest->created_at)) }}</td>
                <td class="column4"><?php if($closeaccountrequest->admin_activity==1)echo"<button type='button' class='btn btn-success btn-xs'>Approved</button>";
                 elseif($closeaccountrequest->admin_activity==0)echo"<button type='button' class='btn btn-warning btn-xs'>Pending</button>";
                 elseif($closeaccountrequest->admin_activity==2)echo"<button type='button' class='btn btn-danger btn-xs'>Denied</button>"; 
                 ?></td>
                <td class="column5"><p>{{ $closeaccountrequest->close_reason }}</p></td>
              </tr>
              <?php $sno++; ?>
              @endforeach
              @else
              <tr><td align="center" colspan="5">No Record Found</td></tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
      @endif
      <br>
<!--       <div class="col-sm-12">
        <div class="requestbox tablediv tablesection">
          <table class="table table-borderless wow fadeInLeft">
          <p>Recent Request Payments</p>
          <thead>
            <tr>
              <th>ID</th>
              <th>User</th>
              <th>Data</th>
              <th>Status</th>
              <th>Reason</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr class="dashboard_table2 tabledata">
              <td>183</td>
              <td> john Doe</td>
              <td>11-7-2014</td>
              <td><button type="button" class="btn btn-success">Approved</button></td>
              <td><p>Bacon ips doler sit amet salami vension chicken flank fatback do</p>
              </td>
              <td>$1200.00</td>
            </tr>
            <tr class="dashboard_table2 tabledata">
              <td>219</td>
              <td> john Doe</td>
              <td>11-7-2014</td>
              <td><button type="button" class="btn btn-warning">Pending</button></td>
              <td><p>Bacon ips doler sit amet salami vension chicken flank fatback do</p>
              </td>
              <td>$1200.00</td>
            </tr>
            <tr class="dashboard_table2 tabledata">
              <td>657</td>
              <td> john Doe</td>
              <td>11-7-2014</td>
              <td><button type="button" class="btn btn-primary approved">Approved</button></td>
              <td><p>Bacon ips doler sit amet salami vension chicken flank fatback do</p>
              </td>
              <td>$1200.00</td>
            </tr>
            <tr class="dashboard_table2 tabledata">
              <td>175</td>
              <td> john Doe</td>
              <td>11-7-2014</td>
              <td><button type="button" class="btn btn-danger">Denied</button></td>
              <td><p>Bacon ips doler sit amet salami vension chicken flank fatback do</p>
              </td>
              <td>$1200.00</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div> -->
    <?php
    $lat_str1 = '';
    $long_str1 = '';
    foreach($users as $user_info){
      if($user_info->latitude!=''){
        $lat_str1 .= $user_info->latitude.',';
        $long_str1 .= $user_info->longitude.',';
      }
    }
    $lat_str = explode(',',rtrim($lat_str1,","));
    $long_str = explode(',',rtrim($long_str1,','));
    $data = array_combine($lat_str,$long_str);

    $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
     $latitude_center =$new_arr[0]['geoplugin_latitude'];
     $longitude_center = $new_arr[0]['geoplugin_longitude'];
    
    ?>
    <div class="container-fluid">
      <div class="col-sm-12 Ser_Locations wow fadeInLeft tab_imgbox">
        <nav class="navbar navborder">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand minus" href="#">Services Providers Locations</a>
            </div>
            <!-- <ul class="nav navbar-nav navbar-right">
              <li><a href="#"><span class="fa fa-minus minus"></span> </a></li>
              <li><a href="#"><span class="fa fa-times minus"></span> </a></li>
            </ul> -->
          </div>
          </nav>
          <section class="mapsection">
            <div id="map" style="width: 100%; height: 490px; background: #89C4FA"/></div>
          </section>
      </div>
    </div>
 
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&sensor=false&libraries=places"></script>
<script>
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        maxZoom: 16
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var markers = [
      <?php 
      $intpart = '';
      foreach($providerUsers as $user_info) {
        if(empty($user_info->profile_image)){
          $image_path =  url('/public').'/images/profileimage/1542707849.images.png';
        } else {
          $image_path =  url('/public').'/images/profileimage/'.$user_info->profile_image;
        }
        if($user_info->latitude!='' ){
          $email = $user_info->email;
          $cat_image = $user_info->cat_image; 
          $username = $user_info->username;
          $latitude = $user_info->latitude;
          $longitude = $user_info->longitude;
          if($cat_image==''){
            $image = url('/public').'/images/abtservice.pngmages/categoryimage/clothing_icon.png';
          } else {
            $image = url('/public').'/images/categoryimage/'.$cat_image;  
          }
        }
           echo '["'.$username.'", '.$latitude.', '.$longitude.', "'.$image.'"],';
      }
      ?>  
    ];
    var infoWindowContent = [
      <?php $intpart = ''; 
      foreach($providerUsers as $user_info) {
        if(empty($user_info->profile_image)){
          $image_path =  url('/public').'/images/profileimage/1542707849.images.png';
        } else {
          $image_path =  url('/public').'/images/profileimage/'.$user_info->profile_image;
        }
        if($user_info->latitude!='' ){
          $email = $user_info->email;
          $cat_image = $user_info->cat_image;
          if($cat_image==''){
            $image = url('/public').'/images/abtservice.pngmages/categoryimage/clothing_icon.png';
          } else {
            $image = url('/public').'/images/categoryimage/'.$cat_image;  
          }
          ?>
          ['<div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodytext"><a class="user_txt" href="{{ url("/admin-users/show")}}/<?php echo $user_info->id; ?>" ><?php echo ucfirst($user_info->username); ?></a>' +
          '<div><span class="mapheading">City:</span> <?php echo $user_info->city; ?></div><div><span class="mapheading"> Category: </span> <?php echo $user_info->name; ?></div></a><div class="mapbodyrating"><span class="mapheading"><?php if($user_info->profile_rating){
              $rating = number_format($user_info->profile_rating,1);
              $intpart = floor ( $rating );
              $fraction = $rating - $intpart;
              for($x=1;$x<=$user_info->profile_rating;$x++) { ?> <i class="fa fa-star"></i> <?php } if (strpos($user_info->profile_rating,'.')) {?> <i class="fa fa-star-half-o" aria-hidden="true"></i> <?php $x++; }  while($x <= 5) {?> <i class="fa fa-star-o"></i> <?php  $x++; } } else { for($z=1;$z<=5;$z++) { ?> <i class="fa fa-star-o"></i> <?php }  }?> </span></div></div></div>','<?php echo $image; ?>'],
  <?php }
      }
  ?>
    ];
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      marker = new google.maps.Marker({
          position: position,
          map: map,
          title: markers[i][0],
          icon: markers[i][3]
      });
      bounds.extend(marker.position);   
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              infoWindow.setContent(infoWindowContent[i][0]);
              infoWindow.open(map, marker);
          }
      })(marker, i));
      map.fitBounds(bounds);
    }
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        this.setCenter({lat: -23.60332035036982, lng: -46.723359508984345});
        google.maps.event.removeListener(boundsListener);
    });
}
google.maps.event.addDomListener(window, 'load', initMap);
</script>
<style type="text/css">
.mail-images {
  width: 24%;
  margin-top: 0%;
  margin-left: 0%;
}
.mapbodytext {
  color:#000;
  padding: 3px 15px;
}
.mapheading {
  color: #000 !important;
}
</style>
@stop