$listresult=   DB::table('notifications')
               ->leftjoin('users', 'notifications.user_id', '=','users.id' )
               ->leftjoin('close_account_request', 'notifications.type_id', '=', 'close_account_request.id')
               ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
               ->where('notifications.read_status', '0')
               ->select('notifications.*', 'booking.booking_message','close_account_request.close_reason','users.*')

               ->orderBy('notifications.created_at', 'desc')
               ->limit(5)
               ->get();
@if($listresult != ' ')

@foreach($listresult as $userInfo) 

  <li><img src="@if($userInfo->profile_image =='') {{ url('/public/admin/images') }}/gravtar.jpeg @else {{ url('public/images/')}}/profileimage/{{ $userInfo->profile_image }} @endif"><span><b>{{ $userInfo->name}}</b><br/>@if($userInfo->notification_type =='closerequest')
     <a href="/admin-closerequest"> {{ $userInfo->close_reason}} </a>
  @elseif($userInfo->notification_type =='booking') <a href="/admin-orders">{{ $userInfo->booking_message}} </a>
  @else
    <a href="/admin-users">@if($userInfo->user_role =='Provider') New Provider Arrived @else New Taker Arrived @endif.</a>
  @endif</span>
  </li>
@endforeach
@endif