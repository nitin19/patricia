@extends('layouts.default-header-admin')

@section('title', 'Add Service')

@section('content')
<!-- <div class="alert alert-success" id="Alert_Success" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
  Service deleted successfully!
</div> -->
<!-- Custom styles for this template -->
<link href="https://getbootstrap.com/docs/4.0/examples/starter-template/starter-template.css" rel="stylesheet">
<div class="content-wrap">
 <div class="yelloback">
  <div class="container-fluid">
    <div class="tablediv">  
      <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-12">
          <div class="media payment_box">
            <!-- <div class="col-sm-10"> -->
              <div class="media-body button_side">
                <h3 style="text-align: left;">Other Services <a href="{{url('/admin-other-services/create')}}" class="btn btn-success pull-right">Add New </a></h3>
              </div>
            <!-- </div> -->
          </div>
        </div>
      </div>
      <div class="row">
        @include('layouts.notify-message')
        
        <div class="col-sm-12 content_box">
          <div class="row">
            <div class="col-sm-8 col-xs-8 category_title">
              
            </div>
          </div>
          <table id="subcategory_table" class="table table-borderless heading-name">
            <thead>
              <tr>
                <th> Sr No </th>
                <th> Title </th>
                <th> Service Postion </th>
                <th> Action </th>
              </tr>
            </thead>
            
            <tbody>
              @if($category->count()>0)
             
              <?php $count = 0; ?>
              @foreach($category as $categories)
              <tr id="{{ $categories->id }}"  class="service_table tabledata">
                <td class="service_pos" > {{ ++$count }}</td>
                <td> {{ ucfirst($categories->title) }} </td>
                <td> {{ $categories->service_position }}</td>  
                <td><a  data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" ="" href="{{ url('/admin-other-services') }}/{{ $categories->id }}/edit"><i class="fa fa-edit" style="font-size:16px"></i></a> 
                  <a class="deletebutton" style="cursor: pointer;" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" style="font-size:16px"></i></a> 
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          {{$category->links()}}
        </div>
        <input type="hidden" value="{{$category->count()}}" class="record_count">
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div> 
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>  
<script type="text/javascript">
   $('tbody').sortable(
              { 
                revert       : true,
                connectWith  : ".sortable",
                stop : function(event,ui){ 
                  //alert(' current index is :  '+  $(ui.item).index() );
                  var updated_index = [];
                  $(this).find('tr').each(function(i){
                    //alert($(this).attr('id')  + "----" + $(this).index() );
                    updated_index.push({ "id" : $(this).attr('id'), "order" : $(this).index() });
                  });
                  console.log("updated array");
                  console.log( updated_index );
                  jQuery.ajax({
                  url: "{{url('/')}}/admin-other-services/service_position",
                  type: 'POST',
                  data: {'updated': updated_index,"_token": "{{ csrf_token() }}" },
                  success: function(data) {
                    if(data == 1)
                    {
                      $('#Alert_Success').show();
                        $.notify({
                          message: 'Your service position updated successfully.',
                        },{
                          type: 'success',
                          offset: 
                          {
                           x: 10,
                           y: 130
                         },
                         animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                         },
                       });
                      }  
                      else
                      {
                        $('#Alert_Danger').show();
                        $.notify({
                          message: 'Something happen wrong',
                        },{
                          type: 'success',
                          offset: 
                          {
                           x: 10,
                           y: 130
                         },
                         animate: {
                           enter: 'animated fadeInRight',
                           exit: 'animated fadeOutRight'
                         },
                       });
                      } 
                    }          
                  })  

                }
            });
//   jQuery('.service_table').dragend(function(){
//     var serviceid = jQuery(this).attr('id');
//     var col_index = $(this).index();
//     alert( 'tr index : ' + col_index );
//     var servicepos = $(this).closest('tr').children('td:first').text();
//     //alert(servicepos);
//     jQuery.ajax({
//       url: "{{url('/')}}/admin-other-services/service_position",
//       type: 'POST',
//       data: {'id': serviceid,'service_position': servicepos,"_token": "{{ csrf_token() }}" },
//       success: function(data) {
//         if(data == 1)
//         {
//           $('#Alert_Success').show();
//             $.notify({
//               message: 'Your service position updated successfully.',
//             },{
//               type: 'success',
//               offset: 
//               {
//                x: 10,
//                y: 130
//              },
//              animate: {
//                enter: 'animated fadeInRight',
//                exit: 'animated fadeOutRight'
//              },
//            });
//           }  
//           else
//           {
//             $('#Alert_Danger').show();
//             $.notify({
//               message: 'Something happen wrong',
//             },{
//               type: 'success',
//               offset: 
//               {
//                x: 10,
//                y: 130
//              },
//              animate: {
//                enter: 'animated fadeInRight',
//                exit: 'animated fadeOutRight'
//              },
//            });
//           } 
//         }          
//       })        
// });
 if($('.record_count').val() == 0){
    $('#subcategory_table_info').css("display","none");
    $('#subcategory_table_paginate').css("display","none");
    $('.paginate_button').css("display","none");
 }
$('#subcategory_table').DataTable({
  // if($('.record_count').val() == 0){
  //   "bPaginate": false,
  //   "paging":   false,
  //   "ordering": false,
  //   "info":     false
  // }
});
jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
  if(confirm("Are you sure you want to delete this?")){
    var catid = jQuery(this).closest('tr').attr('id');
    jQuery.ajax({
      url: "{{url('/')}}/admin-other-services/category_remove",
      type: 'POST',
      data: {'catid': catid,"_token": "{{ csrf_token() }}" },
      success: function(data) {
        if(data == 1)
        {
          $('#Alert_Success').show();
          jQuery('#'+catid).hide();
            // window.setTimeout(function() {
            //   $('#Alert_Success').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your service deleted successfully!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          }  
          else
          {
            $('#Alert_Danger').show();
            // window.setTimeout(function() {
            //   $('#Alert_Danger').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your service  cannot  deleted!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          } 
        }          
      })      
  }
  else{
    return false;
  }
  window.setTimeout(function() {
            // $(".").fadeTo(500, 0).slideUp(500, function(){
              $('test21').remove(); 
            // });
          }, 4000); 
});
</script>
<script> 
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop