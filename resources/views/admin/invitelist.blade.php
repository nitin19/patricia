@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')

<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">
<div class="form_section">
  <div class="row">
<div class="col-sm-12 content_box">
  <div class="marbot media">
    <div class="col-sm-12"> 
      <div>
       <h3>Invitation<span class="list_clr"> ({{ $invitecount }})</span>
        <a href ="{{ url('/admin-invite/excel') }}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a></h3>
       <!-- <hr> -->
      </div>
    </div>
    <!-- <div class="col-sm-2">
     
    </div> -->
  </div>
   
<div class="col-sm-12">
<!--  <div class="service_box tablediv"> -->
 <div class="tablediv">
  <table id="invitelist_table" class="table table-borderless heading-name">
    <thead>
      <tr>
        <th class="column2">Invited By</th>
        <th class="column4">Email</th>
        <!-- <th class="column4">Total No. of
          Invitation Sent</th> -->
        <th class="column3">Email Invite Friend</th>
        <th class="column2">Phone </th>
        <th class="column2">Date-Inv</th>
        <th class="column2">Date-Reg</th>
        <th class="column3">Inviter Location</th>
        <th class="column2">Reg-Occured?</th>
        <!-- <th class="column2">Status</th> -->
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user_info)
      <?php
      $name = $user_info->name;
      $email = $user_info->email;
      $is_active = $user_info->is_active;
      $invite_friend_count = DB::table('invite_friend')
                       ->where('invite_by_id', $user_info->id)
                       ->where('is_active', '1')
                       ->where('is_deleted', '0')
                       ->count();
      $invite_friend_data = DB::table('invite_friend')
                       ->where('invite_by_id', $user_info->id)
                       ->where('is_active', '1')
                       ->where('is_deleted', '0')
                       ->first();

      $invite_friend_check_registration = DB::table('users')
                                    ->where('email', $invite_friend_data->invite_to_email)
                                    ->where('is_active', '1')
                                    ->where('is_deleted', '0')
                                    ->first();
      if($invite_friend_check_registration){
        $register_status = 'Yes';
        $invitation_registration_data = date('Y-m-d', strtotime($invite_friend_check_registration->created_at));
      } else {
        $register_status = 'No';
        $invitation_registration_data = '---';
      }

      $invitation_date = date('Y-m-d', strtotime($invite_friend_data->invite_date));
      
      ?>
      <tr class="tabledata invite_table">
        <td class="column2">{{ $name }}</td>
        <td class="column4"><span class="painting">{{ $email }}</span></td>
        <td class="column3">{{$invite_friend_data->invite_to_email}}</td>
        <!-- <td class="column4">{{ $user_info->countinvite }}</td> -->
        <td class="column2">{{ $user_info->phone }}</td>
        <td class="column2">{{$invitation_date}}</td>
        <td class="column2">{{$invitation_registration_data}}</td>
        <td class="column3">{{ $user_info->city }}</td>
        <td class="column2">{{$register_status}}</td>
        <!-- <td class="column2">@if($is_active==1)Active @else Inactive @endif</td> -->
      </tr> 
@endforeach
    </tbody>
  </table>

</div>
  </div>
 </div>
  </div>
</div>

<script type="text/javascript">
// $(document).ready(function() {
//     $('#invitelist_table').DataTable();
// } );

$(document).ready(function() {
    $('#invitelist_table').dataTable( {
        "bFilter": false,
        "bInfo": false,
         "ordering": false,
         "searching": true
                 } );
} );
</script>
@stop