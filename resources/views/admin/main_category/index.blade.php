@extends('layouts.default-header-admin')

@section('title', 'Categories List')

@section('content')
<div class="yelloback">
 <div class="lock_box">
<div class="contact_box">
<div class="form_section">
<div class="col-sm-12 content_box">
  <div class="payment_box">
  <div class="row">
    <div class="col-sm-12">
      <div class="media payment_box">
                  <div class="col-sm-6">
          <div class="media-body button_side">
           <h3>Main Category List <span class="list_clr">( {{count($main)}} Category )</span></h3>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="media-body button_sky bluee" style="text-align: right;">
          <a href="{{ url('/admin/main-category/create') }}" class="btn btn-primary button_blueblue"
            style="z-index: 9; position: relative;text-align: right;">Add Main-Category</a>
         </div>
        </div>
    </div>
      
    </div>
    
  
</div>
</div>

<div class="tablediv">
  <br />
  <div class="alert alert-success" id="Alert_Success" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
     Category deleted successfully!
</div>
  <div class="alert alert-danger" id="Alert_Danger" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
     Sorry you can't delete this category, its already in use!
</div>
  <div class="alert alert-success alert-block success_show">
        <button type="button" class="close" data-dismiss="alert">×</button>    
  </div>
  @include('layouts.flash-message') 
<table id="main_cat_table" class="table stripped-table">
    <thead>
        <th>S.no</th>
        <th>Image</th>
        <th>Name</th>
        <th>Action</th>
    </thead>
    <?php $i=0;?>
@foreach($main as $main_cat)
   <?php $cat_image = $main_cat->cat_image; 
    if($cat_image==''){
    $categoryimage = "/public/images/default-img.png";

    }else {
    $categoryimage = "/public/images/main_category/".$cat_image;  
    }
     ?>
<?php $i++;?>
<tr id="{{ $main_cat->id }}" class="category_table tabledata">
    <td>{{$i}}</td>
    <td> <img src="{{ $categoryimage }}" height="40" width="40"> </td>
    <td>{{$main_cat->name}}</td>
    <td colspan="2">
        <a href="/admin/main-category/edit/{{$main_cat->id}}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" class="fa fa-edit"></i>
          <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" class="fa fa-trash delete_icon" aria-hidden="true"></i></a>
        <!--<a href="{{url('/admin/main-category/'.$main_cat->id.'/edit' )}}"><i class="fa fa-edit"></i><i class="fa fa-trash delete_icon" aria-hidden="true"></i>-->
  
    <form method="POST" action="{{url('/admin/main-category/destroy'.$main_cat->id )}}">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form>
    
    
    </td>
</tr>
@endforeach

</table>
</div>
</div>
</div>
</div>


<style type="text/css">
.service_description { color: #323334 !important; }	
button.btn.btn-box {
    background-color: #0E3C60;
    box-shadow: 0 0 20px #ccc;
    color: #ffffff;
    font-family: lato;
    font-size: 17px;
    font-weight: bold;
    letter-spacing: 0.8px;
    padding: 12px;
    text-transform: uppercase;
}
#service_img {
    border: 0px;
}
.content_box h3 {margin:0px;}
</style>

<script type="text/javascript">
  jQuery('.success_show').hide();
  jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure?")){
      var catid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "/admin/main-category/destroy",
          type: 'POST',
          data: {'id': catid,"_token": "{{ csrf_token() }}" },
          success: function(data) {
          if(data == 1)
          {
            $('#Alert_Success').show();
            jQuery('#'+catid).remove();
            setTimeout(function() {
            $('#Alert_Success').remove(); 
            }, 4000);
          }
          else
          {
            $('#Alert_Danger').show();
            setTimeout(function() {
            $('#Alert_Danger').remove(); 
            }, 4000);
          }            
          }      
      })
    }
      else{
        return false;
      }
});
</script>
<script type="text/javascript">
$(document).ready(function() {  
$('#main_cat_table').DataTable();
  });
 </script>

<!--<script>
    $(document).ready(function(){
    $('.delete_icon').on('click',function(){
      if(confirm('Are You sure??')){
            $('.btn_del').click();
      }
      else{
      return false;
      }
    });
});
</script>-->
@stop


