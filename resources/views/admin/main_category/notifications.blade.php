@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="content-wrap">
<section class="notificationpage">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="notificationhead wow fadeInDown">
                        <h1>Notifications</h1>
                    </div>
                </div>
               
                <div class="notificationbox">
                    <div class="alert alert-success alert-block success_show">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>Notification deleted successfully!</strong>
                    </div> 

                    <div class="firstnotification wow animated fadeInLeft">
                        @foreach($listresult as $userInfo)
                        
                        <div class="crossbtnnoti">
                            <span class="n_id" id ="{{ $userInfo->id }}" ><a class="deletebutton" style="cursor: pointer;" ><img src="{{ url('/') }}/public/admin/images/noticlose.png" alt="Delete"/></a></span>
                            
                        </div>
                        <div class="notifidata">
                             @if($userInfo->notificaton_type =='booking')
                            <button class="btn btn-primary join mess"> Booking</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                            date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                            <h1>Booking Message: {{ $userInfo->booking_message }}</h1>
                          @elseif($userInfo->notificaton_type =='closerequest') 
                           <button class="btn btn-warning join">Close Account Request</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                            date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                            <h1>Account Close Reason : {{ $userInfo->close_reason }}</h1>
                          @else
                           <button class="btn btn-primary join">Join New user</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                            date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                           <h1> @if($userInfo->user_role =='Provider')
                              New Registration: New Provider Arrived.
                               @else 
                                New Registration: New Taker Arrived.
                            @endif</h1>
                        @endif
                    </h1>
                            <h3>{{ $userInfo->name }}</h3>
                            
                        </div>
                        <hr class="notiboxline">
                        @endforeach
                    </div>

                   
                    
                   

                    

                </div>

            </div>
        </div>

    </section>
</div>
<script type="text/javascript">
  jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var id = jQuery('.n_id').attr('id');
      alert(id);exit;
          jQuery.ajax({
          url: "/admin/notifications/destroy",
          type: 'POST',
          data: {'id':id,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          jQuery('.success_show').show();
         
          }            
          });      
      }
});
</script>


@stop
