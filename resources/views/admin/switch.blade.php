@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box">
  <table class="table table-borderless heading-name">
    <thead>
      <tr>
        <th>Sr No.</th>
        <th>Name</th>
        <th>Email</th>
        <th>Category </th>
         <th>Address</th>
          <th>Status</th>
      </tr>
    </thead>
    
  </table>
  <div class="pagination-button">

<div class="pagination prev_next">
 {!! $users->render() !!}
</div>
  </div>
</div>
  </div>
 </div>
@stop