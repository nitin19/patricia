@extends('layouts.default-header-admin')

@section('title', 'Feedback List')

@section('content')
<style>
	span.btnmore {
    color: #3399cc;;
    word-break: normal;
    cursor: pointer!important;
}
span.btnless {
    color: #3399cc; 
    cursor: pointer!important;
}
</style>

<div class="yelloback">
	<div class="lock_box">
		<!-- <div class="alert alert-success feedback_alert notify"  style="display:none;">
		
		</div> -->
		<div class="contact_box">
			<div class="form_section">
				<div class="col-sm-12 content_box">
					<div class="row marbot">
						<div class="col-sm-6 col-xs-6 category_title">
							<h3>Feedback List <span class="list_clr"> ({{ count($feedbacks) }})</span>
                             <a href="{{ url('/admin/feedback-export') }}" class="btn btn-primary excl_export">Excel</a> 
							</h3>
                            
						</div>
						
					</div>
					<div class="tablediv">
 
						@include('layouts.notify-message') 
						<table id="main_cat_table" class="table stripped-table">
							<?php $i=0;?>
							<thead>
								<tr>
									<th class="column2">Order Name</th>
									<th class="column2">Taker Name</th>
									<th class="column4">Taker Email</th>
									<th class="column2">Provider Name</th>
									<th class="column4">Provider Email</th>
									<th class="column2">Message</th>
									<th class="column2">Rating</th>
									<th></th>
									<th class="column2">Action</th>
								</tr>   
							</thead>
							<tbody>
								@if(count($feedbacks) > 0)
								
								@foreach($feedbacks  as $feedback)

								<?php 
								// echo "<pre>";
								// print_r($feedback);
								// echo "</pre>";

								//DB::enableQueryLog();
								$taker_name =  DB::table('users')->select('name as taker_name', 'email as taker_email','is_deleted','is_active','approved_by_admin')->where('id', $feedback->user_id)->first();
								//dd(DB::getQueryLog());
								// echo "<pre>";
								// print_r($taker_name);
								// echo "</pre>";
								 ?>
								<tr class="feedbk_table tabledata">
									<td class="column2">{{$feedback->cat_name}}</td>
									<td class="column2">{{ucfirst($taker_name->taker_name)}}</td>
									<td class="column4">{{ucfirst($taker_name->taker_email)}}</td>
									<td class="column2">{{ucfirst($feedback->name)}}</td>
									<td class="column4">{{ucfirst($feedback->email)}}</td>
									<td class="column2 fdbmsgrow" id="msgrow{{$feedback->feedback_id}}"><?php if(strlen($feedback->message) > 100){ echo substr($feedback->message, 0, 16). '<span class="dots" id="dotmsg'.$feedback->feedback_id.'">...</span><span class="readmore" id="moremsg'.$feedback->feedback_id.'" style="display:none;">'.substr($feedback->message, 100, strlen($feedback->message)).'</span><span class="btnmore">Read More</span><span class="btnless" style="display:none;">Read Less</span>';}else{ echo $feedback->message;} ?>
                                    </td>
								
									<td class="column2">
										<?php 
										$rating = number_format($feedback->rating,1);
										$intpart = floor ( $rating );
										$fraction = $rating - $intpart;
										for($x=1;$x<=$feedback->rating;$x++) {?>
											<i class="fa fa-star ycolor" aria-hidden="true"></i>
										<?php }
										if (strpos($feedback->rating,'.')){ if($fraction>0){?>
											<i class="fa fa-star-half-o ycolor" aria-hidden="true"></i>
										<?php } }
										while($x <= 5) {?> <i class="fa fa-star-o ycolor"></i> <?php  $x++; }
										?>
									</td>
									
									<td>{{$feedback->approve_status}}</td>

									<td class="column2 fdbatnrow" colspan="2">
										@if($feedback->approve_status == 0)
										<a  class="btn btn-success btn-xs  approve-feedback approve-{{$feedback->feedback_id}}" data-id="{{$feedback->feedback_id}}"><i class="fa fa-check approv_btn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve"></i></a>
										<a class="btn btn-danger btn-xs denied-feedback deny-{{$feedback->feedback_id}}"  data-id="{{$feedback->feedback_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deny"><i class="fa fa-close approv_btn"></i></a>
										@elseif($feedback->approve_status == 1)
										<a  class="btn btn-success btn-xs  " data-id="{{$feedback->feedback_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approved" disabled><i class="fa fa-check approv_btn"></i></a>
										@else
										<a class="btn btn-danger btn-xs  "  data-id="{{$feedback->feedback_id}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Denied" disabled><i class="fa fa-close approv_btn"></i></a>
										@endif
										<!-- <a  class="btn btn-success btn-xs approve-feedback approve-{{$feedback->feedback_id}}" data-id="{{$feedback->feedback_id}}">Approve</a>
											<a class="btn btn-danger btn-xs denied-feedback deny-{{$feedback->feedback_id}}"  data-id="{{$feedback->feedback_id}}">Deny</a> -->
										</td>

									</tr>
									@endforeach
									@endif
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="approve_feedback" value="1" class="approve-val">
									<input type="hidden" name="denied_feedback" value="2"   class="denied-val">
								</tbody>

							</table>
						</div>
					</div>  
				</div>
			</div>
		
	<style type="text/css">
	.service_description { color: #323334 !important; }	
	button.btn.btn-box {
		background-color: #0E3C60;
		box-shadow: 0 0 20px #ccc;
		color: #ffffff;
		font-family: lato;
		font-size: 17px;
		font-weight: bold;
		letter-spacing: 0.8px;
		padding: 12px;
		text-transform: uppercase;
	}
	#service_img {
		border: 0px;
	}
	.content_box h3 {margin:0px;}
	.excl_export { z-index: 9; position: relative;}
	
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

	table, thead, tbody, th, td, tr { 
		display: block; 
	}
	
	thead tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
	
	tr { border: 1px solid #ccc; }
	
	td { 
		border: none;
		border-bottom: 1px solid #eee; 
		position: relative;
		padding-left: 50%; 
	}
	
	td:before { 
		position: absolute;
		top: 6px;
		left: 6px;
		width: 45%; 
		padding-right: 10px; 
		white-space: nowrap;
	}

	/*td:nth-of-type(1):before { content: "Order Name"; }
	td:nth-of-type(2):before { content: "Taker Name"; }
	td:nth-of-type(3):before { content: "Taker Email"; }
	td:nth-of-type(4):before { content: "Provider Name"; }
	td:nth-of-type(5):before { content: "Provider Email"; }
	td:nth-of-type(6):before { content: "Message"; }
	td:nth-of-type(7):before { content: "Rating"; }
	td:nth-of-type(8):before { content: "Action"; }*/

</style>
<script>
	$(".btnmore").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
    });

    $(".btnless").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
    });
	//$(document).ready(function(){
	

		//$('#main_cat_table').dataTable();
		$('#main_cat_table').DataTable( {
			"columnDefs": [
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 7, "asc" ]]
        } );

		//$('#main_cat_table').DataTable().column(7).visible(false);

		$('#main_cat_table tbody').on('click','.approve-feedback',function(){
			//if(confirm('Are You sure You want To Approve This Feedback')){
				var approve = $('.approve-val').val();
				var feedback_id = $(this).data('id');
				var token = $("input[name=_token]").val();
				var url = '{{url("/admin/approve-feedback")}}';
				$.ajax({
					url:url,
					type:'POST',
					dataType:'json',
					data:{'approve':approve,'_token':token,'feedback_id':feedback_id},
					success:function(data){
						var html = 'Feedback Approved Successfully';
						if(data.success == "true"){
							$('.feedback_alert').show();
							$('.feedback_alert').html(html);
							$('.approve-'+feedback_id).html('<i class="fa fa-check approv_btn"></i>');
							$('.approve-'+feedback_id).addClass('disabled');
							$('.deny-'+feedback_id).hide();
							$.notify({
								message: 'Your Feedback  Status Approved successfully!',
							},{
								type: 'success',
								offset: 
								{
									x: 10,
									y: 130
								},
								animate: {
									enter: 'animated fadeInRight',
									exit: 'animated fadeOutRight'
								},
							});

							// window.setTimeout(function() {
							// 	$(".feedback_alert").hide();
							// }, 3000);
						}
					}
				});
			// }
			// else{
			// 	return false;
			// }
		});
		$('#main_cat_table tbody').on('click','.denied-feedback',function(){
			// if(confirm('Are You sure You want To Denied This Feedback')){
				var denied = $('.denied-val').val();
				var feedback_id = $(this).data('id');
				var token = $("input[name=_token]").val();
				var url = '{{url("/admin/denied-feedback")}}';
				$.ajax({
					url:url,
					type:'POST',
					dataType:'json',
					data:{'denied':denied,'_token':token,'feedback_id':feedback_id},
					success:function(data){
						var html = 'Feedback Denied Successfully';
						if(data.success == "true"){
							$('.feedback_alert').show();
							$('.feedback_alert').html(html);
							$('.deny-'+feedback_id).html('<i class="fa fa-close approv_btn"></i>');
							$('.deny-'+feedback_id).addClass('disabled');
							$('.approve-'+feedback_id).hide();
							$.notify({
								message: 'Your Feedback  Status Denied successfully!',
							},{
								type: 'success',
								offset: 
								{
									x: 10,
									y: 130
								},
								animate: {
									enter: 'animated fadeInRight',
									exit: 'animated fadeOutRight'
								},
							});

							// alert($('.deny-'+feedback_id));
							// window.setTimeout(function() {
							// 	$(".feedback_alert").hide();
							// }, 3000);
						}
					}
				})	
			// }
			// else{
			// 	return false;
			// }	
		});
		
	//});

</script>

@stop