@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<style>
#send-email {
    margin: -28px 0 0px -20px;
}
input[type="submit"] {
    margin: -1% 0 8px 42%; 
}

</style>
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box tablediv tablesection">
 <!--  @include('layouts.flash-message') -->
<div class="row marbot">
  <div class="col-sm-12">
    <div class="media payment_box">
    <!-- <div class="col-sm-10"> -->
      <div class="media-body button_side">
          <h3>Takers List<span class="list_clr"> ({{ $taker_users->count() }})</span>
            <a href ="{{ url('/admin-taker/excel') }}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a>
            <select name="selected_profiles" class="takers-profiles providers-profiles" id="table-filter taker-profiles-status" style="z-index: 9; position: relative;">
            <option value="alltakers">All Takers</option>
            <option value="Completed">Completed Profiles</option>
            <option value="Incompleted">Incompleted Profiles</option> 
           </select>

            <form method="POST" action="{{ url('/admin-taker/complete-profile-mail') }}/" id="send-email" style="z-index: 3; position: relative; display:none;">
          {!! csrf_field() !!}
          <input type="hidden" name="recipients" id="recipients" value="">
          <input type="submit" name="send_email" value="Send Email" >
          </form>

          </h3>
      </div>

       @include('layouts.notify-message')

     <!--  <div class="alert alert-success alert-block success_show">
            <button type="button" class="close" data-dismiss="alert">×</button>
      </div>  -->
   
    </div>
  </div>
</div>

  <table id="taker_table" class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th>Name</th>
        <th>Email</th>
        <th>Registration date</th>
        <th>Access</th>
        <!-- <th>Role</th>
        <th>Category</th> -->
        <th>Phone </th>
        <th>Address</th>
       <!--  <th>Address Approve</th> -->
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($taker_users as $taker_info)
<?php
      $name = $taker_info->name;
      $email = $taker_info->email;
      $created_at = $taker_info->created_at;
      $is_active = $taker_info->is_active;
?>
<tr id="{{ $taker_info->id }}" class="tuserstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td>{{ $name }}</td>
      <td>{{ $email }}</td>
      <td>{{ $created_at }}</td>
      <td>({{ $taker_info->hd_password }})</td>
      <!-- <td>{{ ucfirst($taker_info->user_role) }}</td>
      <td>{{ ucfirst($taker_info->category) }}</td> -->
      <td>{{ $taker_info->phone }}</td>
      <td>{{ $taker_info->city }}</td>
      <!-- <td><form method="POST" action="{{ url('/admin-users/approved_address') }}">@if($taker_info->address_verified == 1)
          <button class="btn btn-success btn-sm" name="approve_status" type="submit" value="2" disabled="">Approved</button>
          @elseif($taker_info->address_verified == 2)
          <button class="btn btn-danger btn-sm" name="approve_status" type="submit" value="1" disabled="">Denied</button>
          @else
           <button class="btn btn-success btn-sm" name="approve_status" type="submit" value="1">Approve</button>
            <button class="btn btn-danger btn-sm" name="approve_status" type="submit" value="2">Deny</button>
          @endif
          <input type="hidden" name="user_id" value="{{$taker_info->id}}">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          </form>
      </td> -->
      <td class="fourIcon"><a href="{{ url('/admin-users/show') }}/{{ $taker_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="View" title="" class="fa fa-eye" style="font-size:16px"></i></a>
        <a href="{{ url('/admin-users/edit')}}/{{$taker_info->id}}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" class="fa fa-pencil" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" title="" class="fa fa-trash" style="font-size:16px"></i>
          <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form>
        </a>

        @if($taker_info->is_active!='1')
        <a href="{{ url('/admin-taker/resendemail') }}/{{ $taker_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Resend account activation email" title="" class="fa fa-envelope" style="font-size:16px"></i></a>
        @endif
        
      </td>
</tr>
@endforeach
    </tbody>
  </table>
  <table id="profile_taker_table" style="display:none;" class="table table-borderless heading-name">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th>Name</th>
        <th>Email</th>
        <th>Registration date</th>
        <th>Access</th>
        <!-- <th>Role</th>
        <th>Category</th> -->
        <th>Phone </th>
        <th>Address</th>
       <!--  <th>Address Approve</th> -->
      </tr>
    </thead>
  </table>

</div>
  </div>


 <script type="text/javascript">
  jQuery('.success_show').hide();
  jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure?")){
      var userid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "/admin/usersdestroy",
          type: 'POST',
          data: {'id': userid,"_token": "{{ csrf_token() }}" },
          success: function(response) {
            $.notify({
              message: response,
              },{
              type: 'success',
              offset: 
              {
                x: 10,
                y: 130
              },
              animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
              },
            });
          // jQuery('.success_show').show();
          // jQuery('.success_show').html(response);
          jQuery('#'+userid).remove();
           // setTimeout(function() {
           //  $('.success_show').remove(); 
           //  }, 2000);
          //alert("User deleted successfully");
          }            
          });      
      }
      else{
        return false;
      }
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    //$('#taker_table').DataTable();
    var table = $('#taker_table').DataTable();
  jQuery('.takers-profiles').on('change', function() {
      var p_status = this.value;
      
      if(p_status == "Incompleted") {
        $('#send-email').show();
      }else {
        $('#send-email').hide();
      }
      
     var newtable = $('#profile_taker_table').DataTable();
     table.destroy();
     newtable.destroy();


     jQuery.ajax({
          url: "/admin-taker/incomplete-profile",
          type: 'GET',
          data: {'p_status': p_status,"_token": "{{ csrf_token() }}" },
          success: function(response) {
            $('#taker_table').hide();
            $('#profile_taker_table').show();
            //var data = JSON.parse(response.data);
            console.log(response);
            $('#profile_taker_table').DataTable( {
                data: response,
                columns: [
                            { 'data': 'name' },
                            { 'data': 'email' },
                            { 'data': 'created_at' },
                            { 'data':'hd_password'},
                            { 'data': 'phone' },
                            { 'data': 'city' },
                           
                        ]
               
            } );
          }
        });
     
    });
} );
</script>
@stop
