<?php
$admin_login_id = Auth::id();
//print_r(Auth::user());die;
$admin_info = DB::table('users')
                 ->where('is_active', '1')
                 ->where('is_deleted', '0')
                 ->where('approved_by_admin', '1')
                 ->where('user_role', 'admin')
                 ->where('id', $admin_login_id)
                 ->first();
//print_r($admin_info);die;
$admin_img = $admin_info->profile_image;
if($admin_img==''){
    $img_profile = "/public/admin/images/gravtar.jpeg";
}
else {
   $img_profile = "/public/admin/images".'/'.$admin_img;
}                 

?>
<!--header-->
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <div class="header">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="notificationicons">
              <ul class="header_div">
                <li class="header-icon">
                  <a data-toggle="collapse" data-target="#msgnotification"><i class="fa fa-envelope-o"></i><span class="counter blue">11</span></a>
                  <div id="msgnotification" class="collapse notifi_dropdown">
                    <span class="text-left notifi_title">Messages<a href="#" class="viewlink">View All</a></span>
                    <ul>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                    </ul>
                  </div>
                </li>
              </ul>
              <ul class="header_div">
                <li class="header-icon">
                  <a data-toggle="collapse" data-target="#notifications" class="quickView"><i class="fa fa-bell-o" aria-hidden="true"></i>
                  <span class="counter red" id = "notiCount"> </span></a>
                  <div  class="collapse notifi_dropdown" id="notifications">
                    <span class="text-left notifi_title"  style="background-color:#cc3333;">Recent Notifications
                      <a href="{{ url('/') }}/admin/notifications" class="viewlink">View All</a></span>
                    <ul class="hfdhfgd" id="loadnotification">
                    </ul>
                  </div>
                </li>
              </ul>
              <ul class="header_div">
                <li class="header-icon headerprflimg dropdown">
                  <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" role="button" aria-haspopup="true">
                  <img src="@if($admin_img=='') {{ url('/public/admin/images') }}/gravtar.jpeg @else {{ url('public/images/')}}/profileimage/{{ $admin_img }} @endif" class="headerprflimg"><span>{{ $admin_info->name }}<i class="fa fa-cog"></i></span></a>
                  <ul class="dropdown-menu profile_dropdown">
                    <span class="text-left notifi_title"></span>
                    <li><a href="/admin-dashboard">Dashboard</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="{{ url('/admin/change-password/') }}">Change Password</a></li>
                    <li><a href="{{ url('/admin/business') }}">Business Settings</a></li>
                    <li><a href="/lockscreen">Lock Screen</a></li>
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                  </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<style>
nav.navbar.navbar-default.navigationbar {
    margin-top: 0px !important;}
</style>