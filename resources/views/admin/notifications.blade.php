@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')

<div class="content-wrap">
    <div class="yelloback">
<div class="container-fluid">
<section class="notificationpage">
    <div class="">
        <div class="row">
            <div class="col-lg-12">
                <div class="notificationhead wow fadeInDown">
                    <h1>Notifications</h1>
                </div>
            </div>
            <div class="col-lg-12">
            <div class="notificationbox">
                <div class="alert alert-success alert-block success_show">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>Notification deleted successfully!</strong>
                </div> 

                <div class="firstnotification wow animated fadeInLeft">
                    @foreach($listresult as $userInfo)
                    <div class="main_{{ $userInfo->notify_id }}">
                    <div class="crossbtnnoti">
                        <span  ><a class="deletebutton" style="cursor: pointer;" id="{{ $userInfo->notify_id }}" ><img src="{{ url('/') }}/public/admin/images/noticlose.png" alt="Delete"/></a></span>
                        
                    </div>
                    <div class="notifidata">
                        @if($userInfo->notificaton_type =='booking')
                        <button class="btn btn-success join mess"> Booking</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                        date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                        <h1>Booking Message: {{ $userInfo->booking_message }}</h1>
                        @elseif($userInfo->notificaton_type =='closerequest') 
                        <button class="btn btn-warning join">Close Account Request</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                        date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                        <h1>Account Close Reason : {{ $userInfo->close_reason }}</h1>
                        @else
                        <button class="btn btn-success join">Join New user</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{  
                        date('d M Y h:i A', strtotime($userInfo->created_at)) }}</span>
                        <h1> @if($userInfo->user_role =='Provider')
                          New Registration: New Provider Arrived.
                           @else 
                            New Registration: New Taker Arrived.
                        @endif</h1>
                        @endif
               
                        <h3 >{{ $userInfo->name }}</h3>
                        
                    </div>
                    <hr class="notiboxline">
                    </div>

                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    <div class="pagination-button">
        <div class="pagination prev_next">
          {!! $listresult->render() !!}
       </div>
    </div>
    </section>
</div>
</div>
<script type="text/javascript">
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
      console.log(idd);
      //alert(idd);exit
      'notifications.id',
          $.ajax({
          url: "/admin/notifications/destroy",
          type: 'POST',
          data: {'id':idd,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          jQuery('.success_show').show();
          jQuery('.main_'+idd).remove();
          }            
          });      
      }
});
</script>
@stop
