<?php
$admin_login_id = Auth::id();
$admin_info = DB::table('users')
                 ->where('is_active', '1')
                 ->where('is_deleted', '0')
                 ->where('approved_by_admin', '1')
                 ->where('user_role', 'admin')
                 ->where('id', $admin_login_id)
                 ->first();
$admin_img = $admin_info->profile_image;
if($admin_img==''){
    $img_profile = "/public/admin/images/gravtar.jpeg";
}
else {
   $img_profile = "/public/admin/images".'/'.$admin_img;
}
$listresult=   DB::table('notifications')
                 ->leftjoin('users', 'notifications.user_id', '=','users.id' )
                 ->leftjoin('close_account_request', 'notifications.type_id', '=', 'close_account_request.id')
                 ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
                 ->where('notifications.read_status', '0')
                 ->select('notifications.*', 'booking.booking_message','close_account_request.close_reason','users.*')

                 ->orderBy('notifications.created_at', 'desc')
                 ->limit(5)
                 ->get();
                 //print_r($listresult);

$notificationCount = DB::table('notifications')
                 ->where('read_status', '0')
                 ->count();
?>
<!--header-->
           <div class="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="notificationicons">
                            <ul class="float-right">
                                <li class="header-icon">

                                    <a data-toggle="collapse" data-target="#msgnotification"><i class="fa fa-envelope-o"></i><span class="counter blue">11</span></a>

                                    <div id="msgnotification" class="collapse notifi_dropdown">
                                    <span class="text-left notifi_title">Messages<a href="#" class="viewlink">View All</a></span>
                                    <ul>
                                                <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                                                  <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                                                    <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                                                    <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                                                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                                            </ul>

                                    </div>
                                </li>
                                  <li class="header-icon">

                                    <a data-toggle="collapse" data-target="#notifications"><i class="fa fa-bell-o" aria-hidden="true"></i><span class="counter red">{{ $notificationCount }}</span></a>

                                    <div id="notifications" class="collapse notifi_dropdown">
                                    <span class="text-left notifi_title"  style="background-color:#cc3333;">Recent Notifications<a href="{{ url('/admin/notifications')}}" class="viewlink">View All</a></span>
                                    <ul>
                                         @if($listresult != ' ')
                                       @foreach($listresult as $userInfo)

                                        <li><img src="@if($userInfo->profile_image =='') {{ url('/public/admin/images') }}/gravtar.jpeg @else {{ url('public/images/')}}/profileimage/{{ $userInfo->profile_image }} @endif"><span><b>{{ $userInfo->name}}</b><br/>
                                        @if($userInfo->notificaton_type =='closerequest')
                                           <a href="/admin-closerequest"> {{ $userInfo->close_reason}} </a>
                                        @elseif($userInfo->notificaton_type =='booking') 
                                        <a href="/admin-orders">{{ $userInfo->booking_message}} </a>
                                        @else
                                          <a href="/admin-users">@if($userInfo->user_role =='Provider') New Provider Arrived @else New Taker Arrived @endif.</a>
                                       @endif</span>
                                        </li>
                                        @endforeach
                                        @endif

                                            </ul>

                                    </div>
                                </li>
                                 <li class="header-icon headerprflimg dropdown">
<a href="#" class="dropdown-toggle profile" data-toggle="dropdown" role="button" aria-haspopup="true">
    <img src="@if($admin_img=='') {{ url('/public/admin/images') }}/gravtar.jpeg @else {{ url('public/images/')}}/profileimage/{{ $admin_img }} @endif" class="headerprflimg"><span>{{ $admin_info->name }}<i class="fa fa-cog"></i></span></a>
<ul class="dropdown-menu profile_dropdown">
    <span class="text-left notifi_title"></span>
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Profile</a></li>
    <li><a href="{{ url('/admin/change-password/') }}">Change Password</a></li>
    <li><a href="{{ url('/admin/business') }}">Business Setting</a></li>
    <li><a href="/lockscreen">Lock Screen</a></li>
    <li><a href="{{ route('logout') }}">Logout</a></li>
  </ul></li>


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>
  $(document).ready(function(){
  	//var base_url = <?php url('/'); ?>
  	$( "#notifications" ).load( "/admin-dashboard/notification" , '#notifications');
        alert('hello');
   })

  </script>