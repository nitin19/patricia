@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="yelloback">
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box tablediv tablesection">
<div class="row marbot" style="position: inherit;">
  <div class="col-sm-12">
      <div class="media payment_box m-b">
        <!-- <div class="col-sm-10"> -->
        <div class="media-body button_side">
          <h3>Inactive Users List <span class="list_clr"> ({{ $inactiveusers->count() }})</span>
            <a href="{{url('/admin-inactiveusers/excel')}}" class="btn btn-primary" style="z-index: 9; position: relative;">Excel</a> 
            Show: 
            <select id="table-filter" class="providers-profiles" style="z-index: 9; position: relative;">
            <option value="">Select Option</option>
            <option>Blank User</option>
            <option>Sent Email User</option>
            </select> 
          </h3>
        </div>
        @include('layouts.notify-message')
      </div>

  </div>
</div>
        
 
 

 
  <table id="provider_table" class="table table-borderless heading-name" style="z-index: 9; position: relative;">
    <thead>
      <tr>
        <!-- <th>Sr No.</th> -->
        <th class="column3">Name</th>
        <th class="column4">Email</th>
        <!-- <th class="column2">Access</th> -->
        <th class="column2">Type</th>
        <th class="column2">Phone </th>
        <th class="column3">Address</th>
        <th class="column2">Email Count</th>
        <th class="column2 date">Date</th>
        <th class="column2">Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($inactiveusers as $user_info)
<?php
      $name = $user_info->name;
      $email = $user_info->email;
      $is_active = $user_info->is_active;
      $resend_ac_email = $user_info->resend_ac_email;
?>
<tr id="{{ $user_info->id }}" class="iuserstable tabledata">
      <!-- <td>{{ ++$count }}</td> -->
      <td class="column3">{{ $name }}</td>
      <td class="column4">{{ $email }}</td>
      <!-- <td class="column2">({{ $user_info->hd_password }})</td> -->
      <td class="column2">{{ $user_info->user_role }}</td>
      <td class="column2">{{ $user_info->phone }}</td>
      <td class="column3">{{ $user_info->city }}</td> 
      <td class="column2"><?php if($resend_ac_email == '0'){echo 'Blank User';} else { echo 'Sent Email User';} ?></td>
      <td class="column2"><?php if($user_info->resend_ac_email_date!='') { echo date('d M, Y h:i A', strtotime($user_info->resend_ac_email_date)); } ?></td> 
      <td class="column2 fourIcon"><a href="{{ url('/admin-users/show') }}/{{ $user_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="View" title="" class="fa fa-eye" style="font-size:16px"></i></a>
       <!--  <a href="{{ url('/admin-users/edit') }}/{{ $user_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Edit" title="" class="fa fa-pencil" style="font-size:16px"></i></a> -->
        <a class="deletebutton" style="cursor: pointer;"><i data-toggle="tooltip" data-placement="top"  data-original-title="delete" title="" class="fa fa-trash" style="font-size:16px"></i>
        <form method="POST" action="">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" name="delete" value="delete" class=" btn_del hidden" >
        </form></a>

        @if($user_info->is_active!='1')
        <a href="{{ url('/admin-inactiveusers/resendemail') }}/{{ $user_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Resend account activation email" title="" class="fa fa-envelope" style="font-size:16px"></i></a>

        <a href="{{ url('/admin-inactiveusers/activateuser') }}/{{ $user_info->id }}"><i data-toggle="tooltip" data-placement="top"  data-original-title="Activate user account" title="" class="fa fa-user" style="font-size:16px"></i></a>
        @else

        @endif
        
      </td>
</tr>
@endforeach
    </tbody>
  </table>

</div>
  </div>



<script type="text/javascript">
  // jQuery('.success_show').hide();
  jQuery('.deletebutton').click(function(){
    if(confirm("Are you sure?")){
      var userid = jQuery(this).closest('tr').attr('id');
          jQuery.ajax({
          url: "/admin/usersdestroy",
          type: 'POST',
          data: {'id': userid,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          // jQuery('.success_show').show();
          // jQuery('.success_show').html(response);
          $.notify({
              message: response,
              },{
              type: 'success',
              offset: 
              {
                x: 10,
                y: 130
              },
              animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
              },
            });
          jQuery('#'+userid).remove();
           // setTimeout(function() {
           //  $('.success_show').remove(); 
           //  }, 2000);
          }            
          });      
      }
      else{
        return false;
      }
});
</script>
 <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#provider_table').DataTable();
    $('#table-filter').on('change', function(){
       table.search(this.value).draw();   
    });
  } );
</script>
 <!-- <script type="text/javascript">
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#provider_table thead tr').clone(true).appendTo( '#provider_table thead' );
    $('#provider_table thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        //alert($(".date").text());
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
 
    var table = $('#provider_table').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );
} );
</script> -->
<style type="text/css">
@media screen and (min-width:320px) and (max-width: 767px){
  #provider_table_filter, #provider_table_paginate {
    position: relative;
    z-index: 9;
  }
}
  
</style>
@stop
