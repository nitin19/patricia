@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="content-wrap">
<section class="notificationpage">
        <div class="">
            <div class="">
                <div class="col-lg-12">
                    <div class="notificationhead wow fadeInDown">
                        <h1>Messages</h1>
                    </div>
                </div>

                <div class="notificationbox">

                    <div class="firstnotification wow animated fadeInLeft">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join">Join New user</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>New Registration: Finibus Bonorum et Malorum</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Allen Deu</h3>
                        </div>
                        <hr class="notiboxline">
                    </div>

                    <div class="firstnotification wow animated fadeInRight">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join mess">Message</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>Darren Smith sent new message</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Darren</h3>
                        </div>
                        <hr class="notiboxline">
                    </div>
                    <div class="firstnotification wow animated fadeInLeft">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join comm">Comment</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>Arin Gansihram Commented on post</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Arin Gansihram</h3>
                        </div>
                        <hr class="notiboxline">
                    </div>
                    <div class="firstnotification wow animated fadeInRight">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join conn">Connect</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>Jullet Den <label>Connect</label> Allen Depk</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Jullet Den</h3>
                        </div>
                        <hr class="notiboxline">
                    </div>
                    <div class="firstnotification wow animated fadeInLeft">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join comm">Comment</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>Arin Gansihram Commented on post</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Arin Gansihram</h3>
                        </div>
                        <hr class="notiboxline">
                    </div>

                    <div class="firstnotification wow animated fadeInRight">
                        <div class="crossbtnnoti">
                            <span><a href="javascript:void(0);"><img src="http://navizinhanca.com/public/admin/images/noticlose.png" alt="#"/></a></span>
                        </div>
                        <div class="notifidata">
                            <button class="btn btn-primary join mess">Message</button><span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> 24 Nov 2018 at 9:30 AM</span>
                            <h1>Darren Smith sent new message</h1>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
                            <h3>Darren</h3>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>
</div>
@stop
