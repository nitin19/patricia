@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>/* calender events customized css*/
.calendar {width: 650px !important;height: 319px;position: absolute;z-index: 9999;}
.prev, .next, .today{background-color:red;}
/* Main carousel style */
.carousel {width: 600px;}
/* Indicators list style */
.article-slide .carousel-indicators {bottom: 0;left: 0;margin-left: 5px;width: 100%;}
/* Indicators list style */
.article-slide .carousel-indicators li {border: medium none;border-radius: 0;float: left;height: 54px;margin-bottom: 5px;margin-left: 0;margin-right: 5px !important;margin-top: 0;width: 100px;}
/* Indicators images style */
.article-slide .carousel-indicators img {border: 2px solid #FFFFFF;float: left;height: 54px;left: 0;width: 100px;}
/* Indicators active image style */
.article-slide .carousel-indicators .active img {border: 2px solid #428BCA;opacity: 0.7;}

.pricing_table.usrpf>tbody>tr>td, .pricing_table.usrpf>tbody>tr>th, .pricing_table.usrpf>tr>td, .pricing_table.usrpf>tr>th {
width: 100% !important;
padding: 4px;
}

</style>
<section class="maincontent searchbarbg">
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
    <div class="message"></div>
    <div class="bannerimg">
      <div class="container">
      	<div class="col-sm-2 profile_img">
          <?php if($user_info->profile_image!=''){ ?>
            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $user_info->profile_image; ?>" class="profimg mr">
            <?php } else { ?>
            <img src="{{ url('/public/') }}/images/profileimage/1542707849.images.png" class="profimg my-css">
            <?php }
            $category_name = explode(",", $user_info->category);
            $current_userid = Auth::id();
            /*$current_username = Auth::user()->name;*/
            $user_id = $user_info->id;
            //$current_user_role = Auth::user()->user_role;
            $uaddrs = $user_info->address;
            $pieces = explode(",", $uaddrs);
            ?>
        </div>
        <div class="col-sm-6 profileinfo">
          <h2>{{ $user_info->category }}</h2>
          <p class="mobile_section"><b>{{ ucfirst($user_info->name) }}</b></p>
          <p><b class="desktop_section">{{ ucfirst($user_info->name) }}</b> <span>
            <?php 
            if(count($userreview) > 0) {
            if($user_info->profile_rating != ''){
              $number = number_format($user_info->profile_rating,1);
              $integer_part = floor($number);
              $fraction_part = $number-$integer_part;
              for($x=1;$x<=$number;$x++) {?>
                <i class="fa fa-star"></i>
                <?php } if (strpos($number,'.')) { if($fraction_part != 0) { ?>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                  <?php  $x++; } }
              while($x<=5){?>
               <i class="fa fa-star-o" aria-hidden="true"></i>
               <?php $x++; } } }?>
           </span><span class="reviews">@if(count($userreview) > 0) ({{count($userreview)}} @if(count($userreview) > '1' ) Avaliações @else Avaliação @endif) @else (No Review) @endif</span></p>
           <p class="prflinfo"><span>Categoria: </span>{{$user_info->main_cat_name}}</p>
           <p class="prflinfo"><span>Serviço: </span>{{$user_info->cat_name}}</p>
           <p class="prflinfo"><span>Distância: </span>{{number_format($user_info->distances,2,',','.')}} Km</p>
           <p class="prflinfo"><span>Bairro: </span> {{ucfirst($user_info->neighborhood) }}</p>
           <p class="prflinfo"><span>Cidade: </span> {{ucfirst($user_info->city) }}</p>
           @if($user_id != $current_userid)
           <div class="profilebtns">
            <!-- <input type="hidden" name="booking_status" id="booking_status" value="@if(!empty($check_existing_booking)){{$check_existing_booking->booking_verified}}@endif"> -->
            <a href="{{url('/login')}}?{{ $user_info->id }}" class="yellowbtn prflbtn marbot"><i class="fa fa-paper-plane"></i>Contatar</a>
            <a href="{{url('/login')}}?{{ $user_info->id }}" class="prflbtn <?php if($wish_list_exist>0){ echo 'removewhishlist_profile'; } else { echo 'addwhishlists'; } ?>" value="{{ $user_info->id }}" id="whishlist_id"><input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}"><i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o'; } ?>" value="{{ $user_info->id }}"></i>@if($wish_list_exist>0) Adicionado na sua lista de seleção @else Adicionar na lista de seleção @endif</a>

            <!-- <div class="prflbtn <?php //if($wish_list_exist>0){ echo 'removewhishlist_profile'; } else { echo 'addwhishlists'; } ?>" value="{{ $user_info->id }}" id="whishlist_id" >
            <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
            <i class="fa <?php //if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o'; } ?>" value="{{ $user_info->id }}"></i>@if($wish_list_exist>0) Adicionado na sua lista de seleção @else Adicionar na lista de seleção @endif 
            </div> -->
            <a href="{{url('/login')}}?{{ $user_info->id }}" class="yellowbtn prflbtn marbot" data-toggle="modal" data-target="#blackuserModal" data-whatever="@mdoblk"><i class="fa fa-bug" ></i>Solicitar bloqueio</a>
            <?php
              $paypal_email = 'ravi@49webstreet.com';
              $base_url = url('/');
              $return_url =   $base_url.'/success';
              $cancel_url =  $base_url.'/failure';
            ?>
           </div>
           @endif

        </div>
        <!-- end alert modal -->
          <div class="col-sm-4 durationdiv">
            <div class="singlepagesidebar">
              @if(count($provider_price_lists) > 0)
              <h3>Preços</h3>
              <table class="table pricing_table usrpf">
                <tbody>
                  @foreach($provider_price_lists as $provider_price_list)
                  <tr>
                      <th>{{$provider_price_list->queries}}</th>
                      <td>&#82;&#36;{{number_format($provider_price_list->amount,2,",",".")}}</td>
                  </tr>
                  @endforeach  
                </tbody>
              </table>
              @endif
            </div>
           
          </div>
       </div>
      </div>
      <div class="singlepagecontent">
        <div class="container">
          <div class="col-sm-8 singlepageleft">
          	<h2>Sobre {{ ucfirst($user_info->name) }}</h2>
            <p> <?php echo htmlspecialchars_decode($user_info->bio); ?></p>
            <?php $count_user_gallery = count($user_gallery); $active_count = 1;
            if($count_user_gallery>0) { ?>
            <div class="carousel slide article-slide carousel slide singlepageslider" id="article-photo-carousel" >
              <div class="carousel-inner cont-slider">
                <?php foreach($user_gallery as $user_gallery_img){ ?>
                <div class="item <?php if($active_count==1) { echo 'active'; } ?>">
                  <a href="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" data-lightbox="user-gallery"><img src="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" alt="..."></a>
                </div>
                <?php ++$active_count; } ?>
              </div>
              <ol class="carousel-indicators user_glry_slider">
                <?php $count_user_gallery = count($user_gallery); $active_count = 1;
                  $i=-1; if($count_user_gallery>0){ foreach($user_gallery as $user_gallery_img){ ?>
                <li class="active" data-slide-to="<?php echo $i=$i+1;?>" data-target="#article-photo-carousel">
                  <div class="item <?php if($active_count==1) { echo 'active'; } ?>">
                    <img src="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" alt="...">
                  </div>
                </li>
                <?php ++$active_count;} } ?>
              </ol>
              <?php if($count_user_gallery>1){ ?>
              <a class="left carousel-control" href="#article-photo-carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#article-photo-carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <?php  } ?>
            </div>
            <?php } ?> 
            <div class="singleprofile_listing">
            	<h3>Cursos e certificados</h3>
            	<p><?php echo $user_info->additional_details;?></p>
          	</div>
          	<div class="singleprofile_listing">
            	<h3>Dados gerais</h3>
            	<ul class="listingitem2">
              		<li>Clientes repetidos: <span>{{ $repeat_clients }}</span></li>
              		<li># de serviço(s) confirmados: <span>{{ $complete_booking }}</span></li>
              		<li>Taxa de resposta: <span>100%</span></li>
              		<li>Tempo de resposta: <span>em poucas horas</span></li>
            	</ul>
          	</div>
          	<div class="singleprofile_listing">
          			<h3>Horário</h3>
            		<a href="javascript:void(0);"><h4 class="view_cal"></h4></a>
            		<div class="event-calendar"></div>
	            	<ul class="listingitem1">
	                @if($get_user_availablity->isCompleted=='1')
	                  @if($get_user_availablity->openhrs=='1')
	                    <li> @if($get_user_availablity->day_monday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Segunda-feira:
	                      @if($get_user_availablity->day_monday=='1')
	                      {{$get_user_availablity->monday_start}} - {{$get_user_availablity->monday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_tuesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Terça-feira:
	                      @if($get_user_availablity->day_tuesday=='1')
	                      {{$get_user_availablity->tuesday_start}} - {{$get_user_availablity->tuesday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_wednesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quarta-feira:
	                      @if($get_user_availablity->day_wednesday=='1')
	                      {{$get_user_availablity->wednesday_start}} - {{$get_user_availablity->wednesday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_thursday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quinta-feira:
	                      @if($get_user_availablity->day_thursday=='1')
	                      {{$get_user_availablity->thursday_start}} - {{$get_user_availablity->thursday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_friday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sexta-feira:
	                      @if($get_user_availablity->day_friday=='1')
	                      {{$get_user_availablity->friday_start}} - {{$get_user_availablity->friday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_saturday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sábado:
	                      @if($get_user_availablity->day_saturday=='1')
	                      {{$get_user_availablity->saturday_start}} - {{$get_user_availablity->saturday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                    <li>@if($get_user_availablity->day_sunday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Domingo:
	                      @if($get_user_availablity->day_sunday=='1')
	                      {{$get_user_availablity->sunday_start}} - {{$get_user_availablity->sunday_end}}
	                      @else
	                      Fechado
	                      @endif
	                    </li>
	                  @else
	                    <li><i class="fa fa-check-circle"></i>Segunda-feira 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Terça-feira 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Quarta-feira 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Quinta-feira 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Sexta-feira 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Sábado 00:00 - 23:59</li>
	                    <li><i class="fa fa-check-circle"></i>Domingo 00:00 - 23:59</li>
	                  @endif
	                @else
	                  <li><i class="fa fa-times-circle"></i>No availability set by {{$get_user_availablity->name}}</li>
	                </ul>
	                @endif
	             </ul>
            	</div>
          	   </div>
          	   <div class="col-sm-4 singlepageright">
            	@if(count($similar_category_users)>0)
            		<div class="singlepagesidebar">
          				<h3>Similar {{ lcfirst($user_info->main_cat_name) }} </h3>
          				@foreach($similar_category_users as $cat_info)
            				@if($cat_info->id != $id)
              					<div class="babysitter_div">
				                    <div class="babysitter_img ">
				                      <?php if($cat_info->profile_image!=''){ ?>
				                        <img src="{{ url('/public/images/profileimage/') }}/<?php echo $cat_info->profile_image; ?>" class="profimg my-css">
				                        <?php } else { ?>
				                          <img src="{{ url('/public/images/profileimage/1542707849.images.png') }}" class="profimg my-css">
				                      <?php } ?>
				                    </div>
                					<div class="babysitter_text my-css-2">
          								<h4>
          									<!-- <a href="{{url('/profile/'.$cat_info->id)}}">{{ucfirst($cat_info->name) }}</a> -->
          									<a href="{{url('/userprofile/'.$cat_info->id)}}">{{ucfirst($cat_info->name) }}</a>
          								</h4>
          								<?php   
          								$interger_part = ''; $fraction_part= '';
          							    $uaddrs1 = $cat_info->address;
          							    $pieces1 = explode(",", $uaddrs1);
          							    if(!empty($cat_info->profile_rating)){
                							$number = number_format($cat_info->profile_rating,1);
                							$integer_part = floor($number);
                							$fraction_part = $number-$integer_part;
                							for($x=1;$x<=$number;$x++) {?>
                  								<i class="fa fa-star"></i>
                							<?php }
                							if (strpos($number,'.')) {
                  								if($fraction_part != 0) {?>
                   									<i class="fa fa-star-half-o" aria-hidden="true"></i>
                   								<?php 
                   									$x++;
                 								}
               								}
                							while ($x<=5) {?>
                  								<i class="fa fa-star-o" aria-hidden="true"></i>
                  							<?php
                 								$x++;
                							}
          								} else{
					                        for($x=1;$x<=5;$x++){?>
					                        <i class="fa fa-star-o" aria-hidden="true"></i>
					                        <?php  }
          								}
				                        if(!empty($get_feedbacks)){
				                        	foreach($get_feedbacks as $u=>$f){
				                          		if($cat_info->id == $u)
				                            		echo "(".$f.")";
				                        	}
				                      	} ?>
          								<h5>{{ucfirst($cat_info->subcat_name) }}</h5>
          								<h5>{{ucfirst($cat_info->city) }} ({{number_format($cat_info->distances,2,",",".")}} km)</h5>
          								<h5>{{ $cat_info->price_des }} &#82;&#36;{{number_format($cat_info->amount,2,",",".")}}  @if($cat_info->price_type!='') {{ $cat_info->price_type }} @endif </h5>
                					</div>
              					</div>
            				@endif
          				@endforeach
            		</div>
            	@endif
               </div>
               <div class="singleprofile_listing">
            		<h3>Comentário dos contratantes (@if(count($userreview) > 1){{count($userreview).' comentários' }} @elseif(count($userreview) == 1) {{ '1 comentário' }}@else 0 comentário @endif )</h3>
            		<div class="review_section">
              			@if(count($userreview) > 0)
              				@foreach($userreview as $feedback)
                				<?php 
                 					$fmt = datefmt_create(
                  						'pt-BR',
					                	IntlDateFormatter::FULL,
					                	IntlDateFormatter::FULL,
					                	'Brazil/East',
					                	IntlDateFormatter::GREGORIAN,
					                	"dd/MMM/YYYY"  
                   					);
                    				//notification time
                  					$dt = new DateTime($feedback->created_at. "+1 days");
				                    $d = $dt->format('d');
				                    $m = $dt->format('m');
				                    $y = $dt->format('Y');
				                    $startdate = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                   				?>
              					<div class="reviewblk">
                					<div class="row">
					                    <div class="clientimg">
					                    	<img class="user_image" src="@if(!empty($feedback->profile_image)) {{ url('/public/images/profileimage/'.$feedback->profile_image) }}@else {{url('/public/images/gravtar.jpeg')}} @endif">
					                    </div>
                  						<div class="client_text nopadding">
                    						<h4 class="user_name">{{$feedback->name}}<span>
							                    <?php 
							                        for($i=0; $i<$feedback->rating; $i++){
							                          echo '<i class="fa fa-star" aria-hidden="true"></i>';
							                        }
							                    ?>
                     						  </span>
                     						</h4>
                      						<h5 class="feedback_date">{{$startdate}}</h5>
                      						<p class="feedback_message" id="msgrow{{$feedback->id}}"><?php if(strlen($feedback->message) > 200){ echo substr($feedback->message, 0, 200). '<span class="dots" id="dotmsg'.$feedback->id.'">...</span><span class="readmore" id="moremsg'.$feedback->id.'" style="display:none;">'.substr($feedback->message, 200, strlen($feedback->message)).'</span><span class="btnmore">Veja mais</span><span class="btnless" style="display:none;">Veja menos</span>';}else{ echo $feedback->message;} ?>
                      						</p>  
                    						<input type="hidden" name="page" value="2" id="page_number">
                 						</div>
               						</div>
             					</div>
            				@endforeach
            			@else
              				<h2 align="center"> Nenhum comentário feito pelos contratantes para esse prestador de serviço.</h2>
            			@endif
          			</div>
          			<div class="loadbtn">
            			<?php if(count($feedbacks)==5){?>
              				<a data-id="{{$user_info->id}}" class="load_more">Load More</a>
            			<?php }?>
          			</div>
        	   </div>
        	</div>
    	</div>
    	<div id="map" style="width: 100%; height: 400px; background: #5497b7" />
    	<style>
		    #proPhoneModal .modal-content { border: 12px solid #d5d7d8;border-radius: 5px;padding: 30px 0;}
		    .provider_price { display: block; width: 100%;text-align: center;font-weight: bold;font-size: 20px;}
		    .fa-phone {-webkit-animation: wrench 2s infinite;animation: wrench 2s infinite;color: #1B9BD8;font-size: 18px;}
		    @-webkit-keyframes wrench {
		        0% { -webkit-transform: rotate(-12deg);transform: rotate(-12deg);}
		        8% {-webkit-transform:  rotate(12deg);transform: rotate(12deg);}
		      10% { -webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      18% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg); }
		      20% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
		      28% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      30% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      38% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
		      40% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
		      48% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      50% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      58% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
		      60% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
		      68% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
		      75% {-webkit-transform: rotate(0deg);transform: rotate(0deg);}
		    }
    	</style>
    	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&callback=initMaps"></script>
    	<script  type="text/javascript" charset="UTF-8" >
    		/* carousel script for user gallery*/
	        $('.carousel').carousel({
	        	interval: 2000,
	        	cycle:true
	      	});
		    var lat = '<?php echo $user_info->latitude; ?>';
		    var lan = '<?php echo $user_info->longitude; ?>';
		    var center = new google.maps.LatLng(lat, lan);     
      		function initMaps() {
          		// MAP ATTRIBUTES.
		        var mapAttr = {
		            center: center,
		            zoom: 14.5,
		            zoomControl: false,
		            gestureHandling: 'none',
		            mapTypeId: google.maps.MapTypeId.TERRAIN
		        };
          		// THE MAP TO DISPLAY.
          		var map = new google.maps.Map(document.getElementById("map"), mapAttr);
          		var circle = new google.maps.Circle({
	                center: center,
	                map: map,
	                radius: 1200,          // IN METERS.
	                fillColor: 'rgba(0, 128, 0, 0.7)',
	                fillOpacity: 0.5,
	                strokeColor: "rgba(55, 85, 170, 0.6)",
	                strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
          		});
      		}
      		google.maps.event.addDomListener(window, 'load', initMaps);
    	</script>
    </div>
</section>
<style type="text/css">
.greybtn_anchr {background: #696969 !important;color: #fff;background: red none repeat scroll 0 0;border-radius: 5px;box-shadow: 1px 3px 12px 0 #919191;font-family: Lato;font-size: 16px;font-weight: bold;margin-right: 10px;padding: 10px 25px;}
.prflbtn { float: left; cursor: pointer; }
.payment_type { color: #898989; font-size: 20px; }
.vlderror { border: 2px solid red !important; }
#bokngSuccess { opacity: 1 !important; }
#bokngError { opacity: 1 !important; }
.article-slide .carousel-indicators {
    overflow: -webkit-paged-x;
    display: flex;
}
.article-slide .carousel-indicators li {
    height: 100px;
    width: 100px;
}
.article-slide .carousel-indicators img {
    height: 100px;
    width: 100px;
}
@media screen and (max-width:991px) {
  .greybtn_anchr {padding: 10px 15px;font-size:14px;}
  .profilebtns a {padding: 10px 15px;font-size: 12px;}
  .profilebtns a i {font-size: 12px;}
}
label.radio_btn_label {
    font-weight: 100;
}
.datesec{
  text-align: left;
}

/*.radio_btn_label .checkmark redme:after {
  top:5px  !important;
  left:5px !important;
}*/
.checkmark .redme {
    background-color: #ccc !important;
    padding: 8px;
}
/*.radio_btn_label input:checked~.checkmark redme {
    border: 5px solid #39c;   
}*/
.buttondiv {
    text-align: center;
}
.checkmark {
    position: absolute;
    top: 2px;
    left: 0;
    background-color: #ccc !important;
    border-radius: 50%;
    padding: 8px;
}
  .radioradio {
    display: inline-block;
    margin-right: 10px;
}
.checkmark {
    top: 6px;
}
</style>
@stop