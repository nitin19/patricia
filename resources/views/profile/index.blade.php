@extends('layouts.default-header')

@section('title', 'Profile')

@section('content')
<section class="maincontent searchbarbg"> 
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
    <div class="bannerimg">
      <div class="container">
        <div class="col-sm-2 profile_img">
      <img src="{{ url('/public/') }}/images/prflimg.png">
    </div>
    <div class="col-sm-8 profileinfo">
      <h2>Home from Home with The Coopers</h2>
      <p><b>Laura</b> <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span><span class="reviews">(33 reviews)</span></p>
      <p class="prflinfo"><span>Address:</span> 490 E South. Orlando, Florida USA 140119,</p>
      <p class="prflinfo"><span>Distance</span> 0.7 mi</p>
      <div class="profilebtns">
        <a href="#" class="yellowbtn prflbtn" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-paper-plane"></i> BOOK</a>
<?php 
$user_id = $user_info->id;
   $wish_list_exist = DB::table('wish_list')->where('user_id', $user_id)->count();
   if($user_id!=''){
    ?>
    <div class="greybtn_anchr prflbtn <?php if($wish_list_exist>0){ echo ''; } else { echo 'addwhishlist'; } ?>" value="{{ $user_info->id }}"" id="whishlist_id">
   <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
   <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o'; } ?>" value="{{ $user_info->id }}"></i>Add to Wishlist</div>
   <?php
 }
   else {
    ?>
    <a href="{{ url('/login/') }}" class="greybtn prflbtn">
    <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>Add to Wishlist</a>
    <?php
   }
?>
          <a href="#" class="greybtn prflbtn"><i class="fa fa-envelope"></i> MESSAGE</a>
      </div>

      <!--modal-->
      <div class="modalsection red">
        <div class="modal fade booknow_popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <h1 class="text-center">Book With Tasha</h1>
        <h4 class="text-center"> Tasha will need basic dog details. please fill below</h4>
      <div class="modal-body">
        <form>
          <div class="row">
          <div class="col-sm-6">
          <div class="form-group datesec">
            <input type="text" class="form-control" id="startdate" placeholder="Select Start Date">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group datesec">
            <input type="text" class="form-control" id="startdate" placeholder="Select End Date">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 datesec">
          <div class="form-group">
          <select>
            <option>4 Dogs</option>
            <option>5 Dogs</option>
            <option>6 Dogs</option>
            <option>7 Dogs</option>
          </select>
        </div>
        </div>
      </div>
            <div class="row">
        <div class="col-sm-12">
          <div class="form-group datesec">
          <textarea rows="6" placeholder="Write your message here"></textarea>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="bookingamt_sec">
            <div class="col-sm-12 amtsec">
            <div class="col-sm-8 amtdetails">
              <p>1 dog × £25/night × 1 night:</p>
            </div>
            <div class="col-sm-4 amountdiv">
              <span>£25.00</span>
            </div>
          </div>
           <div class="col-sm-12 amtsec">
            <div class="col-sm-8 amtdetails">
              <p>+ 3 extra dog × £20/night
× 1 night: </p>
            </div>
            <div class="col-sm-4 amountdiv">
              <span>£60.00</span>
            </div>
          </div>
           <div class="col-sm-12 amtsec">
            <div class="col-sm-8 amtdetails">
              <p>Service Fee:</p>
            </div>
            <div class="col-sm-4 amountdiv">
              <span>£6.00</span>
            </div>
          </div>
           <div class="col-sm-12 totalamtdiv amtsec">
            <div class="col-sm-8 amtdetails">
              <p>Total Amount</p>
            </div>
            <div class="col-sm-4 amountdiv">
              <span>£91.75</span>
            </div>
          </div>
          </div>
        </div>
      </div>
      <div class="booknowbtn">
        <input type="button" class="btn" value="Book Now">
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
      </div>



    </div>
    <div class="col-sm-2 durationdiv">
      <h2>$17/hours</h2>
    </div>
    </div>
    </div>

    <div class="singlepagecontent">
       <div class="container">
        <div class="col-sm-8 singlepageleft">
          <h2>About Laura</h2>
          <p>My Partner and I have been raised with dogs since birth, we have 2 dogs of our own- lana who's a Spoodle and Winston a Jackahuahua! </p>
          <div id="carousel-example-generic" class="carousel slide singlepageslider" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{ url('/public/') }}/images/sliderimg.jpg" alt="...">
    </div>
    <div class="item">
      <img src="{{ url('/public/') }}/images/sliderimg.jpg" alt="...">
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<p>They love to cuddle up on the sofa with us, and they often try and sneak in bed with us when they think we're not looking! (With faces like theirs it's hard to tell them they're not allowed!) 
<br/><br/>
We welcome all dogs and cannot wait to look after yours! We are very passionate about all dog's, your dogs will feel very comfortable with us, and if they are allowed on the sofas in their own home then they are more than welcome to join us on ours. </p>
<div class="singleprofile_listing">
<h3>Cradentials</h3>
<ul class="listingitem1">
  <li><i class="fa fa-check-circle"></i>Address Verified</li>
  <li><i class="fa fa-check-circle"></i>Passed assessment</li>
  <li><i class="fa fa-times-circle"></i>Approved by the Admin</li>
</ul>
<ul class="listingitem2">
  <li>Repeat clients: <span>13</span></li>
  <li>Completed bookings: <span>58</span></li>
  <li>Response rate: <span>100%</span></li>
  <li>Response time: <span>within a few hours</span></li>
  <li>Cancellation policy for other services:  <span>Flexible</span></li>
</ul>
  </div>
  <div class="singleprofile_listing">
<h3>Availability</h3>
<h4>View Laura's Calendar</h4>
<p>If you have special time requirements, simply let Helen know.</p>
<h5>Babysitter preferred times:</h5>
<ul class="listingitem1">
  <li><i class="fa fa-check-circle"></i>Drop-off from noon</li>
  <li><i class="fa fa-check-circle"></i>Pick-up before 6 p.m.</li>
</ul>

  </div>
    <div class="singleprofile_listing">
<h3>Additional information</h3>
<ul class="listingitem1">
  <li><i class="fa fa-check-circle"></i>I have a car for emergencies</li>
  <li><i class="fa fa-check-circle"></i>I can administer oral medications</li>
   <li><i class="fa fa-check-circle"></i>I have children under 18 in the household</li>
</ul>

  </div>

        </div>
           <div class="col-sm-4 singlepageright">
            <div class="singlepagesidebar">
              <h5>All Babysitter include:</h5>
              <ul>
                <li><img src="{{ url('/public/') }}/images/babysittericon1.png">24/7 Emergency support & cancellation protection</li>
                  <li><img src="{{ url('/public/') }}/images/babysittericon2.png">Up to 1.7M pet & liability insurance</li>
                    <li><img src="{{ url('/public/') }}/images/babysittericon3.png">Free GPS tracked walks</li>
                      <li><img src="{{ url('/public/') }}/images/babysittericon4.png">Photo & video updates</li>
              </ul>
            </div>
            <div class="singlepagesidebar">
              <h3>Similar Babysitters</h3>
              <div class="babysitter_div">
                <div class="babysitter_img">
                  <img src="{{ url('/public/') }}/images/prflimg.png">
                </div>
                <div class="babysitter_text">
                 <h4>Happy , Fun and Lots of Cuddles 
Dog Boarding</h4>
<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
<i class="fa fa-star"></i><i class="fa fa-star"></i></span>
<h5>DE21, Derby</h5>
<h5>£20 per night</h5>
                </div>

              </div>
              <div class="babysitter_div">
                <div class="babysitter_img">
                  <img src="{{ url('/public/') }}/images/prflimg.png">
                </div>
                <div class="babysitter_text">
                 <h4>Happy , Fun and Lots of Cuddles 
Dog Boarding</h4>
<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
<i class="fa fa-star"></i><i class="fa fa-star"></i></span>
<h5>DE21, Derby</h5>
<h5>£20 per night</h5>
                </div>
                
              </div>
              <div class="babysitter_div">
                <div class="babysitter_img">
                  <img src="{{ url('/public/') }}/images/prflimg.png">
                </div>
                <div class="babysitter_text">
                 <h4>Happy , Fun and Lots of Cuddles 
Dog Boarding</h4>
<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
<i class="fa fa-star"></i><i class="fa fa-star"></i></span>
<h5>DE21, Derby</h5>
<h5>£20 per night</h5>
                </div>
                
              </div>
              <div class="babysitter_div">
                <div class="babysitter_img">
                  <img src="{{ url('/public/') }}/images/prflimg.png">
                </div>
                <div class="babysitter_text">
                 <h4>Happy , Fun and Lots of Cuddles 
Dog Boarding</h4>
<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
<i class="fa fa-star"></i><i class="fa fa-star"></i></span>
<h5>DE21, Derby</h5>
<h5>£20 per night</h5>
                </div>
                
              </div>
              <div class="babysitter_div">
                <div class="babysitter_img">
                  <img src="{{ url('/public/') }}/images/prflimg.png">
                </div>
                <div class="babysitter_text">
                 <h4>Happy , Fun and Lots of Cuddles 
Dog Boarding</h4>
<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
<i class="fa fa-star"></i><i class="fa fa-star"></i></span>
<h5>DE21, Derby</h5>
<h5>£20 per night</h5>
                </div>
                
              </div>

            </div>
             <div class="singlepagesidebar faqsec">
              <h3>FAQs</h3>
              <ul>
                <li><a href="#">What is a babysitter?</a></li>
                   <li><a href="#">How does it works?</a></li>
                    <li><a href="#">What services are available?</a></li>
                      <li><a href="#">How do I book?</a></li>
                      <li><a href="#">Can I meet the babysitter before the booking starts?</a></li>
                      <li>More questions? Check all our <a href="#">FAQs</a></li>
              </ul>
            </div>
        </div>
   <div class="singleprofile_listing">
<h3>Clients Reviews(36 reviews)</h3>
<div class="review_section">
<div class="reviewblk">
  <div class="row">
  <div class="clientimg">
    <img src="{{ url('/public/') }}/images/clientimg1.png">
  </div>
  <div class="client_text nopadding">
    <h4>Adele</h4>
    <p>Milo had another lovely day today. Helen and her family are very supportive and I feel very comfortable leaving Milo with them. He’s having a good sleep on the sofa right now</p>
    <h5>18th September 2018</h5>
    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
    <i class="fa fa-star"></i><i class="fa fa-star"></i></span>
  </div>
</div>
</div>
<div class="reviewblk">
  <div class="row">
  <div class="clientimg">
    <img src="{{ url('/public/') }}/images/clientimg1.png">
  </div>
  <div class="client_text nopadding">
    <h4>Adele</h4>
    <p>Milo had another lovely day today. Helen and her family are very supportive and I feel very comfortable leaving Milo with them. He’s having a good sleep on the sofa right now</p>
    <h5>18th September 2018</h5>
    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
    <i class="fa fa-star"></i><i class="fa fa-star"></i></span>
  </div>
</div>
</div>
<div class="reviewblk">
  <div class="row">
  <div class="clientimg">
    <img src="{{ url('/public/') }}/images/clientimg1.png">
  </div>
  <div class="client_text nopadding">
    <h4>Adele</h4>
    <p>Milo had another lovely day today. Helen and her family are very supportive and I feel very comfortable leaving Milo with them. He’s having a good sleep on the sofa right now</p>
    <h5>18th September 2018</h5>
    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
    <i class="fa fa-star"></i><i class="fa fa-star"></i></span>
  </div>
</div>
</div>
<div class="reviewblk">
  <div class="row">
  <div class="clientimg">
    <img src="{{ url('/public/') }}/images/clientimg1.png">
  </div>
  <div class="client_text nopadding">
    <h4>Adele</h4>
    <p>Milo had another lovely day today. Helen and her family are very supportive and I feel very comfortable leaving Milo with them. He’s having a good sleep on the sofa right now</p>
    <h5>18th September 2018</h5>
    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
    <i class="fa fa-star"></i><i class="fa fa-star"></i></span>
  </div>
</div>
</div>
</div>
<div class="loadbtn">
  <a href="#">Load More</a>
</div>


  </div>


  </div>
</div>
  <div class="profilemap_img">
          <img src="{{ url('/public/') }}/images/profilemap-img.jpg" class="img-responsive">
        </div>
</section>
<style type="text/css">
.greybtn_anchr {
    background: #696969 !important;
    color: #fff;
    background: red none repeat scroll 0 0;
    border-radius: 5px;
    box-shadow: 1px 3px 12px 0 #919191;
    font-family: Lato;
    font-size: 18px;
    font-weight: bold;
    margin-right: 10px;
    padding: 15px 30px;
}
.prflbtn {
    float: left; cursor: pointer;
}
</style>
@stop