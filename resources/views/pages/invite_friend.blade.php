@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<section class="maincontent searchbarbg">
  <div class="col-sm-12 pagecontent whitebgdiv nopadding nomap">
    <div class="invitepagecontent">
      <div class="container">
        <div class="col-sm-12 invitepage_div centered maxmax-width">
          <h1 class="text-center">Invite A Friend and Get Paid</h1>
          <form class="form-inline inviteform" id="inviteform">
            <div class="form-group col-sm-9 nopadding">
              <input type="text" class="form-control" id="myInput" value="{{ url('/') }}/referral/{{ $user_info->referal_code }}" readonly>
            </div>
            <div class="col-sm-3 nopadding copybtn">
              <input type="button" class="btn btn-default" id="copy_referal" value="Copy"  onclick="myFunction_copy()">
            </div>
          </form>
          <div class="invite-img">
            <img src="{{ url('/public/') }}/images/inviteimg.jpg">
          </div>
          <div class="getfrndsec">
            <h2>Your friends get a £10 discount</h2>
            <h3>You get £10 off your next booking!</h3>
            @include('layouts.flash-message')
            <form class="form-inline getfriendform" id="getfriendform" method="post" action="{{ url('/invite-friend') }}/send">
              {{ csrf_field() }}
              <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="Enter Email address" name="email" required>
                <input type="hidden" name="referal_link" value="{{ url('/') }}/referral/{{ $user_info->referal_code }}">
              </div>
              <div class="sendbtn">
                <input type="submit" class="btn btn-default" value="Send">
              </div>
            </form>
            <div class="getbaldiv">
              <h1>£0.00</h1>
              <h5>Current Cradit Balance to Spend</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
  window.setTimeout(function() {
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
  $(this).remove();
  });
  }, 4000);
  </script>
  @stop