@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<div class="singleservice">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 align="center">{{ucfirst($category_info->title)}}</h3>
				<div class="Service_img hhh">
					{!!html_entity_decode($category_info->description)!!}
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
.singleservice {
    margin: 15px 0px 60px 0px;
}
.singleservice h3 {
    font-size: 24px;
    font-weight: 600;
    margin-bottom: 24px;
}
	.Service_img img {
	    width: 100%;
	    height: 350px;
	    object-fit: cover;
	}
</style>
@stop