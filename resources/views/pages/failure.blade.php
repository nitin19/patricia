@extends('layouts.default-header')

@section('title', 'Failure')

@section('content')
<section class="maincontent searchbarbg"> 
  <div class="col-sm-12 pagecontent nopadding">
    <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
      <h1>Sorry! Your payment was processed failure. Please try again.</h1>
    </div>
    </div>
    </div>
    
  </div>
</section>
@stop