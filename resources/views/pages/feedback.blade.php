@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
@if(Auth::check() && empty(Auth::user()->address) && empty(Auth::user()->phone))
<div class="alert alert-danger alert-dismissible fade in profle_alert">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Alert!!</strong> please Complete Your Profile.
</div>
@endif
<script>
window.setTimeout(function() {
$(".alert").fadeTo(500, 0).slideUp(500, function(){
$(this).remove();
});
}, 4000);
</script>
<div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
          <h1>Contate-nos</h1>
        </div>
      </div>
</div>
<div class="contact_box">
  <div class="container">
    <div class="form_section">
      <div class="row">
        <div class="col-sm-7 content_box fullwidth marbot">
          <h2>Contate a equipe do NaVizinhança agora mesmo!</h2>
          <p>Digite abaixo seus dados e mensagem que retornaremos assim que possível.</p>
          @include('layouts.flash-message')
          <form class="contactform" id="contactform" method="post" action="{{ url('/contact/send') }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-md-6">
              <input class="form-control" name="first_name" placeholder="Primeiro nome" class="form-control" type="text" required>
            </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control" name="last_name" placeholder="Sobrenome">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <input class="form-control" name="email" id="email" placeholder="E-mail" class="form-control" type="text" required>
            </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control"  name="phonenumber" id="phonenumber" placeholder="Telefone celular. Ex.: (11)91111-1111" maxlength="14" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');" required>
            </div>
            <div class="form-group col-md-12">
              <textarea id="comment" placeholder="Mensagem" name="comment" cols="40" rows="7" placeholder="" class="form-control" required></textarea>
            </div>
          </div>
          <div class="form-button">
            <button type="submit" class="btn btn-box">Enviar mensagem</button>
          </div>
        </form>
      </div>
      <div class="col-md-5  content_map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5168725.743302812!2d-53.26910942763851!3d-22.048141022420687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce597d462f58ad%3A0x1e5241e2e17b7c17!2sS%C3%A3o+Paulo%2C+Brasil!5e1!3m2!1spt-PT!2sin!4v1554966872441!5m2!1spt-PT!2sin" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
        
        <div class="single contact-info contact-email">
          <ul class="list-unstyled">
            <!--<li>-->
            <!--<div class="icon"><i class="fa fa-map-marker"></i></div>-->
            <!--<div class="info"><p>P.O. Box 532, 3225 Lacus. Avenue <br>-->
            <!--69761, Stockholm</p></div>-->
            <!--</li>-->
            <!--<li>-->
            <!--<div class="icon"><i class="fa fa-phone"></i></div>-->
            <!--<div class="info"><p>1800–419–7713</p><br></div>-->
            <!--</li>-->
            <li>
              <div class="icon"><i class="fa fa-envelope"></i></div>
              <div class="info"><p><a href="mailto:contato@navizinhanca.com">contato@navizinhanca.com</a></p><br></div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$('.place-name').css('display', 'none');
$('.star-entity-text').css('display', 'none');
/***********************/
//for phone format

document.getElementById("phonenumber").onkeyup = function(){
var phone = this.value.length;

if (phone = 14) {
//alert(phone);
this.value = this.value.replace(/^(\d{2})(\d{5})(\d{4}).*/, '($1)$2-$3');
} else {
//alert(phone);
this.value = this.value.replace(/^(\d{2})(\d{4})(\d{4}).*/, '($1)$2-$3');
}
};
/* phone number max number validation*/
$('#phonenumber').on('keydown',function(event){
var key = event.keyCode || event.charCode;
if($(this).val().length == 14){
if(event.keyCode !== 8) {
event.preventDefault();
}
}
});
/***********************************/
</script>
<style type="text/css">
form#contactform {
width: 100%;
}
.content_box h2{color: #1B9BD8;
font-size: 20px;margin-bottom: 5px;}
.contact-email p {
font-size: 24px;
padding-top: 7px;
}
@media (max-width: 1024px){
.row {
margin-right: -15px;
margin-left: -15px;
}
}
@media (max-width: 767px){
#contactform .form-group {
padding: 0px 15px;
}
}

</style>
@stop