@extends('layouts.default-header')
@section('title', 'Cadastre-se')
@section('content')
@include('layouts.flash-message')
<div class="formsection"> 
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg">
          @include('layouts.flash-message')
          <form class="form-horizontal registerform formarea" method="POST" id="registerform" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h2 class="text-center  flipInY">Cadastre-se</h2>
            <!--  <p class="text-center  fadeInLeft">Sign Up by entering the information below</p> -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}  forminput  fadeInLeft">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Nome Completo" required autofocus>
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}  forminput  fadeInLeft">   
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" id="email" placeholder="Email" required>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  forminput  fadeInLeft">
              <input id="password" type="password" class="form-control" name="password" id="password" placeholder="Senha" required>
              <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group forminput  fadeInLeft">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirme sua senha" required>
              <span toggle="#password-confirm" class="fa fa-fw fa-eye field-icon toggle-password">
              </div>
              <input type="hidden" name="user_role" value="Provider">
              
              <div class="form-group  fadeInLeft">
                <label class="checkboxdiv">Eu li e concordo com os termos de uso.
                  <input type="checkbox" name="agree" id="agree" checked required>
                  <span class="checkmark"></span>
                </label>
              </div>
              <input type="hidden" name="ispvrgfrm" value="yes">
              <div class="form-group buttondiv ">
                <input type="submit" class="btn btn-primary btn-lg" value="Cadastre-se">
              </div>
              <div class="formlink">
                <span>Já possui uma conta? Clique aqui para</span>
                <a href="{{ url('/login') }}">Entrar</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
  $.validator.addMethod("pwcheck", function(value) {
  return /^[A-Za-z\d=!\-@._*#$%^&]*$/.test(value)
  && /[a-z]/.test(value) && /[A-Z]/.test(value)
  && /\d/.test(value)
  });
  jQuery('#registerform').validate({
  rules: {
  password: {
  required: true,
  minlength: 6
  },
  password_confirmation: {
  equalTo: "#password"
  }
  },
  messages: {
  password: {
  required: "Favor preencher o campo acima",
  minlength: "Minimum 6 characters required"
  },
  password_confirmation: {
  equalTo: "The confirm password does not match with the password"
  }
  },
  });
  </script>
  @endsection