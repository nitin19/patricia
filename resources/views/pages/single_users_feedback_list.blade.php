@extends('layouts.default-header')

@section('title', 'Feedback ')

@section('content')

@if(Auth::check() && empty(Auth::user()->address) && empty(Auth::user()->phone))
<div class="alert alert-danger alert-dismissible fade in profle_alert">
	<!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
	<strong>Alert!!</strong> please Complete Your Profile.
</div>
@endif
<div class="bannerimg text-center">
	<div class="container">
		<div class="banner_content">
			<h1>Prestador(es) de serviço contatado(s) por você:</h1>
		</div>
	</div>
</div>
<div class="feedback_listsec">
	<div class="container">
		<div class="feedback_listing">
			<div class="alert alert-info alert-dismissible  msgAlertBlock fade in" style="display:none;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div> 
			@if(count($get_user_bookings)>0)
			<ul>
				<li>
					@foreach($get_user_bookings as $booking)
					<?php $userblock = DB::table('block_providers')->where('taker_id','=',$booking->user_id)->where('provider_id','=',$booking->profile_id)->first();
					//print_r($userblock->block_status);
					 $feedbacks = DB::table('feedback')->where('user_id',Auth::user()->id)->where('booking_id',$booking->booking_id)->where('provider_id',$booking->profile_id)->first();
					 $fmt = datefmt_create(
                                'pt-BR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL,
                                'Brazil/East',
                                IntlDateFormatter::GREGORIAN,
                                "dd/MMMM/YYYY"  
                              );
                                //start time
                              $dt = new DateTime($booking->booking_start_time);
                              $d = $dt->format('d');
                              $m = $dt->format('m');
                              $y = $dt->format('Y');
                                //end time
                              $edt = new DateTime($booking->booking_end_time);
                              $ed = $edt->format('d');
                              $em = $edt->format('m');
                              $ey = $edt->format('Y');

					 ?>
					<div class="row backback">
						<div class="col-sm-4 feedback_leftdiv leftspace">
								<!--  <i class="fa fa-user feedbackicon"></i> -->
							<img src="{{ url('/public/images/categoryimage/'.$booking->cat_image) }}" style="float: left;"><p><b>{{$booking->cat_name}}</b><br/>
							<span>Primeiro contato<br/><i class="fa fa-calender"></i>{{datefmt_format($fmt, mktime(0,0,0,$m,$d,$y))}}<br/>Último contato<br/><i class="fa fa-calender"></i>{{datefmt_format($fmt, mktime(0,0,0,$em,$ed,$ey))}}</span></p>
						</div>
						<div class="col-sm-4 feedback_centerdiv leftspace">
							@if(empty($booking->profile_image))
							    <img class="img-responsive" src="{{url('/public/images/')}}/prof_dummy2.png"><p><b>Prestador(a) de serviço</b></p><br/>
							@else
								<img class="img-responsive" src="{{url('/public/images/profileimage/'.$booking->profile_image)}}"><p><b>Professional</b></p><br/>
							@endif
							<span>{{ucfirst($booking->name)}}<br/></span>
						</div>
						<div class="col-sm-4 feedback_rightdiv leftspace">
	                        <div class="unblocked_{{ $booking->booking_id }}" data-bid="{{ $booking->booking_id }}">
							   <a href ="{{ url('/feedback/booking_details/'.$booking->user_id.'/'.$booking->profile_id) }}" >Booking Details</a>	
							</div>		
						</div>
					</div>
					@endforeach
				</li>
			</ul>
			@else
			<p align="center">There are no service Provider Contacted By You</p>
			@endif
			{{ $get_user_bookings->links() }}
        </div>
    </div>
</div>

<script>
	jQuery(document).ready(function(){
	    window.setTimeout(function() {
	 	   $(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
	 		    $(this).remove(); 
	 	    });
	     }, 4000);
	});
</script>
@stop
