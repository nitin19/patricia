@extends('layouts.default-header')
@section('title', 'Informações úteis')
@section('content')
<div class="payment_box">
  <div class="imgservices">
    <div class="banner_content text-center">
      <h1>Other Services</h1>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="pxlr-club-faq">
          <div class="content">
            <div class="panel-group 2_section" id="accordion" role="tablist" aria-multiselectable="true">
              <?php $count =1; ?>
              @foreach($category_info as $cat_info)
                @if($count%2==1)
                  <?php $cat_id = $cat_info->cat_id;
                    $sub_category = DB::table('category')->where('is_active', '1')->where('is_deleted', '0')->where('cat_id', $cat_id)->get();
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading bac_clr" id="headingOne" role="tab">
                      <h4 class="panel-title">
                      <a class="panel-collapse collapse in" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $cat_info->cat_id }}" aria-expanded="true" aria-controls="collapse{{ $cat_info->cat_id }}">For {{ $cat_info->cat_name }}<i class="more-less glyphicon glyphicon-plus"></i></a>
                      </h4>
                    </div>
                    <div class="panel-collapse collapse in" id="collapse{{ $cat_info->cat_id }}" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body pxlr-faq-body">
                        @foreach($sub_category as $sub_category_info)
                          <p>{!!html_entity_decode($sub_category_info->cat_description) !!}</p>
                        @endforeach
                      </div>
                    </div>
                  </div>
                @endif
                <?php ++$count; ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="pxlr-club-faq">
          <div class="content">
            <div class="panel-group 2_section" id="accordion" role="tablist" aria-multiselectable="true">
              <?php $count_odd =1; ?>
              @foreach($category_info as $cat_info)
              @if($count_odd%2==0)
              <?php
              $cat_id = $cat_info->cat_id;
              $sub_category = DB::table('category')->where('is_active', '1')->where('is_deleted', '0')->where('cat_id', $cat_id)->get();
              ?>
              <div class="panel panel-default">
                <div class="panel-heading bac_clr" id="heading4" role="tab">
                  <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $cat_info->cat_id }}" aria-expanded="false" aria-controls="collapseOne">For {{ $cat_info->cat_name }}<i class="more-less glyphicon glyphicon-plus"></i></a>
                  </h4>
                </div>
                <div class="panel-collapse collapse in" id="collapse{{ $cat_info->cat_id }}" role="tabpanel"
                  aria-labelledby="heading4">
                  <div class="panel-body pxlr-faq-body">
                    @foreach($sub_category as $sub_category_info)
                    <p>{!!html_entity_decode($sub_category_info->cat_description) !!}</p>
                    @endforeach
                  </div>
                </div>
              </div>
              @endif
              <?php ++$count_odd; ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop