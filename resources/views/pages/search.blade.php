@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<style type="text/css">
.starratingdiv .rating_checkbox { position: unset !important; }
.list-group {display: block;}
.wishlful {list-style: none;}
.wishlful li {
padding: 5px 0px;
font-size: 13px;
font-weight: bold;
}



</style>
<section class="maincontent searchbarbg greybgdiv fillter-div">

<div class=" filtros-part"> 
  <div class="button-box">
  <button class="btn btn-primary-button dropdown-toggle" type="button" data-toggle="dropdown">Serviços disponíveis
  <i class="fa fa-caret-down icon-drop" aria-hidden="true"></i></button>
  </div>
 </div>

  <div class="col-sm-3 nopadding page_sidebar">
    <div class="sidebarbg">
      <div class="col-sm-12 nopadding">
        <div class="sidebartitle">
          <div class="row">
            <div class="col-sm-9">
              <h1>Serviços disponíveis:</h1>
            </div>
            <div class="col-sm-3 text-right nopadding">
              <span><a href="{{ url('/search/') }}?cat=&sub_id=&area=&amount=&rating=&name=&bio=&certificate=&sortby=" >Mostrar todos</a></span>
            </div>
          </div>
        </div>
      </div>
      <div id="MainMenu"  class="col-sm-12 mainmenu nopadding nav">
        <div class="list-group panel">
          <?php $count = 1; ?>
          @foreach($get_category1 as $category_info_key=>$category_info_val)
          <?php
          $mainCatId = DB::table('main_category')->where('name',$category_info_key)->first();
          ?>
          <a href="#link{{ $count }}" id="{{$mainCatId->id}}" class="list-group-item mymainCat @if($category_info_key == $main_category_data) selected @else collapse @endif" data-toggle="collapse" data-parent="#MainMenu"> {{ $category_info_key }}  <i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></a>
          <div class=" submenu @if($category_info_key == $main_category_data) selected @else collapse @endif" id="link{{ $count }}">
            @foreach($category_info_val as $cat_key=>$cat_val)
            <a href="#" id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" class="list-group mysubCat @if($sub_id == $cat_val['cat_id']) active @endif" data-id="{{$cat_val['cat_id']}}" data-url="{{ url('/subcategory_filter/').'/'.$cat_val['cat_id'] }}">{{$cat_val['cat_name']}}</a>
            @endforeach
          </div>
          <?php ++$count; ?>
          @endforeach
        </div>
      </div>
      <div class="col-sm-12 nopadding ">
        <div class="sidebartitle filterdiv">
          <div class="row">
            <div class="col-sm-8">
              <h2>Filtro aplicado</h2>
            </div>
            <div class="col-sm-4 text-right nopadding">
              <span><a href="{{ url('/search/') }}?cat=&sub_id=&area=&amount=&rating=&name=&bio=&certificate=&sortby=" >Limpar filtro</a></span><!-- onclick="return confirm('Você tem certeza?')" -->
            </div>
          </div>
        </div>
        <div class="filterblks">
          <ul>
            @if($cat)
            <li class="badge">
              <a href="{{ url('/search/') }}?cat=&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating={{$rating}}&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" >
                <?php
                $cat_data = DB::table('main_category')->where('id',$cat)->first();
                ?>
                {{ $cat_data->name }}
              <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif
            @if($sub_id)
            <li class="badge">
              <a href="{{ url('/search/') }}?cat={{$cat}}&sub_id=&area={{$area}}&amount={{$amount}}&rating={{$rating}}&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" >
                <?php
                $sub_cat_data = DB::table('category')->where('cat_id',$sub_id)->first();
                ?>
                {{ $sub_cat_data->cat_name }}
              <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif

            @if($area) 
            <li class="badge">
              <a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area=&amount={{$amount}}&rating={{$rating}}&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" >{{ $area}} <i class="fa fa-close area_close"></i></a>
            </li>
            @endif
            @if($amount) 
            <li class="badge"><a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount=&rating={{$rating}}&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" >R${{ $amount }} <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif
            @if($rating) 
              @if($rating > 1)
                <li class="badge"><a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating=&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" > {{ $rating}} estrelas<i class="fa fa-close rating_close"></i></a>
                </li>
              @else
                <li class="badge"><a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating=&name={{$name}}&bio={{$bio}}&certificate={{$certificate}}&sortby=" > {{ $rating}} estrela<i class="fa fa-close rating_close"></i></a>
                </li>
              @endif
            @endif

            @if($name) 
            <li class="badge">
              <a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating={{$rating}}&name=&bio={{$bio}}&certificate={{$certificate}}&sortby=" >{{ $name }} <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif

            @if($bio) 
            <li class="badge">
              <a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating={{$rating}}&name={{$name}}&bio=&certificate={{$certificate}}&sortby=" >{{ $bio }} <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif

            @if($certificate) 
            <li class="badge">
              <a href="{{ url('/search/') }}?cat={{$cat}}&sub_id={{$sub_id}}&area={{$area}}&amount={{$amount}}&rating={{$rating}}&name={{$name}}&bio={{$bio}}&certificate=&sortby=" >{{ $certificate }} <i class="fa fa-close cat_close"></i></a>
            </li>
            @endif

          </ul>
        </div>
      </div>
      <!--city-sec-->
      <div class="col-sm-12 nopadding">
        <form name="search_form" id="searchFrm" method="get" action="{{ url('/search') }}" class="cityform">
          <input type="hidden" name="cat" id="mainCatId" value="{{ $cat }}">
          <input type="hidden" name="sub_id" id="subCatId" value="{{ $sub_id }}">
          
          <div class="row address_related_section">
            <div class="">

              <div class="col-sm-12 sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Área</h3>
                    </div>
                  </div>
                </div>
                <input data-subid="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="text" name="area" id="autocomplete" value="{{$area}}" class="form-control myseararea" placeholder="Digite a área">
              </div>
              <div class="col-sm-12 sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Preço estimado para 8 horas de serviço
                      <a href="#" data-toggle="tooltip" data-placement="auto" title="Você não precisa contratar o serviço por 8 horas. A disponibilidade do profissional variará conforme sua necessidade. Colocamos 8 horas aqui como padrão para que você consiga comparar o preço por um dia de trabalho entre os profissionais cadastrados no site, facilitando sua busca caso preço seja um quesito relevante para você."><img src="{{ url('/public/images/question-mark.png') }}" class="question-mark" height="21px;"></a></h3>
                    </div>
                  </div>
                </div>
                <!-- <input type="text" name="amount" id="amount" value="{{$amount}}" class="form-control mysearprice" placeholder="Digite o preço" pattern="^\d*(\.\d{0,2})?$" > -->

                <input type="text" name="amount" id="amount" value="{{$amount}}" class="form-control mysearprice" placeholder="Digite o preço" pattern="^\d{1,3}(.\d{3})*(\,\d+)?" data-type="currency">
              </div>


               <div class="col-sm-12 sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Nome</h3>
                    </div>
                  </div>
                </div>
                <input data-subid="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="text" id="name" name="name" value="{{$name}}" class="form-control mysearname" placeholder="Digite o nome que deseja encontrar">
              </div>

              <div class="col-sm-12 sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Informações sobre o prestador de serviço</h3>
                    </div>
                  </div>
                </div>
                <input data-subid="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="text" id="bio" name="bio" value="{{$bio}}" class="form-control mysearbio" placeholder="Digite aqui alguma palavra chave ou conhecimento que você busca referente à prestação de serviço">
              </div>

              <div class="col-sm-12 sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h3>Cursos e certificados</h3>
                    </div>
                  </div>
                </div>
                <input data-subid="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="text" id="certificate" name="certificate" value="{{$certificate}}" class="form-control mysearcertificate" placeholder="Digite aqui cursos ou certificados que você procura">
              </div>


            </div>
          </div>
          <div class="ratingsec">
            <div class="sidebartitle">
              <div class="row">
                <div class="col-sm-12">
                  <h3>Avaliação dos usuários</h3>
                </div>
              </div>
            </div>
            <div class="topspacing">
              <div class="starratingsec">
                <div class="starratingdiv">
                  <input id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="radio" name="rating" value="4" class="rating_checkbox mysearrate"  @if($rating == '4') checked="checked" @endif>
                  <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i>
                  <i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i>(ou mais)</span>
                </div>
                <div class="starratingdiv">
                  <input id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="radio" name="rating" value="3" class="rating_checkbox mysearrate"  @if($rating == '3') checked="checked" @endif >
                  <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i>
                  <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i>(ou mais)</span>
                </div>
                <div class="starratingdiv">
                  <input id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="radio" name="rating" value="2" class="rating_checkbox mysearrate" @if($rating == '2') checked="checked" @endif >
                  <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i>
                  <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i>(ou mais)</span>
                </div>
                <div class="starratingdiv">
                  <input id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="radio" name="rating" value="1" class="rating_checkbox mysearrate"   @if($rating == '1') checked="checked" @endif >
                  <span><i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i>
                  <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i>(ou mais)</span>
                  
                </div>
                <div class="starratingdiv">
                  <input id="{{$cat_val['cat_id']}}" data-typeid="{{$mainCatId->id}}" type="radio" name="rating" value="" class="rating_checkbox mysearrate"   @if($rating == '') checked="checked" @endif >
                  <span>Com ou sem avaliação</span>
                </div>
                
              </div>
            </div>
            <!-- <div class="col-sm-12 formbutton">
              <input type="submit" class="btn find_provider" value="Encontre provedores">
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-9  nopadding map-div">
      <div id="map" style="width: 100%; height: 390px; background: #89C4FA"/></div>
      <div class="searchheaderdiv">
        <div class="col-sm-12">
          <div class="col-sm-12 full-width" style="overflow:hidden;">
            <div class="srchheaderbrdr">
              <div class="col-sm-4 col-xs-12 resultdiv nopadding">
                <h5><?php
                if($cat) {
                $cat_data = DB::table('main_category')->where('id',$cat)->first();
                $category_name = $cat_data->name;
                } else {
                $category_name = '';
                }
                if($sub_id) {
                $sub_cat_data = DB::table('category')->where('cat_id',$sub_id)->first();
                $subcategory_name = $sub_cat_data->cat_name;
                } else {
                $subcategory_name = '';
                }
                if(count($users) > 0 ) {
                if(!empty(Auth::user())) {
                if(count(array_keys($final_data, "1")) > 0)
                { $usrListCount = (count($users) - count(array_keys($final_data, "1")));
                }else{ $usrListCount = count($users); }
                }
                else {
                $usrListCount = count($users);
                }
                $totalmapsuer = count($mapusers);
                $currentURL = url()->full();
                //echo'url:'. $currentURL;
                if (strpos($currentURL, 'sub_id') !== false) {
                  if($usrListCount > 1){
                    echo $usrListCount.' de ' .$totalmapsuer. ' resultados ';
                  } else {
                    echo $usrListCount.' de ' .$totalmapsuer. ' resultado ';
                  }
                }else{
                  if($usrListCount > 1  &&  $totalmapsuer > 1){
                    echo $usrListCount.' de ' .$totalmapsuer. ' resultados ';
                  } else {
                    echo $usrListCount.' de ' .$totalmapsuer. ' resultado ';
                  }
                }
                } else { 
                echo 'Não encontramos prestadores de serviço nessa região.<br/>
                Reveja os filtros aplicados no menu desta tela.';
                }
                ?></h5>
              </div> 
              <div class="col-sm-4 col-xs-12 full-width">
 

<!--                    <div class=" filtros-part"> 

                    <div class="button-box">
                    <button class="btn btn-primary-button dropdown-toggle" type="button" data-toggle="dropdown">Serviços disponíveis
                    <i class="fa fa-caret-down icon-drop" aria-hidden="true"></i></button>
                    </div>

                  </div> -->
 

              </div>
              <div class="col-sm-4 col-xs-12 resultselbox nopadding">
                <select name="sortby" id="sort_res">
                  <option value="rating" @if($sortby == 'rating') selected @endif>Avaliação  usuários</option>
                  <option value="price" @if($sortby == 'price') selected @endif>Preço</option>
                  <option value="distance" @if($sortby == 'distance' || $sortby == '') selected @endif>Prestadores mais próximos</option>
                </select>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="searchresults">
          @if(count($users) > 0)
          @foreach($users as $user_info)
          <?php
          $lat = $user_info->latitude;
          $lon = $user_info->longitude;
          $profile_image = $user_info->profile_image;
          $profile_rating = $user_info->profile_rating;
          $category_image = $user_info->cat_image;
          $user_id = $user_info->id;
          $area_arr = $area;
          $pvamount = $user_info->amount;
          $pvquery = $user_info->pvquery;
          ?>
          @if(!empty(Auth::user()))
          <?php $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=',$user_info->id)->where('block_status','=','1')->first();
          if(empty($userblock)) { ?>
          <div class="col-md-4 col-sm-6 searchresultdiv">
            <div class="searchresultbg">
              @if($profile_image=='')
              <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="profimg img-shot" style="margin-left: 17%"></a>
              @else
              <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="profimg img-shot" style="margin-left: 17%"></a>
              @endif
              <div class="wishlistbox">
                <div class="wishlistdiv">
                  <div class="discountdiv">
                  </div>
                  <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                    <?php
                    $usertotalreview = DB::table('feedback')
                    ->join('users', 'users.id', '=', 'feedback.user_id')
                    ->where('feedback.provider_id',$user_info->id)
                    ->where('approve_status',1)
                    ->where('role','taker')
                    ->count();
                    $current_u = Auth::id();
                    $current_userid = Auth::check();
                    if($current_userid==1){
                    $wish_list_exist = DB::table('wish_list')
                    ->where('user_id', $current_u)
                    ->where('wish_profile_id', $user_info->id)
                    ->where('status','1')
                    ->count();
                    ?>
                    <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                    <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart alreadyadd'; } else { echo 'fa-heart-o ffff addwhishlist'; } ?>"></i>
                    <?php
                    } else {
                      $wish_list_exist_ip = DB::table('wish_list_ip')
                      ->where('ip_address', $current_ip)
                      ->where('wish_profile_id', $user_info->id)
                      ->count();
                      ?>
                      <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                      <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                      <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart noadd'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                    <?php } ?>
                  </div>
                </div>
                <div class="resultcntntbox resul-part">
                  <div class="row">
                    <div class="col-sm-8 col-xs-12 resultbox_title">
                      <ul class="wishlistul">
                        <li>Nome: <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>" class="wishli">{{ substr($user_info->username,0,16)}}
                          <?php
                          if (strlen($user_info->username)>='16'){
                          echo '...';
                          }
                          ?>
                        </a>
                      </li>
                      <?php  $uaddrs = $user_info->address;
                      $pieces = explode(",", $uaddrs); ?>  
                    </ul>
                  </div>
                  <div class="col-sm-4 col-xs-12 ttt">
                    <ul class="wishlistul textright">
                      <li>
                        <?php
                        if($usertotalreview){
                          if($user_info->profile_rating != ''){
                            $ipnumber = number_format($user_info->profile_rating,1);
                            $ipinteger_part = floor($ipnumber);
                            $ipfraction_part = $ipnumber-$ipinteger_part;
                            for($x=1;$x<=$ipnumber;$x++) { ?>
                              <i class="fa fa-star ycolor"></i>
                              <?php 
                            }
                              if (strpos($ipnumber,'.')) {
                                if($ipfraction_part > 0){ ?>
                                   <i class="fa fa-star-half-o ycolor" aria-hidden="true"></i>
                                  <?php
                                  $x++;
                                }
                              }
                              while($x<=5){ ?>
                                <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                                <?php
                                $x++;
                              }
                          } 
                        } else{
                          for($i=1;$i<=5;$i++){ ?>
                            <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                            <?php 
                          }
                        }
                        ?>
                        <span class="reviews">@if($usertotalreview > 0) ({{$usertotalreview}}) @endif</span>
                      </li>
                    </ul>
                  </div>
                  <ul class="col-sm-12 wishlful">
                    <li>Distância: {{ number_format($user_info->distances,2,",",".") }} km</li>
                    <li>Preços: @if($pvamount) &#82;&#36;{{ number_format($pvamount,2,",",".") }} / {{$pvquery}} @endif</li>
                    <li>Categoria: {{ $user_info->name }} </li>
                    <li>Serviço: {{ $user_info->cat_name }} </li>
                    <?php if($user_info->neighborhood!=''){?>
                    <li>Bairro: {{ $user_info->neighborhood }}</li>
                    <?php } else { ?>
                    <li>Bairro: </li>
                    <?php } ?>
                    <li>Cidade: {{ $user_info->city }} </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        @else
        <div class="col-sm-4 col-xs-12 searchresultdiv desktop_section">
          <div class="searchresultbg">
            @if($profile_image=='')
            <!-- <a href="{{url('/login?')}}<?php //echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="img-responsive"></a> -->
            <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="profimg img-shot" style="margin-left: 17%"></a>
            @else
            <!-- <a href="{{url('/login?')}}<?php //echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/<?php //echo $profile_image; ?>" class="img-responsive"></a> -->

            <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli "><img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="profimg img-shot" style="margin-left: 17%"></a>
            @endif
            <div class="wishlistbox">
              <div class="wishlistdiv">
                <div class="discountdiv">
                </div>
                <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                  <?php
                  $usertotalreview = DB::table('feedback')
                  ->join('users', 'users.id', '=', 'feedback.user_id')
                  ->where('feedback.provider_id',$user_info->id)
                  ->where('approve_status',1)
                  ->where('role','taker')
                  ->count();
                  $current_u = Auth::id();
                  $current_userid = Auth::check();
                  if($current_userid==1){
                  $wish_list_exist = DB::table('wish_list')
                  ->where('user_id', $current_u)
                  ->where('wish_profile_id', $user_info->id)
                  ->where('status','1')
                  ->count();
                  ?>
                  <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                  <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart alreadyadd'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>
                  <?php
                  } else {
                  $wish_list_exist_ip = DB::table('wish_list_ip')
                  ->where('ip_address', $current_ip)
                  ->where('wish_profile_id', $user_info->id)
                  ->count();
                  ?>
                  <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                  <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                  <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart noadd'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                  <?php
                  }
                  ?>
                </div>
              </div>
              <div class="resultcntntbox resul-part">
                <div class="row">
                  <div class="col-sm-6 resultbox_title">
                    <ul class="wishlistul">
                      <li> Nome: <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli">{{ substr($user_info->username,0,16)}}
                        <?php
                        if (strlen($user_info->username)>='16'){
                        echo '...';
                        }
                      ?></a>
                    </li>
                    <?php  $uaddrs = $user_info->address;
                    $pieces = explode(",", $uaddrs); ?>
                  </ul>
                </div>
                <div class="col-sm-6 vvv">
                  <ul class="wishlistul textright">
                    <li>
                      <?php
                      if($usertotalreview){
                      if($user_info->profile_rating != ''){
                      $ipnumber = number_format($user_info->profile_rating,1);
                      $ipinteger_part = floor($ipnumber);
                      $ipfraction_part = $ipnumber-$ipinteger_part;
                      for($x=1;$x<=$ipnumber;$x++) {?>
                      <i class="fa fa-star ycolor"></i>
                      <?php }
                      if (strpos($ipnumber,'.')) {
                      if($ipfraction_part > 0){?>
                      <i class="fa fa-star-half-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      while($x<=5){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      } else{
                      for($i=1;$i<=5;$i++){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php }
                      }
                      
                      ?>
                      <span class="reviews">@if($usertotalreview > 0) ({{$usertotalreview}}) @endif</span>
                    </li>
                  </ul>
                </div>
                <ul class="col-sm-12 wishlful">
                  <li>Distância: {{ number_format($user_info->distances,2,",",".") }} km</li>
                  <li>Preços: @if($pvamount) &#82;&#36;{{ number_format($pvamount,2,",",".") }} / {{$pvquery}} @endif</li>
                  <li>Categoria: {{ $user_info->name }} </li>
                  <li>Serviço: <a class="wishli">{{ $user_info->cat_name }}</a>
                </li>
                <?php if($user_info->neighborhood!=''){ ?>
                <li>Bairro: {{ $user_info->neighborhood }}</li>
                <?php } else { ?>
                <li>Bairro:</li>
                <?php } ?>
                <li>Cidade: {{ $user_info->city }} </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12 searchresultdiv mobile_section">
          <div class="searchresultbg">

            <div class="col-sm-3 vvv">
            @if($profile_image=='')
            <!-- <a href="{{url('/login?')}}<?php //echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="img-responsive"></a> -->
            <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="profimg img-shot" style="margin-left: 17%">
              <div class="col-sm-6 vvv">
                  <ul class="wishlistul textright">
                    <li>
                      <?php
                      if($usertotalreview){
                      if($user_info->profile_rating != ''){
                      $ipnumber = number_format($user_info->profile_rating,1);
                      $ipinteger_part = floor($ipnumber);
                      $ipfraction_part = $ipnumber-$ipinteger_part;
                      for($x=1;$x<=$ipnumber;$x++) {?>
                      <i class="fa fa-star ycolor"></i>
                      <?php }
                      if (strpos($ipnumber,'.')) {
                      if($ipfraction_part > 0){?>
                      <i class="fa fa-star-half-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      while($x<=5){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      } else{
                      for($i=1;$i<=5;$i++){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php }
                      }
                      
                      ?>
                      <span class="reviews">@if($usertotalreview > 0) ({{$usertotalreview}}) @endif</span>
                    </li>
                  </ul>
                </div>
            </a>
            @else
            <!-- <a href="{{url('/login?')}}<?php //echo $user_info->id;?>" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/<?php //echo $profile_image; ?>" class="img-responsive"></a> -->

            <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli "><img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="profimg img-shot" style="margin-left: 17%">
              <div class="col-sm-6 vvv">
                  <ul class="wishlistul textright">
                    <li>
                      <?php
                      if($usertotalreview){
                      if($user_info->profile_rating != ''){
                      $ipnumber = number_format($user_info->profile_rating,1);
                      $ipinteger_part = floor($ipnumber);
                      $ipfraction_part = $ipnumber-$ipinteger_part;
                      for($x=1;$x<=$ipnumber;$x++) {?>
                      <i class="fa fa-star ycolor"></i>
                      <?php }
                      if (strpos($ipnumber,'.')) {
                      if($ipfraction_part > 0){?>
                      <i class="fa fa-star-half-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      while($x<=5){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php
                      $x++;
                      }
                      }
                      } else{
                      for($i=1;$i<=5;$i++){?>
                      <i class="fa fa-star-o ycolor" aria-hidden="true"></i>
                      <?php }
                      }
                      
                      ?>
                      <span class="reviews">@if($usertotalreview > 0) ({{$usertotalreview}}) @endif</span>
                    </li>
                  </ul>
                </div>
            </a>
            @endif
            </div>

            <div class="col-sm-9">
            <div class="wishlistbox">
              <div class="wishlistdiv">
                <div class="discountdiv">
                </div>
                <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                  <?php
                  $usertotalreview = DB::table('feedback')
                  ->join('users', 'users.id', '=', 'feedback.user_id')
                  ->where('feedback.provider_id',$user_info->id)
                  ->where('approve_status',1)
                  ->where('role','taker')
                  ->count();
                  $current_u = Auth::id();
                  $current_userid = Auth::check();
                  if($current_userid==1){
                  $wish_list_exist = DB::table('wish_list')
                  ->where('user_id', $current_u)
                  ->where('wish_profile_id', $user_info->id)
                  ->where('status','1')
                  ->count();
                  ?>
                  <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                  <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart alreadyadd'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>
                  <?php
                  } else {
                  $wish_list_exist_ip = DB::table('wish_list_ip')
                  ->where('ip_address', $current_ip)
                  ->where('wish_profile_id', $user_info->id)
                  ->count();
                  ?>
                  <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                  <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                  <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart noadd'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                  <?php
                  }
                  ?>
                </div>
              </div>
              <div class="resultcntntbox resul-part">
                <div class="row">
                  <div class="col-sm-6 resultbox_title vvv">
                    <ul class="wishlistul">
                      <li> Nome: <a href="{{url('/userprofile/')}}/<?php echo $user_info->id;?>" class="wishli">{{ substr($user_info->username,0,16)}}
                        <?php
                        if (strlen($user_info->username)>='16'){
                        echo '...';
                        }
                      ?></a>
                    </li>
                    <?php  $uaddrs = $user_info->address;
                    $pieces = explode(",", $uaddrs); ?>
                  </ul>
                </div>
                
                <ul class="col-sm-12 wishlful">
                  <li>Distância: {{ number_format($user_info->distances,2,",",".") }} km</li>
                  <li>Preços: @if($pvamount) &#82;&#36;{{ number_format($pvamount,2,",",".") }} / {{$pvquery}} @endif</li>
                  <li>Categoria: {{ $user_info->name }} </li>
                  <li>Serviço: <a class="wishli">{{ $user_info->cat_name }}</a>
                </li>
                <?php if($user_info->neighborhood!=''){ ?>
                <li>Bairro: {{ $user_info->neighborhood }}</li>
                <?php } else { ?>
                <li>Bairro:</li>
                <?php } ?>
                <li>Cidade: {{ $user_info->city }} </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      </div>
    </div>
    @endif
    @endforeach
    @endif
    <div class="col-sm-12" style="float: left;">{{ $users->appends(['cat' => $cat,'sub_id' => $sub_id,'area'  => $area,'amount' => $amount,'rating' => $rating,'name' => $name,'bio' => $bio,'certificate' => $certificate,'sortby' =>$sortby])->render() }}
    </div>
  </div>
</div>
</div>
</div>
</section>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<script>
$(document).ready(function(){
  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});
</script>

<script>
$(document).ready(function(){
  $('button.btn.btn-primary-button.dropdown-toggle').on("click", function(e){
    $(".sidebarbg").toggle('slow');
  });
});
</script>

<script  type="text/javascript">
$('[data-toggle="tooltip"]').tooltip();

$('.alreadyadd').click(function(){
  $.notify({
  message: 'Prestador de serviço já adicionado na sua lista de seleção. Clique aqui para ver sua lista completa',
  url: '{{ url("/wishlist") }}',
  },{
  type: 'danger',
  offset:
  {
  x: 10,
  y: 130
  },
  animate: {
  enter: 'animated fadeInRight',
  exit: 'animated fadeOutRight'
  },
  url_target: '_self',
  });
});
$('.noadd').click(function(){
  $.notify({
  message: 'Prestador de serviço já adicionado na sua lista de seleção. Clique aqui para ver sua lista completa',
  url: '{{ url("/wishlistip") }}',
  },{
  type: 'danger',
  offset:
  {
  x: 10,
  y: 130
  },
  animate: {
  enter: 'animated fadeInRight',
  exit: 'animated fadeOutRight'
  },
  url_target: '_self',
  });
});
/*$("#autocomplete").autocomplete({
  source: function (request, response) {
  var url_base = '{{ url('/') }}';
  $.ajax({
  url: url_base+'/search_address/search_address.php',
  type: "GET",
  data: request,
  dataType: "json",
  beforeSend: function(xhr){
  xhr.setRequestHeader('Accept', 'application/json');
  },
  success: function (data) {
  response(data);
  }
  });
  },
  select: function (event, ui) {
  // Set selection
  $('#autocomplete').val(ui.item.value); // display the selected text
  return false;
  }
});*/
$('#sort_res').on('change',function(){
  $("#searchFrm").submit();
})
$(".mysubCat").click(function(){
  var my_sub_id = $(this).attr('id');
  var my_main_id = $(this).attr('data-typeid');
  $('#subCatId').val(my_sub_id);
  $('#mainCatId').val(my_main_id);
  $("#searchFrm").submit();
});
$(".mymainCat").click(function(){
  var my_main_id = $(this).attr('id');
  $('#mainCatId').val(my_main_id);
  $('#subCatId').val('');
  $("#searchFrm").submit();
});
$(".mysearrate").click(function(){
  var my_sub_id = $(this).attr('id');
  var my_main_id = $(this).attr('data-typeid');
  //$('#subCatId').val(my_sub_id);
  // $('#mainCatId').val(my_main_id);
  $("#searchFrm").submit();
});
$(".mysearcep").blur(function()  {
  var my_sub_id = $(this).attr('data-subid');
  var my_main_id = $(this).attr('data-typeid');
  // $('#mainCatId').val(my_main_id);
  var chkZipVal = $(this).val();
  if(chkZipVal != '') {
    $("#searchFrm").submit();
  }
});

/*$(".myseararea").change(function()  {
  $("#searchFrm").submit();
});*/

$(".mysearname").blur(function()  {
  var my_sub_id = $(this).attr('data-subid');
  var my_main_id = $(this).attr('data-typeid');
  // $('#mainCatId').val(my_main_id);
  $("#searchFrm").submit();
});

$(".mysearbio").blur(function()  {
  var my_sub_id = $(this).attr('data-subid');
  var my_main_id = $(this).attr('data-typeid');
  // $('#mainCatId').val(my_main_id);
  $("#searchFrm").submit();
});

$(".mysearcertificate").blur(function()  {
  var my_sub_id = $(this).attr('data-subid');
  var my_main_id = $(this).attr('data-typeid');
  // $('#mainCatId').val(my_main_id);
  $("#searchFrm").submit();
});


$(".mysearprice").blur(function()  {
  $('#mainCatId').val('');
  $('#subCatId').val('');
  $("#searchFrm").submit();
});
$('#amount').keyup('input[pattern]',function () {
  this.value = this.value.replace(/[^0-9\.]/g,'');
});
$(document).mouseup(function(e){
  var container = $(".H_ib_body");
  // if the target of the click isn't the container nor a descendant of the container
  if (!container.is(e.target) && container.has(e.target).length === 0)
  {
  container.hide();
  }
});
$('.sub_cat').on('click',function(){
  var id=$(this).data('id');
  var url = $(this).data('url');
  // alert(url);
  $.ajax({
    type:'GET',
    dataType:"JSON",
    data:id,
    url:url,
    success:function(data){
      if(data != ""){
      $.each(data,function(k,v){
      if($('.category_name').text()!=v.cat_name){
      $('.category_name').parent().parent().parent().parent().hide()
      }
      });
      }
    }
  });
});
jQuery('.fa-close').click(function(){
  $(this).parents('li').hide();
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&callback=initMaps&sensor=false&libraries=places"></script>
<script type="text/javascript">
<?php
$lat_str1 = '';
$long_str1 = '';
$cat_image = '';
$latitude = '';
$longitude = '';
$user_id = '';
$username = '';
$city = '';
$latitude_center = '';
$longitude_center ='';
?>
function initMaps() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        maxZoom: 16,
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var markers = [
      <?php 
      $intpart = '';
      //foreach($users as $map_user_info) {
      foreach($mapusers as $map_user_info) {
        if(empty($map_user_info->profile_image)){
          $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
        }
        else{
          $image_path =  url('/public')."/images/profileimage/".$map_user_info->profile_image;
        }
        if($map_user_info->latitude!='' ){
          $email = $map_user_info->email;
          $cat_image = $map_user_info->cat_image; 
          $username = $map_user_info->username;
          $latitude = $map_user_info->latitude;
          $longitude = $map_user_info->longitude;
          if(!empty(Auth::user())){
            $profile_url =url('/profile/'.$user_id);
            $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=', $user_id)->where('block_status','=','1')->first();
            if(!empty($userblock)) {
              $image =" ";
            }else {
              if($cat_image==''){
                $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
              }else {
                $image = url('/public')."/images/categoryimage/".$cat_image;
              }
            }
          } else {
            //$profile_url =url('/login?'.$user_id);
            $profile_url =url('/userprofile/'.$user_id);
            if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }
          }
           echo '["'.$username.'", '.$latitude.', '.$longitude.', "'.$image.'"],';
          }
      } ?>  
    ];
                        
    var infoWindowContent = [

      <?php $intpart = '';
      foreach($mapusers as $map_user_info) {
        if(empty($map_user_info->profile_image)){
          $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
        }
        else{
          $image_path =  url('/public')."/images/profileimage/".$map_user_info->profile_image;
        }
        if($map_user_info->latitude!='' ){
            $lat_str1 .= $map_user_info->latitude.',';
            $long_str1 .= $map_user_info->longitude.',';
        }
        $lat_str = explode(',',rtrim($lat_str1,","));
        $long_str = explode(',',rtrim($long_str1,','));
        $data = array_combine($lat_str,$long_str);
        $latitude_center =  $frwdLat;
        $longitude_center = $frwdLng;
        if($map_user_info->latitude!='' ){
          $latitude = $map_user_info->latitude;
          $longitude = $map_user_info->longitude;
          $user_id = $map_user_info->id;
          $username = $map_user_info->username;
          $city = $map_user_info->city;
          if($map_user_info->neighborhood!=''){
            $neighborhood = $map_user_info->neighborhood;
          } else {
            $neighborhood = '';
          }
          $category_name = $map_user_info->cat_name;
          $email = $map_user_info->email;
          $cat_image = $map_user_info->cat_image; 

          if(!empty(Auth::user())){
            $profile_url =url('/profile/'.$user_id);
            $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=', $user_id)->where('block_status','=','1')->first();
            if(!empty($userblock)) {
              $image =" ";
            }else {
              if($cat_image==''){
                $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
              }else {
                $image = url('/public')."/images/categoryimage/".$cat_image;
              }
            }
          } else {
            //$profile_url =url('/login?'.$user_id);
            $profile_url =url('/userprofile/'.$user_id);
            if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }
          } ?>

          
        ['<div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodytext"><a class="user_txt" href="<?php echo $profile_url; ?>" ><?php echo ucfirst($username); ?></a>' +
        '<div><span class="mapheading">Bairro:</span> <?php echo $neighborhood; ?></div><div class=""><span class="mapheading"> Serviço: </span> <?php echo $category_name; ?></div></a><div class="mapbodyrating"><span class="mapheading"><?php if($map_user_info->profile_rating){
        $rating = number_format($map_user_info->profile_rating,1);
        $intpart = floor ( $rating );
        $fraction = $rating - $intpart;
        for($x=1;$x<=$map_user_info->profile_rating;$x++) { ?> <i class="fa fa-star"></i> <?php } if (strpos($map_user_info->profile_rating,'.')) {?> <i class="fa fa-star-half-o" aria-hidden="true"></i> <?php $x++; }  while($x <= 5) {?> <i class="fa fa-star-o"></i> <?php  $x++; } } else { for($z=1;$z<=5;$z++) { ?> <i class="fa fa-star-o"></i> <?php }  }?> </span><a href="<?php echo $profile_url; ?>" class="raitingread">Mais info</a></div></div><div>','<?php echo $image; ?>'],

        <?php }
        } ?>  

    ];
        
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            icon: markers[i][3]
        });
        bounds.extend(marker.position);
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        map.fitBounds(bounds);
    }
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(13);
        this.setCenter({lat: <?php echo $frwdLat; ?>, lng: <?php echo $frwdLng; ?>})
        google.maps.event.removeListener(boundsListener);
    });
    
}

google.maps.event.addDomListener(window, 'load', initMaps);
</script>
<script>

document.getElementById("amount").onkeyup = function(){
  this.value = this.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

</script>
<script type="text/javascript">
 function initialize() {
    var input = document.getElementById('autocomplete');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setComponentRestrictions({'country': ['br']});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.address_components);
        document.getElementById('autocomplete').innerHTML = place.formatted_address;
        document.getElementById("searchFrm").submit();
    });
 }
 google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style type="text/css">
.mail-images {
  width: 24%;
  margin-top: 0%;
  margin-left: 0%;
}
.mapbodytext {
  color:#000;
  padding: 3px 15px;
}
.mapheading {
  color: #000 !important;
}
@media screen and (max-width:767px){
.resul-part {
    position: inherit !important;
    padding: 4px 13px;
}
}
</style>
@stop