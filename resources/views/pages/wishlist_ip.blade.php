@extends('layouts.default-header')
@section('title', 'My Wishlist')
@section('content')
<section class="maincontent searchbarbg">
  <div class="col-sm-12 pagecontent nopadding">
    <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
          <h1>My Wishlist</h1><br>
          <?php $count_wish_listusers = count($wish_listusers); ?>
          @if($count_wish_listusers>0)
          <h1 class="crete_account"><a href="{{ url('/register') }}">Create free account</a></h1><br>
          <p class="wiship_msg">This will let you find it again later, even after you close the browser.<br>
            Already a member? <a href="{{ url('/login') }}">Log in here</a>
          </p>
          @else
          <h1>Feeling empty in here!</h1>
          @endif
        </div>
      </div>
    </div>
    <div class="searchheaderdiv">
      <div class="container">
        <div class="col-sm-12">
          <!--  <div class="alert alert-success alert-block success_show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>User Removed From Wish List</strong>
          </div> -->
          <div class="searchresults">
            @if($count_wish_listusers>0)
            @foreach($wish_listusers as $wish_user_info)
            <?php
            /*check user exist in provider list */
            $user_id = $wish_user_info->wish_profile_id;
            $user_provider_exist = DB::table('users')
            ->Where('id', $user_id)
            ->Where('is_active', '1')
            ->Where('is_deleted', '0')
            ->Where('approved_by_admin', '1')
            ->Where('user_role', 'Provider')
            ->count();
            if($user_provider_exist>0){
            /*check user exist in provider list */
            /* user provider info list */
            $user_info = DB::table('users')
            ->Where('id', $user_id)
            ->Where('is_active', '1')
            ->Where('is_deleted', '0')
            ->Where('approved_by_admin', '1')
            ->Where('user_role', 'Provider')
            ->first();
            $profile_image = $user_info->profile_image;
            $profile_rating = $user_info->profile_rating;
            $lat = $user_info->latitude;
            $lon = $user_info->longitude;
            $userid = $user_info->id;
            $user_role = $user_info->user_role;
            $distance_info = DB::select("SELECT * FROM
            (SELECT *, ('6371'  *  acos(cos(radians('$lat')) * cos(radians(latitude)) *
            cos(radians(longitude) - radians('$lon')) +
            sin(radians('$lat')) * sin(radians(latitude))))
            AS distance
            FROM users WHERE user_role = 'Provider' and id='$userid') AS distances
            ORDER BY distance");
            foreach($distance_info as $distances) {
            $distance_mi =  round($distances->distance, 2);
            }
            ?>
            <div class="col-sm-4 searchresultdiv" id="unwish_{{ $user_info->id }}">
              <div class="searchresultbg">
                @if($profile_image=='')
                <img src="{{ url('/public/images/') }}/result1.jpg" class="img-responsive">
                @else
                <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-responsive">
                @endif
                <div class="wishlistbox">
                  <div class="wishlistdiv">
                    <div class="discountdiv">
                      <!--  <span>5% discount</span> -->
                    </div>
                    <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                      <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                      <input type="hidden" name="userip" id="userip" value="{{ $wish_user_info->ip_address }}">
                      <i class="fa fa-heart removewhishlist_ip"></i>
                    </div>
                  </div>
                  <div class="resultcntntbox">
                    <div class="row">
                      <div class="col-sm-7 resultbox_title">
                        <h4><a href="{{ url('/profile') }}/{{ $user_info->id }}">{{ ucfirst($user_info->name) }}</a><span></span></h4>
                      </div>
                      <div class="col-sm-5 startdiv">
                        @if($profile_rating>1)
                        <i class="fa fa-star"></i>@endif
                        @if($profile_rating>2)
                        <i class="fa fa-star"></i>@endif
                        @if($profile_rating>3)
                        <i class="fa fa-star"></i>@endif
                        @if($profile_rating>4)
                        <i class="fa fa-star"></i>@endif
                        @if($profile_rating==5)
                        <i class="fa fa-star"></i>@endif
                      </div>
                    </div>
                    <p>{{ $user_info->description }}</p>
                    <span class="amtdiv">$ {{ $user_info->price }}<span>/per {{ $user_info->price_type }}</span></span>
                  </div>
                </div>
              </div>
            </div>
            <?php
            /* user provider info list */
            }
            ?>
            @endforeach
            <br>
            <div class="pagination_section col-sm-6 col-md-6">
              {{ $wish_listusers->appends(Request::only('show'))->links() }}
            </div>
            @else
            <h1 style='text-align: center;'>Oops! Your Wishlist is Empty</h1>
            <p align="center"> You can Find New service Provider And Add them to Your wishlist</p>
            <div class="provider_btn_div">
              <a href="/home"><button type="button" class="Find_provider " name="Find service Provider Here">Find service Provider Here</button></a>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style type="text/css">.wishlisticon .fa-heart { cursor: pointer !important; }
</style>
<script>
window.setTimeout(function() {
$(".alert").fadeTo(500, 0).slideUp(500, function(){
$(this).remove();
});
}, 4000);
</script>
@stop