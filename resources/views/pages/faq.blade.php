@extends('layouts.default-header')
@section('title'){!! $menu_data->meta_title !!} @stop
@section('description'){!! $menu_data->meta_description !!} @stop
@section('keywords'){!! $menu_data->meta_keywords !!} @stop
@section('content')

<div class="container Privacy">
                   <h1 class="page_title">Perguntas Frequentes</h1>
                   <div class="General wow fadeInUp animated" style="visibility: visible;">

                    <h2>Por que preciso de uma conta no NaVizinhança?</h2>
                    <p>Ao se cadastrar, o usuário passa a ter a chance de buscar prestadores de serviços ou de oferecerem seus serviços. Parte do acesso pode ser feito sem necessidade de login e senha. Entretanto, por meio da identificação do usuário, disponibilizamos acesso completo à plataforma, tais como solicitação de serviço para o profissional, lista das mensagens que geraram a solicitação de serviço, possibilidade de avaliação dos usuários (tanto prestadores de serviços quanto contratante), e assim por diante.</p>

                  <h2>Posso me cadastrar para mais de um tipo de prestação de serviço no NaVizinhança?</h2>
                  <p>Atualmente o prestador de serviço precisa optar por apenas uma categoria de serviço (exemplo: Babá). Caso você se enquadre em mais de uma categoria, selecione a que você considera ser a principal. Lembre-se que os usuários avaliarão o seu serviço. Sendo assim, capriche na escolha.</p>

                  <h2>Sou prestador de serviço. Pago para me cadastrar?</h2>
                  <p>Não. O prestador de serviço não é cobrado pelo cadastro no site.</p>

                  <h2>Me cadastrei como contratante, mas agora quero disponibilizar meus serviços. Ou vice-versa, era prestador de serviço, mas agora quero contratar alguém. Como faço?</h2>
                  <p>É simples. Acesse o menu, onde está seu nome, disponível do lado direito na parte superior da tela. Ao clicar nele, uma das opções é “Mudar de função”. Ao clicar nela, você poderá mudar o perfil de contratante para prestador de serviço e vice-versa.</p>
                  <p>Para os prestadores de serviço, pedimos mais informações que para os contratantes, tais como: cursos e certificados, e informações adicionais (um breve descritivo) para que o contratante possa te conhecer melhor, além de informações referentes ao serviço que você deseja se cadastrar. Ao selecionar “Mudar de função” o site te guiará para o preenchimento das informações necessárias.</p>

                  <h2>Alguma dica para a foto que devo usar no site?</h2>
                  <p>Por tratar-se de um site com fins profissionais, sugerimos usar uma foto com fundo branco, que permita uma boa visualização do seu rosto, até mesmo para identificação quando necessário. Evite exagerar nos decotes, excesso de maquiagem, mostrar bebidas ou ambiente de fundo bagunçado... enfim, certifique-se que a foto escolhida irá passar uma boa imagem do seu profissionalismo.</p>

                  <h2>Tenho referência de serviços anteriores prestados. Como consigo adicionar as avaliações para melhor pontuação do meu perfil no NaVizinhança?</h2>
                  <p>Peça para que a pessoa que irá te avaliar se cadastrar no NaVizinhança. Ela deverá selecionar você (use os filtros ou o mapa, como for mais fácil) e clicar em “Contatar”. Quando você receber um e-mail com uma solicitação de serviço, a aceite. A partir desse momento você poderá ser avaliado por esse contratante.</p>

                </div>
 
    </div>

@stop
