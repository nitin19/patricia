@extends('layouts.default-header')

@section('title', 'Home')

@section('content')

<!--- Include the above in your HEAD tag -->
<div class="advertisement_bg">
<div class="container">
<div class="advertisement_sec">
	<div class="row">
	<div class="col-sm-3 adver_left">
		<div class="adver_srchfilters">
			<h3>Search Filters</h3>
			<form class="filterform">
			<label class="radiofilter">One
  <input type="radio" checked="checked" name="radio">
  <span class="checkmark"></span>
</label>
<label class="radiofilter">Two
  <input type="radio" name="radio">
  <span class="checkmark"></span>
</label>
<div class="form-group">
	<label>Activities</label>
	<input type="text" name="activities" class="form-control" id="activities">
</div>
<div class="form-group">
<label>Price</label>
<section class="range-slider">
  <input value="500" min="500" max="50000" step="500" type="range">
  <input value="50000" min="500" max="50000" step="500" type="range">
  <span class="rangeValues"></span>
</section>
</div>
<div class="form-group">
	<label>Date and Time</label>
	<input type="text" name="datetime" class="form-control" id="datetime">
</div>
<div class="form-group srchfilter">
	<label>Name</label>
	<input type="text" name="search" class="form-control" id="search">
	<button><i class="fa fa-search"></i></button>
</div>

	</form>
	</div>
</div>
<div class="col-sm-9 adver_right">
<div class="searchbar">
	<input type="text" class="form-control" id="searchinput">
	<button>Search</button>
</div>
<div class="col-sm-12 nopadding">
	<div class="row">
	<div class="col-sm-4 adver_blk">
		<div class="adver_blkbg">
			<div class="adver_img">
			<img src="http://navizinhanca.com/public/images/gravtar.jpeg">
		</div>
			<div class="col-sm-8 adver_blkleft">
				<h3>Neusa</h3>
				<p><i class="fa fa-map-marker"></i>8.0 km from you</p>
			</div>
			<div class="col-sm-4 adver_blkright">
				<h4>Daily</h4>
				<h2>Rs 90</h2>
			</div>
			<div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
				<div class="col-sm-6">
					<h5>Evaluation</h5>
				</div>
				<div class="col-sm-6 ratingdiv text-right">
					<p><i class="fa fa-star"></i>New</p>
				</div>
      </div>
      </div>
			</div>
			<div class="col-sm-12 schedulebtn">
				<a href="#">Schedule</a>
			</div>
			</div>
		</div>
    <div class="col-sm-4 adver_blk">
    <div class="adver_blkbg">
      <div class="adver_img">
      <img src="http://navizinhanca.com/public/images/gravtar.jpeg">
    </div>
      <div class="col-sm-8 adver_blkleft">
        <h3>Neusa</h3>
        <p><i class="fa fa-map-marker"></i>8.0 km from you</p>
      </div>
      <div class="col-sm-4 adver_blkright">
        <h4>Daily</h4>
        <h2>Rs 90</h2>
      </div>
      <div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
        <div class="col-sm-6">
          <h5>Evaluation</h5>
        </div>
        <div class="col-sm-6 ratingdiv text-right">
          <p><i class="fa fa-star"></i>New</p>
        </div>
      </div>
      </div>
      </div>
      <div class="col-sm-12 schedulebtn">
        <a href="#">Schedule</a>
      </div>
      </div>
    </div>
    <div class="col-sm-4 adver_blk">
    <div class="adver_blkbg">
      <div class="adver_img">
      <img src="http://navizinhanca.com/public/images/gravtar.jpeg">
    </div>
      <div class="col-sm-8 adver_blkleft">
        <h3>Neusa</h3>
        <p><i class="fa fa-map-marker"></i>8.0 km from you</p>
      </div>
      <div class="col-sm-4 adver_blkright">
        <h4>Daily</h4>
        <h2>Rs 90</h2>
      </div>
      <div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
        <div class="col-sm-6">
          <h5>Evaluation</h5>
        </div>
        <div class="col-sm-6 ratingdiv text-right">
          <p><i class="fa fa-star"></i>New</p>
        </div>
      </div>
      </div>
      </div>
      <div class="col-sm-12 schedulebtn">
        <a href="#">Schedule</a>
      </div>
      </div>
    </div>
  <div class="col-sm-4 adver_blk">
    <div class="adver_blkbg">
      <div class="adver_img">
      <img src="http://navizinhanca.com/public/images/gravtar.jpeg">
    </div>
      <div class="col-sm-8 adver_blkleft">
        <h3>Neusa</h3>
        <p><i class="fa fa-map-marker"></i>8.0 km from you</p>
      </div>
      <div class="col-sm-4 adver_blkright">
        <h4>Daily</h4>
        <h2>Rs 90</h2>
      </div>
      <div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
        <div class="col-sm-6">
          <h5>Evaluation</h5>
        </div>
        <div class="col-sm-6 ratingdiv text-right">
          <p><i class="fa fa-star"></i>New</p>
        </div>
      </div>
      </div>
      </div>
      <div class="col-sm-12 schedulebtn">
        <a href="#">Schedule</a>
      </div>
      </div>
    </div>
    <div class="col-sm-4 adver_blk">
    <div class="adver_blkbg">
      <div class="adver_img">
      <img src="http://navizinhanca.com/public/images/gravtar.jpeg">
    </div>
      <div class="col-sm-8 adver_blkleft">
        <h3>Neusa</h3>
        <p><i class="fa fa-map-marker"></i>8.0 km from you</p>
      </div>
      <div class="col-sm-4 adver_blkright">
        <h4>Daily</h4>
        <h2>Rs 90</h2>
      </div>
      <div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
        <div class="col-sm-6">
          <h5>Evaluation</h5>
        </div>
        <div class="col-sm-6 ratingdiv text-right">
          <p><i class="fa fa-star"></i>New</p>
        </div>
      </div>
      </div>
      </div>
      <div class="col-sm-12 schedulebtn">
        <a href="#">Schedule</a>
      </div>
      </div>
    </div>
    <div class="col-sm-4 adver_blk">
    <div class="adver_blkbg">
      <div class="adver_img">
      <img src="http://navizinhanca.com/public/images/gravtar.jpeg">
    </div>
      <div class="col-sm-8 adver_blkleft">
        <h3>Neusa</h3>
        <p><i class="fa fa-map-marker"></i>8.0 km from you</p>
      </div>
      <div class="col-sm-4 adver_blkright">
        <h4>Daily</h4>
        <h2>Rs 90</h2>
      </div>
      <div class="col-sm-12 adver_bottom">
        <div class="adver_bottom_bg">
          <div class="row">
        <div class="col-sm-6">
          <h5>Evaluation</h5>
        </div>
        <div class="col-sm-6 ratingdiv text-right">
          <p><i class="fa fa-star"></i>New</p>
        </div>
      </div>
      </div>
      </div>
      <div class="col-sm-12 schedulebtn">
        <a href="#">Schedule</a>
      </div>
      </div>
    </div>
  </div>
	</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
section.range-slider {
  position: relative;
  width: 100%;
  height: 300px;
  float: left;
  text-align: center;
}
section.range-slider input[type="range"] {
  pointer-events: none;
  position: absolute;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  border: none;
  border-radius: 14px;
  background: #F1EFEF;
  box-shadow: inset 0 1px 0 0 #cdc6c6, inset 0 -1px 0 0 #d9d4d4;
  -webkit-box-shadow: inset 0 1px 0 0 #cdc6c6, inset 0 -1px 0 0 #d9d4d4;
  overflow: hidden;
  left: 0;
  top: 0px;
  width: 100%;
  outline: none;
  height: 20px;
  margin: 0;
  padding: 0;
}
section.range-slider input[type="range"]::-webkit-slider-thumb {
  pointer-events: all;
  position: relative;
  z-index: 1;
  outline: 0;
  -webkit-appearance: none;
  width: 20px;
  height: 20px;
  border: none;
  border-radius: 14px;
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #dad8da), color-stop(100%, #413F41));
  /* android <= 2.2 */
  background-image: -webkit-linear-gradient(top, #dad8da 0, #413F41 100%);
  /* older mobile safari and android > 2.2 */
  background-image: linear-gradient(to bottom, #dad8da 0, #413F41 100%);
  /* W3C */
}
section.range-slider input[type="range"]::-moz-range-thumb {
  pointer-events: all;
  position: relative;
  z-index: 10;
  -moz-appearance: none;
  width: 20px;
  height: 20px;
  border: none;
  border-radius: 14px;
  background-image: linear-gradient(to bottom, #dad8da 0, #413F41 100%);
  /* W3C */
}
section.range-slider input[type="range"]::-ms-thumb {
  pointer-events: all;
  position: relative;
  z-index: 10;
  -ms-appearance: none;
  width: 20px;
  height: 20px;
  border-radius: 14px;
  border: 0;
  background-image: linear-gradient(to bottom, #dad8da 0, #413F41 100%);
  /* W3C */
}
section.range-slider input[type=range]::-moz-range-track {
  position: relative;
  z-index: -1;
  background-color: black;
  border: 0;
}
section.range-slider input[type=range]:last-of-type::-moz-range-track {
  -moz-appearance: none;
  background: none transparent;
  border: 0;
}
section.range-slider input[type=range]::-moz-focus-outer {
  border: 0;
}

</style>
<script type="text/javascript">
	function getVals(){
  // Get slider values
  var parent = this.parentNode;
  var slides = parent.getElementsByTagName("input");
    var slide1 = parseFloat( slides[0].value );
    var slide2 = parseFloat( slides[1].value );
  // Neither slider will clip the other, so make sure we determine which is larger
  if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }
  
  var displayElement = parent.getElementsByClassName("rangeValues")[0];
      displayElement.innerHTML = "$ " + slide1 + "k - $" + slide2 + "k";
}

window.onload = function(){
  // Initialize Sliders
  var sliderSections = document.getElementsByClassName("range-slider");
      for( var x = 0; x < sliderSections.length; x++ ){
        var sliders = sliderSections[x].getElementsByTagName("input");
        for( var y = 0; y < sliders.length; y++ ){
          if( sliders[y].type ==="range" ){
            sliders[y].oninput = getVals;
            // Manually trigger event first time to display values
            sliders[y].oninput();
          }
        }
      }
}
</script>
@stop
