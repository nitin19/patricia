  @extends('layouts.default-header')
  @section('title', 'Termo de Uso | NaVizinhança')
  @section('description', 'Termos de uso do NaVizinhança.')
  @section('keywords','termos de uso, NaVizinhança, Na Vizinhança, condições, regras')
  @section('content')
  <div class="top_div contactbanner">
  <div class="container">
<h1>Termos & Condições de Uso</h1>
</div>
</div>
     <div class="container">
          <div class="termbox">
            <h2>Termos & Condições</h2>
          </div>
           
          <div class="General termcontent">
            <p>Por favor, LEIA COM ATENÇÃO o seguinte acordo de termos e condições gerais de uso uma vez que o uso de www.navizinhanca.com e os seus serviços depende da ACEITAÇÃO DESTES TERMOS. Ao cadastrar-se ou utilizar os nossos serviços, você CONCORDA QUE LEU E COMPREENDEU O ACORDO. Caso você não concorde com estes Termos e Condições Gerais de Uso, em partes ou na sua totalidade, você não deveria utilizar os serviços do NAVIZINHANÇA.</p>
          </div>

          <div class="termbox">
            <h2>Introdução</h2>
          </div>
           
          <div class="General termcontent">
            <p>Este documento é um acordo entre o Usuário e o NAVIZINHANÇA. O NAVIZINHANÇA TECNOLOGIA DA INFORMAÇÃO LTDA é uma sociedade empresária limitada, inscrita no CNPJ/MF 35.339.157/0001-20, doravante denominada NAVIZINHANÇA. O conteúdo destes Termos e Condições Gerais de Uso pode ser alterado ou ajustado quando considerado necessário pela empresa. Qualquer alteração nestes termos será informada ao usuário por meio do website.</p>

            <p>Com a aceitação dos termos de uso do acordo, o usuário do site www.navizinhanca.com fica ciente e de acordo com todos os itens de contrato descritos neste documento de Termos e Condições Gerais de Uso e quaisquer alterações que este documento poderá sofrer futuramente.</p>

            <p>Os serviços oferecidos pelo NAVIZINHANÇA poderão ser diferentes para cada região do país. Estes Termos deverão ser interpretados de acordo com a região em que foi efetuado o cadastro do Prestador de Serviço.</p>
          </div>

           <div class="General">
              <h2>Definições usadas nestes Termos e Condições</h2>

             <ul class="same-size change-color">
              <li>USUÁRIO(S): pessoas físicas e jurídicas que acessam e usam os serviços providos no site www.navizinhanca.com
              </li>
              <li>
              SITE: www.navizinhanca.com: Refere-se ao site www.navizinhanca.com, microsite e aplicativos.
              </li>
              <li>
              PRESTADORES DE SERVIÇO(S): pessoas físicas e jurídicas que se cadastram no site www.navizinhanca.com com a finalidade de divulgar seus serviços.
              </li>
              <li>
              CONTRATANTE(S): pessoas físicas e jurídicas que se cadastram no site www.navizinhanca.com com a finalidade de encontrar prestadores de serviços.
              </li>
              <li> 
              SERVIÇOS: Refere-se a quaisquer serviços oferecidos pelo NAVIZINHANÇA, tanto pelo site como por qualquer outro meio de comunicação da empresa.
              </li>
              <li>
              COOKIES: Recurso que possibilita a identificação de usuários e o direcionamento de documentos de acordo com parâmetros estabelecidos previamente por meio dos documentos de Política de Privacidade e Condições gerais de uso.
              </li>
            </ul>
          </div>

          <div class="General">
              <h2>Questões Gerais</h2>

             <ul class="same-size change-color">
              <li>O uso do website ou dos serviços oferecidos pelo NAVIZINHANÇA está sujeito a aceitação dos termos e à Política de Privacidade.
              </li>
              <li>
              O NAVIZINHANÇA e seus serviços estão sujeitos à legislação vigente no Brasil, a empresa não se responsabiliza pelo uso ou consequências do uso fora do território nacional.
              </li>
            </ul>
          </div>

          <div class="General">
            <h2>Objeto</h2>
            <p>Os serviços objeto dos presentes Termos consistem em:</p>
            <ul class="same-size change-color">
              <li>Disponibilizar espaço de divulgação para os Prestadores de Serviço(s);</li>
              <li>Permitir aos Contratantes que utilizem o Site para livremente e sem coação busquem orçamentos de Prestadores conforme a vossa necessidade;
              </li>
              <li>Propiciar o contato direto entre Prestadores de Serviços e Contratantes com interesse em adquirir os Serviços, por meio da divulgação das informações de contato de uma parte à outra;
              </li> 
              <li>Disponibilizar informações que possam ser úteis para Prestadores de Serviços e/ou Contratantes;
              </li> 
              <li>Permitir divulgação do Site para terceiros que possam ter interesse em se tornar Usuários;
              </li>
              <li>Caso seja de interesse dos Usuários, oferecer serviço de vendas de pacotes de visibilidade ou disponibilizar opções para meios de pagamento entre as partes;
              </li>
              <li>O NaVizinhança possibilita que os Usuários negociem entre si diretamente, sem intervir no contato, na negociação ou na efetivação dos negócios, não sendo, nesta qualidade, fornecedor de quaisquer Serviços anunciados por seus Usuários no Site;
              </li>
              <li>O NaVizinhança não impõe ou interfere em qualquer negociação sobre condições, valor, qualidade, forma ou prazo na contratação entre os Contratantes e Prestadores, tampouco garante a qualidade, ou entrega dos Serviços contratados entre os Usuários;
              </li>
              <li>Uma vez realizado o cadastro, o Usuário poderá utilizar todos os serviços disponibilizados no Site disponíveis para sua região, declarando, para tanto, ter lido, compreendido e aceitado os Termos de Uso e a Política de Privacidade.
              </li>
            </ul>
          </div>

          <div class="General">
            <h2>Cadastro, contas, imagens, segurança e encerramento de contas</h2>
            <ul class="same-size change-color">
              <li>Os serviços do NAVIZINHANÇA estão disponíveis para pessoas físicas e pessoas jurídicas regularmente inscritas nos cadastros de contribuintes federal e estaduais que tenham capacidade legal para contratá-los. Não podem utilizá-los, assim, pessoas que não gozem dessa capacidade, inclusive menores de idade ou pessoas que tenham sido inabilitadas do NAVIZINHANÇA, temporária ou definitivamente. Ficam, desde já, os Usuários advertidos das sanções legais cominadas no Código Civil.</li>

              <li>Ao término do cadastro, o usuário estará automaticamente cadastrado para recebimento de newsletters do NAVIZINHANÇA. O NAVIZINHANÇA reserva o direito de negar qualquer cadastro, assim como de deletar qualquer conta a qualquer momento, sem um aviso prévio.
              </li>
              <li>Dependendo da funcionalidade específica do nosso site, o usuário precisará, a qualquer momento, registrar-se para a utilização de funcionalidades ou para acessar grande parte dos nossos serviços. O log-in poderá ser realizado através do registro de uma conta “dentro” do site do NAVIZINHANÇA (“entrar”, “cadastre-se”) ou fazendo o login através de redes sociais como o Facebook ou Google +.
              </li> 
              <li>Todas as contas devem ser registradas com um endereço de e-mail pessoal válido e que seja acessado regularmente. Todas as contas que tenham sido registradas com o endereço de e-mail de outra pessoa ou com endereços de e-mail temporários podem ser bloqueadas sem aviso prévio. Podemos exigir que os usuários revalidem a sua conta a qualquer tempo, se acreditarmos que a mesma seja um endereço de e-mail inválido.

              </li> 
              <li>O NAVIZINHANÇA se reserva o direito de utilizar todos os meios válidos e possíveis para identificar seus Usuários, bem como de solicitar dados adicionais e documentos que estime serem pertinentes a fim de conferir os dados pessoais informados.
              </li>
              <li>Ao realizar o registro em nosso site, o usuário deverá cadastrar uma senha pessoal e intransferível. Deverá mantê-la de forma confidencial e nos informar imediatamente se qualquer terceiro não autorizado tiver acesso ou tomar conhecimento de sua senha pessoal, se houver qualquer uso não autorizado do seu endereço de e-mail ou qualquer violação de segurança que venha a seu conhecimento. O Usuário será o único responsável pelas operações efetuadas em sua conta, uma vez que o acesso só será possível mediante a utilização de senha de seu exclusivo conhecimento.
              </li>
              <li>É de exclusiva responsabilidade dos Usuários fornecer, atualizar e garantir a veracidade dos dados cadastrais, não cabendo ao NAVIZINHANÇA qualquer tipo de responsabilidade civil e criminal.
              </li>
              <li>O Usuário deverá utilizar uma foto sua atualizada. O NAVIZINHANÇA poderá editar a imagem. Esta poderá ser utilizada pelo NAVIZINHANÇA de forma irrestrita, não onerosa e por prazo indeterminado.
              </li>
              <li>O usuário pode cancelar o recebimento de newsletters e seu cadastro no NAVIZINHANÇA. Para tal, este deverá acessar o link de descadastramento no próprio e-mail de publicidade ou enviar e-mail para contato@navizinhanca.com.
              </li>
              <li>O usuário cadastrado no site do NAVIZINHANÇA, receberá do NAVIZINHANÇA e-mails de solicitação de serviço ou respostas às solicitações feitas. Para cancelar o recebimento dos e-mails, o usuário tem que se descadastrar no site e não poderá se cadastrar novamente, perdendo também o histórico e registros de usuários feitos em sua conta. Para acesso à opção de descadastramento, o usuário deve entrar em seu perfil de usuário (fazer o login) e acessar a opção “Sua conta” no “Painel de Controle” e selecionar o motivo para o descadastramento. O NAVIZINHANÇA se reserva ao direito de analisar a solicitação antes da exclusão do usuário.
              </li>
              <li>Em nenhuma hipótese será permitida a cessão, a venda, o aluguel ou outra forma de transferência da conta.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Tarifas</h2>
            <ul class="same-size change-color">
              <li>O cadastramento dos Usuários no NAVIZINHANÇA é gratuito. Contudo para determinados serviços prestados relativos a impulsionamento ou destaques e visibilidade em anúncios dos Prestadores de Serviços será cobrada uma tarifa.</li>

              <li>O Contratante será tarifado quando a contratação de um Prestador de Serviço for efetivada. A tarifação se dará por meio de mensalidade fixa ou taxa operacional sobre o valor acordado com o Prestador de Serviço, conforme opção assinalada no Site.
              </li>
              <li>Os usuários Prestadores de Serviços e Contratante ao acordarem o serviço a ser executado e o valor final para execução do mesmo, deverão informar esse valor para o NAVIZINHANÇA. O referido valor será base de cálculo da taxa operacional, a qual estará destacada no Site quando da confirmação da contratação do serviço, a ser calculada com base no preço final acordado, e a taxa operacional deverá ser paga para o NAVIZINHANÇA.
              </li> 
              <li>O Contratante poderá efetuar o pagamento do serviço contratado diretamente para o Prestador de Serviço ou por meio do NAVIZINHANÇA. Quando a opção for para pagamento ao Prestador de Serviço, o valor da mensalidade ou da taxa operacional será o único valor cobrado do Contratante pelo NAVIZINHANÇA. Caso o pagamento ocorra no site, será descontado do Contratante tanto o valor do serviço a ser repassado bem como a mensalidade ou taxa operacional.
              </li> 

              <li>Após confirmado o aceite da prestação de serviços no NAVIZINHANÇA e a opção assinalada para tarifação for a taxa operacional, a mesma será cobrada e não será devolvida em caso de cancelamento do serviço e não será alterada em caso de mudança dos valores inicialmente informados.
              </li>
            </ul>
          </div>

          <div class="General">
            <h2>Obrigações do usuário</h2>
            <ul class="same-size change-color">
              <li>Como usuário você aceita enviar e receber informações, materiais e mensagens pertinentes ao site www.navizinhanca.com.</li>

              <li>O Prestador de Serviço deve ter capacidade legal para prestar o Serviço.</li>

              <li>Em virtude do NAVIZINHANÇA não figurar como parte nas transações de contratação dos Serviços que se realizam entre os Usuários, a responsabilidade por todas as obrigações delas decorrentes, sejam fiscais, trabalhistas, consumeristas ou de qualquer outra natureza, será exclusivamente do Contratante, do Prestador de Serviço ou de ambos, conforme o caso. Na hipótese de interpelação judicial que tenha como Réu o NAVIZINHANÇA, cujos fatos fundem-se em ações do Prestador de Serviço, este será chamado ao processo, devendo arcar com todos os ônus que daí decorram, incluindo despesas com taxas, emolumentos, acordos, honorários advocatícios entre outros.
              </li> 
              <li>O Prestador de Serviço deverá ter em mente que, na medida em que atue como um fornecedor de serviços, sua oferta o vincula, nos termos do artigo 30 do Código de Defesa do Consumidor e do artigo 429 do Código Civil, cujo cumprimento poderá ser exigido judicialmente pelo Contratante.
              </li> 

              <li>O NAVIZINHANÇA não se responsabiliza pelas obrigações tributárias que recaiam sobre as atividades dos Usuários. Assim como estabelece a legislação pertinente em vigor, o Contratante deverá exigir nota fiscal do Prestador em suas transações. O Prestador de Serviço, nos moldes da lei vigente, responsabilizar-se-á pelo cumprimento da integralidade das obrigações oriundas de suas atividades, notadamente aquelas referentes a tributos incidentes.
              </li>
              <li>Como usuário, você aceitará:</li>
              <p>o Não publicar conteúdos inapropriados no site.</p>
              <p>o Não enviar informações ou materiais que você não tenha o direito de uso.</p>
              <p>o Não receber ou distribuir arquivos do site postado por outros usuários que não possam ser distribuídos legalmente.</p>
              <p>o Não violar os direitos legais de terceiros ou das leis aplicáveis no Brasil.</p>
              <p>o Não violar o código de conduta aplicado diretamente no site do NAVIZINHANÇA e nos serviços desta.</p>
              <p>o Não violar os Termos e Condições Gerais de Uso ou qualquer outro acordo estabelecido com a empresa.</p>
              <p>o Manter apenas um cadastro no NAVIZINHANÇA.</p>
              <p>O usuário tem como responsabilidade verificar a veracidade dos dados e informações visualizadas no site www.navizinhanca.com, tomando assim, medidas necessárias para a proteção de seus dados de danos e atos fraudulentos.</p>
            </ul>
          </div>

          <div class="General">
            <h2>Dos serviços anunciados</h2>
            <ul class="same-size change-color">
              <li>O Prestador de Serviços oferecerá aos Contratantes seus serviços a partir de seu perfil. Seu perfil poderá conter um nome, foto atualizada, localização, qualificações e avaliações recebidas, preços sugeridos para determinados serviços bem como a descrição das atividades prestadas.</li>

              <li>O Prestador de Serviços se obriga a formular descrições com o máximo de clareza para que o Contratante compreenda a qualidade, forma, data de início e término e outras características impactantes do Serviço. Essa informação pode ser trocada por meio escrito dentro ou fora do Site do NAVIZINHANÇA, assim como por meio oral entre os Usuários.</li>

              <li>O NAVIZINHANÇA não se responsabiliza pelas informações prestadas pelo Prestador de Serviços, bem como os mesmos não são contratados, empregados, não possuindo vínculo de qualquer espécie.
              </li> 
              <li>O Contratante fica incumbido de observar no Site os dados disponibilizados sobre os Prestadores, tais como estimativa de preço, disponibilidade, comentários dos demais contratantes, avaliação, entre outros. Entretanto, a decisão pela escolha de um Prestador de Serviço dá-se após contato direto com o profissional, seja por meio do NAVIZINHANÇA ou fora do Site, não sendo o NAVIZINHANÇA responsável por qualquer divergência relacionada aos dados divulgados ou acordados.
              </li> 

              <li>Cabe somente aos Usuários as responsabilidades e as obrigações do serviço e condições acordados entre as partes. O NAVIZINHANÇA é isento de qualquer prejuízo resultante dos serviços contratados por intermédio do Site.
              </li>
              <li>Os valores de estimativa de preços descritos no Site pelos Prestadores de Serviço devem ser atualizados pelos mesmos. Divergências entre a informação descrita e o valor efetivamente acordado não são de responsabilidade do NAVIZINHANÇA. Caso o serviço seja contratado, o Contratante pode utilizar do campo comentários para descrever dados relevantes ou informar divergências para os demais Usuários.
              </li>
              <li>O NAVIZINHANÇA é um site de classificados online de Serviços, sendo proibida a veiculação de anúncios de serviços ilegais de acordo com a legislação vigente ou que possam ser considerados ofensivos a terceiros, o NAVIZINHANÇA não realiza uma curadoria prévia dos anúncios veiculados no Site.</li>
              <li>Qualquer Usuário ou pessoa física ou jurídica que se sentir ofendido por qualquer anúncio veiculado no Site poderá requisitar ao NAVIZINHANÇA que exclua aquele anúncio de seu Site, pelos seus canais de atendimento ou pelo e-mail contato@navizinhanca.com.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Sistema de qualificação dos prestadores de serviço(s)</h2>
            <ul class="same-size change-color">
              <li>Os Prestadores de Serviço aceitam e se submetem ao sistema de qualificação adotado pelo NAVIZINHANÇA.</li>

              <li>Os Prestadores de Serviço concordam que o NAVIZINHANÇA poderá cancelar, excluir ou suspender por tempo indeterminado cadastros que apresentem qualificações negativas de forma reiterada.</li>

              <li>Não assistirá aos Prestadores de Serviço qualquer tipo de indenização ou ressarcimento por perdas e danos, lucros cessantes e danos morais, em razão da qualificação atribuída aos serviços anunciados.
              </li> 

              <li>Tendo em vista que os comentários postados são opiniões pessoais dos Usuários, estes serão responsáveis pelas opiniões publicadas no Site, seja perante o NAVIZINHANÇA, perante os Prestadores de Serviços ou Contratantes, terceiros, órgãos governamentais ou demais Usuários do site, isentando o NAVIZINHANÇA de qualquer responsabilidade relativa à veiculação dos comentários.
              </li> 
            </ul>
          </div>

          <div class="General">
            <h2>Privacidade da informação</h2>
            <ul class="same-size change-color">
              <li>Todas as informações ou os dados pessoais prestados pelo Usuário do NAVIZINHANÇA são armazenados em servidores ou meios magnéticos de máxima segurança, com as devidas confidencialidades.</li>
              <li>O NAVIZINHANÇA poderá utilizar e ceder os dados fornecidos pelos Usuários e as informações relativas ao uso do Site, para quaisquer finalidades, incluindo, mas não se limitando a, fins comerciais, publicitários, educativos e jornalísticos.</li>
              <li>O NAVIZINHANÇA também poderá utilizar os dados dos Usuários e Parceiras para análises, estudos, elaboração de relatórios, tudo conforme necessário para o bom funcionamento e o desenvolvimento do Site e dos serviços oferecidos.
              </li> 
              <li>Os Usuários concordam que as suas informações pessoais serão enviadas para a outra Parte, Prestador de Serviço ou Contratante, para que seja possível a comunicação entre as Partes em caso de tentativa de contratação do serviço.
              </li> 
              <li>O apelido e a senha de cada Usuário servem para garantir a privacidade e a sua segurança. Não devendo serem compartilhados.</li>
              <li>O nome, e-mail e outros dados dos Usuários poderão ser utilizados para o envio de notificações, informações sobre a conta ou Serviços prestados, avisos sobre violações ao Termo e outras comunicações que NAVIZINHANÇA considerar necessárias.</li>
              <li>Ao fornecer o e-mail nos devidos campos de cadastro, o Usuário estará aceitando receber newsletters, promoções do site e comunicações entre Prestador de Serviço e Contratante pelo e-mail.</li>
              <li>O NAVIZINHANÇA prestará todas as informações requisitadas por órgãos públicos, desde que devidamente justificadas e compatíveis com a lei em vigor.</li>
              <li>O NAVIZINHANÇA utiliza cookies e softwares de monitoramento de seus Usuários para prover a seus Usuários a melhor navegação possível, baseado em suas necessidades, e para pesquisas internas. Tais cookies não coletam informações pessoais, apenas informam preferências de uso e de navegação de cada Usuário, além de prover a estatísticas e dados para aprimorar seus serviços.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Uso do site www.navizinhanca.com e seus serviços</h2>
            <ul class="same-size change-color">
              <li>O NAVIZINHANÇA não garante que os seus serviços ou site estão livres de erros de programação, porém, garante que tomará medidas para manter o sigilo de todos os dados contidos no website e nos serviços.</li>
              <li>O NAVIZINHANÇA não garante que o serviço funcionará de forma ininterrupta, podendo este ser restrito, suspenso ou terminado a qualquer momento.</li>

              <li>Reservamos o direito de modificar e apagar, caso seja necessário, qualquer informação ou dado a qualquer momento, sem aviso prévio.
              </li> 

              <li>Nada contido neste acordo de Termos e Condições Gerais de Uso pode ser utilizado para constituir uma parceria ou Joint Venture entre o usuário e o NAVIZINHANÇA.
              </li> 
              <li>O Usuário deverá aceitar ser identificado por meio dos sistemas utilizados pelo site, como Cookies e formulários.</li>

              <li>O NAVIZINHANÇA não se responsabiliza por eventuais danos decorrentes de erros no site ou pela indisponibilidade do serviço.</li>

              <li>Lembramos que toda comunicação no ambiente da internet deverá ser realizada com cautela pelo Usuário. O NAVIZINHANÇA se exime de toda e qualquer responsabilidade legal referente a esse processo.</li>

              <li>Dúvidas pertinentes e questões sobre a política de privacidade poderão ser consultadas no próprio site www.navizinhanca.com, na área de Política de Privacidade.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Restrições ao Usuário</h2>
            <ul class="same-size change-color">
              <li>O Usuário não poderá cadastrar informações falsas ou de propriedade de terceiros.</li>
              <li>Não poderá utilizar os serviços disponíveis para outros fins que não os propostos pela empresa.</li>
              <li>O Usuário não poderá apagar ou modificar dados e informações de terceiros.</li>
              <li>No cadastramento do Usuário, não poderá ser utilizado nomes que buscam semelhanças com o NAVIZINHANÇA, outros usuários ou anunciantes. Serão eliminados cadastros considerados ofensivos ou que infrinjam a legislação em vigor.</li> 
              <li>É terminantemente vedado aos Usuários, entre outras atitudes previstas nestes Termos, manipular, direta ou indiretamente, os preços dos Serviços anunciados.</li>
              <li>É proibido aos Prestadores de Serviço divulgar o mesmo anúncio, conteúdo, item ou serviço em mais de uma categoria e/ou de forma repetida. O NAVIZINHANÇA se reserva o direito de excluir anúncios ou serviços repetidos, assim como suspender ou excluir o cadastro do Prestador de Serviço responsável pela duplicidade.</li>
              <li>Os Usuários não poderão: (i) Obter, guardar, divulgar, comercializar e/ou utilizar dados pessoais sobre outros Usuários para fins comerciais ou ilícitos; (ii) Usar meios automáticos, incluindo spiders, robôs, crawlers, ferramentas de captação de dados ou similares para baixar dados do site (exceto ferramentas de busca na Internet e arquivos públicos não comerciais); (iii) Burlar, ou tentar burlar, de qualquer forma que seja, o sistema, mecanismo e/ou a plataforma do site; e (iv) incluir meios de contato como telefone, e-mail, endereço e outras formas de comunicação nos anúncios.</li>
              <li>Qualquer transmissão de arquivos nocivos ao sistema ou com potencial destrutivo para os seus Usuários que cause danos permanentes ou temporários é proibido.</li>
              <li>É vedada a utilização de dispositivo, software ou outro recurso que possa interferir nas atividades e nas operações do Site, bem como nos anúncios, nas descrições, nas contas ou em seus bancos de dados. Qualquer intromissão, tentativa de, ou atividade que viole ou contrarie as leis de direito de propriedade intelectual e as proibições estipuladas nestes Termos tornará o responsável passível de sofrer os efeitos das ações legais pertinentes, bem como das sanções aqui previstas, sendo ainda responsável por indenizar o NAVIZINHANÇA ou seus Usuários por eventuais danos causados.</li>
              <li>A violação de privacidade dos Usuários poderá originar em processos legais.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Suspensão e exclusão de contas</h2>
            <ul class="same-size change-color">
              <li>É proibida a criação de mais de um cadastro por Usuário. Em caso de multiplicidade de cadastros elaborados por um só Usuário, o NAVIZINHANÇA reserva se o direito de, a seu exclusivo critério e sem necessidade de prévia anuência dos ou comunicação aos Usuários, inabilitar todos os cadastros existentes e impedir eventuais cadastros futuros vinculados a estes.</li>
              <li>O NAVIZINHANÇA se reserva o direito de suspender o acesso do Usuário aos serviços ou ao site www.navizinhanca.com se o usuário infringir os termos contratados neste acordo. Nesse caso, o Usuário não terá direito a qualquer tipo de indenização ou ressarcimento por perdas e danos, lucros cessantes ou danos morais.</li>
              <li>Sem prejuízo de outras medidas, o NAVIZINHANÇA poderá, a seu exclusivo critério e sem necessidade de prévia anuência dos ou comunicação aos Usuários, advertir, suspender ou cancelar, temporária ou permanentemente, o cadastro ou os anúncios do Usuário, podendo aplicar sanção que impacte negativamente em sua reputação, a qualquer tempo, iniciando as ações legais cabíveis e suspendendo a prestação de seus serviços, se: (i) o Usuário não cumprir qualquer dispositivo destes Termos e as demais políticas do NAVIZINHANÇA; (ii) descumprir com seus deveres de Usuário; (iii) praticar atos delituosos ou criminais; (iv) não puder ser verificada a identidade do Usuário, qualquer informação fornecida por ele esteja incorreta ou se as informações prestadas levarem a crer que o cadastro seja falso ou de pessoa diversa; (v) o NAVIZINHANÇA entender que os anúncios ou qualquer outra atitude do Usuário tenham causado algum dano a terceiros ou ao NAVIZINHANÇA ou tenham a potencialidade de assim o fazer.</li>
              <li>Nos casos de suspensão, temporária ou permanente, do cadastro do Prestador de Serviço, todos os anúncios ativos e as ofertas realizadas serão automaticamente canceladas.</li> 
              <li>O NAVIZINHANÇA pode unilateralmente excluir o cadastro dos Usuários quando verificado que a conduta do Usuário é ou será prejudicial ou ofensiva a outros Usuários ou viole a legislação em vigor ou esses Termos.</li>
            </ul>
          </div>


          <div class="General">
            <h2>Limitações da Responsabilidade</h2>
            <ul class="same-size change-color">
              <p>O site www.navizinhanca.com e os seus serviços não se responsabilizam por qualquer prejuízo direto, indireto, punitivo, acidental ou por qualquer outra espécie de dano oriundo de:</p>
              <li>Incapacidade de uso do website e de seus serviços</li>
              <li>Falhas em sua conexão com a internet, com o seu provedor, no sistema, com o sistema de SMS, com a linha telefônica ou no servidor utilizados pelo Usuário, decorrentes de condutas de terceiros, caso fortuito ou força maior.</li>
              <li>Acesso não autorizado ou alterações na transmissão de dados do usuário.</li>
              <li>Qualquer outro assunto referente aos serviços, inclusive mas não restrito a, danos oriundos pela perda de conexão, receptação de arquivos corrompidos ou quaisquer danos pertinentes ao desempenho do website.</li> 
              <li>O NAVIZINHANÇA não é responsável pela compatibilidade entre o seu Site e hardwares de propriedade do Usuário. O Usuário deverá manter o seu equipamento atualizado e não poderá responsabilizar NaVizinhança caso o Site não seja acessível em equipamentos antiquados.</li>
              <li>O NAVIZINHANÇA também não será responsável por qualquer vírus, trojan, malware, spyware ou qualquer software que possa danificar, alterar as configurações ou infiltrar o equipamento do Usuário em decorrência do acesso, da utilização ou da navegação na internet, ou como consequência da transferência de dados, informações, arquivos, imagens, textos ou áudio.</li>
              <li>O usuário entende e aceita que materiais ou dados obtidos no website estão sujeitos a erros que podem gerar prejuízos ao mesmo. O usuário aceita a responsabilidade pelos danos nos próprios computadores e a perda de dados recorrente da utilização dos serviços do NAVIZINHANÇA.</li>
              <li>O NAVIZINHANÇA não se responsabiliza por vícios ou defeitos técnicos e/ou operacionais oriundos do sistema do Usuário ou de terceiros, não é responsável pela entrega dos Serviços anunciados pelos Prestadores de Serviço no Site.</li>
              <li>O NAVIZINHANÇA tampouco se responsabiliza pela existência, quantidade, qualidade, estado, integridade ou legitimidade dos Serviços oferecidos ou contratados pelos Usuários, assim como pela capacidade para contratar dos Usuários ou pela veracidade dos dados pessoais por eles fornecidos, não outorgando garantia por vícios ocultos ou aparentes nas negociações entre os Usuários. Cada Usuário conhece e aceita ser o único responsável pelos Serviços que anuncia ou pelas ofertas que realiza.</li>
              <li>O NAVIZINHANÇA não será responsável por ressarcir seus Usuários por quaisquer gastos com ligações telefônicas, pacotes de dados, SMS, mensagens, e-mails, correspondência ou qualquer outro valor despendido pelo Usuário em razão de contato com o NAVIZINHANÇA ou quaisquer outros Usuário, por qualquer motivo que o seja.</li>
              <li>O NAVIZINHANÇA não poderá ser responsabilizada pelo efetivo cumprimento das obrigações assumidas pelos Usuários. Os Usuários reconhecem e aceitam que, ao realizar negociações com outros Usuários, fazem-no por sua conta e risco.</li>
              <li>Em nenhum caso o NAVIZINHANÇA será responsável pelo lucro cessante ou por qualquer outro dano e/ou prejuízo que o Usuário possa sofrer devido às negociações realizadas ou não realizadas por meio do Site, decorrentes da conduta de outros Usuários.</li>
              <li>Por se tratar de negociações realizadas por meio eletrônico entre dois Usuários que não se conheciam previamente à negociação, o NAVIZINHANÇA recomenda que toda transação seja realizada com cautela e prudência.</li>
              <li>O NAVIZINHANÇA se reserva o direito de auxiliar e cooperar com qualquer autoridade judicial ou órgão governamental, podendo enviar informações cadastrais ou negociais de seus Usuários, quando considerar que seu auxílio ou cooperação sejam necessários para proteger seus Usuários, funcionários, colaboradores, administradores, sócios ou qualquer pessoa que possa ser prejudicada pela ação ou omissão combatida.</li>
              <li>O NAVIZINHANÇA não se responsabiliza pelas obrigações tributárias que recaiam na negociação e contratação entre o Prestador de Serviço e o Contratante, portanto, o Contratante deverá solicitar ao Prestador de Serviço os devidos documentos fiscais da negociação.</li>
              <li>O NAVIZINHANÇA não garante que os preços estimados informados pelo Prestador de Serviço estão corretos, o Contratante deverá especificar a necessidade do serviço e validar os valores com o Prestador de Serviço antes da contratação.</li>
              <li>Estes Termos não geram nenhum contrato de sociedade, de mandato, de franquia ou relação de trabalho entre o NAVIZINHANÇA e o Usuário. O Usuário manifesta ciência de que NaVizinhança não é parte de nenhuma transação realizada entre Usuários, nem possui controle algum sobre a qualidade, a segurança ou a legalidade dos Serviços anunciados pelos Usuários, sobre a veracidade ou a exatidão dos anúncios elaborados pelos Usuários, e sobre a capacidade dos Usuários para negociar.</li>
              <li>O NAVIZINHANÇA não pode assegurar o êxito de qualquer transação realizada entre Usuários, tampouco verificar a identidade ou os dados pessoais dos Usuários.</li>
            </ul>
          </div>

          <div class="General">
            <h2>Propriedade Intelectual</h2>
            <ul class="same-size change-color">
              <p>O NAVIZINHANÇA possui todos os direitos intelectuais pelos conteúdos contidos no Website, incluindo, sem limitação, todos os direitos de títulos, patentes, marcas, logos, modelos de serviço, nomes comerciais, design, segredos de marca e inovações contidas no site (patenteadas ou não), código fonte, banco de dados, textos, conteúdos, gráficos, ícones, e hyperlinks, a não ser que explicitamente mencionado que o conteúdo está sob posse de um terceiro. Ao concordar com este documento você estará concordando em não reproduzir ou distribuir quaisquer conteúdo do website do NAVIZINHANÇA e seus serviços sem a obtenção e consentimento da empresa.</p>
            </ul>
          </div>

          <div class="General">
            <h2>Indenização</h2>
            <ul class="same-size change-color">
              <p>O Usuário indenizará o NAVIZINHANÇA, suas filiais, empresas controladas, controladores diretos ou indiretos, diretores, administradores, colaboradores, representantes e empregados, inclusive quanto a honorários advocatícios, por qualquer demanda promovida por outros Usuários ou terceiros, decorrentes das atividades do primeiro no Site, de quaisquer descumprimentos, por aquele, dos Termos e das demais políticas de NaVizinhança ou, ainda, de qualquer violação, pelo Usuário, de lei ou de direitos de terceiros.</p>
            </ul>
          </div>

          <div class="General">
            <h2>Considerações Finais</h2>
            <ul class="same-size change-color">
              <p>Toda e qualquer comunicação realizada entre as partes sobre direitos ou obrigações contidas neste presente instrumento deverá assumir a forma escrita e será tida como eficazmente entregues no endereço que consta da introdução deste termo, se:</p>
              <ul class="same-size change-color">
                <li>Entregue em mãos mediante protocolo escrito por quem de direito represente a parte remetida.</li>
                <li>Enviados por serviço de “courier” mediante protocolo escrito ou se pelo correio, na forma de carta registrada (“AR”).</li>
                <li>Enviadas por e-mail, ou outro meio eletrônico hábil para tais comunicações.</li>
              </ul>
              <p>É de conveniência que as partes confirmem o recebimento de mensagens e arquivos documentais para todos os efeitos.</p>
              <p>Fica eleito o foro da Comarca de São Paulo, Estado de São Paulo, como competente para dirimir quaisquer controvérsias decorrentes deste documento de Termos e Condições Gerais de Uso independente de qualquer outro, por mais privilegiado que seja ou venha a ser.</p>
            </ul>
          </div>
         </div>
  @stop
