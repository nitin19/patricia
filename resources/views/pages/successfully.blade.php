@extends('layouts.default-header')

@section('title', 'Payment')

@section('content')
<section class="maincontent searchbarbg"> 
  <div class="col-sm-12 pagecontent nopadding">
    <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
      <h1>Thank you! Your payment was processed successfully.</h1>
    </div>
    </div>
    </div>
    
  </div>
</section>
@stop