@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<div class="payment_box">
   <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
          <h1>Selecionamos abaixo alguns tópicos que podem ser relevantes para você:</h1>
        </div>
      </div>
    </div>
  <div class="container">
    <div class="row other_sec">
      @if($category_info->count()>0)
      @foreach($category_info as $cat_info)
      <div class="col-sm-4  ">
        <div class="other_box servicebox">
          <?php $cat_des = strip_tags($cat_info->description);  ?>
          <h2><a href="{{ url('/service/detail/') }}/{{ $cat_info->id }}">{{ucfirst($cat_info->title)}}</a></h2>
          <?php $des = substr($cat_des,0,100);?>
          <p>{{$des}}
            <?php if(strlen($cat_des) > 100){?>
            <a href="{{ url('/service/detail/') }}/{{ $cat_info->id }}">Veja mais...</a>
            <?php } ?>
          </p>
        </div>
      </div>
      @endforeach
      @else
      <div class="no-rec"> <h3><span><strong>No Record Found </strong></span></h3></div>
      @endif 
    </div>
    <div align="center">{{$category_info->links()}}</div>
  </div>
</div>
@stop