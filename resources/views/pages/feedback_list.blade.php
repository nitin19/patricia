@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<style type="text/css">.readmore, .btnless{display: none;}</style>
@if(Auth::check() && empty(Auth::user()->address) && empty(Auth::user()->phone))
<div class="alert alert-danger alert-dismissible fade in profle_alert">
	<strong>Atenção!</strong>Complete seu cadastro abaixo.
</div>
@endif
<div class="bannerimg text-center">
	<div class="container">
		<div class="banner_content">
			<h1>Avaliação/feedback:</h1>
		</div>
	</div>
</div>
<div class="feedback_listsec">
	<div class="container">
		<div class="feedback_listing">
			<h1 align="center">Prestador(es) de serviço contatado(s) por você:</h1>
			<div class="alert alert-info alert-dismissible  msgAlertBlock fade in" style="display:none;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>
			@if(count($get_user_bookings)>0)
			<ul>
				<li>
					@foreach($get_user_bookings as $booking)
					<?php $userblock = DB::table('block_providers')->where('taker_id','=',$booking->user_id)->where('provider_id','=',$booking->profile_id)->first();
					//print_r($userblock->block_status);
					$feedbacks = DB::table('feedback')->where('user_id',Auth::user()->id)->where('booking_id',$booking->booking_id)->where('role','taker')->where('provider_id',$booking->profile_id)->first();
					$providerdata = DB::table('users')->where('id', $booking->profile_id)->where('is_active','1')->where('user_role','provider')->where('is_deleted','0')->first();
					
					$fmt = datefmt_create(
					'pt-BR',
					IntlDateFormatter::FULL,
					IntlDateFormatter::FULL,
					'Brazil/East',
					IntlDateFormatter::GREGORIAN,
					"dd/MMM/YYYY"
					);
					//start time
					$dt = new DateTime($booking->booking_start_time. "+1 days");
					$d = $dt->format('d');
					$m = $dt->format('m');
					$y = $dt->format('Y');
					?>
					<div class="row backback">
						<div class="col-sm-4 feedback_leftdiv leftspace">
							<!--  <i class="fa fa-user feedbackicon"></i> -->
							<img src="{{ url('/public/images/categoryimage/'.$booking->cat_image) }}" style="float: left;">
							<p>
							<b><?php if($providerdata){ echo $providerdata->name;} else { echo '';}?></b>
							</p>
							<p>
							    <b>{{$booking->cat_name}} </b>
								<!-- <span>Primeiro contato</span>  -->
							</p>
							<p class="date-created" style="float:right;margin-right:20px;">
								<i class="fa fa-calender"></i><span>Primeiro contato: </span> {{datefmt_format($fmt, mktime(0,0,0,$m,$d,$y))}}
							</p>
						</div>
							<div class="col-sm-4 feedback_centerdiv leftspace">
								@if(empty($booking->profile_image))
								<img class="img-responsive" src="{{url('/public/images/')}}/prof_dummy2.png">
								<figcaption><p>
									<b>Prestador(a) de serviço</b><span class="Pro_span">{{ucfirst($booking->name)}}</span></p></figcaption>
									@else
									<img class="img-responsive" src="{{url('/public/images/profileimage/'.$booking->profile_image)}}">
									<figcaption><p>
										<b>Prestador(a) de serviço</b><span class="Pro_span">{{ucfirst($booking->name)}}</span></p></figcaption>
										@endif
										<!-- <p>{{ mb_substr($booking->booking_message, 0,50)}}</p> -->
										<div id="msgrow{{$booking->booking_id}}"><p><?php if(strlen($booking->booking_message) > 50){echo substr($booking->booking_message,0,50).'<span style="padding-left:0px!important;" class="dots" id="dotmsg'.$booking->booking_id.'">...</span><span style="padding-left:0px!important;" class="readmore" id="moremsg'.$booking->booking_id.'">'.substr($booking->booking_message, 50, strlen($booking->booking_message)).'</span><span style="padding-left:0px!important;" class="btnmore">Veja mais</span><span style="padding-left:0px!important;" class="btnless">Veja menos</span>';}else{ echo $booking->booking_message;} ?></p></div>
									</div>
									<div class="col-sm-4 feedback_rightdiv leftspace">
										<!-- <p><i class="fa fa-forward"></i><span>See Conversation</span></p> -->
										<div class="unblocked_{{ $booking->booking_id }}" data-bid="{{ $booking->booking_id }}"  data-prostatus ="<?php if(!empty($userblock)) { echo $userblock->block_status; } else { echo "0"; } ?>">
											@if(empty($feedbacks))
											<p><a class="blkfeed_{{ $booking->booking_id }} forid " href="#" data-toggle="modal" data-target="#feedbackmodal" data-providerid="{{ $booking->booking_id }}" data-providerNm="{{ucfirst($booking->name)}}" ><i class="fa fa-pencil"></i><span class="feedback_text">Avaliar prestador de serviço</a></span></p>
											@else
											@if($feedbacks->approve_status == '1')
											<p><i class="fa fa-pencil"></i><span class="">Sua avaliação foi publicada</span></p>
											@elseif($feedbacks->approve_status == '2')
											<p><i class="fa fa-pencil"></i><span class="">Sua avaliação não foi publicada.</span></p>
											@else
											<p><i class="fa fa-pencil"></i><span class="feedback_text">Avaliação em análise</span></p>
											@endif
											@endif
											<p>
												<a class="unblkavil_{{ $booking->booking_id }} forid" href="#" data-toggle="modal" data-target="#myModal_{{ $booking->booking_id }}" data-providerid="{{ $booking->booking_id }}"><i class="fa fa-calendar"></i><span>Verificar horários</span></a>
											</p>
											<p><i class="fa fa-plus"></i><span>
												<a class="blkserviveprovider_{{ $booking->booking_id }} forid" href="{{ url('/profile/'.$booking->profile_id) }}" data-providerid="{{ $booking->booking_id }}">Ir para a página do prestador de serviço</a> </span></p>
												<p class="unblkpro_{{ $booking->booking_id }}"><i class="fa fa-user-times"></i><a href ="#"><span class="block_provider" data-providerblk="{{ $booking->booking_id }}"><span class="blktext_{{ $booking->booking_id }}"><?php  if(!empty($userblock && $userblock->block_status == '1')) { echo "Desbloquear prestador de serviço"; } else { echo "Bloquear prestador de serviço"; } ?></span>
												<form method="POST" class="blocker_{{ $booking->booking_id }}">
													<input type="hidden" name="provider_id" class="providerID_{{ $booking->booking_id }}" value="{{ $booking->profile_id }}" required="required">
													<input type="hidden" name="taker_id" class="takerID" value="{{ $booking->user_id}}" required="required">
													<input type="hidden" class="block_status_{{ $booking->booking_id }}" name="block_status" value="<?php if(!empty($userblock)) { echo $userblock->block_status; } else { echo "1";} ?>" required="required">
													<input type="hidden"  class="providertoken" name="_token" value="{{ csrf_token() }}" required="required">
												</form></span></a>
											</p>
										</div>
										
									</div>
								</div>
								<!-- /*availiability modal*/  -->
								<div class="modal fade" id="myModal_{{ $booking->booking_id }}" role="dialog">
				     <div class="modal-dialog check_avail">
					      <!-- Modal content-->
				      	<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Horários</h4>
												</div>
												<div class="modal-body">
													<ul class="listingitem1">
												<?php
												    if($providerdata){ 
												      $providerdata_id = $providerdata->id;
												    } else { 
												      $providerdata_id = '';
												    }
												    
												    $availablitiesf = DB::table('spavailability')->where('user_id',$providerdata_id)->first();
												   

												?>	@if($availablitiesf)
									                @if($availablitiesf->isCompleted=='1')
									                  @if($availablitiesf->openhrs=='1')
									                    <li> @if($availablitiesf->day_monday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Segunda-feira:
									                      @if($availablitiesf->day_monday=='1')
									                      {{$availablitiesf->monday_start}} - {{$availablitiesf->monday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_tuesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Terça-feira:
									                      @if($availablitiesf->day_tuesday=='1')
									                      {{$availablitiesf->tuesday_start}} - {{$availablitiesf->tuesday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_wednesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quarta-feira:
									                      @if($availablitiesf->day_wednesday=='1')
									                      {{$availablitiesf->wednesday_start}} - {{$availablitiesf->wednesday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_thursday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quinta-feira:
									                      @if($availablitiesf->day_thursday=='1')
									                      {{$availablitiesf->thursday_start}} - {{$availablitiesf->thursday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_friday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sexta-feira:
									                      @if($availablitiesf->day_friday=='1')
									                      {{$availablitiesf->friday_start}} - {{$availablitiesf->friday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_saturday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sábado:
									                      @if($availablitiesf->day_saturday=='1')
									                      {{$availablitiesf->saturday_start}} - {{$availablitiesf->saturday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                    <li>@if($availablitiesf->day_sunday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Domingo:
									                      @if($availablitiesf->day_sunday=='1')
									                      {{$availablitiesf->sunday_start}} - {{$availablitiesf->sunday_end}}
									                      @else
									                      Fechado
									                      @endif
									                    </li>
									                  @else
									                    <li><i class="fa fa-check-circle"></i>Segunda-feira 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Terça-feira 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Quarta-feira 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Quinta-feira 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Sexta-feira 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Sábado 00:00 - 23:59</li>
									                    <li><i class="fa fa-check-circle"></i>Domingo 00:00 - 23:59</li>
									                  @endif
									                @else
									                  <li><i class="fa fa-times-circle"></i>No availability set by {{$availablitiesf->name}}</li>
									                </ul>@endif
									                @else 
									                @endif
												</div>
											</div>
										</div>
									</div>
							 <!--/* */ -->
								<div class="feedback_info_{{ $booking->booking_id }}">
									<input type="hidden" name="provider_id" class="p_id" value="{{ $booking->profile_id }}" required="required">
									<input type="hidden" name="prviderName" value="{{ucfirst($booking->name)}}" required="required">
									<input type="hidden" name="user_id" value="{{ $booking->user_id}}" required="required">
									<input type="hidden"  name="booking_id" value="{{ $booking->booking_id }}" required="required">
									<input type="hidden"  name="role" value="taker">
									<input type="hidden"  name="_token" value="{{ csrf_token() }}" required="required">
								</div>
								<hr>
								@endforeach
							</li>
						</ul>
						@else
						<p align="center">Você ainda não contatou nenhum profissional. Após contatar o prestador de serviço de sua preferência, você poderá avaliá-lo.</p>
						@endif
						{{ $get_user_bookings->links() }}
					</div>
				</div>
			</div>
			<!-- Model Content for Feedback form -->
			<div class="modal fade" id="feedbackmodal" role="dialog">
				<div class="modal-dialog feedback_modaldiv">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Avaliação/feedback: <span class="providertitle"></span></h4>
						</div>
						<div class="modal-body">
							<div class="message" style="display:none;">
								<div class="alert alert-info alert-dismissible  msgAlerts fade in">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								</div>
							</div>
							<h3>Favor avalie o prestador de serviço preenchendo os dados abaixo.</h3>
							<form method="post" id="feedback_rating" class="fordata">
								<div class="form-group" id="rating-ability-wrapper">
									<label class="control-label" for="rating">
										<span class="field-label-header">Sua nota para esse prestador de serviço:</span><br>
										<span class="field-label-info"></span>
										<input type="hidden" id="selected_rating" name="selected_rating" value="" required="required" class="rating_score">
									</label>
									<div class="demo">
									</div>
									<div class="fordata2">
									</div>
								</div>
								<textarea name= "message" rows="5" class="form-control" placeholder="Comente sobre o serviço prestado (qualidade, entrega dentro/fora do prazo, agilidade na resposta, preço...)." ></textarea>
								<span class="spn_msg" style="color:red;"></span>
								<input type="submit" class="modalsubmit" value="Enviar">
							</form>
						</div>
					</div>
				</div>
			</div>
			
<script>
	jQuery(document).ready(function(){
$(".btnmore").click(function(){
	var tdid = $(this).closest('div').attr('id');
	$('#'+tdid).find('.readmore').css('display', 'inline');
	$('#'+tdid).find('.btnless').css('display', 'block');
	$('#'+tdid).find('.dots').css('display', 'none');
	$(this).css('display', 'none');
	});
	$(".btnless").click(function(){
	var tdid = $(this).closest('div').attr('id');
	$('#'+tdid).find('.readmore').css('display', 'none');
	$('#'+tdid).find('.btnmore').css('display', 'block');
	$('#'+tdid).find('.dots').css('display', 'inline');
	$(this).css('display', 'none');
	});
	//get data from div
	$(".forid").click(function(){
		var profile =$(this).data("providerid");
		var feed_html = $(".feedback_info_"+profile).html();
		var bid = $(this).data("providerid");
		var pname = $(this).attr("data-providerNm");
	    $(".providertitle").html(pname);
        var providerID = $('.providerID_'+bid).val();
	    var hosts = '{{ url('/') }}';
	    var value = $('.unblocked_'+bid).data("prostatus");
		if(value == 1){
			$('.blkserviveprovider_'+bid).removeAttr("href");
			$('.unblkavil_'+bid).removeAttr("data-target");
			$('.blkfeed_'+bid).removeAttr("data-target");
		}
		$('.fordata2').html('');
			var data = $(".fordata2").append(feed_html);
		$(".demo").raty('set', {'score' : 0});
	});
	//Rating jquery
	var base_url = '{{url('/')}}';
	$('.demo').raty({
		starHalf    : base_url+'/public/images/star-half.png',
		starOff     : base_url+'/public/images/star-off.png',
		starOn      : base_url+'/public/images/star-on.png',
		hints       : ['bad', 'poor', 'regular', 'good', 'gorgeous'],
		click:function(score){
			$(".rating_score").val(score);
		}
	});
	//rating formdata
	$('#feedback_rating').on('submit', function(e) {
		e.preventDefault();
		var msg = $('textarea[name = "message"]').val();
		if(msg == '')
		{
			$('.spn_msg').html('Para auxiliar os demais usuários, deixe um breve comentário sobre o serviço prestado.');
		} else {
			var formData = $(this).serialize();
			var host = '{{ url("/") }}';
			$.ajax({
				type: "POST",
				url: host+'/feedback/create',
				data: formData,
				dataType: "json",
				success: function(response) {
			        // console.log(response);
				    if(response.success == true){
						$(".message").show();
						$(".msgAlerts").html(response.msg);
						//$('.forid').hide();
						$('.feedback_text').html('Avaliação em progresso');
						setTimeout(function(){
							$(".message").hide();
							$('#feedbackmodal').modal('hide');
							$("#feedback_rating")[0].reset();
						},4000);
					window.setTimeout(function(){window.location.reload()}, 3000);
				    }else{
						$(".message").show();
						$(".msgAlerts").html(response.msg);
						setTimeout(function(){
							$(".message").hide();
							$('#feedbackmodal').modal('hide');
							$("#feedback_rating")[0].reset();
						},3000);
					}
		        }
	        });
	    }
	});
	function blockprovider(pid){
		var formData = $(".blocker_"+pid).serialize();
	    var providerID = $('.providerID_'+pid).val();
	    var hosts = '{{ url('/') }}';
		$.ajax({
			type: "POST",
			url: hosts+'/blockprovider/create',
			data: formData,
	        dataType: "json",
	        success: function(response) {
				if(response.success == true){
					var msg = response.msg;
					$.notify({
						message: msg,
						},{
						type: 'success',
						offset:
						{
						x: 10,
						y: 130
						},
						animate: {
						enter: 'animated fadeInRight',
						exit: 'animated fadeOutRight'
						},
			        });
					if(response.blkstatus == '1')
					{
						
						$('body').find('.unblocked_'+pid).attr("data-prostatus",response.blkstatus);
						$('body').find('.block_status_'+pid).val(response.blkstatus);
						
						$('.unblkpro_'+pid).find('.blktext_'+pid).html('Desbloquear prestador de serviço');
						// $('.blktext_'+pid).text("Unblock provider");
						
						$('.forid').on('click',function(){
					        var value1 = $('.unblocked_'+pid).data("prostatus");
					        $('body').find('.unblkavil_'+pid).removeAttr("data-target");
						    $('body').find('.blkfeed_'+pid).removeAttr("data-target");
						    $('body').find('.blkserviveprovider_'+pid).removeAttr("href");
					    });
					} else {
			            $('body').find('.unblocked_'+pid).attr("data-prostatus",response.blkstatus);
                        $('body').find('.block_status_'+pid).val(response.blkstatus);
			            $('.unblkpro_'+pid).find('.blktext_'+pid).html('Bloquear prestador de serviço');
			            $('.forid').on('click',function(){
		                    var value1 = $('.unblocked_'+pid).data("prostatus");
							$('body').find('.blkserviveprovider_'+pid).attr("href",hosts+'/profile/'+providerID);
			                $('body').find('.blkfeed_'+pid).attr("data-target","#feedbackmodal");
			                $('body').find('.unblkavil_'+pid).attr("data-target","#myModal");
						});
		            }
		        }else{
		            var msg = response.msg;
		            $.notify({
						message: msg,
						},
						{
						type: 'danger',
						offset:
						{
						x: 10,
						y: 130
						},
						animate: {
						enter: 'animated fadeInRight',
						exit: 'animated fadeOutRight'
						},
					});			
				}
			}
		});
	}
	//block_provider
	$('.block_provider').on('click', function(e) {
		e.preventDefault();
		var pid =$(this).data("providerblk");
		var block_status = $('.block_status_'+pid).val();
		if(block_status == '1'){
			var confirmsgs = 'Você quer mesmo desbloquear esse prestador de serviço? Ao fazer isso, você passará a vê-lo no seu mapa quando buscar um serviço.';
		} else {
			var confirmsgs = 'Você quer mesmo bloquear esse prestador de serviço? Ao fazer isso, você não o verá mais no seu mapa quando buscar um serviço.';
		}
		$.confirm({
				title: '',
				content: confirmsgs,
				buttons: {
				confirme: function () {
				blockprovider(pid);
				},
				Cancelar: function () {
				}
				}
		});
				
	});
					
	window.setTimeout(function() {
		$(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove();
		});
	}, 4000);
});
</script>
<style type="text/css">
.listingitem1>li i {
    font-size: 22px;
}
.feedback_listing ul li {
    padding: 2px 16px;
}
.listingitem1>li {
    font-size: 14px;
}
</style>
			@stop