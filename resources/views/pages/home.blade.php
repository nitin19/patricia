@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<style>#mc-embedded-subscribe-form label#mce-EMAIL-error {display: none !important;}
#mce-responses a {display: none!important;}</style>
<main>
  <!--mapsection-start-->
  <!--Mouse-Cursor-Effect-(div-HTML)-->
  <div id="age-gate" class="display dismissed">
    <div class="feathers">
      <i class="feather"></i>
      <i class="feather"></i>
      <i class="feather"></i>
    </div>
  </div>
  <div class="custom-cursor" style="opacity: 1;">
    <span class="main" style="top: 493px; left: 250px;">
    </span>
    <span class="follow" style="top: 493px; left: 250px;">
    </span>
  </div>
  <!--Mouse-Cursor-Effect-(div-HTML)End-->
  <section class="mapsection">
    <div id="map" style="width: 100%; height: 490px; background: #89C4FA"/></div>
  </section>
  <!--mapsection-end-->
  <!--serviceform-start-->
  <?php
  $lat_str1 = '';
  $long_str1 = '';
  foreach($users as $user_info){
    if($user_info->latitude!=''){
    $lat_str1 .= $user_info->latitude.',';
    $long_str1 .= $user_info->longitude.',';
    }
  }
  $lat_str = explode(',',rtrim($lat_str1,","));
  $long_str = explode(',',rtrim($long_str1,','));
  $data = array_combine($lat_str,$long_str);

  ?>

  <section class="seriveformsec">
    <div class="container">
      <div class="row">
        <div class="serviceformbg">
          <form class="serviceform" id="serviceform">
            <div class="row">
              <div class="col-sm-12"><h1>Encontre prestadores de serviço</h1></div>   
            </div>
            <div class="">
              <div class="col-sm-2 formdiv homeformdiv">
                <input type="hidden" name="sortby" value="">
                <select class="custom-select" id="cat" name="cat" required oninvalid="this.setCustomValidity('Please select the category.')" placeholder="Selecione a Categoria">
                 <!--  <option value="" selected="selected">Selecione a Categoria</option> -->
                  <option value=""  style="display:none;"></option>
                  @foreach($category as $category_info)
                  <option value="{{ $category_info->id }}">{{ $category_info->name }}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-sm-2 formdiv homeformdiv">
                <input type="text" class = "form-control" id="autocomplete" name="area" placeholder="Busque por localização, rua, bairro, CEP..." />
              </div>
              <div class="col-sm-2 formdiv homeformdiv">
                <select name="rating" id="rating" class="custom-select sources rating" placeholder="Avaliação dos usuários">
                  <option value=""></option>
                  <option value="5">&#xf005;&#xf005;&#xf005;&#xf005;&#xf005; </option>
                  <option value="4">&#xf005;&#xf005;&#xf005;&#xf005; </option>
                  <option value="3">&#xf005;&#xf005;&#xf005; </option>
                  <option value="2">&#xf005;&#xf005; </option>
                  <option value="1">&#xf005; </option>
                  <option value="">Com ou sem avaliação</option>
                </select>
              </div>
              <div class="col-sm-2 formbutton homeformbutton">
                <input type="submit" class="btn find_provider" value="Encontre prestadores"> <img id="loadingmessage" src="{{url('/public')}}/images/loading_30.gif" style="display:none;"/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="howitworksec pagesection">
    <div class="container ">
      <div class="row ">
        <div class="col-sm-12 howitworkcontent text-center">
          <h2 class="pagetitle">Como funciona</h2>
          <p class="pagetext">Simples e rápido!<br/>
          Use o NaVizinhança a qualquer hora e em qualquer lugar.</p>
          <div class="howitworkblks">
            <div class="row setcontentview">
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                  <div class="howitworkimg">
                    <img src="{{ url('/public') }}/images/Find-The-Service-Provider.png" class="img-responsive" alt="Prestadores de serviço">
                  </div>
                  <h2>FAÇA SUA BUSCA </h2>
                  <p>Selecione o tipo de prestação de serviço ou região para encontrar um profissional.</p>
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                  <div class="howitworkimg">
                    <img src="{{ url('/public') }}/images/howitblk2.png" class="img-responsive" alt="workable">
                  </div>
                  <h2>CONTATE OS PROFISSIONAIS</h2>
                  <p>Após a seleção dos profissionais, <br/> contate-os para validação de disponibilidade e orçamento.</p>
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                  <div class="howitworkimg">
                    <img src="{{ url('/public') }}/images/feedback.png" class="img-responsive" alt="Feedback">
                  </div>
                  <h2>NÃO ESQUEÇA DE DAR O SEU FEEDBACK</h2>
                  <p>Depois do contato, ajude-nos dando o feedback/avaliação desse profissional, para que possamos continuamente melhorar nossas recomendações para consultas futuras.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="servicesec pagesection">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 serviceseccontent text-center">
          <h3 class="pagetitle">Tipos de Serviços</h3>
          <div class="serviceblks">
            <div class="row">
              <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                  <?php $count=1;
                  foreach($category as $category_info){
                    $cat_image = $category_info->cat_image;
                    $cat_name = $category_info->name;
                    if($cat_image==''){
                      $categoryimage = url('/')."/public/images/main_category/default.png";
                      $altname= "service";
                    }else {
                      $categoryimage =  url('/')."/public/images/main_category"."/".$cat_image;
                      $altname= $cat_name;
                    }
                  ?>
                  <div class="item">
                    <div class="pad15">
                      <a href="{{ url('/search?cat='.$category_info->id) }}"><img src="{{$categoryimage}}" alt="{{$altname}}"></a>
                      <h4><a href="{{ url('/search?cat='.$category_info->id) }}">{{ $category_info->name }}</a></h4>
                    </div>
                  </div>
                  <?php
                  ++$count;
                  }
                  ?>
                </div>
                <button class="btn btn-primary leftLst"><i class="fa fa-angle-left"></i></button>
                <button class="btn btn-primary rightLst"><i class="fa fa-angle-right"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="aboutservice pagesection">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 abtservicecontent text-center">
          <h3 class="pagetitle">Estamos aqui para ajudar! E sem burocracia!!!</h3>
          <p class="pagetext">O NaVizinhança nasce com o intuito de simplificar a busca de prestadores de serviços.</p>
          <p class="pagetext"> Procurando alguém na sua região? Buscando alguém bem recomendado? Tem restrição de despesas para a contratação do serviço? Utilize os filtros que melhor se adequam para sua busca e contate o profissional de sua preferência.</p>
          <p class="pagetext">Não esqueça de avaliar o prestador de serviço após o contato, positivamente ou negativamente!!! </p>
          <p class="pagetext">Seja qual for o motivo, sua opinião é importante para nós.</p>
          <span class="pagetext" style="color:#0E3C5E;font-weight: bold;">Exemplos: profissional com telefone incorreto, ou excelente profissional, ou profissional não apareceu...</span>
          <p class="pagetext">Só assim podemos melhorar nossas indicações para quem precisa dos serviços desses profissionais. </p>
          <p class="pagetext">Não espere, faça sua busca agora mesmo e encontre o profissional que melhor se enquadra para sua necessidade!</p>
          <div class="abtserviceimg">
            <img src="{{ url('/public') }}/images/service_provider.png" class="img-responsive" alt="abtserviceimg">
          </div>
        </div>
      </div>
    </div>
  </section>
<section class="whitebg"></section>
@if(count($testimonial)>0)
<section class="testimonialsection pagesection">
  <div class="container">
    <div class="row">
      <div id="carousel-example-generic" class="col-sm-12 carousel slide testimonialslider" data-ride="carousel">
        <!-- <h4>CLIENT FEEDBACK</h4> -->
        <h3>O que os nossos usuários falam sobre o NaVizinhança:</h3>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          @foreach($testimonial as $testimonials)
          <div class="item @if($testimonials->id == $testimonial[0]->id){{'active'}}@endif">
            <div class="row">
              
              <?php
              $profile_pic = $testimonials->profille_pic;
              if($profile_pic==''){
              $profilepic = "../public/images/testimonialimage/clothing_icon.png";
              }else {
              $profilepic = "../public/images/testimonialimage/".$profile_pic;
              }
              ?>
              <div class="col-sm-12">
                <div class="testislidecntnt">
                  <p>{{ $testimonials->description }}</p>
                  <div class="clientinfo">
                    <img src="{{ $profilepic }}">
                    <h4>{{ $testimonials->name }}</h4>
                    <span>{{$testimonials->profile}}</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
  @endif
  <section class="pagesection newslettersec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 centered">
          <div class="newsletterbox slower-4s">
            <h3>INSCREVA-SE</h3>
            <h4>Fique por dentro das novidades do NaVizinhança</h4>
            <!-- Begin Mailchimp Signup Form -->
            <link href="{{ url('/public') }}/css/classic-10_7.css" rel="stylesheet" type="text/css">
            <link href="{{ url('/public') }}/css/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
            #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            </style>
            <div id="mc_embed_signup">
              <form action="https://navizinhanca.us20.list-manage.com/subscribe/post?u=6afdec3041ad4b6f838402be4&amp;id=4d143d65f1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                  <div class="mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Digite aqui seu Email" style="position:relative;height:34px; text-transform: unset!important;" required title="Não deixe para depois. Cadastre-se agora mesmo!" />
                  </div>
                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6afdec3041ad4b6f838402be4_4d143d65f1" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                  </div>
                </form>
              </div>
              <script type='text/javascript' src="{{ url('/public/') }}/js/mc-validate.js"></script>
              <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
              <!--End mc_embed_signup-->
            </div
          </div>
        </div>
      </div>
    </section>
    <div class="container-fluid cta_bg mt-4">
      <div class="row">
        <div class="col-md-4 emailicon">
          <img src="{{url('/public')}}/images/mail-icon (1).png" style="height: 150px;margin-top: -30px;" class="img_move">
        </div>
        <div class="col-md-8">
          <h4 class="emailshow">E-mail para contato: <a href="mailto:contato@navizinhanca.com"><u> contato@navizinhanca.com</u></a></h4>
        </div>
      </div>
    </div>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&sensor=false&libraries=places"></script>

<script>
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        maxZoom: 16,
    };
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    // Multiple markers location, latitude, and longitude
    var markers = [
      <?php $intpart = '';
      foreach($users as $user_info) {
        if(empty($user_info->profile_image)){
          $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
        }
        else{
          $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
        }
        if($user_info->latitude!='' ){
          $email = $user_info->email;
          $cat_image = $user_info->cat_image;
          if(!empty(Auth::user())){
            $profile_url =url('/profile/'.$user_info->p_id);
            $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=', $user_info->p_id)->where('block_status','=','1')->first();
           
            if(!empty($userblock)) {
              $image =" ";
            }else {
              if($cat_image==''){
                $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
                }else {
                $image = url('/public')."/images/categoryimage/".$cat_image;
                }
            }
          } else {
            $profile_url =url('/userprofile/'.$user_info->p_id);
            //$profile_url =url('/login?'.$user_info->p_id);
            if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }
          } 
      echo '["'.$user_info->name.'", '.$user_info->latitude.', '.$user_info->longitude.', "'.$image.'"],';
          }
      } ?>  
    ];
                        
    // Info window content
    var infoWindowContent = [

      <?php $intpart = '';
      foreach($users as $user_info) {
        if(empty($user_info->profile_image)){
          $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
        }
        else{
          $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
        }
        if($user_info->latitude!='' ){
          $email = $user_info->email;
          $cat_image = $user_info->cat_image;
          if(!empty(Auth::user())){
            $profile_url =url('/profile/'.$user_info->p_id);
            $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=', $user_info->p_id)->where('block_status','=','1')->first();
            //print_r($userblock);
            if(!empty($userblock)) {
              $image = url('/public')."/images/marker_icon.png";
            }else {
              if($cat_image==''){
                $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
                }else {
                $image = url('/public')."/images/categoryimage/".$cat_image;
                }
            }
          } else {
            $profile_url =url('/userprofile/'.$user_info->p_id);
            //$profile_url =url('/login?'.$user_info->p_id);
            if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }
          } ?>
         
            ['<div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodytext"><a class="user_txt" href="<?php echo $profile_url; ?>" target="_blank"><?php echo ucfirst($user_info->username); ?></a>' +
            '<div><span class="mapheading">Bairro:</span> <?php echo $user_info->neighborhood; ?></div><div><span class="mapheading"> Serviço: </span> <?php echo $user_info->cat_name; ?></div></a><div class="mapbodyrating"><span class="mapheading"><?php if($user_info->profile_rating){
            $rating = number_format($user_info->profile_rating,1);
            $intpart = floor ( $rating );
            $fraction = $rating - $intpart;
            for($x=1;$x<=$user_info->profile_rating;$x++) { ?> <i class="fa fa-star"></i> <?php } if (strpos($user_info->profile_rating,'.')) {?> <i class="fa fa-star-half-o" aria-hidden="true"></i> <?php $x++; }  while($x <= 5) {?> <i class="fa fa-star-o"></i> <?php  $x++; } } else { for($z=1;$z<=5;$z++) { ?> <i class="fa fa-star-o"></i> <?php }  }?> </span><a href="<?php echo $profile_url; ?>" class="raitingread" target="_blank">Mais info</a></div></div></div>'
            ],
        <?php }
      } ?>  

    ];
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
       // bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            icon: markers[i][3]
        });
        bounds.extend(marker.position);
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    var MyZoom = 0;
    if(screen.width < '768'){
        MyZoom = 12;
    } else {
        MyZoom = 13;
    }
    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(MyZoom);
        this.setCenter({lat: <?php echo $latitude_center; ?>, lng: <?php echo $longitude_center; ?>});
        google.maps.event.removeListener(boundsListener);
    });
    
    
}
// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
</script>
<script  type="text/javascript">

  jQuery(document).ready(function() {
    setInterval(function(){
      if($(".rightLst").hasClass("over")){
        $(".MultiCarousel-inner").css("transform", "translateX(0px)"); 
      } else {
        $('.rightLst').click(); 
      } 
    }, 2000);
    $('.rightLst').click(function(){ 
          $(".rightLst").removeClass("over");
        });
    
    /*----------------------------------------------------------------------------*/
    $("#serviceform").validate({
      rules: {
      cat: {
      required: true
      }
      },
      messages: {
      cat: {
      required: "Por favor, selecione a categoria."
      }
      },
      submitHandler: function(form) {
      var APP_URL = {!! json_encode(url('/search')) !!}
      var cat = jQuery('#cat').val();
      $('#loadingmessage').show();
      $.ajax({
      url: "{{ url('/search/infoexist') }}",
      type: 'POST',
      data: {'_token': "{{ csrf_token() }}", 'cat': cat},
      success: function(response) {
      $('#loadingmessage').hide();
      $("#serviceform").attr('action',APP_URL);
      form.submit();
      }
      });
      }
    });
    /*----------------------------------------------------------------------------*/
    //Home-page-dropdorw
    $(".custom-select").each(function() {
      var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
      var template =  '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
          $(this).find("option").each(function() {
          template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
          });
        template += '</div></div>';
        $(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
    });
    /*----------------------------------------------------------------------------*/
    $(".custom-option:first-of-type").hover(function() {
      $(this).parents(".custom-options").addClass("option-hover");
      },function() {
      $(this).parents(".custom-options").removeClass("option-hover");
    });
    /*----------------------------------------------------------------------------*/
    $(".custom-select-trigger").on("click", function(event) {
      $('html').one('click',function() {
      /*$(".custom-select").removeClass("opened");*/
    });
    $(this).parents(".custom-select").toggleClass("opened");
      event.stopPropagation();
    });
    /*----------------------------------------------------------------------------*/
    $(".custom-option").on("click", function() {
      $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
      $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
      $(this).addClass("selection");
      $(this).parents(".custom-select").removeClass("opened");
      $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
    });
    /*validation for mail subscription*/
    $('#mc-embedded-subscribe-form').validate({
      rules: {
      email: {
      required: true,
      email:true
      }
      },
      message:{
      email:{
      required:"This field is Required",
      email:"Please Enter Valid format"
      }
      }, 
      submitHandler: function(form) {
      }
    });/* end script validation*/
  });
</script>
<script type="text/javascript">
 function initialize() {
    var input = document.getElementById('autocomplete');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setComponentRestrictions({'country': ['br']});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.address_components);
    });
 }
 google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script src="{{ url('/public') }}/js/home_page_slider.js"></script>
</main>
<style type="text/css">
.mail-images {
  width: 24%;
  margin-top: 0%;
  margin-left: 0%;
}
.mapbodytext {
  color:#000;
  padding: 3px 15px;
}
.mapheading {
  color: #000 !important;
}
@media screen and (max-width: 767px){
  .serviceformbg {
   margin-top: 15px;
  }
  .gmnoprint .gm-style-mtc [role=button] {
    font-size: 12px !important;
    padding: 15px 10px 15px 10px !important;
  } 
}
.newslettersec{
  display: none;
}
#loadingmessage{
    position: absolute;
    top: 14px;
    right: 7px;
    width: 20px;   
}
</style>
@stop