@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<section class="maincontent searchbarbg">
  @if(Auth::check() && empty(Auth::user()->address) && empty(Auth::user()->phone))
  <script>
  $.notify({
  message: 'Complete seu cadastro abaixo.'
  },{
  type: 'danger',
  offset:
  {
  x: 10,
  y: 130
  },
  animate: {
  enter: 'animated fadeInRight',
  exit: 'animated fadeOutRight'
  },
  });
  </script>
  @endif
  <div class="col-sm-12 pagecontent nopadding">
    <div class="bannerimg text-center">
      <div class="container">
        <div class="banner_content">
          <h1>Prestadores de serviço selecionados por você</h1>
        </div>
      </div>
    </div>
    <div class="searchheaderdiv">
      <div class="container">
        <div class="col-sm-12">
          <div class="searchresults">
            <?php $count_wish_listusers = count($wish_listusers);
            ?>
            @if($count_wish_listusers>0)
            @foreach($wish_listusers as $wish_user_info)
            <?php
            $user_id = $wish_user_info->wish_profile_id;
            $wishlist_user_id = $wish_user_info->user_id;
            /*check user exist in provider list */
            $user_provider_exist = DB::table('users')
            ->Where('id', $user_id)
            ->Where('is_active', '1')
            ->Where('is_deleted', '0')
            ->count();
            if($user_provider_exist>0) {
            /* end check user exist in provider list */
            /* user provider info list */
            $user_info = DB::table('users')
            ->Where('id', $user_id)
            ->Where('is_active', '1')
            ->Where('is_deleted', '0')
            ->first();
            $profile_image = $user_info->profile_image;
            $profile_rating = $user_info->profile_rating;
            $userid = $user_info->id;
            $providerpricelist = DB::table('provider_price_list')
            ->leftjoin('price_queries','provider_price_list.query_id','=','price_queries.id')
            ->where('user_id', '=', $user_id)
            ->where('price_queries.priority', '1')
            ->first();
            $usertotalreview = DB::table('feedback')
            ->join('users', 'users.id', '=', 'feedback.user_id')
            ->where('feedback.provider_id',$user_id)
            ->where('approve_status',1)
            ->where('role','taker')
            ->count();
            ?>
            <div class="col-sm-4 searchresultdiv" id="unwish_{{ $user_info->id }}">
              <div class="searchresultbg">
                @if($profile_image=='')
                <a href="{{ url('/profile') }}/{{ $user_info->id }}" class="wishli"><img src="{{ url('/public/images/profileimage') }}/1542707849.images.png" class="profimg profimg img-shot" style="margin-left: 22%"></a>
                @else
                <a href="{{ url('/profile') }}/{{ $user_info->id }}" class="wishli"><img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="profimg profimg img-shot" style="margin-left: 22%"></a>
                @endif
                <div class="wishlistbox">
                  <div class="wishlistdiv">
                    <div class="discountdiv">
                      <!--  <span>5% discount</span> -->
                    </div>
                    <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                      <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                      <i class="fa fa-heart removewhishlist "></i>
                    </div>
                  </div>
                  <div class="resultcntntbox resul-part">
                    <div class="row">
                      <div class="col-md-7 col-sm-12 resultbox_title">
                        <ul class="wishlistul kkk">
                          <li>Nome:  <a href="{{ url('/profile') }}/{{ $user_info->id }}" class="wishli">{{ ucfirst($user_info->name) }}</a></li>
                        </ul>
                      </div>
                      
                      <div class="col-md-5 col-sm-12">
                        <ul class="wishlistul textalign ">
                          <li>
                            <?php if($usertotalreview) {
                            if($user_info->profile_rating != ''){
                            $number = number_format($user_info->profile_rating,1);
                            $integer_part = floor($number);
                            $fraction_part = $number-$integer_part;
                            for($x=1;$x<=$number;$x++) { ?>
                            <i class="fa fa-star"></i>
                            <?php }
                            if (strpos($number,'.')) {
                            if($fraction_part != 0) { ?>
                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                            <?php
                            $x++;
                            }
                            }
                            while($x<=5){?>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <?php
                            $x++;
                            }
                            }
                            echo '('.$usertotalreview.')';
                            } else{
                            for($i=1;$i<=5;$i++){ ?>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <?php }
                            } ?>
                            
                          </li>
                          
                        </ul>
                      </div>
                      <ul class="col-sm-12 wishlful">
                        
                        <li>Distância: {{number_format($wish_user_info->distances,2,',','.')}}  km</li>
                        <li>Preços: R${{ number_format($providerpricelist->amount,2,",",".") }}/{{$providerpricelist->queries}}</li>
                        <li>Categoria: {{$user_category_name[$wish_user_info->name]}}</li>
                      </ul>
                    </div>
                    
                  </div>
                  <p>{{ $user_info->description }}</p>
                  
                </div>
              </div>
            </div>
            
            <?php
            /* user provider info list */
            }
            ?>
            @endforeach
            <br>
            <div class="pagination_section col-sm-6 col-md-6">
              {{ $wish_listusers->appends(Request::only('show'))->links() }}
              
              @else
              <div class="empty-wishlist-div" style="margin: auto;">
                <h1 style="text-align: center;'" id="wishlist-empty-text">Você ainda não selecionou nenhum prestador de serviço como preferencial.</h1>
                <p align="center" id="wishlist-add-text"> Para facilitar a comparação dos prestadores de serviço, ao encontrar um prestador de serviço com perfil semelhante ao que você busca, clique em “Adicionar na lista de seleção” (campo com um coração ao lado). Isso feito, os profissionais selecionados serão listados aqui nessa página, onde você poderá compará-los e contatá-los com mais facilidade.</p>
                <div class="" style="text-align: center;margin-top: 20px;">
                  <a href="{{url('/search?sortby=asc&cat=&zipcode=&area=&rating=')}}"><button style="text-transform: unset;" type="button" class="btn btn-box" name="Find service Provider Here">Encontrar prestadores de serviços</button></a>
                </div>
              </div>
              @endif
              <!-- For wishlistip's provider -->
              @if(!empty($wish_listip))
              <?php $count_wish_listip = count($wish_listip); ?>
              @if($count_wish_listip>0)
              @foreach($wish_listip as $wish_info)
              <?php
              $ipuser_id = $wish_info->wish_profile_id;
              $ipwishlist_user_id = $wish_info->user_id;
              /*check user exist in provider list */
              $ipuser_provider_exist = DB::table('users')
              ->Where('id', $ipuser_id)
              ->Where('is_active', '1')
              ->Where('is_deleted', '0')
              ->Where('user_role', 'Provider')
              ->count();
              if($ipuser_provider_exist>0) {
              /* end check user exist in provider list */
              /* user provider info list */
              $ipuser_info = DB::table('users')
              ->Where('id', $ipuser_id)
              ->Where('is_active', '1')
              ->Where('is_deleted', '0')
              ->Where('user_role', 'Provider')
              ->first();
              $ipprofile_image = $ipuser_info->profile_image;
              $ip_profile_rating = $ipuser_info->profile_rating;
              $ipuserid = $ipuser_info->id;
              ?>
              <div class="col-sm-4 searchresultdiv" id="unwish_{{ $ipuser_info->id }}">
                <div class="searchresultbg">
                  @if($ipprofile_image=='')
                  <img src="{{ url('/public/images/') }}/result1.jpg" class="profimg profimg img-shot" style="margin-left: 22%">
                  @else
                  <img src="{{ url('/public/images/profileimage/') }}/<?php echo $ipprofile_image; ?>" class="profimg profimg img-shot" style="margin-left: 22%">
                  @endif
                  <div class="wishlistbox">
                    <div class="wishlistdiv">
                      <div class="discountdiv">
                        <!--  <span>5% discount</span> -->
                      </div>
                      <div class="wishlisticon" id="wishlist_{{ $wish_info->wish_profile_id }}">
                        <input type="hidden" name="userid" id="userid" value="{{ $wish_info->wish_profile_id }}">
                        <input type="hidden" name="userip" id="userip" value="{{ $wish_info->ip_address }}">
                        <i class="fa fa-heart removewhishlist_ip"></i>
                      </div>
                    </div>
                    <div class="resultcntntbox resul-part">
                      <div class="row">
                        <div class="col-md-7 col-sm-12 resultbox_title">
                          <ul class="wishlistul">
                            <li>Nome: <a href="{{ url('/profile') }}/{{ $ipuser_info->id }}" class="wishli"> {{ ucfirst($ipuser_info->name) }}</a></li>
                            <li>Distância  <a href="" class="wishli"> : {{number_format($wish_info->distances,2,',','.')}}  km</a></li> -->
                            <li>Categoria : <a href="" class="wishli"> {{$user_category_name[$wish_info->name]}}</a></li>
                          </ul>
                        </div>
                        <div class="col-md-5 col-sm-12">
                          <ul class="wishlistul textalign">
                            <li>
                              <?php
                              if($ipuser_info->profile_rating != ''){
                              $ipnumber = number_format($ipuser_info->profile_rating,1);
                              $ipinteger_part = floor($ipnumber);
                              $ipfraction_part = $ipnumber-$ipinteger_part;
                              for($x=1;$x<=$ipnumber;$x++) {?>
                              <i class="fa fa-star"></i>
                              <?php }
                              if (strpos($ipnumber,'.')) {
                              if($ipfraction_part > 0){?>
                              <i class="fa fa-star-half-o" aria-hidden="true"></i>
                              <?php
                              $x++;
                              }
                              }
                              while($x<=5){?>
                              <i class="fa fa-star-o" aria-hidden="true"></i>
                              <?php
                              $x++;
                              }
                              }
                              else{
                              for($i=1;$i<=5;$i++){?>
                              <i class="fa fa-star-o" aria-hidden="true"></i>
                              <?php }
                              }
                              ?>
                              
                            </li>
                          </ul>
                        </div>
                      </div>
                      <p>{{ $ipuser_info->description }}</p>
                      
                    </div>
                  </div>
                </div>
              </div>
              <?php }?>
              @endforeach
              <br>
              
              @endif
            </div>
            @endif
            <!-- End of wishlistip -->
            
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style type="text/css">.wishlisticon .fa-heart { cursor: pointer !important; }</style>
@stop