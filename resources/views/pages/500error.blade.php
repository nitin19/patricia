@extends('layouts.default-header')

@section('title', '500 Error')

@section('content')

<link href="{{ url('/public') }}/css/error.css" rel="stylesheet">
<section class="innercontentpart">
    <div class="innercontent_text">
      <div class="error_page">
        <div class="container">
          <div class="errorpage_content">
           <div class="error_msg">
            <h1>500</h1>
            <h2>Sorry!</h2>
            <h3>Ooooops! Server Error Occured. </h3>
            <a href="{{ url('/home') }}">Back to Home</a>
           </div>
        </div>
      </div>
    </div>
  </div>
 </section>

@endsection