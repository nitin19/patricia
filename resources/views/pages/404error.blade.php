@extends('layouts.default-header')

@section('title', '404 Error')

@section('content')
<link href="{{ url('/public') }}/css/error.css" rel="stylesheet">
<section class="innercontentpart">
    <div class="innercontent_text">
      <div class="error_page">
        <div class="container">
          <div class="errorpage_content">
           <div class="error_msg">
            <h1>404</h1>
            <h3>Ooooops! Essa página não existe. </h3>
            <a href="{{ url('/home') }}">Ir para a página inicial</a>
           </div>
        </div>
      </div>
    </div>
  </div>
 </section>

 @endsection