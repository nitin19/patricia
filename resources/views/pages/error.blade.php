@extends('layouts.default-header')

@section('title', 'Error')

@section('content')

<link href="{{ url('/public') }}/css/error.css" rel="stylesheet">
 <section class="innercontentpart">
    <div class="innercontent_text">
      <div class="error_page">
        <div class="container">
          <div class="errormsg_content">
           <div class="row error_msg_page">
            <div class="col-sm-6 error_list">
              <ul>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
              </ul>
            </div>
            <div class="col-sm-6 errormsg_div">
              <h1><i class="fa fa-exclamation-triangle"></i>Error</h1>
            <h2>Sorry, the Page not found</h2>
            <h3>The link you followed probably broken,or the page has been removed.</h3>
            <a href="{{ url('/home') }}">Back to Home</a>
            <div class="socialicons">
<!--               <ul>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
    <li><a href="#"><i class="fa fa-google"></i></li></a></ul> -->

            </div>
            </div>
           </div>
        </div>
      </div>
    </div>
  </div>
 </section>

@endsection