@extends('layouts.default-header')

@section('title', 'Users')

@section('content')

<section class="maincontent searchbarbg greybgdiv">
  <div class="col-sm-3 nopadding">
    <div class="sidebarbg">
      <div class="col-sm-12 nopadding">
        <div class="sidebartitle">
          <div class="row">
            <div class="col-sm-9">
              <h4>Services Available</h4>
            </div>
            <div class="col-sm-3 text-right nopadding">
              <span>Show All</span>
            </div>
          </div>
        </div>
      </div>
      <div id="MainMenu"  class="col-sm-12 mainmenu nopadding nav">
        <div class="list-group panel">
          <?php $count = 1; ?>
          @foreach($get_category1 as $category_info_key=>$category_info_val)
          <a href="#link{{ $count }}" class="list-group-item @if($category_info_key == $_GET['cat']) selected @else collapse @endif" data-toggle="collapse" data-parent="#MainMenu"> {{ $category_info_key }}  <i class="fa fa-angle-up"></i><i class="fa fa-angle-down"></i></a>
          <div class=" submenu @if($category_info_key == $_GET['cat']) selected @else collapse @endif" id="link{{ $count }}">
            @foreach($category_info_val as $cat_key=>$cat_val)
            <a href="{{ url('/search/') }}?sortby=asc&cat=@if($_GET['cat']){{ $_GET['cat'] }}@endif&zipcode=@if($_GET['zipcode']){{ $_GET['zipcode'] }}@endif&area=@if($_GET['area']){{ $_GET['area'] }}@endif&rating=@if(!empty($_GET['rating'])){{ $_GET['rating'] }}@endif&sub_id={{$cat_val['cat_id']}}" class="list-group-item " data-id="{{$cat_val['cat_id']}}" data-url="{{ url('/subcategory_filter/').'/'.$cat_val['cat_id'] }}">{{$cat_val['cat_name']}}</a>
            @endforeach
          </div>
          <?php ++$count; ?>
          @endforeach
        </div>
      </div>
      <div class="col-sm-12 nopadding">
        <div class="sidebartitle filterdiv">
          <div class="row">
            <div class="col-sm-8">
              <h4>Applied Filter</h4>
            </div>
            <div class="col-sm-4 text-right nopadding">
              <span><a href="{{ url('/search/') }}?sortby=asc&cat=&zipcode=&area=&rating=">Clear Filter</a></span>
            </div>
          </div>
        </div>
        <div class="filterblks">
          <ul>
            @if($_GET['cat']) <li>{{ $_GET['cat'] }} <i class="fa fa-close cat_close"></i></li>@endif

            @if(!empty($_GET['area'])) <li> {{ $_GET['area'] }} <i class="fa fa-close area_close"></i></li>@endif
            @if(!empty($_GET['rating'])) <li> {{ $_GET['rating'] }} <i class="fa fa-close rating_close"></i></li> @endif
          </ul>
        </div>
      </div>
      <select class="form-control" id="cat" name="cat" required oninvalid="this.setCustomValidity('Please select the category.')" >
        <option value="" selected="selected">Select Category</option>
        @foreach($get_category1 as $category_info=>$category->val)
        <option value="{{$category_info}}">{{$category_info}}</option>
        @endforeach
      </select>
      <!--city-sec-->
      <div class="col-sm-12 nopadding">
        <form name="search_form" method="get" action="{{ url('/search') }}" class="cityform">
          <div class="row">
            <div class="topspacing">
              <div class="col-sm-12 form-group sidebarinput">
                <input type="hidden" name="cat" value="<?php echo $_GET['cat']; ?>">
              </div>
              <div class="col-sm-12 form-group sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h4>Area</h4>
                    </div>
                  </div>
                </div>
                <!-- <select class="form-control" name="area" onchange="this.form.submit()"  > -->
                <select class="form-control" name="area"  >
                  <option value="">Select Area</option>
                  <option value="2" <?php if(!empty($_GET['area'])){if($_GET['area']=='2') { echo 'selected';} }?>>2</option>
                  <option value="3" <?php  if(!empty($_GET['area'])){if($_GET['area']=='3') { echo 'selected';}} ?>>3</option>
                  <option value="4" <?php if(!empty($_GET['area'])){if($_GET['area']=='4') { echo 'selected';}}?>>4</option>
                  <option value="5" <?php  if(!empty($_GET['area'])){ if($_GET['area']=='5') { echo 'selected';}}?>>5</option>
                </select>
              </div>
              <div class="col-sm-12 form-group sidebarinput">
                <div class="sidebartitle areatitle">
                  <div class="row">
                    <div class="col-sm-12">
                      <h4>Zipcode</h4>
                    </div>
                  </div>
                </div>
                <input type="text" name="zipcode" value="{{ $_GET['zipcode'] }}" class="form-control">
              </div>
            </div>
          </div>
          <div class="pricesec">
            <div class="sidebartitle">
              <div class="row">
                <div class="col-sm-12">
                  <h4>Price</h4>
                </div>
              </div>
            </div>
            <div class="topspacing">
              <div class="pricechkboxdiv">
                <label class="pricecheckbox">Fixed Rate
                  <input type="checkbox" checked="checked">
                  <span class="checkmark"></span>
                </label>
                <label class="pricecheckbox">Hourly Rate
                  <input type="checkbox">
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="ratingsec">
            <div class="sidebartitle">
              <div class="row">
                <div class="col-sm-12">
                  <h4>Select Your Ratings</h4>
                </div>
              </div>
            </div>
            <div class="topspacing">
              <div class="starratingsec">
                <div class="starratingdiv">
                  <input type="checkbox" name="rating" value="5" class="rating_checkbox" @if(!empty($_GET['rating']))@if($_GET['rating']==5) checked="checked" @endif @endif>
                  <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i>
                    <i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i></span>
                  </div>
                  <div class="starratingdiv">
                    <input type="checkbox" name="rating" value="4" class="rating_checkbox"  @if(!empty($_GET['rating']))@if($_GET['rating']>4) checked="checked" @endif @endif >
                    <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i>
                      <i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i></span>
                    </div>
                    <div class="starratingdiv">
                      <input type="checkbox" name="rating" value="3" class="rating_checkbox"  @if(!empty($_GET['rating']))@if($_GET['rating']>3) checked="checked" @endif @endif >
                      <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i>
                        <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i></span>
                      </div>
                      <div class="starratingdiv">
                        <input type="checkbox" name="rating" value="2" class="rating_checkbox" @if(!empty($_GET['rating'])) @if($_GET['rating']>2) checked="checked" @endif @endif >
                        <span><i class="fa fa-star ycolor"></i><i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i>
                          <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i></span>
                        </div>
                        <div class="starratingdiv">
                          <input type="checkbox" name="rating" value="1" class="rating_checkbox"  @if(!empty($_GET['rating'])) @if($_GET['rating']>1) checked="checked" @endif @endif >
                          <span><i class="fa fa-star ycolor"></i><i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i>
                            <i class="fa fa-star gcolor"></i><i class="fa fa-star gcolor"></i></span>
                            @if(!empty($_GET['rating']))@if($_GET['rating']=='')
                            <input type="hidden" name="rating" value="">
                            @endif
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12 formbutton">
                        <input type="submit" class="btn find_provider" value="Find Providers">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-9  nopadding">
                <div id="map" style="width: 100%; height: 390px; background: grey"/></div>
                <div class="searchheaderdiv">
                  <div class="col-sm-12">
                    <div class="col-sm-12">
                      <div class="srchheaderbrdr">
                        <div class="col-sm-8 resultdiv nopadding">
                          <h5><?php $count_user = count($users);
                          $category_name = '';
                          if(empty($_GET['sub_id'])){
                            foreach($users as $user_info){
                              $category_name = $user_info->name;
                            }

                            // if($count_user>0){
                            echo $count_user.' Results '. $category_name;
                            // }
                          }
                          elseif(!empty($_GET['sub_id'])){
                            foreach($users_sub_list as $sublist){
                              $category_name = $sublist->cat_name;
                            }
                            echo $count_user=count($users_sub_list).' Results '. $category_name;
                          }
                          else{
                            echo 'No Results Found'. $_GET['cat'];
                          }
                          ?></h5>
                        </div>
                        <div class="col-sm-4 resultselbox nopadding">
                          <select name="sortby" onChange="this.form.submit()">
                            <option value="desc">Sort by Descending</option>
                            <option value="asc">Sort by Ascending</option>
                          </select>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="success_show_ip">
                    <h1 class="crete_account"><a href="{{ url('/register') }}">Create free account</a></h1><br>
                    <p class="wiship_msg">This will let you find it again later, even after you close the browser.<br>
                      Already a member? <a href="{{ url('/login') }}">Log in here</a>
                    </p>
                  </div>
                  <div class="alert alert-success alert-block success_show">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>User Added In Wish List!</strong>
                  </div>
                  <div class="searchresults">
                    @if(empty($_GET['sub_id']))
                    @foreach($users as $user_info)
                    <?php
                    $lat = $user_info->latitude;
                    $lon = $user_info->longitude;
                    $profile_image = $user_info->profile_image;
                    $profile_rating = $user_info->profile_rating;
                    $category_image = $user_info->cat_image;
                    $user_id = $user_info->id;
                    //   $distance_mi =  round($user_info->distance, 2);
                    ?>
                    <?php
                    $area_arr = explode('-',$_GET['area']);
                    ?>
                    @if(!empty($_GET['area'])&&$distance>=$area_arr[0] && $distance<=$area_arr[1])
                    <div class="col-sm-4 searchresultdiv">
                      <div class="searchresultbg">
                        @if($profile_image=='')
                        <img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="img-responsive">
                        @else
                        <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-responsive">
                        @endif
                        <div class="wishlistbox">
                          <div class="wishlistdiv">
                            <div class="discountdiv">
                            </div>
                            <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                              <?php
                              $current_userid = Auth::id();
                              if($current_userid!=''){
                                $wish_list_exist = DB::table('wish_list')
                                ->where('user_id', $current_userid)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>
                                <?php
                              }
                              else {
                                $wish_list_exist_ip = DB::table('wish_list_ip')
                                ->where('ip_address', $current_ip)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                                <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                                <?php
                              }
                              ?>
                            </div>
                          </div>
                          <div class="resultcntntbox">
                            <div class="row">
                              <div class="col-sm-7 resultbox_title">
                              </div>
                              <div class="col-sm-5 startdiv">
                                @if($profile_rating>1)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>2)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>3)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>4)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating==5)
                                <i class="fa fa-star"></i>@endif
                              </div>
                            </div>
                            <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>"> <div class="mapheading-city"><strong>Name:</strong></span>{{ $user_info->username }}{{$user_info->sub_id}}</div></a>
                            <div class="mapheading-city "><strong>category:</strong><span class="category_name">{{ $user_info->cat_name }}</span></div>
                            <p>{{ $user_info->description }}</p>
                            <span class="amtdiv">$ {{ $user_info->price }}<span>/per {{ $user_info->price_type }}</span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    @else
                    <div class="col-sm-4 searchresultdiv">
                      <div class="searchresultbg">
                        @if($profile_image=='')
                        <img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="img-responsive">
                        @else
                        <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-responsive">
                        @endif
                        <div class="wishlistbox">
                          <div class="wishlistdiv">
                            <div class="discountdiv">
                            </div>
                            <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                              <?php
                              $current_userid = Auth::id();
                              if($current_userid!=''){
                                $wish_list_exist = DB::table('wish_list')
                                ->where('user_id', $current_userid)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>
                                <?php
                              }
                              else {
                                $wish_list_exist_ip = DB::table('wish_list_ip')
                                ->where('ip_address', $current_ip)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                                <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                                <?php
                              }
                              ?>
                            </div>
                          </div>
                          <div class="resultcntntbox">
                            <div class="row">
                              <div class="col-sm-7 resultbox_title">
                              </div>
                              <div class="col-sm-5 startdiv">
                                @if($profile_rating>1)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>2)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>3)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>4)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating==5)
                                <i class="fa fa-star"></i>@endif
                              </div>
                            </div>
                            <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>"> <div class="mapheading-city"><strong>Name:</strong></span>{{ $user_info->username }}{{$user_info->sub_id}}</div></a>
                            <div class="mapheading-city "><strong>category:</strong><span class="category_name">{{ $user_info->cat_name }}</span></div>
                            <p>{{ $user_info->description }}</p>
                            <span class="amtdiv">$ {{ $user_info->price }}<span>/per {{ $user_info->price_type }}</span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                    @if(!empty($_GET['sub_id']))
                    @foreach($users_sub_list as $user_info)
                    @if($_GET['sub_id'] == $user_info->sub_id)
                    <?php
                    $lat = $user_info->latitude;
                    $lon = $user_info->longitude;
                    $profile_image = $user_info->profile_image;
                    $profile_rating = $user_info->profile_rating;
                    // echo "<pre>";
                    //   print_r($profile_image);die;
                    $category_image = $user_info->cat_image;
                    $user_id = $user_info->id;
                    //   $distance_mi =  round($user_info->distance, 2);
                    ?>
                    <div class="col-sm-4 searchresultdiv">
                      <div class="searchresultbg">
                        @if($profile_image=='')
                        <img src="{{ url('/public/images/profileimage/') }}/1542707849.images.png" class="img-responsive">
                        @else
                        <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-responsive">
                        @endif
                        <div class="wishlistbox">
                          <div class="wishlistdiv">
                            <div class="discountdiv">
                            </div>
                            <div class="wishlisticon" id="wishlist_{{ $user_info->id }}">
                              <?php
                              $current_userid = Auth::id();
                              if($current_userid!=''){
                                $wish_list_exist = DB::table('wish_list')
                                ->where('user_id', $current_userid)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist'; } ?>"></i>
                                <?php
                              }
                              else {
                                $wish_list_exist_ip = DB::table('wish_list_ip')
                                ->where('ip_address', $current_ip)
                                ->where('wish_profile_id', $user_info->id)
                                ->count();
                                ?>
                                <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
                                <input type="hidden" name="userip" id="userip" value="{{ $current_ip }}">
                                <i class="fa <?php if($wish_list_exist_ip>0){ echo 'fa-heart'; } else { echo 'fa-heart-o addwhishlist_ip'; } ?>"></i>
                                <?php
                              }
                              ?>
                            </div>
                          </div>
                          <div class="resultcntntbox">
                            <div class="row">
                              <div class="col-sm-7 resultbox_title">
                              </div>
                              <div class="col-sm-5 startdiv">
                                @if($profile_rating>1)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>2)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>3)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating>4)
                                <i class="fa fa-star"></i>@endif
                                @if($profile_rating==5)
                                <i class="fa fa-star"></i>@endif
                              </div>
                            </div>
                            <a href="{{url('/profile/')}}/<?php echo $user_info->id;?>"> <div class="mapheading-city"><strong>Name:</strong></span>{{ $user_info->username }}</div></a>
                            <div class="mapheading-city "><strong>category:</strong><span class="category_name">{{ $user_info->cat_name }}</span></div>
                            <p>{{ $user_info->description }}</p>
                            <span class="amtdiv">$ {{ $user_info->price }}<span>/per {{ $user_info->price_type }}</span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                    @if(empty($_GET['cat']) && empty($_GET['zipcode']) && empty($_GET['area']) && empty($_GET['rating']) )
                    @foreach($providers_list as $user_info1)
                    <?php
                    $user_info = $user_info1;
                    ?>
                    @endforeach
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </section>
          <script  type="text/javascript">
          $('.sub_cat').on('click',function(){
            var id=$(this).data('id');
            var url = $(this).data('url');
            alert(url);
            $.ajax({
              type:'GET',
              dataType:"JSON",
              data:id,
              url:url,
              success:function(data){
                if(data != ""){
                  $.each(data,function(k,v){
                    if($('.category_name').text()!=v.cat_name){
                      $('.category_name').parent().parent().parent().parent().hide()
                    }
                  });
                }
              }
            })
          })
          jQuery('.fa-close').click(function(){
            $(this).parents('li').hide();
          });
          function addMarkerToGroup(group, coordinate, html, image) {
            var icon = new H.map.Icon(image);
            var marker = new H.map.Marker(coordinate, { icon: icon });
            marker.setData(html);
            group.addObject(marker);
          }
          function addInfoBubble(map) {
            var group = new H.map.Group();
            map.addObject(group);
            group.addEventListener('tap', function (evt) {
              var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
                content: evt.target.getData(),
              });
              ui.addBubble(bubble);
            }, false);
            <?php
            $lat_str1 = '';
            $long_str1 = '';
            $lat_str1 = '';
            $long_str1 = '';
            $cat_image = '';
            $latitude = '';
            $longitude = '';
            $user_id = '';
            $username = '';
            $city = '';
            $latitude_center = '';
            $longitude_center ='';
            $location_array = [];
            if(!empty($users)){
              foreach($users as $user_info) {
                if(empty($user_info->profile_image)){
                  $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
                }
                else{
                  $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
                }
                if($user_info->latitude!=''){
                  $lat_str1 .= $user_info->latitude.',';
                  $long_str1 .= $user_info->longitude.',';
                }
                $lat_str = explode(',',rtrim($lat_str1,","));
                $long_str = explode(',',rtrim($long_str1,','));
                $data = array_combine($lat_str,$long_str);
                $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
                $latitude_center =$new_arr[0]['geoplugin_latitude'];
                $longitude_center = $new_arr[0]['geoplugin_longitude'];
                if($user_info->latitude!='' ){
                  if(!empty($_GET['sub_id'])){
                    foreach($users_sub_list as $sub_cat){
                      $cat_image = $sub_cat->cat_image;
                      $latitude = $sub_cat->latitude;
                      $longitude = $sub_cat->longitude;
                      $user_id = $sub_cat->user_id;
                      $username = $sub_cat->username;
                      $city = $sub_cat->city;
                      $category_name = $sub_cat->cat_name;
                      if(empty($sub_cat->profile_image)){
                        $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
                      }
                      else{
                        $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
                      }
                    }
                  }
                  else{
                    $latitude = $user_info->latitude;
                    $longitude = $user_info->longitude;

                    $user_id = $user_info->id;
                    $username = $user_info->username;
                    $city = $user_info->city;
                    $category_name = $user_info->name;
                    $email = $user_info->email;
                    $cat_image = $user_info->cat_image;
                  }
                  if($cat_image==''){
                    $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
                  }else {
                    $image = url('/public')."/images/categoryimage/".$cat_image;
                  }?>
                  addMarkerToGroup(group, {lat:'<?php echo $latitude; ?>', lng:'<?php echo $longitude; ?>'},
                  '<div><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" ><?php echo ucfirst($username); ?></a>' +
                  '</div><div class="mapbodytext"><span class="mapheading">City:</span> <?php echo $city; ?></div><div class="mapbodytext"><span class="mapheading"> Category: </span> <?php echo $username; ?></div></a></div><div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodyrating"><span class="mapheading"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" class="raitingread">Read More</a></div>','<?php echo $image; ?>');
                  <?php
                  if(empty($_GET['area'])){
                    ?>
                    addMarkerToGroup(group, {lat:'<?php echo $latitude; ?>', lng:'<?php echo $longitude; ?>'},
                    '<div><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" ><?php echo ucfirst($username); ?></a>' +
                    '</div><div class="mapbodytext"><span class="mapheading">City:</span> <?php echo $city; ?></div><div class="mapbodytext"><span class="mapheading"> Category: </span> <?php echo $username; ?></div></a></div><div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodyrating"><span class="mapheading"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" class="raitingread">Read More</a></div>','<?php echo $image; ?>');
                    <?php
                  }
                  else {
                    $area_arr = explode('-',$_GET['area']);
                    if($distance>=$area_arr[0] && $distance<=$area_arr[1]){?>
                      addMarkerToGroup(group, {lat:'<?php echo $latitude; ?>', lng:'<?php echo $longitude; ?>'},
                      '<div><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" ><?php echo ucfirst($username); ?></a>' +
                      '</div><div class="mapbodytext"><span class="mapheading">City:</span> <?php echo $city; ?></div><div class="mapbodytext"><span class="mapheading"> Category: </span> <?php echo $username; ?></div></a></div><div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodyrating"><span class="mapheading"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span><a href="<?php echo url('/');?>/profile/<?php echo $user_id; ?>" class="raitingread">Read More</a></div>','<?php echo $image; ?>');
                      <?php
                    }
                  }
                }
              }
              $location_array = [];
              $location_array['longitude'] = $user_info->longitude;
              $location_array['latitude'] = $user_info->latitude;
            }
            else{
              $location_array = [];
            }
            ?>
          }
          // var coordinate = new Array();
          // <#?php foreach($location_array as $key => $val){ ?>
          //   console.log('<#?php echo $val;?>');
          //   coordinate.push('<#?php echo $val; ?>');
          //   <#?php } ?>

          var circle = new H.map.Circle({lat: '<?php echo  $location_array["latitude"];?>', lng: '<?php echo $location_array["longitude"];?>'}, 108000);
          var coordinate = new Array();

            var platform = new H.service.Platform({
              app_id: 'devportal-demo-20180625',
              app_code: '9v2BkviRwi9Ot26kp2IysQ',
              useHTTPS: true
            });
            var pixelRatio = window.devicePixelRatio || 1;
            var defaultLayers = platform.createDefaultLayers({
              tileSize: pixelRatio === 1 ? 256 : 512,
              ppi: pixelRatio === 1 ? undefined : 320
            });
            var map = new H.Map(document.getElementById('map'),
            defaultLayers.normal.map,{
              center: {lat: '<?php echo $latitude_center; ?>', lng: '<?php echo $longitude_center; ?>'},
              zoom: 7,
              pixelRatio: pixelRatio
            });
            var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
            var ui = H.ui.UI.createDefault(map, defaultLayers);
            addInfoBubble(map);
            map.addObject(circle);
            // map.addObject(polyline);
            // map.setViewBounds(polyline.getBounds());
            /*---------close map info  on click out side of it --------------------------*/
            $(document).mouseup(function(e)
            {
              var container = $(".H_ib_body");
              // if the target of the click isn't the container nor a descendant of the container
              if (!container.is(e.target) && container.has(e.target).length === 0)
              {
                container.hide();
              }
            });
            </script>
            @stop
