@extends('layouts.default-header')
@section('title', 'Forgot password')
@section('content')
<div class="formsection">
  @include('layouts.notify-message')
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="loginformbg formbg">
          <form class="loginform formarea" id="loginform" name="loginform" action="{{ route('login') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h2 class="text-center wow flipInY">Login</h2>
            <p class="text-center wow fadeInLeft">Sign up by entering the information below</p>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} forminput wow fadeInLeft">
              
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus id="email" placeholder="Enter email address">
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif 
            </div>
            <div class="buttondiv wow flipInX">
              <input type="submit" class="btn" value="Login">
            </div>
            <div class="formlink wow fadeInUp">
              <span>Dont have an account? Please</span><br/>
              <a href="{{ url('/register') }}">Signup</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection