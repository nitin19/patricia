@extends('layouts.default-header')
@section('title', 'Registeration')
@section('content')
<main>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="panel panel-floating-header">
          <div class="panel-heading">
            <h5>Personal Details<span class="important-text"> *</span></h5>
          </div>
          <div class="panel-body">
            <form class="signup-details-form form-horizontal" action="{{ url('/registerationstep2') }}" method="post">
              {{ csrf_field() }}
              <div class="signup-details-form__input ">
                <div class="floatlabel-wrapper" style="position:relative"><label for="id_phone_number" class="label-floatlabel">Phone number</label><input name="phone_number" required="" placeholder="Phone number" class="form-group form-control js-floatlabel" id="id_phone_number" type="text"></div>
                
              </div>
              <div class="signup-details-form__input ">
                <div>
                  <div class="form-group a js-changeable-form-field-input">
                    <div class="select">
                      <input id="autocomplete" placeholder="Enter your address"
                      onFocus="geolocate()" type="text"></input>
                      <table id="address">
                        <tr>
                          <td class="label">Street address</td>
                          <td class="slimField"><input class="field" id="street_number"
                          disabled="true"></input></td>
                          <td class="wideField" colspan="2"><input class="field" id="route" disabled="true" name="street"></input></td>
                        </tr>
                        <tr>
                          <td class="label">City</td>
                          <td class="wideField" colspan="3"><input class="field" id="locality" name ="city" disabled="true"></input></td>
                        </tr>
                        <tr>
                          <td class="label">State</td>
                          <td class="slimField"><input class="field"
                          id="administrative_area_level_1" name ="state" disabled="true"></input></td>
                          <td class="label">Zip code</td>
                          <td class="wideField"><input class="field" name ="postal_code"  id="postal_code"
                          disabled="true"></input></td>
                        </tr>
                        <tr>
                          <td class="label">Country</td>
                          <td class="wideField" colspan="3"><input class="field"
                          id="country" name ="country" disabled="true"></input></td>
                        </tr>
                        
                      </table>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div class="signup-details-form__input ">
                <div class="floatlabel-wrapper" style="position:relative"><label for="id_postal_code" class="label-floatlabel">Postal code</label><input name="postal_code" required="" placeholder="Postal code" class="form-group form-control js-floatlabel" id="id_postal_code" type="text"></div>
                
              </div>
              <br>
              <div class="text-center form-group">
                <button type="submit" class="btn btn-primary js-submit-button">Finish</button>
              </div>
            </form>
            <script>
            var placeSearch, autocomplete;
            var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
            };
            function initAutocomplete() {
            
            autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
            }
            function fillInAddress() {
            var place = autocomplete.getPlace();
            for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
            }
            for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            }
            }
            }
            function geolocate() {
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
            });
            }
            }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I&libraries=places&callback=initAutocomplete" async defer></script>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop