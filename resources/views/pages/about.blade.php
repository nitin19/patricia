@extends('layouts.default-header')
@section('title','Conheça o NaVizinhança')
@section('description','About us' )
@section('keywords','Sobre o NaVizinhança' )

@section('content')
<div class="top_div contactbanner">
    <div class="container">
        <h1>Sobre o NaVizinhança</h1>
    </div>
</div>
<div class="container">
	<div class="about-content">
        <h2>NaVizinhança</h2>
        <p>O NaVizinhança nasce com a finalidade de facilitar a contratação de prestadores de serviço na sua região. Os prestadores de serviço se cadastram gratuitamente, enquanto os contratantes (usuários que buscam os prestadores de serviço) os localizam por meio do nosso sítio/site.</p>
        <p>A jornada dá-se da seguinte forma:<p>
        <ol type="1">
            <li>Prestador de serviço se cadastra;</li>
            <li>Usuários contratantes localizam os prestadores no perfil que precisam;</li>
            <li>Usuários contratantes contatam esses prestadores diretamente;</li>
            <li>Usuários contratantes nos auxiliam informando como foi a experiência com este prestador de serviço por meio de avaliação/feedback em nosso sítio/site.</li>
        </ol>
        <p> Vale ressaltar que todo feedback é válido. Seja para informar que o telefone está incorreto ou para elogiar um prestador de serviço. O feedback é a única forma de mantermos a melhor rede de profissionais ativa para uma próxima busca que você e outros usuários façam futuramente.</p>
        <p>Nós acreditamos que o sucesso do NaVizinhança depende do número de prestadores de serviços cadastrados no site e do feedback/avaliação dos usuários que contataram ou contrataram esses profissionais.
        Sendo assim, cadastre-se, utilize nossos serviços e divulgue para seus amigos, sejam eles prestadores de serviços ou contratantes. Divulgando nosso site, você ajuda sua rede de relacionamento, ao mesmo tempo que viabiliza que sua próxima experiência no site seja ainda mais completa, com mais profissionais, com mais usuários contratando-os e avaliando-os e com mais funcionalidades para você.</p>
    </div>
</div>
@stop