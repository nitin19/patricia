@extends('layouts.default-header')
@section('title',$menu_meta_details['meta_title'])
@section('description', $menu_meta_details['meta_description'])
@section('keywords',$menu_meta_details['meta_keywords'] )

@section('content')
@include('layouts.flash-message')
<div class="top_div">
    <img src="{{ url('/public/') }}/images/09_contact us.jpg">
</div>
<div class="contact_box">
    <div class="container">
        <div class="form_section">
            <div class="row">
                <div class="col-sm-7 content_box">
                    <h3>Contact Us</h3>
                    <p>Conheça o NaVizinhança</p>
                    <form class="contactform" id="contactform" method="post" action="{{ url('/contact/send') }}">
	                    {{ csrf_field() }} 
	                    <div class="row">
	                        <div class="form-group col-md-6">
	                          <input class="form-control" name="first_name" placeholder="First Name" class="form-control" type="text">
	                        </div>
	                        <div class="form-group col-md-6">
							    <input type="text" class="form-control" name="last_name" placeholder="Last name">
							</div>
	                    </div>
	                    <div class="row">
	                        <div class="form-group col-md-6">
	                            <input class="form-control" name="email" id="email" placeholder="Email Address" class="form-control" type="text">
	                        </div>
	                        <div class="form-group col-md-6">
	                            <input type="text" class="form-control" placeholder="Phone Number" name="phonenumber" id="phonenumber" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');">
	                        </div>
							<div class="form-group col-md-12">
							    <textarea id="comment" placeholder="Message" name="comment" cols="40" rows="7" placeholder="" class="form-control" ></textarea>
							</div>
						</div>
						<div class="form-button">
						    <button type="submit" class="btn btn-box">Send Message</button>
						</div>
                    </form>
                </div>
		        <div class="col-md-5  content_maps">
		            <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1272195.4774994666!2d16.427430090198946!3d59.417374554482954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sP.O.+Box+532%2C+3225+Lacus.+Avenue+69761%2C+Stockholm!5e0!3m2!1sen!2sin!4v1539151452903" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div class="single contact-info">
					    <h3>Reach to us</h3>
						<ul class="list-unstyled">
						    <li>
						        <div class="icon"><i class="fa fa-map-marker"></i></div>
						        <div class="info"><p>P.O. Box 532, 3225 Lacus. Avenue <br>
						        69761, Stockholm</p></div>
						     </li>
							<li>
							    <div class="icon"><i class="fa fa-phone"></i></div>
							    <div class="info"><p>1800–419–7713</p><br></div>
							</li>
							<li>
							    <div class="icon"><i class="fa fa-envelope"></i></div>
							    <div class="info"><p><a href="mailto:contato@navizinhanca.com">contato@navizinhanca.com</a></p><br></div>
							</li>
						</ul>
					</div>
		        </div>
            </div>
        </div>
    </div>
</div>
@stop