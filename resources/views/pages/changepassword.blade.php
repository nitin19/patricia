@extends('layouts.default-header')
@section('title', 'Change Password')
@section('content')
@include('layouts.flash-message')
<div class="formsection">
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg">
          @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
          @endif   
          <form class="form-horizontal registerform formarea" method="POST" id="registerform" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h2 class="text-center  flipInY">Change Password</h2> 
            <p class="text-center  fadeInLeft">Change Password by entering the information below</p>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}  forminput  fadeInLeft">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Old Password" required autofocus>
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  forminput  fadeInLeft">
              <input id="password" type="password" class="form-control" name="password" id="password" placeholder="New Password" required>
              <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group forminput  fadeInLeft">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm New Password" required>
              <span toggle="#password-confirm" class="fa fa-fw fa-eye field-icon toggle-password">
            </div>
            <div class="form-group  fadeInLeft">
              <label class="checkboxdiv">I read and agree to term & conditions.
                <input type="checkbox" name="agree" id="agree" required>
                <span class="checkmark"></span>
              </label>
            </div>   
            <div class="buttondiv  flipInX">
              <input type="submit" class="btn" value="Sign Up">
            </div>
            <div class="formlink  fadeInUp">
              <span>Already Have an Account Please</span>
                <a href="{{ url('/login') }}">Login</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
  $.validator.addMethod("pwcheck", function(value) {
    return /^[A-Za-z0-9\d=!\-@._*#$%^&]*$/.test(value)
      && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[!@*#$%^&]/.test(value)
      && /\d/.test(value)
  });
  jQuery('#registerform').validate({
    rules: {
      password: {  
        required: true,
        pwcheck: true,
        minlength: 8,
        maxlength: 15   
      },
      password_confirmation: {
        minlength: 8,
        maxlength: 15,   
        equalTo: "#password"
      }
    },
    messages: {
      password: {
        required: "This field is required",
        pwcheck: "Password should contain alphabets, atleast 1 speacial character and 1 number",
        minlength: "Password length minimum 8 character",
        maxlength: "Password length minimum 15 character"
      },
      password_confirmation: {
        minlength: "Confirm password length minimum 8 character",
        maxlength: "Confirm password length minimum 15 character",
        equalTo: "The confirm password does not match with the password"
      }
    },
  });
</script>
@endsection