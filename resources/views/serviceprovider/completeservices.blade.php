@extends('layouts.default-header-withoutfooter')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

<style>
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
</style>
@section('content')
<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
   foreach($currentuser_ip as $currentuser_info) {
     $wish_profile_id = $currentuser_info->wish_profile_id;
     $currentuser_count = DB::table('wish_list')
     ->where('user_id', $login_userid)
     ->where('wish_profile_id', $wish_profile_id)
     ->count();
     if($currentuser_count==0 || $currentuser_count==''){
       $insert_whish = DB::table('wish_list')->insert(
         ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
       );
     }
   }
 }
 ?>
  <!-- @if(session()->has('message'))
  <div class="alert alert-success profle_alert">
    {{ session()->get('message') }}
  </div>
  @endif -->
 <!--<div id="dialog-message" title="My Dialog Alternative">-->
   <!--<p style='color:red'> Hello world </p>-->
   <section class="maincontent searchbarbg">

    @include('layouts.notify-message')
     <div class="col-sm-12 pagecontent whitebgdiv nopadding">
      <div class="container">
        <div class="acccompletionsec profile-accountcomplete">
           @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
           <div class="alert alert-danger alert-dismissible fade in profle_alert">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Atenção!</strong>  Complete seu cadastro abaixo.
           </div>
           @endif
         <!--  <h1>Account Completion</h1> -->
          <div class="acccompletion_tabs">
            <ul class="nav nav-tabs" role="tablist">
                <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
                @if($user_role=='Provider')
                <li role="presentation"  class=" @if (Session::has('success'))
                @endif" ><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Serviços</a></li>
                @endif
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">

          @if($user_role=='Provider')  
        <div role="tabpanel" class="tab-pane  active " id="services" >
          <div class="col-sm-12 form-group profileform_right">
            <form name="complete_service" action="{{ url('/savequeryprice') }}" id="complete_service" class="complete_service"  method="post">
              <div class="ser_tak_profile">
              {{ csrf_field() }}
              <input type="hidden" class="services_action" value="{{ url('/accountcomplete/service') }}">  
              <div class="col-md-12 form-group">   
                <div class="form-group col-md-6" id="maincat">
                  <input type="hidden" name="main_categroy_id" id="main_category" class="form-control" value="{{ $user_info->cat_id }}">
                </div>
                <div class="form-group col-md-6" id="sub_cat">
                  <input type="hidden" class="sub_cat_check_address" value ="{{ url('/check_subcategory/') }}">
                  <input type="hidden" name="sub_categroy_id"  class="form-control" id="sub_category" value="{{ $user_info->sub_id }}"> 
                   
                  </div>
                  
                  <div id="subcat_queries"></div>
                </div>
                <!-- <div class="">
                    <div class="col-md-12"> 
                      <a style="padding: 7px 22px;" href="{{ url('/serviceprovider/profile')}}" class="btn btn-default pull-right">Cancelar</a> 
                    </div>
                  </div> -->
                </div>
              </form>
            </div>
          </div>
          @endif

        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>

</main>
<script type="text/javascript">
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
  </script>
<!--<script-->
<!--  src="https://code.jquery.com/jquery-3.3.1.min.js"-->
<!--  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="-->
<!--  crossorigin="anonymous"></script>-->
<script>
  /*-------price formatting-----------*/


/*$("input[data-type='currency']").on({
    keyup: function() {
      //this.value = this.value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(",") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(",");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = left_side + "," + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ",00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}*/



    /*------end price formatting-----*/
  $(document).ready(function(){
   
       $('input[type=submit]').click(function() {
         $(this).attr('disabled', 'disabled');
         $(this).parents('form').submit();
       });

   /*   window.history.pushState('', null, './');
      $(window).on('popstate', function() {
        var prev_url =  '{{ url()->previous() }}';
        window.location.href=prev_url;
      });*/


      // if($("input[name=phone]").val() == 'N/A' || $("input[name=address]").val() == 'N/A'){
      //     alert('please complete your profile');
      // }
// $('.hides1').hide();
   // $('.hides2').hide();
    // $( "#dialog-message" ).dialog({
    //       modal: true,
    //       buttons: {
    //       Ok: function() {
    //         $( this ).dialog( "close" );
    //       }
    // }});
   // $('.hides1').hide();
   // $('.hides2').hide();
  //  jQuery('#store_availablity').validate({
  //    rules: {
  //     date: {
  //       required: true,
  //     },
  //
  //   },
  //   messages: {
  //     date: {
  //       required: "This field is required",
  //     }
  //   },
  // });
    // $('#main_category').multiselect();

    // $('#demo').multiselect();

    var sub_category = $('#sub_category').val();
    var main_category = $('#main_category').val();
    if(main_category!='' && sub_category!='') {
        $.ajax({
        type:"get",
        url:"{{url('/subcategory_queries')}}",
        data:{'sub_category': sub_category, 'main_category': main_category},
        success:function(data){
            $('#subcat_queries').html(data);
            $("input[data-type='currency']").on({
              keyup: function() {
                //this.value = this.value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                formatCurrency($(this));
              },
              blur: function() { 
                formatCurrency($(this), "blur");
              }
          });
        }
      })
    }

  var base_url = '<?php echo url('/'); ?>';

  $( "#address" ).on('blur',function() {
        //   alert('fdg');
        if ($( "#address" ).val().length == 0 ) {
          $('#zipcode').val('');
          $('#country option').text('');
          return;
        }
        var address_val = $('#address').val();
        var val =  $(this).val();
        var url = '/convert_address/'+address_val;
        var data  = $('#completionform').serialize();
            //console.log(data);
            $.ajax({
              type: 'GET',
              url: url,
              dataType: 'json',
              success: function(data ){
                console.log(data.res.zipcode);
                if(data.success = "true"){
                  $('#zipcode').val(data.res.zipcode);
                  $('#country option').text(data.res.country_name);
                }
              },
              error: function(data){
               alert('error');
             }
           });
          })

  $('.datetimepicker1').datetimepicker({
   format: 'DD/MM/YYYY'
 });
  $('#main_category').on('change',function(){
    var url = $('.sub_cat_check_address').val();
    // alert(url);
    var data = $('#complete_service').serialize();
    // var url = '/accountcomplete/check_subcategory/';
    $.ajax({
      type:"POST",
      dataType:'JSON',
      url:url,
      data:data,
      success:function(data){
        $('#sub_category').empty();
        if(data != ''){
          $('#sub_category').html('<option value="" selected disabled>Selecione um serviço</option>');
          $.each(data,function(k,v){
            // $('#sub_category option').val(k);
            // $('#sub_category option').text(v);
            // Create option
            //console.log(k);
            var option = $("<option />");
            option.attr("value", k).text(v);
            $('#sub_category').append(option);
          });
        }
        else{
          var option = $("<option />");
          option.attr("value", '0').text('Desculpe dados não encontrados');
          $('#sub_category').append(option);
        }

      }
    })
  });
  // $('.next_1').on('click',function(){
  //   var addRess = $('.proAddress').val();
  //   var profileNumbers = $('.profileNumber').val();
  //   if(addRess == 'N/A' || addRess == '' ){
  //     var msgss ='Please enter the address.';
  //     $('.msgAlert').show();
  //     $('.msgAlertVal').html(msgss);
  //     }if(profileNumbers == 'N/A' || profileNumbers == ''){
  //       var msgss1 ='Please enter the valid phone number.';
  //       $('.msgAlert').show();
  //       $('.msgAlertVal').html(msgss1);
  //   }
  // });
  $(document).ready(function(){
  $("#maincat").on("change", function(){
    $("#sub_cat").show();
  });
});

  $('#sub_category').on('change',function(){
    var sub_category = $(this).val();
    var main_category = $('#main_category').val();
    $.ajax({
      type:"get",
      url:"{{url('/subcategory_queries')}}",
      data:{'sub_category': sub_category, 'main_category': main_category},
      success:function(data){
          $('#subcat_queries').html(data);
          $("input[data-type='currency']").on({
            keyup: function() {
              //this.value = this.value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
              formatCurrency($(this));
            },
            blur: function() { 
              formatCurrency($(this), "blur");
            }
        });
      }
    })
  });

})
</script>
<script type="text/javascript">
  jQuery.extend(jQuery.validator.messages, {
    required: 'Campo obrigatório. Se não fizer sentido para sua atividade, preencha com um valor estimado/aproximado.'
    //remote: 'bla bla bla'
  });
</script>
@stop
