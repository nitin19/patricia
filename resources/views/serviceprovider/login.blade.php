<?php Session::put('backUrl', URL::previous());
 $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','login')->where('is_active', '1')->where('is_deleted', '0')->first();?>

@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<div class="formsection">
    <div class="container">
        <div class="formdiv loginformdiv">
            <div class="col-sm-8 col-md-7 col-lg-5 centered">
                <div class="loginformbg formbg">
                
                @include('layouts.notify-message')
                    <form class="loginform formarea" id="loginform" name="loginform" action="{{ route('login') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <h2 class="text-center  flipInY">Entrar</h2>
                       
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} forminput  fadeInLeft">
                            <label>E-MAIL</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus id="email" placeholder="e-mail">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  forminput  fadeInLeft">
                            <label>SENHA</label>
                            <input id="password" type="password" class="form-control" name="password" required  id="password" placeholder="senha">
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                              
                        </div>
                        <div class="form-group buttondiv">
                            <input type="submit" class="btn btn-primary btn-lg" value="Entrar">
                            <span class="formlink">
                                <a href="{{ url('/forgotpassword') }}" class="pull-right martop">Esqueci minha senha ?</a>
                            </span>
                        </div>

                        <div id="status"></div> 
                        <!-- Facebook login or logout button -->
                        <p style="text-align: center;"><a class="btn btn-primary btn-large Facebookbtn" style="width: 100%;" href="javascript:void(0);" onclick="fbLogin()" id="logininfacebook"><i class="fa fa-facebook-square" aria-hidden="true"></i> Entrar com Facebook</a></p>
                        <!-- Display user profile data -->
                        <div id="userData"></div>
             
                        <p style="text-align: center;">    
                            <a class="btn btn-primary btn-large" href="{{ url('/register') }}" style="width: 100%;"><span>Não tem conta?</span> Criar Conta</a>
                        </p> 
                    </form> 
                </div> 
            </div>
        </div>         
    </div>
</div>
<style> 
   #password::placeholder, #email::placeholder {
  color: #666 !important;
}

#password:-ms-input-placeholder, #email:-ms-input-placeholder { 
  color: #666 !important;
}
 
#password::-ms-input-placeholder, #email::-ms-input-placeholder {
  color: #666 !important;
}  
#password, #email  {
    color:#666 !important;
}

</style>
@endsection