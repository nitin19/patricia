@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<!-- @if($spavailabilityInfo->isCompleted=='0') 
<div class="alert alert-danger alert-dismissible fade in profle_alert">
<strong>Atenção!</strong> Preencha os dias da semana e horários em que você está disponível. <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui</a> 
</div>
@endif -->
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{url('/serviceprovider/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
             <li role="presentation" class="active"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="bookingmsg" role="tab">  Contatos & Mensagens</a></li>
             <li role="presentation"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="settings" role="tab">Sua conta</a></li>
             <!--<li role="presentation"><a href="{{url('/serviceprovider/switch/role')}}" aria-controls="switch" role="tab">Mudar de função</a></li>-->
           </ul>
         </div>
       </div>
       @include('layouts.flash-message')
       <?php
       $profile_image = $user_info->profile_image;
       $profile_image = $user_info->profile_image;
       $user_role = $user_info->user_role;
       ?>
       <!-- Tab panes -->
       <div class="tab-content">

        <div role="tabpanel" class="tab-pane codeRefer active" id="bookingmsg">

          <div class="bookingmsg_sec">
            <div class="container">
              <div class="bookmsgsec">
                <div class="bookmsgtab" id="">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="{{url('/serviceprovider/booking/pending')}}" aria-controls="pending" role="tab" >Pendentes <span class="tkr-count">@if($booking_payment_pending_count)({{ $booking_payment_pending_count }})@endif</span></a></li>
                    <li role="presentation" class="active"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="confirmed" role="tab" >Aceitas <span class="tkr-count">@if($booking_payment_confirm_count)({{$booking_payment_confirm_count}})@endif</span></a></li>
                    <li role="presentation"><a href="{{url('/serviceprovider/booking/declined')}}" aria-controls="past" role="tab" >Negadas <span class="tkr-count">@if($booking_payment_past_count)({{ $booking_payment_past_count }})@endif</span></a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <!--confirmed-modal-->
                    <div role="tabpanel" class="tab-pane active" id="confirmed">
                      <div class="tablewrapper tablediv">
                        @if($booking_payment_confirm_count>0)
                        <table class="table-bordered table-striped">
                          <thead class="tableheader">
                            <tr class="headings">
                              <th class="column3">Nome</th>
                              <th class="column3">Mensagem</th>
                              <th class="column3">Categoria</th>
                              <th class="column3">Serviço</th>
                              <th class="column3">Data de início</th>
                              <!-- <th class="column2">Data final</th> -->
                              <th class="column3">Valor base</th>
                              <th class="column2">Sobre o contratante</th>
                            </tr>
                          </thead>
                          <tbody class= "data_fill_confirm">
                            
                            @foreach($booking_payment_confirm as $booking_payment_confirm_info)
                            <?php
                            $bookingid = $booking_payment_confirm_info->booking_id;
                            $payment_confirmuser_book_by = $booking_payment_confirm_info->user_id;
                            $payment_confirmbooking_message = $booking_payment_confirm_info->booking_message;
                            $payment_confirmbooking_amount = $booking_payment_confirm_info->booking_amount;
                            $payment_confirmcategory = $booking_payment_confirm_info->category;
                            $start_time = date('d-m-Y', strtotime($booking_payment_confirm_info->booking_start_time. "+1 days"));
                            $end_time = date('d-m-Y', strtotime($booking_payment_confirm_info->booking_end_time. "+1 days"));
                            $fmt = datefmt_create(
                                'pt-BR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL,
                                'Brazil/East',
                                IntlDateFormatter::GREGORIAN,
                                "dd/MMM/YYYY"  
                            );
                              //start time
                            $dt = new DateTime($start_time);
                            $d = $dt->format('d');
                            $m = $dt->format('m');
                            $y = $dt->format('Y');
                           
                            
                            $payment_confirmbooking_start_time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                            //$payment_confirmbooking_start_time = date('j/F/ Y ', strtotime($booking_payment_confirm_info->booking_start_time));
                           
                            $payment_confirmuser_book_to = $booking_payment_confirm_info->profile_id;
                            $paymentuser_info = DB::table('users')
                            ->where('id', $payment_confirmuser_book_by)
                            ->first();      
                            $paymentname = $paymentuser_info->name;
                            $paymentprofile_image = $paymentuser_info->profile_image;
                            if($paymentprofile_image==''){
                              $payment_confirmuserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                            }
                            else{
                              $payment_confirmuserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                            }
                            ?>
                            <tr class="tabledata confirmedtable leftbox confirm_{{ $booking_payment_confirm_info->booking_id }}">

                              <td class="column2 bkngActionMob leftbox2"> 
                                  <button class="btn btn-success btn-sm butpadd tofeedback_{{ $booking_payment_confirm_info->booking_id }} touser_id" href="#" data-toggle="modal" data-target="#feedbackmodal" data-bookingid="{{ $booking_payment_confirm_info->booking_id }}" data-touser_id="{{ $booking_payment_confirm_info->user_id }}"  data-providerNm="{{ ucfirst($paymentname) }}"><i class="fa fa-comments" data-toggle="tooltip" data-placement="top" data-providerNm="{{ ucfirst($paymentname) }}" data-original-title="Avalie o contratante" title="Avalie o contratante" ></i></button>
                                 <button  class="btn btn-success btn-sm butpadd blackuserModal_{{ $booking_payment_confirm_info->booking_id }}" data-toggle="modal" data-target="#blackuserModal_{{ $booking_payment_confirm_info->booking_id }}"  data-bookingid="{{ $booking_payment_confirm_info->booking_id }}" ><i data-toggle="tooltip" data-placement="top" class="fa fa-bug" data-original-title="Solicitar bloqueio"></i></button></td>

                              <td class="username column3 leftbox3" ><h4><a>
                                @if($paymentprofile_image=='')
                                <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                @else
                                <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>">
                                @endif
                                <span data-toggle="modal" data-target="#exampleModal_{{ $booking_payment_confirm_info->booking_id }}">{{ substr(ucfirst($paymentname),0,13) }} @if (strlen($paymentname)>='13')...@endif
                            </span></a></h4></td>
                               <td class="userdesc column3 leftbox3" id="msgrow{{$bookingid}}"><p><?php if(strlen($payment_confirmbooking_message) > 100){ echo substr($payment_confirmbooking_message, 0, 40). '<span class="dots" id="dotmsg'.$bookingid.'">...</span><span class="readmore" id="moremsg'.$bookingid.'">'.substr($payment_confirmbooking_message, 100, strlen($payment_confirmbooking_message)).'</span><span class="btnmore">Veja mais</span><span class="btnless">Veja menos</span>';}else{ echo $payment_confirmbooking_message;} ?></p>
                                  </td>
                                <td class="column3 leftbox3">{{ $payment_confirmcategory }}</td>
                                <td class="column3 leftbox3">{{ $booking_payment_confirm_info->sub_category}}</td>
                                <td class="column3 leftbox3">{{ $payment_confirmbooking_start_time }}</td>
                               
                                <td class="column3 leftbox3"><span class="hifen">#</span>{{ $payment_confirmbooking_amount }}</td>
                                <td class="column2 leftbox3 bkngActionDsk"> 
                                 <!--  <button class="btn btn-success btn-sm butpadd" data-toggle="modal" data-target="#exampleModal_{{ $booking_payment_confirm_info->booking_id }}"   ><i class="fa fa-eye approv_btn" data-toggle="tooltip" data-placement="top" title="Dados do contratante"></i></button> -->
                                  <button class="btn btn-success btn-sm butpadd tofeedback_{{ $booking_payment_confirm_info->booking_id }} touser_id" href="#" data-toggle="modal" data-target="#feedbackmodal" data-bookingid="{{ $booking_payment_confirm_info->booking_id }}" data-touser_id="{{ $booking_payment_confirm_info->user_id }}"  data-providerNm="{{ ucfirst($paymentname) }}"><i class="fa fa-comments" data-toggle="tooltip" data-placement="top" data-providerNm="{{ ucfirst($paymentname) }}" data-original-title="Avalie o contratante" title="Avalie o contratante" ></i></button>
                                 <button  class="btn btn-success btn-sm butpadd blackuserModal_{{ $booking_payment_confirm_info->booking_id }}" data-toggle="modal" data-target="#blackuserModal_{{ $booking_payment_confirm_info->booking_id }}"  data-bookingid="{{ $booking_payment_confirm_info->booking_id }}" ><i data-toggle="tooltip" data-placement="top" class="fa fa-bug" data-original-title="Solicitar bloqueio"></i></button></td>
                              </tr>
                              <!-- Modal -->
                                <div class="modal fade exampleModal_{{ $booking_payment_confirm_info->booking_id }}" id="exampleModal_{{ $booking_payment_confirm_info->booking_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Dados do contratante</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div> 
                                      @if(!empty(Auth::user()))
                                       <?php $userblock = DB::table('block_providers')->where('taker_id','=',$payment_confirmuser_book_by)->where('provider_id','=',Auth::user()->id)->where('block_status','=','1')->first();
                                        $rating_avg = DB::table('feedback')
                                        ->where('user_id', $payment_confirmuser_book_by)
                                        ->where('approve_status', 1)
                                        ->where('role', 'provider')
                                        ->avg('rating');
                                      if(empty($userblock)) { 
                                          
                                        ?>
                                      <div class="modal-body">
                                        <ul class="model-profile">
                                          @if($paymentprofile_image=='')
                                          <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="profimg">
                                          @else
                                          <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>" class="profimg">
                                          @endif
                                          <li>Nome: <span>{{ ucfirst($paymentuser_info->name) }}</span></li>
                                          <li>E-mail: <span>{{ $paymentuser_info->email }}</span></li>
                                          <li>Celular: <span>{{ $paymentuser_info->phone }}</span></li>
                                          <li>Bairro: <span>{{ ucfirst($paymentuser_info->city) }}</span></li>
                                          <li>Feedback:<span><?php
                                          $x=0; 
                                          if($rating_avg != ''){
                                            $number = number_format($rating_avg,1);
                                            $integer_part = floor($number);
                                            $fraction_part = $number-$integer_part;
                                            
                                            for($x=1;$x<=$number;$x++) {?>
                                              <i class="fa fa-star" style="color:#ffcd00;"></i>
                                            <?php }
                                            // echo '(stars)';
                                            if (strpos($number,'.')) {
                                              if($fraction_part != 0) {
                                                ?>
                                                <i class="fa fa-star-half-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                                <?php 
                                                $x++;
                                              }else { ?>
                                                 <i class="fa fa-star-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                              <?php }
                                            } } else {
                                            while($x<5){ ?>
                                             <i class="fa fa-star-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                            
                                             <?php
                                             $x++;
                                           }
                                           
                                         }
                                         ?>
                                           </span></li>
                                            <!-- <li style="display: flex;">Documents:<span>
                                           <?php  $user_documents = DB::table('user_documents')->where('user_id', $paymentuser_info->id)->where('is_active', '1')->where('document_status', '1')->where('is_deleted', '0')->get();?>
                                           @if($user_documents)
                                       
                                           @foreach($user_documents as $documents)
                                          
                                           <img src="{{ url('/public') }}/images/user_document/{{ $documents->document }}" class="img-responsive" height: 20px; style="width: 20%!important;float: left;">
                                         
                                           @endforeach
                                         
                                           @else
                                           No document uploaded.
                                           @endif
                                            </span></li> -->
                                         
                                        </ul>
                                      </div>
                                    <?php } else {  ?>
                                      <div class="modal-body">
                                        <h5>You not able to view profile.Because you blocked by <span style="color:blue;">{{ ucfirst($paymentuser_info->name) }}</span></h5>
                                      </div>
                                    <?php } ?>
                                    @endif
                                    </div>
                                  </div>
                                </div>
                              <div class="feedback_info_{{ $booking_payment_confirm_info->user_id }}">
                                      <input type="hidden" name="provider_id" class="p_id" value="{{ Auth::user()->id }}" required="required">
                                      <input type="hidden" name="prviderName" value="{{ucfirst(Auth::user()->name)}}" required="required">
                                      <input type="hidden" name="user_id" value="{{ $booking_payment_confirm_info->user_id }}" required="required">
                                      <input type="hidden"  name="booking_id" value="{{ $booking_payment_confirm_info->booking_id }}" required="required">
                                      <input type="hidden"  name="_token" value="{{ csrf_token() }}" required="required">
                                      <input type="hidden"  name="role" value="provider">
                              </div>
                              <!-- black user popup -->
                              <div class="modalsection">

                                <div class="modal fade blacknow_popup" id="blackuserModal_{{ $booking_payment_confirm_info->booking_id }}" tabindex="-1" role="dialog" aria-labelledby="blackuserModalModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <h2 class="text-center" style="color: #fdfdfd;background-color: #1b9bd8;border-color: #1b9bd8;padding:20px;font-size: 18px;margin:0px;">Ajude-nos a entender o motivo de solicitar o bloqueio desse contratante no nosso site: <a href="#" class="close close_about" data-dismiss="modal" aria-label="close">&times;</a></h2>
                      
                                        <div class="modal-body">
                                          <div class="alert alert-success blackSuccess" id="blackSuccess" style="display:none;"></div>
                                            <div class="alert alert-danger blackError" id="blackError"style="display:none;"></div>
                                              <form id="blackForm">
                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="form-group datesec">
                                                      <h4>Selecione o motivo:</h4>
                                                      <label class="radio_btn_label">Contratante não responde nos contatos disponibilizados
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante não responde nos contatos disponibilizados">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante não efetuou os pagamentos conforme combinado 
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante não efetuou os pagamentos conforme combinado ">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante inviabiliza a finalização dos serviços conforme combinado
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante inviabiliza a finalização dos serviços conforme combinado">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante com comportamento inadequado
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante com comportamento inadequado">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <input type="hidden" id="from_id" name="from_id" value="{{ Auth::user()->id }}">
                                                      <input type="hidden" id="to_id" name="to_id" value="{{ $paymentuser_info->id }}">
                                                      <input type="hidden" id="to_name" name="to_name" value="{{ ucfirst($paymentuser_info->name) }}">
                                                      <input type="hidden" id="from_name" name="from_name" value="{{ ucfirst(Auth::user()->name) }}">
                                                      <input type="hidden" id="user_type" name="user_type" value="Provider">
                                                       <input type="hidden"  name="_token" value="{{ csrf_token() }}" required="required">
                                                    </div>
                                                  </div>
                                                </div>       
                                                <div class="blackBtn">
                                                  <input type="submit" class="btn btn-primary blackuserBtn" id="blackBtn" value="Enviar">
                                                </div>
                                              </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- end black users -->
                              @endforeach

                            </tbody>
                          </table>
                           <div align="center">{{ $booking_payment_confirm->links() }}</div>
                          @else
                          <tr>
                            <h2>Sem propostas confirmadas para você no momento.</h2></tr>
                            @endif
                          </div> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="feedbackmodal" role="dialog">
  <div class="modal-dialog feedback_modaldiv">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Avaliação/feedback: <span class="providertitle"></span></h4>
      </div>
      <div class="modal-body">
        <div class="message" style="display:none;">
          <div class="alert alert-info alert-dismissible  msgAlerts fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          </div> 
        </div>
        <h3>Favor avalie o contratante preenchendo os dados abaixo.</h3>
        <form method="post" id="feedback_rating" class="fordata">
          <div class="form-group" id="rating-ability-wrapper">
            <label class="control-label" for="rating">
              <span class="field-label-header">Sua nota para esse contratante:</span><br>
              <span class="field-label-info"></span>
              <input type="hidden" id="user_id" name="user_id" value="" class="rating_score">
              <input type="hidden" id="selected_rating" name="selected_rating" value="" required="required" class="rating_score">
            </label>
            <div class="demo">
            </div>
            <div class="fordata2">
            </div>
          </div>
          <textarea name= "message" rows="5" class="form-control" placeholder="Comente sobre sua satisfação na execução do serviço para esse contratante (pontualidade no pagamento, tratamento cortês/atencioso, boa comunicação...)." required oninvalid="this.setCustomValidity('Favor inserir seu comentário sobre o Contratante.')" oninput="this.setCustomValidity('')"></textarea>
          <input type="submit" class="modalsubmit" value="Enviar">
        </form>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  function confirmMessage()
  {
    var retVal = confirm("Are you sure?");
    if( retVal == true ) {
      return true;
    }
    else {
      return false;
    }
  }
  var base_url = '<?php echo url('/');?>';
  $('.change_role').on('click',function(){
    if(confirm("Are you Sure You want to logout By clicking here You are logout form website shortly,you have to login again with changed role")){
     $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
     $('#switchdashboard').submit();
   }
   else{
    return false;
  }

});
  $('.status_submit').on('click', function(e) {
   e.preventDefault(); 
   var status = confirm("Are you sure?");
   if( status == true ) {

    var token = $('.book_token').val();
    var bookingId = $('.bookId').val();
    var status = $('.status_submit').val();
    var host = '{{ url('/') }}';
     // alert("#pending_"+bookingId);die;
     $.ajax({
       type: "POST",
       url: host+'/providerbooking/status',
       data: {'booking_id':bookingId,'_token':token,'approve_status':status},
       dataType: "json",
       success: function(response) {
         $(".statusAlerts").show();
         $(".statusAlerts").html(response.msg);
         $("#pending_"+bookingId).hide();
         if(response.status == '1'){
          $('.data_fill_confirm').focus();
          $(".data_fill_confirm").append(response.html);
        } else if(response.status == '2') {
          $('.data_fill_confirm').focus();
          $('.data_fill_confirm').append(response.html);
        }else {
          $(".data_fill_pending").focus();
          $(".data_fill_pending").append(response.html);
        }
        setTimeout(function () {
          $('.statusAlerts').hide();
        }, 3000);
      }
    });
   } else {
    return false;
  }

});

</script>
<script type="text/javascript">
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
      'notifications.id',
      $.ajax({
        url: "/dashboard/notifications",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          jQuery('.success_show').show();
          jQuery('.main_'+idd).remove();
        }            
      });      
    }
  });
</script>

<script>
$(document).ready(function () {
  /*$(".confirmedtable").click(function() {
    var h = $(this).data("href");
    console.log(h);
        window.location = $(this).data("href");
    });*/

  $(".btnmore").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
  });

  $(".btnless").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
  });
      $(".touser_id").click(function(){
      var touser_id = $(this).data("touser_id");
      var feed_html = $(".feedback_info_"+touser_id).html();
      var bid = $(this).data("bookingid");
      var pname = $(this).attr("data-providerNm");
      $(".providertitle").html(pname);
          var providerID = $('.providerID_'+bid).val();
          var hosts = '{{ url('/') }}';
          var value = $('.unblocked_'+bid).data("prostatus");
    
        if(value == 1){
          $('.blkserviveprovider_'+bid).removeAttr("href");
          $('.unblkavil_'+bid).removeAttr("data-target");
          $('.blkfeed_'+bid).removeAttr("data-target");
          } 
        //   else {
         //   $('.blkserviveprovider_'+bid).attr("href",hosts+'.profile/'+providerID);
         //   $('.unblkavil_'+bid).attr("data-target",'#myModal');
         //   $('.blkfeed_'+bid).attr("data-target",'#feedbackmodal');
         // }
         
      $('.fordata2').html('');
      var data = $(".fordata2").append(feed_html);
      $(".demo").raty('set', {'score' : 0});
    });
      var base_url = 'https://www.navizinhanca.com';
      $('.demo').raty({
        starHalf    : base_url+'/public/images/star-half.png',
        starOff     : base_url+'/public/images/star-off.png',
        starOn      : base_url+'/public/images/star-on.png',
        hints       : ['bad', 'poor', 'regular', 'good', 'gorgeous'],
        click:function(score){
          $(".rating_score").val(score);
        } 
      });
        //rating formdata
  $('#feedback_rating').on('submit', function(e) {
    e.preventDefault(); 
    var formData = $(this).serialize();
    var host = '{{ url("/") }}';
    $.ajax({
      type: "POST",
      url: host+'/feedback/create',
      data: formData,
      dataType: "json",
      success: function(response) {
            // console.log(response);
            if(response.success == true){
              $(".message").show();
              $(".msgAlerts").html(response.msg);
              //$('.forid').hide();
              $('.feedback_text').html('Avaliação em progresso');
              setTimeout(function(){
                $(".message").hide();
                $('#feedbackmodal').modal('hide');
                $("#feedback_rating")[0].reset();
              },4000);
            }else{
              $(".message").show();
              $(".msgAlerts").html(response.msg);
              setTimeout(function(){
                $(".message").hide();
                $('#feedbackmodal').modal('hide');
                $("#feedback_rating")[0].reset();
              },3000);
            }
        }
    });
  });


});
</script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
// ajax black list
    $('.blackuserBtn').click(function(e) {
      e.preventDefault();
      var black_reason = $('#black_reason').val();
      var Url  = '<?php echo url('/');?>';
      if(black_reason=='') {
       $('#black_reason').addClass('vlderror');
       return false;
       }
      $.ajax({
           type:'POST',
           url:Url+'/addblackuser',
           data:$('#blackForm').serialize(),
           success:function(data){
            data=jQuery.parseJSON(data);
            if(data.msgcode == 1) {
              
              $("#blackForm")[0].reset();
              $('.blackSuccess').html(data.message);
              $('.blackSuccess').show();
              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');
              
              window.setTimeout(function () {
                $(".blackSuccess").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  //window.location.href="<?php echo url('/');?>/profile/"+profile_id;
                });
              }, 3000);

            } else {

              $('.blackError').html(data.message);
              $('.blackError').css('opacity','1');
              $('.blackError').show();

              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');

              window.setTimeout(function () {
                $(".blackError").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  //$('#blackuserModal').modal('hide');
                });
              }, 2000);

            }
          }
        });
       });

    //end ajax black list
</script>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("This field cannot be left blank");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})
</script>
<style type="text/css">
  .readmore, .btnless{
    display: none;
  }
  @media (min-width: 767px){
.column4 {
    width: 16%;
    float: left;
}
.column5 {
    width: 8%;
    float: left;
    text-align: center !important;
}}
button.btn.btn-success.btn-sm.status_submit_approve {
    background-color: #1B9BD8 !important;
    border: 1px solid #1B9BD8 !important;
}
</style>

@stop
