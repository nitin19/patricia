@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<!-- @if($spavailabilityInfo->isCompleted=='0') 
<div class="alert alert-danger alert-dismissible fade in profle_alert">
 <strong>Atenção!</strong> Preencha os dias da semana e horários em que você está disponível. <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui</a>
</div>
@endif -->
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
               <li role="presentation"><a href="{{url('/serviceprovider/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="bookingmsg" role="tab"> Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="settings" role="tab">Sua conta</a></li>
             <!-- <li role="presentation"><a href="{{url('/serviceprovider/switch/role')}}" aria-controls="switch" role="tab">Mudar de função</a></li>-->
            </ul>
          </div>
        </div>
        <!-- @include('layouts.flash-message') -->
        @include('layouts.notify-message')
        <?php
        $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;
        ?>
        <!-- Tab panes -->
        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active" id="account">
            <div class="container">
              <div class="accounttabs">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
                <!-- <li role="presentation"><a href="{{url('/serviceprovider/card/details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                <li role="presentation" class="active"><a href="{{url('/serviceprovider/notifications/details')}}" aria-controls="notifications-tabs" role="tab" >Notificações</a></li>
                <li role="presentation" class="close_account"><a href="{{url('/serviceprovider/closeaccount')}}" aria-controls="closeacc" role="tab" >Encerrar seu cadastro</a></li>
                </ul>
                <div class="tab-content profiletabss">
                <div role="tabpanel" class="tab-pane active" id="profileinfo">
                  <div class="acccompletion_tabs">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                    <!--  <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                        <div class="col-sm-12 form-group profileform_right">
                          <form name="complete_service" id="complete_service" class="complete_service" action="{{ url('/accountcomplete/service') }}" method="post">
                            csrf_field() 
                            <?php
                            $user_cateogry = $user_info->category;
                            foreach($category as $category_name){
                              $categrry_name = $category_name->cat_name;
                              ?>
                              <div class="col-sm-3 form-group">
                                <label class="col-sm-11"><?php echo ucfirst($category_name->cat_name); ?></label><div class="profileinput col-sm-1 nopadding"><input type="checkbox" name="cat_name[]" value="<?php echo $category_name->cat_name; ?>" <?php if (strpos($user_cateogry, $categrry_name) !== false) {
                                  echo 'checked';
                                } ?> data-trigger="focus"></div>
                              </div>
                              <?php
                            }
                            ?>
                            <div class="col-sm-12 nextbtn">
                              <input class="form-control" value="Save" type="submit">
                            </div>
                          </form>
                        </div>
                      </div> -->
                    </div>

                  </div>
                </div>
              </div>
                 <div role="tabpanel" class="tab-pane active " id="notifications-tabs">
                    <div class="tabcontentbg">
                      <div class="tablewrapper tablediv">
                        <!--====notication code start--==-->
                        <div class="notificationbox">
                         <!--  <div class="alert alert-success alert-block success_show">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>Notification deleted successfully!</strong>
                          </div>  -->

                          <div class="firstnotification">
 
                            @if(count($listresult)>0)

                            @foreach($listresult as $allnotifications)
                            <?php
                            $booking_message = $allnotifications->booking_message;
                            $booking_time = $allnotifications->notify_time;
                            $msg = $allnotifications->notify_message;
                            $profile_image = $allnotifications->provider_image;
                            $username = $allnotifications->provider_name;
                            if($allnotifications->notificaton_type == 'booking') {
                                    if($allnotifications->booking_verified==0) {
                                      $hyperLink =  url('/serviceprovider/booking/pending');
                                    } elseif($allnotifications->booking_verified==1) {
                                      $hyperLink =  url('/serviceprovider/bookings');
                                    } else {
                                      $hyperLink =  url('/serviceprovider/booking/declined');
                                    }
                            ?>
                            <div class="main_notificationData main_{{ $allnotifications->notify_id }}">
                              <div class="notifidata"> 
                                  <a href="<?php echo $hyperLink;?>" class="btn btn-success join mess" style="background-color: #0c3e64;border: 1px solid #0c3e64;color: #fff;font-size: 12px;padding: 3px 20px;border-radius: 40px;margin-bottom: 10px;">Mensagem recebida</a> 
                                <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                <div class="media">
                                  <div class="media-left">
                                    @if($profile_image=='')
                                    <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                    @else
                                    <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                    @endif
                                  </div>
                                  <div class="media-body">
                                  <a href="<?php echo $hyperLink;?>">
                                    <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                    <h3>Mensagem:  {{ $msg }}</h3>
                                  </a>  
                                  </div>
                                </div>
                                <hr class="notiboxline">
                              </div>
                            </div>
                            <?php } elseif($allnotifications->notificaton_type == 'document') { 
                              if($allnotifications->document_status==1) {
                                        $hyperLink =  url('/serviceprovider/profile');
                                      } else {
                                        $hyperLink = url('/serviceprovider/profile');
                                      }
                              ?>
                            <div class="main_notificationData main_{{ $allnotifications->notify_id }}">
                              <div class="notifidata"> 
                                  <a href="<?php echo $hyperLink;?>" class="btn btn-success join mess" style="background-color: #0c3e64;border: 1px solid #0c3e64;color: #fff;font-size: 12px;padding: 3px 20px;border-radius: 40px;margin-bottom: 10px;">Mensagem recebida</a> 
                                <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                <div class="media">
                                  <div class="media-left">
                                    @if($profile_image=='')
                                    <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                    @else
                                    <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                    @endif
                                  </div>
                                  <div class="media-body">
                                  <a href="<?php echo $hyperLink;?>">
                                    <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                    <h3>Mensagem:  {{ $msg }}</h3>
                                  </a>  
                                  </div>
                                </div>
                                <hr class="notiboxline">
                              </div>
                            </div>
                            <?php } ?>

                            @endforeach
                            @else

                            <h2>Nenhuma notificação encontrada.</h2>
                            @endif
                          </div> 
                        </div>
                         <div class="pagination_section "> 
                              {{ $listresult->links() }}
                            </div>
                        <!--===end notification code --==-->

                      </div> 
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    function confirmMessage()
    {
      var retVal = confirm("Are you sure?");
      if( retVal == true ) {
        return true;
      }
      else {
        return false;
      }
    }
    
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    //if(confirm("Tem certeza de que deseja excluir este?")){
      var idd = $(this).attr('id');
      'notifications.id',
      $.ajax({
        url: "/dashboard/notifications",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          // jQuery('.success_show').show();
          $.notify({
                    message: 'Notificação excluída com sucesso!',
                    },{
                    type: 'success',
                    offset: 
                    {
                      x: 10,
                      y: 130
                    },
                    animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
                    },
                  });
          jQuery('.main_'+idd).remove();
        }            
      });      
    //}
  });
</script>
@stop
