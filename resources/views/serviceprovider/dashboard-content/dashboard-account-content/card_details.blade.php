@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
@if($spavailabilityInfo->isCompleted=='0') 
<div class="alert alert-danger alert-dismissible fade in profle_alert">
<strong>Atenção!</strong> Preencha os dias da semana e horários em que você está disponível. <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui</a>
</div>
@endif
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{url('/serviceprovider/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
              <li role="presentation"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="bookingmsg" role="tab"> Contatos & Mensagens</a></li>
              <li role="presentation" class="active"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="settings" role="tab">Sua conta</a></li>
            <!--  <li role="presentation"><a href="{{url('/serviceprovider/switch/role')}}" aria-controls="switch" role="tab">Mudar de função</a></li>-->
           </ul>
         </div>
       </div>
       @include('layouts.flash-message')
       <?php
       $profile_image = $user_info->profile_image;
       $profile_image = $user_info->profile_image;
       $user_role = $user_info->user_role;
       ?>
       <!-- Tab panes -->
       <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="account">
          <div class="container">
            <div class="accounttabs">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="profileinfo" role="tab" >Seus dados</a></li>
               <!--  <li role="presentation" class="active"><a href="{{url('/serviceprovider/card/details')}}" aria-controls="carddetail" role="tab" >Add Your card Detail<span>&nbsp;&nbsp;</span></a></li> -->
                <li role="presentation"><a href="{{url('/serviceprovider/notifications/details')}}" aria-controls="notifications-tabs" role="tab" >Notificações</a></li>
                <li role="presentation"><a href="{{url('/serviceprovider/closeaccount')}}" aria-controls="closeacc" role="tab" >Encerrar seu cadastro</a></li>
              </ul>
              <div class="tab-content profiletabss">
                <div role="tabpanel" class="tab-pane active" id="profileinfo">
                  <div class="acccompletion_tabs">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                   <!--  <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                        <div class="col-sm-12 form-group profileform_right">
                          <form name="complete_service" id="complete_service" class="complete_service" action="{{ url('/accountcomplete/service') }}" method="post">
                            csrf_field() 
                            <?php
                            $user_cateogry = $user_info->category;
                            foreach($category as $category_name){
                              $categrry_name = $category_name->cat_name;
                              ?>
                              <div class="col-sm-3 form-group">
                                <label class="col-sm-11"><?php echo ucfirst($category_name->cat_name); ?></label><div class="profileinput col-sm-1 nopadding"><input type="checkbox" name="cat_name[]" value="<?php echo $category_name->cat_name; ?>" <?php if (strpos($user_cateogry, $categrry_name) !== false) {
                                  echo 'checked';
                                } ?> data-trigger="focus"></div>
                              </div>
                              <?php
                            }
                            ?>
                            <div class="col-sm-12 nextbtn">
                              <input class="form-control" value="Save" type="submit">
                            </div>
                          </form>
                        </div>
                      </div> -->
                    </div>

                  </div>
                </div>
              </div>

              <div role="tabpanel" class="tab-pane subtabcontent active " id="carddetail">

                <div class="alert alert-success alert-block success_show">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Account Info Saved Successfully!</strong>
                </div>
                <div class="tabcontentbg">
                  <form class="carddetailform" id="carddetailform" method="post" action="#">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label class="col-sm-4">Credit/debit card number</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="cardnumber" name="card_num" placeholder="4704 - 1258 - 3698 - 1235" value="{{ $user_info->cardnumber }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4">Name on Card</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="cardname" name="paymentname" placeholder="" value="{{ $user_info->cardname }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4">Expiry Date</label>
                      <div class="col-sm-8">
                        <select class="selectmonth" name="exp_month">
                          <option value="">Select Month</option>
                          <?php
                          $exp_month = $user_info->exp_month;
                          for ($i = 1; $i <= 12; ++$i) {
                            ?>
                            <option value="<?php
                            echo $i;
                            ?>" <?php
                            if ($exp_month == $i) {
                              echo 'selected';
                            }
                            ?>><?php
                            echo $i;
                            ?></option>
                            <?php
                          }
                          ?>
                        </select>
                        <select class="selectyears" name="exp_year">
                          <option value="">Select Years</option>
                          <?php
                          $exp_year = $user_info->exp_year;
                          for ($i = 2018; $i <= 2035; ++$i) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php if ($exp_year == $i) {
                              echo 'selected';  } ?>><?php echo $i; ?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4">Security code (CVV/CVC)</label>
                        <div class="col-sm-8">
                          <input type="text" name="cvv" value="{{ $user_info->cvv }}" id="cvv" class="cvv">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-4"></label>
                        <div class="col-sm-8 addcardbtn">
                          <input type="submit" class="btn" value="Add Card">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<script type="text/javascript">
  function confirmMessage()
  {
    var retVal = confirm("Are you sure?");
    if( retVal == true ) {
      return true;
    }
    else {
      return false;
    }
  }
  var base_url = '<?php echo url('/');?>';
  $('.change_role').on('click',function(){
    if(confirm("Are you Sure You want to logout By clicking here You are logout form website shortly,you have to login again with changed role")){
     $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
     $('#switchdashboard').submit();
   }
   else{
    return false;
  }

});
  $('.status_submit').on('click', function(e) {
   e.preventDefault(); 
   var status = confirm("Are you sure?");
   if( status == true ) {

    var token = $('.book_token').val();
    var bookingId = $('.bookId').val();
    var status = $('.status_submit').val();
    var host = '{{ url('/') }}';
     // alert("#pending_"+bookingId);die;
     $.ajax({
       type: "POST",
       url: host+'/providerbooking/status',
       data: {'booking_id':bookingId,'_token':token,'approve_status':status},
       dataType: "json",
       success: function(response) {
         $(".statusAlerts").show();
         $(".statusAlerts").html(response.msg);
         $("#pending_"+bookingId).hide();
         if(response.status == '1'){
          $('.data_fill_confirm').focus();
          $(".data_fill_confirm").append(response.html);
        } else if(response.status == '2') {
          $('.data_fill_confirm').focus();
          $('.data_fill_confirm').append(response.html);
        }else {
          $(".data_fill_pending").focus();
          $(".data_fill_pending").append(response.html);
        }
        setTimeout(function () {
          $('.statusAlerts').hide();
        }, 2000);
      }
    });
   } else {
    return false;
  }

});

</script>
@stop
