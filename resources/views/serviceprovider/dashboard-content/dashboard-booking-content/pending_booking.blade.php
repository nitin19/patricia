@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<!-- @if($spavailabilityInfo->isCompleted=='0') 
<div class="alert alert-danger alert-dismissible fade in profle_alert">
<strong>Atenção!</strong> Preencha os dias da semana e horários em que você está disponível. <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui</a> </div>
@endif -->
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
             <li role="presentation"><a href="{{url('/serviceprovider/dashboard')}}" aria-controls="dashboard" role="tab" >Painel de controle</a></li>
             <li role="presentation" class="active"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="bookingmsg" role="tab">  Contatos & Mensagens</a></li>
             <li role="presentation"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="settings" role="tab">Sua conta</a></li>
            <!-- <li role="presentation"><a href="{{url('/serviceprovider/switch/role')}}" aria-controls="switch" role="tab">Mudar de função</a></li>-->
           </ul>
         </div>
       </div>
       @include('layouts.flash-message')
       <?php
       $profile_image = $user_info->profile_image;
       $profile_image = $user_info->profile_image;
       $user_role = $user_info->user_role;
       ?>
       <!-- Tab panes -->
       <div class="tab-content">

        <div role="tabpanel" class="tab-pane active codeRefer" id="bookingmsg">

          <div class="bookingmsg_sec">
            <div class="container">
              <div class="bookmsgsec">
                <div class="bookmsgtab" id="">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="{{url('/serviceprovider/booking/pending')}}" aria-controls="pending" role="tab" >Pendentes <span class="tkr-count">@if($booking_payment_pending_count)({{ $booking_payment_pending_count }})@endif</span></a></li>
                    <li role="presentation"  ><a href="{{url('/serviceprovider/bookings')}}" aria-controls="confirmed" role="tab">Aceitas <span class="tkr-count">@if($booking_payment_confirm_count)({{$booking_payment_confirm_count}})@endif</span></a></li>
                    <li role="presentation"><a href="{{url('/serviceprovider/booking/declined')}}" aria-controls="past" role="tab" >Negadas <span class="tkr-count">@if($booking_payment_past_count)({{ $booking_payment_past_count }})@endif</span></a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pending">
                      <div class="tablewrapper tablediv">
                       <!-- <div class="alert alert-info alert-dismissible   statusAlerts" style="display:none;">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       </div>  -->
                       @if($booking_payment_pending_count>0)
                       <table class="table-bordered table-striped">
                        <thead class="tableheader">
                          <tr class="headings">
                            <th class="column3">Nome</th>
                            <th class="column5">Mensagem</th>
                            <th class="column2">Categoria</th>
                            <th class="column2">Serviço</th>
                            <th class="column2">Data de início</th>
                            <!-- <th class="column2">Data final</th> -->
                            <th class="column4">Valor base</th>
                            <th class="column2">Tem interesse no serviço?</th>
                          </tr>
                        </thead>
                        <tbody class= "data_fill_pending">
                          @foreach($booking_payment_pending as $booking_payment_pending_info)
                          <?php
                          $bookingid = $booking_payment_pending_info->booking_id;
                          $payment_pendinguser_book_by = $booking_payment_pending_info->user_id;
                          $payment_pendingbooking_message = $booking_payment_pending_info->booking_message;
                          $payment_pendingbooking_amount = $booking_payment_pending_info->booking_amount;
                          $payment_pendingcategory = $booking_payment_pending_info->category;
                          $fmt = datefmt_create(
                            'pt-BR',
                            IntlDateFormatter::FULL,
                            IntlDateFormatter::FULL,
                            'Brazil/East',
                            IntlDateFormatter::GREGORIAN,
                            "dd/MMM/YYYY"  
                            );
                              //start time
                            $dt = new DateTime($booking_payment_pending_info->booking_start_time. "+1 days");
                            $d = $dt->format('d');
                            $m = $dt->format('m');
                            $y = $dt->format('Y');
                              //end time
                            // $edt = new DateTime($booking_payment_pending_info->booking_end_time. "+1 days");
                            // $ed = $edt->format('d');
                            // $em = $edt->format('m');
                            // $ey = $edt->format('Y');
                            $payment_pendingbooking_start_time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                            // $payment_pendingbooking_end_time = datefmt_format($fmt, mktime(0,0,0,$em,$ed,$ey));
                          
                          $payment_pendingmuser_book_to = $booking_payment_pending_info->profile_id;
                          $paymentuser_info = DB::table('users')
                          ->where('id', $payment_pendinguser_book_by)
                          ->first();

                          $paymentname = $paymentuser_info->name;
                          $paymentprofile_image = $paymentuser_info->profile_image;
                          if($paymentprofile_image==''){
                            $payment_pendinguserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                          }
                          else{
                            $payment_pendinguserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                          }
                          ?>
                          <tr class="tabledata confirmedtable ppending" id= "pending_{{ $booking_payment_pending_info->booking_id  }}">

                            <td class="column2 bkngaction bkngActionMob colum-2"> 
                               <form method="POST" id= "booking_status{{ $booking_payment_pending_info->booking_id }}" action="" style="display: inline;"> 
                                <input type="hidden" class="bookId" name="booking_id" value="{{ $booking_payment_pending_info->booking_id }}">
                                <input type="hidden" class="book_token" name="_token" value="{{ csrf_token() }}" required="required">
                                <input type="hidden" name="approve_status" value="1" class="status_approve">
                                <input type="hidden" name="denied_status" value="2" class="status_denied">

                                <button class="btn btn-success btn-sm status_submit_approve butpadd" name="approve_status" id= "{{ $booking_payment_pending_info->booking_id }}" type="submit" value="1" data-toggle="tooltip" data-placement="top" title="Aceitar"><i class="fa fa-check approv_btn"></i></button>
                                <button style="background-color: #ff3333 !important;border: 2px solid #ff3333 !important;" class="btn btn-danger btn-sm status_submit_denied butpadd butt" name="approve_status" id= "{{ $booking_payment_pending_info->booking_id }}"  type="submit" value="2" data-toggle="tooltip" data-placement="top" title="Negar"><i class="fa fa-close approv_btn"></i></button><br>
                               
                              </form>
                              
                              <button  class="btn btn-success btn-sm butpadd blackuserModal_{{ $booking_payment_pending_info->booking_id }}" data-toggle="modal" data-target="#blackuserModal_{{ $booking_payment_pending_info->booking_id }}" data-whatever="@mdoblk" data-bookingid="{{ $booking_payment_pending_info->booking_id }}"  ><i data-toggle="tooltip" data-placement="top" class="fa fa-bug" data-original-title="Solicitar bloqueio"></i></button>

                            </td>

                             <td class="username column3 colu-3" ><h4><a>
                              @if($paymentprofile_image=='')
                              <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                              @else
                              <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>"> 
                              @endif
                              <span data-toggle="modal" 
                                data-target="#takerModal_{{$booking_payment_pending_info->booking_id }}">{{ substr(ucfirst($paymentname),0,13) }} @if (strlen($paymentname)>='13')...@endif</span></a></h4></td>
                              <td class="userdesc column5" id="msgrow{{$bookingid}}"><p><?php if(strlen($payment_pendingbooking_message) > 100){ echo substr($payment_pendingbooking_message, 0, 40). '<span class="dots" id="dotmsg'.$bookingid.'">...</span><span class="readmore" id="moremsg'.$bookingid.'">'.substr($payment_pendingbooking_message, 100, strlen($payment_pendingbooking_message)).'</span><span class="btnmore">Veja mais</span><span class="btnless">Veja menos</span>';}else{ echo $payment_pendingbooking_message;} ?></p>
                              </td>
                              <td class="column2">{{ $payment_pendingcategory }}</td>
                              <td class="column2">{{ $booking_payment_pending_info->sub_category }}</td>
                              <td class="column2">{{ $payment_pendingbooking_start_time }}</td>
                           
                              <td class="column4"><span class="hifen">#</span>{{ $payment_pendingbooking_amount }}</td>
 
                              <td class="column2 bkngaction bkngActionDsk"> 
                               <form method="POST" id= "booking_status{{ $booking_payment_pending_info->booking_id }}" action="" style="display: inline;"> 
                                <input type="hidden" class="bookId" name="booking_id" value="{{ $booking_payment_pending_info->booking_id }}">
                                <input type="hidden" class="book_token" name="_token" value="{{ csrf_token() }}" required="required">
                                <input type="hidden" name="approve_status" value="1" class="status_approve">
                                <input type="hidden" name="denied_status" value="2" class="status_denied">

                                <button class="btn btn-success btn-sm status_submit_approve butpadd" name="approve_status" id= "{{ $booking_payment_pending_info->booking_id }}" type="submit" value="1" data-toggle="tooltip" data-placement="top" title="Aceitar"><i class="fa fa-check approv_btn"></i></button>
                                <button style="background-color: #ff3333 !important;border: 2px solid #ff3333 !important;" class="btn btn-danger btn-sm status_submit_denied butpadd butt" name="approve_status" id= "{{ $booking_payment_pending_info->booking_id }}"  type="submit" value="2" data-toggle="tooltip" data-placement="top" title="Negar"><i class="fa fa-close approv_btn"></i></button><br>
                               
                              </form>
                              <!--  <button  style="display: inline;margin-right: 2px;margin-top: 4px;" class="btn btn-primary btn-sm butpadd" data-toggle="modal" 
                                data-target="#takerModal_{{$booking_payment_pending_info->booking_id }}"  >
                                <i class="fa fa-eye approv_btn" data-toggle="tooltip" data-placement="top" title="Dados do contratante" ></i></button> -->
                              <button  class="btn btn-success btn-sm butpadd blackuserModal_{{ $booking_payment_pending_info->booking_id }}" data-toggle="modal" data-target="#blackuserModal_{{ $booking_payment_pending_info->booking_id }}" data-whatever="@mdoblk" data-bookingid="{{ $booking_payment_pending_info->booking_id }}"  ><i data-toggle="tooltip" data-placement="top" class="fa fa-bug" data-original-title="Solicitar bloqueio"></i></button>

                            </td>
                          </tr>   
                           <!-- Modal -->
                                <div class="modal fade" id="takerModal_{{$booking_payment_pending_info->booking_id }}" tabindex="-1" role="dialog" aria-labelledby="takerModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="takerModalLabel">Dados do contratante</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                       @if(!empty(Auth::user()))
                                       <?php $userblock = DB::table('block_providers')->where('taker_id','=',$payment_pendinguser_book_by)->where('provider_id','=',Auth::user()->id)->where('block_status','=','1')->first();
                                       // $rating_avg = $paymentuser_info->profile_rating;
                                        $rating_avg = DB::table('feedback')
                                        ->where('user_id', $payment_pendinguser_book_by)
                                        ->where('approve_status', 1)
                                        ->where('role', 'provider')
                                        ->avg('rating');
                                      if(empty($userblock)) { 
                                          
                                        ?>
                                      <div class="modal-body">
                                        <ul class="model-profile">
                                          @if($paymentprofile_image=='')
                                          <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="profimg">
                                          @else
                                          <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>" class="profimg">
                                          @endif
                                          <li>Nome: <span>{{ ucfirst($paymentuser_info->name) }}</span></li>
                                          <li>E-mail: <span>{{ $paymentuser_info->email }}</span></li>
                                          <li>Celular: <span>{{ $paymentuser_info->phone }}</span></li>
                                          <li>Bairro: <span>{{ ucfirst($paymentuser_info->city) }}</span></li>
                                          <li>Feedback:<span> <?php
                                          $x=0; if($rating_avg != ''){
                                            $number = number_format($rating_avg,1);
                                            $integer_part = floor($number);
                                            $fraction_part = $number-$integer_part;
                                            
                                            for($x=1;$x<=$number;$x++) {?>
                                              <i class="fa fa-star" style="color:#ffcd00;"></i>
                                            <?php }
                                            // echo '(stars)';
                                            if (strpos($number,'.')) {
                                              if($fraction_part != 0) {
                                                ?>
                                                <i class="fa fa-star-half-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                                <?php 
                                                $x++;
                                              }else { ?>
                                                 <i class="fa fa-star-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                              <?php }
                                            } } else {
                                            while($x<5){ ?>
                                             <i class="fa fa-star-o" aria-hidden="true" style="color:#ffcd00;"></i>
                                             
                                             <?php
                                             $x++;
                                           }
                                          
                                         }
                                         ?>
                                           </span></li>
                                           <li style="display: flex;">Documents:<span>
                                           <?php  $user_documents = DB::table('user_documents')->where('user_id', $paymentuser_info->id)->where('is_active', '1')->where('document_status', '1')->where('is_deleted', '0')->get();?>
                                           @if($user_documents)
                                       
                                           @foreach($user_documents as $documents)
                                          
                                           <img src="{{ url('/public') }}/images/user_document/{{ $documents->document }}" class="img-responsive" height: 20px; style="width: 20%!important;float: left;">
                                         
                                           @endforeach
                                         
                                           @else
                                           No document uploaded.
                                           @endif
                                            </span></li>
                                         
                                        </ul>
                                     
                                      </div>
                                      <?php } else {  ?>
                                      <div class="modal-body">
                                        <h5>You not able to view profile.Because you blocked by <span style="color:blue;">{{ ucfirst($paymentuser_info->name) }}</span></h5>
                                      </div>
                                    <?php } ?>
                                    @endif
                                    </div>
                                  </div>
                                </div>
                                <!-- black user popup -->
                              <div class="modalsection">

                                <div class="modal fade blacknow_popup" id="blackuserModal_{{ $booking_payment_pending_info->booking_id }}" tabindex="-1" role="dialog" aria-labelledby="blackuserModalModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <h2 class="text-center" style="color: #fdfdfd;background-color: #1b9bd8;border-color: #1b9bd8;padding:20px;font-size: 18px; margin:0px;">Ajude-nos a entender o motivo de solicitar o bloqueio desse contratante no nosso site: <a href="#" class="close close_about" data-dismiss="modal" aria-label="close">&times;</a></h2>
                                        
                                        <div class="modal-body">
                                          <div class="alert alert-success blackSuccess" id="blackSuccess" style="display:none;"></div>
                                            <div class="alert alert-danger blackError" id="blackError"style="display:none;"></div>
                                              <form id="blackForm">
                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <div class="form-group datesec">
                                                      <h4>Selecione o motivo:</h4>
                                                       <label class="radio_btn_label">Contratante não responde nos contatos disponibilizados
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante não responde nos contatos disponibilizados">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante não efetuou os pagamentos conforme combinado 
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante não efetuou os pagamentos conforme combinado ">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante inviabiliza a finalização dos serviços conforme combinado
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante inviabiliza a finalização dos serviços conforme combinado">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <label class="radio_btn_label">Contratante com comportamento inadequado
                                                        <input type="radio" checked="checked" name="black_reason" value="Contratante com comportamento inadequado">
                                                        <span class="checkmark"></span>
                                                      </label>
                                                      <input type="hidden" id="from_id" name="from_id" value="{{ Auth::user()->id }}">
                                                      <input type="hidden" id="to_id" name="to_id" value="{{ $paymentuser_info->id }}">
                                                      <input type="hidden" id="to_name" name="to_name" value="{{ ucfirst($paymentuser_info->name) }}">
                                                      <input type="hidden" id="from_name" name="from_name" value="{{ ucfirst(Auth::user()->name) }}">
                                                      <input type="hidden" id="user_type" name="user_type" value="Provider">
                                                       <input type="hidden"  name="_token" value="{{ csrf_token() }}" required="required">
                                                    </div>
                                                  </div>
                                                </div>       
                                                <div class="blackBtn">
                                                  <input type="submit" class="btn btn-primary blackuserBtn" id="blackBtn" value="Enviar">
                                                </div>
                                              </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- end black users -->
                          @endforeach

                        </tbody>
                      </table>
                      <div align="center">{{ $booking_payment_pending->links() }}</div>
                      @else
                      <tr>
                        <h2>Sem propostas pendentes para você no momento.</h2></tr>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<script type="text/javascript">
  
  var base_url = '<?php echo url('/');?>';
  $('.change_role').on('click',function(){
    if(confirm("Are you Sure You want to logout By clicking here You are logout form website shortly,you have to login again with changed role")){
     $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
     $('#switchdashboard').submit();
   }
   else{
    return false;
  }

});
  $('.status_submit_approve').on('click', function(e) {
   e.preventDefault(); 
   var bookingId = $(this).closest(".status_submit_approve").attr('id');
   //var status = confirm("Are you sure?");
  // if( status == true ) {

    var token = $('.book_token').val();
    //var bookingId = $('.bookId').attr('value');
    var status = $('.status_submit').val();
    var host = '{{ url('/') }}';
    var approve_status_val = $('.status_approve').val();
    var status_denied = $('.status_denied').val();
    // alert(status_denied);
    $.ajax({
     type: "POST",
     url: host+'/serviceprovider/providerbooking/status/approved',
     data: {'booking_id':bookingId,'_token':token,'approve_status':status,'approve_status_val':approve_status_val,'status_denied':status_denied},
     dataType: "json",
     success: function(response) {
      $.notify({
          message: 'Contratante informado que você tem interesse no serviço.' 
          },{
          type: 'success',
          offset: 
          {
            x: 10,
            y: 130
          },
          animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
          },
        });
       // $(".statusAlerts").show();
       // $(".statusAlerts").html(response.msg);
       // $("#pending_"+bookingId).hide();
       if(response.status == '1'){
        
        //window.location.replace(baseurl+"/serviceprovider/bookings");
        // $('.data_fill_confirm').focus();
        // $(".data_fill_confirm").append(response.html);
        setTimeout(function () {
            window.location.href = '{{ url('/').'/serviceprovider/bookings' }}';
          $('.statusAlerts').hide();
        }, 4000);
       }else {
        $(".data_fill_pending").focus();
        $(".data_fill_pending").append(response.html);
          setTimeout(function () {
          $('.statusAlerts').hide();
        }, 4000);
      }
      
    }
  });
  //} else {
    //return false;
  //}

});

  $('.status_submit_denied').on('click', function(e) {
   e.preventDefault(); 
   var bookingId = $(this).closest(".status_submit_denied").attr('id');
   //var status = confirm("Are you sure?");
   //if( status == true ) {
    var token = $('.book_token').val();
    //var bookingId = $('.bookId').val();
    //alert(bookingId);
    var status = $('.status_submit').val();
    var host = '{{ url('/') }}';
    var approve_status_val = $('.status_approve').val();
    var status_denied = $('.status_denied').val();
    // alert(status_denied);
    $.ajax({
     type: "POST",
     url: host+'/serviceprovider/providerbooking/status/denied',
     data: {'booking_id':bookingId,'_token':token,'approve_status':status,'approve_status_val':approve_status_val,'status_denied':status_denied},
     dataType: "json",
     success: function(response) {
      $.notify({
          message: 'Contratante informado que você não tem interesse no serviço.' 
          },{
          type: 'success',
          offset: 
          {
            x: 10,
            y: 130
          },
          animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
          },
        });

       // $(".statusAlerts").show();
       // $(".statusAlerts").html(response.msg);
       // $("#pending_"+bookingId).hide();
      if(response.status == '2') {
        var baseurl = '{{ url('/') }}';
        setTimeout(function () {
        window.location.replace(baseurl+"/serviceprovider/booking/declined");
      }, 3000);
        // $('.data_fill_denied').focus();
        // $('.data_fill_denied').append(response.html);
      }else {
        $(".data_fill_pending").focus();
        $(".data_fill_pending").append(response.html);
      }
      setTimeout(function () {
        $('.statusAlerts').hide();
      }, 4000);
    }
  });
  //} else {
    //return false;
  //}

});
// ajax black list
    $('.blackuserBtn').click(function(e) {
      e.preventDefault();
      var black_reason = $('#black_reason').val();
      var Url  = '<?php echo url('/');?>';
      if(black_reason=='') {
       $('#black_reason').addClass('vlderror');
       return false;
       }
      $.ajax({
           type:'POST',
           url:Url+'/addblackuser',
           data:$('#blackForm').serialize(),
           success:function(data){
            data=jQuery.parseJSON(data);
            if(data.msgcode == 1) {
              
              $("#blackForm")[0].reset();
              $('.blackSuccess').html(data.message);
              $('.blackSuccess').css('opacity','1');
              $('.blackSuccess').show();
              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');
              
              window.setTimeout(function () {
                $(".blackSuccess").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  //window.location.href="<?php echo url('/');?>/profile/"+profile_id;
                });
              }, 3000);

            } else {

              $('.blackError').html(data.message);
              $('.blackError').css('opacity','1');
              $('.blackError').show();

              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');

              window.setTimeout(function () {
                $(".blackError").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  //$('#blackuserModal').modal('hide');
                });
              }, 2000);

            }
          }
        });
       });

    //end ajax black list

</script>
<script type="text/javascript">
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    //if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
      'notifications.id',
      $.ajax({
        url: "/dashboard/notifications",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          jQuery('.success_show').show();
          jQuery('.main_'+idd).remove();
        }            
      });      
    //}
  });
</script>
<script>
$(document).ready(function () {

  $(".btnmore").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
  });

  $(".btnless").click(function(){
      var tdid = $(this).closest('td').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
  });

});
</script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<style type="text/css">
  .readmore, .btnless{
    display: none;
  }
  button.btn.btn-success.btn-sm.status_submit_approve {
    background-color: #1B9BD8 !important;
    border: 1px solid #1B9BD8 !important;
}
</style>
@stop
