@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
<style>
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
.edit_price input {
    background-color: transparent;
    margin-top: -14px;
    cursor: text !important;
}
@media screen and (max-width:320px) {
.edit_price input {
    background-color: transparent;
    margin-top: 0px;
    text-align: left !important;
    cursor: text !important;
}
}
</style>
@section('content')


<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
    foreach($currentuser_ip as $currentuser_info) {
      $wish_profile_id = $currentuser_info->wish_profile_id;
      $currentuser_count = DB::table('wish_list')
      ->where('user_id', $login_userid)
      ->where('wish_profile_id', $wish_profile_id)
      ->count();
      if($currentuser_count==0 || $currentuser_count==''){
        $insert_whish = DB::table('wish_list')->insert(
          ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
        );
      }
    }
  }
  ?> 
  @if(session()->has('message'))
  <script>
    var msg = "{{ Session::get('message') }}";
  $.notify({
      message: msg ,
      },{
      type: 'info',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
  </script>
  <!-- <div class="alert alert-success profle_alert">
    {{ session()->get('message') }}
  </div> -->
  @endif

  <!--<div id="dialog-message" title="My Dialog Alternative">-->
    <!--<p style='color:red'> Hello world </p>-->
    <section class="maincontent searchbarbg">

      

      <div class="col-sm-12 pagecontent whitebgdiv nopadding">
        <div class="container">
          <div class="acccompletionsec profile-accountcomplete">
            @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
            <script>
              $.notify({
                  message:"Complete seu cadastro abaixo." ,
                  },{
                  type: 'danger',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                });
            </script>
            @endif

           
            <div class="image123_alert profle_alert">

            </div>
            <!-- <h1>Account Completion</h1>-->
            <div class="acccompletion_tabs">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"  class="">
                  <a href="{{url('/serviceprovider/profile')}}" role="tab">Meus dados</a></li>
                  <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
                  <li role="presentation"  class="@if (!Session::has('success')) active @endif" ><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Serviços & Preços</a></li>
                  <li role="presentation"  class="@if (!Session::has('success'))  @endif" ><a href="{{ url('/serviceprovider/spavailablty')}}" aria-controls="availibility" role="tab">Agenda</a></li>
                </ul>

                @include('layouts.flash-message')
 
                <!-- Tab panes -->
                <div class="tab-content">
                  @if($user_role=='Provider')
                  <div role="tabpanel" class="tab-pane active" id="services" >
                    <div class="col-sm-12 form-group ">
                      <form name="complete_service" action="{{ url('/updatequeryprice')}}" id="complete_service" class="complete_service"  method="post">
                        <div class="ser_tak_profile">
                        {{ csrf_field() }}
                        <div class="col-md-12">  
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label>Selecione uma categoria de serviço</label>
                            <select name="main_categroy_id" id="main_category" class="form-control" required>
                              <option value="" selected disabled="disabled">Selecione um serviço</option>
                              @foreach($main_category as $main_cat_id=>$main_cat_name)
                              <option value="{{$main_cat_id}}" @if($main_cat_id== $user_info->cat_id) selected @endif>{{$main_cat_name}}</option>
                              @endforeach
                            </select>
                          </div> 
                          <div class="form-group col-md-6">
                            <input type="hidden" class="sub_cat_check_address" value ="{{ url('/check_subcategory/') }}">
                            <label>Escolha a subcategoria aqui</label>
                            <select name="sub_categroy_id"  class="form-control" id="sub_category" required>
                              <?php if($user_info->cat_id!=''){
                                $subCategories = DB::table('category')->where('main_cat_id', '=', $user_info->cat_id)->where('is_active', '=', 1)->where('is_deleted', '=', 0)->get();
                                if(count($subCategories) > 0 ) {
                                  foreach($subCategories as $subCat) {
                                ?>
                              <option value="{{$subCat->cat_id}}" @if($subCat->cat_id == $user_info->sub_id) selected @endif>{{$subCat->cat_name}}</option>
                              <?php 
                                }
                              }
                            }  
                            ?>
                            </select>
                          </div>
                        </div>
                          <div id="subcat_queries">
                            <?php
                            $provider_prices_ids = array();
                            if(count($provider_price_lists) > 0){
                              foreach($provider_price_lists as $provider_price_list){
                                array_push($provider_prices_ids, $provider_price_list->query_id);
                              }
                            }

                            if(count($querieslists) > 0){
                              $queryids = array();
                              ?>
                              <div class="form-group col-md-12"><h3>Preços</h3></div>

                              <div class="row">
                              <div class="col-sm-12">
                                  <?php foreach($querieslists as $querieslist){
                                    if (in_array($querieslist->id, $provider_prices_ids)){
                                      $provide_query_price = DB::table('provider_price_list')->where('query_id', $querieslist->id)->where('user_id', Auth::user()->id)->first();
                                      $amount = number_format($provide_query_price->amount,2,",",".");
                                    } else {
                                      $amount = '';
                                    }
                                    ?>
                              <table class="table pricing_table edit_price">
                                <tbody>  
                                  <tr>
                                    <th>
                                      {{$querieslist->queries}}
                                    </th>
                                    <th>&#82;&#36;
                                      <input type="text" value="{{$amount}}" name="pricequery_<?php echo $querieslist->id;?>" id="query_amount" data-type="currency" >
                              
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                                    <?php
                                    array_push($queryids, $querieslist->id);
                                  }?>
                                  <input type="hidden" name="queryids" value="<?php echo implode(',', $queryids);?>">
                                  <div class="col-sm-12 nextbtn">
                                    &nbsp;
                                    <input class="form-control save Salvar" value="Salvar" type="submit">
                                    &nbsp;
                                    <a href="{{ url('/serviceprovider/profile')}}" class="btn btn-default pull-right cancelcancel">Cancelar</a> 
                                    &nbsp;
                                  </div>
                                </div>
                                </div>
                            <?php } ?>
                          </div>

                          <div id="subcat_queries2" ></div>

                        </div>
                      </div>
                      </form>
                    </div>
                  </div>
                  @endif

                </div>
              </div>
            </form>
          </div>

        </div>
      </div>
    </section>

  </main>
  <script type="text/javascript">
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
  </script>
  <script>

    window.setTimeout(function() {
      $(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).hide();
      });
    }, 4000);

    $(document).ready(function(){
      var base_url = '<?php echo url('/'); ?>';

      $('#main_category').on('change',function(){
        var url = $('.sub_cat_check_address').val();
        var data = $('#complete_service').serialize();
        $.ajax({
          type:"POST",
          dataType:'JSON',
          url:url,
          data:data,
          success:function(data){
            $('#sub_category').empty();
            if(data != ''){
              $('#sub_category').html('<option value="" selected disabled>Selecione um serviço</option>');
              $.each(data,function(k,v){
            // console.log(k);
            var option = $("<option />");
            option.attr("value", k).text(v);
            $('#sub_category').append(option);
          });
            }
            else{
              var option = $("<option />");
              option.attr("value", '0').text('Desculpe dados não encontrados');
              $('#sub_category').append(option);
            }

          }
        })
      });

      $('#sub_category').on('change',function(){
        var user_cat_id = '{{Auth::user()->cat_id}}';
        var user_sub_id = '{{Auth::user()->sub_id}}';
        var sub_category = $(this).val();
        var main_category = $('#main_category').val();
        if(user_cat_id==main_category && user_sub_id==sub_category){
          var action = '{{url('/updatequeryprice')}}';
          $('#complete_service').attr('action', action);
          $('#subcat_queries2').hide();
          $('#subcat_queries').show();
        } else {
          var action = '{{url('/savequeryprice')}}';
          $('#complete_service').attr('action', action);
          $('#subcat_queries').hide();
          $('#subcat_queries2').show();
        }
        $.ajax({
          type:"get",
          url:"{{url('/subcategory_queries')}}",
          data:{'sub_category': sub_category, 'main_category': main_category},
          success:function(data){
            $('#subcat_queries2').html(data);
          }
        })
      });
    });
  </script>
  @stop
