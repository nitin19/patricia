@extends('layouts.default-header')
@section('title', 'Chat Box')
@section('content')
<?php 
$current_userId = Auth::id();

if($recentchat){
  if($recentchat->receiver_id!=$current_userId){
      $receiverid = $receiver_id;
  } else {
      $receiverid = $recentchat->sender_id;
  }
} else {
  $receiverid = '';
}
?>
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/emojis/css/emoji.css">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container">
    <!--chat-box-div-->
    <div class="row chatback">
        <div class="col-md-12">
            <div class="col-sm-4">
                <div class="main_side_bar_sec">
                    <div class="side_header_section">
                        <div class="back_side_bar">
                            @if(Auth::user()->profile_image!='')
                                <img id="profile-img" src="{{url('/public')}}/images/profileimage/{{Auth::user()->profile_image}}" class="online profilpic img-circle" alt="" draggable="false">
                            @else
                                <img id="profile-img" src="{{url('/public')}}//images/profileimage/1542707849.images.png" class="online profilpic img-circle" alt="">
                            @endif
                        </div>
                        <div class="name">{{  Auth::user()->name }}</div>
                    </div>
                    <div class="sidebar_friend_listing">
                        <ul class="friend_listing_sec">
                            <?php if(count($get_friend_list) > 0){ 
                            $ii = 0;
                            ?>
                            @foreach($get_friend_list as $get_friend_lists)
                            <?php 
                             $get_friend_last_msg = DB::table('chathistory')
                                        ->where('sender_id', $get_friend_lists->receiver_id)
                                        ->limit(1)
                                        ->orderBy('id','desc')
                                        ->get()->toArray();
                             $time = $get_friend_last_msg[0]->updated_at;
                             $newtime = date("H:i",strtotime($time));
                             /*$get_unread_message = DB::table('chathistory')
                                ->where('receiver_id','=', Auth::user()->id)
                                ->where('is_read', '=', '0')
                                ->get()->toArray();*/
                             $get_unread_message = DB::table('chathistory')
                                ->where('receiver_id', '=', Auth::user()->id)
                                ->where('sender_id', '=', $get_friend_lists->receiver_id)
                                ->where('is_read', '=', 0)
                                ->where('deleted', '=', 0)
                                ->get()->toArray();
                            ?>
                            <a href="{{url('/')}}/userchat/{{$get_friend_lists->receiver_id}}">
                                <li class="single_friend_list img_cont" id="side<?php echo $get_friend_lists->id;?>" data-user="<?php echo $get_friend_lists->id;?>" data-recentusername="{{$get_friend_lists->name}}">
                                    @if($get_friend_lists->profile_image!='')
                                        <img id="profile-img" src="{{url('/public')}}/images/profileimage/{{$get_friend_lists->profile_image}}" class="online profilpic img-circle user_img" alt="" draggable="false">
                                        <?php if(Cache::has('user-is-online-' .$get_friend_lists->receiver_id)){ ?>
                                            <span class="online_icon"></span>
                                        <?php } else { ?>
                                            <span></span>
                                        <?php } ?>
                                        <span class="last_message_time">{{$newtime}}</span>
                                    @else
                                        <img id="profile-img" src="{{url('/public')}}//images/profileimage/1542707849.images.png" class="online profilpic img-circle" alt="">
                                        <?php if(Cache::has('user-is-online-' .$get_friend_lists->receiver_id)){ ?>
                                            <span class="online_icon"></span>
                                        <?php } else { ?>
                                            <span></span>
                                        <?php } ?>
                                        <span class="last_message_time">{{$newtime}}</span>
                                    @endif
                                    <span class="friend_name">{{  $get_friend_lists->name }}</span>
                                    <span class="last_msg">{{$get_friend_last_msg[0]->message}}</span>
                                    <span class="unreadmsgcount" id="countunread{{$get_friend_lists->id}}">
                                            @if(count($get_unread_message) > 0)<span class="badge badge-secondary unreadcount">
                                                {{count($get_unread_message)}}
                                            </span>
                                            @endif
                                        </p>
                                    <input type="hidden" name="sidebar_receiver_id" id="sidebar_receiver_id" value="{{$get_friend_lists->receiver_id}}">
                                </li>
                            </a>
                            <?php $ii ++; ?>
                            @endforeach
                            <?php } else { ?>
                                <p class="no_record_found_text">No User found</p>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="menu">
                    <div class="col-md-12">
                        <div class="col-sm-8">
                            <div class="back">
                                <?php
                                    $receiverdata = DB::table('users')->where('id','=', $receiver_id)->where('is_active','=','1')->where('is_deleted','=','0')->first();
                                ?>
                                @if($receiverdata->profile_image!='')
                                    <img id="profile-img" src="{{url('/public')}}/images/profileimage/{{$receiverdata->profile_image}}" class="online profilpic img-circle" alt="" draggable="false">
                                @else
                                    <img id="profile-img" src="{{url('/public')}}//images/profileimage/1542707849.images.png" class="online" alt="">
                                @endif
                            </div>
                            <div class="name">{{  $receiverdata->name }}</div>
                            <div class="last"><span class="user_status_icon"></span></div>
                        </div>
                        <!-- <div class="col-sm-4">
                            <div class="receiver_right_data">
                                <p class="device_name">{{$receiverdata->login_device}}</p>
                                <p class="browser_name">{{$receiverdata->login_browser}}</p>
                                <p class="last_active">{{$receiverdata->last_activity}}</p>
                            </div>
                        </div> -->
                    </div>
                </div>
                <?php
                    $frienddetailQuery = DB::table('users');
                      if($current_userId==Auth::user()->id){
                        $frienddetailQuery->where('id', '=', $receiver_id);
                      } else {
                        $frienddetailQuery->where('id', '=', Auth::user()->id);
                      }
                    $getfriendInfo = $frienddetailQuery->first();

                ?>
                <ol class="tab-pane chat active" id="user_<?php echo $getfriendInfo->id;?>" userid="<?php echo $getfriendInfo->id;?>">
                </ol>
                <div id="typing_on"></div>
                <div class="panel-footer">
                    <form name="sendmessage" id="sendmessage">
                        <div class="input-group">
                            <input type="hidden" name="friend_request_id" id="friend_request_id" value=""
                                class="friend_request_id">
                            <input type="hidden" name="sender_id" id="sender_id" value="<?php echo $current_userid;?>">
                            <input type="hidden" name="receiver_id" id="receiver_id" value="<?php echo $receiverid;?>">
                            <input type="hidden" name="current_user_name" id="current_user_name" value="{{Auth::user()->name}}">
                            <input type="hidden" name="is_read" id="is_read" value="">

                            <div class="msg-box">
                                <input id="message" name="message" type="text" class="form-control input-sm"
                                    placeholder="Escreva sua mensagem aqui..." value="" data-emojiable="true"/>
                                <input type="file" id="attachments" name="attachments[]" style="display:none;" multiple>
                                <i class="attachmenticon fa fa-paperclip" id="countfile"></i>
                                    
                            </div>
                            <div class="send-button">
                                <span class="input-group-btn">
                                    <button class="btn btn-warning sendmsg pull-right" id="btn-chat" type="submit">
                                            Enviar</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Chat-box-end-->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ url('/public') }}/emojis/js/config.js"></script>
<script src="{{ url('/public') }}/emojis/js/util.js"></script>
<script src="{{ url('/public') }}/emojis/js/jquery.emojiarea.js"></script>
<script src="{{ url('/public') }}/emojis/js/emoji-picker.js"></script>

<script type="text/javascript">
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);
$(document).ready(function() {
    
    $('.attachmenticon').click(function() {
        $('#attachments').trigger('click');
    });

    $('#attachments').change(function() {
        if (this.files.length > 0) {
            $('.attachmenticon').html(this.files.length + ' files');
        } else {
            $('.attachmenticon').html('');
        }
    });

    $('#searchcontact').keyup(function() {
        var search = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'userchatsearch',
            data: {
                search: search
            },
            success: function(resp) {
                $('.sidebar_friend_listing').html(resp);
            }
        });
    });

    $('#searchmsgs').keyup(function() {
        var search = $(this).val();
        var senderid = $("#sender_id").val();
        var receiverid = $('.chat.active').attr('userid');
        var activediv = $('.chat.active').attr('id');
        $.ajax({
            type: 'GET',
            url: 'adminchatmsgsearch',
            data: {
                search: search,
                receiverid: receiverid,
                senderid: senderid
            },
            success: function(resp) {
                $('#' + activediv).html(resp);
            }
        });
    });

    $('.chat.active.tab-pane').on('scroll', function() {
        var userid = $(this).attr('userid');
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var sender_id = $("#sender_id").val();
            var receiver_id = $("#receiver_id").val();
            var base_url = "{{url('/')}}";
            $.ajax({
                type: 'GET',
                url: base_url+'/updateunreaduser',
                data: {
                    sender_id: sender_id,
                    receiver_id: receiver_id
                },
                success: function(resp) {
                    $('#side'+userid).find('.unreadcount').hide();
                }
            });
        }
    });
    $('body').on('click', '.single_friend_list', function() {
        $('#searchmsgs').val('');
        var activepane = $('.chat.active.tab-pane').attr('id');
        $('#sendmessage').show();
        $('#receiver_id').val($(this).attr('data-user'));
        $('#friend_request_id').val($(this).attr('data-friendrequestid'));
        $('.recent_user_name').html($(this).attr('data-recentusername'));

        fetchRecords();
        var receiver_id = $('.chat.active.tab-pane').attr('userid');;
        var sender_id = '{{Auth::user()->id}}';
        var base_url = "{{url('/')}}";
        $.ajax({
            type: 'GET',
            url: base_url+'/updateunreaduser',
            data: {
                sender_id: sender_id,
                receiver_id: receiver_id
            },
            success: function(resp) {
                $('#side'+receiver_id).find('.unreadcount').hide();
                $('#msgid').hide();
            }
        });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*-----------------------------------------------*/
 
    $("#sendmessage").submit(function(e) {
        var activepane = $('.chat.active.tab-pane').attr('id');
        e.preventDefault(); //prevent default action 
        proceed = true;
        var allowed_file_types = ['image/png', 'image/gif', 'image/jpeg', 'image/jpg',
            'application/pdf', 'application/vnd.ms-excel', 'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip',
            'video/3gpp'
        ]; 
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            if (this.elements['attachments[]'].files.length > 5) {
                alert("Can not select more than 5 file(s)");
                proceed = false;
            }
            $(this.elements['attachments[]'].files).each(function(i, ifile) {
                if (ifile.value !== "") {
                    if (allowed_file_types.indexOf(ifile.type) === -
                        1) { 
                        alert(ifile.name + " is not allowed!");
                        proceed = false;
                        $('.attachmenticon').html('');
                        $('#attachments').val('');
                    }
                }
            });
            if (this.elements['attachments[]'].files.length < 1 && $('#message').val() == '') {
                proceed = false;
            }
        }
        var form_data = new FormData(this);
        if (proceed) {
            var base_url = "{{url('/')}}";
            $.ajax({
                url: base_url+'/usersajaxRequest',
                type: 'POST',
                data: form_data,
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false
            }).done(function(data) {
                if (data.statusCode == 1) {
                    $("#message").val('');
                    $(".emoji-wysiwyg-editor").html('');
                    $('.attachmenticon').html('');
                    $('#attachments').val('');
                    scrollToBottom(activepane);
                } else {
                    return false;
                }
            });
        }
    });
    fetchRecords();
    user_check_status();
    setInterval(function() {
        var msgsrch = $('#searchmsgs').val();
        if (msgsrch == '' || msgsrch == undefined) {
            fetchRecords();
            user_check_status();
        }
        refreshfriendlist();
    }, 2000);
});

function fetchRecords() {
    var senderid = $("#sender_id").val();
    var receiverid = $('.chat.active').attr('userid');
    var activediv = $('.chat.active').attr('id');
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/getusersChat/' + senderid + '/' + receiverid,
        type: 'GET',
        dataType: 'html',
        success: function(response) {
            $('#' + activediv).html(response);
        }
    });
}

function refreshfriendlist() {
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/ajaxfriendlistRefresh',
        type: 'GET',
        success: function(response) {
            $('.sidebar_friend_listing ul').html(response);
        }
    });
}

function user_check_status() {
    var receiverid = $('.chat.active').attr('userid');
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/userstatuscheck/' + receiverid,
        type: 'GET',
        success: function(response) {
            if(response== 'online'){
                $('.user_status_icon').html('<span class="text-success user_online">conectada</span>');
               
            } else {
                $('.user_status_icon').html('<span class="text-success user_offline">desconectada</span>');
                
            }
        }
    });
}

function fetchfriendlist() {
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/ajaxfriendlist',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            var unreadchats = response.unreadchats;
            for (i in unreadchats) {
                $('#countunread' + unreadchats[i].sender_id).html(unreadchats[i].name +
                    '<span class="unreadcount">' + unreadchats[i].chat + '</span>');
            }
        }
    });
}
//===========================//
function myKeyPressFunction(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        $('#sendmessage').submit();
    }
}
//===========================//
function scrollToBottom(id) {
    var div = document.getElementById(id);
    setTimeout(function() {
        div.scrollTop = div.scrollHeight - div.clientHeight;
    }, 800);
}

function search() {
    if (event.keyCode == 13) {
        alert("should get the innerHTML or text value here");
    }
}
/*******typing section*********/
var message = $('#message');
var username = $('#current_user_name').val();
var typingStatus = $('#typing_on');
var lastTypedTime = new Date(0); 
var typingDelayMillis = 5000;
function refreshTypingStatus() {
    if (!message.is(':focus') || message.val() == '' || new Date().getTime() - lastTypedTime.getTime() > typingDelayMillis) {
        typingStatus.html('');
    } else {
        typingStatus.html('is typing...');
    }
}
function updateLastTypedTime() {
    lastTypedTime = new Date();
}
setInterval(refreshTypingStatus, 100);
message.keypress(updateLastTypedTime);
message.blur(refreshTypingStatus);
</script>
<script>
      $(function() {
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '{{ url("/public") }}/emojis/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();
      });
    </script>

<style type="text/css">
/********Chat-box-css**********/
.chatback {
    margin: 30px 0px;
   /* background-color: #e6eaea;
    box-shadow: 0px 0px 20px #ccc;*/
}

.chat {
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li {
    margin-bottom: 10px;
    padding: 6px 20px 4px 5px;
}

.left.clearfix {
    background-color: #435f7a;
    border-radius: 4px;
    color: #fff;
    width: 50%;
}

.right.clearfix {
    background-color: #eee6;
    border-radius: 4px;
    color: #000;
    width: 50%;
    float: right;
}

.right.clearfix .text-muted {
    color: #000 !important;
}

.right.clearfix .chat-body p {
    color: #000;
    font-family: initial;
}

.recevechat {
    width: 100%;
    display: block;
    float: left;
}

.chat-img.pull-right {
    width: 15%;
    float: left !important;
    margin-top: 5px;
}

/*.chat li.right .chat-body
{
    margin-right: 60px;
}*/


.chat li .chat-body p {
    margin: 0;
    font-size: 16px;
    word-wrap: break-word;
}

.panel .slidedown .glyphicon,
.chat .glyphicon {
    margin-right: 5px;
}
.profilpic.img-circle {
    width: 40px !important;
    border-radius: 30px;
    border: 1px solid #ccc;
    height: 40px !important;
}

.chat-img.pull-left {
    width: 15%;
    float: left;
}

.text-muted {
    float: right;
    color: #fff !important;
    font-size: 10px;
    padding-top: 5px;
}

.primary-font {
    font-size: 12px;
    font-weight: 400;
    float: none !important; 
}

.panel-heading {
    background-color: #fff;
    color: #666;
    font-size: 20px;
    padding: 10px;
    position: relative;
}

.panel.panel-primary {
    background-color: #e6eaea;
}

.panel-body {
    padding: 0px 0 0 13px;
    background: #fff;
    border-top: 1px solid #eee;
    border-left: 1px solid #eeee;
    border-bottom: 1px solid #eee;
}

.chat {
    max-height: 450px;
    min-height: 450px;
    overflow-y: scroll;
    margin-bottom: 47px;
}

.panel-footer {
    background-color: #eee0;
    padding: 18px;
    position: absolute;
    width: 100%;
    bottom: -6px;
    right: 0;
    border-top: none;
}

.emoji-wysiwyg-editor.form-control.input-sm.cke_editable.cke_editable_inline.cke_contents_ltr.cke_show_borders {
    height: 46px !important;
}

.chatback .col-md-9 {
    padding: 0px;
}

/*********************************************************************/
.wrap #profile-img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
    border: 3px solid red;
    float: left;
}

.wrap p {
    float: left;
    margin-left: 15px;
    color: #666;
    line-height: 60px;
}

.wrap .expand-button {
    float: right;
    margin-top: 23px;
    font-size: 0.8em;
    cursor: pointer;
    color: #435f7a;
}

#status-options ul {
    display: block;
    padding: 0px;
    float: left;
    width: 100%;
}

#status-options ul li {
    display: inline-block;
    width: 100%;
    background: #32465a;
    padding: 5px;
}

#profile {
    padding: 10px 0px;
}

.btn.btn-warning.sendmsg {
    background: rgba(82,179,217,0.9) !important;
    border-color: rgba(82,179,217,0.9);
    color: #fff !important;
}

#status-options input {
    text-transform: capitalize;
    width: 100%;
    margin-bottom: 5px;
}

.panel-heading i.fa.fa-search {
    position: absolute;
    right: 17px;
    top: 17px;
}

#search {
    width: 100%;
    float: left;
    color: #fff;
    padding: 10px 0px;
    position: relative;
}

.chatback .col-md-3 {
    background-color: #fff;
}

.contact {

    position: relative;
    padding: 10px 0 15px 0;
    font-size: 0.9em;
    cursor: pointer;
    list-style: none;
    float: left;
    width: 100%;
}

#contacts {
    height: 400px;
    float: left;
    width: 100%;
    overflow-y: auto;

}

.contact img {
    width: 20%;
    border-radius: 50%;
    float: left;
    margin-right: 10px;
}

input#searchmsgs {
    font-size: 15px;
    padding: 5px;
    float: right;
}

#contacts ul {
    float: left;
    width: 100%;
    padding: 0px;
}

.contact-status.online {

    position: absolute;
    right: 8px;
    margin: 0;
    width: 8px;
    height: 8px;
    border-radius: 4px;
    background: lightgreen;
    top: 10px;

}

li.contact .wrap {
    width: 100%;
    margin: 0 auto;
    position: relative;
    float: left;
}

#contacts ul li.contact .wrap span.away {
    background: #f1c40f;
}

#contacts ul li.contact .wrap .meta {
    padding: 13px 0 0 0;
    float: left;
    width: 75%;
}

#contacts ul li.contact .wrap .meta .name {
    color: #666;
    line-height: 0px !important;
    margin-left: 0px;
    float: left;
    width: 100%;
    font-weight: 600;
    margin-bottom: 5px;
    font-size: 12px;
}

.unreadcount {
    float: right;
    /*margin-right: 20px;*/
    background: rgba(82,179,217,0.9);
    height: 20px;
    width: 21px;
    line-height: 12px;
    color: #fff;
    text-align: center;
    border-radius: 100%;
    position: relative;
    bottom: 0px;
    font-size: 13px;
    top: 21px;
    right: 0;
    left: 25px;
}

#contacts ul li.contact .wrap .meta .preview {
    margin: 5px 0 0 0;
    padding: 0 0 1px;
    font-weight: 400;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    -moz-transition: 1s all ease;
    -o-transition: 1s all ease;
    -webkit-transition: 1s all ease;
    transition: 1s all ease;
    line-height: 15px;
}

#addcontact {
    background-color: transparent;
    color: #666;
    border: 0;
    float: left;
    margin-right: 5px;
    cursor: pointer;
}

#settings {
    background-color: transparent;
    color: #666;
    border: 0;
    float: left;
    cursor: pointer;
}

.emoji-picker-icon.emoji-picker.fa.fa-smile-o {
    position: absolute;
    top: 11px;
    right: 113px;
    font-size: 25px;
}

#btn-chat {
    font-size: 16px;
    line-height: 1.9;
    border-radius: 0px .2rem .2rem 0px !important;
    padding: 0.735rem .75rem !important;
}

.sideuser.active.show {
    background-color: #6c1414 !important;
    float: left;
    padding: 5px 0px;
}

.chatback .input-group {
    background: #fff;
    width: 100%;
    z-index:999;
}

.attachmenticon.fa.fa-paperclip {
    position: absolute;
    right: 180px;
    top: 7px;
    font-size: 28px;
    cursor: pointer;
}
.fa-paperclip:before {
    right: -110px;
    position: relative;
}
.emoji-wysiwyg-editor.form-control.input-sm {
    border: 0px !important;
    box-shadow: none;
}

.cke_editable:focus {
    border: 0px;
    box-shadow: none;
}

.active .wrap {
    background-color: #f3f6f8;
    padding: 10px;
}

.chat-body.clearfix {
    float: left;
    width: 85%;
}

div#search label {
    position: absolute;
    right: 12px;
    color: #666;
    top: 16px;
    font-size: 19px;
}

div#search input {
    width: 100%;
    height: 40px;
    padding: 5px;
    color: #666666;
    font-size: 14px;
}

i.fa.fa-download {
    color: #ffcc33;
}
.msg-box {
    width: 92%;
}
input#message {
    border-radius: 0px;
    height: 47px;
}
.emoji-wysiwyg-editor{
    width:95% !important;
    background-color: transparent !important;
}
.input-group-btn{
    float: right;
}
/*.user_status_icon {
    position: relative;
    left: -13.7%;
    top: 13px;
    font-size: 10px;
}*/
.user_status_icon .user_online{
    color: #4cd137;
}
.user_status_icon .user_offline{
    color: #fff;
}
/**********new design css*********/
@import url(https://fonts.googleapis.com/css?family=Lato:100,300,400,700);
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

html, body {
    background: #e5e5e5;
    font-family: 'Lato', sans-serif;
    margin: 0px auto;
}
::selection{
  background: rgba(82,179,217,0.3);
  color: inherit;
}
a{
  color: rgba(82,179,217,0.9);
}

/* M E N U */

.menu {
    position: inherit;
    top: 0px;
    left: 0px;
    right: 0px;
    width: 100%;
    height: 50px;
    background: rgba(82,179,217,0.9);
    z-index: 100;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}

.back {
    position: absolute;
    width: 80px;
    height: 50px;
    top: 0px;
    left: 0px;
    color: #fff;
    line-height: 50px;
    font-size: 30px;
    padding-left: 10px;
    cursor: pointer;
}
.back img {
    position: absolute;
    top: 5px;
    left: 0px;
    width: 40px;
    height: 40px;
    background-color: rgba(255,255,255,0.98);
    border-radius: 100%;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    -ms-border-radius: 100%;
    margin-left: 15px;
    }
.back:active {
    background: rgba(255,255,255,0.2);
}
.name{
    position: absolute;
    top: 0px;
    left: 70px;
    font-family: 'Lato';
    font-size: 25px;
    font-weight: 300;
    color: rgba(255,255,255,0.98);
    cursor: default;
}
.last{
    position: absolute;
    top: 30px;
    left: 70px;
    font-family: 'Lato';
    font-size: 11px;
    font-weight: 400;
    color: rgba(255,255,255,0.6);
    cursor: default;
}

/* M E S S A G E S */

.chat {
    list-style: none;
    background: none;
    margin: 0;
    padding: 0 0 50px 0;
    /*margin-top: 60px;*/
    margin-bottom: 10px;
    border:2px solid rgba(82,179,217,0.9);
}
.chat li {
    padding: 0.5rem;
    overflow: hidden;
    display: flex;
}
.chat .avatar {
    width: 40px;
    height: 40px;
    position: relative;
    display: block;
    z-index: 2;
    border-radius: 100%;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    -ms-border-radius: 100%;
    background-color: rgba(255,255,255,0.9);
}
.chat .avatar img {
    width: 40px;
    height: 40px;
    border-radius: 100%;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    -ms-border-radius: 100%;
    background-color: rgba(255,255,255,0.9);
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
.chat .day {
    position: relative;
    display: block;
    text-align: center;
    color: #c0c0c0;
    height: 20px;
    text-shadow: 7px 0px 0px #e5e5e5, 6px 0px 0px #e5e5e5, 5px 0px 0px #e5e5e5, 4px 0px 0px #e5e5e5, 3px 0px 0px #e5e5e5, 2px 0px 0px #e5e5e5, 1px 0px 0px #e5e5e5, 1px 0px 0px #e5e5e5, 0px 0px 0px #e5e5e5, -1px 0px 0px #e5e5e5, -2px 0px 0px #e5e5e5, -3px 0px 0px #e5e5e5, -4px 0px 0px #e5e5e5, -5px 0px 0px #e5e5e5, -6px 0px 0px #e5e5e5, -7px 0px 0px #e5e5e5;
    box-shadow: inset 20px 0px 0px #e5e5e5, inset -20px 0px 0px #e5e5e5, inset 0px -2px 0px #d7d7d7;
    line-height: 38px;
    margin-top: 5px;
    margin-bottom: 20px;
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}

.other .msg {
    order: 1;
    border-top-left-radius: 0px;
    box-shadow: -1px 2px 0px #D4D4D4;
}
.other:before {
    content: "";
    position: relative;
    top: 0px;
    right: 0px;
    left: 40px;
    width: 0px;
    height: 0px;
    border: 5px solid #fff;
    border-left-color: transparent;
    border-bottom-color: transparent;
}

.self {
    justify-content: flex-end;
    align-items: flex-end;
}
.self .msg {
    order: 1;
    border-bottom-right-radius: 0px;
    box-shadow: 1px 2px 0px #D4D4D4;
}
.self .avatar {
    order: 2;
}
.self .avatar:after {
    content: "";
    position: relative;
    display: inline-block;
    bottom: 59px;
    right: 0px;
    width: 0px;
    height: 0px;
    border: 5px solid #fff;
    border-right-color: transparent;
    border-top-color: transparent;
    box-shadow: 0px 2px 0px #D4D4D4;
}

.msg {
    background: white;
    min-width: 50px;
    padding: 10px;
    border-radius: 2px;
    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.07);
}
.msg p {
    font-size: 1.3rem;
    margin: 0 0 0.2rem 0;
    color: #777;
    word-break: break-all;
}
.msg img {
    position: relative;
    display: block;
    width: 200px;
    border-radius: 5px;
    box-shadow: 0px 0px 3px #eee;
    transition: all .4s cubic-bezier(0.565, -0.260, 0.255, 1.410);
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
@media screen and (max-width: 800px) {
    .msg img {
    width: 300px;
}
}
@media screen and (max-width: 550px) {
    .msg img {
    width: 174px;
}
.panel-footer { bottom: -17px; }
.attachmenticon.fa.fa-paperclip {
    right: 176px;
    font-size:20px;
    z-index:999;
}
.emoji-picker-icon.emoji-picker.fa.fa-smile-o{
    right: 94px;
    font-size: 17px;
}
.name{
    top: 8px;
    font-size: 16px;
}
}

.msg time {
    font-size: 1.1rem;
    color: #ccc;
    margin-top: 3px;
    float: right;
    cursor: default;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
.msg time:before{
    content:"\f017";
    color: #ddd;
    font-family: FontAwesome;
    display: inline-block;
    margin-right: 4px;
}

emoji{
    display: inline-block;
    height: 18px;
    width: 18px;
    background-size: cover;
    background-repeat: no-repeat;
    margin-top: -7px;
    margin-right: 2px;
    transform: translate3d(0px, 3px, 0px);
}
emoji.please{background-image: url(https://imgur.com/ftowh0s.png);}
emoji.lmao{background-image: url(https://i.imgur.com/MllSy5N.png);}
emoji.happy{background-image: url(https://imgur.com/5WUpcPZ.png);}
emoji.pizza{background-image: url(https://imgur.com/voEvJld.png);}
emoji.cryalot{background-image: url(https://i.imgur.com/UUrRRo6.png);}
emoji.books{background-image: url(https://i.imgur.com/UjZLf1R.png);}
emoji.moai{background-image: url(https://imgur.com/uSpaYy8.png);}
emoji.suffocated{background-image: url(https://i.imgur.com/jfTyB5F.png);}
emoji.scream{background-image: url(https://i.imgur.com/tOLNJgg.png);}
emoji.hearth_blue{background-image: url(https://i.imgur.com/gR9juts.png);}
emoji.funny{background-image: url(https://i.imgur.com/qKia58V.png);}

@-webikt-keyframes pulse {
  from { opacity: 0; }
  to { opacity: 0.5; }
}

::-webkit-scrollbar {
    min-width: 12px;
    width: 12px;
    max-width: 12px;
    min-height: 12px;
    height: 12px;
    max-height: 12px;
    background: #e5e5e5;
    box-shadow: inset 0px 50px 0px rgba(82, 179, 217, 0), inset 0px -52px 0px rgba(82, 179, 217, 0);
}

::-webkit-scrollbar-thumb {
    background: #bbb;
    border: none;
    border-radius: 100px;
    border: solid 3px #e5e5e5;
    box-shadow: inset 0px 0px 3px #999;
}

::-webkit-scrollbar-thumb:hover {
    background: #b0b0b0;
  box-shadow: inset 0px 0px 3px #888;
}

::-webkit-scrollbar-thumb:active {
    background: #aaa;
  box-shadow: inset 0px 0px 3px #7f7f7f;
}

::-webkit-scrollbar-button {
    display: block;
    height: 0px;
}

/* T Y P E */

input.textarea {
    position: fixed;
    bottom: 0px;
    left: 0px;
    right: 0px;
    width: 100%;
    height: 50px;
    z-index: 99;
    background: #fafafa;
    border: none;
    outline: none;
    padding-left: 55px;
    padding-right: 55px;
    color: #666;
    font-weight: 400;
}
.emojis {
    position: fixed;
    display: block;
    bottom: 8px;
    left: 7px;
    width: 34px;
    height: 34px;
    background-image: url(https://i.imgur.com/5WUpcPZ.png);
    background-repeat: no-repeat;
    background-size: cover;
    z-index: 100;
    cursor: pointer;
}
.emojis:active {
    opacity: 0.9;
}
.zip_file_image, .doc_file_image, .docs_file_image, .xlsx_file_image {
    width: 80px !important;
}
/*******sidebar css*******/
.side_header_section {
    background: rgba(82,179,217,0.9);
    padding: 6px;
}
.main_side_bar_sec{
    border: 2px solid rgba(82,179,217,0.9);
    margin-bottom: 15px;
    min-height: 500px;
}
.friend_listing_sec {
    padding-left: 0px;
    list-style: none;
    padding: 8px 8px 8px 6px;
    margin-bottom: 0px;
}
.friend_name {
    position: absolute;
    padding-left: 10px;
    font-size: 18px;
    color: #5FB7D9;
    top: 7px;
}
.last_msg {
    position: relative;
    top: 10px;
    left: 10px;
    color: #717171;
}
/*.friend_status_icon .user_offline {
    color: #000;
}
.friend_status_icon .user_online {
    color: #4cd137;
}
.friend_status_icon{
    position: relative;
    left: 10px;
    top: 8px;
}*/

.user_img{
	height: 70px;
	width: 70px;
	border:1.5px solid #f5f6fa;
}
.img_cont{
    position: relative;
    height: auto;
    width: 100%;
    margin-bottom: 10px;
    padding: 10px;
}
.online_icon{
	position: absolute;
    height: 11px;
    width: 11px;
    background-color: #4cd137;
    border-radius: 50%;
    bottom: 0.7em;
    right: 0.4em;
    border: 1px solid white;
    left: 38px;
}
.last_message_time{
    float:right;
}
</style>
<script>
$(document).ready(function() {
    $(".expand-button").click(function() {
        $("#status-options").toggle();
    });
});
</script>

@stop