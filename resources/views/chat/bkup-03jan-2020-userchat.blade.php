@extends('layouts.default-header')
@section('title', 'Chat Box')
@section('content')
<?php 
$current_userId = Auth::id();

if($recentchat){
  if($recentchat->receiver_id!=$current_userId){
      $receiverid = $receiver_id;
  } else {
      $receiverid = $recentchat->sender_id;
  }
} else {
  $receiverid = '';
}

?>
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/emojis/css/emoji.css">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container">
    <!--chat-box-div-->
    <div class="row chatback">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <?php
                     $receiverdata = DB::table('users')->where('id','=', $receiver_id)->where('is_active','=','1')->where('is_deleted','=','0')->first();
                     ?>
                     @if($receiverdata->profile_image!='')
                        <img id="profile-img" src="{{url('/public')}}/images/profileimage/{{$receiverdata->profile_image}}"
                            class="online profilpic img-circle" alt="">
                    @else
                        <img id="profile-img" src="{{url('/public')}}/images/avtar.png" class="online" alt="">
                    @endif <span class="recent_user_name" style="text-transform: capitalize;">{{  $receiverdata->name }}</span>
                    <!--@if(Auth::user()->profile_image!='')
                        <img id="profile-img" src="{{url('/public')}}/images/profileimage/{{Auth::user()->profile_image}}"
                            class="online profilpic img-circle" alt="">
                        @else
                        <img id="profile-img" src="{{url('/public')}}/images/avtar.png" class="online" alt="">
                        @endif <span class="recent_user_name"
                        style="text-transform: capitalize;">{{  Auth::user()->name }}</span>-->
                        <span class="user_status_icon"></span>
                    <i class="fa fa-search" aria-hidden="true"></i> <input placeholder="Search messages..."
                        id="searchmsgs" name="searchmsgs" value="" type="text" style="margin-left: 20px;">
                </div>
                <div class="panel-body tab-content" id="sidepanel">
                    <?php
                        $frienddetailQuery = DB::table('users');
                          if($current_userId==Auth::user()->id){
                            $frienddetailQuery->where('id', '=', $receiver_id);
                          } else {
                            $frienddetailQuery->where('id', '=', Auth::user()->id);
                          }
                        $getfriendInfo = $frienddetailQuery->first();

                    ?>
                    <div role="tabpanel" class="tab-pane chat active"
                        id="user_<?php echo $getfriendInfo->id;?>" userid="<?php echo $getfriendInfo->id;?>">
                    </div>

                </div>
                <div class="panel-footer">
                    <form name="sendmessage" id="sendmessage">
                        <div class="input-group">
                            <input type="hidden" name="friend_request_id" id="friend_request_id" value=""
                                class="friend_request_id">
                            <input type="hidden" name="sender_id" id="sender_id" value="<?php echo $current_userid;?>">
                            <input type="hidden" name="receiver_id" id="receiver_id" value="<?php echo $receiverid;?>">
                            <input type="hidden" name="is_read" id="is_read" value="">

                            <div class="msg-box">
                                <input id="message" name="message" type="text" class="form-control input-sm"
                                    placeholder="Type your message here..." value="" data-emojiable="true" />
                                <input type="file" id="attachments" name="attachments[]" style="display:none;" multiple>
                                <i class="attachmenticon fa fa-paperclip" id="countfile"></i>
                                    
                                </div>
                                <div class="send-button">
                                    <span class="input-group-btn">
                                        <button class="btn btn-warning sendmsg pull-right" id="btn-chat" type="submit">
                                            Send</button>
                                    </span>
                                </div>
                            </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

    <!--Chat-box-end-->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ url('/public') }}/emojis/js/config.js"></script>
<script src="{{ url('/public') }}/emojis/js/util.js"></script>
<script src="{{ url('/public') }}/emojis/js/jquery.emojiarea.js"></script>
<script src="{{ url('/public') }}/emojis/js/emoji-picker.js"></script>

<script type="text/javascript">
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

$(document).ready(function() {

    $('.attachmenticon').click(function() {
        $('#attachments').trigger('click');
    });

    $('#attachments').change(function() {
        if (this.files.length > 0) {
            $('.attachmenticon').html(this.files.length + ' files');
        } else {
            $('.attachmenticon').html('');
        }
    });

    $('#searchcontact').keyup(function() {
        var search = $(this).val();
        $.ajax({
            type: 'GET',
            url: 'userchatsearch',
            data: {
                search: search
            },
            success: function(resp) {
                $('#contacts').html(resp);
            }
        });
    });

    $('#searchmsgs').keyup(function() {
        var search = $(this).val();
        var senderid = $("#sender_id").val();
        var receiverid = $('.chat.active').attr('userid');
        var activediv = $('.chat.active').attr('id');
        $.ajax({
            type: 'GET',
            url: 'adminchatmsgsearch',
            data: {
                search: search,
                receiverid: receiverid,
                senderid: senderid
            },
            success: function(resp) {
                $('#' + activediv).html(resp);
            }
        });
    });

    $('.chat.active.tab-pane').on('scroll', function() {
        var userid = $(this).attr('userid');
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var sender_id = $("#sender_id").val();
            var receiver_id = $("#receiver_id").val();
            var base_url = "{{url('/')}}";
            $.ajax({
                type: 'GET',
                url: base_url+'/updateunreaduser',
                data: {
                    sender_id: sender_id,
                    receiver_id: receiver_id
                },
                success: function(resp) {
                    $('#side' + userid).find('.unreadcount').hide();
                }
            });
        }
    });

    $('body').on('click', '.contact', function() {
        $('#searchmsgs').val('');
        var activepane = $('.chat.active.tab-pane').attr('id');
        $('#sendmessage').show();
        $('#receiver_id').val($(this).attr('data-user'));
        $('#friend_request_id').val($(this).attr('data-friendrequestid'));
        $('.recent_user_name').html($(this).attr('data-recentusername'));

        fetchRecords();
        var receiver_id = $('.chat.active.tab-pane').attr('userid');;
        var sender_id = '{{Auth::user()->id}}';
        var base_url = "{{url('/')}}";
        $.ajax({
            type: 'GET',
            url: base_url+'/updateunreaduser',
            data: {
                sender_id: sender_id,
                receiver_id: receiver_id
            },
            success: function(resp) {
                $('#side' + receiver_id).find('.unreadcount').hide();
                $('#msgid').hide();
            }
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /*-----------------------------------------------*/
 
    $("#sendmessage").submit(function(e) {
        var activepane = $('.chat.active.tab-pane').attr('id');

        e.preventDefault(); //prevent default action 
        proceed = true;

        var allowed_file_types = ['image/png', 'image/gif', 'image/jpeg', 'image/jpg',
            'application/pdf', 'application/vnd.ms-excel', 'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip',
            'video/3gpp'
        ]; 

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            if (this.elements['attachments[]'].files.length > 5) {
                alert("Can not select more than 5 file(s)");
                proceed = false;
            }
            $(this.elements['attachments[]'].files).each(function(i, ifile) {
                if (ifile.value !== "") {
                    if (allowed_file_types.indexOf(ifile.type) === -
                        1) { 
                        alert(ifile.name + " is not allowed!");
                        proceed = false;
                        $('.attachmenticon').html('');
                        $('#attachments').val('');
                    }
                }
            });

            if (this.elements['attachments[]'].files.length < 1 && $('#message').val() == '') {
                proceed = false;
            }

        }

        var form_data = new FormData(this);
        if (proceed) {
            var base_url = "{{url('/')}}";
            $.ajax({
                url: base_url+'/usersajaxRequest',
                type: 'POST',
                data: form_data,
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false
            }).done(function(data) {
                if (data.statusCode == 1) {
                    $("#message").val('');
                    $(".emoji-wysiwyg-editor").html('');
                    $('.attachmenticon').html('');
                    $('#attachments').val('');
                    scrollToBottom(activepane);
                } else {
                    return false;
                }
            });
        }
    });
    
    fetchRecords();
    user_check_status();
    setInterval(function() {
        var msgsrch = $('#searchmsgs').val();
        if (msgsrch == '' || msgsrch == undefined) {
            fetchRecords();
            user_check_status();
        }
    }, 5000);

});

function fetchRecords() {
    var senderid = $("#sender_id").val();
    var receiverid = $('.chat.active').attr('userid');
    var activediv = $('.chat.active').attr('id');
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/getusersChat/' + senderid + '/' + receiverid,
        type: 'GET',
        dataType: 'html',
        success: function(response) {
            $('#' + activediv).html(response);
        }
    });
}

function user_check_status() {
    var receiverid = $('.chat.active').attr('userid');
    var base_url = "{{url('/')}}";
    $.ajax({
        url: base_url+'/userstatuscheck/' + receiverid,
        type: 'GET',
        success: function(response) {
            //alert(response);
            if(response== 'online'){
                $('.user_status_icon').html('<span class="text-success user_online">Online</span>');
            } else {
                $('.user_status_icon').html('<span class="text-success user_offline">Offline</span>');
            }
        }
    });
}

function fetchfriendlist() {
    $.ajax({
        url: 'ajaxfriendlist',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            var unreadchats = response.unreadchats;
            for (i in unreadchats) {
                $('#countunread' + unreadchats[i].sender_id).html(unreadchats[i].name +
                    '<span class="unreadcount">' + unreadchats[i].chat + '</span>');
            }
        }
    });
}
//===========================//
function myKeyPressFunction(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        $('#sendmessage').submit();
    }
}
//===========================//
function scrollToBottom(id) {
    var div = document.getElementById(id);
    setTimeout(function() {
        div.scrollTop = div.scrollHeight - div.clientHeight;
    }, 800);
}

function search() {
    if (event.keyCode == 13) {
        alert("should get the innerHTML or text value here");
    }
}
</script>
<script>
      $(function() {
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '{{ url("/public") }}/emojis/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();
      });
    </script>

<style type="text/css">
/********Chat-box-css**********/
.chatback {
    margin: 30px 0px;
    background-color: #e6eaea;
    box-shadow: 0px 0px 20px #ccc;
}

.chat {
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li {
    margin-bottom: 10px;
    padding: 6px 20px 4px 5px;
}

.left.clearfix {
    background-color: #435f7a;
    border-radius: 4px;
    color: #fff;
    width: 50%;
}

.right.clearfix {
    background-color: #eee6;
    border-radius: 4px;
    color: #000;
    width: 50%;
    float: right;
}

.right.clearfix .text-muted {
    color: #000 !important;
}

.right.clearfix .chat-body p {
    color: #000;
    font-family: initial;
}

.recevechat {
    width: 100%;
    display: block;
    float: left;
}

.chat-img.pull-right {
    width: 15%;
    float: left !important;
    margin-top: 5px;
}

/*.chat li.right .chat-body
{
    margin-right: 60px;
}*/


.chat li .chat-body p {
    margin: 0;
    font-size: 16px;
    word-wrap: break-word;
}

.panel .slidedown .glyphicon,
.chat .glyphicon {
    margin-right: 5px;
}

/*ul.chat {
    max-height: 400px;
    overflow-y: scroll;
}*/
.profilpic.img-circle {
    width: 40px !important;
    border-radius: 30px;
    border: 1px solid #ccc;
    height: 40px !important;
}

.chat-img.pull-left {
    width: 15%;
    float: left;
}

.text-muted {
    float: right;
    color: #fff !important;
    font-size: 10px;
    padding-top: 5px;
}

.primary-font {
    font-size: 12px;
    font-weight: 400;
    float: none !important; 
}

.panel-heading {
    background-color: #fff;
    color: #666;
    font-size: 20px;
    padding: 10px;
    position: relative;
}

.panel.panel-primary {
    background-color: #e6eaea;
}

.panel-body {
    padding: 0px 0 0 13px;
    background: #fff;
    border-top: 1px solid #eee;
    border-left: 1px solid #eeee;
    border-bottom: 1px solid #eee;
}

.chat {
    max-height: 450px;
    min-height: 450px;
    overflow-y: scroll;
    margin-bottom: 47px;
}

.panel-footer {
    background-color: #eee;
    padding: 10px;
    position: absolute;
    width: 100%;
    bottom: 0;
    right: 0;
}

.emoji-wysiwyg-editor.form-control.input-sm.cke_editable.cke_editable_inline.cke_contents_ltr.cke_show_borders {
    height: 46px !important;
}

.chatback .col-md-9 {
    padding: 0px;
}

/*********************************************************************/
.wrap #profile-img {
    width: 50px;
    height: 50px;
    border-radius: 50%;
    border: 3px solid red;
    float: left;
}

.wrap p {
    float: left;
    margin-left: 15px;
    color: #666;
    line-height: 60px;
}

.wrap .expand-button {
    float: right;
    margin-top: 23px;
    font-size: 0.8em;
    cursor: pointer;
    color: #435f7a;
}

#status-options ul {
    display: block;
    padding: 0px;
    float: left;
    width: 100%;
}

#status-options ul li {
    display: inline-block;
    width: 100%;
    background: #32465a;
    padding: 5px;
}

#profile {
    padding: 10px 0px;
}

.btn.btn-warning.sendmsg {
    background: #ff3333 !important;
    border-color: #ff3333;
    color: #fff !important;
}

#status-options input {
    text-transform: capitalize;
    width: 100%;
    margin-bottom: 5px;
}

.panel-heading i.fa.fa-search {
    position: absolute;
    right: 17px;
    top: 17px;
}

#search {
    width: 100%;
    float: left;
    color: #fff;
    padding: 10px 0px;
    position: relative;
}

.chatback .col-md-3 {
    background-color: #fff;
}

.contact {

    position: relative;
    padding: 10px 0 15px 0;
    font-size: 0.9em;
    cursor: pointer;
    list-style: none;
    float: left;
    width: 100%;
}

#contacts {
    height: 400px;
    float: left;
    width: 100%;
    overflow-y: auto;

}

.contact img {
    width: 20%;
    border-radius: 50%;
    float: left;
    margin-right: 10px;
}

input#searchmsgs {
    font-size: 15px;
    padding: 5px;
    float: right;
}

#contacts ul {
    float: left;
    width: 100%;
    padding: 0px;
}

.contact-status.online {

    position: absolute;
    right: 8px;
    margin: 0;
    width: 8px;
    height: 8px;
    border-radius: 4px;
    background: lightgreen;
    top: 10px;

}

li.contact .wrap {
    width: 100%;
    margin: 0 auto;
    position: relative;
    float: left;
}

#contacts ul li.contact .wrap span.away {
    background: #f1c40f;
}

#contacts ul li.contact .wrap .meta {
    padding: 13px 0 0 0;
    float: left;
    width: 75%;
}

#contacts ul li.contact .wrap .meta .name {
    color: #666;
    line-height: 0px !important;
    margin-left: 0px;
    float: left;
    width: 100%;
    font-weight: 600;
    margin-bottom: 5px;
    font-size: 12px;
}

.unreadcount {
    float: right;
    margin-right: 20px;
    background: #fff;
    height: 15px;
    width: 15px;
    line-height: 15px;
    color: red;
    text-align: center;
    border-radius: 100%;
    position: relative;
    bottom: 9px;
    font-size: 13px;
}

#contacts ul li.contact .wrap .meta .preview {
    margin: 5px 0 0 0;
    padding: 0 0 1px;
    font-weight: 400;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    -moz-transition: 1s all ease;
    -o-transition: 1s all ease;
    -webkit-transition: 1s all ease;
    transition: 1s all ease;
    line-height: 15px;
}

#addcontact {
    background-color: transparent;
    color: #666;
    border: 0;
    float: left;
    margin-right: 5px;
    cursor: pointer;
}

#settings {
    background-color: transparent;
    color: #666;
    border: 0;
    float: left;
    cursor: pointer;
}

.emoji-picker-icon.emoji-picker.fa.fa-smile-o {
    position: absolute;
    top: 11px;
    right: 113px;
    font-size: 25px;
}

#btn-chat {
    font-size: 16px;
    line-height: 1.9;
    border-radius: 0px .2rem .2rem 0px !important;
    padding: 0.735rem .75rem !important;
}

.sideuser.active.show {
    background-color: #6c1414 !important;
    float: left;
    padding: 5px 0px;
}

.chatback .input-group {
    background: #fff;
    width: 100%;
}

.attachmenticon.fa.fa-paperclip {
    position: absolute;
    right: 180px;
    top: 7px;
    font-size: 28px;
    cursor: pointer;
}
.fa-paperclip:before {
    right: -110px;
    position: relative;
}
.emoji-wysiwyg-editor.form-control.input-sm {
    border: 0px !important;
    box-shadow: none;
}

.cke_editable:focus {
    border: 0px;
    box-shadow: none;
}

.active .wrap {
    background-color: #f3f6f8;
    padding: 10px;
}

.chat-body.clearfix {
    float: left;
    width: 85%;
}

div#search label {
    position: absolute;
    right: 12px;
    color: #666;
    top: 16px;
    font-size: 19px;
}

div#search input {
    width: 100%;
    height: 40px;
    padding: 5px;
    color: #666666;
    font-size: 14px;
}

i.fa.fa-download {
    color: #ffcc33;
}
.msg-box {
    width: 92%;
}
input#message {
    border-radius: 0px;
    height: 47px;
}
.emoji-wysiwyg-editor{
    width:95% !important;
    background-color: transparent !important;
}
.input-group-btn{
    float: right;
}
.user_status_icon {
    position: relative;
    left: -13.7%;
    top: 13px;
    font-size: 10px;
}
.user_status_icon .user_online{
    color: #4cd137;
}
.user_status_icon .user_offline{
    color: #fff;
}
</style>
<script>
$(document).ready(function() {
    $(".expand-button").click(function() {
        $("#status-options").toggle();
    });
});
</script>

@stop