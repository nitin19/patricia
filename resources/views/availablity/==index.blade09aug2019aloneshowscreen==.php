@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<!-- @include('layouts.flash-message')-->
@include('layouts.notify-message')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<input type="hidden" name="cat_id" value="{{Auth::user()->cat_id}}" id="user_cat_id">
<input type="hidden" name="address" value="{{Auth::user()->address}}" id="user_address">
  <div class="avalability_sec">
    <div class="avalability_bg" id="availabilityDiv">
      <h2>Agenda</h2>
        <p>Descreva abaixo os dias da semana e horários em que você está disponível:</p>
      <div style="clear: both;"> </div>
      <div style="clear: both;"> </div>
      <div style="clear: both;"> </div>
      <div class="col-sm-3 hours_title">
        <h3>Horários</h3>
      </div>
      <div class="col-sm-9 addbusiness_hours">
       <div class="radiobtns">
          <label class="radiodiv">Atendimento nas horas descritas abaixo
            <input type="radio" class="spopen_hrs" name="openhrs" id="openhrs_opt1" value="1" {{ $spavailabilityInfo->openhrs == '1' ? 'checked' : '' }}>
            <span class="checkmark"></span>
          </label>
          <label class="radiodiv">Estou sempre disponível
            <input type="radio" class="spopen_hrs" name="openhrs" id="openhrs_opt2" value="0" {{ $spavailabilityInfo->openhrs == '0' ? 'checked' : '' }}>
            <span class="checkmark"></span>
          </label>
        </div>
        <div id="div1">
          <form>                          
            <div class="schedule_timings">
              <label class="checkbox_div">Segunda-feira 
                <input type="checkbox" class="spdays" name="day_monday" id="day_monday" value="1" {{ $spavailabilityInfo->day_monday == '1' ? 'checked' : '' }}>
                <span class="checkmark"></span>
              </label>
              <div class="schedule_input">
                <input type="text" name="monday_start" id="monday_start" value="{{ $spavailabilityInfo->monday_start }}" class="form-control">
                <span>-</span>
                <input type="text" name="monday_end" id="monday_end" value="{{ $spavailabilityInfo->monday_end }}" class="form-control">
              </div>
            </div>
            <div class="schedule_timings">
              <label class="checkbox_div">Terça-feira
               <input type="checkbox" class="spdays" name="day_tuesday" id="day_tuesday" value="1" {{ $spavailabilityInfo->day_tuesday == '1' ? 'checked' : '' }}>
               <span class="checkmark"></span>
             </label>
             <div class="schedule_input">
              <input type="text" name="tuesday_start" id="tuesday_start" value="{{ $spavailabilityInfo->tuesday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="tuesday_end" id="tuesday_end" value="{{ $spavailabilityInfo->tuesday_end }}" class="form-control">
            </div>
          </div>
          <div class="schedule_timings">
            <label class="checkbox_div">Quarta-feira
              <input type="checkbox" class="spdays" name="day_wednesday" id="day_wednesday" value="1" {{ $spavailabilityInfo->day_wednesday == '1' ? 'checked' : '' }}>
              <span class="checkmark"></span>
            </label>
            <div class="schedule_input">
              <input type="text" name="wednesday_start" id="wednesday_start" value="{{ $spavailabilityInfo->wednesday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="wednesday_end" id="wednesday_end" value="{{ $spavailabilityInfo->wednesday_end }}" class="form-control">
            </div>
          </div>
          <div class="schedule_timings">
            <label class="checkbox_div">Quinta-feira 
             <input type="checkbox" class="spdays" name="day_thursday" id="day_thursday" value="1" {{ $spavailabilityInfo->day_thursday == '1' ? 'checked' : '' }}>
             <span class="checkmark"></span>
           </label>
            <div class="schedule_input">
              <input type="text" name="thursday_start" id="thursday_start" value="{{ $spavailabilityInfo->thursday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="thursday_end" id="thursday_end" value="{{ $spavailabilityInfo->thursday_end }}" class="form-control">
            </div>
          </div>
          <div class="schedule_timings">
            <label class="checkbox_div">Sexta-feira
              <input type="checkbox" class="spdays" name="day_friday" id="day_friday" value="1" {{ $spavailabilityInfo->day_friday == '1' ? 'checked' : '' }}>
              <span class="checkmark"></span>
            </label>
            <div class="schedule_input">
              <input type="text" name="friday_start" id="friday_start" value="{{ $spavailabilityInfo->friday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="friday_end" id="friday_end" value="{{ $spavailabilityInfo->friday_end }}" class="form-control">
            </div>
          </div>
          <div class="schedule_timings">
            <label class="checkbox_div">Sábado
              <input type="checkbox" class="spdays" name="day_saturday" id="day_saturday" value="1" {{ $spavailabilityInfo->day_saturday == '1' ? 'checked' : '' }}>
              <span class="checkmark"></span>
            </label>
            <div class="schedule_input">
              <input type="text" name="saturday_start" id="saturday_start" value="{{ $spavailabilityInfo->saturday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="saturday_end" id="saturday_end" value="{{ $spavailabilityInfo->saturday_end }}" class="form-control">
            </div>
          </div>
          <div class="schedule_timings">
            <label class="checkbox_div">Domingo
              <input type="checkbox" class="spdays" name="day_sunday" id="day_sunday" value="1" {{ $spavailabilityInfo->day_sunday == '1' ? 'checked' : '' }}>
              <span class="checkmark"></span>
            </label>
            <div class="schedule_input">
              <input type="text" name="sunday_start" id="sunday_start" value="{{ $spavailabilityInfo->sunday_start }}" class="form-control">
              <span>-</span>
              <input type="text" name="sunday_end" id="sunday_end" value="{{ $spavailabilityInfo->sunday_end }}" class="form-control">
            </div>
          </div>
          <div class="available_footer">      
            <!--  <button class="btn" type="button" onclick="clearInput();">Cancelar</button>  -->
          <button class="btn savebtn" type="button" id="saveBtn">Salvar</button>
        </div>
      </form>
    </div>
    <div id="div2">
      <form>
        <div class="schedule_timings">
          <label class="checkbox_div">Segunda-feira
            <input type="checkbox" class="spdays_2" name="day_monday_2" id="day_monday_2" value="1" checked="checked" disabled="disabled">
            <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="monday_start_2" id="monday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="monday_end_2" id="monday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Terça-feira
           <input type="checkbox" class="spdays_2" name="day_tuesday_2" id="day_tuesday_2" value="1" checked="checked" disabled="disabled">
           <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="tuesday_start_2" id="tuesday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="tuesday_end_2" id="tuesday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Quarta-feira
           <input type="checkbox" class="spdays_2" name="day_wednesday_2" id="day_wednesday_2" value="1" checked="checked" disabled="disabled">
           <span class="checkmark"></span>
         </label>
         <div class="schedule_input">
          <input type="text" name="wednesday_start_2" id="wednesday_start_2" value="00:00" class="form-control" disabled="disabled">
          <span>-</span>
          <input type="text" name="wednesday_end_2" id="wednesday_end_2" value="23:59" class="form-control" disabled="disabled">
        </div>
      </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Quinta-feira
           <input type="checkbox" class="spdays_2" name="day_thursday_2" id="day_thursday_2" value="1" checked="checked" disabled="disabled">
           <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="thursday_start_2" id="thursday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="thursday_end_2" id="thursday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Sexta-feira
            <input type="checkbox" class="spdays_2" name="day_friday_2" id="day_friday_2" value="1" checked="checked" disabled="disabled">
            <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="friday_start_2" id="friday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="friday_end_2" id="friday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Sábado
           <input type="checkbox" class="spdays_2" name="day_saturday_2" id="day_saturday_2" value="1" checked="checked" disabled="disabled">
           <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="saturday_start_2" id="saturday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="saturday_end_2" id="saturday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="schedule_timings">
          <label class="checkbox_div">Domingo
            <input type="checkbox" class="spdays_2" name="day_sunday_2" id="day_sunday_2" value="1" checked="checked" disabled="disabled">
            <span class="checkmark"></span>
          </label>
          <div class="schedule_input">
            <input type="text" name="sunday_start_2" id="sunday_start_2" value="00:00" class="form-control" disabled="disabled">
            <span>-</span>
            <input type="text" name="sunday_end_2" id="sunday_end_2" value="23:59" class="form-control" disabled="disabled">
          </div>
        </div>                               
        <div class="available_footer">
          <!--  <button class="btn" type="button" onclick="clearInput();">Cancelar</button>  -->
          <button class="btn savebtn" type="button" id="saveBtn_2">Salvar</button>
        </div>
      </form>
    </div>
</div>

</div>
<div  class="profile_compl"  align="center">
  @if(Auth::user()->address == ''&&Auth::user()->cat_id == '')
  <a href="@if(Auth::user()->user_role='Provider'){{url('/serviceprovider/profile')}}@endif" ><button type="button" class="
    btn btn-success">Clique aqui para completar seu perfil </button></a>
    @endif
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {

    $('#Alert_Success').hide();
    $('#Alert_Danger').hide();

    var isoptChecked = $('#openhrs_opt1').is(':checked');
    if(isoptChecked == true) {
     $('#div2').hide('fast');
     $('#div1').show('fast');
   } else {
    $('#div1').hide('fast');
    $('#div2').show('fast');
  }

  $('#openhrs_opt1').click(function () {
   $('#div2').hide('fast');
   $('#div1').show('fast');
 });

  $('#openhrs_opt2').click(function () {
    $('#div1').hide('fast');
    $('#div2').show('fast');
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#saveBtn_2').click(function(e) {
    e.preventDefault();
    var baseUrl  = '<?php echo url('/');?>'; 
    var openhrs = '0';
    $.ajax({
     type:'POST',
     url:baseUrl+'/serviceprovider/savespavailablty',
     data:{ openhrs:openhrs },
     success:function(data){
      if(data==1) {

        $('#availabilityDiv').animate({
          scrollTop: 0
        }, 'slow');

        $.notify({
          message: 'Sua agenda foi salva com sucesso!',
        },{
          type: 'success',
          offset: 
          {
            x: 10,
            y: 130
          },
          animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
          },
        });



        window.setTimeout(function () {
         if($('#user_cat_id').val() == '' ||$('#user_address').val() == '' ){
           window.location.href = baseUrl+'/serviceprovider/completeprofile' ;
         }
         else{
           window.location.href = baseUrl+'/serviceprovider/dashboard' ;
         }
       }, 5000);
      }
    }
  });
  });


  //$('#monday_start').prop('readonly', true);
  //$('#monday_end').prop('readonly', true);
  $('#monday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  }); 

  $('#monday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  }); 

  $("#monday_start").on("dp.change", function (e) {
    $('#monday_end').prop('readonly', false);
    if( e.date ){
      $('#monday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#monday_end').data("DateTimePicker").minDate(e.date);
  });


  //$('#tuesday_start').prop('readonly', true);
  //$('#tuesday_end').prop('readonly', true);
  $('#tuesday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#tuesday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#tuesday_start").on("dp.change", function (e) {
    $('#tuesday_end').prop('readonly', false);
    if( e.date ){
      $('#tuesday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#tuesday_end').data("DateTimePicker").minDate(e.date);
  });

  //$('#wednesday_start').prop('readonly', true);
 // $('#wednesday_end').prop('readonly', true);
  $('#wednesday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#wednesday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#wednesday_start").on("dp.change", function (e) {
    $('#wednesday_end').prop('readonly', false);
    if( e.date ){
      $('#wednesday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#wednesday_end').data("DateTimePicker").minDate(e.date);
  });

  //$('#thursday_start').prop('readonly', true);
  //$('#thursday_end').prop('readonly', true);
  $('#thursday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#thursday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#thursday_start").on("dp.change", function (e) {
    $('#thursday_end').prop('readonly', false);
    if( e.date ){
      $('#thursday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#thursday_end').data("DateTimePicker").minDate(e.date);
  });

  //$('#friday_start').prop('readonly', true);
  //$('#friday_end').prop('readonly', true);
  $('#friday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#friday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#friday_start").on("dp.change", function (e) {
    $('#friday_end').prop('readonly', false);
    if( e.date ){
      $('#friday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#friday_end').data("DateTimePicker").minDate(e.date);
  });

  //$('#saturday_start').prop('readonly', true);
  //$('#saturday_end').prop('readonly', true);
  $('#saturday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#saturday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#saturday_start").on("dp.change", function (e) {
    $('#saturday_end').prop('readonly', false);
    if( e.date ){
      $('#saturday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#saturday_end').data("DateTimePicker").minDate(e.date);
  });

  //$('#sunday_start').prop('readonly', true);
  //$('#sunday_end').prop('readonly', true);
  $('#sunday_start').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $('#sunday_end').datetimepicker({
    format: 'HH:mm',
    useCurrent: false,
  });

  $("#sunday_start").on("dp.change", function (e) {
    $('#sunday_end').prop('readonly', false);
    if( e.date ){
      $('#sunday_end').data("DateTimePicker").date(e.date.add(30, 'm'));
    }

    $('#sunday_end').data("DateTimePicker").minDate(e.date);
  });



  $('#day_monday').click(function() {
    var isMondayChecked = $(this).is(':checked');
    if(isMondayChecked == true) {
      $('#monday_start').prop('readonly', false);
      $('#monday_start').prop('required', true);
      $('#monday_end').prop('required', true);
           // $('#monday_end').prop('readonly', false);
         } else {
          $('#monday_start').val('');
          $('#monday_end').val('');
          $('#monday_start').prop('required', false);
          $('#monday_end').prop('required', false);
          $('#monday_start').prop('readonly', false);
          $('#monday_end').prop('readonly', false);
          $('#monday_start').removeClass('vlderror');
          $('#monday_end').removeClass('vlderror');
        }
      });

  $('#day_tuesday').click(function() {
    var isTuesdayChecked = $(this).is(':checked');
    if(isTuesdayChecked == true) {
      $('#tuesday_start').prop('required', true);
      $('#tuesday_end').prop('required', true);
      $('#tuesday_start').prop('readonly', false);
          //  $('#tuesday_end').prop('readonly', false);
        } else {
          $('#tuesday_start').val('');
          $('#tuesday_end').val('');
          $('#tuesday_start').prop('required', false);
          $('#tuesday_end').prop('required', false);
          $('#tuesday_start').prop('readonly', false);
          $('#tuesday_end').prop('readonly', false);
          $('#tuesday_start').removeClass('vlderror');
          $('#tuesday_end').removeClass('vlderror');
        }
      });

  $('#day_wednesday').click(function() {
    var isWednesdayChecked = $(this).is(':checked');
    if(isWednesdayChecked == true) {
      $('#wednesday_start').prop('required', true);
      $('#wednesday_end').prop('required', true);
      $('#wednesday_start').prop('readonly', false);
          //  $('#wednesday_end').prop('readonly', false);
        } else {
          $('#wednesday_start').val('');
          $('#wednesday_end').val('');
          $('#wednesday_start').prop('required', false);
          $('#wednesday_end').prop('required', false);
          $('#wednesday_start').prop('readonly', false);
          $('#wednesday_end').prop('readonly', false);
          $('#wednesday_start').removeClass('vlderror');
          $('#wednesday_end').removeClass('vlderror');
        }
      });

  $('#day_thursday').click(function() {
    var isThursdayChecked = $(this).is(':checked');
    if(isThursdayChecked == true) {
      $('#thursday_start').prop('required', true);
      $('#thursday_end').prop('required', true);
      $('#thursday_start').prop('readonly', false);
          //  $('#thursday_end').prop('readonly', false);
        } else {
          $('#thursday_start').val('');
          $('#thursday_end').val('');
          $('#thursday_start').prop('required', false);
          $('#thursday_end').prop('required', false);
          $('#thursday_start').prop('readonly', false);
          $('#thursday_end').prop('readonly', false);
          $('#thursday_start').removeClass('vlderror');
          $('#thursday_end').removeClass('vlderror');
        }
      });

  $('#day_friday').click(function() {
    var isFridayChecked = $(this).is(':checked');
    if(isFridayChecked == true) {
      $('#friday_start').prop('required', true);
      $('#friday_end').prop('required', true);
      $('#friday_start').prop('readonly', false);
           // $('#friday_end').prop('readonly', false);
         } else {
          $('#friday_start').val('');
          $('#friday_end').val('');
          $('#friday_start').prop('required', false);
          $('#friday_end').prop('required', false);
          $('#friday_start').prop('readonly', false);
          $('#friday_end').prop('readonly', false);
          $('#friday_start').removeClass('vlderror');
          $('#friday_end').removeClass('vlderror');
        }
      });

  $('#day_saturday').click(function() {
    var isSaturdayChecked = $(this).is(':checked');
    if(isSaturdayChecked == true) {
      $('#saturday_start').prop('required', true);
      $('#saturday_end').prop('required', true);
      $('#saturday_start').prop('readonly', false);
          //  $('#saturday_end').prop('readonly', false);
        } else {
          $('#saturday_start').val('');
          $('#saturday_end').val('');
          $('#saturday_start').prop('required', false);
          $('#saturday_end').prop('required', false);
          $('#saturday_start').prop('readonly', false);
          $('#saturday_end').prop('readonly', false);
          $('#saturday_start').removeClass('vlderror');
          $('#saturday_end').removeClass('vlderror');
        }
      });

  $('#day_sunday').click(function() {
    var isSundayChecked = $(this).is(':checked');
    if(isSundayChecked == true) {
      $('#sunday_start').prop('required', true);
      $('#sunday_end').prop('required', true);
      $('#sunday_start').prop('readonly', false);
           // $('#sunday_end').prop('readonly', false);
         } else {
          $('#sunday_start').val('');
          $('#sunday_end').val('');
          $('#sunday_start').prop('required', false);
          $('#sunday_end').prop('required', false);
          $('#sunday_start').prop('readonly', false);
          $('#sunday_end').prop('readonly', false);
          $('#sunday_start').removeClass('vlderror');
          $('#sunday_end').removeClass('vlderror');
        }
      });

  $("#monday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#monday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#tuesday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#tuesday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#wednesday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#wednesday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#thursday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#thursday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#friday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#friday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#saturday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#saturday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });

  $("#sunday_start").focusout(function(){
    $(this).removeClass('vlderror');
  });
  $("#sunday_end").focusout(function(){
    $(this).removeClass('vlderror');
  });      


  $('#saveBtn').click(function(e) {
    e.preventDefault();

    var isoptChecked = $('#openhrs_opt1').is(':checked');
    if(isoptChecked == true) {
      var openhrs = 1; 
    } else {
      var openhrs = 0;
    }

    var isMondayChecked = $('#day_monday').is(':checked'); 
    var isTuesdayChecked = $('#day_tuesday').is(':checked');
    var isWednesdayChecked = $('#day_wednesday').is(':checked');
    var isThursdayChecked = $('#day_thursday').is(':checked');
    var isFridayChecked = $('#day_friday').is(':checked');
    var isSaturdayChecked = $('#day_saturday').is(':checked');
    var isSundayChecked = $('#day_sunday').is(':checked');

    var monday_start = $('#monday_start').val();
    var monday_end = $('#monday_end').val();
    var tuesday_start = $('#tuesday_start').val();
    var tuesday_end = $('#tuesday_end').val();
    var wednesday_start = $('#wednesday_start').val();
    var wednesday_end = $('#wednesday_end').val();
    var thursday_start = $('#thursday_start').val();
    var thursday_end = $('#thursday_end').val();
    var friday_start = $('#friday_start').val();
    var friday_end = $('#friday_end').val();
    var saturday_start = $('#saturday_start').val();
    var saturday_end = $('#saturday_end').val();
    var sunday_start = $('#sunday_start').val();
    var sunday_end = $('#sunday_end').val();

    if(isMondayChecked == true || isTuesdayChecked == true || isWednesdayChecked == true || isThursdayChecked == true || isFridayChecked == true || isSaturdayChecked == true || isSundayChecked == true || isMondayChecked == true) {

      if(isMondayChecked == true) {
        var day_monday = 1; 
        if(monday_start=='') {
         $('#monday_start').addClass('vlderror');
         return false;
       }
       if(monday_end=='') {
         $('#monday_end').addClass('vlderror');
         return false;
       }
     } else {
      var day_monday = 0;
    }

    if(isTuesdayChecked == true) {
      var day_tuesday = 1; 
      if(tuesday_start=='') {
       $('#tuesday_start').addClass('vlderror');
       return false;
     }
     if(tuesday_end=='') {
       $('#tuesday_end').addClass('vlderror');
       return false;
     }
   } else {
    var day_tuesday = 0;
  }

  if(isWednesdayChecked == true) {
    var day_wednesday = 1; 
    if(wednesday_start=='') {
     $('#wednesday_start').addClass('vlderror');
     return false;
   }
   if(wednesday_end=='') {
     $('#wednesday_end').addClass('vlderror');
     return false;
   }
 } else {
  var day_wednesday = 0;
}

if(isThursdayChecked == true) {
  var day_thursday = 1;
  if(thursday_start=='') {
   $('#thursday_start').addClass('vlderror');
   return false;
 }
 if(thursday_end=='') {
   $('#thursday_end').addClass('vlderror');
   return false;
 } 
} else {
  var day_thursday = 0;
}

if(isFridayChecked == true) {
  var day_friday = 1; 
  if(friday_start=='') {
   $('#friday_start').addClass('vlderror');
   return false;
 } 
 if(friday_end=='') {
   $('#friday_end').addClass('vlderror');
   return false;
 } 
} else {
  var day_friday = 0;
}

if(isSaturdayChecked == true) {
  var day_saturday = 1;
  if(saturday_start=='') {
   $('#saturday_start').addClass('vlderror');
   return false;
 } 
 if(saturday_end=='') {
   $('#saturday_end').addClass('vlderror');
   return false;
 }  
} else {
  var day_saturday = 0;
}

if(isSundayChecked == true) {
  var day_sunday = 1; 
  if(sunday_start=='') {
   $('#sunday_start').addClass('vlderror');
   return false;
 } 
 if(sunday_end=='') {
   $('#sunday_end').addClass('vlderror');
   return false;
 } 
} else {
  var day_sunday = 0;
}

var baseUrl  = '<?php echo url('/');?>'; 
$.ajax({
 type:'POST',
 url:baseUrl+'/serviceprovider/updatespavailablty',
 data:{ openhrs:openhrs, day_monday:day_monday, day_tuesday:day_tuesday, day_wednesday:day_wednesday, day_thursday:day_thursday, day_friday:day_friday, day_saturday:day_saturday, day_sunday:day_sunday, monday_start:monday_start, monday_end:monday_end, tuesday_start:tuesday_start, tuesday_end:tuesday_end, wednesday_start:wednesday_start, wednesday_end:wednesday_end, thursday_start:thursday_start, thursday_end:thursday_end, friday_start:friday_start, friday_end:friday_end, saturday_start:saturday_start, saturday_end:saturday_end, sunday_start:sunday_start, sunday_end:sunday_end},
 success:function(data){
  if(data==1) {

    $('#availabilityDiv').animate({
      scrollTop: 0
    }, 'slow');

    $.notify({
      message: 'Sua agenda foi salva com sucesso!',
    },{
      type: 'success',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });


    window.setTimeout(function () {
     if($('#user_cat_id').val() == ''){
       window.location.href = baseUrl+'/serviceprovider/completeservice' ;
     }
     else{
       window.location.href = baseUrl+'/serviceprovider/dashboard' ;
     }

   }, 5000);
  }
}
});

} else {

  $('#availabilityDiv').animate({
    scrollTop: 0
  }, 'slow');
  $.notify({
    message: 'You have not selected any day. Please select at least one!',
  },{
    type: 'success',
    offset: 
    {
      x: 10,
      y: 130
    },
    animate: {
      enter: 'animated fadeInRight',
      exit: 'animated fadeOutRight'
    },
  });

            // $('#Alert_Danger').show();
            // window.setTimeout(function () {
            //     $("#Alert_Danger").fadeTo(500, 0).slideUp(500, function () {
            //         $(this).hide();
            //     });
            // }, 5000);
          }

        });

});

function clearInput() {
  window.location.href="<?php echo url('/serviceprovider/spavailablty');?>";
}

</script>
<style type="text/css">
 /* #Alert_Success { opacity: 1 !important; } 
 #Alert_Danger { opacity: 1 !important; }  */
 .vlderror { border: 2px solid red; }
</style>   

@stop
