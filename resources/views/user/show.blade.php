@extends('layouts.default-header')

@section('title', 'Home')

@section('content')
<main>
<?php
$user_info = Auth::user();
$profile_image = $user_info->profile_image;
?>
<section class="maincontent searchbarbg"> 
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
    <div class="container">
      <div class="acccompletionsec">
        <h1>Account Completion</h1>
        <div class="acccompletion_tabs">
           <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
  </ul>
@include('layouts.flash-message')
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="profile">
      <form class="completionform" id="completionform" method="post" action="{{ url('/accountcomplete/update/') }}"  enctype="multipart/form-data">
      	{{ csrf_field() }}
        <div class="row">
        <div class="col-sm-2 profileimage">
               <div class="form-group imagediv" >
              <div class="imagebrowse" id="previewdiv">
                <?php
                if($profile_image==''){
                  ?>
              <img src="{{ url('/public/images/') }}/prflimg.png" class="img-circle" style="width: 200px;height:200px;">
              <?php
            }
            else {
              ?>
              <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-circle" style="width: 200px;height:200px;">
              <?php
            }
              ?>
              </div>
              <div class="col-sm-3 nopadding imgbtns">
                <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i>
               </div>
              <div class="col-md-3 prflinput">
                <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
              </div>
            </div>         
        </div>
        <div class="col-sm-9 col-sm-offset-1  profileform_right">
          <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Name</label>
              <div class="profileinput col-sm-9 nopadding">
                <input type="text" name="name" id="name" class="form-control" value="{{ $user_info->name }}" placeholder="" readonly>
                <i class="fa fa-check"></i>
              </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Email Address</label>
              <div class="profileinput col-sm-9 nopadding">
                <input type="email" name="email" id="email" class="form-control" value="{{ $user_info->email }}" placeholder="" readonly>
                <i class="fa fa-check"></i>
                <input type="hidden" name="user_id" value="{{ $user_info->id }}">
              </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Phone Number</label>
              <div class="profileinput col-sm-9 nopadding">
                <input type="text" name="phone" id="phone" class="form-control" value="{{ $user_info->phone }}" placeholder="">
                <i class="fa fa-check"></i>
              </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Password</label>
              <div class="profileinput col-sm-9 nopadding">
                <input type="password" name="password" id="password" class="form-control" value="" placeholder="">
              </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">I Want</label>
              <div class="profileinput col-sm-9 nopadding">
                <select class="form-control" name="user_role">
                  <option vlaue="">Select your option</option>
                  <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Provider</option>
				  <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Taker</option>
                </select>
              </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Country</label>
              <div class="profileinput col-sm-9 nopadding">
                <select class="form-control" name="country">
                	<?php
                	foreach($countries as $country_name){
                		?>
                		<option value="<?php echo $country_name; ?>" <?php if($user_info->country==$country_name) { echo 'selected'; } ?>><?php echo $country_name; ?></option>
                		<?php
                	}
                		?>
                </select>
              </div>
            </div>
        </div>
   <div class="col-sm-12">
            <div class="form-group profileform_right">
              <label class="col-sm-3">Postal Code</label>
              <div class="profileinput col-sm-9 nopadding">
                <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $user_info->zipcode; ?>" placeholder="">
              </div>
            </div>
        </div>
        <div class="col-sm-12 nextbtn">
          <input type="submit" class="form-control" value="Next Step">
        </div>
        </div>
      </div>
      </form>


    </div>
    <div role="tabpanel" class="tab-pane" id="services">....</div>
  </div>

        </div>
      </div>
    </div>
  </div>
</section>
</main>
@stop