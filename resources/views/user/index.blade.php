@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="lock_box">
<div class="col-sm-12">
 <div class="service_box">
  <table class="table table-borderless heading-name">
    <thead>
      <tr>
        <th>Sr No.</th>
        <th>Name/Email</th>
        <th>Role</th>
        <th>Category</th>
        <th>Phone </th>
        <th>Address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
<?php $count=0; ?>
@foreach($users as $user_info)
<?php
      $name = $user_info->name;
      $email = $user_info->email;
      $is_active = $user_info->is_active;
?>
<tr>
      <td>{{ ++$count }}</td>
      <td>{{ $name }}<br><span class="painting">{{ $email }}</span></td>
      <td>{{ ucfirst($user_info->user_role) }}</td>
      <td>{{ ucfirst($user_info->category) }}</td>
      <td>{{ $user_info->phone }}</td>
      <td>{{ $user_info->address }}</td>
      <td><a href="{{ url('/admin-users/edit') }}/{{ $user_info->id }}"><i class="fa fa-eye" style="font-size:16px"></i></a>
        <a class="deletebutton" style="cursor: pointer;"><i class="fa fa-trash" style="font-size:16px"></i></a></td>
</tr>
@endforeach
    </tbody>
  </table>
  <div class="pagination-button">

<div class="pagination prev_next">
 {!! $users->render() !!}
</div>
  </div>
</div>
  </div>
 </div>
@stop