@extends('layouts.default-header')

@section('title', 'Política de Privacidade | NaVizinhança')
@section('description', 'Confira aqui a política de privacidade do NaVizinhança.')
@section('keywords','Política de Privacidade, NaVizinhança, Na Vizinhança, termos de uso, condições')

@section('content')
<div class="Privacy">
<div class="container">
    <div class="termbox">
        <h2 class="font-change2">POLÍTICA DE PRIVACIDADE</h2>
<p class="pribacytext">Esta Política de Privacidade descreve as práticas da plataforma <strong>NaVizinhança</strong>, em relação aos dados coletados por meio do site <a href="{{url('/')}}">www.navizinhanca.com</a>, de acordo com as leis em vigor.</p>
<h4>"Your trust is our top priority."</h4>
<p class="pribacytext">Esta política se aplica aos dados que identificam o <strong>USUÁRIO</strong> individualmente (Dados Pessoais) e demais dados fornecidos pelo Usuário ou coletados durante a utilização do site <a href="{{url('/')}}">www.navizinhanca.com</a>. Em todos os casos, o <strong>NaVizinhança</strong> cumpre com toda legislação brasileira aplicável à proteção de dados.</p>
</div>
    <div class="General">
    	<h3>1. DA ACEITAÇÃO</h3>
    	<p>1.1. Ao acessar e/ou utilizar o conteúdo do site <a href="{{url('/')}}">www.navizinhanca.com</a>, o <strong>USUÁRIO</strong> expressa sua aceitação plena e sem reservas aos termos aqui constantes, para todos os fins de direito. Assim, declara que fez leitura atenta e completa da presente <strong>Política de Privacidade</strong>.  Se o <strong>USUÁRIO</strong> não concordar com algum dos termos e regras aqui previstos, não deverá acessar e/ou utilizar o conteúdo.</p> 

    </div>
    <div class="General">
    	<h3>2. QUAIS SÃO AS INFORMAÇÕES OBTIDAS</h3>
    	<p>2.1 Os dados são coletados quando inseridos ou submetidos voluntariamente pelo <strong>USUÁRIO</strong> ao se cadastrar no <a href="{{url('/')}}">www.navizinhanca.com</a>.</p> 

        <p>2.2. As informações que coletamos diretamente do <strong>USUÁRIO</strong> são: foto, nome, e-mail, telefone, CPF, RG e endereço. No caso de prestador de serviço, o mesmo pode incluir mais detalhes sobre sua experiências, assim como cursos e certificados.</p> 

        <p>2.2.2. Informações consideradas dados pessoais identificáveis que podemos coletar a partir da navegação do <strong>USUÁRIO</strong> no site <a href="{{url('/')}}">www.navizinhanca.com</a>:</p>

        <p>– logs</p>

        <p>– cookies, por meio da ferramenta Google Analytics, que coleta de forma anônima e para fins analíticos, os seguintes dados:
        a) idioma;
        b) país e cidade;
        c) navegador, sistema operacional e provedor de serviços;
        d) resolução de tela, tipo de dispositivo (desktop ou celular), modelo do aparelho de celular;
        e) páginas que o usuário navega dentro do site, se está no site naquele momento, forma como o usuário acessou o site (se foi via buscador ou direto pela URL do site), termo de pesquisa que utilizou no site, tempo na página do site.
        </p>

        <p>2.2.2.1. O <strong>USUÁRIO</strong> poderá saber mais detalhes sobre o Google Analytics em “Como o Google utiliza os dados quando o usuário usa sites ou aplicativos dos nossos parceiros“, localizado em <a href="google.com/intl/pt-BR/policies/privacy/partners/"><google.com/intl/pt-BR/policies/privacy/partners/></a>.</p>

        <p>2.2.2.2. Qualquer uso feito pela Google dos dados do <strong>USUÁRIO</strong> coletados por meio dessas ferramentas serão de responsabilidade única e exclusiva da Google, isentando completamente o <strong>NaVizinhança</strong> de qualquer responsabilidade decorrente.</p>

        <p>2.2.2.3. Caso o <strong>USUÁRIO</strong> não concorde com a coleta de dados pelo Google Analytics poderá desativá-lo por meio do Add-on do seu navegador, disponível no link <a href="https://tools.google.com/dlpage/gaoptout?hl=pt-BR"><https://tools.google.com/dlpage/gaoptout?hl=pt-BR></a>.</p>

        <p>2.3 Cabe ao <strong>USUÁRIO</strong> o dever de prestar informações corretas e atualizadas. Não somos responsáveis pela precisão, veracidade ou falta dela nas informações prestadas pelo <strong>USUÁRIO</strong>.</p>

        <p>2.4 Todas as tecnologias utilizadas pelo site <a href="{{url('/')}}">www.navizinhanca.com</a>  respeitarão sempre a legislação vigente e os termos desta <strong>Política de Privacidade</strong>.</p>
    </div>
    <div class="General">
    	<h3>3. ISENÇÃO DE RESPONSABILIDADE</h3>
    	<p>3.1 Ao navegar no site, o <strong>USUÁRIO</strong> poderá ser conduzido, via hyperlink, a conteúdos ou serviços, a outros portais ou plataformas que poderão coletar suas informações e ter sua própria Política de Privacidade. Hyperlinks externos não constituem endosso pelo <strong>NaVizinhança</strong> dos sites/ambientes vinculados ou as informações, produtos ou serviços ali contidos.</p>
        <p>3.2. O <strong>NaVizinhança</strong> não concede qualquer garantia relacionada à disponibilidade ou continuidade de funcionamento do site <a href="{{url('/')}}">www.navizinhanca.com</a>. Além disso,  consideradas as características da internet, o <strong>NaVizinhança</strong> não garante a segurança e privacidade do Conteúdo fora do domínio <a href="{{url('/')}}">www.navizinhanca.com</a>, nem que o mesmo será ininterrupto, livre de vírus ou outros problemas, erros e ataques e, em particular, não garante que terceiros não autorizados não possam acessar e, eventualmente, interceptar, eliminar, alterar, modificar ou manipular de qualquer modo os dados presentes e/ou transmitidos a seus servidores.</p> 

    </div>
<?php /*******next content add after this(neha) ***************/?>
    <div class="General">
        <h3>4. COMO SERÃO UTILIZADAS AS INFORMAÇÕES</h3>
        <p>4.1. Podemos utilizar os dados que foram coletados e os registros das atividades no site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com</a> para as seguintes finalidades:
            <ul>
                <li>vinculação de prestador de serviço e contratante</li>
                <li>posicionamento no mapa para localização do usuário</li>
                <li>indicação de prestadores de serviço com perfil semelhante</li>
                <li>possível verificação e validação das informações descritas no perfil do usuário</li>
                <li>contato com potenciais clientes</li>
                <li>análise estatística de resultados</li>
                <li>promoção dos serviços ofertados</li>
                <li>aperfeiçoamento dos serviços no site</li>
                <li>melhora do conteúdo informado</li>
            </ul>
        </p>
        <p>4.2.  O consentimento referente à coleta de dados fornecido pelo<strong> USUÁRIO </strong>é coletado de forma livre, expressa, individual, clara, específica e legítima.</p>
        <p>4.2.1. Caso o <strong>USUÁRIO</strong> queira atualizar seus dados cadastrais, o mesmo poderá acessar o “Painel de controle” mediante login em seu perfil.</p> 
        <p>4.2.2. Caso o <strong>USUÁRIO</strong> queira cancelar sua conta, o mesmo poderá acessar a opção “Encerrar seu cadastro” no menu “Sua conta”, localizado no “Painel de controle” mediante login em seu perfil.</p>
        <p>4.3. Os dados coletados e as atividades registradas poderão ser compartilhados com autoridades judiciais, administrativas ou governamentais competentes, sempre que houver requerimento, requisição ou ordem judicial.</p>
        <p>4.5. A base de dados formada por meio da coleta de dados no site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com</a> é de nossa propriedade e responsabilidade, sendo que seu uso, acesso e compartilhamento, quando necessários, serão realizados dentro dos limites e propósitos dos nossos negócios, podendo, neste sentido, serem disponibilizados para consulta e cedidos a fornecedores e autoridades, desde que obedecido ao disposto na presente <strong>Política de Privacidade.</strong></p>
        <p>4.6. Internamente, os dados de nossos <strong>USUÁRIOS</strong> são acessados somente por profissionais devidamente autorizados, respeitando os princípios de proporcionalidade, necessidade e relevância para os nossos objetivos, além do compromisso de confidencialidade e preservação da privacidade nos termos desta <strong>Política de Privacidade.</strong></p>
    
    </div>
    <div class="General">
        <h3>5. ARMAZENAMENTO DOS DADOS E REGISTROS</h3>
        <p>5.1. Os dados e os registros de atividades coletados serão armazenados em ambiente seguro e controlado, pelo prazo mínimo de 6 (seis) meses, nos termos do Marco Civil da Internet, observado o estado da técnica disponível. Todavia, considerando que nenhum sistema de segurança é infalível, nos eximimos de quaisquer responsabilidades por eventuais danos e/ou prejuízos decorrentes de falhas, vírus ou invasões do nosso banco de dados, salvo nos casos em que tiver dolo ou culpa.</p>
        <p>5.1.1. Os dados poderão ser excluídos antes desse prazo, caso haja solicitação de encerramento de cadastro por parte do <strong>USUÁRIO </strong>ou por bloqueio feito a este <strong>USUÁRIO</strong> diretamente pela equipe do <strong>NaVizinhança</strong>.</p>
        <p>5.1.2. No entanto, por motivo de lei e/ou determinação judicial os dados podem ser mantidos por período superior, findo o qual, serão excluídos com uso de métodos de descarte seguro.</p>
        <p>5.2 Os dados coletados são armazenados em servidores próprios do <strong>NaVizinhança</strong>, localizados no Brasil, bem como na nuvem, o que pode ensejar, nessa situação, a transferência ou processamento dos dados fora do Brasil, em países terceiros.</p>
    </div>
     <div class="General">
        <h3>6. EXIBIÇÃO, RETIFICAÇÃO E EXCLUSÃO DE DADOS</h3>
        <p>6.1. O <strong>USUÁRIO</strong> poderá exibir ou retificar ou excluir seus dados pessoais, por meio do site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com </a>em “Painel de Controle”.</p>
        <p>6.2. O <strong>USUÁRIO</strong> poderá enviar e-mail <a href="mailto::contato@navizinhanca.com">contato@navizinhanca.com</a> caso tenha dúvidas em como proceder.</p>
        <p>6.3 Podemos, para fins de auditoria, segurança, controle de fraudes e preservação de direitos, permanecer com o histórico de registro dos dados de nossos <strong>USUÁRIOS</strong> por prazo maior nas hipóteses que a lei ou norma regulatória assim estabelecer ou para preservação de direitos. Contudo, temos a faculdade de excluí-los definitivamente segundo nossa conveniência em prazo menor, ou conforme solicitação da empresa contratante dos serviços do <strong>NaVizinhança</strong>. </p>
    </div>
    <div class="General">
        <h3>7. DISPOSIÇÕES GERAIS</h3>
        <p>7.1. Podemos alterar o teor desta <strong>Política de Privacidade</strong> a qualquer momento, conforme a finalidade ou necessidade, tal qual para adequação e conformidade legal de disposição de lei ou norma que tenha força jurídica equivalente, cabendo ao nosso <strong>USUÁRIO</strong> verificá-la sempre que efetuar o acesso ao site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com</a>.</p>
        <p>7.1.1. Ocorrendo atualizações significativas neste documento e que demandem coleta de consentimento, o <strong>NaVizinhança<strong> notificará o <strong>USUÁRIO</strong> pelo e-mail fornecido.</p>
        <p>7.2. Caso haja alguma dúvida sobre as condições estabelecidas nesta <strong>Política de Privacidade</strong> ou qualquer documento do <strong>NaVizinhança</strong>, por favor entre em contato por meio dos canais de atendimento supramencionados.</p>
        <p>7.3. Caso alguma disposição desta <strong>Política de Privacidade</strong> seja considerada ilegal ou ilegítima por autoridade da localidade em que o nosso <strong>USUÁRIO</strong> resida ou da sua conexão à Internet, as demais condições permanecerão em pleno vigor e efeito.</p>
        <p>7.4. O <strong>USUÁRIO</strong> reconhece que toda comunicação realizada por e-mail (aos endereços por ele informados), SMS, aplicativos de comunicação instantânea ou qualquer outra forma digital, virtual e digital também são válidas como prova documental, sendo eficazes e suficientes para a divulgação de qualquer assunto que se refira aos serviços prestados pelo <strong>NaVizinhança</strong>, bem como às condições de sua prestação, ressalvadas as disposições expressamente diversas previstas nesta <strong>Política de Privacidade</strong>.</p>
    </div>
    <div class="General">
        <h3>8. LEI APLICÁVEL E JURISDIÇÃO</h3>
        <p>8.1. Esta <strong>Política de Privacidade</strong> e a relação decorrente das ações aqui compreendidas, assim como qualquer disputa que surja em virtude disto será regulada exclusivamente pela legislação brasileira.</p>
        <p>8.2. Fica eleito o Foro Central da Comarca da Capital do Estado de São Paulo para dirimir qualquer questão envolvendo o presente documento, renunciando as partes a qualquer outro, por mais privilegiado que seja ou venha a ser.</p>
    </div>
     <div class="General">
        <h3>9. GLOSSÁRIO</h3>
        <p>Para os fins deste documento, devem se considerar as seguintes definições e descrições para seu melhor entendimento:</p>
        <p><strong>Cookies:</strong> são informações enviadas pelo servidor do site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com</a> para o computador dos <strong>USUÁRIOS</strong>, para identificá-lo e obter dados de acesso, como páginas navegadas ou links acessados, permitindo, desta forma, personalizar a navegação dos USUÁRIOS no site, de acordo com o seu perfil. Não é necessário aceitar os cookies para navegar no site.</p>
        <p><strong>Dado Pessoal:</strong> Informações relativas a uma pessoa singular/natural identificada ou identificável, que é a titular dos dados. É considerada identificável, a pessoa singular que possa ser identificada, direta ou indiretamente, em especial por referência a um identificador, como por exemplo um nome, um número de identificação, dados de localização, identificadores por via eletrônica ou a um ou mais elementos específicos da identidade física, fisiológica, genética, mental, econômica, cultural ou social.</p>
        <p><strong>IP:</strong> Abreviatura de Internet Protocol. É conjunto alfanumérico que identifica os dispositivos dos <strong>USUÁRIOS</strong> na Internet.</p>
        <p><strong>Logs:</strong> Registros de atividades dos USUÁRIOS efetuadas no site <a href="{{url('/')}}" target="_blank">www.navizinhanca.com</a></p>
        <p><strong>USUÁRIO:</strong> Pessoa que acessa ou interage com as atividades oferecidas no site.</p>
    </div>
    
    
    

   <!--  <div class="General">
    	<h6>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h6>
    	<p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."

</p> 

    </div>
    <div class="General">
    	<h6>1914 translation by H. Rackham</h6>
    	<p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p> 

    </div>   -->
</div>
</div>
<script>
  
  window.setTimeout(function() {
    $(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
</script>

@stop
