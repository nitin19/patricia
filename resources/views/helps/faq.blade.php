@extends('layouts.default-header')

@section('title', 'FAQ')

@section('content')

<div class="container Privacy">
                  <h2 class="font-change2"> FAQ’s</h2>
                  <h4>Ancestry FAQ’s:</h4>
                  <h4>Q. What is Lorem Ipsum?</h4>
                  <h4>Q. Where does it come from?</h4>
				  <h4>Q. Why do we use it?</h4>
                  <h4>Q. Where can I get some?</h4>
                  
                  <br>

                   <div class="General wow fadeInUp animated" style="visibility: visible;">

                    <h6>What is Lorem Ipsum?</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                  <h6>Where does it come from?</h6>
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                  <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>

                  <h6>Why do we use it?</h6>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                  <p>You can read the Privacy Statement for the particular website(s) or service(s)
                  you are using for more details about how Ancestry shares your data and your
                  choices related to that sharing.</p>

                  <h6>Where can I get some?</h6>
                  <p>You can review, update, or delete most information about yourself through your
                  account page on the relevant website (e.g., the “Your Account” page on
                  Ancestry.com or the “Account Details” page on Newspapers.com and Fold3.com)
                  usually located in the pull-down menu in the top right corner of the relevant
                  website.</p>
                  <p>For AncestryDNA, you can review and update your information on your DNA
                  settings page. If you agreed to the Informed Consent to participate in research,
                  and then withdraw consent, we will stop using information about you for the
                  Research Project and any future research. However, information cannot be
                  withdrawn from studies in progress, completed studies, or published results.
                  </p>
                  <p>If you have questions about how to review, update, or delete information, you
                  should look in the “Manage Your privacy” pages below, or contact our Member
                  Services team.</p>

                </div>
 
    </div>

@stop
