
<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Lock Screen</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>    
<div class="lockscreen-back">
    <?php $id=Auth::user()->id;
        $name=Auth::user()->name;
        $image=Auth::user()->profile_image;
        $role=Auth::user()->user_role;
        $admin_info= DB::table('business_settings')
                    ->join('users','business_settings.user_id','=','users.id')
                    ->where('users.user_role','=','admin')
                    ->select('business_settings.*','users.user_role','users.id')->first();
        $admin_img = $admin_info->admin_profile_picture;
     ?>
    <div class="lockscreen">
    <h1>{{ ucfirst($name) }}</h1>
    <div class="lockscreen-item">

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="POST" action="{{url('/unlock')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" class="form-control" value="{{$name}}" name="e_user">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
        <!--  <img src="/public/images/userimages/{{$image}}" alt="User Image" class="user-img">-->
          <img src="@if($image=='') {{ url('/public/images') }}/prof_dummy2.png @else @if($role == 'admin') {{ url('/public/images/admin-assets') }}/{{ $admin_img }} @else{{ url('/public/images/profileimage') }}/{{ $image }} @endif @endif" alt="User Image" class="user-img">
        </div>
        <!-- /.lockscreen-image -->
          <div class="input-group">
            <input type="password" class="lockinput" placeholder="password" name="e_password" required>
             

            <!--<div class="input-group-btn">-->
                 <!-- <input type="submit" class="lockbtn" value="Unlock">-->
             <button type="button" class="lockbtn" onClick = 'this.form.submit();'><i class="fa fa-arrow-right text-muted"></i></button>
           <!-- </div>-->
          </div>
        </form>
        
        <!-- /.lockscreen credentials -->

    </div>
    @if($errors->any())
                <p style="color:red;font-size:12px;">{{$errors->first()}}</p>
            @endif
    <p class="help-block">Enter your password to retrieve your session</p>
    <a href="{{ route('logout') }}" class="signlink">Logout</a>
    <h5 class="lockscreen-footer">Copyright © 2019 <a href="#" class="signlink"> navizinhanca </a></h5>
    <h5 class="lockscreen-footerend"> All rights reserved</h5>
</div>
</div>
<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    </script>
<style type="text/css">
.lockscreen-back {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;padding: 0px 20px;
    text-align: center;
    background: #d2d6de;
}
.lockscreen h1 {
    font-family: 'Roboto', sans-serif;
    font-size: 32px;
    text-align: center;
    margin-bottom: 25px;
    color: #444;
    font-weight: bold;
}
.lockscreen h1 span {
    font-weight: 300;
}
.lockscreen h6 {
    text-align: center;
    font-weight: 600;
    font-size: 13px;
    color: #444;
}
.lockscreen-item {
    border-radius: 4px;
    padding: 0;
    background: #fff;
    position: relative;
    margin: 10px auto 30px auto;
    width: auto;
    /*float: left;*/
}
.lockscreen-image {
    border-radius: 50%;
    position: absolute;
    left: -10px;
    top: -25px;
    background: #fff;
    padding: 5px;
    z-index: 10;
}
.user-img {
    border-radius: 50%;
    width: 70px;
    height: 70px;
}
.lockscreen-credentials {
    margin-left: 70px;
}
.lockscreen-credentials .form-control {
    border: 0;
}

.input-group-btn {
    position: relative;
    font-size: 0;
    white-space: nowrap;
}
.text-muted {
    color: #777;
}
.help-block {
    display: block;
    margin-top: 5px;
    margin-bottom: 5px;
    color: #737373;
    font-size: 13px;
    float: left;
    width: 100%;
    font-family: 'Roboto', sans-serif;
}
.lockscreen-footer {
    margin-top: 10px;
    color:#333;
    font-size: 13px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 0px;
}
.lockscreen-footerend {
    margin-top: 5px;
    color:#333;
    font-size: 13px;
    font-weight: 400;
    font-family: 'Roboto', sans-serif;
    margin-bottom: 0px;
}
.signlink {
    font-size: 13px;
    color: #3c8dbc;
    text-decoration: none;
    font-family: 'Roboto', sans-serif;
}

.lockinput {
    width: 70%;
    float: left;
    /*height: 25px;*/
    padding: 10px 10px;
    border:none;
    outline:none;
}
button.lockbtn {
    padding: 10px;
    border: none;
    background: #fff;
    outline:0;
    cursor:pointer;
}
</style>
</body>
</html>

