  @extends('layouts.default-header')
  @section('title', 'Home')
  @section('content')

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <main>
    <!--mapsection-start-->
    <!--Mouse-Cursor-Effect-(div-HTML)-->
    <div id="age-gate" class="display dismissed">
     <div class="feathers">
       <i class="feather"></i>
       <i class="feather"></i>
       <i class="feather"></i>
     </div>
   </div>
   <div class="custom-cursor" style="opacity: 1;">
    <span class="main" style="top: 493px; left: 250px;">
    </span>
    <span class="follow" style="top: 493px; left: 250px;">
    </span>
  </div>
  <!--Mouse-Cursor-Effect-(div-HTML)End-->
  <section class="mapsection">
    <div id="map" style="width: 100%; height: 490px; background: #89C4FA"/></div>
  </section>
  <!--mapsection-end-->
  <!--serviceform-start-->
  <?php
  $lat_str1 = '';
  $long_str1 = '';
  foreach($users as $user_info){
    if($user_info->latitude!=''){
      $lat_str1 .= $user_info->latitude.',';
      $long_str1 .= $user_info->longitude.',';
    }
  }
  $lat_str = explode(',',rtrim($lat_str1,","));
  $long_str = explode(',',rtrim($long_str1,','));
  $data = array_combine($lat_str,$long_str);
  $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip, true));

  $latitude_center =$new_arr[0]['geoplugin_latitude'];
  $longitude_center = $new_arr[0]['geoplugin_longitude'];
  $getcurrentposition= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip, true));
  $latitudeCurrent =$getcurrentposition['geoplugin_latitude'];
  $longitudeCurrent = $getcurrentposition['geoplugin_longitude'];
  ?>
  <section class="seriveformsec">
    <div class="container">
      <div class="row">
        <div class="serviceformbg">
          <form class="serviceform" id="serviceform">
            <div class="row">
              <div class="col-sm-12"><h3>Encontre prestadores de serviço</h3></div>
              
            </div>
            <div class="">
              <div class="col-sm-2 formdiv homeformdiv">
                <input type="hidden" name="sortby" value="desc">
            <select class="custom-select" id="cat" name="cat" required oninvalid="this.setCustomValidity('Please select the category.')" placeholder="Selecione a Categoria">
             <!--  <option value="" selected="selected">Selecione a Categoria</option> -->
             <option value=""  style="display:none;"></option>
              @foreach($category as $category_info)
              <option value="{{ $category_info->id }}">{{ $category_info->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-sm-2 formdiv homeformdiv">
            <input type="text" name="zipcode" class="form-control" id="zipcode" value="" placeholder="Digite o CEP" maxlength="9" style="text-transform: unset!important;">
          </div>
          <div class="col-sm-2 formdiv homeformdiv">

          <input type="text" class = "form-control" id="autocomplete" name="area" placeholder="Busque por localização" />

          <!--  <select name="area" id="area" class="custom-select sources area" placeholder="Select Area">
          <option value="">Please Select</option>
            <option value="0-10">0-10</option>
            <option value="10-20">10-20</option>
            <option value="20-30">20-30</option>
            <option value="30-40">30-40</option>
            <option value="40-50">40-50</option>
          </select> -->
      </div>
         <div class="col-sm-2 formdiv homeformdiv">
          <select name="rating" id="rating" class="custom-select sources rating" placeholder="Avaliação dos usuários">
            <!-- <option value="">Por favor selecione</option> -->
            <option value=""></option>
            <option value="5">&#xf005;&#xf005;&#xf005;&#xf005;&#xf005; </option>
            <option value="4">&#xf005;&#xf005;&#xf005;&#xf005; </option>
            <option value="3">&#xf005;&#xf005;&#xf005; </option>
            <option value="2">&#xf005;&#xf005; </option>
            <option value="1">&#xf005; </option>
          </select>
          </div>
          <div class="col-sm-2 formbutton homeformbutton">
            <input type="submit" class="btn find_provider" value="Encontre provedores">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
<section class="howitworksec pagesection">
  <div class="container ">
    <div class="row ">
      <div class="col-sm-12 howitworkcontent text-center">
        <h1 class="pagetitle">Como funciona</h1>
        <p class="pagetext">Simples e grátis!<br/>
       Use o NaVizinhança a qualquer hora e em qualquer lugar.</p>
        <div class="howitworkblks">
          <div class="row setcontentview">
            <div class="col-sm-4 howitwrkblk">
              <div class="howitwrkblkbg">
                <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/Find-The-Service-Provider.png" class="img-responsive">
                </div>
                <h3>FAÇA SUA BUSCA </h3>
                  <p>Selecione o tipo de prestação de serviço ou região para encontrar um profissional.</p>
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                 <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/howitblk2.png" class="img-responsive">
                </div>
                <h3>CONTATE OS PROFISSIONAIS</h3>
                  <p>Após a seleção dos profissionais, <br/> contate-os para validação de disponibilidade e orçamento.</p> 
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                 <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/feedback.png" class="img-responsive">
                </div>
                <h3>NÃO ESQUEÇA DE DAR O SEU FEEDBACK</h3>
                  <p>Depois do contato, ajude-nos dando o feedback/avaliação desse profissional, para que possamos continuamente melhorar nossas recomendações para consultas futuras.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="servicesec pagesection">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 serviceseccontent text-center">
          <h1 class="pagetitle">Tipos de Serviços</h1>
          <div class="serviceblks">
            <div class="row">
              <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                  <?php
                  $count=1;
                  foreach($category as $category_info){
                    $cat_image = $category_info->cat_image;
                    if($cat_image==''){
                      $categoryimage = url('/')."/public/images/main_category/default.png";
                    }else {
                      $categoryimage =  url('/')."/public/images/main_category"."/".$cat_image;
                    }
                    ?>
                    <div class="item">
                      <div class="pad15">
                       <a href="{{ url('/search?cat='.$category_info->id) }}"><img src="{{$categoryimage}}"></a>
                       <h4><a href="{{ url('/search?cat='.$category_info->id) }}">{{ $category_info->name }}</a></h4>
                     </div>
                   </div>
                   <?php
                   ++$count;
                 }
                 ?>
               </div>
               <button class="btn btn-primary leftLst"><i class="fa fa-angle-left"></i></button>
               <button class="btn btn-primary rightLst"><i class="fa fa-angle-right"></i></button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <section class="aboutservice pagesection">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 abtservicecontent text-center">
        <h1 class="pagetitle">Estamos aqui para ajudar! E sem custo!!!</h1>
          <p class="pagetext">O NaVizinhança nasce com o intuito de simplificar a busca de prestadores de serviços.</p>
          <p class="pagetext"> Procurando alguém na sua região? Buscando alguém bem recomendado? Tem restrição de despesas para a contratação do serviço? Utilize os filtros que melhor se adequam para sua busca e contate o profissional de sua preferência.</p>
          <p class="pagetext">Não esqueça de avaliar o prestador de serviço após o contato, positivamente ou negativamente!!! </p>
          <p class="pagetext">Seja qual for o motivo, sua opinião é importante para nós.</p>
           <span class="pagetext" style="color:#0E3C5E;font-weight: bold;">Exemplos: profissional com telefone incorreto, ou excelente profissional, ou profissional não apareceu...</span> 
          <p class="pagetext">Só assim podemos melhorar nossas indicações para quem precisa dos serviços desses profissionais. </p>
          <p class="pagetext">Não espere, faça sua busca agora mesmo e encontre o profissional que melhor se enquadra para sua necessidade!</p>
        <div class="abtserviceimg">
          <img src="{{ url('/public') }}/images/service_provider.png" class="img-responsive" alt="abtserviceimg">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="whitebg"></section>
@if(count($testimonial)>0)
<section class="testimonialsection pagesection">
  <div class="container">
    <div class="row">
      <div id="carousel-example-generic" class="col-sm-12 carousel slide testimonialslider" data-ride="carousel">
        <!-- <h4>CLIENT FEEDBACK</h4> -->
       <h1>O que os nossos usuários falam sobre o NaVizinhança:</h1>
        <!-- Wrapper for slides -->

        <div class="carousel-inner" role="listbox">
          @foreach($testimonial as $testimonials)
          <div class="item @if($testimonials->id == $testimonial[0]->id){{'active'}}@endif">
            <div class="row">
              
               <?php
             $profile_pic = $testimonials->profille_pic;
              if($profile_pic==''){
              $profilepic = "../public/images/testimonialimage/clothing_icon.png";
              }else {
              $profilepic = "../public/images/testimonialimage/".$profile_pic;  
              }
          ?>
              <div class="col-sm-12">
                <div class="testislidecntnt">
                  <p>{{ $testimonials->description }}</p>
                  <div class="clientinfo">
                    <img src="{{ $profilepic }}">
                    <h4>{{ $testimonials->name }}</h4>
                    <span>{{$testimonials->profile}}</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
            
            
<!--               <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Devido às fortes chuvas, precisei de reparos no meu telhado. Pelo NaVizinhança consegui facilmente uma lista de profissionais e os contatei. Resolvi meu problema no mesmo dia, e ele me cobrou um preço que cabia no meu bolso, apesar da urgência.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div> -->
            </div>
          </div>
          @endforeach
<!--           <div class="item">
            <div class="row">
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Meu filho ficou de recuperação e precisei de ajuda para reforço escolar. Encontrei aqui no site uma pessoa que mora no nosso prédio. “Solução boa, bonita e barata” para ambas as partes.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Devido às fortes chuvas, precisei de reparos no meu telhado. Pelo NaVizinhança consegui facilmente uma lista de profissionais e os contatei. Resolvi meu problema no mesmo dia, e ele me cobrou um preço que cabia no meu bolso, apesar da urgência.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
            </div>
          </div> -->
         
        </div>
      </div>
    </div>
  </section>
 @endif
  <section class="pagesection newslettersec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 centered">
          <div class="newsletterbox slower-4s">
           <h1>INSCREVA-SE</h1>
            <h4>Fique por dentro das novidades do NaVizinhança</h4>
            <div id="mc_embed_signup">
              <form action="https://navizinhanca.us20.list-manage.com/subscribe/post?u=6afdec3041ad4b6f838402be4&amp;id=4d143d65f1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate onSubmit=formValidate()>
                <div id="mc_embed_signup_scroll">
                  <!--<label for="mce-EMAIL">Subscribe to our mailing list</label>-->
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Digite aqui seu Email" required="required" style="position:relative;height:34px; text-transform: unset!important;">
                 <!--  <p id="msg"style="text-transform: capitalize;"></p> -->
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                   <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6afdec3041ad4b6f838402be4_4d143d65f1" tabindex="-1" value=""></div>
                  <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary"></div>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container-fluid cta_bg mt-4">
    <div class="row">
        <div class="col-md-3 emailicon">
            <img src="https://collegefashionnetwork.com/public/images/mail-icon.png" style="height: 150px;margin-top: -30px;" class="img_move"> 
        </div>
        <div class="col-md-9">
            <h4 class="emailshow">E-mail para contato: <a href="mailto:contato@navizinhanca.com"><u> contato@navizinhanca.com</u></a></h4>
        </div>
    </div>
</div>
  <style>
      #mce-EMAIL-error {
        position: absolute;
        margin-left: 104px !important;
      }
    @media screen and (max-width:768px) {
      #mce-EMAIL-error {
          position: relative;
          margin-left: 0px !important;
      }
    }

   h4.emailshow {
    font-weight: 600;
    line-height: 35px;
    padding: 40px 0px;
    margin: 0px;
    font-family: Lato;
    font-size: 25px;
    text-align: left;
}
    .img_move {
    -webkit-animation: mover 1s infinite alternate;
    animation: mover 1s infinite alternate;
}

.img_move {
    -webkit-animation: mover 1s infinite alternate;
    animation: mover 1s infinite alternate;
}

@-webkit-keyframes mover {
    0% {
        transform: translateY(0);
    }

    100% {
        transform: translateY(-20px);
    }
}

@keyframes  mover {
    0% {
        transform: translateY(0);
    }

    100% {
        transform: translateY(-20px);
    }
}
  </style>
  <script  type="text/javascript">
  

      $("#autocomplete").autocomplete({
        source: function (request, response) {
             var u = '{{ url("/")}}';
             $.ajax({
                 url: u+'/search_address/search_address.php',
                 type: "GET",
                 data: request,
                 dataType: "json",
                  beforeSend: function(xhr){
                  xhr.setRequestHeader('Accept', 'application/json');
                  },
                 success: function (data) {
                  response(data);
                 }
             });
          },
          select: function (event, ui) {
           // Set selection
           $('#autocomplete').val(ui.item.value); // display the selected text
           return false;
          }
      });
      jQuery(document).ready(function() {
        setInterval(function(){
          if($(".rightLst").hasClass("over")){
              $(".MultiCarousel-inner").css("transform", "translateX(0px)");
              $(".rightLst").removeClass("over");
          } else {
            $('.rightLst').click();
          }
        }, 2000);
        // jQuery(".custom-option").click(function(){
        //   var area_value = $(this).attr("data-value");
        //   if(area_value!=""){
        //     $("#zipcode").prop('required',true);
        //   }
        // });


      function addMarkerToGroup(group, coordinate, html, image) {
        var icon = new H.map.Icon(image);
        var marker = new H.map.Marker(coordinate, { icon: icon, lat: '<?php echo $latitudeCurrent; ?>', lng: '<?php echo $longitudeCurrent; ?>' });
        marker.setData(html);
        map.setZoom(11);
        group.addObject(marker);
      }
      function addInfoBubble(map) {
        var group = new H.map.Group();
        map.addObject(group);
        group.addEventListener('tap', function (evt) {
        var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
            content: evt.target.getData(),
          });
          ui.addBubble(bubble);
        }, false);
        <?php
        $intpart = '';
        foreach($users as $user_info) {
         if(empty($user_info->profile_image)){
          $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
        }
        else{
         $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
       }
       if($user_info->latitude!='' ){
        $email = $user_info->email;
        $cat_image = $user_info->cat_image;

        if(!empty(Auth::user())){
          $userblock = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id','=', $user_info->p_id)->where('block_status','=','1')->first(); 
         //print_r($userblock);
          if(!empty($userblock)) {
            $image =" ";
          }else {
            if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }
          }
        }else{
          if($cat_image==''){
              $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
            }else {
              $image = url('/public')."/images/categoryimage/".$cat_image;
            }

        }

        ?>
        addMarkerToGroup(group, {lat:'<?php echo $user_info->latitude; ?>', lng:'<?php echo $user_info->longitude; ?>'},
          '<div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodytext"><a class="user_txt" href="<?php echo url('/');?>/profile/<?php echo $user_info->p_id; ?>" ><?php echo ucfirst($user_info->username); ?></a>' +
          '<div><span class="mapheading">Cidade :</span> <?php echo $user_info->city; ?></div><div><span class="mapheading"> Serviço: </span> <?php echo $user_info->cat_name; ?></div></a><div class="mapbodyrating"><span class="mapheading"><?php if($user_info->profile_rating){
            $rating = number_format($user_info->profile_rating,1);
            $intpart = floor ( $rating );
            $fraction = $rating - $intpart;
            for($x=1;$x<=$user_info->profile_rating;$x++) { ?> <i class="fa fa-star"></i> <?php } if (strpos($user_info->profile_rating,'.')) {?> <i class="fa fa-star-half-o" aria-hidden="true"></i> <?php $x++; }  while($x <= 5) {?> <i class="fa fa-star-o"></i> <?php  $x++; } } else { for($z=1;$z<=5;$z++) { ?> <i class="fa fa-star-o"></i> <?php }  }?> </span><a href="<?php echo url('/');?>/profile/<?php echo $user_info->p_id; ?>" class="raitingread">Mais info</a></div></div></div>','<?php echo $image; ?>');
          <?php
          }
        } ?>
      }

      var platform = new H.service.Platform({
        app_id: 'devportal-demo-20180625',
        app_code: '9v2BkviRwi9Ot26kp2IysQ',
        useHTTPS: true
      });

      var circle = new H.map.Circle({lat: 52.51, lng: 13.4}, 8000);

      // Add the circle to the map:
      var pixelRatio = window.devicePixelRatio || 1;
      var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320

      });

      var map = new H.Map(document.getElementById('map'),
        defaultLayers.normal.map,{
          center: {lat: '<?php echo $latitude_center; ?>', lng: '<?php echo $longitude_center; ?>'},
          zoom: 7,
          pixelRatio: pixelRatio
        });
      var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
      var ui = H.ui.UI.createDefault(map, defaultLayers);

    addInfoBubble(map);
    map.addObject(circle);
/*---------close map info  on click out side of it --------------------------*/

    $(document).mouseup(function(e)
    {
        var container = $(".H_ib_body");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });
    /***********************/
      //for zipcode format
      document.getElementById("zipcode").onkeyup = function(){
      this.value = this.value.replace(/^(\d{5})(\d{3}).*/, '$1-$2');
      };
      /***********************************/

/*----------------------------------------------------------------------------*/
      $("#serviceform").validate({
        rules: {
          cat: {
            required: true
          }
          },
          messages: {

            cat: {
              required: "Por favor, selecione a categoria."
            }
          },
          submitHandler: function(form) {
            var APP_URL = {!! json_encode(url('/search')) !!}

            var cat = jQuery('#cat').val();
            var zipcode = jQuery('#zipcode').val();
            $.ajax({
              url: "{{ url('/search/infoexist') }}",
              type: 'POST',
              data: {'_token': "{{ csrf_token() }}", 'cat': cat, 'zipcode': zipcode},
              success: function(response) {
                  $("#serviceform").attr('action',APP_URL);
                  form.submit();
                /*if(response==1){
                  $("#serviceform").attr('action',APP_URL);
                  form.submit();
                } else { 
                  alert("Entered information doesn't match. Please try again!");
                }*/
              }
            });
          }
        });
  /*----------------------------------------------------------------------------*/       
  //Home-page-dropdorw

    $(".custom-select").each(function() {
      var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
      var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
      template += '</div></div>';

      $(this).wrap('<div class="custom-select-wrapper"></div>');
      $(this).hide();
      $(this).after(template);
    });
/*----------------------------------------------------------------------------*/
    $(".custom-option:first-of-type").hover(function() {
      $(this).parents(".custom-options").addClass("option-hover");
    }, function() {
      $(this).parents(".custom-options").removeClass("option-hover");
    });
/*----------------------------------------------------------------------------*/
    $(".custom-select-trigger").on("click", function(event) {
      $('html').one('click',function() {
        $(".custom-select").removeClass("opened");
      });
      $(this).parents(".custom-select").toggleClass("opened");
      event.stopPropagation();
    });
/*----------------------------------------------------------------------------*/
    $(".custom-option").on("click", function() {
      $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
      $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
      $(this).addClass("selection");
      $(this).parents(".custom-select").removeClass("opened");
      $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
    });
/*validation for mail subscription*/
      $('#mc-embedded-subscribe-form').validate({
        rules: {
          email: {
            required: true,
            email:true
            }
       },
      message:{
        email:{
          required:"This field is Required",
          email:"Please Enter Valid format"
        }
      },
     
      submitHandler: function(form) {
      }
      });
        /* end script validation*/

});

</script>
<script src="{{ url('/public') }}/js/home_page_slider.js"></script>

        </main>

        @stop
