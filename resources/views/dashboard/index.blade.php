@extends('layouts.default-header')

@section('title', 'Dashboard')

@section('content')
 @if($role == 'Provider')
         @if($spavailabilityInfo->isCompleted=='0') 
      <div class="alert alert-danger alert-dismissible fade in profle_alert">
       <!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
       <strong>Alert!!</strong> Your Availability is not complete. Please  <a href="{{ url('/')}}/serviceprovider/spavailablty"> click here </a> to complete.
     </div>
     @endif
     @endif
<section class="maincontent searchbarbg">
  <div class="row">

    <div class="col-md-12">

      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid">
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation" class="active"><a href="#dashboard" aria-controls="dashboard" role="tab" data-toggle="tab">Dashboard</a></li>
              <li role="presentation"><a href="#bookingmsg" aria-controls="bookingmsg" role="tab" data-toggle="tab"> Bookings & Messages</a></li>
              <!--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Past</a></li>-->
              <!--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Contacts</a></li>-->
              <li role="presentation"><a href="#account" aria-controls="settings" role="tab" data-toggle="tab">Account</a></li>
              @if($user_info->user_role!='')
              <li role="presentation"><a href="#switch" aria-controls="switch" role="tab" data-toggle="tab">Switch Dashboard</a></li>
              @endif
            </ul>
          </div>
        </div>
        @include('layouts.flash-message')
        <?php
        $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;
        ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="dashboard">
            <div class="istbox">
              <div class="container">
                <div class="row">
                  <div class="col-sm-8 full_box">
                    <div class="profile_box">
                      <div class="row  box-clr">
                        <div class="col-sm-4 text-center">
                          @if(!empty($user_info->profile_image ))
                          <img class="profimg" src="{{url('public/images/profileimage')}}/<?php echo $user_info->profile_image; ?>" alt="dashboardimg" width="100%">
                          @else
                          <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="profimg" alt="dashboardimg" width="100%">
                          @endif
                        </div>
                        <div class="col-sm-8 text_content">
                          <h3>{{ucfirst($user_info->name)}}</h3>
                          <ul class="style-chnage">
                            <li><span> Address: </span>{{$user_info->address}}</li>
                            <li><span> Email Address:</span> {{$user_info->email}} </li>
                            <li><span> Phone Number: </span>{{$user_info->phone}}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="topicon">
                      <?php
                        if($user_info->user_role == 'Provider'){
                          $url = '/serviceprovider/profile';
                        }
                        else{
                            $url = '/servicetaker/profile';
                        }
                       ?>
                      <a href="{{url($url)}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon2.png" alt="dashboard_designi" width="100%" class="img-responsive"></a>
                    </div>
                  </div>

                  @if($role == 'Provider')

                  <?php 
                    $main_category_detail = DB::table('main_category')->where('id', Auth::user()->cat_id)->first();
                    $sub_category_detail = DB::table('category')->where('cat_id', Auth::user()->sub_id)->first();
                  ?>
                  <div class="col-sm-4 baby_box">
                    <div class="singlepagesidebar">
                      <div class="row">
                        <div class="col-sm-6">
                        <img src="{{url('/public/images/main_category/'.$main_category_detail->cat_image)}}">{{$main_category_detail->name}}
                        </div>
                        <div class="col-sm-6">
                        <img src="{{url('/public/images/categoryimage/'.$sub_category_detail->cat_image)}}">{{$sub_category_detail->cat_name}}
                        </div>
                      </div>
                      <h3>Pricing</h3>
                      <table class="table pricing_table">
                        <tbody>
                          @if(count($provider_price_lists) > 0)
                          @foreach($provider_price_lists as $provider_price_list)
                          <tr>
                           <th>{{$provider_price_list->queries}}</th>
                           <td>${{$provider_price_list->amount}}</td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                  @endif

                </div>
              </div>
            </div>
            <div class="istbox">
              <div class="container">
                <div class="row">
                  <div class="col-sm-8">
                      <h3>All Babysitter include:</h3>
                          <div class="singlepagesidebar">
                      <ul class="right_icon">
                        <li><img src="http://navizinhanca.com/public/images/babysittericon1.png">24/7 Emergency support &amp; cancellation protection</li>
                        <li><img src="http://navizinhanca.com/public/images/babysittericon2.png">Up to 1.7M pet &amp; liability insurance</li>
                        <li><img src="http://navizinhanca.com/public/images/babysittericon3.png">Free GPS tracked walks</li>
                        <li><img src="http://navizinhanca.com/public/images/babysittericon4.png">Photo &amp; video updates</li>
                      </ul>
                    </div>

<!-- 
                    <div class="box">
                      <p><span class="alert">Alerts</span>(You are just a few steps away from completing your profile!)</p>
                      <ul class="right_icon">
                        <li><i class="fa fa-share ico"></i> Add your card now for easy payment later - sitters only accept payment through DogBuddy</li>
                        <li><i class="fa fa-share ico"></i> Add a great profile pic of yourself!</li>
                        <li><i class="fa fa-share ico"></i> Create a profile for your dog (this helps you contact sitters effectively)</li>
                        <li><i class="fa fa-share ico"></i> Add a great profile pic of yourself!</li>
                      </ul>
                  </div> -->
                </div>
                <div class="col-sm-4 baby_box">

                  <div class="singlepagesidebar">
                    <div class="box_div invite_box">
                      <img class="inviteimg inving" src="http://navizinhanca.com/public/images/14_dashboard_design.icon1.png" alt="design" width="100%">
                      <div class="paid">
                        <h3>Invite A Friend and Get Paid</h3>
                      </div>
                      <form class="form-inline inviteform dashboardinviteform" id="inviteform">
                        <div class="form-group nopadding nopadding copybtn">

                          <input class="col-xs-9" id="myInput" value="http://navizinhanca.com/invite-friend/{{$user_info->referal_code}}" type="text">

                          <input class="btn btn-default col-xs-3 redme" value="Copy" onclick="myFunction_copy()" type="button">
                        </div>
                      </form>
                      <h3><strong>Your friends get a £10 discount</strong></h3>
                      <h4><strong class="sky_clr">You get £10 off your next booking!</strong></h4>
                      <div class="doler_box">
                        <h4>£0.00</h4>
                        <p>Current Cradit Balance to Spend</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       
        <div role="tabpanel" class="tab-pane codeRefer" id="bookingmsg">

          <div class="bookingmsg_sec">
            <div class="container">
              <div class="bookmsgsec">
                <h4>Bookings & Message</h4>
                <div class="bookmsgtab" id="">
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Pending</a></li>
                    <li role="presentation" class="active"><a href="#confirmed" aria-controls="confirmed" role="tab" data-toggle="tab">Confirmed</a></li>
                    <li role="presentation"><a href="#past" aria-controls="past" role="tab" data-toggle="tab">Decline</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="pending">
                          <div class="tablewrapper tablediv">
                             <div class="alert alert-info alert-dismissible   statusAlerts" style="display:none;">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </div> 
                           
                            @if($booking_payment_pending_count>0)
                            <table class="table-bordered table-striped">
                              <thead class="tableheader">
                                <tr class="headings">
                                  <th class="">Name</th>
                                  <th class="">Message</th>
                                  <th class="">Requirement</th>
                                  <th class="">Start Date</th>
                                  <th class="">End Date</th>
                                  <th class="">Total Amount</th>
                                  @if($role =='Provider')
                                  <th class="">Action</th>
                                  @endif
                                </tr>
                              </thead>
                              <tbody class= "data_fill_pending">
                                @foreach($booking_payment_pending as $booking_payment_pending_info)
                                <?php
                                $payment_pendinguser_book_by = $booking_payment_pending_info->user_id;
                                $payment_pendingbooking_message = $booking_payment_pending_info->booking_message;
                                $payment_pendingbooking_amount = $booking_payment_pending_info->booking_amount;
                                $payment_pendingcategory = $booking_payment_pending_info->category;
                                $payment_pendingbooking_start_time = date('F j, Y ', strtotime($booking_payment_pending_info->booking_start_time));
                                $payment_pendingbooking_end_time = date('F j, Y ', strtotime($booking_payment_pending_info->booking_end_time));
                                $payment_pendingmuser_book_to = $booking_payment_pending_info->profile_id;
                                if($role == 'Provider'){
                                  $paymentuser_info = DB::table('users')
                                  ->where('id', $payment_pendinguser_book_by)
                                  ->first();
                                } else {
                                  $paymentuser_info = DB::table('users')
                                  ->where('id', $payment_pendingmuser_book_to)
                                  ->first();
                                }
                                $paymentname = $paymentuser_info->name;
                                $paymentprofile_image = $paymentuser_info->profile_image;
                                if($paymentprofile_image==''){
                                  $payment_pendinguserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                                }
                                else{
                                  $payment_pendinguserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                                }
                                ?>
                                <tr class="tabledata confirmedtable" id= "pending_{{ $booking_payment_pending_info->booking_id  }}">
                                  <td class="username "><h4>
                                    @if($paymentprofile_image=='')
                                    <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                    @else
                                    <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>">
                                    @endif
                                    <span>{{ ucfirst($paymentname) }}</span></h4></td>
                                    <td class="userdesc "><p>{{ mb_substr($payment_pendingbooking_message, 0, 50) }}</p></td>
                                    <td class="">{{ $payment_pendingcategory }}</td>
                                    <td class="">{{ $payment_pendingbooking_start_time }}</td>
                                    <td class="">{{ $payment_pendingbooking_end_time }}</td>
                                    <td class="">{{ '$'.$payment_pendingbooking_amount }}</td>
                                     @if($role =='Provider')
                                    <td class="">
                                       <form method="POST" id= "booking_status"> 
                                          <input type="hidden" class="bookId" name="booking_id" value="{{ $booking_payment_pending_info->booking_id }}">
                                           <input type="hidden" class="book_token" name="_token" value="{{ csrf_token() }}" required="required">
                                       
                                           <button class="btn btn-success btn-sm status_submit" name="approve_status" type="submit" value="1">Approve</button>
                                            <button class="btn btn-danger btn-sm status_submit" name="approve_status" type="submit" value="2">Deny</button>
                                         </form>
                                          
                                    </td>
                                    @endif


                                  </tr>
                                  @endforeach

                                </tbody>
                              </table>
                              @else
                              <tr>
                                <h2>No Pending Booking request for you.</h2></tr>
                                @endif
                              </div>
                            </div>
                    <!--confirmed-modal-->
                    <div role="tabpanel" class="tab-pane active" id="confirmed">
                      <div class="tablewrapper tablediv">
                        @if($booking_payment_confirm_count>0)
                        <table class="table-bordered table-striped">
                          <thead class="tableheader">
                            <tr class="headings">
                              <th class="column4">Name</th>
                              <th class="column5">Message</th>
                              <th class="column3">Requirement</th>
                              <th class="column3">Start Date</th>
                              <th class="column2">End Date</th>
                              <th class="column3">Total Amount</th>
                            </tr>
                          </thead>
                          <tbody class= "data_fill_confirm">
                            @foreach($booking_payment_confirm as $booking_payment_confirm_info)
                            <?php
                            $payment_confirmuser_book_by = $booking_payment_confirm_info->user_id;
                            $payment_confirmbooking_message = $booking_payment_confirm_info->booking_message;
                            $payment_confirmbooking_amount = $booking_payment_confirm_info->booking_amount;
                            $payment_confirmcategory = $booking_payment_confirm_info->category;
                            $payment_confirmbooking_start_time = date('F j, Y ', strtotime($booking_payment_confirm_info->booking_start_time));
                            $payment_confirmbooking_end_time = date('F j, Y ', strtotime($booking_payment_confirm_info->booking_end_time));
                            $payment_confirmuser_book_to = $booking_payment_confirm_info->profile_id;
                            if($role == 'Provider'){
                              $paymentuser_info = DB::table('users')
                              ->where('id', $payment_confirmuser_book_by)
                              ->first();
                            } else {
                              $paymentuser_info = DB::table('users')
                              ->where('id', $payment_confirmuser_book_to)
                              ->first();
                            }

                            $paymentname = $paymentuser_info->name;
                            $paymentprofile_image = $paymentuser_info->profile_image;
                            if($paymentprofile_image==''){
                              $payment_confirmuserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                            }
                            else{
                              $payment_confirmuserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                            }
                            ?>
                            <tr class="tabledata confirmedtable confirm_{{ $booking_payment_confirm_info->booking_id }}">
                              <td class="username column4"><h4>
                                @if($paymentprofile_image=='')
                                <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                @else
                                <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>">
                                @endif
                                <span>{{ ucfirst($paymentname) }}</span></h4></td>
                                <td class="userdesc column5"><p>{{ mb_substr($payment_confirmbooking_message, 0, 50) }}</p></td>
                                <td class="column3">{{ $payment_confirmcategory }}</td>
                                <td class="column3">{{ $payment_confirmbooking_start_time }}</td>
                                <td class="column2">{{ $payment_confirmbooking_end_time }}</td>
                                <td class="column3">{{ '$'.$payment_confirmbooking_amount }}</td>
                              </tr>
                              @endforeach

                            </tbody>
                          </table>
                          @else
                          <tr>
                            <h2>No Status Confirmed List Found.</h2></tr>
                            @endif
                          </div>
                        </div>

                        
                            <div role="tabpanel" class="tab-pane" id="past">
                              <div class="tablewrapper tablediv">
                                @if($booking_payment_past_count>0)
                                <table class="table-bordered table-striped">
                                  <thead class="tableheader">
                                    <tr class="headings">
                                      <th class="column4">Name</th>
                                      <th class="column5">Message</th>
                                      <th class="column3">Requirement</th>
                                      <th class="column3">Start Date</th>
                                      <th class="column2">End Date</th>
                                      <th class="column3">Total Amount</th>
                                    </tr>
                                  </thead>
                                  <tbody class= "data_fill_reject">
                                    @foreach($booking_payment_past as $booking_past_pending_info)
                                    <?php
                                    $payment_pastuser_book_by = $booking_past_pending_info->user_id;
                                    $payment_pastbooking_message = $booking_past_pending_info->booking_message;
                                    $payment_pastbooking_amount = $booking_past_pending_info->booking_amount;
                                    $payment_pastcategory = $booking_past_pending_info->category;
                                    $payment_pastbooking_start_time = date('F j, Y ', strtotime($booking_past_pending_info->booking_start_time));
                                    $payment_pastbooking_end_time = date('F j, Y ', strtotime($booking_past_pending_info->booking_end_time));
                                    $payment_pastmuser_book_to = $booking_past_pending_info->profile_id;
                                    if($role == 'Provider'){
                                      $paymentuser_info = DB::table('users')
                                      ->where('id', $payment_pastuser_book_by)
                                      ->first();
                                    } else {
                                      $paymentuser_info = DB::table('users')
                                      ->where('id', $payment_pastmuser_book_to)
                                      ->first();
                                    }
                                    $paymentname = $paymentuser_info->name;
                                    $paymentprofile_image = $paymentuser_info->profile_image;
                                    if($paymentprofile_image==''){
                                      $payment_pastuserimage = "{{ url('/public/images/') }}/gravtar.jpeg";
                                    }
                                    else{
                                      $payment_pastuserimage = "{{ url('/public/images/') }}/profileimage".'/'.$paymentprofile_image;
                                    }
                                    $current_date = date("Y-m-d H:i:s");
                                    if(strtotime($current_date)>strtotime($booking_past_pending_info->booking_end_time)){
                                      ?>
                                      <tr class="tabledata confirmedtable past_{{ $booking_payment_confirm_info->booking_id }}">
                                        <td class="username column4"><h4>
                                          @if($paymentprofile_image=='')
                                          <img src="{{ url('/public/images/') }}/gravtar.jpeg">
                                          @else
                                          <img src="{{ url('/public/images/') }}/profileimage/<?php echo $paymentprofile_image; ?>">
                                          @endif
                                          <span>{{ ucfirst($paymentname) }}</span></h4></td>
                                          <td class="userdesc column5"><p>{{ mb_substr($payment_pastbooking_message, 0, 50) }}</p></td>
                                          <td class="column3">{{ $payment_pastcategory }}</td>
                                          <td class="column3">{{ $payment_pastbooking_start_time }}</td>
                                          <td class="column2">{{ $payment_pastbooking_end_time }}</td>
                                          <td class="column3">{{ '$'.$payment_pastbooking_amount }}</td>
                                        </tr>
                                        <?php
                                      }
                                      ?>
                                      @endforeach

                                    </tbody>
                                  </table>
                                  @else
                                  <tr>
                                    <h2>No Any Booking List Found</h2></tr>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="account">
                      <div class="container">
                        <div class="accounttabs">
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#profileinfo" aria-controls="profileinfo" role="tab" data-toggle="tab">Profile Information<span>Add Profile Information</span></a></li>
                            <li role="presentation"><a href="#carddetail" aria-controls="carddetail" role="tab" data-toggle="tab">Add Your card Detail<span>&nbsp;&nbsp;</span></a></li>
                            <li role="presentation"><a href="#notifications-tabs" aria-controls="notifications-tabs" role="tab" data-toggle="tab">Notifications<span>Data Sharing</span></a></li>
                            <li role="presentation"><a href="#closeacc" aria-controls="closeacc" role="tab" data-toggle="tab">Close your Account<span>Reason to close account</span></a></li>
                          </ul>
                          <div class="tab-content profiletabss">
                            <div role="tabpanel" class="tab-pane active" id="profileinfo">
                              <div class="acccompletion_tabs">
                                <!--<ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"  @if (!Session::has('success'))
                                class="active"
                                @endif><a href="#profile" aria-controls="profile" class="one" role="tab" data-toggle="tab">Profile</a></li>
                                @if($user_role=='Provider')
                                <li role="presentation" @if (Session::has('success'))
                                class="active"
                                @endif ><a href="#services" aria-controls="services" role="tab" data-toggle="tab" class="three">Services</a></li>@endif
                              </ul>-->

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane @if (!Session::has('success'))
                                active
                                @endif" id="profile">
                                <form class="completionform" id="completionform" method="post" action="{{ url('/dashboard/update_account/') }}"  enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <div class="row">
                                    <div class="col-sm-4 profileimage">
                                      <div class="form-group imagediv" >
                                        <div class="imagebrowse" id="previewdiv">
                                          <?php
                                          if($profile_image==''){
                                            ?>
                                            <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="profimg-img" style="width: 100%;height:100%;">
                                            <?php
                                          }
                                          else {
                                            ?>
                                            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-circle" style="width: 100%;height:100%;">
                                            <?php
                                          }
                                          ?>
                                          <div class="nopadding imgbtns">
                                            <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i>
                                          </div>
                                          <div class="prflinput">
                                            <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
                                          </div>
                                        </div>
                                        <h5 class="usrnam">{{ $user_info->name }}</h5>
                                        <h6 class="usreml">{{ $user_info->email }}</h6>
                                      </div>
                                    </div>
                                    <div class="col-sm-8 profileformmainright">
                                      <div class="row">
                                        <div class="col-sm-6 profileform_right">
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Name</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <input type="text" name="name" id="name" class="form-control" value="{{ $user_info->name }}" placeholder="" readonly>
                                                <!--<i class="fa fa-check name"></i>-->
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Email Address</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <input type="email" name="email" id="email" class="form-control" value="{{ $user_info->email }}" placeholder="" readonly>
                                                <!--<i class="fa fa-check email"></i>-->
                                                <input type="hidden" name="user_id" value="{{ $user_info->id }}">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Phone Number</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <input type="text" name="phone" id="phone" class="form-control" value="{{ $user_info->phone }}" placeholder="" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');" readonly>
                                                <!--<i class="fa fa-check phone"></i>-->
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">User Role</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <select class="form-control" name="user_role" disabled>
                                                  <option vlaue="">Select your option</option>
                                                  <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Provider</option>
                                                  <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Taker</option>
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Additional Details</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <input type="text" name="additionaldetail" id="additionaldetail" class="form-control" value="{{ $user_info->additional_details }}" placeholder="" readonly>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                        <div class="col-sm-6 profileform_right">
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Bio</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <textarea name="bio" id="bio" class="form-control" readonly><?php echo $user_info->bio; ?></textarea>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Address</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <textarea name="address" id="address" class="form-control" readonly><?php echo $user_info->address; ?></textarea>
                                                <!--<i class="fa fa-check address"></i>-->
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Zip Code</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $user_info->zipcode; ?>" placeholder="" readonly>
                                                <!--<i class="fa fa-check zipcode"></i>-->
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                            <div class="form-group profileform_right">
                                              <label class="col-sm-6">Country</label>
                                              <div class="profileinput col-sm-6 nopadding">
                                                <select class="form-control" name="country" disabled>
                                                  <option value=""></option>>
                                                  <?php
                                                  foreach($countries as $country_name){
                                                    ?>
                                                    <option value="<?php echo $country_name; ?>" <?php if($user_info->country==$country_name) { echo 'selected';} ?>><?php echo $country_name; ?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                      <div class="col-sm-12 form-group forminput profileform_right radimate">
                                        <label class="col-sm-3">Upload Your Gallery Images</label>
                                        <div class="col-sm-9 browseimage nopadding" id="browse_img">
                                          <div class="">
                                            @if(count($user_image) > 0)
                                            @foreach ($user_image as $user_images)
                                            <!-- <div class="col-sm-3 single_img" id="prv_{{ $user_images->image_id }}"> -->
                                              <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a>
                                              <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" style="margin-bottom: 10px !important;">
                                            <!-- </div> -->

                                            @endforeach
                                            @else
                                            <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                                            @endif
                                          </div>
                                        </div>

                                        <label class="col-sm-3"></label>
                                        <div class="col-sm-9 col-xs-9 new_up_sec nopadding">
                                          <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
                                          <div class="browse_btn" id="browsebtn">
                                            <span>Select Images</span>

                                          </div>
                                        </div>

                                      </div>
                                      <div class="col-sm-12 nextbtn text-right">
                                        <input type="submit" class="form-control" value="Save">
                                      </div>
                                    </div>
                                  </div>
                                </form>


                              </div>
                              <style>
                              .nopadding input {
                                width: auto;
                              }
                            </style>
                            <div role="tabpanel" class="tab-pane @if (Session::has('success')) active @endif" id="services" >
                              <div class="col-sm-12 form-group profileform_right">
                                <form name="complete_service" id="complete_service" class="complete_service" action="{{ url('/accountcomplete/service') }}" method="post">
                                  {{ csrf_field() }}
                                  <?php
                                  $user_cateogry = $user_info->category;
                                  foreach($category as $category_name){
                                    $categrry_name = $category_name->cat_name;
                                    ?>
                                    <div class="col-sm-3 form-group">
                                      <label class="col-sm-11"><?php echo ucfirst($category_name->cat_name); ?></label><div class="profileinput col-sm-1 nopadding"><input type="checkbox" name="cat_name[]" value="<?php echo $category_name->cat_name; ?>" <?php if (strpos($user_cateogry, $categrry_name) !== false) {
                                        echo 'checked';
                                      } ?> data-trigger="focus"></div>
                                    </div>
                                    <?php
                                  }
                                  ?>
                                  <div class="col-sm-12 nextbtn">
                                    <input class="form-control" value="Save" type="submit">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane subtabcontent" id="carddetail">
                        <!-- <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class=""><a href="#profile" class="two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"></a></li>
                        <li role="presentation" class="active"><a href="#profile" class="one" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Add Your Card Details</a></li>
                      </ul>-->

                      <div class="alert alert-success alert-block success_show">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Account Info Saved Successfully!</strong>
                      </div>
                      <div class="tabcontentbg">
                        <form class="carddetailform" id="carddetailform" method="post" action="#">
                          {{ csrf_field() }}
                          <div class="form-group">
                            <label class="col-sm-4">Credit/debit card number</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="cardnumber" name="card_num" placeholder="4704 - 1258 - 3698 - 1235" value="{{ $user_info->cardnumber }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4">Name on Card</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="cardname" name="paymentname" placeholder="" value="{{ $user_info->cardname }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-4">Expiry Date</label>
                            <div class="col-sm-8">
                              <select class="selectmonth" name="exp_month">
                                <option value="">Select Month</option>
                                <?php
                                $exp_month = $user_info->exp_month;
                                for ($i = 1; $i <= 12; ++$i) {
                                  ?>
                                  <option value="<?php
                                  echo $i;
                                  ?>" <?php
                                  if ($exp_month == $i) {
                                    echo 'selected';
                                  }
                                  ?>><?php
                                  echo $i;
                                  ?></option>
                                  <?php
                                }
                                ?>
                              </select>
                              <select class="selectyears" name="exp_year">
                                <option value="">Select Years</option>
                                <?php
                                $exp_year = $user_info->exp_year;
                                for ($i = 2018; $i <= 2035; ++$i) {
                                  ?>
                                  <option value="<?php echo $i; ?>" <?php if ($exp_year == $i) {
                                    echo 'selected';  } ?>><?php echo $i; ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4">Security code (CVV/CVC)</label>
                              <div class="col-sm-8">
                                <input type="text" name="cvv" value="{{ $user_info->cvv }}" id="cvv" class="cvv">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4"></label>
                              <div class="col-sm-8 nextbtn">
                                <!-- <input type="submit" class="btn btn-default" value="Add Card"> -->
                                <input type="submit" class="form-control" value="Add Card">
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="notifications-tabs">
                        <div class="tabcontentbg">
                          <div class="tablewrapper tablediv">
                            <!--====notication code start--==-->
                            <div class="notificationbox">
                              <div class="alert alert-success alert-block success_show">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>Notification deleted successfully!</strong>
                              </div> 

                              <div class="firstnotification wow animated fadeInLeft">

                                @if(count($listresult)>0)

                                  @foreach($listresult as $userInfo)
                                    <?php
                                    $user_book_by = $userInfo->user_id;
                                    $booking_message = $userInfo->booking_message;
                                    $booking_time = date('F j, Y ', strtotime($userInfo->booking_time));
                                    $user_book_to = $userInfo->profile_id;
                                    if($role == 'Provider'){
                                      $bookingUserInfo = DB::table('users')
                                      ->where('id', $user_book_by)
                                      ->first();
                                    } else {
                                      $bookingUserInfo = DB::table('users')
                                      ->where('id', $user_book_to)
                                      ->first();
                                    }
                                    $username = $bookingUserInfo->name;
                                    $profile_image = $bookingUserInfo->profile_image;
                                    ?>
                                    <div class="main_notificationData main_{{ $userInfo->notify_id }}">
                                      <div class="crossbtnnoti">
                                        <span  ><a class="deletebutton" style="cursor: pointer;" id="{{ $userInfo->notify_id }}" ><img src="{{ url('/') }}/public/images/crossimages.png" alt="Delete" style="width: 16px;" /></a></span>                           
                                      </div>
                                      <div class="notifidata">
                                        <button class="btn btn-success join mess"> Booking</button>
                                        <span><img src="http://navizinhanca.com/public/admin/images/timer.png" alt="#"> {{ $booking_time }}</span>
                                        <div class="media">
                                          <div class="media-left">
                                            @if($profile_image=='')
                                            <img src="{{ url('/public/images/') }}/gravtar.jpeg" class="media-object" style="width:40px">
                                            @else
                                            <img src="{{ url('/public/images/') }}/profileimage/<?php echo $profile_image; ?>" class="media-object" style="width:40px">
                                            @endif
                                          </div>
                                          <div class="media-body">
                                            <h1 class="media-heading" >{{ ucfirst($username) }}</h1>
                                            <h3>Booking Message:{{ mb_substr($booking_message, 0, 50) }}</h3>
                                          </div>
                                        </div>
                                        <hr class="notiboxline">
                                      </div>
                                    </div>
                                  @endforeach
                                @else

                                <h2>No  Notification Found.</h2>
                                @endif
                              </div> 
                            </div>
                            <!--===end notification code --==-->
                            
                          </div> 
                        </div>
                      </div>
                      
                           
                       

                            <div role="tabpanel" class="tab-pane" id="closeacc">
                              <!--Close account-->

                              <div class="">

                                <div class="switch subtabcontent">
                                  <!-- <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class=""><a href="#profile" class="two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"></a></li>
                                  <li role="presentation" class=""><a href="#profile" class="two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"></a></li>
                                  <li role="presentation" class=""><a href="#profile" class="two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"></a></li>
                                  <li role="presentation" class="active"><a href="#profile" class="one" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Request For Close Account</a></li>
                                </ul>-->

                                <div class="alert alert-success alert-block success_request">
                                  <button type="button" class="close" data-dismiss="alert">×</button>
                                  <strong>Your Request Submited Successfully!</strong>
                                </div>
                                <form class="closeaccount" id="closeaccount" method="post" action="#">
                                  {{ csrf_field() }}
                                  <div class="form-group">
                                    <label class="col-sm-4"><h4>Reason</h4></label>
                                    <div class="col-sm-8">


                                      <input type="hidden" id="userid" name="current_userId" value="{{ $user_info->id }}">
                                      <label class="radio_btn_label">Access the benefits of paid subscriptions
                                        <input type="radio" checked="checked" name="close_reason" value="Access the benefits of paid subscriptions">
                                        <span class="checkmark"></span>
                                      </label>
                                      <label class="radio_btn_label" >Personalize your Crunchbase experience
                                        <input type="radio" name="close_reason" value="Personalize your Crunchbase experience">
                                        <span class="checkmark"></span>
                                      </label>
                                      <label class="radio_btn_label">The ability to make edits to the site
                                        <input type="radio" checked="checked" name="close_reason" value="The ability to make edits to the site">
                                        <span class="checkmark"></span>
                                      </label>
                                      <div class="nextbtn">
                                        <input type="submit" class="form-control" value="Submit" id="sub" onClick="confirmMessage();">
                                        <!-- <input class="btn btn-default" value="Submit" type="submit"   id="sub" onClick="confirmMessage();"> -->
                                        </div>
                                      </div>
                                    </div>

                                  </form>
                                </div></div>
                                <!--End Close account-->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--account-tab-->
                      <div role="tabpanel" class="tab-pane subtabcontent" id="switch">
                        <div class="container">
                          <div class="alert alert-success alert-block success_role">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Your role changed successfully!</strong>
                          </div>
                          <div class="switchrolhead">
                            <h6>You can switch Your Role </h6>
                          </div>
                          <div class="switch switchrole">
                            <form class="switchdashboard" id="switchdashboard" method="post" action="#">
                              {{ csrf_field() }}
                              <div class="form-group">
                                <label class="col-sm-4"><h4>User Role</h4></label>
                                <div class="col-sm-4">

                                  <input type="hidden" id="userid" name="current_userId" value="{{ $user_info->id }}">
                                  <div class="radioradio">
                                    <label class="radio_btn_label">Provider
                                      <input type="radio" checked="checked" name="role_switch" value="Provider" @if($user_info->user_role=='Provider') checked @endif >
                                      <span class="checkmark"></span>
                                    </label>
                                  </div>
                                  <div class="radioradio">
                                    <label class="radio_btn_label">Taker
                                      <input type="radio" name="role_switch" value="Taker" @if($user_info->user_role=='Taker') checked @endif>
                                      <span class="checkmark"></span>
                                    </label>
                                  </div>
                                  <!--if($user_info->user_role=='Taker') checked endif-->
                                </div>
                                <div class="col-sm-4 nextbtn">
                                  <input type="submit" class="form-control"  value="Change">
                                  <!-- <input class="btn btn-default change_role" value="Change" type="submit" > -->
                                </div>
                              </div>

                            </form>
                          </div></div></div>


                        </div></div></div></div></div>
                      </section>
  <script type="text/javascript">
  function confirmMessage()
  {
    var retVal = confirm("Are you sure?");
    if( retVal == true ) {
      return true;
    }
    else {
      return false;
    }
  }
  var base_url = '<?php echo url('/');?>';
  $('.change_role').on('click',function(){
    if(confirm("Are you Sure You want to logout By clicking here You are logout form website shortly,you have to login again with changed role")){
       $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
       $('#switchdashboard').submit();
    }
    else{
      return false;
    }
   
  });
  $('.status_submit').on('click', function(e) {
   e.preventDefault(); 
   var status = confirm("Are you sure?");
    if( status == true ) {
     
      var token = $('.book_token').val();
      var bookingId = $('.bookId').val();
      var status = $('.status_submit').val();
      var host = '{{ url('/') }}';
     // alert("#pending_"+bookingId);die;
       $.ajax({
           type: "POST",
           url: host+'/providerbooking/status',
           data: {'booking_id':bookingId,'_token':token,'approve_status':status},
           dataType: "json",
           success: function(response) {
              console.log(response);
                 //alert( msg );
                 
              $(".statusAlerts").show();
              $(".statusAlerts").html(response.msg);
              $("#pending_"+bookingId).hide();
              if(response.status == '1'){
                $("#confirm").focus();
                $(".data_fill_confirm").append(response.html);
              } else if(response.status == '2') {
                $("#past").focus();
                $('.data_fill_confirm').append(response.html);
              
              }else {
                $("#past_"+bookingId).focus();
                $(".data_fill_pending").append(response.html);
              }
              //$("#pending_"+bookingId).load('url',"#pending_"+bookingId);
              setTimeout(function () {
                  $('.statusAlerts').hide();
              }, 1000);
             }
              
        });
      } else {
        return false;
     }
    
});

  </script>
  <script type="text/javascript">
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    if(confirm("Are you sure you want to delete this?")){
      var idd = $(this).attr('id');
      console.log(idd);
      //alert(idd);exit
      'notifications.id',
          $.ajax({
          url: "/dashboard/notifications",
          type: 'POST',
          data: {'id':idd,"_token": "{{ csrf_token() }}" },
          success: function(response) {
          jQuery('.success_show').show();
          jQuery('.main_'+idd).remove();
          }            
          });      
      }
});
</script>
  @stop
