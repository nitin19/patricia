<html>
   
   <head>
      <title>Dashboard</title>
	   <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">                           
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   
   <body>
   
	
				<!--counter text-add-store-heading --> 
			<div class="errer-page">    
				<div class="row">
					<div class="col-sm-12 errer-page-505">    
						<div class="">  
	  						<h1>500</h1>    
	  						<h5>Oops, This Page Not Be Found!</h5>    
	  						<p>We are really sorry but the page you requested is missing :(</p>    
	  						<a href="{{url('/')}}" class="help-link btn btn-primary" role="button">Back To Home</a>   
  						</div>     
					</div>
  				</div>
          <!-- <div class="row">
            <div class="col-sm-12 errer-page-505">    
              <div class="">  
                <span>For Developers</span>
                <p>{{$customerror}}</p>    
              </div>     
            </div>
          </div> -->
			</div>	  

  
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	  <script type="text/javascript">
		$(window).on('load', function(){
			$(".loader").fadeOut("slow");
		});
		</script>

   </body>
</html>  
  <style>	
  body {padding:0px;margin:0px;}
.errer-page {
    background-color: #eef2f6;
    height: 100%;
}
.errer-page-505 {  
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    text-align: center;
}
.errer-page-505 h1 {
    color: #07a8de;
    font-size: 150px;
    font-weight: bold;
    margin-bottom: -15px;  
}  
.errer-page-505 p {
    font-weight: 300;
    font-size: 17px;
    color: #555555;
    margin-bottom: 5px;  
}
.errer-page-505 h5 {
    font-size: 32px;
    font-weight: 500;
    color: #555555;
}    
.help-link {
    color: #07a8de;
    font-size: 20px;
    font-weight: bold;
}  
.help-link:hover {  
    color: #07a8de;
    font-size: 20px;
    font-weight: bold;
}    

a.help-link.btn.btn-primary {
    background-color: #07a8de;
    border-color: #07a8de;
    color: #fff !important;
}

footer.add_list_footer {
    padding: 20px 30px 0px;
}  
footer.add_list_footer p {
    font-size: 14px;
}  
footer.add_list_footer .Binary_design-p {
    text-align: right;
}  
footer.add_list_footer .Binary_design-p a {
    color: #0099cc;
    text-decoration: none;
}  
</style>
  