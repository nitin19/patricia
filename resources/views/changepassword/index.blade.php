@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')

@include('layouts.flash-message')

<div class="formsection">
 
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg">
   
                        <form class="form-horizontal registerform formarea" method="POST" id="changepassword" action="{{ url('/changepassword/update') }}">
                            <div class="message">
                                @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                                @endif
                            </div>
                        
                        {{ csrf_field() }}
                        <h2 class="text-center  flipInY">Alterar a senha </h2> 
                        <p class="text-center  fadeInLeft">Para alterar sua senha, digite as informações abaixo.</p>
                        <div class="form-group forminput  fadeInLeft">
                        <input id="oldpassword" type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Senha antiga" required autofocus>
                       <span toggle="#oldpassword" class="fa fa-fw fa-eye toggle-password">
                        </div>
                     <!--  <div class="form-group forminput  fadeInLeft">
                        <input id="confirm_oldpassword" type="password" class="form-control" name="confirm_oldpassword" id="confirm_oldpassword" placeholder="Confirm Old Password" required>
                        <span toggle="#confirm_oldpassword" class="fa fa-fw fa-eye field-icon toggle-password">
                        </div> -->

                        <div class="form-group forminput  fadeInLeft">
                        <input id="newpassword" type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Nova senha" required>
                        <span toggle="#newpassword" class="fa fa-fw fa-eye toggle-password"></span>
                        </div>

                     <div class="form-group forminput  fadeInLeft">
                     <input type="password" class="form-control" name="newpassword_confirmation" id="newpassword_confirmation" placeholder="Confirme a nova senha" required>
                     <span toggle="#newpassword_confirmation" class="fa fa-fw fa-eye toggle-password">
                     </div>
                    <div class="form-group buttondiv">
                    <input type="submit" class="btn btn-primary btn-lg" value="Enviar">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
/*$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*#$%^&]*$/.test(value)
       && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[!@*#$%^&]/.test(value)
       && /\d/.test(value)
});*/
    jQuery('#changepassword').validate({
         rules: {
            oldpassword: {  
                required: true,
               // pwcheck: true,
                minlength: 6,
                maxlength: 15  
            },
            // confirm_oldpassword: {
            //     minlength: 6,
            //     maxlength: 15, 
            //     equalTo: "#oldpassword"
            // },
            newpassword: {  
                required: true,
                //pwcheck: true,
                minlength: 6,
                maxlength: 15   
            },
            newpassword_confirmation: {
                required: true,
                minlength: 6,
                maxlength: 15,   
                equalTo: "#newpassword"
            }
         },
         messages: {
            oldpassword: {
                required: "Favor preencher o campo acima",
                pwcheck: "Old password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "Sua senha antiga tinha ao menos 6 dígitos/letras. ",
                maxlength: "Sua senha antiga tinha ao menos 15 dígitos/letras. "
            },
            // confirm_oldpassword: {
            //     minlength: "Confirm old password length minimum 6 character",
            //     maxlength: "Confirm old password length minimum 15 character",
            //     equalTo: "Confirm old password does not match with old password"
            // },
            newpassword: {
                required: "Favor preencher o campo acima",
                pwcheck: "New password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "A senha tem que ter ao menos 6 dígitos/letras. ",
                maxlength: "A senha tem que ter ao menos 6 dígitos/letras."
            },
            newpassword_confirmation: {
                required: "Todos os campos são obrigatórios.",
                minlength: "A senha tem que ter ao menos 6 dígitos/letras.",
                maxlength: "A senha tem que ter ao menos 15 dígitos/letras.",
                equalTo: "Divergência nas novas senhas digitadas. As digite novamente, por favor."
            }
         },
    });
$("document").ready(function(){
    setTimeout(function(){
        $("div.alert").remove();
    }, 5000 ); // 5 secs

});
</script>
<style type="text/css">
    .registerformbg.formbg {
    padding: 30px 40px;
}
</style>
@endsection