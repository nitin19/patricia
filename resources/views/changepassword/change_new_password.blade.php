@extends('layouts.default-header')

@section('title', 'Mudar senha')

@section('content')

@include('layouts.flash-message')

<div class="formsection">
  
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg">                 
              <form class="form-horizontal" method="POST" id="changepasswords" action="{{url('/change-new-password/update/'.$email) }}">
                 <input type="hidden" name="_method" value="PUT">
              @if(session()->has('message'))
              <div class="alert alert-success">
                  {{ session()->get('message') }}
              </div>
              @endif
              {{ csrf_field() }}
              <input name="_method" type="hidden" value="PUT">
              <h2 class="text-center  rrtop flipInY">Esqueci minha senha</h2> 
              <!-- <p class="text-center  fadeInLeft">Change Password by entering the information below</p> -->
           

              <div class="form-group forminput  fadeInLeft">
              <input id="newpasswords" type="password" class="form-control" name="newpassword"  placeholder="Nova senha" required>
              <span toggle="#newpasswords" class="fa fa-fw fa-eye field-icon toggle-password" required></span>
              </div>

           <div class="form-group forminput  fadeInLeft">
           <input type="password" class="form-control" name="newpassword_confirmation" id="newpassword_confirmation" placeholder="Confirme a nova senha" required>
           <span toggle="#newpassword_confirmation" class="fa fa-fw fa-eye field-icon toggle-password">
           </div>
           <input type="hidden" name="email" value={{$email}}>
          <div class="form-group buttondiv  flipInX">
          <input type="submit" class="btn btn-primary btn-lg" value="Enviar">
          </div>
          </form>
        </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

  // $(".toggle-password").click(function() {
  //       $(this).toggleClass("fa-eye fa-eye-slash");
  //       var input = $($(this).attr("toggle"));
  //       if (input.attr("type") == "password") {
  //         input.attr("type", "text");
  //       } else {
  //         input.attr("type", "password");
  //       }
  //     });
// $.validator.addMethod("pwcheck", function(value) {
//    return /^[A-Za-z0-9\d=!\-@._*#$%^&]*$/.test(value)
//        && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[!@*#$%^&]/.test(value)
//        && /\d/.test(value)
// });
    $('#changepasswords').validate({
         rules: {
            newpassword: {  
                required: true,
               // pwcheck: true,
                minlength: 6,
                maxlength: 15   
            },
            newpassword_confirmation: {
                required: true,
                //pwcheck: true,
                minlength: 6,
                maxlength: 15,   
                equalTo: "#newpasswords"
            }
         },
         messages: {
            newpassword: {
                required: "Favor preencher o campo acima",
                //pwcheck: "New password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "A senha tem que ter ao menos 6 dígitos/letras.",
                maxlength: "A senha tem que ter ao menos 15 dígitos/letras."
            },
            newpassword_confirmation: {
                required: "Todos os campos são obrigatórios.",
                //pwcheck: "New password should contain alphabets, atleast 1 speacial character and 1 number",
                minlength: "A senha tem que ter ao menos 6 dígitos/letras.",
                maxlength: "A senha tem que ter ao menos 15 dígitos/letras.",
                equalTo: "Divergência nas novas senhas digitadas. As digite novamente, por favor."
            }
         },
         submitHandler: function(form) {
            form.submit();
          }
    });
</script>
@endsection