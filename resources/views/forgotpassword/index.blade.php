@extends('layouts.default-header')
@section('title', 'Esqueci minha senha')
@section('content')
<div class="formsection">
  <div class="container">
    <div class="formdiv loginformdiv">
      <div class="col-sm-8 col-md-7 col-lg-5 centered">
        
        <div class="alert alert-danger" id="bokngError" style="display:none;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        <div class="loginformbg formbg">
          <form class="loginform formarea" id="loginform" name="loginform" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h2 class="text-center  flipInY rrtop">Esqueci minha senha</h2>
            <!-- <p class="text-center  fadeInLeft">Sign up by entering the information below</p> -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} forminput  fadeInLeft">
              {{ csrf_field() }}
              @include('layouts.notify-message')
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus id="email" placeholder="Digite seu e-mail">
            </div>
            <div class="buttondiv  flipInX">
              <input type="submit" class="btn btn-primary btn-lg" id="forgetemailbtn" value="Enviar" >
            </div>
            <!-- <div class="formlink  fadeInUp animated animated" style="visibility: visible;">
              <span>Already Have an Account Please</span>
              <a href="{{ url('/login') }}">Login</a>
            </div>
            <div class="formlink  fadeInUp">
              <span>Don't have an account? Please</span>
              <a href="{{ url('/register') }}">Signup</a>
            </div> -->
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /*email modal */ -->
<div class="modalsection">
  <div class="modal fade provider_phn_popup" id="emailjunkModal" tabindex="-1" role="dialog" aria-labelledby="emailjunkModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="alert alert-success" id="bokngSuccessmesgs">
          <img src="{{url('/public/images/admin-assets/1557376582.1550728494.new_logo.png') }}" alt="image description" height="70" ><span class="bokngSuccessmesgs"></span>
          <!--  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
        </div>
        <h1 class="text-center"></h1>
        <h6 class="text-center">Dê uma olhada no seu e-mail, você recebeu nova mensagem para a criação de uma nova senha.</h6>
        <div class="modal-body">
          
          <span class="provider_price" >Atenção: se não receber o e-mail em poucos segundos, veja a caixa de lixo eletrônico no seu e-mail. Lembre-se de marcar o conteúdo do NaVizinhança como confiável para ficar sempre por dentro das mensagens recebidas.</span>
          
          
        </div>
        <div class="crossbtn">
          <button type="button" class=" btn btn-primary" data-dismiss="modal">&times;</button>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.crossbtn {
position: absolute;
top: -10px;
right: -10px;
}
.crossbtn .btn-primary {
border-radius: 50%;
font-size: 16px;}
.alert-success {
color: #fdfdfd;
background-color: #1b9bd8;
border-color: #1b9bd8;
font-size: 18px;
}
.modalsection h6 {
font-size: 16px;
max-width: 450px;
margin: auto;
line-height: 1.4;
}
span.provider_price {
font-size: 15px;
text-align: center;
width: 100%;
display: inline-block;
}
span.provider_price i.fa.fa-phone {
margin-right: 5px;
font-size: 20px;
}
</style>
<!-- end phone modal -->
<script>
$('#forgetemailbtn').click(function(e) {
e.preventDefault();
var startdate = $('#email').val();
var baseUrl  = '<?php echo url('/');?>';
if(startdate=='') {
$('#email').addClass('vlderror');
return false;
}
$.ajax({
type:'POST',
url:baseUrl+'/forgotpassword/store',
data:$('#loginform').serialize(),
success:function(data){
data=jQuery.parseJSON(data);
if(data.msgCode == 1) {
$("#loginform").trigger("reset");
$('#emailjunkModal').modal('toggle');
$('#emailjunkModal').animate({
scrollTop: 0
}, 'slow');
$('.bokngSuccessmesgs').html(data.message);
$('#bokngSuccessmesgs').show();
//$('#proPhoneModal').modal('show');

// window.setTimeout(function () {
//   $("#bokngSuccessmesgs").fadeTo(500, 0).slideUp(500, function () {
//     $(this).hide();
//     window.location.href="<?php echo url('/');?>/profile/"+profile_id;
//   });
// }, 3000);
} else {
$('#bokngError').html(data.message);
$('#bokngError').show();
$('#bokngError').css('opacity', '1');
window.setTimeout(function () {
$("#bokngError").fadeTo(500, 0).slideUp(500, function () {
$(this).hide();
});
}, 3000);
}
}
});
});
</script>
<style>
#email::placeholder {
color: #666 !important;
}
#email:-ms-input-placeholder {
color: #666 !important;
}

#email::-ms-input-placeholder {
color: #666 !important;
}
#email  {
color:#666 !important;
}
</style>
@endsection