@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="content-wrap">
<div class="yelloback">
<div class="container-fluid">

<?php
$profile_image = $user_info->profile_image;
$user_role = $user_info->user_role;
?>
<!-- <ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
@if($user_role=='Provider')
<li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>@endif
  </ul> -->
  <form class="seredback" id="completionform" method="post" action="{{ url('/admin-users/update') }}"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
        <div class="col-sm-2 profileimage">
          <div class="form-group" >
            <div class="" id="previewdiv">
              <?php
              if($profile_image==''){
                ?>
              <img src="{{ url('/public/images/') }}/prof_dummy2.png" style="width: 150px;height:150px;">
              <?php
              }
              else {
                ?>
                <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" style="width: 150px;height:150px;">
                <?php
              }
              ?>
            </div>
            <div class="col-sm-3 nopadding imgbtns">
              <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i>
            </div>
            <div class="col-md-3 prflinput">
              <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
            </div>
          </div>         
        </div>
        <div class="col-sm-5">
            <div class="form-group profileform_right">
              <label>Name</label>
              <div class="profileinput">
                <input type="text" name="name" id="name" class="form-control" value="{{ $user_info->name }}" placeholder="" readonly>
                <!-- <i class="fa fa-check name"></i> -->
              </div>
            </div>
          <!-- </div>
        <div class=""> -->
            <div class="form-group profileform_right">
              <label>Email Address</label>
              <div class="profileinput">
                <input type="email" name="email" id="email" class="form-control" value="{{ $user_info->email }}" placeholder="" readonly>
                <!-- <i class="fa fa-check email"></i> -->
                <input type="hidden" name="user_id" value="{{ $user_info->id }}">
              </div>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group profileform_right">
              <label>Phone Number</label>
              <div class="profileinput">
                <input type="text" name="phone" id="phone" class="form-control" value="{{ $user_info->phone }}" placeholder="" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');">
                <!-- <i class="fa fa-check phone"></i> -->
              </div>
            </div>
        <!-- </div>
        <div class=""> -->
            <div class="form-group profileform_right">
              <label>User Role</label>
              <div class="profileinput">
                <select class="form-control" disabled>
                  <option vlaue="">Select your option</option>
                  <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Provider</option>
                  <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Taker</option>
                </select>
                <input type="hidden" name="user_role" value ='{{ $user_info->user_role }}'>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
      <div class="col-sm-12 profileform_right">
          
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group profileform_right">
            <label class="">Country</label>
            <div class="profileinput">
              <select class="form-control" name="country" disabled>
                <option value=""></option>>
                <?php
                foreach($countries as $country_name){
                ?>
                <option value="<?php echo $country_name; ?>" <?php if($user_info->country==$country_name) { echo 'selected';} ?>><?php echo $country_name; ?></option>
                <?php
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group profileform_right">
              <label class="">Bio</label>
              <div class="profileinput">
                <input type="text" name="bio" id="bio" class="form-control" value="{{ $user_info->bio }}" placeholder="" readonly>
                <!-- <i class="fa fa-check bio"></i> -->
              </div>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group profileform_right">
            <label class="">Zip Code</label>
            <div class="profileinput">
              <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $user_info->zipcode; ?>" placeholder="" readonly>
             <!--  <i class="fa fa-check zipcode"></i> -->
            </div>
          </div>
        </div>
        
        <div class="col-sm-6">
            <div class="form-group profileform_right">
              <label class="">Address</label>
              <div class="profileinput">
                <textarea name="address" id="address" class="form-control"><?php echo $user_info->address; ?></textarea>
                <!-- <i class="fa fa-check address"></i> -->
              </div>
            </div>
        </div>
      </div>
      <div class="row">
          @if($user_role=='Provider')
            <div class="col-sm-12 form-group profileform_right">
             <!--  <form name="complete_service" id="complete_service" class="complete_service" action="/admin-users/update/" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }} -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <input type="hidden" class="sub_cat_check_address" value ="{{ url('/check_subcat/') }}">
                       <label>Choose Category Here</label>
                        <select name="main_categroy_id" id="main_category" class="form-control" required>
                        <option value="" selected disabled="disabled">Please choose one</option>
                         @foreach($main_category as $main_cat_id=>$main_cat_name)
                         <option value="{{$main_cat_id}}" @if($main_cat_id== $user_info->cat_id) selected @endif>{{  $main_cat_name  }}</option>
                        @endforeach
                        </select>
                    </div>
                  <div class="form-group col-md-6">
                    <input type="hidden" class="sub_cat_check_address" value ="{{ url('/check_subcat/') }}">
                    <label>Choose Sub Category Here</label>
                    <select name="sub_categroy_id"  class="form-control" id="sub_category" required>
                    <option value="{{ $category->cat_id }}" selected>{{ $category->cat_name }} </option>
                    </select>
                  </div>
                  </div>    
              
            </div>
          @endif   
        </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group profileform_right">
            <label class="">Upload User Image</label>
            <div class="profileinput" id="browse_img">
              @if(count($user_image) > 0)
              @foreach ($user_image as $user_images)
              <div class="col-sm-2 single_img" id="prv_{{ $user_images->image_id }}">  
                <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a>
                <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" style="margin: 0 10px;">
              </div>
                @endforeach
                @else
               <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                @endif
              <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" >
              <div class="dash_profile" id="browsebtn">

                <span >Select Images  </span>
                <!-- <p>No File Selected</p> -->
              </div>

            </div>

          </div>
        </div>

      </div>
      <div class="img_limit_msg" style="display:none;"><br><p style="color:red;font-size:15px;">Sorry! You can not upload more than 5 Images.</p></div>
        <!-- <div class="col-sm-12 form-group forminput">
          <label>Upload User Image</label>
            <div class="col-sm-12 col-xs-3 browseimage nopadding" id="browse_img">
              @if(count($user_image) > 0)
              @foreach ($user_image as $user_images)
              <div class="col-sm-2 single_img" id="prv_{{ $user_images->image_id }}">  
                <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a>
                <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" style="margin: 0 10px;">
              </div>
                @endforeach
                @else
               <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                @endif
            </div>
            <div class="col-sm-12 col-xs-9 new_up_sec">
                <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
              <div class="browse_btn" id="browsebtn">
                <span>Select Images</span>
                <p>No File Selected</p>
              </div>
            </div>
        </div> -->
          <div class="row">
            <div class="col-sm-12 nextbtn">
                <button type="submit" class="btn button_blueblue">Save</button>
                <!-- <input class="form-control" value="Save" type="submit"> -->
            </div>   
         </div>
      </div>
    </div>
  </form>
</div>  
</div>
</div>
 
<style type="text/css">
/*.profileform_right input {
    width: 90%;
}*/
.profileinput input {
    background-color: transparent !important;
    border: 1px solid #ddd;
}
.profileinput textarea#address {
    background-color: transparent;
    border: 1px solid #ddd;
    font-size: 15px;
    /*height: 35px;*/
}
.profileinput select {
  background-color: transparent !important;
    border: 1px solid #ddd;
}
.profileinput {
    overflow: hidden;
}
  .browseimage img {
    height: 90px;
    width: 90px;
}
.browse_btn span {
    background-image: -webkit-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -moz-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -ms-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -o-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: linear-gradient(top, #ffffff, #e1e1e1);
    border: 1px solid #d6d6d6;
    border-radius: 5px;
    color: #666666;
    float: left;
    font-size: 15px;
    padding: 10px 30px;
    margin-top: 20px;
    cursor: pointer;
}
.browse_btn p {
    color: #996666;
    float: left;
    font-size: 16px;
    width: 100%;
}
.profileinput textarea {
    border: 0 none;
    box-shadow: none;
    color: #444545;
    float: left;
    font-size: 20px;
    padding: 0;
    width: calc(100% - 30px);
}
</style>      
<script>
  $('#main_category').on('change',function(){
    var main_cat = $('#main_category').val();
   var token =  $("input[name=_token]").val();

    var url = $('.sub_cat_check_address').val();
    // var data = $('#complete_service').serialize();
    $.ajax({
      type:"POST",
      dataType:'JSON',
      url:url,
      data:{'main_cat_id':main_cat,'_token':token},
      success:function(data){
        $('#sub_category').empty();
        if(data != ''){
          $('#sub_category').html('<option value="" selected disabled>Please select one</option>');
          $.each(data,function(k,v){
            console.log(k);
            var option = $("<option />");
            option.attr("value", k).text(v);
            $('#sub_category').append(option);
          });
        }
        else{
          var option = $("<option />");
          option.attr("value", '0').text('Sorry data not found');
          $('#sub_category').append(option);
        }

      }
    })
  });

  $(".delete_btn").on('click', function(e){
        if(confirm("Are you sure you want to delete?")){
          var imageid = jQuery(this).attr('id');
          if(imageid!='') {
            jQuery.ajax({
              type : 'GET',
              url : "{{ url('/accountcomplete') }}/"+imageid,
              data:{ 'imageid': imageid, '_token': "{{ csrf_token() }}"},
              success :  function(resp) {
                if(resp="success"){
                   $('#prv_'+imageid).remove();
                    $('#'+imageid).remove();
                }
              }
            });
          }
        } else {
          return false;
        }
      });
</script>
@stop      