<?php
$admin_login_id = Auth::id();
$admin_info = DB::table('users')
->where('is_active', '1')
->where('is_deleted', '0')
->where('user_role', 'admin')
->where('id', $admin_login_id)
->first();
$admin_img = $admin_info->profile_image;
if($admin_img==''){
  $img_profile = "/public/admin/images/gravtar.jpeg";
}
else {
 $img_profile = "/public/admin/images".'/'.$admin_img;
}
/* COunt */
$userCount = DB::table('users')
->where('user_role', '!=', 'admin')
->where('is_active', '=', '1')
->where('is_deleted', '=', '0')
->count();
/*$providerCount = DB::table('users')
->where('is_active', '1')
->where('is_deleted', '0')
->where('cat_id','!=','')
->where('sub_id','!=','')
->where('user_role','=', 'Provider')
->count();*/
$providerCount = DB::table('users')
->where('user_role', '=', 'Provider')
->where('is_active', '=', '1')
->where('is_deleted', '=', '0')
->count();
$takerCount = DB::table('users')
->where('user_role', '=', 'Taker')
->where('is_active', '=', '1')
->where('is_deleted', '=', '0')
->count();
$orderCount = DB::table('booking')
->where('is_active', '1')
->where('is_deleted', '0')
->where('booking_verified', '0')
->count();
/*$feedbackCount = DB::table('feedback')
->where('approve_status', '0')
->count();*/
$feedbackCount = DB::table('feedback')
->Join('users','feedback.user_id','=','users.id')
->where('feedback.approve_status', 0)
->where('users.is_deleted',0)
->where('users.is_active',1)
->count();

$closeRequestCount = DB::table('close_account_request')
->Join('users','close_account_request.user_id','=','users.id')
->where('users.close_request_action', '1')
->count();
$blackCount =DB::table('black_users')
->where('black_status', '0')
->where('is_deleted', '0')
->count();
$inactiveUsersCount = DB::table('users')
->where('user_role', '!=', 'admin')
->where('close_request_action', '=', '0')
->where('is_active', '=', '0')
->where('is_deleted', '=', '0')
->count();

$url_segment = \Request::segment(1);

$business_setting  = DB::table('business_settings')->where('user_id',Auth::user()->id)->first();
 // echo "<pre>";
 // print_r($business_setting);die;
?>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures wow fadeInLeft">
  <div class="nano">
    <div class="nano-content" style="right: -25px;">
      <div class="dashboardlogo"><a href="#"><img src="{{ url('/public') }}/admin/images/yellow_logo.png" alt="" /></a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button></div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <form class="navbar-form sidebar_search">
            <div class="box_round">
              <div class="col-sm-4 rounded">
                <img src="{{ url('/public/images/admin-assets') }}/{{ $business_setting->admin_profile_picture }}" class="img-rounded">
              </div>
              <div class="online">
                <p>{{ $admin_info->name }}</p>
                <!-- <i class="fa fa-circle online_icon" aria-hidden="true"><span class="online_clr">Online</span></i> -->
              </div>
            </div>



            <div class="inner-addon right-addon icontext">
              <i class="glyphicon glyphicon-search "></i>
              <input type="text" id="myInput" onkeyup="searchFunction()" placeholder="Search" class="form-control text_icon" />
            </div>
          </form>


          <ul id="findtext" class="navmain">
            <li class="label label_box">MAIN NAVIGATION</li>

            <li class="@if($url_segment=='admin-dashboard')active @endif"><a href="{{ url('/admin-dashboard') }}" $payment_past class=""><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard<span class="sidebar-collapse-icon ti-angle-down"></span></a>
            </li>
            <li class="@if($url_segment=='admin-feedback')active @endif"><a href="{{ url('/admin-feedback') }}" $payment_past class=""><i class="fa fa-comments-o" aria-hidden="true"></i>Feedback<span class="badge badge-green">{{ $feedbackCount }}</span><span class="sidebar-collapse-icon ti-angle-down"></span></a>
            </li>
            <li class="@if($url_segment=='admin-other-services')active @endif"><a href="{{ url('/admin-other-services') }}" class="sidebar-sub-toggle"><i class="fa fa-envelope" aria-hidden="true"></i>Other Services </a></li>
           <!--  <li class="@if($url_segment == 'admin/feedback')active @endif"><a href="{{ url('/admin/feedback') }}" class="sidebar-sub-toggle"><i class="fa fa-envelope" aria-hidden="true"></i>Feedback  </a>
            </li>  -->
            <li class="@if($url_segment=='admin-menu-details')active @endif"><a href="{{ url('/admin-menu-details') }}" class="sidebar-sub-toggle"><i class="fa fa-envelope" aria-hidden="true"></i>Menu Details </a></li>
            <li class="@if($url_segment=='admin-closerequest')active @endif"><a href="{{ url('/admin-closerequest') }}" $payment_past><i class="fa fa-window-close" aria-hidden="true"></i> Close Request<span class="badge badge-green">{{ $closeRequestCount }}</span> <span class="sidebar-collapse-icon ti-angle-down"></span></a></li>
            <li class="@if($url_segment=='admin-orders')active @endif"><a href="{{ url('/admin-orders') }}" class="sidebar-sub-toggle"><i class="fa fa-truck" aria-hidden="true"></i> Orders<span class="badge badge-green">{{ $orderCount }}</span> <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            </li>
            <li data-toggle="collapse" data-target="#user1" class="collapsed"><a><i class="fa fa-users" aria-hidden="true"></i>Users<!-- <span class="badge badge-green">{{ $userCount }}</span> --><span class="clr"><i class="fa fa-chevron-left clr" aria-hidden="true"></i></span><span class="sidebar-collapse-icon ti-angle-down"></span> </a>

              <ul class="sub-menu collapse sidebar_submenu" id="user1">
                <li><a href="{{ url('/admin-provider') }}" class="@if($url_segment=='admin-provider')active @endif">Providers<span class="badge badge-green">{{ $providerCount }}</span></a></li>
                <li><a href="{{ url('/admin-taker') }}" class="@if($url_segment=='admin-taker')active @endif">Takers<span class="badge badge-green">{{ $takerCount }}</span></a></li>
                <li><a href="{{ url('/admin-inactiveusers') }}" class="@if($url_segment=='admin-inactiveusers')active @endif">Inactive<span class="badge badge-green">{{ $inactiveUsersCount }}</span></a></li>
              </ul>
            </li>
            <li class="@if($url_segment=='admin-blackusers')active @endif"><a href="{{ url('/admin-blackusers') }}" class="sidebar-sub-toggle"><i class="fa fa-user" aria-hidden="true"></i>Black Users <span class="badge badge-green">{{ $blackCount }}</span></a>


            <!--<ul class="sub-menu collapse" id="products">-->
              <!--    <li><a href="{{ url('/admin-users') }}" ><i class="fa fa-th" aria-hidden="true"></i>Users</a></li>-->
              <!--    <li><a href="#">General</a></li>-->
              <!--    <li><a href="#">Buttons</a></li>-->
              <!--    <li><a href="#">Tabs & Accordions</a></li>-->
              <!--    <li><a href="#">Typography</a></li>-->
              <!--    <li><a href="#">FontAwesome</a></li>-->
              <!--    <li><a href="#">Slider</a></li>-->
              <!--    <li><a href="#">Panels</a></li>-->
              <!--    <li><a href="#">Widgets</a></li>-->
              <!--    <li><a href="#">Bootstrap Model</a></li>-->
              <!--</ul>-->
              <li data-toggle="collapse" data-target="#products" class="collapsed"><a style="position: relative;"><i class="fa fa-laptop" aria-hidden="true"></i>Manage Services<span class="clr"><i class="fa fa-chevron-left clr" aria-hidden="true"></i></span><span class="sidebar-collapse-icon ti-angle-down"></span> </a>

                <ul class="sub-menu collapse sidebar_submenu" id="products">
                  <li><a href="{{ url('/admin/main-category/') }}" class="@if($url_segment=='admin/main-category')active @endif">Main Category</a></li>
                  <li><a href="{{ url('/admin-services') }}" class="@if($url_segment=='admin-services')active @endif">Sub-Category</a></li>
                </ul>
              </li>

              <!-- <li><a href="#" $payment_past><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Settings<span class="sidebar-collapse-icon ti-angle-down"></span></a></li> -->

              <li class="@if($url_segment=='admin-invite')active @endif"><a href="{{ url('/admin-invite') }}" $payment_past><i class="fa fa-user-plus"></i> Invitations<span class="sidebar-collapse-icon ti-angle-down"></span></a></li>

                        <!-- <li><a href="#" class="sidebar-sub-toggle"><i class="fa fa-calendar" aria-hidden="true"></i> Invoice</a>
                        </li> -->

                        <li class="@if($url_segment=='admin/testimonials')active @endif"><a href="{{ url('/admin/testimonials')}}" class="sidebar-sub-toggle"><i class="fa fa-quote-left" aria-hidden="true"></i>Testimonials </a>
                        </li>
                        <li class="@if($url_segment=='admin-mails')active @endif"><a href="{{ url('/admin-mails') }}" class="sidebar-sub-toggle"><i class="fa fa-envelope" aria-hidden="true"></i>Mailbox </a>
                        </li>
                      </ul>
                    </div>
                    <div class="nano-pane"><div class="nano-slider" style="height: 365px; transform: translate(0px, 0px);"></div></div></div>
                  </div>

                </div>
                <script>
                  $(document).ready(function(){
                    $('.navmain li a').click(function(){
                      $('li a').removeClass("active");
                      $(this).addClass("active");
                    });

                  });
                </script>
                <script>
                  $(document).ready(function(){
                    $("#myInput").on("keyup", function() {
                      var value = $(this).val().toLowerCase();
                      $("#findtext li").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                      });
                    });
                  });
                </script>
