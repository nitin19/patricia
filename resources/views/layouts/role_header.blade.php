<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/public') }}/images/icon.png" />
  <!-- Bootstrap -->
  <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/style.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/change.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/new.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/dropdown.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/table-css.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="{{ url('/public') }}/css/animate.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/mapsjs-ui.css" />
  <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/other.css" />
  <link href="{{ url('/public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-core.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-service.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-ui.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-mapevents.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap-multiselect.css" type="text/css">
<script type="text/javascript" src="{{ url('/public') }}/js/bootstrap-multiselect.js"></script>
  <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/wow.min.js"></script>
  <link href="{{ url('/public/') }}/css/bootstrap-datetimepicker.css" rel="stylesheet">
  <script type="text/javascript" src="{{ url('/public/') }}/js/moment.js"></script>
  <script type="text/javascript" src="{{ url('/public/') }}/js/bootstrap-datetimepicker.js"></script>
  <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
</head>
<body>

  <header class="site-header" id="myHeader">
    <div class="top_header">
      <div class="container-fluid">
        <div class="col-sm-8 leftheader wow fadeInLeft" data-wow-delay="0.1s">

        </div>
        <div class="col-sm-4 rightheader wow fadeInLeft" data-wow-delay="0.3s">
          <ul class="navbar-right">
           <span>Follow Us</span>
          <li><a href="https://www.facebook.com/navizinhanca/  " target="_blank"><i class="fa fa-facebook-square"></i></a></li>
          <li><a href="https://www.linkedin.com/company/navizinhanca/
" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
          <li><a href="https://www.instagram.com/accounts/login/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
       </div>
     </div>
   </div>


   <nav class="navbar navbar-default navigationbar">@if(!empty(Auth::user())&&Auth::user()->user_role == 'Taker') 
    @include('layouts.taker_header')
    @elseif(!empty(Auth::user())&&Auth::user()->user_role == 'admin')
      @include('layouts.admin_frontend_header')
    @elseif(!empty(Auth::user())&&Auth::user()->user_role == 'Provider')
     @include('layouts.provider_header')
   @endif </nav></header>
   <main> @yield('content') </main>
   <footer> @include('layouts.footer') </footer>
   <script>
    $(document).ready( function() {

      $('.success_show_ip').hide();
      $('.success_show').hide();
      $('.success_role').hide();
      $('.success_request').hide();
      $('.removewhishlist').click(function(){
        alert('dffd');
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/removewhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='true') {
              $('.success_show').show();
              $('#unwish_'+userid).hide();
            }
          }
        });
      });
      $('.removewhishlist_profile').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/removewhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='true') {
              window.location.href="{{ url('/profile') }}/"+userid;
            }
          }
        });
      });
      $('.addwhishlists').click(function(){
        var parent_id = $("#whishlist_id").attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='true') {
              window.location.href="{{ url('/profile') }}/"+userid;
            }
          }
        });
      });
      $('.addwhishlist').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='true') {
              $('.success_show').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist");
            }
          }
        });
      });
      $('.addwhishlist_ip').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        var userip = jQuery('#'+parent_id+' #userip').val();
        $.ajax({
          url: "{{ url('/wishlistip/addwhish_ip') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
          success: function(response) {
            if(response=='true') {
              $('.success_show').show();
              $('.success_show_ip').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist_ip");
            //   window.location.href = '/login';

            }
          }
        });
      });
      $('.removewhishlist_ip').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        var userip = jQuery('#'+parent_id+' #userip').val();
        $.ajax({
          url: "{{ url('/wishlistip/removewhish_ip') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
          success: function(response) {
            if(response=='true') {
              $('.success_show').show();
              $('#unwish_'+userid).hide();
            }
          }
        });
      });

      $.validator.addMethod("alphaLetterNumber", function(value, element) {
       return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
     });

      $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
       return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
     });

      $.validator.addMethod("alphaLetter", function(value, element) {
       return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
     });

      $("#loginform").validate({
        rules: {
          email: {
            required: true,
            email: true,
            maxlength: 120

          },
          password: {
            required: true
          }
        },
        messages: {

          email: {
            required: "Please enter email.",
            email: "Please enter a valid email.",
            maxlength: "Maximum 120 characters allowed."
                 // remote: "Email already in use."
               },
               password: {
                required: "Please enter password."
              }
            },
            submitHandler: function(form) {
              form.submit();
            }
          });
      $("#contactform").validate({
        rules: {
          first_name: {
            required: true
          },
          last_name: {
            required: true
          },
          email: {
            required: true,
            email: true
          },
          phonenumber: {
            required: true,
            minlength: 10,
            maxlength: 14
          }
        },
        messages: {

          first_name: {
            required: "Please enter first name."
          },
          last_name: {
            required: "Please enter last name."
          },
          email: {
            required: "Please enter email.",
            email: "Please enter correct email."
          },
          phonenumber: {
            required: "Please enter phone number.",
            minlength: "Minimum 10 characters required.",
            maxlength: "Maximum 14 characters allowed."
          }
        },
        submitHandler: function(form) {
          form.submit();
        }
      });

      $("#carddetailform").validate({
        rules: {
          paymentname: {
            required: true
          },
          card_num: {
            required:true,
            minlength:16,
            maxlength:20
          },
          exp_month:{
            required: true
          },
          exp_year:{
            required : true
          },
          cvv:{
            required:true,
            minlength: 3,
            maxlength: 4
          }
        },
        messages: {
          paymentname: {
            required: "Please enter your name."
          },
          card_num: {
            required: "Please enter card Number",
            minlength: "Minimum 16-digits are required.",
            maxlength: "Maximum 20-digits are required."
          },
          exp_month: {
            required: "Please select expiry month of card."
          },
          exp_year: {
            required: "Please select expiry year of card."
          },
          cvv: {
            required:"Please enter your cvv",
            minlength: "Minimum 3-digits are required.",
            maxlength: "Maximum 4-digits are required."
          }
        },
        submitHandler: function(form) {
          $.ajax({
            url: "{{ url('/dashboard/save_card') }}",
            type: 'POST',
            data: $(form).serialize(),
            success: function(response) {
              $('.success_show').show();
            }
          });
        }
      });
      $("#closeaccount").validate({
        rules: {
          close_reason: {
            required: true
          }
        },
        messages: {
          close_reason: {
            required: "Please Select Reason."
          }
        },
        submitHandler: function(form) {
          $.ajax({
            url: "{{ url('/dashboard/closeaccount') }}",
            type: 'POST',
            data: $(form).serialize(),
            success: function(response) {
              $('.success_request').show();
              setTimeout(function() {
               $('#logout-form').submit();
             }, 10000);
            }
          });
        }
      });
    });
  </script>
  <script>
    wow = new WOW(
    {
      animateClass: 'animated',
      offset: 100,
      callback: function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
      }
    }
    );
    wow.init();
/*    document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };*/
  </script>
  <script type="text/javascript">
    (function(){
      $('#serviceslider').carousel({ interval: 5000 });
    }());

    (function(){
      $('.carousel-showmanymoveone .item').each(function(){
        var itemToClone = $(this);

        for (var i=1;i<6;i++) {
          itemToClone = itemToClone.next();

      // wrap around if at end of item collection
      if (!itemToClone.length) {
        itemToClone = $(this).siblings(':first');
      }

      // grab item, clone, add marker class, add to collection
      itemToClone.children(':first-child').clone()
      .addClass("cloneditem-"+(i))
      .appendTo($(this));
    }
  });
    }());
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
     jQuery('#uploadbtn').on('click', function() {
      jQuery('#brwsebtn').click();
    });

     jQuery("#brwsebtn").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var imageTypes= ["image/jpeg","image/png","image/jpg"];
      if(imageTypes.indexOf(imagefile) == -1)
      {
        jQuery("#previewdiv").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
        return false;
      }
      else
      {
        var reader = new FileReader();
        reader.onload = function(e){
          jQuery("#previewdiv").html('<img src="' + e.target.result + '" style="width:100px;height:100px;"/>');
        };
        reader.readAsDataURL(this.files[0]);
      }
    });

     $(".list-group a").click(function() {
      $('.list-group a').removeClass('selected');
      $(this).addClass('selected');
    });
   });

 </script>
 <script>
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }
  function myFunction_copy() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    document.execCommand("copy");
    alert("Copied the URL: " + copyText.value);
  }
</script>
<!-- Compete Profile js */ -->
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#browsebtn').on('click', function() {
     jQuery('#user_images').click();
   });
    $(function() {
         // Multiple images preview in browser
         var imagesPreview = function(input, placeToInsertImagePreview) {
           if (input.files) {
             var filesAmount = input.files.length;
             for (i = 0; i < filesAmount; i++) {
               var reader = new FileReader();
               reader.onload = function(event) {
                $($.parseHTML('<img style="margin:0 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
              }
              reader.readAsDataURL(input.files[i]);
            }
          }

        };
        $('#user_images').on('change', function() {
          jQuery('#nofile').empty();
          jQuery('#browse_img').empty();
          imagesPreview(this, '#browse_img');
        });
      });


    var phone = jQuery('#phone').val();
    var zipcode = jQuery('#zipcode').val();
    var address = jQuery('#address').val();
    jQuery('.phone').show();
    jQuery('.zipcode').show();
    jQuery('.address').show();
    if(phone==''){
      jQuery('.phone').hide();
    }
    if(zipcode==''){
      jQuery('.zipcode').hide();
    }
    if(address==''){
      jQuery('.address').hide();
    }
    jQuery("#phone").keypress(function(){
      var phone_val = jQuery('#phone').val();
      if(phone_val==''){
        jQuery('.phone').hide();
      }
      else {
        jQuery('.phone').show();
      }
    });
    jQuery("#zipcode").keypress(function(){
      var zipcode_val = jQuery('#zipcode').val();
      if(zipcode_val==''){
        jQuery('.zipcode').hide();
      }
      else {
        jQuery('.zipcode').show();
      }
    });
    jQuery("#address").keypress(function(){
      var address_val = jQuery('#address').val();
      if(address_val==''){
        jQuery('.address').hide();
      }
      else {
        jQuery('.address').show();
      }
    });
  });
  jQuery("#completionform").validate({
    rules: {
      phone: {
        required: true,
        minlength: 10,
        maxlength: 14
      },
      zipcode: {
        required: true,
        minlength: 4,
        maxlength: 8
      },
      address: {
       required: true
     }
   },
   messages: {
    phone: {
      required: "This field is required",
      minlength: "Minimum 10 Number required.",
      maxlength: "Maximum 14 Number allowed."
    },
    zipcode: {
      required: "This field is required",
      minlength: "Minimum 4 Number required.",
      maxlength: "Maximum 8 Number allowed."
    },
    address: {
      required: "This field is required"
    }
  },
  submitHandler: function(form) {
    form.submit();
  }
});
  jQuery("#complete_service").validate({
    rules: {
      'cat_name[]': {
        required: true
      }
    },
    messages: {
      'cat_name[]': {
        required: "This field is required"
      },
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
  jQuery(".delete_btn").on('click', function(e){
    if(confirm("Are you sure you want to delete?")){
      var imageid = jQuery(this).attr('id');

      if(imageid!='') {
        jQuery.ajax({
          type : 'GET',
          url : "{{ url('/accountcomplete') }}/"+imageid,
          data:{ 'imageid': imageid, '_token': "{{ csrf_token() }}"},
          success :  function(resp) {
            jQuery('#prv_'+imageid).remove();
          }
        });
      }
    } else {
     return false;
   }
 });
</script>
<!-- Compete Profile js */ -->
</body>
</html>
