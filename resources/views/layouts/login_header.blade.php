<?php
$business_info = DB::table('business_settings')
                ->join('users','business_settings.user_id','=','users.id')
                ->where('users.user_role','=','admin')
                ->select('business_settings.*','users.user_role','users.id')->first();
$header_img=$business_info->header_logo;
?>
<div class="container-fluid">
  <div class="col-sm-3 col-xs-9 logodiv">
    <a href="{{ url('/') }}"><img src="{{ url('/public/images') }}/admin-assets/{{ $header_img }}" class="img-responsive"></a>
  </div>
  <div class="col-xs-3 col-sm-8">
    <div class="navbar-header navbar-right">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
  </div>
  <div class="col-sm-9 col-xs-12 navlinks nopadding">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        @if(empty(Auth::user()))
        <li @if(Request::path() === 'home') class="active"@endif><a href="{{ url('/home') }}">Faça sua busca</a></li>
        <li @if(Request::path() === 'otherservices') class="active"@endif><a href="{{ url('/otherservices') }}">Informações úteis</a></li>
        <li @if(Request::path() === 'feedback') class="active"@endif><a href="{{ url('/contactus') }}">Contate-nos</a></li>
        <li @if(Request::path() === 'register') class="active"@endif><a class="signup" href="{{ url('/register') }}"><!-- <i class="fa fa-plus"></i> -->Cadastre-se</a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Link 1</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
          </div>
        </li>
        <li @if(Request::path() === 'login') class="active"@endif><a href="{{ url('/login') }}"><i class="fa fa-user-o"></i> Entrar</a></li>
        @endif
    </ul>
  </div>
</div>

</div>
