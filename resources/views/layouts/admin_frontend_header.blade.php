<div class="container-fluid">
<div class="col-sm-4 col-xs-9 col-md-2 logodiv wow fadeInLeft" data-wow-delay="0.5s">
<a href="{{ url('/') }}"><img src="{{ url('/public') }}/images/yellow_logo_new.png" class="img-responsive"></a>
</div>
<div class="col-xs-3 col-sm-8">
<div class="navbar-header navbar-right">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
</div>
    <div class="col-sm-9 col-xs-12 navlinks nopadding wow fadeInLeft" data-wow-delay="0.6s">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        @if(!empty(Auth::user()))
          <li ><a href="{{ url('/home') }}">Find Service Provider</a></li>
          <li><a href="{{ url('/wishlist') }}">WishList</a></li>
          <!-- <li><a href="{{ url('/invite-friend') }}">Invite a Friend</a></li> -->
            <li><a href="{{ url('/otherservcie') }}">Other Services</a></li>
            <li><a href="{{ url('/feedback') }}">Feedback</a></li>
            <!-- <li><a href="{{ url('/invite-friend') }}">Invite a Friend</a></li> -->
            <li class="provider_service"><a href="{{ url('/provider/register') }}">Offer Your Services</a></li>
            <li><a class="signup" href="{{ url('/register') }}"><i class="fa fa-plus"></i>&nbsp;Sign Up</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
              </div>
            </li>

            @else
            <li ><a href="{{ url('/home') }}">Find Service Provider</a></li>
            <li><a href="{{ url('/wishlistip') }}">WishList</a></li>
            <li><a href="{{ url('/otherservcie') }}">Other Services</a></li>
            <li><a href="{{ url('/feedback') }}">Feedback</a></li>
            <!-- <li><a href="{{ url('/invite-friend') }}">Invite a Friend</a></li> -->
            <li class="provider_service"><a href="{{ url('/provider/register') }}">Offer Your Services</a></li>
            <li><a class="signup" href="{{ url('/register') }}"><i class="fa fa-plus"></i>&nbsp;Sign Up</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
              </div>
            </li>
          @endif
            <li class="loginbtn text-right wow fadeInLeft
            @if($user = Auth::user()) 'logoutcls'@endif" data-wow-delay="0.7s">
            <?php
            $current_userid = Auth::id();
            $current_username = ucfirst(Auth::user()->name);
            $profile_image = Auth::user()->profile_image;
            ?>
            @if($user = Auth::user())

            <a href="{{ url('/accountcomplete') }}" class="dropdown-toggle profile" data-toggle="dropdown" role="button" aria-haspopup="true">
              @if($profile_image=='')
              <img src="{{ url('public/images/')}}/gravtar.jpeg" class="logimg">
              @else
              <img src="{{ url('public/images/')}}/profileimage/{{ $profile_image }}" class="logimg">
              @endif
              {{ $current_username }} <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu mega-dropdown-menu  droplist">
              <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
              <li><a href="{{ url('/changepassword') }}">Change Passwoard</a></li>
              <li><a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">logout</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
            @else
            <a href="{{ url('/login') }}"><i class="fa fa-user-o"></i>Login</a>
            @endif
          </li>

      </ul>
  </div>
  </div>

  </div>
