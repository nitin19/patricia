<?php
$business_info = DB::table('business_settings')
->join('users','business_settings.user_id','=','users.id')
->where('users.user_role','=','admin')
->select('business_settings.*','users.user_role','users.id')->first();
$header_img=$business_info->header_logo;
if (Auth::check()) {
  $cLogInUid = Auth::user()->id;
  $clUser = DB::table('users')->where('id', '=', $cLogInUid)->first();
}
?>
<div class="container-fluid">
  <div class="row">  
    <div class="col-sm-3 col-xs-9 logodiv">
      <?php 
      if($clUser->cpf_id !='' && $clUser->phone !='' && $clUser->zipcode !='' && $clUser->address !='' && $clUser->city !='' && $clUser->neighborhood !='' && $clUser->state !='' && $clUser->country !='' ) {
        $home_url =  url('/');
      } else { 
        $home_url=url('servicetaker/completeprofile');
      } 
      ?>
      <a href="{{$home_url }}"><img src="{{ url('/public/images') }}/admin-assets/{{ $header_img }}" class="img-responsive"></a>
    </div>
    <div class="col-xs-3 col-sm-9">
      <div class="navbar-header navbar-right">
        <button id="ChangeToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <div id="navbar-hamburger">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
        <div id="navbar-close" class="hidden">
          <span class="glyphicon glyphicon-remove" style="color: #fff; float: left; margin-left: 8px;"></span>
        </div>
        </button>
      </div>
    </div>
    <div class="col-sm-9 col-xs-12 navlinks nopadding">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">

          @if(Auth::check())

           @if($user = Auth::user())
           @if($clUser->cpf_id !='' && $clUser->phone !='' && $clUser->zipcode !='' && $clUser->address !='' && $clUser->city !='' && $clUser->neighborhood !='' && $clUser->state !='' && $clUser->country !='' )

          <li @if(Request::path() === 'home') class="active"@endif><a href="{{ url('/home') }}">Faça sua busca</a></li>
          <li @if(Request::path() === 'servicetaker/bookings') class="active"@endif><a href="{{ url('servicetaker/bookings') }}">Contatos & Mensagens</a></li>
          
          <li @if(Request::path() === 'feedback') class="active"@endif><a href="{{ url('/feedback') }}">Avalie seu profissional</a></li>

          <li @if(Request::path() === 'wishlist') class="active"@endif><a href="{{ url('/wishlist') }}">Sua seleção</a></li>
          
          <li @if(Request::path() === 'otherservices') class="active"@endif><a href="{{ url('/otherservices') }}">Informações úteis</a></li>
          
          <li @if(Request::path() === 'invite-friend') class="active"@endif><a href="{{ url('/invite-friend') }}">Convide um amigo</a></li> 
          <li class="loginbtn text-right noticenotice">
            <a href="" class="dropdown-toggle profile quickView" data-toggle="dropdown" role="button" aria-haspopup="true">
              <i class="fa fa-bell-o" aria-hidden="true"></i>
              <span class="counter red" id="notiCount" style="display:none; background-color:#1B9BD8;!important"></span>
            </a>
            <ul class="dropdown-menu mega-dropdown-menu notlist">
              <div class="notihead">
               Notificações recentes<a href="{{ url('/servicetaker/account/notifications')}}" class="viewlink">Ver todas</a>
              </div>
              <div class="notibody">
                <span class= "p_id" data-id="{{ Auth::id() }}"> </span>
                <span class= "noti_role" data-role="{{ Auth::user()->user_role }}"></span>
                <ul id="loadnotification" class="takernotify"></ul>     
              </div>
            </ul>
          </li>

           <li class="smartWrk1"><a href="{{ url('/servicetaker/dashboard') }}"><i class="fa fa-tachometer" aria-hidden="true"></i>Painel de controle</a></li>

           <li class="smartWrk1"><a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('mobile-logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>Sair</a>

              <form id="mobile-logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>

            </li>

           @endif

            <li class="loginbtn logout_menu text-right
            @if($user = Auth::user()) 'logoutcls'@endif">
            <?php
            $current_userid = Auth::id();
            $current_username = ucfirst(Auth::user()->name);
            $profile_image = Auth::user()->profile_image;
            $user_role = Auth::user()->user_role;
            ?>
            <a href="{{ url('/accountcomplete') }}" class="dropdown-toggle profile logout_btn" data-toggle="dropdown" role="button" aria-haspopup="true">
              @if($profile_image=='')
              <img src="{{ url('public/images/')}}/gravtar.jpeg" class="logimg">
              @else
              <img src="{{ url('public/images/')}}/profileimage/{{ $profile_image }}" class="logimg">
              @endif


            @if($clUser->cpf_id !='' && $clUser->phone !='' && $clUser->zipcode !='' && $clUser->address !='' && $clUser->city !='' && $clUser->neighborhood !='' && $clUser->state !='' && $clUser->country !='' )

            {{ $current_username }} <i class="fa fa-angle-down" aria-hidden="true"></i>
              <a href="{{ url('/servicetaker/switch/role') }}" class="user_role_text"><?php if($user_role=='Taker'){ echo 'Contratante';} ?></a>
            </a>
            @else
            {{ $current_username }} <i class="fa fa-angle-down" aria-hidden="true"></i>
              <a href="javascript:void(0)" class="user_role_text"><?php if($user_role=='Taker'){ echo 'Contratante';} ?></a>
            </a> 
            @endif
              
            <ul class="dropdown-menu mega-dropdown-menu droplist">
              <div class="drophead"></div>

              @if($clUser->cpf_id !='' && $clUser->phone !='' && $clUser->zipcode !='' && $clUser->address !='' && $clUser->city !='' && $clUser->neighborhood !='' && $clUser->state !='' && $clUser->country !='' )

              <li class="smartWrk2"><a href="{{ url('/servicetaker/dashboard') }}"><i class="fa fa-tachometer" aria-hidden="true"></i>Painel de controle</a></li>

              <li><a href="{{ url('/changepassword') }}"><i class="fa fa-key" aria-hidden="true"></i>Alterar a senha</a></li>
              
              <!-- <li><a href="{{ url('/servicetaker/switch/role') }}"><i class="fa fa-user" aria-hidden="true"></i>Mudar de função</a></li>-->
              
              <li><a href="{{ url('/switchaccount') }}"><i class="fa fa-user" aria-hidden="true"></i>Mudar de função</a></li>
             
              <li><a href="{{ url('/contactus') }}"><i class="fa fa-user" aria-hidden="true"></i>Contate-nos</a></li>

              <!-- <li><a  href="{{ url('/servicetaker/profile') }}">Perfil completo</a></li> -->

              @endif 

              <li class="smartWrk2"><a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('desktop-logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>Sair</a>

              <form id="desktop-logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>

              </li>

            </ul>

          </li>

          @else

          <!-- <li><a href="{{ url('/login') }}"><i class="fa fa-user-o"></i>Entrar</a></li> -->

          @endif
          
          @else

           <li @if(Request::path() === 'home') class="active"@endif><a href="{{ url('/home') }}">Faça sua busca</a></li>
            <li @if(Request::path() === 'wishlistip') class="active"@endif><a href="{{ url('/wishlistip') }}">Sua seleção</a></li>
            
            <li @if(Request::path() === 'feedback') class="active"@endif><a href="{{ url('/feedback') }}">Avalie seu profissional</a></li>

            <li @if(Request::path() === 'otherservices') class="active"@endif><a href="{{ url('/otherservices') }}">Informações úteis</a></li>
            <!-- <li><a href="{{ url('/invite-friend') }}">Invite a Friend</a></li> -->
          
            <li @if(Request::path() === 'register') class="active"@endif><a class="signup" href="{{ url('/register') }}"><i class="fa fa-plus"></i>&nbsp;</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
              </div>
            </li>

          @endif

      </ul>
    </div>
  </div>

</div>
</div>

<?php
if (Auth::check()) {
  $cLogInUid = Auth::user()->id;
} else {
  $cLogInUid = '';
}
?>
<form name="cLIn" id="cLIn"><input type="hidden" name="chkcLogInUid" id="chkcLogInUid" value="<?php echo $cLogInUid;?>"></form>

<div id="chkppcmpModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Your profile is not completed the below field are mandatory to complete the profile</h4>
      </div>
      <div class="modal-body">

        <p id="clcp_Phone" style="display: none;">Phone</p>
        <p id="clcp_Cpf" style="display: none;">Cpf</p>
        <p id="clcp_MainCat" style="display: none;">Cat</p>
        <p id="clcp_SubCat" style="display: none;">Subcat</p>
        <p id="clcp_Price" style="display: none;">Price</p>
        <p id="clcp_Zipcop" style="display: none;">Zipcode</p>
        <p id="clcp_Address" style="display: none;">Address</p>
        <p id="clcp_City" style="display: none;">City</p>
        <p id="clcp_Neighborhood" style="display: none;">neighborhood</p>
        <p id="clcp_State" style="display: none;">State</p>
        <p id="clcp_Country" style="display: none;">Country</p>
        
      </div>
     
    </div>

  </div>
</div>

<script>
$(document).ready(function(){
    
  $('#ChangeToggle').click(function() {
    $('#navbar-hamburger').toggleClass('hidden');
    $('#navbar-close').toggleClass('hidden');  
  });

/*   var chkcLogInUid = $('#chkcLogInUid').val();
if(chkcLogInUid !='' ) {
        $.ajax({
          url: "{{ url('/home/chklginprofilecomplete') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': chkcLogInUid },
          success: function(data) {
            data=jQuery.parseJSON(data);
            if(data.pStatus!='' && data.pStatus=='Profilenotcompleted') {

              if(data.urole=='Provider') {
                if(data.ucat_id=='' || data.ucat_id==null) {
                  $('#clcp_MainCat').show();
                }
                if(data.usub_id=='' || data.usub_id==null) {
                  $('#clcp_SubCat').show();
                }
                if(data.uprice==0) {
                  $('#clcp_Price').show();
                }
              }

              if(data.ucpf_id=='' || data.ucpf_id==null) {
                $('#clcp_Cpf').show();
              }
              if(data.uphone=='' || data.uphone==null) {
                $('#clcp_Phone').show();
              }
              if(data.uzipcode=='' || data.uzipcode==null) {
                $('#clcp_Zipcop').show();
              }
              if(data.uaddress=='' || data.uaddress==null) {
                $('#clcp_Address').show();
              }
              if(data.ucity=='' || data.uaddress==null) {
                $('#clcp_City').show();
              }
              if(data.uneighborhood=='' || data.uneighborhood==null) {
                $('#clcp_Neighborhood').show();
              }
              if(data.ustate=='' || data.ustate==null) {
                $('#clcp_State').show();
              }
              if(data.ucountry=='' || data.ucountry==null) {
                $('#clcp_Country').show();
              }
              
             // $('#chkppcmpModel').modal('show');
            }
          }
        });
  }*/

  $(document).on("click", "li.listId" , function() {
      var id = $(this).attr('data-value');
      $.ajax({ 
      url: "<?php echo url('/').'/notification_data/update_taker_notification.php'; ?>",
      data: {"id": id, 'providerid': '<?php echo Auth::user()->id;?>'},
      type: 'GET',
      success: function(result)
      {
          
      }
    });
  });
});
/*$('ul li.loginbtn').click( function(){
    if ( $(this).hasClass('callmenu') ) {
        $(this).removeClass('callmenu');
    } else {
        $('li a.current').removeClass('callmenu');
        $(this).addClass('callmenu');    
    }
});*/
$(document).ready(function(){
const $menu = $('.logout_menu');

$(document).mouseup(e => {
   if (!$menu.is(e.target) // if the target of the click isn't the container...
   && $menu.has(e.target).length === 0) // ... nor a descendant of the container
   {
     $menu.removeClass('is-active');
  }
 });

$('.logout_btn').on('click', () => {
  $menu.toggleClass('is-active');
});
});

$(document).ready(function(){
const $menu = $('.noticenotice');

$(document).mouseup(e => {
   if (!$menu.is(e.target) // if the target of the click isn't the container...
   && $menu.has(e.target).length === 0) // ... nor a descendant of the container
   {
     $menu.removeClass('is-active');
  }
 });

$('.quickView').on('click', () => {
  $menu.toggleClass('is-active');
});
});
</script>
<style>
.loginbtn, .logout_menu {
  position: relative;
}

.dropdown-menu {
  display: none;
}

.is-active .dropdown-menu {
  display: block;
}

/*.dropdown-toggle, .dropdown-menu { 
  width: 100%;
}*/

.callmenu>.dropdown-menu {
    display: block;
}
span#notiCount {
  border-radius: 22px;
  color: #fff;
  font-size: 10px;
  height: 18px;
  left: 20px;
  line-height: 19px;
  position: absolute;
  text-align: center;
  top: 10px;
  width: 18px;
}
.user_role_text {
    text-align: left;
    position: absolute !important;
    top: 25px;
    bottom: 0;
    right: 0px;
    left: 34px;
}
#notihCounty {
    border-radius: 22px;
    color: #fff;
    font-size: 10px;
    height: 18px;
    left: 20px;
    line-height: 19px;
    position: absolute;
    text-align: center;
    top: 3px;
    width: 18px;
}
</style>