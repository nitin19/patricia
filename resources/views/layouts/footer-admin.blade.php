    <!-- DataTable script -->
    <!-- <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="{{ url('/public') }}/DataTables-1.10.18/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript" src="{{ url('/public') }}/DataTables-1.10.18/js/dataTables.bootstrap.min.js"></script> -->
<div class="footer_section">

    <div class="content_copy">
     
        <div class="col-sm-6 copyright">
            <p>© 2019 navizinhanca.com</p>
        </div>
        <div class="col-sm-6 sidecontent">
            <p>Designed By : <a href="https://www.binarydata.in/" target="_blank"> Binary Data </a></p>
        </div>
    </div>

</div>
<script>
  jQuery(document).ready(function () {
  	
  	fetchRecords();
  	myInterval = setInterval(function(){
        fetchRecords();
       }, 15000);
    $(".quickView").click(function(e){
       e.preventDefault();
        var url = '<?php url('/'); ?>';
        var tab_attribs = [];
  		$('li.listId').each(function () {
  		  tab_attribs.push( $(this).attr("data-attr") );

  		});
	    //alert(tab_attribs);
       var status = '1';
       $.ajax({
        url: url+'/notification_data/updatenotificationList.php',
        type: 'GET',
        //dataType: 'json',
        data:{ status:status,ids:tab_attribs},
        success: function(response){
        console.log(response);
       
        if(response.notifyCount)
          {  $('#red').show(); 
        $('#notiCount').html('');
        $('#notiCount').html('0');
      }
        $('#loadnotification').html(response.notifyData);
   
     }
   });
   });
  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  	function fetchRecords(){
        var url = '<?php url('/'); ?>';
        $.ajax({
         url: url+'/notification_data/getnotificationList.php',
         type: 'GET',
         dataType: 'json',
         //data:{ senderid:senderid, receiverid:receiverid },
         success: function(response){
         	console.log(response);
         	//var data = JSON.parse(response);
         	//console.log(data);
          if(response.notifyCounts)
            {  $('#red').show(); 
          
         	  $('#notiCount').html('');
            $('#notiCount').html(response.notifyCount);
          }
            $('#loadnotification').html('');
            $('#loadnotification').html(response.notifyData);
         }
       });
    }

});
</script> 

 <script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<!-- <script>
jQuery(document).ready(function(){
$('#findtext li a').on('click', function () {
    $('#findtext li a').removeClass('active');
    $(this).addClass('active');
   
});
});
</script> -->