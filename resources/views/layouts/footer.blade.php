<footer class="site-footer">
  <div class="container">
    <div class="tg-feature">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <!-- <span class="tg-featureicon blueicon"> -->
            <!--  <i class="fa fa-users"></i> -->
            <!-- <img src="{{ url('/public/images/beta.png') }}" alt="Beta version" class="img-responsive">
          </span> -->
          <div class="tg-featurecontent">
            <?php 
  function getIncompleteprofileproviders(){

       $incomprofile_provider_users = DB::table('users')
                                    ->where('user_role', '=', 'Provider')
                                    ->where('is_active', '=', '1')
                                    ->where('is_deleted', '=', '0')
                                    ->where('close_request_action', '0')
                                    ->orderBy('id', 'DESC')
                                    ->get();

        $incomprofile_provider_users_Arr = array();  

        if( count($incomprofile_provider_users) > 0 ) {
         
          foreach($incomprofile_provider_users as $incomprofile_provider_user) {

          $provider_price_list_count = DB::table('provider_price_list')->where('user_id', '=', $incomprofile_provider_user->id)->count();

            if($incomprofile_provider_user->cpf_id =='' || $incomprofile_provider_user->phone =='' || $incomprofile_provider_user->cat_id =='' || $incomprofile_provider_user->sub_id =='' || $incomprofile_provider_user->zipcode =='' || $incomprofile_provider_user->address =='' || $incomprofile_provider_user->city =='' || $incomprofile_provider_user->neighborhood =='' || $incomprofile_provider_user->state =='' || $incomprofile_provider_user->country =='') {

              $incomprofile_provider_users_Arr[] = $incomprofile_provider_user->id;

            }

          }

        } 

      return $incomprofile_provider_users_Arr;                           
  }
          $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT( DISTINCT user_id SEPARATOR ',')) as user_id"))->distinct()->first();
          $provideusers = $providers->user_id;

          $incomprofile_provider_users_Arr = getIncompleteprofileproviders();

          //  DB::enableQueryLog();
          $query = DB::table('users');   
          $query->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority');
      
            $query->join('main_category','users.cat_id','=','main_category.id');   
            $query->join('category','users.sub_id','=','category.cat_id');
            $query->join('provider_price_list','users.id','=','provider_price_list.user_id');
            $query->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
            $query->where('users.user_role', 'Provider');
            $query->where('users.is_active', '1');
            $query->where('users.is_deleted', '0');
            $query->where('users.close_request_action', '0');
            $query->where('price_queries.priority', '1');
            if(Auth::user()){
              $query->where('users.id', '!=', Auth::user()->id);
            }
            if( count($incomprofile_provider_users_Arr) > 0 ) {
              $query->whereNotIn('users.id',$incomprofile_provider_users_Arr);
            }

            /* $query->where(
                function($querys) use ($provideusers){
                  $querys->whereIn('users.id', explode(',', $provideusers));
                  $querys->orWhere('users.user_role', 'Provider');
            }); */

            $query->distinct();
            $AllproviderArr = $query->get();
            $provider_count = count($AllproviderArr);
            //dd(DB::getQueryLog()); exit;
            
                  $business_info = DB::table('business_settings')
                  ->join('users','business_settings.user_id','=','users.id')
                  ->where('users.user_role','=','admin')
                  ->select('business_settings.*','users.user_role','users.id')->first();
              $footer_img = $business_info->footer_logo; ?>
              <h3 style="color:white;">Cadastre-se agora mesmo no NaVizinhança! O seu melhor serviço está aqui!</h3>
                  
              <span style="color:white;">Mais de @if($provider_count>0) {{ $provider_count }}  @endif prestadores de serviço</span>
              <p class="service_provider_footer_text" style="color:white;">Empregada doméstica, faxineira, babá, cozinheira, costureira, cuidador de idoso, adestrador, churrasqueiro, pintor, pedreiro, marido de aluguel, gestor de obras, professor particular... sejam encontrados!</p>
              <p class="service_provider_footer_text" style="color:white;">Nós acreditamos que o sucesso do NaVizinhança depende do número de prestadores de serviços cadastrados no site e do feedback/avaliação dos usuários que contataram ou contrataram esses profissionais. Sendo assim, cadastre-se, utilize nossos serviços e divulgue para seus amigos, sejam eles prestadores de serviços ou contratantes. Divulgando nosso site, você ajuda sua rede de relacionamento, ao mesmo tempo que viabiliza que sua próxima experiência no site seja ainda mais completa, com mais profissionais cadastrados, com mais usuários contratando-os e avaliando-os e com mais funcionalidades para você.</p>
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <br/>
    <div class="container">      
      <div class="row">
        <div class="tg-fourcolumns">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 foot_mar" >
            <div class="tg-footercolumn">
              <?php if(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Provider')){
                  if(!empty(Auth::user()->address)&&!empty(Auth::user()->cat_id)){ $home_url =  url('/home'); } else{ $home_url=url('serviceprovider/completeprofile'); } 
                } elseif(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Taker')){
                  if(!empty(Auth::user()->address)){ $home_url =  url('/'); } else { $home_url=url('servicetaker/completeprofile'); }
                } else{
                   $home_url =  url('/');
                }?>
              <strong class="tg-logo"><a href="{{ $home_url}}"><img src="{{ url('/public/images/') }}/admin-assets/{{ $footer_img }}" alt="image description" height="auto" width="65%"></a></strong>
            </div>
          </div>
         <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" >
            <div class="tg-footercolumn tg-widget tg-widgetusefullinks">
              <div class="tg-widgettitle">
                <h3>NaVizinhança </h3>
              </div>
              <div class="tg-widgetcontent">
                <ul class="screw_text">
                  <?php 
                  if(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Provider')){
                    if(!empty(Auth::user()->address)&&!empty(Auth::user()->cat_id)){ $about_url =  url('/aboutus'); } else{ $about_url=url('serviceprovider/completeprofile'); } 
                  } elseif(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Taker')){
                    if(!empty(Auth::user()->address)){ $about_url =  url('/aboutus'); } else { $about_url=url('servicetaker/completeprofile'); }
                  } else{
                    $about_url =  url('/aboutus');
                  }?>
                  <li><a href="{{$about_url}}">Como trabalhamos</a></li>
                  <li><a href="{{$about_url}}">Quanto pago pela <br> utilização do site?</a></li>

                  <li><a href="{{url('/allcategories')}}">Lista de serviços <br>disponíveis</a></li>
                  
                  <li><a href="{{url('/faq')}}">Perguntas frequentes</a></li>
                  <!-- <li><a href="{{$about_url}}">Versão Beta (em fase de <br> testes)</a></li> -->
                 <!--  <li><a href="{{url('/contactus')}}">Compartilhe conosco <br> sua experiência </a></li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4" >
          <div class="tg-footercolumn tg-widget tg-widgetusefullinks">
            <div class="tg-widgettitle">
              <h3>Alguns serviços disponíveis:</h3>
            </div>
            <div class="tg-widgetcontent">
              <?php
              $category = DB::table('category')
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->take(10)
              ->get();

              if(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Provider')){
                    if(!empty(Auth::user()->address)&&!empty(Auth::user()->cat_id)){ $search_url =  url('/search'); } else{ $search_url=url('serviceprovider/completeprofile'); } 
              } elseif(!empty(Auth::user())&&!empty(Auth::user()->user_role == 'Taker')){
                    if(!empty(Auth::user()->address)){ $search_url =  url('/search'); } else { $search_url=url('servicetaker/completeprofile'); }
              } else{
                    $search_url =  url('/search');
              }?>
              <ul>
               <!--  @foreach($category as $category_info)
                <li><a href="{{ url('/search').'?sub_id='.$category_info->cat_id }}">{{ $category_info->cat_name }}</a></li>
                @endforeach -->
                <li><a href="{{ $search_url.'?sub_id=8' }}">Adestrador</a></li>
                <li><a href="{{ $search_url.'?sub_id=79' }}">Bolos/doces/salgados</a></li>
                <li><a href="{{ $search_url.'?sub_id=73' }}">Costureira</a></li>
                <li><a href="{{ $search_url.'?sub_id=57' }}">Enfermeira</a></li>
                <li><a href="{{ $search_url.'?sub_id=5' }}">Faxineira</a></li>
                <li><a href="{{ $search_url.'?sub_id=14' }}">Manicure/pedicure</a></li>
                <li><a href="{{ $search_url.'?sub_id=70' }}">Mudanças e carretos</a></li>
                <li><a href="{{ $search_url.'?sub_id=64' }}">Pedreiro</a></li>
                <li><a href="{{ $search_url.'?sub_id=61' }}">Pintor</a></li>
                <li><a href="{{ $search_url.'?sub_id=11' }}">Português</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" >
          <div class="tg-footercolumn tg-widget tg-widgetusefullinks">
              <div class="tg-widgettitle">
              <h3>Ajuda</h3>
            </div>
            <div class="tg-widgetcontent">
              <ul class="screw_text">
                @if(empty(Auth::user()))
                <li><a href="{{ url('/register?v=sp') }}">Ofereça seu serviço</a></li>
                @endif
                <li><a href="{{ url('/terms')}}">Termos de uso</a></li>
                <li><a href="{{ url('/privacy_policy')}}">Política de privacidade</a></li>
                <li><a href="{{ url('/').'/sitemap.xml'}}" target="_blank">Mapa do site</a></li>
              </ul>
            </div>
            </div>
          </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 " >
          <div class="tg-footercolumn tg-widget tg-widgetusefullinks">
            <div class="tg-widgettitle">
              <h3>Encontre-nos também aqui</h3>
            </div>
            <ul class="tg-socialicons">
              <li class="tg-facebook"><a href="{{ $business_info->facebook_link }} " target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li class="tg-twitter"><a href="{{ $business_info->instagram_link }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li class="tg-linkedin"><a href="{{ $business_info->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright_text">
        <p align="center"><i class="fa fa-copyright"></i> 2019 NaVizinhança</p>
    </div>
  </div>
  <script>
    window.fbAsyncInit = function() {
        // FB JavaScript SDK configuration and setup
        FB.init({
          appId      : '676198242816683', // FB App ID
          cookie     : true,  // enable cookies to allow the server to access the session
          xfbml      : true,  // parse social plugins on this page
          version    : 'v2.8' // use graph api version 2.8
        });     
    };

    // Load the JavaScript SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Facebook login with JavaScript SDK
    function fbLogin() {
    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
          console.log('hello' +response );
            if (response.status === 'connected') {
                  FB.api(
                  "/me",
                  function (response) {
                    console.log('good to see you' +response.name);
                  }
              );
                //display user data
                getFbUserData();
            }else{
              FB.login(function (response) {
            if (response.authResponse) {
                // Get and display the user profile data

                getFbUserData();
            } else {
                document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        }, {scope: 'email'});
              
            }
        });      
    }
    // Logout from facebook
    function fbLogout() {
       // FB.logout(function(response) {      
          $.ajax({
          url: "{{ url('/facebooklogout')}}",
          type: 'get',
          //dataType: 'json',
          data: $('#logout-form').serialize(),
          success: function(response){
          window.location.reload();
       }
        });
        // });
    }
    // Save user data to the database
    function saveUserData(userData){
      
      $.post('userData.php', {oauth_provider:'facebook',userData: JSON.stringify(userData)}, function(data){ return true; });
    }
    function getFbUserData(){
        FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
        function (response) {
          console.log(response);
         // alert(response);
          if ("email" in response)
          {
            document.getElementById('logininfacebook').innerHTML = 'Logout from Facebook';
            $.ajax({
              type: "POST",
              url: 'facebook_user',
              data: { name:response.first_name, email:response.email, profile_image:response.picture.data.url,facebook_id:response.id, _token: "{{ csrf_token() }}" },
              success: function( msg ) {
                  var baseurl = "{{ url('/')}}"+msg;
                  window.location = baseurl;
                  //exit;
              }
            });
          } else {
              document.getElementById('status').innerHTML = "Sorry! Your facebook account registered with your phone number, first update the email_id on your Pesonal Facebook account and try again."; 
          } 
        });
    }
  </script>
  <script>
  jQuery(document).ready(function () {
    
    fetchRecords();
    myInterval = setInterval(function(){
        fetchRecords();
     }, 15000);
    $(".quickView").click(function(e){
       e.preventDefault();
        var url = "{{ url('/')}}/";
        var tab_attribs = [];
      $('li.listId').each(function () {
        tab_attribs.push( $(this).attr("data-attr") );

      });
      //alert(tab_attribs);
       var status = '1';
        var providerid = $('.p_id').attr('data-id');
        var role = $('.noti_role').attr('data-role');
        if(role == 'Provider')
        {
          var base_url = url+'notification_data/update_providernotification.php';
        } else {
          var base_url = url+'notification_data/update_taker_notification.php';
        }
       $.ajax({
        url: base_url,
        type: 'GET',
        //dataType: 'json',
        data:{ status:status,ids:tab_attribs,providerid : providerid},
        success: function(response){
        //console.log(response);
        $('#notiCount').html('0');
        $('#notiCount').hide();
        $('#loadnotification').html(response.notifyData);
   
     }
   });
   });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function fetchRecords(){
        var url = "{{url('/')}}/";
        var providerid = $('.p_id').attr('data-id');
         var role = $('.noti_role').attr('data-role');
        if(role == 'Provider')
        {
          var base_url1 = url+'notification_data/provider_notification.php';
        } else {
          var base_url1 = url+'notification_data/taker_notification.php';
        }
        $.ajax({
         url: base_url1,
         type: 'GET',
         dataType: 'json',
         data:{ providerid:providerid },
         success: function(response){
          if(response.notifyCount > 0){
            $('#notiCount').show();
            $('#notiCount').html(response.notifyCount);
          }
            $('#loadnotification').html('');
            $('#loadnotification').html(response.notifyData);
         }
       });
    }
});
</script> 
</footer>
