<div class="container-fluid">
  <div class="col-sm-3 col-xs-9 logodiv">
    <a href="{{ url('/') }}"><img src="{{ url('/public') }}/images/yellow_logo_new.png" class="img-responsive"></a>
  </div>
  <div class="col-xs-3 col-sm-8"> 
    <div class="navbar-header navbar-right">
      <button type="button" class="navbar-toggle collapsed fff" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
    </div>
  </div>
  <div class="col-sm-9 col-xs-12 navlinks nopadding">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
          @if(empty(Auth::user()))
          <li ><a href="{{ url('/home') }}">Faça sua busca</a></li>

          @elseif(Auth::user()->user_role == 'Taker')
          <li ><a href="{{ url('/home') }}">Faça sua busca</a></li>
          @else(Auth::user()->user_role == 'Provider'&&empty(Auth::user()->address)&&empty(Auth::user()->phone))
          <li></li>
          @endif
          @if(!empty(Auth::user())&&!empty(Auth::user()->address))
          <li><a href="{{ url('/wishlist') }}">Sua seleção</a></li>
          @elseif(!empty(Auth::user())&&empty(Auth::user()->address))
            <li></li>
          @else
           <li><a href="{{ url('/wishlistip') }}">Sua seleção</a></li>
          @endif
          @if(empty(Auth::user()))
            <li><a href="{{ url('/otherservices') }}">Informações úteis</a></li>
          @elseif(Auth::user()->user_role == 'Taker')
            <li><a href="{{ url('/otherservices') }}">Informações úteis</a></li>
           @else(Auth::user()->user_role == 'Provider')
             <li></li>
          @endif
          <?php
          if(!empty(Auth::user())&&!empty(Auth::user()->address)) {
            ?>
            <li><a href="{{ url('/feedback') }}">Avalie seu profissional</a></li>
            <li><a href="{{ url('/invite-friend') }}">Convidar um amigo</a></li>
            <?php } elseif(!empty(Auth::user())&&empty(Auth::user()->address)){?>
              <li></li>
              <li></li>
            <?php }else{?>
            <li><a href="{{ url('/feedback') }}">Avalie seu profissional</a></li>
            <li><a class="signup" href="{{ url('/register') }}"><i class="fa fa-plus"></i>Cadastre-se</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Link 1</a>
                <a class="dropdown-item" href="#">Link 2</a>
                <a class="dropdown-item" href="#">Link 3</a>
              </div>
            </li><?php } ?>
            <li class="loginbtn text-right <?php
            if($user = Auth::user()) { echo 'logoutcls'; } ?>">
            <?php if($user = Auth::user()) {
              $current_userid = Auth::id();
              $current_username = ucfirst(Auth::user()->name);
              $profile_image = Auth::user()->profile_image;
            ?>
            <a href="{{ url('/accountcomplete') }}" class="dropdown-toggle profile" data-toggle="dropdown" role="button" aria-haspopup="true">
              @if($profile_image=='')
              <img src="{{ url('public/images/')}}/gravtar.jpeg" class="logimg">
              @else
              <img src="{{ url('public/images/')}}/profileimage/{{ $profile_image }}" class="logimg">
              @endif
              {{ $current_username }} <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu mega-dropdown-menu  droplist">
              @if(empty(Auth::user()->address))
              <li></li>
              @else
              <li><a href="{{ url('/dashboard') }}">
                <i class="fa fa-tachometer" aria-hidden="true"></i> 
                painel de controle</a>
              </li>
             @endif
             @if(empty(Auth::user()->address))
              <li></li>
              @else
                <li><a href="{{ url('/changepassword') }}"><i class="fa fa-key" aria-hidden="true"></i> Mudar o Passwoard</a></li>
                @endif
                <li><a  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> sair</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
            <?php } else { ?>
            <a href="{{ url('/login') }}"><i class="fa fa-user-o"></i>Entrar</a>
            <?php } ?>
          </li>
       </ul>
     </div>
    </div>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      $("body").click(function(){
        $("droplist").hide();  
      });
    }
  </script>
