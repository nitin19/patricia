<!-- DataTable Css -->
<!-- <link href="{{ url('/public') }}/DataTables-1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{ url('/public') }}/DataTables-1.10.18/css/dataTables.jqueryui.min.css" rel="stylesheet" type="text/css">
<link href="{{ url('/public') }}/DataTables-1.10.18/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"> -->
<?php
$admin_login_id = Auth::id();
//print_r(Auth::user());die;
$admin_info = DB::table('users')
                 ->where('is_active', '1')
                 ->where('is_deleted', '0')
                 ->where('approved_by_admin', '1')
                 ->where('user_role', 'admin')
                 ->where('id', $admin_login_id)
                 ->first();
//print_r($admin_info);die;
$admin_img = $admin_info->profile_image;
if($admin_img==''){
    $img_profile = "/public/admin/images/gravtar.jpeg";
}
else {
   $img_profile = "/public/admin/images".'/'.$admin_img;
}
$business_setting  = DB::table('business_settings')->where('user_id',Auth::user()->id)->first();
?>
<!--header-->
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <div class="header">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="notificationicons">
               <!-- <ul class="header_div">
                <li class="header-icon dropdown bs">
                  <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"><i class="fa fa-envelope-o"></i><span class="counter blue">11</span></a>
                  <div class="dropdown-menu notifi_dropdown" role="menu" aria-labelledby="drop1">
                    <span class="text-left notifi_title">Messages<a href="#" class="viewlink">View All</a></span>
                    <ul>
                       <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                    </ul>
                  </div>
                </li>
              </ul> -->
               <ul class="header_div">
                <li class="header-icon dropdown bs">
                  <a id="drop1" href="#" class="dropdown-toggle quickView" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"><i class="fa fa-bell-o" aria-hidden="true"></i> <span class="counter red" id = "notiCount" style="display:none;"> </span></a>
                  <div class="dropdown-menu notifi_dropdown" role="menu" aria-labelledby="drop1">
                    <span class="text-left notifi_title">Recent Notifications<a href="{{ url('/') }}/admin/notifications" class="viewlink">View All</a></span>
                     <ul class="" id ="adminloadnotification">
                    </ul>
                  </div>
                </li>
              </ul>
            <!--  <ul class="header_div">
                <li class="header-icon">
                  <a data-toggle="collapse" data-target="#msgnotification"><i class="fa fa-envelope-o"></i><span class="counter blue">11</span></a>
                  <div id="msgnotification" class="collapse notifi_dropdown">
                    <span class="text-left notifi_title">Messages<a href="#" class="viewlink">View All</a></span>
                    <ul>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                      <li><img src="http://navizinhanca.com/public/images/prflimg.png"><span><b>Lorem Ipsum</b><br/>Lorem Ipsum is simply dummy text of the printing.</span></li>
                    </ul>
                  </div>
                </li>
              </ul>
              <ul class="header_div">
                <li class="header-icon">
                  <a data-toggle="collapse" data-target="#notifications" class="quickView"><i class="fa fa-bell-o" aria-hidden="true"></i>
                  <span class="counter red" id = "notiCount"> </span></a>
                  <div  class="collapse notifi_dropdown" id="notifications">
                    <span class="text-left notifi_title"  style="background-color:#cc3333;">Recent Notifications
                      <a href="{{ url('/') }}/admin/notifications" class="viewlink">View All</a></span>
                    <ul class="" id ="loadnotification">
                    </ul>
                  </div>
                </li>
              </ul> -->
              <ul class="header_div">
                <li class="header-icon headerprflimg dropdown">
                  <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" role="button" aria-haspopup="true">
                    <img src="{{ url('/public/images/admin-assets') }}/{{ $business_setting->admin_profile_picture }}" class="img-rounded">
                  </a>
                  <ul class="dropdown-menu profile_dropdown">
                    <span class="text-left notifi_title"></span>
                    <li><a href="/admin-dashboard"><i class="fa fa-tachometer dasicon" aria-hidden="true"></i> Dashboard</a></li>
                    <!-- <li><a href="#">Profile</a></li> -->
                    <li><a href="{{ url('/admin/change-password/') }}"><i class="fa fa-key dasicon" aria-hidden="true"></i> Change Password</a></li>
                    <li><a href="{{ url('/admin/business/create') }}"><i class="fa fa-briefcase dasicon"></i> Business Settings</a></li>
                   <!--  <li><a href="/lockscreen"><i class="fa fa-lock dasicon" aria-hidden="true"></i> Lock Screen</a></li> -->
                    <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out dasicon" aria-hidden="true"></i> Logout</a></li>
                  </ul>
                </li>
              </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <script>
$(document).ready(function(){
  fetchRecords();
    myInterval = setInterval(function(){
        fetchRecords();
     }, 5000);

    function fetchRecords(){
        var url = "{{url('/')}}/";
          var base_url = url+'notification_data/admin_notification.php';
        $.ajax({
         url: base_url,
         type: 'GET',
         dataType: 'json',
         data:{},
         success: function(response){
          if(response.notifyCount > 0){
            $('#notiCount').show();
            $('#notiCount').html(response.notifyCount);
            $('#adminloadnotification').html(response.notifyData);
          } else {
            $('#adminloadnotification').html('There are no more notifications');
            $('#notiCount').hide();
          }
        }
       });
    }


  $(document).on("click", "li.listId" , function() {
      var id = $(this).attr('data-value');
      $.ajax({ 
      url: "<?php echo url('/').'/notification_data/update_adminnotification.php'; ?>",
      data: {"id": id, 'providerid': '<?php echo Auth::user()->id;?>'},
      type: 'GET',
      success: function(result)
      {
      }
    });
  });
});
</script>
