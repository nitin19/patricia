@if (Session::has('success'))
<script>
  var success = "{{ Session::get('success') }}";
  $.notify({
      message: success,
      },{
      type: 'success',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    }).show();
</script>
     @php

       Session::forget('success');

     @endphp
@endif


@if (Session::has('error'))
<script>
  var error = "{{ Session::get('error') }}";
 
  $.notify({
      message: error ,
      },{
      type: 'danger',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
</script>
     @php

       Session::forget('error');

     @endphp
@endif

@if (Session::has('warning'))
<script>
  var warning = "{{ Session::get('warning') }}";
  $.notify({
      message: warning,
      },{
      type: 'warning',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
</script>
     @php

       Session::forget('warning');

     @endphp
@endif


@if (Session::has('info'))
<script>
  var info = "{{ Session::get('info') }}";
  $.notify({
      message: info,
      },{
      type: 'info',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
</script>
     @php

       Session::forget('info');

     @endphp

@endif


@if ($message = $errors->any())
<script>
  $.notify({
      message: Please check the form below for errors,
      },{
      type: 'danger',
      offset: 
      {
        x: 10,
        y: 130
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
    });
</script>
     @php

       Session::forget('message');

     @endphp

@endif
