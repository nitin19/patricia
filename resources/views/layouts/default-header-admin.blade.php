<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/public') }}/images/icon.png" />
    <link href="{{ url('/public') }}/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/style.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/sidebar.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/dashboard.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/table-css.css" rel="stylesheet">

    
    <!-- DataTable Css -->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/public') }}/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/animate.css" rel="stylesheet">
    <link href="{{ url('/public') }}/admin/css/dropdown.css" rel="stylesheet">
    <!--map scripts -->
    <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/mapsjs-ui.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/other.css" />
    <link href="{{ url('/public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">

    <script src="{{ url('/public') }}/admin/js/jquery-1.11.3.min.js"></script>
    <script src="{{ url('/public') }}/admin/js/jquery.min.js"></script>
    <script src="{{ url('/public') }}/admin/js/bootstrap.min.js"></script>
   
    <script src="{{ url('/public') }}/js/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="{{ url('/public') }}/admin/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ url('/public') }}/admin/js/wow.min.js"></script>

    <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-core.js"></script>
    <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-service.js"></script>
    <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-ui.js"></script>
    <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-mapevents.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
<!--Mouse-Cursor-Effect-CSS-->
<link rel="stylesheet" href="Expertly Crafted Gin _ Whitetail Gin_files/2b6749702b507e4f181d42fb4816d2dd.css" as="style" onload="this.onload=null;this.rel=&#39;stylesheet&#39;" data-minify="1">
<!--Mouse-Cursor-Effect-Script-->
<script type="text/javascript" src="Expertly Crafted Gin _ Whitetail Gin_files/jquery.js"></script>

<script type="text/javascript" src="Expertly Crafted Gin _ Whitetail Gin_files/global.min-1544443435.js"></script>
  </head>
  <body id="top" >
     <div class="page-transition loaded">
<nav class="navbar navbar-default navigationbar hhh"> @include('layouts.header-admin') </nav></header>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures wow fadeInLeft">

     @include('layouts.sidebar-admin') </div>
@yield('content')
<footer> @include('layouts.footer-admin') </footer>


<!-- <script type="text/javascript" src="{{ url('/public') }}/js/ckeditor.js"></script> -->

<!-- <script src="http://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script> -->


 
<!-- <script>
    wow = new WOW(  
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      });
</script> -->

<script type="text/javascript">
    $(document).ready(function(){
    $(".list-group a").click(function() {
      $('.list-group a').removeClass('selected');
      $(this).addClass('selected');
    });
});
</script>

<script>
jQuery(function ($) {
jQuery('#uploadbtn').on('click', function() {
          jQuery('#brwsebtn').click();
        });
     jQuery("#brwsebtn").change(function() {
      var file = this.files[0];
      var imagefile = file.type;
      var imageTypes= ["image/jpeg","image/png","image/jpg"];
        if(imageTypes.indexOf(imagefile) == -1)
        {
          jQuery("#previewdiv").html("<span class='msg-error'>Please Select A valid Image File</span><br /><span>Only jpeg, jpg and png Images type allowed</span>");
          return false;
        }
        else
        {
          var reader = new FileReader();
          reader.onload = function(e){
            jQuery("#previewdiv").html('<img src="' + e.target.result + '" style="width:100px;height:100px;"/>');
          };
          reader.readAsDataURL(this.files[0]);
        }
      });
jQuery('#browsebtn').on('click', function() {
    jQuery('#user_images').click();
});
$(function() {
         // Multiple images preview in browser
             var imagesPreview = function(input, placeToInsertImagePreview) {
               if (input.files) {
                   var filesAmount = input.files.length;
                   if(filesAmount <= '5'){
                      for (i = 0; i < filesAmount; i++) {
                          var reader = new FileReader();
                          reader.onload = function(event) {
                            $($.parseHTML('<img style="margin:0 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                          }
                      reader.readAsDataURL(input.files[i]);
                      }
                    }
                    else{
                    $('.img_limit_msg').show();
                  }
                }
                  
             };
             $('#user_images').on('change', function() {
                jQuery('#nofile').empty();
                jQuery('#browse_img').empty();
                imagesPreview(this, '#browse_img');
             });
         });
    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});
$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
});
});
</script>
<style type="text/css">
  .myclass { border:5px solid red;}
</style>
</div>
  </body>
</html>
