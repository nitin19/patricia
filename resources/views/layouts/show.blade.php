
@extends('layouts.default-header')

@section('title', 'Profile')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
/* calender events customized css*/
.calendar {
  width: 650px !important;
  height: 319px;
  position: absolute;
  z-index: 9999;
}
.prev, .next, .today{
  background-color:red;
}

</style>
@section('content')
<section class="maincontent searchbarbg">
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
    <div class="message">
      @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible msgAlert" >
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('message') }}
      </div>
      @endif
    </div>
    <div class="message">
      @if(session()->has('error'))
      <div class="alert alert-danger alert-dismissible msgAlert" >
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('error') }}
      </div>
      @endif
    </div>
    <div class="bannerimg">

      <div class="container">
        <div class="col-sm-2 profile_img">
          <?php
          if($user_info->profile_image!=''){
            ?>
            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $user_info->profile_image; ?>">
            <?php
          }
          else {
            ?>
            <img src="{{ url('/public/') }}/images/profileimage/1542707849.images.png">
            <?php
          }
          $category_name = explode(",", $user_info->category);
          $current_userid = Auth::id();
          $current_username = Auth::user()->name;
          $user_id = $user_info->id;
          $current_userid = Auth::id();
          $current_user_role = Auth::user()->user_role;
          ?>
        </div>
        <div class="col-sm-6 profileinfo">
          <h2>{{ $user_info->category }}</h2>
          <p><b>{{ ucfirst($user_info->name) }}</b> <span>
            <?php 
              if(Auth::user()->profile_rating != ''){
                $number = number_format(Auth::user()->profile_rating,1);
                $integer_part = floor($number);
                $fraction_part = $number-$integer_part;
                for($j=1;$j<=$integer_part;$j++) {?>
                    <i class="fa fa-star ycolor"></i>
                <?php }
                if($fraction_part>0.5){?>
                <i class="fa fa-star-half-o ycolor"></i>
                  <?php
                }

              }
            ?>
          </span><span class="reviews">@if(!empty($feedbacks)) ({{count($feedbacks)}} reviews) @else (No Reviews) @endif</span></p>
          <p class="prflinfo"><span>Address: </span> {{ucfirst($user_info->address) }}</p>
          <p class="prflinfo"><span>Distance: </span>{{number_format($distance,3)}} mi</p>
          <div class="profilebtns">
            @if($user_id!=$current_userid && $current_user_role=='Taker')
            <input type="hidden" name="booking_status" id="booking_status" value="@if(!empty($check_existing_booking)){{$check_existing_booking->booking_verified}}@endif">
            <a href="#" class="yellowbtn prflbtn marbot" data-toggle="modal"  @if($check_existing_booking->booking_verified !=1)   data-target="#exampleModal" @endif data-whatever="@mdo"><i class="fa fa-paper-plane"></i>@if($check_existing_booking->booking_verified == 1) Booked  @else   Book @endif</a>
            @endif
            <div class="greybtn_anchr prflbtn <?php if($wish_list_exist>0){ echo 'removewhishlist_profile'; } else { echo 'addwhishlists'; } ?>" value="{{ $user_info->id }}" id="whishlist_id">
              <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
              <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o'; } ?>" value="{{ $user_info->id }}"></i>@if($wish_list_exist>0) Added to Wishlist @else Add to Wishlist @endif </div>
              <?php

              $paypal_email = 'ravi@49webstreet.com';
              $base_url = url('/');
              $return_url =   $base_url.'/success';
              $cancel_url =  $base_url.'/failure';
              ?>
            </div>
            <div class="modalsection">

              <div class="modal fade booknow_popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <h1 class="text-center">Book With {{ ucfirst($user_info->name) }}</h1>
                    <h4 class="text-center"> {{ ucfirst($user_info->name) }} will need basic details. please fill below</h4>

                    <div class="alert alert-success" id="bokngSuccess"></div>
                    <div class="alert alert-danger" id="bokngError"></div>

                    <div class="modal-body">

                      <!-- <form name="booking_form" id="booking_form"  action="{{url('/profile/booking')}}" method="post">  action="{{url('/provider/booking')}}" -->

                      <form id="booking_form">

                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group datesec">
                              <input type="text" class="form-control" id="startdate" name="startdate" placeholder="Select Start Date" required>
                              <input type="hidden" id="profile_id" name="profile_id" value="{{ $user_id }}">
                              <input type="hidden" id="user_id" name="user_id" value="{{$current_userid}}">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group datesec">
                              <input type="text" class="form-control" id="enddate" name="enddate" placeholder="Select End Date" required>
                            </div>
                          </div>
                        </div>


                      <!--   <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <select name="quantity" id="quantity" required>
                                <option value="" selected>Quantity</option>
                                <option value="1">1 </option>
                                <option value="2">2 </option>
                                <option value="3">3 </option>
                                <option value="4">4 </option>
                                <option value="5">5 </option>
                                <option value="6">6 </option>
                              </select>
                            </div>
                          </div>
                        </div> -->


                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <input value="{{$user_info->main_cat_name}}" name="cat_name" id="cat_name" required readonly class="form-control">

                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <input name ="sub_cat_name" id="sub_cat_name" value="{{$user_info->cat_name}}" required readonly class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <select name="total_amount" class="form-control" id="total_amount">
                                 <option value="" selected disabled>Select Service Price</option>
                                 @if(count($provider_price_lists) > 0)
                                 @foreach($provider_price_lists as $provider_price_list)
                                 <option value="{{$provider_price_list->queries.' - $'. $provider_price_list->amount}}">{{$provider_price_list->queries.' - $'. $provider_price_list->amount}}</option>
                                 @endforeach
                                 @endif
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group datesec">
                              <textarea rows="6" name="booking_msg" id="booking_msg" class="" placeholder="Write your message here" required></textarea>
                            </div>
                          </div>
                        </div>


                    <!--     <div class="row">
                          <div class="col-sm-12">
                            <div class="bookingamt_sec">
                              <div class="col-sm-12 amtsec">
                                <div class="col-sm-8 amtdetails">
                                  <p><span class="select_time">0</span>  × ${{ $user_info->price }}/night × <span class="manynight"></span>night:</p>
                                </div>
                                <div class="col-sm-4 amountdiv total_amt">
                                  <span></span>
                                </div>
                              </div>
                              <div class="col-sm-12 amtsec">
                                <div class="col-sm-8 amtdetails">
                                  <p>Service Fee:</p>
                                </div>
                                <div class="col-sm-4 amountdiv">
                                  <span>$6.00</span>
                                </div>
                              </div>
                              <div class="col-sm-12 totalamtdiv amtsec">
                                <div class="col-sm-8 amtdetails">
                                  <p>Total Amount</p>
                                </div>
                                <div class="col-sm-4 amountdiv withservice">
                                  <span></span>
                                  <input type="hidden" name="total_amount" id="total_amount">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> -->


                        <!-- <input type="submit" value="Book Now"  class="btn btn-success"> -->
                        <!-- <div class="row payment_type"><br>
                        <div class="col-sm-6">
                        <label class="radio_btn_label">Paypal
                        <input type="radio"  id="payment_type_paypal" checked="checked" name="payment_type" value="Paypal">
                        <span class="checkmark"></span>
                      </label>


                    </div>
                    <div class="col-sm-6">
                    <label class="radio_btn_label">Stripe
                    <input type="radio" name="payment_type" id="payment_type_stripe"  value="Stripe">
                    <span class="checkmark"></span>
                  </label>
                  <input type="hidden" name="payment_id" id="payment_id" value="">
                  <input type="hidden" name="error_hide" id="error" value="1">
                </div>
              </div> -->
              <!-- <div class="payment_paypal">
              <input type="hidden" name="business" value="<#?php echo $paypal_email; ?>">
              <input type="hidden" name="cmd" value="_xclick">
              <input type="hidden" name="item_name" value="Payment Form">
              <input type="hidden" name="item_number" id="item_number" value="">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="tax" id="tax" value="" />
              <input type="hidden" name="amount" id="amount" value="" />
              <input type="hidden" name="currency_code" value="USD">
              <input type="hidden" name="rm" value="2">
              <input type="hidden" name="notify_url" value="">
              <input type="hidden" name="no_note" value="1">
              <input name="first_name" id="first_name" type="hidden"  value="{{$current_username}}" />
              <input name="last_name"  id="last_name" type="hidden"  value="{{$current_username}}" />
              <input name="email"  id="email" type="hidden"  value="{{Auth::user()->email}}" />
              <input type='hidden' name='cancel_return' value='<#?php echo $cancel_url;?>'>
              <input type='hidden' name='return' value='<#?php echo $return_url;?>'>
            </div> -->
            <!-- <button class="btn" type="submit">Book Now</button> -->

            <div class="booknowbtn">
              <input type="submit" class="btn" id="booknowBtn" value="Book Now">
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="col-sm-4 durationdiv">
  @if(count($provider_price_lists) > 0)
    @foreach($provider_price_lists as $provider_price_list)
      <h5 class="profile-detail">${{ $provider_price_list->amount.'/'.$provider_price_list->queries }}</h5>
    @endforeach
  @endif
</div>
</div>
</div>
<div class="singlepagecontent">
  <div class="container">
    <div class="col-sm-8 singlepageleft">
      <h2>About {{ ucfirst($user_info->name) }}</h2>
      <p>{{$user_info->bio}}</p>
      <div id="carousel-example-generic" class="carousel slide singlepageslider" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
          <?php
          $count_user_gallery = count($user_gallery);
          $active_count = 1;
          if($count_user_gallery>0){
            foreach($user_gallery as $user_gallery_img){
              ?>
              <div class="item <?php if($active_count==1) { echo 'active'; } ?>">
                <img src="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" alt="...">
              </div>
              <?php
              ++$active_count;
            }
          }
          else {
            ?>
            <div class="item active">
              <img src="{{ url('/public/') }}/images/sliderimg.jpg" alt="...">
            </div>
            <div class="item">
              <img src="{{ url('/public/') }}/images/sliderimg.jpg" alt="...">
            </div>
            <?php
          }
          ?>
        </div>

        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!-- <p>They love to cuddle up on the sofa with us, and they often try and sneak in bed with us when they think we're not looking! (With faces like theirs it's hard to tell them they're not allowed!)
      <br/><br/>
      We welcome all dogs and cannot wait to look after yours! We are very passionate about all dog's, your dogs will feel very comfortable with us, and if they are allowed on the sofas in their own home then they are more than welcome to join us on ours. </p> -->
      <div class="singleprofile_listing">
        <h3>Credentials</h3>
        <ul class="listingitem1">
          <?php if($user_info->address_verified==1){?>
            <li><i class="fa fa-check-circle"></i> Address Verified</li>
          <?php }
          else{?>
            <li><i class="fa fa-times-circle"></i> Address not  Verified</li>
          <?php  }

          ?>
          <?php if($user_info->passed_assesment ==1){?>
            <li><i class="fa fa-check-circle"></i>Passed Assesment</li>
          <?php }
          else{?>
            <li><i class="fa fa-times-circle"></i> Did Not passed assesment</li>
          <?php  }

          ?>
          <?php if($user_info->approved_by_admin ==1){?>
            <li><i class="fa fa-check-circle"></i>Approved By Admin</li>
          <?php }
          else{?>
            <li><i class="fa fa-times-circle"></i> Not Approved By Admin</li>
          <?php  }

          ?>
        </ul>
        <ul class="listingitem2">
          <li>Repeat clients: <span>{{ $repeat_clients }}</span></li>
          <li>Completed bookings: <span>{{ $complete_booking }}</span></li>
          <li>Response rate: <span>100%</span></li>
          <li>Response time: <span>within a few hours</span></li>
   <!--        <li>Cancellation policy for other services:  <span>Flexible</span></li> -->
        </ul>
      </div>
      <div class="singleprofile_listing">
        <h3>Availability</h3>
        <a href="javascript:void(0);"><h4 class="view_cal"></h4></a>
        <div class="event-calendar">
        </div>



        <!--<p>If you have special time requirements, simply let Helen know.</p>-->

        <!--<h5>Babysitter preferred times:</h5>-->

        <!-- <ul class="listingitem1">
          <li><i class="fa fa-check-circle"></i>
            <ul>
              <li></li>
              <li></li>
              <li></li>
            </ul>
          </li>
        </ul> -->

        <ul class="listingitem1">

        @if($get_user_availablity->isCompleted=='1')

          @if($get_user_availablity->openhrs=='1')

          <li> @if($get_user_availablity->day_monday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Monday
            @if($get_user_availablity->day_monday=='1')
               {{$get_user_availablity->monday_start}} - {{$get_user_availablity->monday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_tuesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Tuesday
            @if($get_user_availablity->day_tuesday=='1')
               {{$get_user_availablity->tuesday_start}} - {{$get_user_availablity->tuesday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_wednesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Wednesday
            @if($get_user_availablity->day_wednesday=='1')
               {{$get_user_availablity->wednesday_start}} - {{$get_user_availablity->wednesday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_thursday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Thursday
            @if($get_user_availablity->day_thursday=='1')
               {{$get_user_availablity->thursday_start}} - {{$get_user_availablity->thursday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_friday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Friday
            @if($get_user_availablity->day_friday=='1')
               {{$get_user_availablity->friday_start}} - {{$get_user_availablity->friday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_saturday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Saturday
            @if($get_user_availablity->day_saturday=='1')
               {{$get_user_availablity->saturday_start}} - {{$get_user_availablity->saturday_end}}
               @else
               Closed
               @endif
        </li>

        <li>@if($get_user_availablity->day_sunday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sunday
            @if($get_user_availablity->day_sunday=='1')
               {{$get_user_availablity->sunday_start}} - {{$get_user_availablity->sunday_end}}
               @else
               Closed
               @endif
        </li>

          @else

          <li><i class="fa fa-check-circle"></i>Monday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Tuesday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Wednesday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Thursday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Friday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Saturday 00 - 23:59</li>
          <li><i class="fa fa-check-circle"></i>Sunday 00 - 23:59</li>

          @endif

        @else
        <li><i class="fa fa-times-circle"></i>No availability set by {{$get_user_availablity->name}}</li>
        @endif

        </ul>

      </div>
      @if(!empty($user_info->additional_details))
      <div class="singleprofile_listing">
        <h3>Additional information</h3>
        <ul class="listingitem1">
          <li><i class="fa fa-check-circle"></i>{{$user_info->additional_details}}</li>
          <!--   <li><i class="fa fa-check-circle"></i></li>
          <li><i class="fa fa-check-circle"></i></li> -->
        </ul>
      </div>
      @endif
    </div>
    <div class="col-sm-4 singlepageright">
      @if(count($similar_category_users)>0)
      <div class="singlepagesidebar">
        <h3>Similar{{$user_info->main_cat_name}} </h3>
        @foreach($similar_category_users as $cat_info)
        @if($cat_info->id != $id)
        <div class="babysitter_div">
          <div class="babysitter_img">
            <?php
            if($cat_info->profile_image!=''){
              ?>
              <img src="{{ url('/public/images/profileimage/') }}/<?php echo $cat_info->profile_image; ?>">
              <?php
            }
            else {
              ?>
              <img src="{{ url('/public/images/profileimage/1542707849.images.png') }}">
              <?php
            }
            ?>
          </div>
          <div class="babysitter_text">
            <h4><a href="{{url('/profile/'.$cat_info->id)}}">{{ucfirst($cat_info->name) }}</a></h4>
            <?php  $interger_part = ''; $fraction_part= '';?>
            @foreach($average_rating as $rating)
              @if($cat_info->id == $rating->provider_id)
              <?php
                $number = number_format($rating->avg_rating,1);
                $integer_part = floor($number);
                $fraction_part = $number-$integer_part;
                for($j=1;$j<=$integer_part;$j++) {?>
                    <i class="fa fa-star ycolor"></i>
                <?php }
                if($fraction_part>0.5){?>
                <i class="fa fa-star-half-o ycolor"></i>
                  <?php
                }
                ?>
              @else
              <?php for($i=1;$i<=5;$i++){?> <i class="icon-star-empty"></i>  <?php } ?>
              @endif
              @endforeach
            <h5>{{ $cat_info->address }}</h5>
            <h5>${{ $cat_info->price }} per @if($cat_info->price_type!='') {{ $cat_info->price_type }} @endif</h5>
          </div>
        </div>
        @endif
        @endforeach
      </div>
      @else
      <div class="singlepagesidebar"></div>
      @endif
      <div class="singlepagesidebar faqsec">
        <h3>FAQs</h3>
        <ul>
          <li><a href="#">What is a babysitter?</a></li>
          <li><a href="#">How does it works?</a></li>
          <li><a href="#">What services are available?</a></li>
          <li><a href="#">How do I book?</a></li>
          <li><a href="#">Can I meet the babysitter before the booking starts?</a></li>
          <li>More questions? Check all our <a href="#">FAQs</a></li>
        </ul>
      </div>
    </div>
    <div class="singleprofile_listing">
      <h3>Clients Reviews (@if(!empty($feedbacks)){{count($feedbacks)}}@else 0 @endif reviews)</h3>
      <div class="review_section">
        @if(!empty($feedbacks))
        @foreach($feedbacks as $feedback)
        <div class="reviewblk">
          <div class="row">
            <div class="clientimg">
              <img class="user_image" src="@if(!empty($feedback->profile_image)) {{ url('/public/images/profileimage/'.$feedback->profile_image) }}@else {{url('/public/images/gravtar.jpeg')}} @endif">
            </div>
            <div class="client_text nopadding">
              <h4 class="user_name">{{$feedback->name}}</h4>
              <p class="feedback_message">{{$feedback->message}}</p>
              <h5 class="feedback_date">{{$startdate = date("d-M-Y", strtotime($feedback->created_at))}}</h5>
              <input type="hidden" name="page" value="2" id="page_number">
              <span><?php
                $number = number_format($feedback->avg_rating,1);
                $integer_part = floor($number);
                $fraction_part = $number-$integer_part;
                for($j=1;$j<=$integer_part;$j++) {?>
                    <i class="fa fa-star ycolor"></i>
              <?php }
                if($fraction_part>0.5){?>
                <i class="fa fa-star-half-o ycolor"></i>
              <?php

            }
              ?></span>
            </div>
          </div>
        </div>
        @endforeach
        @else
        <h2 align="center"> Sorry! No Reviews</h2>
        @endif
      </div>
      <div class="loadbtn">
        <?php if(count($feedbacks)==5){?>
          <a data-id="{{$user_info->id}}" class="load_more">Load More</a>
        <?php }?>
      </div>
    </div>
  </div>
</div>
<div id="map" style="width: 100%; height: 400px; background: grey" />
<script  type="text/javascript" charset="UTF-8" >
var lat = '<?php echo $user_info->latitude; ?>';
var lan = '<?php echo $user_info->longitude; ?>';
/**
* Adds a circle over New Delhi with a radius of 1000 metres onto the map
*
* @param  {H.Map} map      A HERE Map instance within the application
*/
function addCircleToMap(map){

  map.addObject(new H.map.Circle(
    // The central point of the circle
    {lat: lat, lng: lan},
    // The radius of the circle in meters
    1000,
    {
      style: {
        strokeColor: 'rgba(55, 85, 170, 0.6)', // Color of the perimeter
        lineWidth: 2,
        fillColor: 'rgba(0, 128, 0, 0.7)'  // Color of the circle
      }
    }
  ));
}
/**
* Boilerplate map initialization code starts below:
*/

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
  app_id: 'devportal-demo-20180625',
  app_code: '9v2BkviRwi9Ot26kp2IysQ',
  useHTTPS: true
});
var pixelRatio = window.devicePixelRatio || 1;
var defaultLayers = platform.createDefaultLayers({
  tileSize: pixelRatio === 1 ? 256 : 512,
  ppi: pixelRatio === 1 ? undefined : 320
});

//Step 2: initialize a map - this map is centered over New Delhi
var map = new H.Map(document.getElementById('map'),
defaultLayers.normal.map,{
  center: {lat: lat, lng: lan},
  zoom: 13,
  pixelRatio: pixelRatio
});

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);

// Now use the map as required...
addCircleToMap(map);
</script>
</div>
</section>
<script type="text/javascript">
$(document).ready(function(){

    $('#bokngSuccess').hide();
    $('#bokngError').hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   $('#booknowBtn').click(function(e) {
        e.preventDefault();
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var cat_name = $('#cat_name').val();
        var sub_cat_name = $('#sub_cat_name').val();
        var booking_msg = $('#booking_msg').val();
        var profile_id = $('#profile_id').val();
        var user_id = $('#user_id').val();
        var total_amount = $('#total_amount').val();
        var baseUrl  = '<?php echo url('/');?>';

        if(startdate=='') {
         $('#startdate').addClass('vlderror');
         return false;
        }

        if(enddate=='') {
         $('#enddate').addClass('vlderror');
         return false;
        }

        if(booking_msg=='') {
         $('#booking_msg').addClass('vlderror');
         return false;
        }

        $.ajax({
           type:'POST',
           url:baseUrl+'/booking',
           data:{ startdate:startdate, enddate:enddate, cat_name:cat_name, sub_cat_name:sub_cat_name, booking_msg:booking_msg, profile_id:profile_id, user_id:user_id, total_amount:total_amount },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.msgCode == 1) {
                $('#bokngSuccess').html(data.message);
                $('#bokngSuccess').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                        $("#bokngSuccess").fadeTo(500, 0).slideUp(500, function () {
                            $('#exampleModal').modal('hide');
                            //window.location.href="<?php echo url('/');?>/profile/"+profile_id;
                        });
                    }, 2000);

              } else {

                $('#bokngError').html(data.message);
                $('#bokngError').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                    $("#bokngError").fadeTo(500, 0).slideUp(500, function () {
                        $('#exampleModal').modal('hide');
                    });
                }, 3000);

              }
           }
        });
     });


$("#startdate").focusout(function(){
  $(this).removeClass('vlderror');
});

$("#enddate").focusout(function(){
  $(this).removeClass('vlderror');
});

$("#booking_msg").focusout(function(){
  $(this).removeClass('vlderror');
});

   $('#startdate').focusout(function() {
       // e.preventDefault();
       // alert('I am ready start date');
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var profile_id = $('#profile_id').val();
        var user_id = $('#user_id').val();
        var baseUrl  = '<?php echo url('/');?>';

        if(enddate=='') {
         $('#enddate').addClass('vlderror');
         return false;
        }

        $.ajax({
           type:'POST',
           url:baseUrl+'/checkbookingavailability',
           data:{ startdate:startdate, enddate:enddate, profile_id:profile_id, user_id:user_id },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.msgCode == 1) {

                $('#booknowBtn').prop('disabled', false);

                $('#bokngSuccess').html(data.message);
                $('#bokngSuccess').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                        $("#bokngSuccess").fadeTo(500, 0).slideUp(500, function () {
                            $(this).hide();
                            window.location.href="<?php echo url('/');?>/profile/"+profile_id;
                        });
                    }, 3000);

              } else {

                $('#booknowBtn').prop('disabled', true);

                $('#bokngError').html(data.message);
                $('#bokngError').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                    $("#bokngError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).hide();
                    });
                }, 3000);

              }
           }
        });
     });

      $('#enddate').focusout(function() {
       // e.preventDefault();
      // alert('I am ready end date');
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var profile_id = $('#profile_id').val();
        var user_id = $('#user_id').val();
        var baseUrl  = '<?php echo url('/');?>';

        if(startdate=='') {
         $('#startdate').addClass('vlderror');
         return false;
        }

        $.ajax({
           type:'POST',
           url:baseUrl+'/checkbookingavailability',
           data:{ startdate:startdate, enddate:enddate, profile_id:profile_id, user_id:user_id },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.msgCode == 1) {

                $('#booknowBtn').prop('disabled', false);

                $('#bokngSuccess').html(data.message);
                $('#bokngSuccess').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                        $("#bokngSuccess").fadeTo(500, 0).slideUp(500, function () {
                            $(this).hide();
                          //  window.location.href="<?php echo url('/');?>/profile/"+profile_id;
                        });
                    }, 3000);

              } else {

                $('#booknowBtn').prop('disabled', true);

                $('#bokngError').html(data.message);
                $('#bokngError').show();

                $('#exampleModal').animate({
                  scrollTop: 0
                }, 'slow');

                window.setTimeout(function () {
                    $("#bokngError").fadeTo(500, 0).slideUp(500, function () {
                        $(this).hide();
                    });
                }, 3000);

              }
           }
        });
     });


  /*jQuery('.payment_paypal').hide();
  var payment_type = jQuery(".payment_type input[type='radio'][name='payment_type']:checked").val();
  if(payment_type=='Paypal'){
    jQuery('.payment_paypal').show();
  }
  jQuery(".payment_type input:radio").click(function (){
    var payment_type = jQuery(".payment_type input[type='radio'][name='payment_type']:checked").val();
    if(payment_type=='Paypal'){
      jQuery('.payment_paypal').show();
    }
  });*/




  /** Onchnage Amount Create ***/
/*  jQuery("#quantity, #startdate, #enddate").on('focusout change', function (){
    var start= $("#startdate").val();
    var end= $("#enddate").val();
    var startDay = new Date(start);
    var price = '<?php echo $user_info->price; ?>';
    var endDay = new Date(end);
    var millisecondsPerDay = 1000 * 60 * 60 * 24;

    var millisBetween = endDay.getTime() - startDay.getTime();
    var days = millisBetween / millisecondsPerDay;
    var days = Math.floor(days);
    var quantity = jQuery('#quantity').val();
    var total_amt = days*quantity*price;
    jQuery('.select_time').html(quantity);
    jQuery('.manynight').html(days);
    jQuery('.total_amt span').html('$'+total_amt+'.00');
    var withservice = total_amt+6;
    jQuery('#total_amount').val(withservice);
    jQuery('.withservice span').html('$'+withservice+'.00');
  });*/
  /** Onchnage Amount Create ***/

  /** Jquery Date Picker ***/

  jQuery(function () {

    $('#startdate,#enddate').datetimepicker({
      useCurrent: false,
      format: 'DD-MM-YYYY',
      minDate: moment()
    });
    $('#startdate').datetimepicker().on('dp.change', function (e) {
      var incrementDay = moment(new Date(e.date));
      incrementDay.add(0, 'days');
      $('#enddate').data('DateTimePicker').minDate(incrementDay);
      $(this).data("DateTimePicker").hide();
    });

    $('#enddate').datetimepicker().on('dp.change', function (e) {
      var decrementDay = moment(new Date(e.date));
      decrementDay.subtract(0, 'days');
      $('#startdate').data('DateTimePicker').maxDate(decrementDay);
      $(this).data("DateTimePicker").hide();
    });

  });


  /* load more feedback to on click load more button*/
  $('.load_more').on('click',function(){
    var page = $('#page_number').val();
    var id=$(this).data('id');``
    var base_url = '<?php echo url('/');?>';
    var url = base_url+'/load_more_feedbacks/'+id+'?page='+page;
    var profile_url ='';
    $.ajax({
      type:'GET',
      url:url,
      dataType:'json',
      success:function(data){
        // alert(page);
        page = ++page;
        $('#page_number').val(page);
        for(var i=0;i<=data.count;i++){
          $.each(data.info, function() {
            $.each(this, function(k, v) {
              if(v.profile_image ==null){
                 profile_url = base_url+'/public/images/gravtar.jpeg';
              }
              else{
                  profile_url = base_url+'/public/images/profileimage'+v.profile_image;
              }
              var feedback_html = '<div class="reviewblk"><div class="row"> <div class="clientimg"> <img class="user_image" src='+profile_url
              +' > </div><div class="client_text nopadding"><h5>'+v.name+'</h5><p>'+v.message+'</p>';

              feedback_html+='<h5>';
              for(var j=0;j< v.rating;j++){
                feedback_html+='<i class="fa fa-star"></i>';
              }
                feedback_html+='</h5></div></div></div>';
              if(k < data.count){
                $('.review_section').append(feedback_html);
              }
            });
          });

        }
      },
    });
  })



  /* emd ajax code*/


  /** Jquery Date Picker ***/

  /** Booking Form And payment Method **/
  //   var validFrom=false;
  //   jQuery("#booking_form").validate({
  //     rules: {
  //       startdate: {
  //        required: true
  //      },
  //      enddate: {
  //        required: true
  //      },
  //      quantity:{
  //       required: true
  //     },
  //     booking_msg:{
  //       required: true
  //     }
  //   },
  //   messages: {
  //    startdate: {
  //     required: "This field is required"
  //   },
  //   enddate: {
  //     required: "This field is required"
  //   },
  //   quantity:{
  //     required: "This field is required"
  //   },
  //   booking_msg:{
  //     required: "This field is required"
  //   }
  // },

  // submitHandler: function(form) {

  //    var startdate =    jQuery("#startdate").val();
  //    var enddate =    jQuery("#enddate").val();
  //    var booking_msg =    jQuery("#booking_msg").val();
  //    var total_amount =    jQuery("#total_amount").val();
  //    var profile_id = jQuery('#profile_id').val();
  //    var user_id = jQuery('#user_id').val();
  //    var cat_name =jQuery('#cat_name').val();
  //    var payment_type = jQuery(".payment_type input[type='radio'][name='payment_type']:checked").val();
  //    var base_url = '<#?php url('/')?>';
  //    jQuery.ajax({
  //     url: 'payment',
  //     type: 'POST',
  //     data: {'startdate':startdate, 'enddate':enddate, 'booking_msg':booking_msg, 'payment_type':payment_type, 'total_amount':total_amount,'_token': "{{ csrf_token() }}", 'user_id': user_id, 'profile_id': profile_id, 'cat_name': cat_name },
  //     success: function(data) {
  //      data=jQuery.parseJSON(data);
  //      if(data!='') {
  //       jQuery('#item_number').prop('value', data);
  //       jQuery('#amount').prop('value', total_amount);
  //       validFrom=true;
  //       if(payment_type=='Paypal'){
  //         jQuery("#booking_form").attr('action', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
  //       }
  //       else {
  //         jQuery('#payment_id').prop('value', data);
  //         jQuery("#booking_form").attr('action', base_url+'/stripe');
  //       }
  //       form.submit();
  //     } else {
  //       alert('Something went wrong. Please try again.');
  //     }
  //   }
  // });
  //    return false;
  //}
  //});
  /** Booking Form And payment Method **/
  //
  // $('.prflbtn').on('click',function(){
  //   alert($('#booking_status').val());
  //   if($('#booking_status').val() == 0){
  //     $('.prflbtn').text('Pending');
  //   }
  //
  // });


});
</script>
<style type="text/css">
.greybtn_anchr {
  background: #696969 !important;
  color: #fff;
  background: red none repeat scroll 0 0;
  border-radius: 5px;
  box-shadow: 1px 3px 12px 0 #919191;
  font-family: Lato;
  font-size: 18px;
  font-weight: bold;
  margin-right: 10px;
  padding: 15px 30px;
}
.prflbtn { float: left; cursor: pointer; }
.payment_type { color: #898989; font-size: 20px; }
.vlderror { border: 2px solid red !important; }
#bokngSuccess { opacity: 1 !important; }
#bokngError { opacity: 1 !important; }
@media screen and (max-width:991px) {
  .greybtn_anchr {padding: 10px 15px;font-size:14px;}
  .profilebtns a {padding: 10px 15px;font-size: 14px;}
  .profilebtns a i {font-size: 14px;}
}
</style>
@stop
