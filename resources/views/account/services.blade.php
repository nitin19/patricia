@extends('layouts.default-header')

@section('title', 'Home')
<style>
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
</style>
@section('content')
<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
   foreach($currentuser_ip as $currentuser_info) {
     $wish_profile_id = $currentuser_info->wish_profile_id;
     $currentuser_count = DB::table('wish_list')
     ->where('user_id', $login_userid)
     ->where('wish_profile_id', $wish_profile_id)
     ->count();
     if($currentuser_count==0 || $currentuser_count==''){
       $insert_whish = DB::table('wish_list')->insert(
         ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
       );
     }
   }
 }
 ?>
 @if(session()->has('message'))
 <div class="alert alert-success profle_alert">
     {{ session()->get('message') }}
 </div>
 @endif
 <!--<div id="dialog-message" title="My Dialog Alternative">-->
   <!--<p style='color:red'> Hello world </p>-->
   <section class="maincontent searchbarbg">

     @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
     <div class="alert alert-danger alert-dismissible fade in profle_alert">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>Alert!!</strong> please Complete Your Profile.
     </div>
     @endif
     <div class="col-sm-12 pagecontent whitebgdiv nopadding">
      <div class="container">
        <div class="acccompletionsec profile-accountcomplete">
          <h1>Account Completion</h1>
          <div class="acccompletion_tabs">
            <ul class="nav nav-tabs" role="tablist">
                <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
                @if($user_role=='Provider')
                <li role="presentation"  class=" @if (Session::has('success'))
                @endif" ><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
                @endif
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">

          @if($user_role=='Provider')
        <div role="tabpanel" class="tab-pane  active " id="services" >
          <div class="col-sm-12 form-group profileform_right">
            <form name="complete_service" action="{{ url('/service') }}" id="complete_service" class="complete_service"  method="post">
              {{ csrf_field() }}
              <input type="hidden" class="services_action" value="{{ url('/accountcomplete/service') }}">
              <div class="row">
                <div class="form-group col-md-6">
                  <label>Choose Category Here</label>
                  <select name="main_categroy_id" id="main_category" class="form-control" required>
                    <option value="" selected disabled="disabled">Please choose one</option>
                    @foreach($main_category as $main_cat_id=>$main_cat_name)
                    <option value="{{$main_cat_id}}" @if($main_cat_id== $user_info->cat_id) selected @endif>{{$main_cat_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <input type="hidden" class="sub_cat_check_address" value ="{{ url('/check_subcategory/') }}">
                  <label>Choose Sub Category Here</label>
                  <select name="sub_categroy_id"  class="form-control" id="sub_category" required>
                    <option value="{{$user_info->sub_cat_id}}">{{$user_info->cat_name}}</option>
                  </select>
                  </div>

                  <div class="row pricing_fields">
                    <div class="form-group col-md-12"><h3>Pricing</h3><hr></div>
                     <div class="form-group col-md-2">
                       <h4>Per Day</h4>
                  </div>
                  <div class="form-group col-md-6">
                        <input type="text" class="input-lg" value ="">
                  </div>
                </div>
                <div class="row pricing_fields">
                  <div class="form-group col-md-2">
                       <h4>Per Week</h4>
                  </div>
                   <div class="form-group col-md-6">
                        <input type="text" class="input-lg" value ="">
                  </div>
                </div>
                <div class="row pricing_fields">
                  <div class="form-group col-md-2">
                       <h4>Per Month</h4>
                  </div>
                   <div class="form-group col-md-6">
                        <input type="text" class="input-lg" value ="">
                  </div>
                </div>
                </div>
                <div class="col-sm-12 nextbtn">
                  <input class="form-control save " value="Save" type="submit">
                </div>
              </form>
            </div>
          </div>
          @endif

        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>

</main>
<!--<script-->
<!--  src="https://code.jquery.com/jquery-3.3.1.min.js"-->
<!--  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="-->
<!--  crossorigin="anonymous"></script>-->
<script>
  $(document).ready(function(){
      // if($("input[name=phone]").val() == 'N/A' || $("input[name=address]").val() == 'N/A'){
      //     alert('please complete your profile');
      // }
// $('.hides1').hide();
   // $('.hides2').hide();
    // $( "#dialog-message" ).dialog({
    //       modal: true,
    //       buttons: {
    //       Ok: function() {
    //         $( this ).dialog( "close" );
    //       }
    // }});
   // $('.hides1').hide();
   // $('.hides2').hide();
  //  jQuery('#store_availablity').validate({
  //    rules: {
  //     date: {
  //       required: true,
  //     },
  //
  //   },
  //   messages: {
  //     date: {
  //       required: "This field is required",
  //     }
  //   },
  // });
    // $('#main_category').multiselect();

    // $('#demo').multiselect();




  var base_url = '<?php echo url('/'); ?>';

  $( "#address" ).on('blur',function() {
        //   alert('fdg');
        if ($( "#address" ).val().length == 0 ) {
          $('#zipcode').val('');
          $('#country option').text('');
          return;
        }
        var address_val = $('#address').val();
        var val =  $(this).val();
        var url = '/convert_address/'+address_val;
        var data  = $('#completionform').serialize();
            //console.log(data);
            $.ajax({
              type: 'GET',
              url: url,
              dataType: 'json',
              success: function(data ){
                console.log(data.res.zipcode);
                if(data.success = "true"){
                  $('#zipcode').val(data.res.zipcode);
                  $('#country option').text(data.res.country_name);
                }
              },
              error: function(data){
               alert('error');
             }
           });
          })

  $('.datetimepicker1').datetimepicker({
   format: 'DD/MM/YYYY'
 });
  $('#main_category').on('change',function(){
    var url = $('.sub_cat_check_address').val();
    // alert(url);
    var data = $('#complete_service').serialize();
    // var url = '/accountcomplete/check_subcategory/';
    $.ajax({
      type:"POST",
      dataType:'JSON',
      url:url,
      data:data,
      success:function(data){
        $('#sub_category').empty();
        if(data != ''){
          $.each(data,function(k,v){
                            // $('#sub_category option').val(k);
                            // $('#sub_category option').text(v);
                            // Create option
                            console.log(k);
                            var option = $("<option />");
                            option.attr("value", k).text(v);
                            $('#sub_category').append(option);
                          });
        }
        else{
          var option = $("<option />");
          option.attr("value", '0').text('Sorry data not found');
          $('#sub_category').append(option);
        }

      }
    })
  })
  // $('.next_1').on('click',function(){
  //   var addRess = $('.proAddress').val();
  //   var profileNumbers = $('.profileNumber').val();
  //   if(addRess == 'N/A' || addRess == '' ){
  //     var msgss ='Please enter the address.';
  //     $('.msgAlert').show();
  //     $('.msgAlertVal').html(msgss);
  //     }if(profileNumbers == 'N/A' || profileNumbers == ''){
  //       var msgss1 ='Please enter the valid phone number.';
  //       $('.msgAlert').show();
  //       $('.msgAlertVal').html(msgss1);
  //   }
  // });
})
</script>
@stop
