@extends('layouts.default-header')

@section('title', 'Home')
<style>
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
</style>
@section('content')
<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
   foreach($currentuser_ip as $currentuser_info) {
     $wish_profile_id = $currentuser_info->wish_profile_id;
     $currentuser_count = DB::table('wish_list')
     ->where('user_id', $login_userid)
     ->where('wish_profile_id', $wish_profile_id)
     ->count();
     if($currentuser_count==0 || $currentuser_count==''){
       $insert_whish = DB::table('wish_list')->insert(
         ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
       );
     }
   }
 }
 ?>
 @if(session()->has('message'))
 <div class="alert alert-success profle_alert">
     {{ session()->get('message') }}
 </div>
 @endif
 <!--<div id="dialog-message" title="My Dialog Alternative">-->
   <!--<p style='color:red'> Hello world </p>-->
   <section class="maincontent searchbarbg">

     @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
     <div class="alert alert-danger alert-dismissible fade in profle_alert">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>Alert!!</strong> please Complete Your Profile.
     </div>
     @endif
     <div class="col-sm-12 pagecontent whitebgdiv nopadding">
      <div class="container">
        <div class="acccompletionsec profile-accountcomplete">
          <!--<h1>Account Completion</h1>-->
          <div class="acccompletion_tabs">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation"  class=" @if (!Session::has('success')) active @endif">
                <a href="#profile" aria-controls="profile"  role="tab" data-toggle="tab">Profile</a></li>
                <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane
                active"
               id="profile">
                <form class="completionform" id="completionform" name ="profileFormData" action="{{ url('/accountcomplete/update/') }}" method="post"   enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" class="profile_action" value="{{ url('/accountcomplete/update/') }}">
                  <div class="row">
                    <div class="col-sm-4 profileimage">

                     <div class="form-group imagediv" >
                       <div class="imagedivborder">
                        <div class="imagebrowse" id="previewdiv">
                          <?php
                          if($profile_image==''){
                            ?>
                            <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="img-circle" style="width: 100%;height: 100%;">
                            <?php
                          }
                          else {
                            ?>
                            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-circle" style="width: 100%;height:100%;">
                            <?php
                          }
                          ?>
                          <div class="nopadding imgbtns">
                            <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i>
                          </div>

                        </div>
                        <h5 class="usrnam">{{ucfirst($user_info->name)}}</h5>
                        <h6 class="usreml">{{$user_info->email}}</h6>
                        <div class="col-md-3 prflinput">
                          <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-8 profileformmainright">
                    <div class="row">
                      <!--Alert Div -->
                      <div class="alert alert-warning alert-dismissible msgAlert" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="msgAlertVal"></strong>
                      </div>
                      <div class="col-sm-6 profileform_right">
                        <div class="col-sm-12">

                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Name : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="name" id="name" class="form-control readcolr" value="<?php  echo  $user_info->name;?>" placeholder="" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Email Address : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="email" name="email" id="email" class="form-control readcolr" value="<?php echo  $user_info->email;?>" placeholder="" required>

                              <input type="hidden" name="user_id" value="{{ $user_info->id }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Phone Number : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="phone" id="phone" class="form-control readcolr profileNumber" value="<?php echo  $user_info->phone;?>" placeholder="" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');" required>
                              <!--    <i class="fa fa-check phone"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">User Role : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <select class="form-control readcolr" name="user_role" disabled="true">
                                <option vlaue="">Select your option</option>
                                <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Provider</option>
                                <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Taker</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Additional Details : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="additional_details" id="phone" class="form-control readcolr" value="<?php  echo $user_info->additional_details; ?>" placeholder="Enter Addition Details">
                              <!--<i class="fa fa-check additional"></i>-->
                            </div>
                          </div>
                        </div>


                      </div>

                      <div class="col-sm-6 profileform_right">

                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Add Bio : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <textarea name="bio" id="bio" class="form-control readcolr" required><?php echo  $user_info->bio;?></textarea>
                              <!--<i class="fa fa-check bio"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Address : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <textarea name="address" id="address" class="form-control readcolr proAddress" required><?php  echo  $user_info->address;?></textarea>
                              <!--<i class="fa fa-check address"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Zip Code : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="zipcode" id="zipcode" class="form-control readcolr" value="<?php  echo  $user_info->zipcode;?>" placeholder="" disabled="true">
                              <!--<i class="fa fa-check zipcode"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Country : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <select class="form-control readcolr" name="country" id="country" disabled="true">
                                <option value=""></option>>
                                <?php
                                foreach($countries as $country_name){
                                  ?>
                                  <option value="<?php echo $country_name; ?>" <?php if($user_info->country==$country_name) { echo 'selected';} ?>><?php echo $country_name; ?></option>
                                  <?php
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>



                      </div>
                    </div>
                    <div class="col-sm-12 form-group forminput profileform_right radimate">
                      <div class="col-sm-3">
                       <label>Upload Unameser Image</label>
                     </div>
                     <div class="col-sm-9 browseimage nopadding" id="browse_img">
                       <div class="">
                        @if(count($user_image) > 0)
                        @foreach ($user_image as $user_images)

                        <div class="col-sm-3 single_img" id="prv_{{ $user_images->image_id }}">
                         <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a>
                         <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" style="margin-bottom: 10px !important;">
                       </div>


                       @endforeach
                       @else
                       <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                       @endif
                     </div>
                   </div>
                   <div class="col-sm-3">

                   </div>
                   <div class="col-sm-9 col-xs-9 new_up_sec">
                    <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
                    <div class="browse_btn" id="browsebtn">
                      <span>Select Images</span>
                      <!--<p>No File Selected</p>-->
                    </div>
                  </div>
                </div>
              @if($user_role=='Provider')
                <div class="col-sm-12 nextbtn text-right">
                  <input type="submit" class="form-control next_1" value="Next Step" disabled>
                </div>
                @else
                <div class="col-sm-12 nextbtn text-right">
                  <input type="submit" class="form-control next_1" value="Next Step" disabled>
                </div>
                @endif

              </div>
            </div>
          </form>
        </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>

</main>
<!--<script-->
<!--  src="https://code.jquery.com/jquery-3.3.1.min.js"-->
<!--  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="-->
<!--  crossorigin="anonymous"></script>-->
<script>
  window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
  $(document).ready(function(){

// $('.hides1').hide();
   // $('.hides2').hide();
    // $( "#dialog-message" ).dialog({
    //       modal: true,
    //       buttons: {
    //       Ok: function() {
    //         $( this ).dialog( "close" );
    //       }
    // }});
   // $('.hides1').hide();
   // $('.hides2').hide();
   jQuery('#store_availablity').validate({
     rules: {
      date: {
        required: true,
      },

    },
    messages: {
      date: {
        required: "This field is required",
      }
    },
  });
    // $('#main_category').multiselect();

    // $('#demo').multiselect();




  var base_url = '<?php echo url('/'); ?>';

  $( "#address" ).on('blur',function() {
        //   alert('fdg');
        if ($( "#address" ).val().length == 0 ) {
          $('#zipcode').val('');
          $('#country option').text('');
          return;
        }
        var address_val = $('#address').val();
        var val =  $(this).val();
        var url = '/convert_address/'+address_val;
        var data  = $('#completionform').serialize();
            //console.log(data);
            $.ajax({
              type: 'GET',
              url: url,
              dataType: 'json',
              success: function(data ){
                console.log(data.res.zipcode);
                if(data.success = "true"){
                  $('#zipcode').val(data.res.zipcode);
                  $('#country option').text(data.res.country_name);
                  // if($("#address").val() != ''&&  $("#country").val() != ''&& $("#zipcode").val() != '') {
                    $('.next_1').removeAttr('disabled')
                  // }
                }
              },
              error: function(data){
               alert('error');
             }
           });
          })

  $('.datetimepicker1').datetimepicker({
   format: 'DD/MM/YYYY'
 });
  $('#main_category').on('change',function(){
    var url = $('.sub_cat_check_address').val();
    // alert(url);
    var data = $('#complete_service').serialize();
    // var url = '/accountcomplete/check_subcategory/';
    $.ajax({
      type:"POST",
      dataType:'JSON',
      url:url,
      data:data,
      success:function(data){
        $('#sub_category').empty();
        if(data != ''){
          $.each(data,function(k,v){
                            // $('#sub_category option').val(k);
                            // $('#sub_category option').text(v);
                            // Create option
                            console.log(k);
                            var option = $("<option />");
                            option.attr("value", k).text(v);
                            $('#sub_category').append(option);
                          });
        }
        else{
          var option = $("<option />");
          option.attr("value", '0').text('Sorry data not found');
          $('#sub_category').append(option);
        }

      }
    })
  })
  // $('.next_1').on('click',function(){
  //   var addRess = $('.proAddress').val();
  //   var profileNumbers = $('.profileNumber').val();
  //   if(addRess == 'N/A' || addRess == '' ){
  //     var msgss ='Please enter the address.';
  //     $('.msgAlert').show();
  //     $('.msgAlertVal').html(msgss);
  //     }if(profileNumbers == 'N/A' || profileNumbers == ''){
  //       var msgss1 ='Please enter the valid phone number.';
  //       $('.msgAlert').show();
  //       $('.msgAlertVal').html(msgss1);
  //   }
  // });
})
</script>
@stop
