<?php $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','register')->where('is_active', '1')->where('is_deleted', '0')->first();?>

@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
@include('layouts.flash-message')
<script>
 var vcc = function(g_recaptcha_response) {
 var $captcha = $( '#recaptcha' );
 
 $( '.msg-error' ).text('');
    $captcha.removeClass( "error" );
 };
</script>
<script src="https://www.google.com/recaptcha/api.js?explicit&hl=pt"></script>
<div class="formsection">
  <div class="container">
    <div class="formdiv loginformdiv row">
      <div class="col-xs-12 col-sm-8 col-md-7 col-lg-5 centered">
        <div class="registerformbg formbg"> 
          @include('layouts.notify-message')
          <form class="form-horizontal registerform formarea" method="POST" id="registerform" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h1 class="text-center  flipInY">Cadastre-se</h1> 
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}  forminput  fadeInLeft">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="nome completo"  autofocus>
              @if ($errors->has('name'))
              <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}  forminput  fadeInLeft">
                <input id="email" type="email" class="form-control msgmy" name="email" value="{{ old('email') }}"  placeholder="e-mail" >
                @if ($errors->has('email'))
                <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
                <span class="checkmark"></span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  forminput">
                <input id="password" type="password" class="form-control" name="password" id="password" placeholder="senha" >
                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                @if ($errors->has('password'))
                <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group forminput">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="confirme sua senha" required>
                <span toggle="#password-confirm" class="fa fa-fw fa-eye field-icon toggle-password">
              </div>
              <div class="form-group role_div">
                <div class="col-sm-12 rolediv">Deseja se cadastrar como:</div>
                  <?php $params   = $_SERVER['QUERY_STRING'];?>
                  @if($params)
                  <div class="col-sm-12 user_roles1 registerradio">
                    <label class="radio_btn_label">Prestador de serviço 
                      <input type="radio" checked="checked" name="user_role" value="Provider">
                      <span class="checkmark redme"></span>
                    </label>
                    <label class="radio_btn_label">Contratante
                      <input type="radio"  name="user_role" value="Taker">
                      <span class="checkmark redme"></span>
                    </label> 
                 </div> 
                  @else
                  <div class="col-sm-12 user_roles1 registerradio">
                      <label class="radio_btn_label">Prestador de serviço 
                        <input type="radio"  name="user_role" value="Provider">
                        <span class="checkmark redme"></span>
                      </label>
                      <label class="radio_btn_label">Contratante
                        <input type="radio" checked="checked" name="user_role" value="Taker">
                        <span class="checkmark redme"></span>
                      </label> 
                  </div> 
                  @endif    
              </div>
              <div class="form-group ">
                <label class="checkboxdiv inlog">Eu li e concordo com os termos de uso.
                  <span class="checkmark"> </span>
                  <input type="checkbox" name="agree" id="agree" checked required>
                  <span class="checkmark"></span>
                </label>
              </div> 
              <input type="hidden" name="ispvrgfrm" value="no">
              <div class="form-group">
              <div id="recaptcha" class="g-recaptcha" data-sitekey="6LdLBKgUAAAAANJRN953JtHdibxJA4AeGMJZtSQv" data-lang ="pt-BR" data-callback="vcc"></div>
               <span class="msg-error" style="color: red;"></span>
             </div>
              <div class="form-group buttondiv ">
                <input type="submit" class="btn btn-primary btn-lg " id="sbt"  value="Cadastre-se">
              </div>
              <div class="form-group formlink ">
                <span>Já possui uma conta? Clique aqui para </span>
                <a href="{{ url('/login') }}">ENTRAR</a>
              </div>
          </form>
          <!-- Facebook login or logout button -->
          <p style="text-align: center;"><a class="btn btn-primary btn-large Facebookbtn" style="width: 100%;" href="javascript:void(0);" onclick="fbLogin()" id="logininfacebook"><i class="fa fa-facebook-square" aria-hidden="true"></i> Entrar com Facebook</a></p>
          <!-- Display user profile data -->
          <div id="userData"></div>
        </div> 
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  #recaptcha *, #recaptcha label {width: 100% !important;} 
</style>
<script type="text/javascript">
  $('body').click(function() {
  var empeml = $('.msgmy').val();
  if(empeml == '') {
  $('#email-error').hide();
   }
   });  
  $(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z\d=!\-@._*#$%^&]*$/.test(value)
       && /[a-z]/.test(value) && /[A-Z]/.test(value)
       && /\d/.test(value)
});

 // $('#sbt').click(function(e){
 //  $( '.msg-error').text();
 //      if( $('#recaptcha').find("span").html();{
 //        e.preventDefault();
 //        return false;
 //      }
 //  });
 // $('#sbt').click(function(){
  $('form').on('submit', function(e) {
    var check = $( '.msg-error').text();
    if(check){
      e.preventDefault();
      return false;
    }
    $captcha = $( '#recaptcha' );
    response = grecaptcha.getResponse();

    if (response.length === 0) {
      $( '.msg-error').text("Marque a caixa para confirmar que você é um humano");

      if( !$captcha.hasClass( "error" ) ){
          $captcha.addClass( "error" );
          e.preventDefault();
         //$('#sbt').attr('disabled','disabled');
        return false;
      } else {
        $( '.msg-error' ).text('');
        $captcha.removeClass( "error" );
        //$('#sbt').removeAttr('disabled');
      }
    }
  });
  
      $.validator.addMethod("alphaLetter", function(value, element) {
        return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
      });
var hostname = " {{ url('/') }}";
jQuery('#registerform').validate({
  rules: {
          name:{
            required: true,
            alphaLetter: true,
            minlength: 7
          },
          user_role: {
            required: true
          },
          password: {  
            required: true,
            minlength: 6
          },
          password_confirmation: { 
            required: true,
            equalTo: "#password"
          },
          agree: { 
            required: true
          },
          email: {
            required: true,
            email: true,
            remote: {
            url: hostname+"/register/emailcheck",
            type: "post",
            data: { email: function(){ return $('input[name=email]').val(); },
                      _token: function(){ return $('input[name=_token]').val(); }
                    },
            dataFilter: function(data) {
                console.log(data);
                if(data == 'Exists'){
                  return false;
                }else{
                  return true;
                }
              }
            }

          }
         },
         messages: {
          name: {
                required: "Favor preencher o campo acima.",
                alphaLetter: "Por favor, insira o valor do caractere.",
                minlength: "Digite pelo menos 7 caracteres."
            },
          user_role: {
                required: "Favor, selecione o papel do usuário"
            },
            password: {
                required: "Favor preencher o campo acima.",
                minlength: "A senha precisa ter ao menos 6 caracteres (letras e/ou números)."
            },
            password_confirmation: {
               required: "Favor preencher o campo acima.",
               equalTo: "A senha nao é a mesma da senha digitada no campo acima, favor corrigir."
            },
            agree: {
               required: "Favor preencher o campo acima."
            },
            email: {
              required:"Favor digitar um endereço de email válido.",
              remote:"Esse email já está cadastrado, acesse a opção 'Entrar'.",
              email: "Digite um endereço de e-mail válido."
            }
         },
    });


// });
</script>
<style>
/*.radio_btn_label .checkmark:after {
  top:5px  !important;
  left:5px !important;
}*/
.checkmark.redme {
    background-color: #ccc !important;
    padding: 8px;
}
/*.radio_btn_label input:checked~.checkmark {
    border: 5px solid #39c;
}*/
.buttondiv {
    text-align: center;
}
.checkmark {
    position: absolute;
    top: 2px;
    left: 0;
    background-color: transparent !important;
    border-radius: 50%;
    padding: 4px;
}
/*.radio_btn_label .checkmark:after {
  background-color: transparent !important;
}*/
.formarea .forminput>input {
  position: relative;}
form#registerform .form-group.forminput {
  margin-bottom: 30px;
}
.forminput .error {
    color: red;
    font-size: 13px;
    position: absolute;
}
.inlog .checkmark:after {
    left: 7px;
    top: 3px;
}
/*.form-horizontal .form-group {display: unset;}*/
.radio_btn_label {
    padding-left: 25px;
    font-size: 14px;
    font-weight: 300;
    margin-right: 5px;
}
/*.checkboxdiv .checkmark {height: 20px;width: 20px;}*/
.buttondiv {margin-top: 0px;}
.formlink {text-align: center;margin-top: 20px;}
.formlink > span {
    color: #323334;
    font-family: Lato;
    font-size: 15px;
}
.formbg {padding: 30px 30px;}
.formarea p {margin-bottom: 30px;}
@media screen and (max-width:767px) {
  .rolediv {margin-bottom: 10px;}
  .registerformbg.formbg {
    padding: 40px 30px;
}
}
@media screen and (max-width: 480px){ 
.formbg {margin: 60px 0;padding: 40px 10px;}
}
   
    #name::placeholder, #email::placeholder, #password::placeholder, #password-confirm::placeholder {
  color: #666666 !important;
}

#name:-ms-input-placeholder, #email:-ms-input-placeholder, #password:-ms-input-placeholder, #password-confirm:-ms-input-placeholder {
  color: #666666 !important;
}

#name::-ms-input-placeholder, #email::-ms-input-placeholder, #password::-ms-input-placeholder, #password-confirm::-ms-input-placeholder {
  color: #666666 !important;
}
#password-confirm, #name, #email, #password {
  color: #666666 !important;
}  
</style>
@endsection