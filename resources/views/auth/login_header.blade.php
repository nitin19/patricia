<?php
$business_info = DB::table('business_settings')
                ->join('users','business_settings.user_id','=','users.id')
                ->where('users.user_role','=','admin')
                ->select('business_settings.*','users.user_role','users.id')->first();
$header_img=$business_info->header_logo;
?>

<div class="container-fluid">
  <div class="col-sm-4 col-xs-9 col-md-2 logodiv" data--delay="0.5s">
    <a href="{{ url('/') }}"><img src="{{ url('/public/images') }}/admin-assets/{{ $header_img }}" class="img-responsive"></a>
  </div>
  <div class="col-xs-3 col-sm-8">
    <div class="navbar-header navbar-right">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
  </div>
  <div class="col-sm-9 col-xs-12 navlinks nopadding" data--delay="0.6s">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        @if(empty(Auth::user()))
        <li ><a href="{{ url('/home') }}">Find Service Provider</a></li>
        <!-- <li><a href="{{ url('/wishlistip') }}">WishList</a></li> -->
        <li><a href="{{ url('/otherservcie') }}">Other Services</a></li>
        <li><a href="{{ url('/feedback') }}">Feedback</a></li>
        <!-- <li><a href="{{ url('/invite-friend') }}">Invite a Friend</a></li> -->
        <li class="provider_service"><a href="{{ url('/provider/register') }}">Offer Your Services</a></li>
        <li><a class="signup" href="{{ url('/register') }}"><!-- <i class="fa fa-plus"></i> -->Sign Up</a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Link 1</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
          </div>
        </li>
        <li><a href="{{ url('/login') }}"><i class="fa fa-user-o"></i> Login</a></li> 
        @endif
    </ul>
  </div>
</div>

</div>
<script type="text/javascript">

    $(document).ready(function() {
    $("li").click(function () {
        $("li").removeClass("activeadd");
        $("li").addClass("activeadd");        
    });
    });

</script>
