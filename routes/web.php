<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.home');
// });

Route::get('/aboutus', function () {
    return view('pages.about');
});

//login path
Route::get('auth/facebook', 'AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'AuthController@handleProviderCallback');
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::post('facebook_user', 'SocialAuthFacebookController@logindb');

//resend activation link
Route::get('register/confirm/resend/{id}','Auth\ResendVerification@resendVerification');

//Route::get('/', 'HomeController@coming_soon');
Route::get('/', 'HomeController@index');
Route::post('register/emailcheck', 'Auth\RegisterController@checkEmailEx');
Route::group(['middleware' => ['lockscreen']], function(){
Route::get('/home', 'HomeController@index')->name('home');
});
Route::get('/coming_soon','HomeController@coming_soon');//coming soon route
Route::POST('/subscribe/newsletter','HomeController@subscribe_newsletter');//route for subscribe newsletter
Auth::routes();
// Route::group(['middleware' => ['role']], function(){
//Route::POST('/image', 'HomeController@image');
Route::group(['middleware' => ['lockscreen']], function(){
  Route::resource('/profile', 'ProfileController');
});
Route::POST('/profile/payment', 'ProfileController@payment');

//Route::POST('/provider/booking', 'ProfileController@provider_booking');// rooute for booking provider without payment
Route::get('/switchaccount', 'ProfileController@switchuserrole');
Route::POST('booking', 'ProfileController@provider_booking');
Route::POST('checkbookingavailability', 'ProfileController@checkbookingavailability');
Route::get('mybooking', 'ProfileController@getmybooking');

Route::get('checkspavability', 'FeedbackController@check_avability');
// view for thanku page
Route::get('/thankumsg', function () { return view('thanku'); });
 //view error page
Route::get('/errorfile', function () { return view('errorpage.error'); });

Route::get('/load_more_feedbacks/{id}/','FeedbackController@load_more_feedbacks');//route for load more feedback
//vies for help folder
Route::get('/faq', 'FaqController@index');
/*Route::get('/faq', function () { return view('helps.faq'); });*/
/*Route::get('/cities_served', function () { return view('helps.cities_served'); });*/
Route::get('/terms', function () { return view('pages.terms'); });
Route::get('/privacy_policy', 'PrivacypolicyController@index');
//Route::get('/privacy_policy', function () { return view('helps.privacy_policy'); });
Route::get('/site_map', function () { return view('helps.site_map'); });
/*Route::get('/be_a_diyist', function () { return view('helps.be_a_diyist'); });*/


Route::get('/provider/register', 'ProviderregisterController@index');
Route::get('/success', 'SuccessController@index');
Route::get('/failure', 'FailureController@index');
Route::resource('/forgotpassword', 'ForgotpasswordController');
Route::POST('/forgotpassword/store', 'ForgotpasswordController@store');
Route::get('/wishlist', 'WishlistController@index');
Route::get('/wishlistip', 'WishlistipController@index');
Route::resource('/search', 'SearchController');
Route::get('/success/test','TestsuccessController@testing_success');//route for testing
Route::get('/subcategory_filter/{id}', 'SearchController@subcategory_filter');//filters subcategory on search page
Route::group(['middleware' => ['lockscreen']], function(){
Route::POST('/search/infoexist', 'SearchController@infoexist');
Route::POST('/wishlist/addwhish', 'WishlistController@addwhish');
Route::POST('/wishlistip/addwhish_ip', 'WishlistipController@addwhish_ip');
Route::POST('/wishlistip/removewhish_ip', 'WishlistipController@removewhish_ip');
// Route::get('/contact', 'ContactusController@index');
Route::POST('/feedback/send','FeedbackController@send');
Route::POST('/feedback/create','FeedbackController@create');
Route::get('/feedback', 'FeedbackController@index');
Route::get('/contactus', 'FeedbackController@feedback');
Route::get('/feedback/booking_details/{user_id}/{provider_id}','FeedbackController@acc_to_users_feedback_list');
Route::get('/feedback_list', 'FeedbackController@feedback_list');
Route::POST('/contact/send', 'ContactusController@send');
Route::POST('/invite-friend/send', 'Invite_friendController@send');
Route::resource('/invite-friend', 'Invite_friendController');
Route::POST('/wishlist/removewhish', 'WishlistController@removewhish');
Route::resource('/accountcomplete', 'AccountcompleteController');
Route::POST('/user_document/delete', 'AccountcompleteController@delete_document');

Route::resource('/service', 'ServiceController');

  Route::post('/savequeryprice', 'ServiceController@savequeryprice');
  Route::post('/updatequeryprice', 'ServiceController@updatequeryprice');
//Route::POST('/accountcomplete/address_converter/', 'AccountcompleteController@address_converter');
});
// Route::get('/convert_address/{address}','AccountcompleteController@convert_address');

// Route::resource('/dashboard', 'DashboardController');

Route::POST('/dashboard/update_account', 'DashboardController@update_account');
Route::resource('/user', 'UserController');
Route::get('user/{id}', 'UserController@show');


Route::POST('/dashboard/save_card', 'DashboardController@save_card');

Route::POST('/dashboard/switchdashboard', 'DashboardController@switchdashboard');

// Route::POST('/dashboard/closeaccount', 'DashboardController@closeaccount');

Route::POST('/dashboard/notifications', 'DashboardController@destroyNotification');

Route::POST('/providerbooking/status', 'Serviceprovider\ServiceproviderdashboardController@approved_status');//route for status of booking by provider

Route::POST('/accountcomplete/update/', 'AccountcompleteController@update');
Route::POST('/check_subcategory/', 'ServiceController@check_subcategory');
Route::get('subcategory_queries/', 'ServiceController@subcategory_queries');
Route::POST('/accountcomplete/wishlistip/', 'AccountcompleteController@wishlistip');
Route::get('/otherservices', 'OtherserviceController@index');
Route::get('/service/detail/{id}', 'OtherserviceController@show');
Route::get('/accountcomplete/delete_image','AccountcompleteController@delete_image');
Route::resource('/stripe', 'AddMoneyController');
Route::get('/stripe', 'AddMoneyController@payWithStripe');
Route::post('/stripe', 'AddMoneyController@postPaymentWithStripe');
Route::group(['middleware' => ['lockscreen']], function(){
Route::resource('/changepassword', 'ChangepasswordController');
Route::PUT('/change-new-password/update/{email}','ChangepasswordController@update_new_password');//password update route when user clicks on password route
Route::get('/change-new-password/{email}', 'ChangepasswordController@change_new_password');
Route::POST('/changepassword/update', 'ChangepasswordController@update');
Route::get('/admin-dashboard', 'AdmindashboardController@index');
Route::resource('/admin-feedback','AdminFeedbackController');
Route::POST('/admin/approve-feedback','AdminFeedbackController@approve_feedback');
Route::POST('/admin/denied-feedback','AdminFeedbackController@denied_feedback');
Route::get('/admin/feedback-export','AdminFeedbackController@excel_export');
Route::get('/admin-invite', 'AdmininviteController@index');
Route::get('/admin-invite/excel', 'AdmininviteController@export_excel');
Route::resource('/admin-users', 'AdminusersController'); //get list of all users
Route::get('/admin-provider', 'AdminusersController@provider'); //get list of all provider
Route::get('/admin-providers/{cat_id}/{sub_id}', 'AdminusersController@provider'); //get list of all provider
Route::get('/admin-provider/excel', 'AdminusersController@excel_export_provider');

Route::get('/admin-provider/incomplete-profile', 'AdminusersController@getIncompleteprofileproviders');
Route::get('/admin-provider/complete-profile', 'AdminusersController@getcompleteprofileproviders');
Route::get('/admin-provider/complete-profile-mail', 'AdminusersController@sendcompleteprofilemsg');

Route::get('/admin-taker/incomplete-profile', 'AdminusersController@getIncompleteprofiletakers');
Route::get('/admin-taker/complete-profile-mail', 'AdminusersController@sendcompleteprofiletakermsg');

Route::get('/admin-taker', 'AdminusersController@taker');//get list of all taker
Route::get('/admin-taker/excel', 'AdminusersController@excel_export_taker');

Route::get('/admin-provider/resendemail/{id}','AdminusersController@resendVerificationEmail');
Route::get('/admin-taker/resendemail/{id}','AdminusersController@resendVerificationEmail');

Route::get('/admin-inactiveusers', 'AdminusersController@Inactiveusers');
Route::get('/admin-inactiveusers/resendemail/{id}','AdminusersController@resendVerificationEmailInactive');
Route::get('/admin-inactiveusers/activateuser/{id}','AdminusersController@ActivateUserAc');
Route::get('/admin-inactiveusers/excel', 'AdminusersController@excel_export_inactiveusers');

Route::POST('/admin/usersdestroy', 'AdminusersController@destroy');
Route::POST('/admin-users/approved_address', 'AdminusersController@approved_address');
Route::get('/admin-users/edit/{id}', 'AdminusersController@edit');
Route::get('/admin-users/show/{id}', 'AdminusersController@show');
Route::get('/admin_users/approve/{user_id}/{doc_id}', 'AdminusersController@approved_document');
Route::get('/admin_users/dispprove/{user_id}/{doc_id}', 'AdminusersController@disapproved_document');
Route::POST('/admin-users/update', 'AdminusersController@update');
Route::POST('/check_subcat/', 'AdminusersController@check_subcat');
Route::get('/admin-orders', 'AdminorderController@index');
Route::POST('/admin-orders/approved_address', 'AdminorderController@approved_address');
Route::get('/admin-orders/excel', 'AdminorderController@excel_export');
Route::get('/admin-mails', 'AdminmailboxController@index');
Route::post('/sendadminmsg', 'AdminmailboxController@sendadminmsg');
Route::get('/admin-switch', 'AdminswitchController@index');
Route::POST('/admin-mails/destroy', 'AdminmailboxController@destroy');
Route::resource('/admin-services', 'AdminserviceController');
Route::POST('/admin-services/store', 'AdminserviceController@store');
Route::POST('/admin-services/category_remove', 'AdminserviceController@category_remove');
Route::POST('/admin-other-services/service_position', 'AdminOtherServiceController@set_service_position');
Route::POST('/admin-other-services/category_remove', 'AdminOtherServiceController@service_remove');
});
Route::get('/admin-services/edit/{id}', 'AdminserviceController@edit');
Route::get('/allcategories', 'FrontserviceController@showallcategories');
Route::POST('/admin-services/update', 'AdminserviceController@update');
Route::POST('/admin-other-services/update', 'AdminOtherServiceController@update');
Route::resource('/admin-subservices', 'AdminsubserviceController');
Route::POST('/admin-subservices/store', 'AdminsubserviceController@store');
Route::POST('/admin-subservices/subcategory_remove', 'AdminsubserviceController@subcategory_remove');
Route::get('/admin-subservices/edit/{id}', 'AdminsubserviceController@edit');
Route::get('/admin-subservices/create/{id}', 'AdminsubserviceController@create');
Route::get('/admin-subservices/show/{id}', 'AdminsubserviceController@show');
Route::POST('/admin-subservices/update', 'AdminsubserviceController@update');
//view of subcategory price page
Route::get('admin/subcategory_price/{subcat_id}', 'AdminSubCatPriceController@index');
Route::post('admin/subcategory_price/save', 'AdminSubCatPriceController@save');
Route::post('admin/subcategory_price/changepriority', 'AdminSubCatPriceController@changepriority');
Route::post('admin/subcategory_price/deletesubcat', 'AdminSubCatPriceController@deletesubcat');
//view of menu details
Route::get('admin-menu-details', 'AdminSiteMenuController@index');
Route::POST('/admin-menu-details/store', 'AdminSiteMenuController@store');
Route::get('/admin-menu-details/edit/{id}', 'AdminSiteMenuController@edit');
Route::POST('/admin-menu-details/update', 'AdminSiteMenuController@update');
Route::POST('/admin-menu-details/remove', 'AdminSiteMenuController@menu_detail_remove');

Route::resource('/admin-other-services','AdminOtherServiceController');
Route::group(['middleware' => ['lockscreen']], function(){

  Route::get('/admin-closerequest', 'AdmincloserequestController@index');
  Route::POST('/requestaction', 'AdmincloserequestController@requestaction');
  Route::get('/error', 'ErrorController@index')->name('error');
  Route::get('/500', 'ErrorfiveController@index')->name('500error');
  Route::get('/404', 'ErrorfourController@index')->name('404');
});

//Route for avaiablity controller
Route::post('/availablity/store','AvailablityController@store_availbility');
// Logout
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');


//route for admin subcategory controller
Route::resource('/admin/main-category/','AdminMainCategoryController');
Route::get('/admin/main-category/edit/{id}','AdminMainCategoryController@edit');
Route::POST('/admin/main-category/update', 'AdminMainCategoryController@update');
Route::POST('/admin/main-category/destroy', 'AdminMainCategoryController@destroy');
Route::get('/admin/main-category/show', 'AdminMainCategoryController@show');
Route::resource('/admin/change-password/','AdminchangepasswordController');//route for admin change pasword
Route::POST('/admin-change-password/','AdminchangepasswordController@password_update');//route for admin change pasword

//route for admin testimonials controller
Route::resource('/admin/testimonials/','AdminTestimonialsController');
Route::get('/admin/testimonials/edit/{id}','AdminTestimonialsController@edit');
Route::POST('/admin/testimonials/update', 'AdminTestimonialsController@update');
Route::POST('/admin/testimonials/destroy', 'AdminTestimonialsController@destroy');
Route::get('/admin/testimonials/show', 'AdminTestimonialsController@show');
Route::POST('/admin/testimonials/store', 'AdminTestimonialsController@store');
Route::POST('/admin/testimonials/testimonial_remove', 'AdminTestimonialsController@testimonial_remove');


// Route::get('/lockscreen', function () {
//   return view('admin.lockscreen');
// });
// Route::get('/provider/lockscreen', function () {
//   return view('serviceprovider.lockscreen');
// });
// Route::get('/taker/lockscreen', function () {
//   return view('servicetaker.lockscreen');
// });//route for business setting
Route::resource('/admin/business','AdminBusinessController');
Route::resource('/admin/notifications','AdminNotificationsController');//route for notification setting

Route::POST('/admin/notifications/destroy', 'AdminNotificationsController@destroy');


Route::get('/admin/messages', function () {
  return view('admin.messages', ['name' => 'notify']);//route for business setting
})->middleware('auth');
Route::get('/get_notifications',function(){
  return view('admin.notification_list');
});
/*Route::get('/availablty',function(){
  return view('availablity.index');
});*/
Route::get('/advertisement',function(){
  return view('pages.advertisement');
});
/* AdminBlackuser list routes*/
Route::resource('/admin-blackusers','AdminblackusersController');
Route::POST('/admin-blackusers/status','AdminblackusersController@requestaction');
Route::get('/admin/blacklist-export','AdminblackusersController@excel_export');

// Route::group(['middleware' => ['auth'],'prefix' => 'serviceprovider'], function(){
//     Route::get('/profile', 'Serviceprovider\ServiceproviderController@index');
//     Route::get('/completeprofile', 'Serviceprovider\ServiceproviderController@completeProfile');
// });

// Route::group(['middleware' => ['auth','lockscreen'],'prefix' => 'serviceprovider'], function(){
//   Route::get('/profile', 'Serviceprovider\ServiceproviderController@index');
// });

Route::group(['middleware' => ['auth','lockscreen'],'prefix' => 'serviceprovider'], function(){

 Route::get('/profile', 'Serviceprovider\ServiceproviderController@index');
  Route::POST('/updateprofile', 'Serviceprovider\ServiceproviderController@update');
  Route::get('/spavailablty', 'Serviceprovider\SpavailabilityController@index');
  Route::POST('updatespavailablty', 'Serviceprovider\SpavailabilityController@updatedata');
  Route::POST('savespavailablty', 'Serviceprovider\SpavailabilityController@savedata');
  Route::POST('/update/profile', 'Serviceprovider\ServiceproviderController@profile_update');
  Route::get('/services', 'ServiceController@user_services');
  /*********myrouteni25nov19*******/
  Route::post('/updatephone', 'Serviceprovider\ServiceproviderController@profile_phone_update');
  Route::post('/updatename', 'Serviceprovider\ServiceproviderController@profile_name_update');
  Route::post('/updatecpf', 'Serviceprovider\ServiceproviderController@profile_cpf_update');
  Route::post('/updaterg', 'Serviceprovider\ServiceproviderController@profile_rg_update');
  Route::post('/updatemaincat', 'Serviceprovider\ServiceproviderController@profile_maincategory_update');
  Route::post('/updatesubcat', 'Serviceprovider\ServiceproviderController@profile_subcategory_update');
  
// Route::post('/uploaddocument', 'Serviceprovider\ServiceproviderController@uploaddocument');
  /*********myrouteni25nov19end*******/
  // route for closeaccount
  Route::POST('/dashboard/closeaccount', 'Serviceprovider\ServiceproviderdashboardController@closeaccount');
  Route::get('/completeprofile', 'Serviceprovider\ServiceproviderController@incomplete_profile');

  Route::get('/completeservice', 'ServiceController@incomplete_service');
  
  Route::get('/dashboard', 'Serviceprovider\ServiceproviderdashboardController@index');
  Route::get('/bookings', 'Serviceprovider\ServiceproviderdashboardController@bookings_messages');
  Route::get('/account/details', 'Serviceprovider\ServiceproviderdashboardController@dashboard_profile_account');
  Route::get('/switch/role', 'Serviceprovider\ServiceproviderdashboardController@user_swicth_role');
  Route::get('/card/details', 'Serviceprovider\ServiceproviderdashboardController@dashboard_profile_card_deatils');
  Route::get('/notifications/details', 'Serviceprovider\ServiceproviderdashboardController@dashboard_notifications');
  Route::get('/closeaccount', 'Serviceprovider\ServiceproviderdashboardController@account_close_request');

  Route::get('/booking/pending', 'Serviceprovider\ServiceproviderdashboardController@bookings_pending');
  Route::get('/booking/declined', 'Serviceprovider\ServiceproviderdashboardController@bookings_declined');
  Route::POST('/providerbooking/status/approved', 'Serviceprovider\ServiceproviderdashboardController@approved_status');//route for status of 
  Route::POST('/providerbooking/status/denied', 'Serviceprovider\ServiceproviderdashboardController@denied_status');
  Route::get('/convert_address/{address}','Serviceprovider\ServiceproviderController@convert_address');
});

Route::POST('/blockprovider/create','BlockproviderController@create');
/* Blackuser list routes*/
Route::POST('/addblackuser','BlackUsersController@create');

/*Route::group(['middleware' => ['auth','lockscreen'],'prefix' => 'incomplete'], function(){
  Route::get('/profile', 'Serviceprovider\ServiceproviderController@incomplete_profile');
  Route::get('/service','ServiceController@incomplete_service');
});*/

// Route::group(['middleware' => ['auth','lockscreen'],'prefix' => 'servicetaker'], function(){
//   Route::get('/profile', 'Servicetaker\ServicetakerController@index');
// });

Route::group(['middleware' => ['auth','lockscreen'],'prefix' => 'servicetaker'], function(){
  Route::get('/profile', 'Servicetaker\ServicetakerController@index');
  Route::get('/completeprofile', 'Servicetaker\ServicetakerController@incomplete_profile');
  Route::POST('/updateprofile', 'Servicetaker\ServicetakerController@update');
  Route::get('/dashboard', 'Servicetaker\ServicetakerdashboardController@index');
  Route::get('/bookings', 'Servicetaker\ServicetakerdashboardController@booking_message');
  Route::get('/account/details', 'Servicetaker\ServicetakerdashboardController@taker_account');
  Route::get('/switch/role', 'Servicetaker\ServicetakerdashboardController@role_switch');
  Route::get('/booking/confirmed', 'Servicetaker\ServicetakerdashboardController@booking_confirmed');
  Route::get('/booking/denied', 'Servicetaker\ServicetakerdashboardController@booking_denied');
Route::get('/account/notifications', 'Servicetaker\ServicetakerdashboardController@account_notifications');
   Route::get('/account/review', 'Servicetaker\ServicetakerdashboardController@review');
  Route::get('/account/close', 'Servicetaker\ServicetakerdashboardController@account_close');
  Route::get('/account/card_details', 'Servicetaker\ServicetakerdashboardController@account_card_details');
  Route::POST('/notifications/destroy', 'Servicetaker\ServicetakerController@destroyNotification');
  /*********myrouteni25nov19*******/
  Route::post('/updatephone', 'Servicetaker\ServicetakerController@profile_phone_update');
  Route::post('/updatename', 'Servicetaker\ServicetakerController@profile_name_update');
  Route::post('/updatecpf', 'Servicetaker\ServicetakerController@profile_cpf_update');
  Route::post('/updaterg', 'Servicetaker\ServicetakerController@profile_rg_update');
  /*********myrouteni25nov19end*******/
  // route for closeaccount
  Route::POST('/dashboard/closeaccount', 'Servicetaker\ServicetakerdashboardController@closeaccount');
  Route::get('/convert_address/{address}','Servicetaker\ServicetakerController@convert_address');

});
//route for cpf check
Route::post('profile/cpfcheck', 'Servicetaker\ServicetakerController@checkCpfExist');

Route::get('/test_patricia','ProfileController@testing');
//route for lockscreen
// Route::POST('/admin/unlockScreen', 'AdmindashboardController@unlockScreen');
// Route::POST('/serviceprovider/unlockScreen', 'Serviceprovider\ServiceproviderController@unlockScreen');
// Route::POST('/servicetaker/unlockScreen', 'Servicetaker\ServicetakerController@unlockScreen');


//route for chat module
Route::resource('/chat','ChatController');
Route::get('/userchat/{id}','ChatController@userchat');
Route::get('/updateunreaduser','ChatController@updateunreaduser');
Route::post('usersajaxRequest', 'ChatController@saveuserChat');

Route::get('getusersChat/{senderid}/{receiverid}','ChatController@getusersChat');
Route::get('getusersChatCount','ChatController@getusersChatCount');

Route::get('/ajaxfriendlistRefresh', 'ChatController@ajaxfriendlistrefresh');
Route::get('/ajaxfriendlistRefreshadmin', 'AdminChatController@ajaxfriendlistrefreshadmin');

Route::get('ajaxfriendlist', 'ChatController@ajaxfriendlist');
Route::get('userchatsearch', 'ChatController@userchatsearch');
Route::get('userstatuscheck/{receiverid}', 'ChatController@user_status_check');
Route::get('friendlistuserstatuscheck/{receiverid}', 'ChatController@user_status_check');

// })
Route::get('/lockscreen', 'LockscreenController@get');
Route::POST('/unlock', 'LockscreenController@unlock');

/*****frontuserprofile**********/
Route::get('/userprofile/{id}', 'UserprofileController@userprofiledata');
/*****frontuserprofile**********/

Route::get('/chkapikey',function(){
  return view('chkapikey');
});
//Clear Cache facade value:
Route::get('/clear-cache', function() {
  $exitCode = Artisan::call('cache:clear');
  return '<h1>Cache facade value cleared</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
  $exitCode = Artisan::call('route:cache');
  return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
  $exitCode = Artisan::call('route:clear');
  return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
  $exitCode = Artisan::call('view:clear');
  return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
  $exitCode = Artisan::call('config:cache');
  return '<h1>Clear Config cleared</h1>';
});


Route::get('/other',function(){
  return view('pages.other');
});