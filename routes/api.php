<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => ['api','cors']], function ($router) {
  Route::post('register', 'Api\AuthController@register');
  Route::post('login', 'Api\AuthController@login');
  Route::post('logout', 'Api\AuthController@logout');
  Route::post('refreshtoken', 'Api\AuthController@refreshtoken');
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'user'], function($router){
  Route::post('detail', 'Api\UserController@details'); 
  Route::post('add_review', 'Api\UserController@add_review');
  Route::post('reviews', 'Api\UserController@reviews'); 
  Route::post('booking', 'Api\UserController@provider_booking');
  Route::post('forgotpassword', 'Api\UserController@forgotpassword');
  Route::post('profile_update', 'Api\UserController@profile_update'); 
  Route::post('updateprofiledata', 'Api\UserController@updateprofiledata');
  Route::post('updateprofilephone', 'Api\UserController@updateprofilephone');
  Route::post('updateprofilename', 'Api\UserController@updateprofilename');
  Route::post('updateprofilecpf', 'Api\UserController@updateprofilecpf');
  Route::post('updateprofilerg', 'Api\UserController@updateprofilerg');
  Route::post('updateprofiledob', 'Api\UserController@updateprofiledob');
  Route::post('getzipcodedata', 'Api\UserController@getZipLatLong');
  Route::post('user_documents_upload', 'Api\UserController@user_documents_upload');
  Route::post('user_gallery_upload', 'Api\UserController@user_gallery_upload');
  Route::post('convert_address', 'Api\UserController@convert_address');
  Route::post('delete_user_images', 'Api\UserController@delete_user_images');
  Route::post('block_provider', 'Api\UserController@block_provider');
  Route::post('add_user_blacklist', 'Api\UserController@add_user_in_blacklist');
  Route::post('user_wishlist_response', 'Api\UserController@user_wishlist_response');
  Route::post('getWhislistDetails', 'Api\UserController@getUserWhislistDetails');
  Route::post('accountCloseRequest', 'Api\UserController@user_close_account_request'); 
  Route::post('deactivate_user', 'Api\UserController@deactivate_user'); 
  Route::post('activate_user', 'Api\UserController@activate_user'); 
  Route::post('user_availabilities', 'Api\UserController@provider_availability');
  Route::post('search_user', 'Api\UserController@search_user'); 
  Route::post('changepassword', 'Api\UserController@changepassword'); 
  Route::post('logout', 'Api\UserController@logout');
  Route::post('switchuseraccount', 'Api\UserController@switchuserrole');  
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'home'], function($router){
  Route::post('lists', 'Api\HomeController@getLists');
  Route::get('categorylist', 'Api\HomeController@getCategoryList');
  Route::post('suggestionslist', 'Api\HomeController@getSuggestionsList'); 
  Route::post('search', 'Api\HomeController@getSearchList');  
  Route::get('allcategorylist', 'Api\HomeController@getAllCategoryList');
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'contact'], function($router){
  Route::post('sendemail', 'Api\ContactController@sendEmail');
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'notification'], function($router){
  
Route::post('takerNotificationList', 'Api\NotificationController@taker_notifications');

//Route::post('testtakerNotificationList', 'Api\NotificationController@testtakernotification');
 
});
Route::group(['middleware' => ['api','cors'], 'prefix' => 'booking'], function($router){
  Route::post('get_pending_bookings', 'Api\BookingController@get_pending_booking');
  Route::post('get_confirmed_bookings', 'Api\BookingController@get_confirmed_bookings');
  Route::post('get_denied_bookings', 'Api\BookingController@get_denied_bookings'); 
  
});


Route::group(['middleware' => ['api','cors']], function($router){
  
Route::get('otherservices', 'Api\HomeController@getotherservices');
Route::post('service', 'Api\HomeController@getsingleservicedetails'); 
//Route::post('testtakerNotificationList', 'Api\NotificationController@testtakernotification');
 
});

 