Author: CodexWorld
Author URL: http://www.codexworld.com/
Author Email: contact@codexworld.com
Tutorial Link: http://www.codexworld.com/send-email-with-attachment-php/

============ Instruction ============

Open the index.php file on text editor ==> Specify the recipient email ($to), sender name ($fromName), sender email ($from), subject ($subject), file to attach ($file), and body content to send ($htmlContent) => Open the file in the browser and test the email sending functionality with attachment.

============ May I Help You ===========
If you have any query about this script, please feel free to comment here - http://www.codexworld.com/send-email-with-attachment-php/#respond. We will reply your query ASAP.