@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
<style>
.image-previewer {
    width: 100% !important;
    height: 100% !important;
}
.form-group.profileform_right a {
    /*display: none;*/
    color: red;
}
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
.profileimage #uploadbtnclick{position: absolute;
    left: 16%;
    padding: 10px 20px;
    color: #fff;
    font-weight: 600;
    border: 0px;
    top: -37px;
    width: 20px;
    height: 20px;
    font-size: 20px;}
.profileimg_div{position: relative;}
#document_img img {

    width: 90px;
    height: 90px;
    border: 2px solid #3399cc;

}
.document_delete_btn i.fa.fa-trash {
    position: absolute;
    top: 10px;
    padding: 5px;
    background-color: #3399cc;
    color: #fff;
    border-radius: 2px;
    margin-left: 0px;
}
@media (min-width: 1200px){
.container {
    width: 1300px !important;
} 
}
</style>
@section('content')

<!-- cropzee CSS bundle -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ url('/public') }}/rotateimg/css/cropzee.css">
    <!-- cropzee JS bundle -->
   <!--  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js" defer></script>
    <script src="{{ url('/public') }}/rotateimg/js/cropzee.js" defer></script>

    <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&sensor=true&libraries=places'></script>
    <script src="{{ url('/public') }}/js/locationpicker.jquery.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

    <script src="{{ url('/public') }}/js/bootstrap-datepicker.pt.min.js"></script>

    <script defer>
        $(document).ready(function(){
            // custom JS (IGNORE)
            $('body').css("min-height", windowHeight);
            // Initialize cropzee
            cropzee("#cropzee-input");
        });
    </script>

<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
    foreach($currentuser_ip as $currentuser_info) {
      $wish_profile_id = $currentuser_info->wish_profile_id;
      $currentuser_count = DB::table('wish_list')
      ->where('user_id', $login_userid)
      ->where('wish_profile_id', $wish_profile_id)
      ->count();
      if($currentuser_count==0 || $currentuser_count==''){
        $insert_whish = DB::table('wish_list')->insert(
          ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
        );
      }
    }
  }
  ?>
 
  <!--<div id="dialog-message" title="My Dialog Alternative">-->
    <!--<p style='color:red'> Hello world </p>-->
    <section class="maincontent searchbarbg">

      @include('layouts.notify-message')

      <div class="col-sm-12 pagecontent whitebgdiv nopadding">
        <div class="container">
          <div class="acccompletionsec profile-accountcomplete">
            @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
            <script>
            $.notify({
              message: 'Atenção! Complete seu cadastro abaixo.',
              },{
              type: 'success',
              offset: 
              {
                x: 10,
                y: 130
              },
              animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
              },
            });
          </script>
      <!-- <div class="alert alert-danger alert-dismissible fade in ">
                <strong>Alert!!</strong> please Complete Your Profile.
      </div> -->
      @endif

      @if($spavailabilityInfo->isCompleted=='0')
      

      <div class="alert alert-danger alert-dismissible fade in">
        <strong>Atenção!</strong>  Preencha os dias da semana e horários em que você está disponível.  <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui </a> 
      </div>
      @endif
      <div class="image123_alert profle_alert">

      </div>
            <!-- <h1>Account Completion</h1>-->
            <div class="acccompletion_tabs"> 
              <ul class="row nav nav-tabs" role="tablist">
                <li role="presentation"  class="@if (!Session::has('success')) active @endif">
                  <a href="#profile" aria-controls="profile"  role="tab" data-toggle="tab">Meus dados</a></li>
                  <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
                  <li role="presentation"  class="@if (!Session::has('success'))  @endif" ><a href="{{url('/serviceprovider/services')}}" aria-controls="services" role="tab" >Serviços & Preços</a></li>
                  <li role="presentation"  class="@if (!Session::has('success'))  @endif" ><a href="{{url('/serviceprovider/spavailablty')}}" aria-controls="availibility" role="tab" >Agenda</a></li>
                </ul>

                <!-- @include('layouts.flash-message') -->
                @include('layouts.notify-message')

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="profile">
                    <form class="completionform" id="completionform" name ="profileFormData" action="{{url('/serviceprovider/update/profile') }}" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                      {{ csrf_field() }}
                      <input type="hidden" class="current_user_id" id="current_user_id" value="{{ $login_userid }}">
                      <div class="row"> 
                        <div class="col-md-3">  
                          <div class="ser_tak_profile">
                          <div class="form-group imagediv" >
                            <div class="imagedivborder">
                              <div class="imagebrowse" id="previewdiv">
                               
                              <div id="" class="image-previewer" data-cropzee="">
                                <?php
                            if($profile_image==''){
                              ?>
                              <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="img-circle">
                              <?php
                            }
                            else {
                              ?>
                              <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-circle">
                              <?php
                            }
                            ?>
                            <input id="cropzee-input" type="file" >
                               </div>  
                              
                              <input id="crop_image" type="hidden" name="profileimage">

                              <input id="check_image" type="hidden" name="check_image" value="@if($profile_image!='') {{$user_info->profile_image}} @endif">
                              
                               <h5 class="usrnam">{{ucfirst($user_info->name)}}</h5>
                        </div>
                         <canvas id="cropzee-hidden-canvas"></canvas>
                          <!-- The crop Modal -->
                          <div id="cropzee-modal-cover" class="cropzee-modal-cover">
                              <!-- Modal content -->
                              <div id="cropzee-modal" class="cropzee-modal">
                                  <div id="cropzee-close" class="cropzee-close">&times;</div>
                                  <div id="cropzee-modal-display-container" class="cropzee-modal-display-container">
                                      <div id="cropzee-modal-display" class="cropzee-modal-display"></div>
                                      <div id="cropzee-modal-cropper" class="cropzee-modal-cropper"></div>
                                      <div id="cropzee-modal-display2" class="cropzee-modal-display" style="clip: rect(46px, 205px, 205px, 46px);"></div>
                                      <div id="cropzee-cropper-outline" class="cropzee-cropper-outline" style="left:45px; top:45px; width: 160px; height: 160px;"></div>
                                  </div>
                                  <div class="cropzee-modal-buttons-container">
                                      <a id="cropzee-download-button" class="cropzee-modal-button" data-ripple="">
                                              <img class="cropzee-svg" src="{{ url('/public') }}/rotateimg/icons/feather/download.svg">
                                      </a>
                                      <a id="cropzee-rotate-button" class="cropzee-modal-button" data-ripple="" title="Girar imagem">
                                          <!-- <svg class="feather">
                                              <use xlink:href="assets/icons/feather/feather-sprite.svg#circle"/>
                                          </svg> -->
                                          <img class="cropzee-svg" src="{{ url('/public') }}/rotateimg/icons/feather/rotate-ccw.svg">
                                      </a>
                                      <a id="cropzee-crop-button" class="cropzee-modal-button" data-ripple="" title="Selecionar área">
                                          <img class="cropzee-svg" src="{{ url('/public') }}/rotateimg/icons/feather/crop.svg">
                                      </a>
                                      <a id="cropzee-save-button" class="cropzee-modal-button" data-ripple="" title="Salvar área selecionada">
                                          <img class="cropzee-svg" src="{{ url('/public') }}/rotateimg/icons/feather/check-square.svg">
                                      </a>
                                  </div>
                              </div>
                              <div class="cropzee-cropping-canvas-container">
                                  <canvas id="cropzee-cropping-canvas"></canvas>
                              </div>
                          </div>
                          <!-- end crop modal-->
                         
                              <div class="col-md-3 prflinput">
                                <!-- <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none"> -->
                              </div>
                              <h6 class="usreml">{{$user_info->email}}</h6>
                            </div>
                          </div>
                        </div>
                        </div>
                        <div class="col-md-9">
                          <div class="ser_tak_profile">
                          <div class="row">
                           
                            <div class="col-sm-6 profileform_right">
                              <div class="col-sm-12">

                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Nome completo*: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="name" id="name" class="form-control readcolr" value="<?php  echo  $user_info->name;?>" placeholder="" required>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Email: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="email" name="email" id="email" class="form-control readcolr" value="<?php echo  $user_info->email;?>" placeholder="" required readonly>
                                    <input type="hidden" name="user_id" value="{{ $user_info->id }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Celular*: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="phone" id="phone" class="form-control readcolr profileNumber" value="<?php echo  $user_info->phone;?>" placeholder="Ex.: (11)91111-1111" maxlength="14" minlength="14" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');"  required>
                                    <!--    <i class="fa fa-check phone"></i>-->
                                    <span class="format_error" style="color:red;" ></span>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Tipo de usuário: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <select class="form-control readcolr Salvar" name="user_role" disabled="true">
                                      <option vlaue="">Selecione sua opção</option>
                                      <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Prestador de serviço</option>
                                      <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Contratante</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">CPF*: </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="cpf_id" id="inputId" class="form-control readcolr cpfNumbers validate" value="<?php if($user_info->cpf_id) { echo  $user_info->cpf_id; } ?>" maxlength="14" placeholder="Ex.: 111.111.111-11">
                              <!--    <i class="fa fa-check phone"></i>-->
                              <span class="cpf_format_error" style="color:red; display:none;" >Por favor digite seu CPF.</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">RG*: </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="rg" id="rg" class="form-control readcolr " value="<?php if($user_info->rg) { echo  $user_info->rg; } ?>"  placeholder=""  required>
                              
                            </div>
                          </div>
                        </div> 

                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6"> Data de nascimento*: </label>
                            <div class="profileinput col-sm-6 nopadding">
                            <input type="text" name="dob" id="dob" class="form-control readcolr datepicker" value="<?php if($user_info->dob !='' && $user_info->dob !='NULL' && $user_info->dob !='0000-00-00') {
                              $newdob = str_replace('-', '/', trim($user_info->dob));
                              echo date('d/m/Y', strtotime($newdob)); } else { echo "01/01/1970"; } ?>"  placeholder=""  required>
                            </div>
                          </div>
                        </div> 

                      
                            </div>
                            <div class="col-sm-6 profileform_right">

                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Para me conhecer melhor<a  data-toggle="tooltip" data-placement="auto" title="Apresente-se e descreva mais sobre você e seu conhecimento, de forma que os contratantes possam comparar os seus serviços com os demais prestadores de serviço cadastrados no site"><span class="more_info" id="more_info_1" title="Apresente-se e descreva mais sobre você e seu conhecimento, de forma que os contratantes possam comparar os seus serviços com os demais prestadores de serviço cadastrados no site">*</span></a>:</label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <textarea name="bio" id="bio" class="form-control readcolr bodborder" required><?php echo  $user_info->bio;?></textarea>
                                    <!--<i class="fa fa-check bio"></i>-->
                                  </div>  
                                </div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6">Cursos e certificados<a  data-toggle="tooltip" data-placement="auto" title="Descreva curso(s) e/ou certificação(ções) feito(s) para aperfeiçoamento da prestação de serviço"><span class="more_info" id="more_info_2" title="Descreva curso(s) e/ou certificação(ções) feito(s) para aperfeiçoamento da prestação de serviço">*</span></a>: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="additional_details" id="" class="form-control readcolr" value="<?php  echo $user_info->additional_details; ?>" placeholder="">
                                  </div>
                                </div> 
                              </div> 

                              
                              <div class="col-sm-12">
                                <div class="form-group profileform_right">
                                  <label class="col-sm-6 zipcode_label">CEP: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input style="font-family: inherit !important;" type="text" name="zipcode" id="zipcode" onblur="getAddress()" class="form-control readcolr" value="<?php  echo  $user_info->zipcode;?>" placeholder="" maxlength="9" inputmode="numeric" onkeypress="return IsNumeric(event);">
                                    <!-- <span id="error" style="color: Red; display: none">* Input digits (0 - 9)</span> -->
                                    <span id="zip_error" style="color: Red;"></span>
                                  </div>
                                </div>
                              </div>

                                
                              
                                <div class="col-sm-12 form-group profileform_right" id="code" style="display: none;">
                                  <label class="col-sm-6 address_label">Endereço: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input style="font-family: inherit !important;" type="text" class = "form-control" id="address" name="address" placeholder="" value="<?php  echo  $user_info->address;?>" style="text-transform: capitalize!important;" readonly/>
                                    <span id="add_error" style="color: Red;"></span>
                                  </div>
                                </div>
                             

                              
                                <div class="col-sm-12 form-group profileform_right" id="code2" style="display: none;">
                                  <label class="col-sm-6 strnumber_label">Número: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input style="font-family: inherit !important;" type="text" name="strnumber" id="strNumber" class="form-control readcolr" value="<?php  echo  $user_info->street_number;?>" placeholder="" style="text-transform: capitalize!important;">
                                  </div>
                                </div>
                             

                             
                                <div class="col-sm-12 form-group profileform_right" id="code1" style="display: none;">
                                  <label class="col-sm-6 complemento_label">Complemento: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input style="font-family: inherit !important;" type="text" name="complemento" id="complemento" class="form-control readcolr" value="<?php  echo  $user_info->complemento;?>" placeholder="Ex.: Apto 121" style="text-transform: capitalize!important;">
                                  </div>
                                </div>
                             
                              
                              
                                <div class="col-sm-12 form-group profileform_right" id="code3" style="display: none;">
                                  <label class="col-sm-6 neighborhood_label">Bairro: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="neighborhood" id="neighborhood" class="form-control readcolr" value="<?php echo  $user_info->neighborhood;?>" placeholder="" style="text-transform: capitalize!important;" readonly disabled>
                                    <span id="bairro_error" style="color: Red;"></span>
                                  </div>
                                </div>
                              
                              
                              
                                <div class="col-sm-12 form-group profileform_right" id="code4" style="display: none;">
                                  <label class="col-sm-6 city_label">Cidade: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="city" id="city" class="form-control readcolr" value="<?php  echo  $user_info->city;?>" placeholder="" style="text-transform: capitalize!important;" readonly disabled>
                                    <span id="city_error" style="color: Red;"></span>
                                  </div>
                                </div>
                              
                              
                             
                                <div class="col-sm-12 form-group profileform_right" id="code5" style="display: none;">
                                  <label class="col-sm-6 state_label">Estado: </label>
                                  <div class="profileinput col-sm-6 nopadding">
                                    <input type="text" name="state" id="state" class="form-control readcolr" value="<?php  echo  $user_info->state;?>" placeholder="" style="text-transform: capitalize!important;" readonly disabled>
                                    <!--<i class="fa fa-check zipcode"></i>-->
                                  </div>
                                </div>
                             

                  <div id="streetMap" style="display: block;">
                    <div class="container-fluid">
                        <div class="col-lg-6">
                            <div id="us3" style="width: 400px; height: 400px;"></div>
                            <p></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <script>
                        $('#us3').locationpicker({
                            /*location: {
                                latitude: -23.56591788384108,
                                longitude: -46.60218524932861
                            },*/
                            location: {
                                latitude: '<?php echo $user_info->latitude;?>',
                                longitude: '<?php echo $user_info->longitude;?>'
                            },
                            radius: 300,
                            onchanged: function (currentLocation, radius, isMarkerDropped) {
                               
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        var lat = currentLocation.latitude;
        var lng = currentLocation.longitude;

        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&country=BR&types=(cities)&language=pt_BR', function (datas) {
          var address = datas.results[0].address_components;
          $.each(address, function () {

              var address_type = this.types[0];
              
              switch (address_type) {
                  case 'route':
                      Myaddress_1 = this.long_name;
                      break;
                  case 'administrative_area_level_2':
                      Mycity = this.long_name;
                      break;
                  case 'administrative_area_level_1':
                      Mystate = this.long_name;
                      break;
                  case 'country':
                      Mycountry = this.long_name;
                      break;
                  case 'political':
                      Myneighborhood = this.long_name;
                      break;
                  case 'street_number':
                      MystrNumber = this.long_name;
                      break;
              }
          });

          $('#zip_error').html('');
          $('#zipcode').val(addressComponents.postalCode);
          $('#hdcity').val(Mycity);
          $('#hdstate').val(Mystate);
          $('#hdneighborhood').val(Myneighborhood);
          $('#hdstrnumber').val(MystrNumber);
          $('#hdcountry').val(Mycountry);
          $('#hdaddress').val(Myaddress_1);
          $('#hdlat').val(lat);
          $('#hdlng').val(lng);

          $('#code').show();
          $('#code1').show();
          $('#code2').show();
          $('#code3').show();
          $('#code4').show();
          $('#code5').show();
          $('#code6').show();
          $('#country_address').show();

          $('#address').val(Myaddress_1);
          $('#state').val(Mystate);
          $('#city').val(Mycity);
          $('#neighborhood').val(Myneighborhood);
          //$('#strNumber').val(MystrNumber);
          $('#country option').text(Mycountry);
          $('#country').prop('readonly', true);
          $('#address').prop('readonly', true);
          $('#state').prop('readonly', true);
          $('#city').prop('readonly', true);
          $('#neighborhood').prop('readonly', true);
          //$('#strNumber').prop('readonly', true);
          $('#address').show();
          $('.address_label').show();
          $('#country').show();
          $('.country_label').show();
          $('#state').show();
          $('.state_label').show();
          $('#city').show();
          $('.city_label').show();
          $('#neighborhood').show();
          $('.neighborhood_label').show();
          $('#strNumber').show();
          $('.strnumber_label').show();
          $('#complemento').show();
          $('.complemento_label').show();
        });

        }
   });
   </script>
                  
                </div>   
                              

                      <input type="hidden" name="hdcity" id="hdcity" value="<?php echo $user_info->city;?>"> 
                      <input type="hidden" name="hdstate" id="hdstate" value="<?php echo $user_info->state;?>">  
                      <input type="hidden" name="hdneighborhood" id="hdneighborhood" value="<?php echo $user_info->neighborhood;?>"> 
                      <input type="hidden" name="hdstrnumber" id="hdstrnumber" value="<?php echo $user_info->street_number;?>">
                      <input type="hidden" name="hdcountry" id="hdcountry" value="<?php echo $user_info->country;?>"> 
                      <input type="hidden" name="hdaddress" id="hdaddress" value="<?php echo $user_info->address;?>"> 
                      <input type="hidden" name="hdlat" id="hdlat" value="<?php echo $user_info->latitude;?>"> 
                      <input type="hidden" name="hdlng" id="hdlng" value="<?php echo $user_info->longitude;?>"> 

                      <input type="hidden" name="hd_Google_status" id="hd_Google_status" value=""> 

                            </div>
                          </div>
                          
                          <div class="col-sm-12 form-group forminput profileform_right radimate">
                            <div class="col-sm-3">
                              <label>Anexar imagem:</label>
                            </div>
                            <div class="col-sm-9 browseimage nopadding" id="browse_img">
                              <div class="">
                                @if(count($user_image) > 0) 
                                <div class="row gallery_img">
                                  @foreach ($user_image as $user_images)
                                  <!-- <div class="col-sm-3 single_img" id="prv_{{ $user_images->image_id }}"> -->
                                  
                                   <!-- <a href="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" target="_blank" class="imgfree"> -->
                                   <div class="files_section"> 
                                    <div href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete" style="">  <i class="fa fa-trash"></i></div>
                                    <a href="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}" target="_blank" class="imgfree"><img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}"   id="prv_{{ $user_images->image_id }}" >
                                  </a> 
                                  </div>  
                                    <!-- </div> -->
                                    @endforeach
                                  </div>
                                  @else
                                  <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive demoGallaryPic">
                                  @endif
                                </div>
                              </div>
                              <div class="col-sm-3">

                              </div>
                              <div class="nopadding col-sm-9 col-xs-9 new_up_sec ">
                                <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
                                <div class="browse_btn row" id="browsebtn">
                                  <span>Selecionar imagem</span>
                                  <!--<p>No File Selected</p>-->
                                  <div class="img_alert"></div>
                                </div>
                              </div>
                            </div>
                            <div class="" style="float:right">
                            <div class="col-sm-12 nextbtn text-right">
                              
                                <!-- <button type="button" class="btn btn-default without_img_btn cancelcancel">Salvar</button> -->
                              
                                <input type="submit" id="saveBtn" class="form-control next_1 cpfvali Salvar" value="Salvar" >
                              
                              <!-- <input type="submit" id="saveBtn" class="form-control next_1 cpfvali Salvar" value="Salvar" > -->
                               
                               <a href="{{ url('serviceprovider/dashboard')}}" class="btn btn-default pull-right cancelcancel">Cancelar</a>
                              &nbsp;
                            </div> 
                          </div>
                          </div>   
                          </div>
                        </div>
                      </form>
                    </div>
                    <div id="confirm" class="modal" tabindex="-1" role="dialog" style="padding-top: 35px;">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                          <p>Você não adicionou foto no seu perfil. Deseja adicionar uma foto agora?</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" data-dismiss="modal" class="btn btn-primary" id="sim_class">Sim</button>
                          <button type="button" id="save_btn" class="btn btn-secondary" data-dismiss="modal">Depois</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </section>

    </main>
<script type="text/javascript">
  $(document).ready(function() {

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        endDate: '-18y',
        language: 'pt'
    });

 var dob = $("#dob").val();
  if(dob == "01/01/1970") {
    $("#dob").removeClass('readcolr');
    $("#dob").css("background-color","#e9ec19 !important");
  }

    $(".more_info_1").click(function () {
        var $title1 = $(this).find(".title1");
        if (!$title1.length) {
            $(this).append('<span class="title1">' + '<span id="closeTitle1"><i class="fa fa-close"></i></span> <br>' + $(this).attr("title") + '</span>');
        } else {
            $title1.remove();
        }
    });

    $(".more_info_2").click(function () {
        var $title2 = $(this).find(".title2");
        if (!$title2.length) {
            $(this).append('<span class="title2">' + '<span id="closeTitle2"><i class="fa fa-close"></i></span> <br>' + $(this).attr("title") + '</span>');
        } else {
            $title2.remove();
        }
    });

    $(document).on('click', '#closeTitle1', function (e) {
      e.preventDefault();
      var $title1 = $(this).find(".title1");
      $title1.remove();
    });

    $(document).on('click', '#closeTitle2', function (e) {
      e.preventDefault();
       var $title2 = $(this).find(".title2");
       $title2.remove();
    });

    /* $('body').on('click', function () {
      
        var $title1 = $(this).find(".title1");
        if($title1.length==1) {
          $title1.remove();
        }

        var $title2 = $(this).find(".title2");
        if($title2.length==1) {
          $title2.remove();
        }
        
    });*/

    $( ".Salvar" ).on('click',function( event ) {
      //event.preventDefault();
      var crop_img = $('#crop_image').val();
      var check_image = $('#check_image').val();

      if(check_image=='' && crop_img==''){
          var $form = $(this).closest('form');
            event.preventDefault();

          $('#confirm').modal({
              backdrop: 'static',
              keyboard: false
          })
          .on('click', '#save_btn', function(e) {
              $form.trigger('submit');
          });
      }
      
    });

    
  });
</script>
<script type="text/javascript">
 function validateForm() {
      var hiddenzipstatus = document.getElementById('hd_Google_status').value;
      var hiddenzipadds = document.getElementById('hdaddress').value;
      var zipadds = document.getElementById('address').value;
      var hdzipneighborhood = document.getElementById('hdneighborhood').value;
      var zipneighborhood = document.getElementById('neighborhood').value;
      var hdzipcity = document.getElementById('hdcity').value;
      var zipcity = document.getElementById('city').value;

      
      if(hiddenzipadds=='' && zipadds=='') {
        $('#add_error').html('Endereço não encontrado');
        setTimeout(function(){
        $('#add_error').html('');
        }, 4000);
        return false;
      }
      if(hdzipneighborhood=='' && zipneighborhood=='') {
        $('#bairro_error').html('Bairro não encontrado');
        setTimeout(function(){
        $('#bairro_error').html('');
        }, 4000);
        return false;
      }
      if(hdzipcity=='' && zipcity=='') {
        $('#city_error').html('Cidade não encontrado');
        setTimeout(function(){
        $('#city_error').html('');
        }, 4000);
        return false;
      }
    
}

</script>
<script type="text/javascript">
  var specialKeys = new Array();
  specialKeys.push(8); //Backspace
  function IsNumeric(e) {
      var keyCode = e.which ? e.which : e.keyCode
      var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
      document.getElementById("error").style.display = ret ? "none" : "inline";
      return ret;
  }
</script>

    <script>
      $(document).ready(function() {
      var zipcode = $('#zipcode').val();
      if(zipcode!=''){
        $('#zip_error').html('');
        $('#code').show();
        $('#code1').show();
        $('#code2').show();
        $('#code3').show();
        $('#code4').show();
        $('#code5').show();
        $('#code6').show();
        $('#country_address').show();
      }
    });
      function getAddress() {

          $('#country').prop('readonly', false);
          $('#address').prop('disabled', false);
          $('#city').prop('readonly', false);
          $('#state').prop('readonly', false);
          $('#neighborhood').prop('readonly', false);
          //$('#strNumber').prop('readonly', false);
          $('#complemento').prop('readonly', false);
          if( $('#country').text() !='' && $('#address').val()!='' && $('#state').val()!='' && $('#neighborhood').val()!='' && $('#strNumber').val()!='' && $('#city').val()!='' && $('#complemento').val()!=''){
            $('#code').show();
            $('#code1').show();
            $('#code2').show();
            $('#code3').show();
            $('#code4').show();
            $('#code5').show();
            $('#code6').show();
            $('#country_address').show();
          }

        if ($( "#zipcode" ).val().length == 0 ) {

            $('#hdcity').val('');
            $('#hdstate').val('');
            $('#hdneighborhood').val('');
            $('#hdstrnumber').val('');
            $('#hdcountry').val('');
            $('#hdaddress').val('');
            $('#hdlat').val('');
            $('#hdlng').val('');

            $('#address').val('');
            $('#address').hide();
            $('.address_label').hide();
            $('#address-error').hide();
            $('#country option').text('');
            $('#country').hide();
            $('.country_label').hide();
            $('#state').val('');
            $('#state').hide();
            $('.state_label').hide();
            $('#city').val('');
            $('#city').hide();
            $('.city_label').hide();
            $('#neighborhood').val('');
            $('#neighborhood').hide();
            $('.neighborhood_label').hide();
            $('#strNumber').val('');
            $('#strNumber').hide();
            $('.strnumber_label').hide();
            $('#complemento').val('');
            $('#complemento').hide();
            $('.complemento_label').hide();

            $('#code').hide();
            $('#code1').hide();
            $('#code2').hide();
            $('#code3').hide();
            $('#code4').hide();
            $('#code5').hide();
            $('#code6').hide();
            $('#country_address').hide();
            return;
          }  else {
            $('#zip_error').html('');
          } 

      //Get Postcode
      var postcode = $('#zipcode').val().toUpperCase();
      var Myaddress_1 = '';
      var Mycity = '';
      var Mystate = '';
      var Mycountry = '';
      var Myneighborhood = '';
      var MystrNumber = '';
     
  $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + postcode + '&sensor=true&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&country=BR&types=(cities)&language=pt_BR', function (data) {
    
        if(data.status=='ZERO_RESULTS') {

            $('#zip_error').html('CEP não reconhecido. Favor mover o ponto vermelho no mapa abaixo até o seu endereço.');

            /*setTimeout(function(){
            $('#zip_error').html('');
            }, 4000);*/

            $( "#zipcode" ).val('');
            $('#hdcity').val('');
            $('#hdstate').val('');
            $('#hdneighborhood').val('');
            $('#hdstrnumber').val('');
            $('#hdcountry').val('');
            $('#hdaddress').val('');
            $('#hdlat').val('');
            $('#hdlng').val('');

            $('#address').val('');
            $('#address').hide();
            $('.address_label').hide();
            $('#address-error').hide();
            $('#country option').text('');
            $('#country').hide();
            $('.country_label').hide();
            $('#state').val('');
            $('#state').hide();
            $('.state_label').hide();
            $('#city').val('');
            $('#city').hide();
            $('.city_label').hide();
            $('#neighborhood').val('');
            $('#neighborhood').hide();
            $('.neighborhood_label').hide();
            $('#strNumber').val('');
            $('#strNumber').hide();
            $('.strnumber_label').hide();
            $('#complemento').val('');
            $('#complemento').hide();
            $('.complemento_label').hide();

            $('#code').hide();
            $('#code1').hide();
            $('#code2').hide();
            $('#code3').hide();
            $('#code4').hide();
            $('#code5').hide();
            $('#code6').hide();
            $('#country_address').hide();

            $('#us3').locationpicker({
              location: {
                  latitude: -23.56591788384108,
                  longitude: -46.60218524932861
              },
              radius: 300,
              onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
              }
            });

            return false;
        } else {

          $('#hd_Google_status').val(data.status);
          var lat = data.results[0].geometry.location.lat;
          var lng = data.results[0].geometry.location.lng;

          $('#us3').locationpicker({
              location: {
                  latitude: lat,
                  longitude: lng
              },
              radius: 300,
              onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
              }
            });

          $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&country=BR&types=(cities)&language=pt_BR', function (datas) {
          var address = datas.results[0].address_components;
          $.each(address, function () {

              var address_type = this.types[0];
              
              switch (address_type) {
                  case 'route':
                      Myaddress_1 = this.long_name;
                      break;
                  case 'administrative_area_level_2':
                      Mycity = this.long_name;
                      break;
                  case 'administrative_area_level_1':
                      Mystate = this.long_name;
                      break;
                  case 'country':
                      Mycountry = this.long_name;
                      break;
                  case 'political':
                      Myneighborhood = this.long_name;
                      break;
                  case 'street_number':
                      MystrNumber = this.long_name;
                      break;
              }
          });

          $('#hdcity').val(Mycity);
          $('#hdstate').val(Mystate);
          $('#hdneighborhood').val(Myneighborhood);
          $('#hdstrnumber').val(MystrNumber);
          $('#hdcountry').val(Mycountry);
          $('#hdaddress').val(Myaddress_1);
          $('#hdlat').val(lat);
          $('#hdlng').val(lng);

          $('#code').show();
          $('#code1').show();
          $('#code2').show();
          $('#code3').show();
          $('#code4').show();
          $('#code5').show();
          $('#code6').show();
          $('#country_address').show();

          $('#address').val(Myaddress_1);
          $('#state').val(Mystate);
          $('#city').val(Mycity);
          $('#neighborhood').val(Myneighborhood);
          //$('#strNumber').val(MystrNumber);
          $('#country option').text(Mycountry);
          $('#country').prop('readonly', true);
          $('#address').prop('readonly', true);
          $('#state').prop('readonly', true);
          $('#city').prop('readonly', true);
          $('#neighborhood').prop('readonly', true);
          //$('#strNumber').prop('readonly', true);
          $('#address').show();
          $('.address_label').show();
          $('#country').show();
          $('.country_label').show();
          $('#state').show();
          $('.state_label').show();
          $('#city').show();
          $('.city_label').show();
          $('#neighborhood').show();
          $('.neighborhood_label').show();
          $('#strNumber').show();
          $('.strnumber_label').show();
          $('#complemento').show();
          $('.complemento_label').show();
        }); 
      }
          
      });
    } 

      $(document).ready(function() {

        jQuery.validator.addMethod("numberonly", function(value, element) {
          return this.optional(element) || /^[0-9]+$/i.test(value);
        }, "Letters only please"); 
        
        /*$.validator.addMethod("alphaLetter", function(value, element) {
          return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
        });*/

        jQuery.validator.addMethod("nameRegex", function(value, element) {
              return this.optional(element) || /^[a-z\ ,ãáàâäéêëïíöôõóûüúç\s]+$/i.test(value);
        }, "Name must contain only letters & space");

        $.validator.addMethod("cpfBR", function(value) {
            // Removing special characters from value
            value = value.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, "");

            // Checking value to have 11 digits only
            if (value.length !== 11) {
              return false;
            }

            var sum = 0,
              firstCN, secondCN, checkResult, i;

            firstCN = parseInt(value.substring(9, 10), 10);
            secondCN = parseInt(value.substring(10, 11), 10);

            checkResult = function(sum, cn) {
              var result = (sum * 10) % 11;
              if ((result === 10) || (result === 11)) {result = 0;}
              return (result === cn);
            };

            // Checking for dump data
            if (value === "" ||
              value === "00000000000" ||
              value === "11111111111" ||
              value === "22222222222" ||
              value === "33333333333" ||
              value === "44444444444" ||
              value === "55555555555" ||
              value === "66666666666" ||
              value === "77777777777" ||
              value === "88888888888" ||
              value === "99999999999"
            ) {
              return false;
            }

            // Step 1 - using first Check Number:
            for ( i = 1; i <= 9; i++ ) {
              sum = sum + parseInt(value.substring(i - 1, i), 10) * (11 - i);
            }

            // If first Check Number (CN) is valid, move to Step 2 - using second Check Number:
            if ( checkResult(sum, firstCN) ) {
              sum = 0;
              for ( i = 1; i <= 10; i++ ) {
                sum = sum + parseInt(value.substring(i - 1, i), 10) * (12 - i);
              }
              return checkResult(sum, secondCN);
            }
            return false;

        }, "Please specify a valid CPF number");

        var hostname = " {{ url('/') }}"; 
      
      $("#completionform").validate({
          rules: {
            name:{
              required: true,
              nameRegex: true,
              minlength: 7
            },
            bio : {
              required: true
            },
            zipcode : {
              required: true
            },
            /*address : {
              required: true,
            }*/
            strnumber: {
              required: true,
              numberonly: true
            },
            phone: {
              required: true,
              minlength: 14,
              maxlength: 14
            },
            rg: {
              required: true
            },
            dob: {
            required: true
            },
           /* iscorrect: {
              required: true
            },*/
            cpf_id: {
            required: true,
            cpfBR: true,
            maxlength: 14,
            remote: {
              url: hostname+"/profile/cpfcheck",
              type: "post",
              data: { cpf_id: function(){ return $('input[name=cpf_id]').val(); },
                      _token: function(){ return $('input[name=_token]').val(); },
                      user_id: function(){ return $('input[name=user_id]').val(); }
                    },
              dataFilter: function(data) {
                console.log(data);
                if(data == 'Exists'){

                  return false;
                }else{
                  return true;
                }
              }
            }
          }
            
          },
          messages : {
            name: {
                required: "Favor preencher o campo acima.",
                nameRegex: "Favor verificar o nome digitado. Não utilize números ou símbolos.",
                minlength: "Digite pelo menos 7 caracteres."
            }, 
            bio: {
              required: "Favor preencher o campo acima."
            },
            zipcode: {
              required: "Campo obrigatório."
            },
            strnumber: {
              required: "Favor preencher o campo acima.",
              numberonly: 'Utilize apenas números nesse campo. Para informações adicionais referentes ao endereço, utilize o campo "Complemento" abaixo.'
            },
            phone: {
              required: "Campo obrigatório.",
              minlength: "Favor digitar 11 números, incluindo o DDD.",
              maxlength: "Favor digitar 11 números, incluindo o DDD."
            },
            rg: {
              required: "Campo obrigatório."
            },
            dob: {
              required: "Favor preencher a data de nascimento."
            }, 
            /*iscorrect: {
              required: "Campo obrigatório."
            },*/
            cpf_id: {
              required: "Favor preencher o campo acima.",
              cpfBR: "Verifique o número do CPF digitado.",
              maxlength: "Maximum 11 Number allowed",
              remote:"CPF já cadastrado em nosso sistema."
           
            }
            /*address: {
              required: "Campo obrigatório para prestadores de serviço."
            }*/
          }
        });
    });

    $('#completionform').on('submit', function(e) {
    var idValid = $("#completionform").valid();
    if(idValid == true) {
    } else {
          $('html, body').animate({
             scrollTop: ($('.error').offset().top - 200)
        }, 2000);
    }
  });

      $(document).on('click','.validate',function(){
         $('.cpf_format_error').hide();
         $(':input[type="submit"]').prop('disabled', false);
       });

      window.setTimeout(function() {
        $(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).hide();
        });
      }, 4000);
      /***********************/
      //for cpf format
      document.getElementById("inputId").onkeyup = function(){
      this.value = this.value.replace(/^(\d{3})(\d{3})(\d{3})(\d{2}).*/, '$1.$2.$3-$4');
      };
      /***********************************/
       /***********************/
      //for phone format
     
      document.getElementById("phone").onkeyup = function(){
      var phone = this.value.length;
       
      if (phone = 11) {
      this.value = this.value.replace(/^(\d{2})(\d{5})(\d{4}).*/, '($1)$2-$3');
      } else { 
         this.value = this.value.replace(/^(\d{2})(\d{4})(\d{4}).*/, '($1)$2-$3');
       }
      };   
      document.getElementById("zipcode").onkeyup = function(){
      this.value = this.value.replace(/^(\d{5})(\d{3}).*/, '$1-$2');
      };                      

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery.cpfcnpj.min.js"></script>
    <script type="text/javascript">
         // function mycpffunction() {
            /*$('.validate').cpfcnpj({
                mask: true,
                validate: 'cpfcnpj',
                event: 'focusout',
                //handler: '.btn',
                ifValid: function (input) {
                  //alert('valid') 
               },
                ifInvalid: function (input) { 
                  //alert('invalid')
                  //$('.cpf_format_error').show();
                 // $(':input[type="submit"]').prop('disabled', true);
                   }
            });*/
         
   
    </script>
<script type="text/javascript">
  $( "#sim_class" ).on('click',function(event) {
            var $form = $(this).closest('form');
            //event.preventDefault();
            jQuery('#cropzee-input').click();
            // Initialize cropzee
            cropzee("#cropzee-input");
            var jj = $('#crop_image').val();
            if(jj){
              $form.trigger('submit');
            }
  });
</script>
<!-- <script src="{{ url('/public')}}/js/ckeditor.js"></script> -->
<script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script>
<script>
        ClassicEditor
            .create( document.querySelector( '#bio' ) )
            .catch( error => {
                console.error( error );
            } );
/*
      CKEDITOR.replace('bio', {
      // Pressing Enter will create a new <div> element.
      enterMode: CKEDITOR.ENTER_P,
      // Pressing Shift+Enter will create a new <p> element.
      shiftEnterMode: CKEDITOR.ENTER_P
    });*/

</script>
<script type="text/javascript">

$("#phone").blur(function(){
    var phone = $('#phone').val();
    var user_id = $('#current_user_id').val();
    $.ajax({
      type:"POST",
      url:"https://www.navizinhanca.com/serviceprovider/updatephone",
      data:{"_token": "{{ csrf_token() }}", phone: phone, user_id: user_id},
      success:function(data){
        //alert(data);
      }
    })
});
$("#name").blur(function(){
    var name = $('#name').val();
    var user_id = $('#current_user_id').val();
    $.ajax({
      type:"POST",
      url:"https://www.navizinhanca.com/serviceprovider/updatename",
      data:{"_token": "{{ csrf_token() }}", name: name, user_id: user_id},
      success:function(data){
        //alert(data);
      }
    })
});
$("#inputId").blur(function(){
    var cpf = $('#inputId').val();
    var user_id = $('#current_user_id').val();
    $.ajax({
      type:"POST",
      url:"https://www.navizinhanca.com/serviceprovider/updatecpf",
      data:{"_token": "{{ csrf_token() }}", cpf: cpf, user_id: user_id},
      success:function(data){
       // alert(data);
      }
    })
});
$("#rg").blur(function(){
    var rg = $('#rg').val();
    var user_id = $('#current_user_id').val();
    $.ajax({
      type:"POST",
      url:"https://www.navizinhanca.com/serviceprovider/updaterg",
      data:{"_token": "{{ csrf_token() }}", rg: rg, user_id: user_id},
      success:function(data){
        //alert(data);
      }
    })
});
</script>
<style type="text/css">
  .document_delete_btn i.fa.fa-trash{
    z-index: 999;
    cursor: pointer;
  }
  .files_section {
    position: relative;
    float: left;
  }

#more_info_1 {
  position: relative;
}

#more_info_1 .title1 {
    position: absolute;
    top: 20px;
    background: silver;
    padding: 4px;
    left: 0;
    z-index: 999;
    width: 250px;
}

#more_info_2 {
  position: relative;
}

#more_info_2 .title2 {
    position: absolute;
    top: 20px;
    background: silver;
    padding: 4px;
    left: 0;
    z-index: 999;
    width: 250px;
}

</style>
    @stop
