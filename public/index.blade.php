@extends('layouts.default-header-admin')

@section('title', 'Add Service')

@section('content')
<!-- <div class="alert alert-success" id="Alert_Success" style="display: none;"> <a href="#" class="close" data-dismiss="alert">&times;</a>
  Service deleted successfully!
</div> -->

<div class="content-wrap">
 <div class="yelloback">
  <div class="container-fluid">
    <div class="seredback">  
      <div class="row">
        @include('layouts.notify-message')
        <a href="{{url('/admin-other-services/create')}}" class="btn btn-success pull-right">Add New </a>
        <div class="col-sm-12 content_box">
          <div class="row">
            <div class="col-sm-8 col-xs-8 category_title">
              <h3 >Other Services</h3>
            </div>
          </div>
          <table id="subcategory_table" class="table table-borderless heading-name">
            <thead>
              <tr>
                <th> Sr No </th>
                <th> Title</th>
                <th> Action </th>
              </tr>
            </thead>
            
            <tbody>
              @if($category->count()>0)
              <?php $count = 0; ?>
              @foreach($category as $categories)
              <tr id="{{ $categories->id }}" class="service_table tabledata">
                <td> {{ ++$count }}</td>
                <td> {{ ucfirst($categories->title) }} </td>
                <td><a href="{{ url('/admin-other-services') }}/{{ $categories->id }}/edit"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit" style="font-size:16px"></i></a>
                  <a class="deletebutton" style="cursor: pointer;" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" style="font-size:16px"></i></a>
                </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          {{$category->links()}}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$('#subcategory_table').DataTable({
  "bPaginate": false
});
jQuery('.success_show').hide();
jQuery('.deletebutton').click(function(){
  if(confirm("Are you sure you want to delete this?")){
    var catid = jQuery(this).closest('tr').attr('id');
    jQuery.ajax({
      url: "{{url('/')}}/admin-other-services/category_remove",
      type: 'POST',
      data: {'catid': catid,"_token": "{{ csrf_token() }}" },
      success: function(data) {
        if(data == 1)
        {
          $('#Alert_Success').show();
          jQuery('#'+catid).hide();
            // window.setTimeout(function() {
            //   $('#Alert_Success').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your service deleted successfully!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          }  
          else
          {
            $('#Alert_Danger').show();
            // window.setTimeout(function() {
            //   $('#Alert_Danger').remove(); 
            // }, 4000);
            $.notify({
              message: 'Your service  cannot  deleted!',
            },{
              type: 'success',
              offset: 
              {
               x: 10,
               y: 130
             },
             animate: {
               enter: 'animated fadeInRight',
               exit: 'animated fadeOutRight'
             },
           });
          } 
        }          
      })      
  }
  else{
    return false;
  }
  window.setTimeout(function() {
            // $(".").fadeTo(500, 0).slideUp(500, function(){
              $('test21').remove(); 
            // });
          }, 4000); 
});
</script>

@stop