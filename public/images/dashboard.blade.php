@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )

@section('content')
<section class="maincontent searchbarbg">
  <div class="row">
    <div class="col-md-12">
      <!-- Nav tabs -->
      <div class="card dashboard_tabs">
        <div class="bottomnav">
          <div class="container-fluid"> 
            <ul class="nav nav-tabs tabs_box main-box " role="tablist">
              <li role="presentation" class="active"><a href="#dashboard" aria-controls="dashboard" role="tab">Painel de controle</a></li>
              <li role="presentation"><a href="{{url('/serviceprovider/bookings')}}" aria-controls="bookingmsg" role="tab"> Contatos & Mensagens</a></li>
              <li role="presentation"><a href="{{url('/serviceprovider/account/details')}}" aria-controls="settings" role="tab">Sua conta</a></li>
              <li role="presentation"><a href="{{url('/serviceprovider/switch/role')}}" aria-controls="switch" role="tab">Mudar de função</a></li>
              
            </ul>
          </div>
        </div>
       <!--  @include('layouts.flash-message') -->
        @include('layouts.notify-message')
        <?php
        $profile_image = $user_info->profile_image;
        $profile_image = $user_info->profile_image;
        $user_role = $user_info->user_role;
        $userId = Auth::id();
        ?>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="dashboard">
            <div class="istbox">
              <div class="container">
                <div class="row">
                  <div class="col-sm-8 full_box">
                    <div class="profile_box">
                      <div class="row  box-clr">
                        <div class="col-sm-4 text-center">
                          @if(!empty($user_info->profile_image ))
                          <img class="profimg" src="{{url('public/images/profileimage')}}/<?php echo $user_info->profile_image; ?>" alt="dashboardimg" width="100%">
                          @else
                          <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="profimg" alt="dashboardimg" width="100%">
                          @endif
                        </div>
                        <div class="col-sm-8 text_content">
                          <h3>{{ucfirst($user_info->name)}}</h3>
                          <ul class="style-chnage">
                            <li><span> Email:</span> {{$user_info->email}} </li>
                            <li><span> Endereço completo: </span>{{$user_info->address}}
                              <?php if($user_info->street_number!=''){
                                echo ',';} else { echo '';} ?> {{$user_info->street_number}} <?php if($user_info->complemento!=''){ echo ',';} else {echo '';} ?> {{$user_info->complemento}}</li>
                            <li><span> Celular: </span>{{$user_info->phone}}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="topicon">
                      <a href="{{url('/serviceprovider/profile')}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon21.png" alt="dashboard_designi" width="70%" class="img-responsive"></a>
                      <a href="{{url('/profile/'.$userId)}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon21.png" alt="dashboard_designi" width="70%" class="img-responsive"></a>
                    </div>
                  </div>
                  <div class="col-sm-4 baby_box">
                    <div class="singlepagesidebar jjj">
                      <div class="row"> 
                        <div class="col-sm-6 col-xs-6 conter">
                          <?php if($main_category_detail->cat_image!=''){ ?>
                          <img src="{{url('/public/images/main_category/'.$main_category_detail->cat_image)}}">
                          <?php echo $main_category_detail->name;?>
                          <?php } else {
                           echo '';
                          }
                          ?>
                        </div>
                        <div class="col-sm-6 col-xs-6 conter"">
                          <?php if($sub_category_detail!=''){ ?>
                          <img src="{{url('/public/images/categoryimage/'.$sub_category_detail->cat_image)}}">
                           <?php echo $sub_category_detail->cat_name;?>
                          <?php } else {
                            echo '';
                          } ?>
                          
                        </div>
                      </div>
                      @if(count($provider_price_lists) > 0)
                      <h3>Preços</h3>
                      <table class="table pricing_table">
                        <tbody>
                          
                          @foreach($provider_price_lists as $provider_price_list)
                          <tr>
                           <th>{{$provider_price_list->queries}}</th>
                           <td>&#82;&#36;{{number_format($provider_price_list->amount,2,",",".")}}</td>
                         </tr>
                         @endforeach
                        
                       </tbody>
                     </table>
                      @endif
                   </div>
                  <div class="topicon">
                      <a href="{{url('/serviceprovider/services')}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon21.png" alt="dashboard_designi" width="70%" class="img-responsive"></a>
                    </div>


                 </div>

               </div>
             </div>
           </div>
           <div class="istbox">
            <div class="container">
              <div class="row">
                <div class="col-sm-8">
                  <div class="singlepagesidebar">
                    <div class="singleprofile_listing" style="margin-top: 0px;">
                      <h3 >Horário
                      </h3>
                      <ul class="listingitem1">
                        @if($spavailabilityInfo->isCompleted=='1')
                  @if($spavailabilityInfo->openhrs=='1')
                    <li> @if($spavailabilityInfo->day_monday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Segunda-feira:
                      @if($spavailabilityInfo->day_monday=='1')
                      {{$spavailabilityInfo->monday_start}} - {{$spavailabilityInfo->monday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_tuesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Terça-feira:
                      @if($spavailabilityInfo->day_tuesday=='1')
                      {{$spavailabilityInfo->tuesday_start}} - {{$spavailabilityInfo->tuesday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_wednesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quarta-feira:
                      @if($spavailabilityInfo->day_wednesday=='1')
                      {{$spavailabilityInfo->wednesday_start}} - {{$spavailabilityInfo->wednesday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_thursday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quinta-feira:
                      @if($spavailabilityInfo->day_thursday=='1')
                      {{$spavailabilityInfo->thursday_start}} - {{$spavailabilityInfo->thursday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_friday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sexta-feira:
                      @if($spavailabilityInfo->day_friday=='1')
                      {{$spavailabilityInfo->friday_start}} - {{$spavailabilityInfo->friday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_saturday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sábado:
                      @if($spavailabilityInfo->day_saturday=='1')
                      {{$spavailabilityInfo->saturday_start}} - {{$spavailabilityInfo->saturday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($spavailabilityInfo->day_sunday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Domingo:
                      @if($spavailabilityInfo->day_sunday=='1')
                      {{$spavailabilityInfo->sunday_start}} - {{$spavailabilityInfo->sunday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                  @else
                    <li><i class="fa fa-check-circle"></i>Segunda-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Terça-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Quarta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Quinta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Sexta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Sábado 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Domingo 00:00 - 23:59</li>
                  @endif
                @else
                  <li><i class="fa fa-times-circle"></i>No availability set by {{ucfirst($user_info->name)}}</li>
                @endif
                      </ul>
                  </div>
                  </div>
                  <div class="topicon">
                             <a href="{{url('/serviceprovider/spavailablty')}}"> <img src="{{ url('/public/images/') }}/14_dashboard_designicon21.png" alt="dashboard_designi" width="70%" class="img-responsive"></a>
                        </div>
                </div>
                <div class="col-sm-4 baby_box">

                  <div class="singlepagesidebar">
                    <div class="box_div invite_box">
                      <img class="inviteimg inving" src="http://navizinhanca.com/public/images/14_dashboard_design.icon1.png" alt="design" width="100%">
                      <div class="paid">
                        <h3>Convide um amigo </h3>
                      </div>
                      <form class="form-inline inviteform dashboardinviteform" id="inviteform">
                        <div class="form-group nopadding nopadding copybtn">

                          <input class="col-xs-9" id="myInput" value="{{ url('/') }}/invite-friend/{{$user_info->referal_code}}" type="text" readonly>

                          <input class="btn btn-default col-xs-3 redme" value="Copiar o link" onclick="CopyText()" type="button" >
                         <!--  <span style="display:none;" class="copy_msg"><b style="font-size:10px;color:#808080;">Copied!!!</b></span> -->
                        </div>
                      </form>
                     <!--  <h3><strong>Seus amigos recebem um &#82;&#36;10 desconto</strong></h3>
                      <h4><strong class="sky_clr">Você começa &#82;&#36;10 fora de sua próxima reserva</strong></h4>
                      <div class="doler_box">
                        <h4>&#82;&#36;0,00</h4>
                        <p>Saldo de Créditos Atual para Gastar</p>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="istbox">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="singlepagesidebar">
                      <div id="map" style="width: 100%; height: 400px; background: #89C4FA" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane codeRefer" id="bookingmsg">


        </div>
        <div role="tabpanel" class="tab-pane" id="account">

        </div>
        <!--account-tab-->
        <div role="tabpanel" class="tab-pane subtabcontent" id="switch">
        </div>


      </div></div></div></div></div>
    </section>
    <script type="text/javascript">
    function CopyText() {
       var copyText = document.getElementById("myInput");
       copyText.select();
       document.execCommand("copy"); 
       $.notify({
          message: 'Copiado!' 
          },{
          type: 'success',
          offset: 
          {
            x: 10,
            y: 130
          },
          animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
          },
        });
    }

      function confirmMessage()
      {
        var retVal = confirm("Você tem certeza?");
        if( retVal == true ) {
          return true;
        }
        else {
          return false;
        }
      }
      var base_url = '<?php echo url('/');?>';
      $('.change_role').on('click',function(){
        if(confirm("Tem certeza que quer sair clicando aqui Você está logout site do formulário em breve, você tem que entrar novamente com papel alterado")){
         $('#switchdashboard').attr('action',base_url+'/dashboard/switchdashboard');
         $('#switchdashboard').submit();
       }
       else{
        return false;
      }

    });
      $('.status_submit').on('click', function(e) {
       e.preventDefault(); 
       var status = confirm("Você tem certeza?");
       if( status == true ) {

        var token = $('.book_token').val();
        var bookingId = $('.bookId').val();
        var status = $('.status_submit').val();
        var host = '{{ url('/') }}';
     // alert("#pending_"+bookingId);die;
     $.ajax({
       type: "POST",
       url: host+'/providerbooking/status',
       data: {'booking_id':bookingId,'_token':token,'approve_status':status},
       dataType: "json",
       success: function(response) {
         $(".statusAlerts").show();
         $(".statusAlerts").html(response.msg);
         $("#pending_"+bookingId).hide();
         if(response.status == '1'){
          $('.data_fill_confirm').focus();
          $(".data_fill_confirm").append(response.html);
        } else if(response.status == '2') {
          $('.data_fill_confirm').focus();
          $('.data_fill_confirm').append(response.html);
        }else {
          $(".data_fill_pending").focus();
          $(".data_fill_pending").append(response.html);
        }
        setTimeout(function () {
          $('.statusAlerts').hide();
        }, 2000);
      }
    });
   } else {
    return false;
  }

});

</script>
<script type="text/javascript">
  $('.success_show').hide();
  $('.deletebutton').click(function(){
    if(confirm("Tem certeza de que deseja excluir este?")){
      var idd = $(this).attr('id');
      'notifications.id',
      $.ajax({
        url: "/dashboard/notifications",
        type: 'POST',
        data: {'id':idd,"_token": "{{ csrf_token() }}" },
        success: function(response) {
          jQuery('.success_show').show();
          jQuery('.main_'+idd).remove();
        }            
      });      
    }
  });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&callback=initMaps">
    </script>
<script  type="text/javascript" charset="UTF-8" >
/* carousel script for user gallery*/
  $('.carousel').carousel({
    interval: 2000,
    cycle:true
  });

  var lat = '<?php echo $user_info->latitude; ?>';
  var lan = '<?php echo $user_info->longitude; ?>';
  var center = new google.maps.LatLng(lat, lan);     
  function initMaps() {
      // MAP ATTRIBUTES.
      var mapAttr = {
          center: center,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.TERRAIN
      };

      // THE MAP TO DISPLAY.
      var map = new google.maps.Map(document.getElementById("map"), mapAttr);

      var circle = new google.maps.Circle({
          center: center,
          map: map,
          radius: 1000,          // IN METERS.
          fillColor: 'rgba(0, 128, 0, 0.7)',
          fillOpacity: 0.5,
          strokeColor: "rgba(55, 85, 170, 0.6)",
          strokeWeight: 0         // DON'T SHOW CIRCLE BORDER.
      });
  }
  google.maps.event.addDomListener(window, 'load', initMaps);
</script>
@stop
