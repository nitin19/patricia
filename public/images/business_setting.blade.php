@extends('layouts.default-header-admin')

@section('title', 'Admin')

@section('content')
<div class="content-wrap">
<div class="col-md-12">
                      <div class="row">
    <div class="col-lg-12">
    <div class="business_setting">
    <h1>Business Settings</h1>    
    </div>    
    </div>    
    </div>

     <div class="row">
     <div class="col-lg-6">
     <div class="form-group settingdata">
    <label>Website Header Logo</label> 
    <div class="col-md-3 nopadding" id="ftrd_browse_img">
          <img src="https://www.collegefashionnetwork.com/development/public/images/1548735926-header.png" i="blah" class="img-responsive" style="width: 100px; height: 100px;">
              <input id="header_logo" type="file" name="header_logo" accept="image/*">
       </div>
    </div>    
    </div>
                           
                           
      <div class="col-lg-6">
    <div class="form-group settingdata">
    <label>Website Footer Logo</label> 
    <div class="col-md-3 nopadding" id="ftrd_browse_img">
          <img src="https://www.collegefashionnetwork.com/development/public/images/1548735926-footer_logo.png" i="blah" class="img-responsive" style="width: 100px; height: 100px;">
              <input id="footer_logo" type="file" name="footer_logo" accept="image/*">
       </div>
     
      
    </div>    
    </div>
    </div>
    <div class="row">
     <div class="col-lg-6">
              
     <div class="form-group settingdata">
    <label>Admin Profile</label> 
    <div class="col-md-3 nopadding" id="ftrd_browse_img">
         <img src="https://www.collegefashionnetwork.com/development/public/profile/1549019936.2.jpg" id="blah" class="img-responsive" style="width: 100px; height: 100px;">
              <input id="header_logo" type="file" name="profile_image" accept="image/*">
       </div>
    </div>    
    </div>
             
    </div>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Business Name*</label>
                                <input id="business_name" type="text" class="form-control valid" name="business_name" value="Business Name" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>E-Mail Address*</label>
                                 <input id="business_email" type="business_email" class="form-control" name="business_email" value="Business Email">
                                 </div>
                                </div>
                           </div>
                           <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Phone*</label>
                                <input id="phone" type="text" class="form-control" name="phone" value="Phone" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>Business City*</label>
                                 <input id="business_city" type="business_city" class="form-control" name="business_city" value="Business City">
                                 </div>
                                </div>
                           </div>
                           <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Business State*</label>
                                <input id="business_state" type="text" class="form-control" name="business_state" value="Business State" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>Business Country*</label>
                                 <input id="business_country" type="business_country" class="form-control" name="business_country" value="Business Country">
                                 </div>
                                </div>

                           </div>

                           <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Business Address*</label>
                                <input id="business_address" type="text" class="form-control" name="business_address" value="Business Address" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>Business Zipcode*</label>
                                 <input id="business_zipcode" type="business_zipcode" class="form-control" name="business_zipcode" value="Business Zipcode">
                                 </div>
                                </div>
                                
                           </div>

                           <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Twitter Link*</label>
                                <input id="twitter_link" type="text" class="form-control" name="twitter_link" value="https://twitter.com" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>Facebook Link*</label>
                                 <input id="facebook_link" type="facebook_link" class="form-control" name="facebook_link" value="https://www.facebook.com/">
                                 </div>
                                </div>
                                
                           </div>

                           <div class="row">
                            <div class="col-md-6">
                              <div class="settingdata form-group">
                                <label> Google Link*</label>
                                <input id="google_link" type="text" class="form-control" name="google_link" value="https://google.com" required="">
                              </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="settingdata form-group">
                                 <label>Linkedin*</label>
                                 <input id="linkedin" type="text" class="form-control" name="linkedin" value="https://linkedin.com">
                                 </div>
                                </div>
                                
                           </div>
                        </div>
</div>

@stop
