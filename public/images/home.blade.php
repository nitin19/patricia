  @extends('layouts.default-header')
  @section('title', 'Home')
  @section('content')
  <main>
    <!--mapsection-start-->
    <!--Mouse-Cursor-Effect-(div-HTML)-->
    <div id="age-gate" class="display dismissed">
     <div class="feathers">
       <i class="feather"></i>
       <i class="feather"></i>
       <i class="feather"></i>
     </div>
   </div>
   <div class="custom-cursor" style="opacity: 1;">
    <span class="main" style="top: 493px; left: 250px;">
    </span>
    <span class="follow" style="top: 493px; left: 250px;">
    </span>
  </div>
  <!--Mouse-Cursor-Effect-(div-HTML)End-->
  <section class="mapsection">
    <div id="map" style="width: 100%; height: 490px; background: grey"/></div>
  </section>
  <!--mapsection-end-->
  <!--serviceform-start-->
  <?php
  $lat_str1 = '';
  $long_str1 = '';
  foreach($users as $user_info){
    if($user_info->latitude!=''){
      $lat_str1 .= $user_info->latitude.',';
      $long_str1 .= $user_info->longitude.',';
    }
  }
  $lat_str = explode(',',rtrim($lat_str1,","));
  $long_str = explode(',',rtrim($long_str1,','));
  $data = array_combine($lat_str,$long_str);
  $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
  $latitude_center =$new_arr[0]['geoplugin_latitude'];
  $longitude_center = $new_arr[0]['geoplugin_longitude'];
  ?>
  <section class="seriveformsec">
    <div class="container">
      <div class="row">
        <div class="serviceformbg">
          <form class="serviceform" id="serviceform">
            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-10"><h3>Services Needed</h3></div>
              <div class="col-sm-1"></div>
            </div>
            <div class="">
              <div class="col-sm-1 formdiv"></div>
              <div class="col-sm-2 formdiv">
                <input type="hidden" name="sortby" value="asc">
            <select class="form-control" id="cat" name="cat" required oninvalid="this.setCustomValidity('Please select the category.')" >
              <option value="" selected="selected">Select Category</option>
              @foreach($category as $category_info)
              <option value="{{ $category_info->name }}">{{ $category_info->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-sm-2 formdiv">
            <input type="text" name="zipcode" class="form-control" id="zipcode" value="" placeholder="Zipcode">
          </div>
          <div class="col-sm-2 formdiv">
           <select name="area" id="area" class="custom-select sources" placeholder="Select Area">
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>

        </div>
        <div class="col-sm-2 formdiv">
          <select name="rating" id="rating" class="custom-select sources" placeholder="User Rating">
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
            <!--<select class="form-control" >
              <option value="" selected="selected">User Rating</option>

            </select>-->
          </div>
          <div class="col-sm-2 formbutton">
            <input type="submit" class="btn find_provider" value="Find Providers">
          </div>
          <div class="col-sm-1 formdiv"></div>
        </div>
      </form>
    </div>
  </div>
</div>
</section>
<!--serviceform-end-->

<!--howitwork-start-->
<section class="howitworksec pagesection">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 howitworkcontent text-center">
        <h1 class="pagetitle">How it Works</h1>
        <p class="pagetext">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br/>
        Lorem Ipsum has been the industry's</p>

        <div class="howitworkblks">
          <div class="row">
            <div class="col-sm-4 howitwrkblk">
              <div class="howitwrkblkbg">
                <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/howitblk1.png" class="img-responsive">
                </div>
                <h3>Find The Service Provider<br/>
                  <span>FOR FREE!!!</span></h3>
                  <p>Inform the service you are looking
                    for and use the filters to find the
                    best option for your needs
                  (rating,price, location…)</p>
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                 <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/howitblk2.png" class="img-responsive">
                </div>
                <h3>Find The Service Provider<br/>
                  <span>FOR FREE!!!</span></h3>
                  <p>Inform the service you are looking
                    for and use the filters to find the
                    best option for your needs
                  (rating,price, location…)</p>
                </div>
              </div>
              <div class="col-sm-4 howitwrkblk">
                <div class="howitwrkblkbg">
                 <div class="howitworkimg">
                  <img src="{{ url('/public') }}/images/howitblk3.png" class="img-responsive">
                </div>
                <h3>Find The Service Provider<br/>
                  <span>FOR FREE!!!</span></h3>
                  <p>Inform the service you are looking for and use the filters to find the best option for your needs (rating,price, location…)</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--howitwork-end-->

  <!--servicesection-start-->
  <section class="servicesec pagesection">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 serviceseccontent text-center">
          <h1 class="pagetitle">Our Services</h1>
          <div class="serviceblks">
            <div class="row">
              <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                  <?php
                  $count=1;
                  foreach($main_category as $category_info){
                    $cat_image = $category_info->cat_image;
                    if($cat_image==''){
                      $categoryimage = url('/')."/public/images/category_home/default.png";
                    }else {
                      $categoryimage =  url('/')."/public/images/category_home"."/".$cat_image;
                    }
                    ?>
                    <div class="item">
                      <div class="pad15">
                       <a href="{{ url('/otherservcie') }}"><img src="{{$categoryimage}}"></a>
                       <h4>{{ $category_info->name }}</h4>
                     </div>
                   </div>
                   <?php
                   ++$count;
                 }
                 ?>
               </div>
               <button class="btn btn-primary leftLst"><i class="fa fa-angle-left"></i></button>
               <button class="btn btn-primary rightLst"><i class="fa fa-angle-right"></i></button>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <section class="aboutservice pagesection">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 abtservicecontent text-center">
        <h1 class="pagetitle">About Services Provider</h1>
        <p class="pagetext">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a
          type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
        remaining essentially unchanged.</p>
        <div class="abtserviceimg">
          <img src="{{ url('/public') }}/images/abtservice.png" class="img-responsive" alt="abtserviceimg">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="whitebg"></section>
<section class="testimonialsection pagesection">
  <div class="container">
    <div class="row">
      <div id="carousel-example-generic" class="col-sm-12 carousel slide testimonialslider" data-ride="carousel">
        <h4>CLIENT FEEDBACK</h4>
        <h1>Our Client loves us because <br/>
        of our quality work.</h1>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <div class="row">
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testislidecntnt">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <div class="clientinfo">
                    <img src="{{ url('/public') }}/images/team-member-1.jpg">
                    <h4>Rashed Ka.</h4>
                    <span>Marketing Manager</span>
                    <img src="{{ url('/public') }}/images/quitesimg.png" class="quoteimg">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="pagesection newslettersec">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 centered">
          <div class="newsletterbox wow fadeIn animated slower-4s">
            <h1>Subscribe Newsletter</h1>
            <h4>BE THE FIRST TO GET UPDATES ON SPECIAL EVENTS AND OFFERS</h4>
            <div id="mc_embed_signup">
              <form action="https://navizinhanca.us20.list-manage.com/subscribe/post?u=6afdec3041ad4b6f838402be4&amp;id=4d143d65f1" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                  <!--<label for="mce-EMAIL">Subscribe to our mailing list</label>-->
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6afdec3041ad4b6f838402be4_4d143d65f1" tabindex="-1" value=""></div>
                  <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script  type="text/javascript">
    function addMarkerToGroup(group, coordinate, html, image) {
      var icon = new H.map.Icon(image);
      var marker = new H.map.Marker(coordinate, { icon: icon });
      marker.setData(html);
      group.addObject(marker);
    }
    function addInfoBubble(map) {
      var group = new H.map.Group();
      map.addObject(group);
      group.addEventListener('tap', function (evt) {
        var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
          content: evt.target.getData(),
        });
        ui.addBubble(bubble);
      }, false);
      <?php
      foreach($users as $user_info) {
       if(empty($user_info->profile_image)){
        $image_path =  url('/public')."/images/profileimage/1542707849.images.png";
      }
      else{
       $image_path =  url('/public')."/images/profileimage/".$user_info->profile_image;
     }
     if($user_info->latitude!='' ){
      $email = $user_info->email;
      $cat_image = $user_info->cat_image;
      if($cat_image==''){
        $image = url('/public')."/images/abtservice.pngmages/categoryimage/clothing_icon.png";
      }else {
        $image = url('/public')."/images/categoryimage/".$cat_image;
      }
      ?>
      addMarkerToGroup(group, {lat:'<?php echo $user_info->latitude; ?>', lng:'<?php echo $user_info->longitude; ?>'},
        '<div><a href="<?php echo url('/');?>/profile/<?php echo $user_info->id; ?>" ><?php echo ucfirst($user_info->username); ?></a>' +
        '</div><div class="mapbodytext"><span class="mapheading">City:</span> <?php echo $user_info->city; ?></div><div class="mapbodytext"><span class="mapheading"> Category: </span> <?php echo $user_info->category; ?></div></a></div><div class="mail-images"><img src="<?php echo $image_path;?>" alt="mapimg"></div><div class="mapbodyrating"><span class="mapheading"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i></span><a href="<?php echo url('/');?>/profile/<?php echo $user_info->id; ?>" class="raitingread">Read More</a></div>','<?php echo $image; ?>');
      <?php
    }
  }
  ?>
}

var platform = new H.service.Platform({
  app_id: 'devportal-demo-20180625',
  app_code: '9v2BkviRwi9Ot26kp2IysQ',
  useHTTPS: true
});
var pixelRatio = window.devicePixelRatio || 1;
var defaultLayers = platform.createDefaultLayers({
  tileSize: pixelRatio === 1 ? 256 : 512,
  ppi: pixelRatio === 1 ? undefined : 320
});

var map = new H.Map(document.getElementById('map'),
  defaultLayers.normal.map,{
    center: {lat: '<?php echo $latitude_center; ?>', lng: '<?php echo $longitude_center; ?>'},
    zoom: 7,
    pixelRatio: pixelRatio
  });

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
var ui = H.ui.UI.createDefault(map, defaultLayers);

addInfoBubble(map);
/*---------close map info  on click out side of it --------------------------*/

$(document).mouseup(function(e)
{
    var container = $(".H_ib_body");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.hide();
    }
});

/*----------------------------------------------------------------------------*/
$("#serviceform").validate({
  rules: {
    cat: {
      required: true
    }
                // zipcode: {
                //     required: true
                // }
              },
              messages: {

                cat: {
                  required: "Please select the category."
                }
                // zipcode: {
                //   required: "Please enter zipcode."
                // }
              },
              submitHandler: function(form) {
                var APP_URL = {!! json_encode(url('/search')) !!}
                var cat = jQuery('#cat').val();
                var zipcode = jQuery('#zipcode').val();
                var area = jQuery('#area').val();
                var rating = jQuery('#rating').val();
                $.ajax({
                  url: "{{ url('/search/infoexist') }}",
                  type: 'POST',
                  data: {'_token': "{{ csrf_token() }}", 'cat': cat, 'zipcode': zipcode,'area':area,'rating':rating},
                  success: function(response) {
                    if(response==1){
                      $("#serviceform").attr('action',APP_URL);
                      form.submit();
                    }
                    else{
                      alert("Entered information doesn't match. Please try again!");
                    }
                  }
                });
              }
            });
          </script>
          <!---Home-page-dropdorwn---------------------------->

          <script>
            $(".custom-select").each(function() {
              var classes = $(this).attr("class"),
              id      = $(this).attr("id"),
              name    = $(this).attr("name");
              var template =  '<div class="' + classes + '">';
              template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
              template += '<div class="custom-options">';
              $(this).find("option").each(function() {
                template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
              });
              template += '</div></div>';

              $(this).wrap('<div class="custom-select-wrapper"></div>');
              $(this).hide();
              $(this).after(template);
            });
            $(".custom-option:first-of-type").hover(function() {
              $(this).parents(".custom-options").addClass("option-hover");
            }, function() {
              $(this).parents(".custom-options").removeClass("option-hover");
            });
            $(".custom-select-trigger").on("click", function() {
              $('html').one('click',function() {
                $(".custom-select").removeClass("opened");
              });
              $(this).parents(".custom-select").toggleClass("opened");
              event.stopPropagation();
            });
            $(".custom-option").on("click", function() {
              $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
              $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
              $(this).addClass("selection");
              $(this).parents(".custom-select").removeClass("opened");
              $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
            });
          </script>
          <script src="{{ url('/public') }}/js/home_page_slider.js"></script>
        </main>
        @stop
