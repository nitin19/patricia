@extends('layouts.default-header')

@section('title', 'Home')
<style>
.nopadding input {
  width: auto;
}
.timing_button{
  border:1px solid #EDEDED;
  padding: 14px;
  text-align: center;
  margin-top: 30px;
  width: 208px;
  margin-left:10px;
  border-radius:0px;
}
.time{
  position:unset !important;
}
.pickimg{
  visibility: hidden;
}
</style>
@section('content')
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link href="{{ url('/public') }}/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="{{ url('/public') }}/css/jquery.awesome-cropper.css">
<script src="{{ url('/public') }}/imgareaselect/scripts/jquery.imgareaselect.js"></script>
<script src="{{ url('/public') }}/js/jquery.awesome-cropper.js"></script>


<main>
  <?php
  $user_info = Auth::user();
  $login_userid = Auth::id();
  $profile_image = $user_info->profile_image;
  $user_role = $user_info->user_role;
  if($currentuser_ip_count>0){
   foreach($currentuser_ip as $currentuser_info) {
     $wish_profile_id = $currentuser_info->wish_profile_id;
     $currentuser_count = DB::table('wish_list')
     ->where('user_id', $login_userid)
     ->where('wish_profile_id', $wish_profile_id)
     ->count();
     if($currentuser_count==0 || $currentuser_count==''){
       $insert_whish = DB::table('wish_list')->insert(
         ['user_id' => $login_userid, 'wish_profile_id' => $wish_profile_id]
       );
     }
   }
 }
 ?>
 @if(session()->has('message'))
 <div class="alert alert-danger address_alert" id="address_alert" style="display:none;">
  <strong>perigo!</strong>Opa, você digitou o endereço errado
</div>
 <div class="alert alert-success profle_alert">
     {{ session()->get('message') }}
 </div>
 @endif
   <section class="maincontent searchbarbg">
    @include('layouts.notify-message')
     <div class="col-sm-12 pagecontent whitebgdiv nopadding">
      <div class="container">
        <div class="acccompletionsec profile-accountcomplete">
           @if(empty(Auth::user()->address) && empty(Auth::user()->phone))
           <div class="alert alert-danger alert-dismissible fade in">
             <!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
             <strong>Atenção!</strong>  Complete seu cadastro abaixo.
           </div>
           @endif

          @if($spavailabilityInfo->isCompleted =='0')
            <div class="alert alert-danger alert-dismissible fade in">
             <!--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
             <strong>Atenção!</strong> Preencha os dias da semana e horários em que você está disponível.  <a href="{{ url('/')}}/serviceprovider/spavailablty"> Clique Aqui </a> 
           </div>
           @endif
            <div class="image123_alert">
          
          </div>
          <!-- <h1>Account Completion</h1> -->
          <div class="acccompletion_tabs">
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation"  class=" @if (!Session::has('success')) active @endif">
                <a href="#profile" aria-controls="profile"  role="tab" data-toggle="tab">Meus dados</a></li>
                <!-- <li role="presentation"  class="hides1 @if (!Session::has('success')) @endif"><a href="#Availablity" aria-controls="Availablity" role="tab" data-toggle="tab">Availablity</a></li> -->
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane
                active"
               id="profile">
                <form class="completionform" id="completionform" name ="profileFormData" action="{{ url('/serviceprovider/updateprofile/') }}" method="post"   enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" class="profile_action" value="{{ url('/serviceprovider/updateprofile/') }}">
                  <div class="row"> 
                    <div class="col-md-3"> 
                    <div class="ser_tak_profile">
                     <div class="form-group imagediv" >  
                       <div class="imagedivborder">
                        <div class="imagebrowse" id="previewdiv"> 
                          <div id="previewimage" class="ser_tak_img">
                          <?php
                          if($profile_image==''){
                            ?>
                            <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="img-circle" style="width: 100%;height: auto;" class="previewimage">
                            <?php
                          }
                          else {
                            ?>
                            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="img-circle" style="width: 100%;height:auto;" class="previewimage">
                            <?php
                          }
                          ?>
                        </div>
                          <input id="sample_input" type="hidden" name="profileimage" >
                          <h5 class="usrnam">{{ucfirst($user_info->name)}}</h5> 
                          <h6 class="usreml">{{$user_info->email}}</h6>
                          <div class="col-md-3 prflinput">
                         <!--  <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none"> -->
                        </div>
                         </div>
                        
                      </div>
                    </div>
                  </div>
                  </div>
                  <div class="col-sm-9 profileformmainright">
                    <div class="ser_tak_profile">
                    <div class="row"> 
                      <!--Alert Div -->
                      <div class="alert alert-warning alert-dismissible msgAlert" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="msgAlertVal"></strong>
                      </div>
                      <div class="col-sm-6 profileform_right">
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Nome completo* : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="name" id="name" class="form-control readcolr" value="<?php  echo  $user_info->name;?>" placeholder="" style="text-transform: capitalize!important;" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Email : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="email" name="email" id="email" class="form-control readcolr" value="<?php echo  $user_info->email;?>" placeholder="" required readonly>
                              <input type="hidden" name="user_id" value="{{ $user_info->id }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Celular* : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="phone" id="phone" class="form-control readcolr profileNumber" value="<?php echo  $user_info->phone;?>" placeholder="Ex.:(11)91111-1111" maxlength="13" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');"  required>
                              <!--    <i class="fa fa-check phone"></i>-->
                              <span class="format_error" style="color:red;" ></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Tipo de usuário : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <select class="form-control readcolr" name="user_role" disabled="true">
                                <option vlaue="">Selecione sua opção</option>
                                <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Prestador de Serviço</option>
                                <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Contratante</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">CPF* : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="cpf_id" id="inputId" class="form-control readcolr cpfNumbers validate" value="<?php if($user_info->cpf_id) { echo  $user_info->cpf_id; } ?>" maxlength="13" placeholder="Ex.: 111.111.111-11"  required>
                              <!--    <i class="fa fa-check phone"></i>-->
                              <span class="cpf_format_error" style="color:red; display:none;" >Por favor corrija o número digitado do CPF.</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Anexar documento : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <?php $docFile = $user_info->documents;
                                if($docFile){
                                $exploded=explode(".",$docFile);
                                //print_R($exploded);die;
                                $cpfname = $exploded[0];
                                $cpfExt = $exploded[1];
                                }
                                if($docFile  == ''){ ?>
                                  <input type="file" name="cpf_documents" id="cpf_documents"  >
                                <?php } else if($cpfExt == 'pdf'){ ?>

                               <img src="{{ url('/public/images/') }}/cpfpdf.png" class="img-circle" style="width: 100%;height:auto;"> 
                                <figcaption>{{ $docFile }}</figcaption>
                                <?php
                                  } elseif($cpfExt == 'doc') {
                                    ?>
                                    <img src="{{ url('/public/images/') }}/cpfdoc.png" class="img-circle" style="width: 100%;height:auto;"><figcaption>{{ $docFile }}</figcaption>
                                    <?php
                                  } else { ?>
                                   <img src="{{ url('/public/images/user_document/') }}/<?php echo $docFile; ?>" class="img-circle" style="width: 100%;height: 10%;"><figcaption>{{ $docFile }}</figcaption>
                                  <?php }
                                ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 profileform_right">
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Para me conhecer melhor* : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <textarea name="bio" id="bio" class="form-control readcolr" required><?php echo  $user_info->bio;?></textarea>
                              <!--<i class="fa fa-check bio"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Detalhes adicionais : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" name="additional_details" id="phone" class="form-control readcolr" value="<?php  echo $user_info->additional_details; ?>" placeholder="Enter Addition Details">
                              <!--<i class="fa fa-check additional"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6">Endereço* : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <input type="text" class = "form-control" id="autocomplete" name="address" placeholder="Digite endereço completo" value="<?php  echo  $user_info->address;?>"  style="text-transform: capitalize!important;" required/>
                            <!--   <textarea name="address" id="address" class="form-control readcolr proAddress" required><?php  echo  $user_info->address;?></textarea> -->
                              <!--<i class="fa fa-check address"></i>-->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12" id="code" style="display: none;">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6 zipcode_label">CEP: </label>
                            <div class="profileinput col-sm-6 nopadding" >
                              <input type="text" name="zipcode" id="zipcode" class="form-control readcolr" value="<?php  echo  $user_info->zipcode;?>" placeholder="" readonly disabled>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12" id="code1" style="display: none;">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6 state_label">Estado : </label>
                            <div class="profileinput col-sm-6 nopadding" >
                              <input type="text" name="state" id="state" class="form-control readcolr" value="<?php  echo  $user_info->state;?>" placeholder="" style="text-transform: capitalize!important;" readonly disabled>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-12" id= "country_address" style="display: none;">
                          <div class="form-group profileform_right">
                            <label class="col-sm-6 country_label">País : </label>
                            <div class="profileinput col-sm-6 nopadding">
                              <select class="form-control readcolr" name="country" id="country" readonly style="text-transform: capitalize!important;" disabled>
                                <option value=""></option>>
                                <?php
                                foreach($countries as $country_name){
                                  ?>
                                  <option value="<?php echo $country_name; ?>" <?php if($user_info->country==$country_name) { echo 'selected';} ?>><?php echo $country_name; ?></option>
                                  <?php
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12 form-group forminput profileform_right radimate">
                      <div class="col-sm-3">
                       <label>Anexar imagem</label>
                     </div>
                     <div class="col-sm-9 browseimage nopadding" id="browse_img">
                       <div class="">
                        @if(count($user_image) > 0)
                        <div class="row gallery_img ">
                        @foreach ($user_image as $user_images)
                          <!-- <div class="col-sm-3 single_img" id="prv_{{ $user_images->image_id }}" style="margin-bottom:5px;"> -->
                           <a href="javascript:void(0)" class="delete_btn" id="{{ $user_images->image_id }}" title="Delete">  <i class="fa fa-trash"></i></a>
                           <img src="{{ url('/public') }}/images/userimages/{{ $user_images->image_name }}"  id="prv_{{ $user_images->image_id }}" style="margin: 10px !important;">
                          <!-- </div> -->
                       @endforeach
                     </div>
                       @else
                       <img src="{{ url('/public') }}/images/default-img.png" class="img-responsive">
                       @endif
                     </div>
                   </div>
                   <div class="col-sm-3">
                   </div>
                   <div class="col-sm-9 col-xs-9 new_up_sec nopadding">
                    <input type="file" id="user_images" name="user_images[]" multiple accept="image/*" style="display: none;">
                    <div class="browse_btn" id="browsebtn">
                      <span>Selecionar imagem</span>
                      <!--<p>No File Selected</p>-->
                      <div class="img_alert"></div>
                    </div>
                  </div>
                </div>
                <div class="row" style="float:right">
                <div class="col-sm-12 nextbtn text-right">
                   
                  <input type="submit" class="form-control next_1 cpfvali" value="Salvar">
                  
                  <button style="padding: 7px 22px;" href="{{ url('/serviceprovider/dashboard')}}" class="btn btn-default">Cancelar</button>
                  &nbsp;
                </div>
                <!-- <div class="col-sm-6">
                    <button type="button" class="form-control pull-right btn btn-primary ">Cancel</button>
                </div> -->
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>
</main>
<script>
  jQuery('#previewimage').on('click', function() {
        jQuery('.pickimg').click();
      });
  $(document).on('click','.validate',function(){
         $('.cpf_format_error').hide();
         $(':input[type="submit"]').prop('disabled', false);
       });
   /***********************/
      //for cpf format
      document.getElementById("inputId").onkeyup = function(){
      this.value = this.value.replace(/^(\d{3})(\d{3})(\d{3})(\d{2}).*/, '$1.$2.$3-$4');
      };
      /***********************************/
       /***********************/
      //for phone format
     
      document.getElementById("phone").onkeyup = function(){
      var phone = this.value.length;
       
      if (phone = 14) {
      //alert(phone);
      this.value = this.value.replace(/^(\d{2})(\d{5})(\d{4}).*/, '($1)$2-$3');
      } else { 
        //alert(phone);
         this.value = this.value.replace(/^(\d{2})(\d{4})(\d{4}).*/, '($1)$2-$3');
       }
      };                         
      /***********************************/
         $('#sample_input').awesomeCropper(
        { width: 150, height: 150, debug: true }
        );

  $(document).ready(function(){
    window.setTimeout(function() {
      $(".profle_alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).hide();
      });
      }, 4000);

       /***********************************/
      $("#autocomplete").autocomplete({

        source: function (request, response) {
             $.ajax({
                 url: 'https://www.navizinhanca.com/search_address/search_address.php',
                 type: "GET",
                 data: request,
                 dataType: "json",
                  beforeSend: function(xhr){
                  xhr.setRequestHeader('Accept', 'application/json');
                  },
                 success: function (data) {
                  response(data);
                 }
             });
          },
          select: function (event, ui) {
           // Set selection
           $('#autocomplete').val(ui.item.value); // display the selected text
           return false;
          }
      });
       /**********************/ 
  var base_url = '<?php echo url('/'); ?>';
  $('#country').prop('readonly', false);
  $('#zipcode').prop('readonly', false);
  $('#state').prop('readonly', false);
  if($('#country').text() !='' && $('#zipcode').val()!='' && $('#state').val()!=''){
    $('#code').show();
    $('#code1').show();
    $('#country_address').show();
  }
  $( "#autocomplete" ).on('blur',function() {
        if ($( "#autocomplete" ).val().length == 0 ) {
          $('#zipcode').val('');
          $('#zipcode').hide();
          $('.zipcode_label').hide();
          $('#state').val('');
          $('#state').hide();
          $('.state_label').hide();
          $('#country option').text('');
          $('#country').hide();
          $('.country_label').hide();
          return;
        }

        var address_val = $('#autocomplete').val();
        var val =  $(this).val();
        var url = '/serviceprovider/convert_address/'+address_val;
        var data  = $('#completionform').serialize();
            $.ajax({
              type: 'GET',
              url: url,
              dataType: 'json',
              success: function(data ){
                console.log(data.res.zipcode);
                if(data.success = "true"){
                  $('#code').show();
                  $('#code1').show();
                  $('#country_address').show();
                  $('#zipcode').val(data.res.zipcode);
                  $('#state').val(data.res.state);
                  $('#country option').text(data.res.country_name);
                  $('#country').prop('readonly', true);
                  $('#zipcode').prop('readonly', true);
                  $('#state').prop('readonly', true);
                  $('#zipcode').show();
                  $('.zipcode_label').show();
                  $('#state').show();
                  $('.state_label').show();
                  $('#country').show();
                  $('.country_label').show();
                }
              },
           });
    })
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery.cpfcnpj.min.js"></script>
    <script type="text/javascript">
      $("#autocomplete").autocomplete({
        source: function (request, response) {
             $.ajax({
                 url: 'http://navizinhanca.com/search_address/search_address.php',
                 type: "GET",
                 data: request,
                 dataType: "json",
                  beforeSend: function(xhr){
                  xhr.setRequestHeader('Accept', 'application/json');
                  },
                 success: function (data) {
                  response(data);
                 }
             });
          },
          select: function (event, ui) {
           // Set selection
           $('#autocomplete').val(ui.item.value); // display the selected text
           return false;
          }
      });
         // function mycpffunction() {
            $('.validate').cpfcnpj({
                mask: true,
                validate: 'cpfcnpj',
                event: 'focusout',
                //handler: '.btn',
                ifValid: function (input) {
                  //alert('valid') 
               },
                ifInvalid: function (input) { 
                  //alert('invalid')
                  $('.cpf_format_error').show();
                  $(':input[type="submit"]').prop('disabled', true);
                   }
            });
         
   
    </script>
@stop
