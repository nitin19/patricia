@extends('layouts.default-header-admin')

@section('title', 'Profile')

@section('content')
<div class="lock_box">
<div class="yelloback">
<div class="container-fluid">
<div class="adminusr">
<div class="seredback">
<div class="row">
<div class="col-sm-12 content_box">
<?php
  if($user_info->profile_image !=''){
    $profile_image = $user_info->profile_image;
  } else{
    $profile_image = '';
  }
$user_role = $user_info->user_role;
?>
<!-- <ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
@if($user_role=='Provider')
<li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>@endif
  </ul> -->

 <div role="tabpanel" class="tab-pane" id="profile">

<!-- <form class="completionform" id="completionform" method="post" action="{{ url('/admin-users/update') }}"  enctype="multipart/form-data">
        {{ csrf_field() }} -->
        <div class="row">
        <div class="col-sm-3 profileimage">
               <div class="form-group" >
              <div class="" id="previewdiv">
                <?php
                if($profile_image==''){
                  ?>

              <img src="{{ url('/public/images/') }}/prof_dummy2.png" class="adusr" style="width: 100%;height:auto;">
              <?php
                }
                else {
                  ?>
                  <img src="{{ url('/public/images/profileimage/') }}/<?php echo $profile_image; ?>" class="adusr" style="width: 100%;height: auto;">
                  <?php
                }
              ?>
              </div>
             <!--  <div class="col-sm-3 nopadding imgbtns">
                <i class="fa fa-camera" aria-hidden="true" id="uploadbtn" > </i>
               </div> -->
              <!-- <div class="col-md-3 prflinput">
                <input id="brwsebtn" type="file" class="imagecls" name="profileimage" style="display: none">
              </div> -->
            </div>         
        </div> 
        <div class="col-sm-9 profileform_right">
          <div class="profilehd">
              <h3>{{ $user_info->name }} <span>@if(!empty($user_info->mainCatName)) ( {{ $user_info->mainCatName }} ) @endif</span></h3>
          </div>
      <div class="col-sm-6">
            <div class="row">
              <div class="profileform_right">
                <label class="col-sm-4">Name</label>
                <div class="profileinput col-sm-8 nopadding">
                  <p><span class="collon">:</span>{{ $user_info->name }}</p>
                  <!-- <i class="fa fa-check name"></i> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Email Address</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->email }}</p>
                <!-- <input type="email" name="email" id="email" class="form-control" value="{{ $user_info->email }}" placeholder="" readonly> -->
                <!-- <i class="fa fa-check email"></i>
                <input type="hidden" name="user_id" value="{{ $user_info->id }}"> -->
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Phone Number</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->phone }}</p>
                <!-- <input type="text" name="phone" id="phone" class="form-control" value="{{ $user_info->phone }}" placeholder="" onkeypress="this.value=this.value.replace(/[^0-9]/g,'');">
                <i class="fa fa-check phone"></i> -->
              </div>
            </div>
        </div>
         <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">CPF Number</label>
              <div class="profileinput col-sm-8 nopadding">
                <p><span class="collon">:</span>{{ $user_info->cpf_id }}</p>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">RG</label>
              <div class="profileinput col-sm-8 nopadding">
                <p><span class="collon">:</span>{{ $user_info->rg }}</p>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">User Role</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->user_role }}</p>
                <!-- <select class="form-control" name="user_role" disabled>
                  <option vlaue="">Select your option</option>
                  <option value="Provider" <?php if($user_info->user_role=='Provider') { echo 'selected'; } ?>>Provider</option>
          <option value="Taker" <?php if($user_info->user_role=='Taker') { echo 'selected'; } ?>>Taker</option>
                </select> -->
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Address</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->address }}</p>
                <!-- <textarea name="address" id="address" class="form-control"><?php echo $user_info->address; ?></textarea>
                <i class="fa fa-check address"></i> -->
              </div>
            </div>
        </div>
      </div>
      <div class="col-sm-6">
       
           <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Zip Code</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->zipcode }}</p>
                <!-- <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $user_info->zipcode; ?>" placeholder="" readonly>
                <i class="fa fa-check zipcode"></i> -->
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Country</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->country }}</p>
              </div>
            </div>
        </div>
        @if($user_info->user_role =='Provider')
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Main Category</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->mainCatName }}</p>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="profileform_right">
              <label class="col-sm-4">Sub Category</label>
              <div class="profileinput col-sm-8 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->cat_name }} </p>
              </div>
            </div>
        </div>
        @endif
       <!--  <div class="row">
               <div class="profileform_right" >
                <label class="col-sm-4">Document</label>
              <div class="profileinput col-sm-8 nopadding">
                <?php
                $docFile = $user_info->documents;
                if($docFile){
                $exploded=explode(".",$docFile);
                //print_R($exploded);die;
                $cpfname = $exploded[0];
                $cpfExt = $exploded[1];
                }
                if($docFile  == ''){
                   echo "<span>No Doc Found.</span>";

                } else if($cpfExt == 'pdf'){
                  ?>

              <a href ="{{url('/public/images/user_document/'.$docFile)}}" target="_blank"><img src="{{ url('/public/images/') }}/cpfpdf.png" class="adusr" style="width: 50%;height:50%;">
              <figcaption>{{ $docFile }}</figcaption></a>
              <?php
                } elseif($cpfExt == 'doc') {
                  ?>
                  <a href ="{{url('/public/images/user_document/'.$docFile)}}" target="_blank"><img src="{{ url('/public/images/') }}/cpfdoc.png" class="adusr" style="width: 50%;height: 50%;"><figcaption>{{ $docFile }}</figcaption></a>
                  <?php
                } else { ?>
                  <a href ="{{url('/public/images/user_document/'.$docFile)}}" target="_blank"><img src="{{ url('/public/images/user_document/') }}/<php echo $docFile; ?>" class="adusr" style="width: 50%;height: 50%;"><figcaption>{{ $docFile }}</figcaption></a>
                <?php }
              ?>
              </div>
            </div>         
        </div>  -->
        <!-- <div class="row">
        <div class="col-md-12">
          <?php 
            $docFile = $user_info->documents;
            if($docFile){ ?>
              <label class="col-sm-4">Document Status</label>
              <div class="profileinput col-sm-8 nopadding">
                <?php
                 if($user_info->document_status == '1')
                 { ?>
                  <span class="">Approved</span>
                 <?php }
                 else if($user_info->document_status == '2')
                 { ?>
                  <span  class="">Disapproved</span>
                 <?php }
                 else
                 { ?>
                  <a  href="{{ url('/admin_users/approve/'.$user_info->id) }}" onclick ="return confirm('Are you sure want to approve document? ');"  >
                       <button class="btn flat-buttons btn-primary site-btn waves-effect waves-button">Approve</button></a>
                  <a onclick ="return confirm('Are you sure want to disapproved document? ');" href="{{ url('/admin_users/dispprove/'.$user_info->id) }}">
                       <button class="btn flat-buttons btn-danger site-btn waves-effect waves-button">Disapprove</button></a>
                <?php } ?>
              </div>
            <?php } ?>
              
        </div>
      </div> -->

      </div>
       <div class="row">
            <div class="profileform_right col-md-12">
              <label class="col-sm-2">Bio</label>
              <div class="profileinput col-sm-10 nopadding">
                
                <p><span class="collon">:</span>{{ $user_info->bio }}</p>
                <!-- <input type="text" name="bio" id="bio" class="form-control" value="{{ $user_info->bio }}" placeholder="" readonly>
                <i class="fa fa-check bio"></i> -->
              </div>
            </div>
        </div>
      
      </div>
      </div>
       </div>

    </div>
</div>
        </div>
@if(count($user_documents) > 0)
<div class="seredback">
<div class="row">
<div class="mb-2">
<h4>Documents</h4>
</div>
@foreach($user_documents as $documents)
 <?php   
  $cpfname = $documents->document;
  $cpfExt = substr($cpfname, strrpos($cpfname, '.') + 1);
  if($cpfExt == 'pdf'){ ?>
  <div class="dcmt-pblc">
    <div class="col-sm-2 mb-2">
    <a href="{{ url('/public') }}/images/user_document/{{ $documents->document }}" target="_blank" title="{{ $documents->document}}" class="imgfree">
      <img src="{{ url('/public/images/') }}/cpfpdf.png"  style="width: 100%;height:auto;"> 
    </a>
    <?php } elseif($cpfExt == 'doc') { ?>
     <div class="col-sm-2 mb-2">
    <a href="{{ url('/public') }}/images/user_document/{{ $documents->document }}" target="_blank" title="{{ $documents->document}}" class="imgfree"><img src="{{ url('/public/images/') }}/cpfdoc.png" class="img-" style="width: 100%;height:auto;"></a>
    <?php } elseif($cpfExt == 'docx') { ?>
     <div class="col-sm-2 mb-2">
    <a href="{{ url('/public') }}/images/user_document/{{ $documents->document }}" target="_blank" title="{{ $documents->document}}" class="imgfree"><img src="{{ url('/public/images/') }}/cpfdocx.png" class="img-" style="width: 100%;height:auto;"></a>
    <?php } else { ?>
    <div class="col-sm-2 mb-2">
    <a href="{{ url('/public') }}/images/user_document/{{ $documents->document }}" target="_blank" title="{{ $documents->document}}" class="imgfree"><img src="{{ url('/public') }}/images/user_document/{{ $documents->document }}"  id="prvdoc_{{ $documents->id }}" style=""></a>
    <?php } ?>
<!-- <div class="col-sm-3"> <a href="{{ url('/public') }}/images/user_document/{{ $documents->document }}" target="_blank" title="{{ $documents->document}}" class="imgfree"><img src="{{ url('/public') }}/images/user_document/{{ $documents->document }}" class="img-responsive"></a> -->
<div class="col-sm-12 content_box">
  <h4>Documents Status:</h4>
    <div class="col-sm-12">
       <?php  if($documents->document_status == '1'){ ?>
      <span class="btn btn-success">Approved</span>
      <?php } else if($documents->document_status == '2'){ ?>
      <span  class="btn btn-danger">Dined</span>
      <?php } else { ?>
      <a  href="{{ url('/admin_users/approve/'.$user_info->id.'/'.$documents->id) }}" onclick ="return confirm('Are you sure want to approve document? ');" class="btn flat-buttons btn-primary site-btn waves-effect waves-button" >Approve</a>
      <a onclick ="return confirm('Are you sure want to disapproved document? ');" href="{{ url('/admin_users/dispprove/'.$user_info->id.'/'.$documents->id) }}" class="btn flat-buttons btn-danger site-btn waves-effect waves-button">Deny</a>
      <?php } ?>
    </div>
</div>

</div>
@endforeach
</div>
</div>
</div>
<
@endif

@if($user_info->user_role =='Provider')
  @if(count($user_prices) >0)
  <div class="seredback">
    <div class="row">
      <div class="mb-2">
        <h4>Price</h4>
        </div>
        @foreach($user_prices as $prices)
        <div class="col-sm-9">
          {{ $prices->queries}}
        </div>
        <div class="col-sm-3 mb-2">&#82;&#36; {{ number_format($prices->amount,2,",",".") }}</div>
        @endforeach
        
    </div>
  </div>
  @endif
  @if(count($user_gallery) > 0)
  <div class="seredback">
  <div class="row">
  <div class="mb-2">
    <h4>Gallery</h4>
  </div>
  @foreach($user_gallery as $gallery)
  <div class="col-sm-3 imgfree mb-2">
    <img src="{{ url('/public') }}/images/userimages/{{ $gallery->image_name }}" class="img-responsive">
  </div>
  @endforeach
  
  </div>
  </div>
  @endif
@endif


      
      </div>     
</div>
        </div>
     
<style type="text/css">
  .browseimage img {
    height: 90px;
    width: 90px;
}
.browse_btn span {
    background-image: -webkit-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -moz-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -ms-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: -o-linear-gradient(top, #ffffff, #e1e1e1);
    background-image: linear-gradient(top, #ffffff, #e1e1e1);
    border: 1px solid #d6d6d6;
    border-radius: 5px;
    color: #666666;
    float: left;
    font-size: 15px;
    padding: 10px 30px;
    margin-top: 20px;
    cursor: pointer;
}
.browse_btn p {
    color: #996666;
    float: left;
    font-size: 16px;
    width: 100%;
}
.profileinput textarea {
    border: 0 none;
    box-shadow: none;
    color: #444545;
    float: left;
    font-size: 20px;
    padding: 0;
    width: calc(100% - 30px);
}
.profileform_right label {
    color: #808080;
    font-family: Lato;
    font-size: 13px;
    font-weight: normal;
}
</style>      
@stop      