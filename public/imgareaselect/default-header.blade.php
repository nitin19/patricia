<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>@yield('title')</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ url('/public') }}/images/icon.png" />
  <!-- Bootstrap -->
  <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/style.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/change.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/new.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/dropdown.css" rel="stylesheet">
  <link href="{{ url('/public') }}/css/table-css.css" rel="stylesheet">
   <link href="{{ url('/public') }}/css/lightbox.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="{{ url('/public') }}/css/animate.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/mapsjs-ui.css"/>
  <link rel="stylesheet" type="text/css" href="{{ url('/public') }}/css/other.css" />
  <link href="{{ url('/public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap-multiselect.css" type="text/css">
  <link href="{{ url('/public/') }}/css/bootstrap-datetimepicker.css" rel="stylesheet">
  <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
  <link href="{{ url('/public') }}/css/jquery.raty.css" rel="stylesheet">
 
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.min.js"></script>
  <script src="{{ url('/public') }}/js/bootstrap-notify.min.js"></script>
  <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-core.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-service.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-ui.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-mapevents.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/bootstrap-multiselect.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
  <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
  <!-- <script type="text/javascript" src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script> -->
  <script type="text/javascript" src="{{ url('/public') }}/js/wow.min.js"></script>  
   <script type="text/javascript" src="{{ url('/public') }}/js/lightbox.min.js"></script>  
  <script type="text/javascript" src="{{ url('/public/') }}/js/moment.js"></script>
  <script type="text/javascript" src="{{ url('/public/') }}/js/bootstrap-datetimepicker.js"></script>
  <script src="{{ url('/public/') }}/js/jquery.raty.js"></script>
  <style type="text/css">
  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
</style>
</head>
<body>
  <header class="site-header" id="myHeader">
    <div class="top_header">
      <div class="container-fluid">
        <div class="col-sm-8 leftheader">
        </div>
        <div class="col-sm-4 rightheader">
          <?php
            $business_info = DB::table('business_settings')
                ->join('users','business_settings.user_id','=','users.id')
                ->where('users.user_role','=','admin')
                ->select('business_settings.*','users.user_role','users.id')->first();
          ?>
          <ul class="navbar-right">
            <!-- <span>Follow Us</span> -->
            <li class="tg-facebook"><a href="{{ $business_info->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li class="tg-twitter"><a href="{{ $business_info->twitter_link }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li class="tg-linkedin"><a href="{{ $business_info->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              <li class="tg-googleplus"><a href="{{ $business_info->google_link }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
            <!-- <li><a href="https://www.facebook.com/navizinhanca/  " target="_blank"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="https://www.linkedin.com/company/navizinhanca/
              " target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
              <li><a href="https://www.instagram.com/accounts/login/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
      <nav class="navbar navbar-default navigationbar">
        @if (Auth::check())
        @if(Auth::user()->user_role == 'Provider')
        @include('layouts.provider_header')
        @elseif(Auth::user()->user_role == 'Taker')
        @include('layouts.taker_header')
        @else
        <input type="hidden" value="{{Auth::user()->user_role}}" name="admin_role_check" id="check_role">
        @endif
        @else
        @include('layouts.login_header')
        @endif
      </nav>
    </header>
    <main> @yield('content') </main>
    <footer> @include('layouts.footer') </footer>
    <script>
      $(document).ready( function() {
        if($('#check_role').val() == 'admin'){
         window.location.href="{{ url('/admin-dashboard') }}";
       }
       $('.success_show_ip').hide();
       $('.success_show').hide();
       $('.success_role').hide();
       $('.success_request').hide();

       $('.removewhishlist').click(function(){
        if(confirm("Are you sure?")){
          var parent_id = $(this).closest('div').attr('id');
          var userid = jQuery('#'+parent_id+' #userid').val();
          var home_url = '{{url("/home")}}'
          $.ajax({
            url: "{{ url('/wishlist/removewhish') }}",
            type: 'POST',
            data: { "_token": "{{ csrf_token() }}",'userid': userid },
            success: function(response) {
              if(response =='1') {
                $.notify({
                  message: 'User Removed From Wish List' 
                  },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                })
                // alert(window.location.pathname);
                $('#unwish_'+userid).remove(); 
                
                var pageno = '<?php echo Request::getQueryString(); ?>'.match(/\d+/);
                if(pageno[0]>1)
                {
                  if($('.searchresultdiv').length == 0){
                    var prepage = pageno[0]-1;
                    var current_location = "http://navizinhanca.com/wishlist?page=";
                    var newlocation_url = current_location.concat(prepage);
                    window.location.replace(newlocation_url);
                  }
                }
                else(pageno[0]==1)
                {
                  if($('.searchresultdiv').length == 0){
                    location.reload();
                  }
                }


                //location.reload();
                // console.log(response);
                  // if($('.searchresultdiv').length == 0){
                  // var html = '<div class="empty-wishlist-div"> <h1 style="text-align: center;" id="wishlist-empty-text">Oops! Your Wishlist is Empty</h1>  <p align="center" id="wishlist-add-text"> You can Find New service Provider And Add them to Your wishlist</p><div class="provider_btn_div"><a href="'+home_url+'"><button type="button" class="Find_provider " name="Find service Provider Here">Find service Provider Here</button></a></div></div>';

                  // $('.searchresults').append(html);
                  // }

                }
              }
            });
        }
        else{
          return false;
        }
      });
       $('.removewhishlist_profile').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/removewhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') {
              // window.location.href="{{ url('/profile') }}/"+userid;
              location.reload();
              $.notify({
                message: 'Provider Remove From wishlist',
                },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              })
            }
          }
        });
      });
       $('.addwhishlists').click(function(){
        var parent_id = $("#whishlist_id").attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') {
              // window.location.href="{{ url('/profile') }}/"+userid;
              location.reload();
              $.notify({
                message: 'Provider Added To Wishlist',
                },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              })
            }
          }
        });
      });
       $('.addwhishlist').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') {

              $('.success_show').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist");
              // $('#Alert_Success').show();
              // setTimeout(function() {
              //   $('#Alert_Success').hide(); 
              // }, 4000);
              $.notify({
                message: 'Provider Added To Your Wishlist. Click Here To Check',
                url: '{{ url("/wishlist") }}',
                },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                  url_target: '_self',
              });

            } else if(response=='2') {

            } else {
              $.notify({
                message: 'Provider Already Added To Your Wishlist. Click Here To Check',
                url: '{{ url("/wishlist") }}',
                },{
                  type: 'danger',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                  url_target: '_self',
              });
            }

          }
        });
      });
       $('.addwhishlist_ip').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        var userip = jQuery('#'+parent_id+' #userip').val();
        $.ajax({
          url: "{{ url('/wishlistip/addwhish_ip') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
          success: function(response) {
            if(response=='1') {
              $('.success_show').show();
              $('.success_show_ip').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist_ip");
                $.notify({
                message: 'Provider Added To Your Wishlist. To Check Wishlist Please Create Account or Login Into Your Account.',
                },{
                  type: 'info',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              });
              }
              else if(response=='2') {
              }
              else
              {
                $.notify({
                message: 'Already Added To Your Wishlist. To Check Wishlist Please Create Account or Login Into Your Account.',
                },{
                  type: 'danger',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              });
              }
            }
          });
      });
       $('.removewhishlist_ip').click(function(){
        if(confirm("Are you sure?")){
              var parent_id = $(this).closest('div').attr('id');
              var userid = jQuery('#'+parent_id+' #userid').val();
              var userip = jQuery('#'+parent_id+' #userip').val();
              $.ajax({
                url: "{{ url('/wishlistip/removewhish_ip') }}",
                type: 'POST',
                data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
                success: function(response) {
                  if(response=='1') {
                    $.notify({
                      message: 'User Removed From Wish List' 
                      },{
                      type: 'success',
                      offset: 
                      {
                        x: 10,
                        y: 130
                      },
                      animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                      },
                    })
                    $('#unwish_'+userid).hide();
                  }
                }
              });
        }
      });
       $.validator.addMethod("alphaLetterNumber", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
      });
       $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
        return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
      });
       $.validator.addMethod("alphaLetter", function(value, element) {
        return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
      });
       $("#loginform").validate({
        rules: {
          email: {
            required: true,
            email: true,
            maxlength: 120
          },
          password: {
            required: true
          }
        },
        messages: {
          email: {
            required: "Please enter email.",
            email: "Please enter a valid email.",
            maxlength: "Maximum 120 characters allowed."
              // remote: "Email already in use."
            },
            password: {
              required: "Please enter password."
            }
          },
          submitHandler: function(form) {
            form.submit();
          }
        });
       $("#contactform").validate({
        rules: {
          first_name: {
            required: true
          },
          last_name: {
            required: true
          },
          email: {
            required: true,
            email: true
          },
          phonenumber: {
            required: true,
            minlength: 10,
            maxlength: 14
          }
        },
        messages: {
          first_name: {
            required: "Please enter first name."
          },
          last_name: {
            required: "Please enter last name."
          },
          email: {
            required: "Please enter email.",
            email: "Please enter correct email."
          },
          phonenumber: {
            required: "Please enter phone number.",
            minlength: "Minimum 10 characters required.",
            maxlength: "Maximum 14 characters allowed."
          }
        },
        submitHandler: function(form) {
          form.submit();
        }
      });

       $("#carddetailform").validate({
        rules: {
          paymentname: {
            required: true
          },
          card_num: {
            required:true,
            minlength:16,
            maxlength:20
          },
          exp_month:{
            required: true
          },
          exp_year:{
            required : true
          },
          cvv:{
            required:true,
            minlength: 3,
            maxlength: 4
          }
        },
        messages: {
          paymentname: {
            required: "Please enter your name."
          },
          card_num: {
            required: "Please enter card Number",
            minlength: "Minimum 16-digits are required.",
            maxlength: "Maximum 20-digits are required."
          },
          exp_month: {
            required: "Please select expiry month of card."
          },
          exp_year: {
            required: "Please select expiry year of card."
          },
          cvv: {
            required:"Please enter your cvv",
            minlength: "Minimum 3-digits are required.",
            maxlength: "Maximum 4-digits are required."
          }
        },
        submitHandler: function(form) {
          $.ajax({
            url: "{{ url('/dashboard/save_card') }}",
            type: 'POST',
            data: $(form).serialize(),
            success: function(response) {
              if(response == 'true')
              {
                    $.notify({
                    message: 'Account Info Saved Successfully!',
                    },{
                    type: 'success',
                    offset: 
                    {
                      x: 10,
                      y: 130
                    },
                    animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
                    },
                  });
              }
              // $('.success_show').show();
            }
          });
        }
      });
      //  $("#closeaccount").validate({
      //   rules: {
      //     close_reason: {
      //       required: true
      //     }
      //   },
      //   messages: {
      //     close_reason: {
      //       required: "Please Select Reason."
      //     }
      //   },
      //   submitHandler: function(form) {
      //     $.ajax({
      //       url: "{{ url('/dashboard/closeaccount') }}",
      //       type: 'POST',
      //       data: $(form).serialize(),
      //       success: function(response) {
      //         $('.success_request').show();
      //         setTimeout(function() {
      //           $('#logout-form').submit();
      //         }, 3000);
      //       }
      //     });
      //   }
      // });

       wow = new WOW(
       {
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
      );
       wow.init();
        /*    document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
      };*/
      (function(){
        $('#serviceslider').carousel({ interval: 5000 });
      }());
      (function(){
        $('.carousel-showmanymoveone .item').each(function(){
          var itemToClone = $(this);
          for (var i=1;i<6;i++) {
            itemToClone = itemToClone.next();
            // wrap around if at end of item collection
            if (!itemToClone.length) {
              itemToClone = $(this).siblings(':first');
            }

            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
            .addClass("cloneditem-"+(i))
            .appendTo($(this));
          }
        });
      }());
      jQuery('#uploadbtn').on('click', function() {
        jQuery('#brwsebtn').click();
      });

      jQuery("#brwsebtn").change(function() {
        var file = this.files[0];
        // console.log(file);
        var imagefile = file.type;
        var imageTypes= ["image/jpeg","image/png","image/jpg"];

        if(imageTypes.indexOf(imagefile) == -1)
        {
          jQuery(".image123_alert").html("<div class='alert alert-danger' role='alert'><span class='msg-error'>Please Select A valid Image File Only jpeg, jpg and png Images type allowed</span></div>");
          $('.image123_alert').show();
          $('.image123_alert').css("opacity", "1");
          $('.next_1').prop('disabled','true');
          return false;
        }
        else
        {
          var reader = new FileReader();
          reader.onload = function(e){
            jQuery("#previewdiv").html('<img src="' + e.target.result + '" style="width:100px;height:100px;"/>');
          };
          reader.readAsDataURL(this.files[0]);
          $('.next_1').removeAttr('disabled','false');
        }
      });

      $(".list-group a").click(function() {
        $('.list-group a').removeClass('selected');
        $(this).addClass('selected');
      });

      window.onscroll = function() {myFunction()};
      var header = document.getElementById("myHeader");
      var sticky = header.offsetTop;
      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("sticky");
        } else {
          header.classList.remove("sticky");
        }
      }
      function myFunction_copy() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        document.execCommand("copy");
        alert("Copied the URL: " + copyText.value);
      }
      // Compete Profile js
      jQuery('#browsebtn').on('click', function() {
        jQuery('#user_images').click();
      });
      $(function() {
        // Multiple images preview in browser
        $('.image_alert').hide();
        var imagesPreview = function(input, placeToInsertImagePreview) {
          if (input.files) {
            var filesAmount = input.files.length;
            if(filesAmount <= '5'){
             for (i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
              reader.onload = function(event) {
                $($.parseHTML('<img style="margin:10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
              }
              reader.readAsDataURL(input.files[i]);
            }
            $('.img_alert').hide();
          }
          else{
            var error_html = '<p align="left" id=""> Sorry !you Cannot Add More Than 5 Images</p>';
            $('.img_alert').html('');
            $('.img_alert').append(error_html);
            $('.img_alert').show();
          }
        }
      };
      $('#user_images').on('change', function() {
        jQuery('#nofile').empty();
        jQuery('#browse_img').empty();
        imagesPreview(this, '#browse_img');
      });
    });
      var phone = jQuery('#phone').val();
      var zipcode = jQuery('#zipcode').val();
      var address = jQuery('#address').val();
      jQuery('.phone').show();
      jQuery('.zipcode').show();
      jQuery('.address').show();
      if(phone==''){
        jQuery('.phone').hide();
      }
      if(zipcode==''){
        jQuery('.zipcode').hide();
      }
      if(address==''){
        jQuery('.address').hide();
      }
      jQuery("#phone").keypress(function(){
        var phone_val = jQuery('#phone').val();
        if(phone_val==''){
          jQuery('.phone').hide();
        }
        else {
          jQuery('.phone').show();
        }
      });
      jQuery("#zipcode").keypress(function(){
        var zipcode_val = jQuery('#zipcode').val();
        if(zipcode_val==''){
          jQuery('.zipcode').hide();
        }
        else {
          jQuery('.zipcode').show();
        }
      });
      jQuery("#address").keypress(function(){
        var address_val = jQuery('#address').val();
        if(address_val==''){
          jQuery('.address').hide();
        }
        else {
          jQuery('.address').show();
        }
      });
      jQuery("#completionform").validate({
        rules: {
          phone: {
            required: true,
            minlength: 10,
            maxlength: 14,
            // matches:'/([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}/g'
          },
          zipcode: {
            required: true,
            minlength: 4,
            maxlength: 8
          },
          address: {
            required: true
          },
          profileimage:{accept: "png|jpe?g|gif", filesize: 1048576  },
        },
        messages: {
          phone: {
            required: "This field is required",
            minlength: "Minimum 10 Number required.",
            maxlength: "Maximum 14 Number allowed."
            // matches:'Invalid Format'
           
          },
          zipcode: {
            required: "This field is required",
            minlength: "Minimum 4 Number required.",
            maxlength: "Maximum 8 Number allowed."
          },
          address: {
            required: "This field is required"
          },
          profileimage:{
           accept: "File must be JPG, GIF or PNG, less than 1MB" ,
          }
        },
        submitHandler: function(form) {
         //  var file = $(this).files[0];
         //  console.log(file);
         //  var imagefile = file.type;
         //  var imageTypes= ["image/jpeg","image/png","image/jpg"];
         //  alert(imageTypes.indexOf(imagefile));
         //  if(imageTypes.indexOf(imagefile) == -1)
         //  {
         //    jQuery(".image123_alert").html("<div class='alert alert-danger' role='alert'><span class='msg-error'>34Please Select A valid Image File Only jpeg, jpg and png Images type allowed</span></div>");
         //     $('.image123_alert').show();
         //     $('.image123_alert').css("opacity", "1");
         //      return false;
         // }
         form.submit();
         
       }
     });
      /* phone number max number validation*/
      $('#phone').on('keydown',function(event){
       var key = event.keyCode || event.charCode;
       if($(this).val().length == 14){
        if(event.keyCode !== 8) {
          event.preventDefault();
        }
      }
    });

      jQuery("#complete_service").validate({
        rules: {
          'cat_name[]': {
            required: true
          }
        },
        messages: {
          'cat_name[]': {
            required: "This field is required"
          },
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
      $(".delete_btn").on('click', function(e){
        if(confirm("Are you sure you want to delete?")){
          var imageid = jQuery(this).attr('id');
          // alert('#prv_'+imageid);
          if(imageid!='') {
            jQuery.ajax({
              type : 'GET',
              url : "{{ url('/accountcomplete') }}/"+imageid,
              data:{ 'imageid': imageid, '_token': "{{ csrf_token() }}"},
              success :  function(resp) {
                if(resp="success"){
                 $('#prv_'+imageid).remove();
                 $('#'+imageid).remove();
               }
             }
           });
          }
        } else {
          return false;
        }
      });
    });
  </script>
  <!-- Compete Profile js */ -->
</body>
</html>
