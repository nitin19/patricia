<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'hd_password', 'profile_image', 'description', 'tittle', 'price', 'price_type', 'address', 'bio', 'additional_details', 'category', 'city', 'phone', 'dob', 'latitude', 'longitude', 'address_verified', 'approved_by_admin', 'passed_assesment', 'repeat_client', 'response_rate', 'response_time', 'profile_rating', 'drop_off', 'pick_up', 'security_question_1', 'security_question_answere_1', 'cat_id', 'sub_id', 'zipcode', 'cardnumber', 'cardname', 'exp_month', 'exp_year', 'cvv', 'referal_code', 'user_role', 'complete_status', 'close_request_action', 'is_active', 'is_deleted', 'resend_ac_email', 'resend_ac_email_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'hd_password',
    ];
    public function MainCategory(){
      return $this->hasOne('App\Models\MainCategory','main_cat_id','id');
    }
    public function isOnline()
    {   
      return Cache::has('user-is-online-' . $this->id);
    }
}
