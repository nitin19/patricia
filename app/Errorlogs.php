<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorlogs extends Model
{   
    protected $table = 'error_logs';

    protected $fillable = [
        'error_message', 'line_number', 'file_name', 'browser', 'operating_system', 'loggedin_id', 'ip_address'
    ];

    protected $hidden = [
        
    ]; 

    

}
