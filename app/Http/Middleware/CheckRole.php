<?php

namespace App\Http\Middleware;

use Closure;
// use App\Http\Middleware\Auth;
use Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::check())
      {
        if(Auth::user()->is_active == 1 && Auth::user()->approved_by_admin ==1){
          if(Auth::user()->user_role=='Provider'){
            if(Auth::user()->address =='' &&Auth::user()->phone=='' &&Auth::user()->cat_id ==''){
              return redirect('/serviceprovider/completeprofile');
            }else{
              return $next($request);
            }
          }
          if(Auth::user()->user_role=='Taker'){
            if(Auth::user()->address =='' &&Auth::user()->phone==''){
              return redirect('/servicetaker/profile');
            } else {
              return $next($request);
            }
          }

        }
        else{
          $request->session()->flash('error', 'plase activate your account');
              return redirect()->intended('/login');
        }
      }
        return $next($request);
    }
}
