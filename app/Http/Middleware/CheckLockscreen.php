<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class CheckLockscreen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('locked') === true)
        {
            // return $next($request);
            return redirect('/lockscreen');
        }
        else
        {
            return $next($request);
        }

            

    }
}
