<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorfourController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('pages.404error');
    }
}
?>