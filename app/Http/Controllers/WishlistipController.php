<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class WishlistipController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    try{
      $current_ip = $request->ip();
      $wish_listusers = DB::table('wish_list_ip')->where('ip_address', $current_ip)->paginate(6);
      $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','wislist')->where('is_active', '1')->where('is_deleted', '0')->first();
      return view('pages.wishlist_ip', compact('wish_listusers','menu_meta_details'))->with('i', ($request->input('page', 1) - 1) * 6);;
    }
    catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
  }

  public function addwhish_ip(Request $request){
    $userip = $request->userip;
    $userid = $request->userid;
    $check_exist = DB::table('wish_list_ip')->where('wish_profile_id',$userid)->where('ip_address',$userip)->count();
    if( $check_exist > 0 )
    {
      echo '0';
    }
    else{
    $insert_whish = DB::table('wish_list_ip')->insert(
      ['ip_address' => $userip, 'wish_profile_id' => $userid]
    );
    if($insert_whish) {
          echo '1';
        } else {
          echo '2';
       }
     }
  }
  public function removewhish_ip(Request $request){
    $userip = $request->userip;
    $userid = $request->userid;
    $remove_whish = DB::table('wish_list_ip')
    ->where('ip_address', $userip)
    ->where('wish_profile_id', $userid)
    ->delete();
    if($remove_whish) {
                  echo '1';
            } else {
                  echo '2';
            }
  }
}
