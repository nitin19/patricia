<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\BusinessSetting;//Import model business in controller

class AdminBusinessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

    }
    public function create(){
      try{
        $business_setting = BusinessSetting::where('user_id',Auth::user()->id)->first();
        return view('admin.business.business_setting',compact('business_setting'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }
    }
    public function store(Request $request)
    {
      try{
        $check_exixting = BusinessSetting::where('user_id',Auth::user()->id)->first();

        if(!empty($check_exixting)){
          if($request->hasfile('header_logo')) {
            $image1 = $request->file('header_logo');
            $name1 = time() . '.' . $image1->getClientOriginalName();
            $image_resize1 = Image::make($image1->getRealPath());
            $image_resize1->resize(480, 400);
            $image_resize1->save(public_path('/images/admin-assets/'.$name1),60);
            $header_logo= $name1;
          }
          else{
            // $header_image_old = $request->old_header_image;
            // $header_name_old = time() . '.' . $header_image_old->getClientOriginalName();
            $header_logo = $request->old_header_image;
          }
          if($request->hasfile('footer_logo')) {
            $image2 = $request->file('footer_logo');
            $name2 = time() . '.' . $image2->getClientOriginalName();
            $image_resize2 = Image::make($image2->getRealPath());
            $image_resize2->resize(480, 400);
            $image_resize2->save(public_path('/images/admin-assets/'.$name2),60);
            $footer_logo = $name2;
          }
          else{
            // $footer_image_old = $request->old_footer_image;
            // $footer_name_old = time() . '.' . $footer_image_old->getClientOriginalName();
            $footer_logo = $request->old_footer_image;
          }
          if($request->hasfile('admin_profile_picture')) {
            $image3 = $request->file('admin_profile_picture');
            $name3 = time() . '.' . $image3->getClientOriginalName();
            $image_resize3 = Image::make($image3->getRealPath());
            $image_resize3->resize(480, 400);
            $image_resize3->save(public_path('/images/admin-assets/'.$name3),60);
            $admin_profile_picture = $name3;
          }
          else{
            // $admin_image_old = $request->old_admin_image;
            // $admin_name_old = time() . '.' . $admin_image_old->getClientOriginalName();
            $admin_profile_picture = $request->old_admin_image;
          }
          $check_exixting->user_id = Auth::user()->id;
          $check_exixting->name = $request->name;
          $check_exixting->email = $request->email;
          $check_exixting->phone = $request->phone;
          $check_exixting->city = $request->city;
          $check_exixting->state = $request->state;
          $check_exixting->country = $request->country;
          $check_exixting->address = $request->address;
          $check_exixting->zipcode = $request->zipcode;
          $check_exixting->facebook_link = $request->facebook_link;
          $check_exixting->twitter_link = $request->twitter_link;
          $check_exixting->google_link = $request->google_link;
          $check_exixting->linkedin = $request->linkedin;
          $check_exixting->header_logo = $header_logo;
          $check_exixting->footer_logo = $footer_logo;
          $check_exixting->admin_profile_picture = $admin_profile_picture;
          $business_save = $check_exixting->update();
        }
        else{
          if($request->hasfile('header_logo')) {
            $image1 = $request->file('header_logo');
            $name1 = time() . '.' . $image1->getClientOriginalName();
            $image_resize1 = Image::make($image1->getRealPath());
            $image_resize1->resize(480, 400);
            $image_resize1->save(public_path('/images/admin-assets/'.$name1),60);
            // $image_resize1->save(public_path('/images/admin-assets/' .$name1));
            $header_logo= $name1;
          }
          if($request->hasfile('footer_logo')) {
            $image2 = $request->file('footer_logo');
            $name2 = time() . '.' . $image2->getClientOriginalName();
            $image_resize2 = Image::make($image2->getRealPath());
            $image_resize2->resize(480, 400);
            $image_resize2->save(public_path('/images/admin-assets/'.$name2),60);
            // $image_resize2->save(public_path('/images/admin-assets/' .$name2));
            $footer_logo = $name2;
          }
          if($request->hasfile('admin_profile_picture')) {
            $image3 = $request->file('admin_profile_picture');
            $name3 = time() . '.' . $image3->getClientOriginalName();
            $image_resize3 = Image::make($image3->getRealPath());
            $image_resize3->resize(480, 400);
            $image_resize3->save(public_path('/images/admin-assets/'.$name3),60);
            // $image_resize3->save(public_path('/images/admin-assets/' .$name3));
            $admin_profile_picture = $name3;
          }
          $business = new BusinessSetting();
          $business->user_id = Auth::user()->id;
          $business->name = $request->name;
          $business->email = $request->email;
          $business->phone = $request->phone;
          $business->city = $request->city;
          $business->state = $request->state;
          $business->country = $request->country;
          $business->address = $request->address;
          $business->zipcode = $request->zipcode;
          $business->facebook_link = $request->facebook_link;
          $business->twitter_link = $request->twitter_link;
          $business->google_link = $request->google_link;
          $business->linkedin = $request->linkedin;
          $business->header_logo = $header_logo;
          $business->footer_logo = $footer_logo;
          $business->admin_profile_picture = $admin_profile_picture;
          $business_save = $business->save();

        }
        if($business_save){
          return redirect('admin/business/create')->with('message','Admin Profile Updated successfully');
        }
        else{
          return redirect('admin/business/create')->with('message','Admin Profile Not Updated!!!!');
        }
        }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }
    }
    public function edit($id)
    {
    }
    public function update(Request $request,$id)
    {
    }
    public function destroy($id)
    {
    }
}
