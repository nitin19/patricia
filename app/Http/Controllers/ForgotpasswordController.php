<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Contact;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class ForgotpasswordController extends Controller
{    

    public function __construct()
    {
        //$this->middleware('auth');
    }

      public function index(Request $request)
    {
        try{
            $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','invite-friend')->where('is_active', '1')->where('is_deleted', '0')->first();
            return view('forgotpassword.index',compact('menu_meta_details'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        try{
            $request11 = array();
            $token = $request->_token;
            $email = $request->email;
            $user_info = DB::table('users')->where('email', $email)->first();
            $password_change_link = "http://navizinhanca.com/change-new-password/".$email;
            $exist = DB::table('users')->where('email', $email)->count();

            if($exist>0){
            $name = $user_info->name;
            $password = $user_info->hd_password;
                
            $to = $email;
            $from = 'NaVizinhanca  <contato@navizinhanca.com>';
            // $from_email = "navizinhanca  <contato@navizinhanca.com>";
               
            $logo_url = url('/public').'/images/logo.png';
            $mail_bg_img = url('/public').'/images/bg.jpg';
            $email_icon = url('/public').'/images/email_icon_mail.png';
            $LI_png = url('/public').'/images/LI.png';
            $fb_png = url('/public').'/images/fb.png';
            $insta_png = url('/public').'/images/insta.png';
            $messageBody = '';
            $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns:v="urn:schemas-microsoft-com:vml">
              <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                  <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                  <!--[if !mso]><!-- -->
                  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                  <!-- <![endif]-->
                  <title>Email Template</title>
                  <style type="text/css">
                  * {font-family: "Lato", sans-serif;}
                  .ii a[href] { text-decoration: none!important; }
                  </style>
              </head>
            <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
            <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                            <tr>
                                                <td align="center" height="70" style="height:70px;">
                                                    <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <hr>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                        <div style="line-height: 35px">
                                            <span style="color: #1497d5;">Solicitação de troca de senha</span>
                                        </div>
                                        <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Você utilizará sua senha em cada acesso que fizer no NaVizinhança.<br>
                                        Tente memorizá-la para rápido acesso ao site. </p>
                                      <p style="color: #333333; font-size: 19px!important;font-weight:400; line-height: 35px;margin-top:5px;"><span style="color: #1497d5;">Importante:</span> Sua senha é confidencial.<br> 
                                           Para sua segurança, não a compartilhe com ninguém.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="hide">
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff"  style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                    <tr>
                        <td align="center">
                            <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                                <tr>
                                    <td height="25">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table border="0" width="450" align="center" cellpadding="0" cellspacing="0" class="container580">
                                            <tr>
                                                <td width="30">&nbsp;</td>
                                                <td align="center" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                                                 
                                                    <div  style="color: #0c3e64;
                                                        font-size: 19px;
                                                        font-weight: 700;">
                                                        <b style="color: #1497d5;
                                                        padding-right: 5px;">Nome:</b>' . $name .'
                                                    </div>
                                                    <div style="color: #0c3e64;
                                                        font-size: 19px;
                                                        font-weight: 700;"><b style="color: #1497d5;
                                                        padding-right: 5px;">E-mail:</b>  ' . $email .'
                                                    </div>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                    </tr>
                    <table border="0" width="690" align="center" cellpadding="0" cellspacing="0"  style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;"> 
                        <tr>
                            <td align="center">
                                <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                    <tr>
                                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                            <div style="line-height: 26px;">
                                                <a href="'.$password_change_link.'" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Trocar senha</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="70">&nbsp;</td>  
                                    </tr>
                                        </table>
                                    </td>
                                </tr>
                    </table>
                </table>
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="f4f4f4"  style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                                <tr> 
                                    <td>
                                        <div class="social-icons" style="text-align: center;">
                                            <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                                <img src="'.$insta_png.'">
                                            </a>
                                            <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                                <img src="'.$LI_png.'">
                                            </a>
                                            <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                                <img src="'.$fb_png.'">
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                </table>
                </table>
              </body>
            </html>';
            $subject ='NaVizinhança: Troca de senha';
            $headers = "From: " .$from  . "\r\n".'X-Mailer: PHP/' . phpversion();
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            // send email
            if(mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com')){
                 $meassg ="Link para recuperação de senha enviado no seu e-mail.";
                
                $request11['success']= True;
                $request11['msgCode']= '1';
                $request11['message'] = $meassg;
            }
            }
            else{
                $request11['success']= False;
                $request11['msgCode']= '0';
               $request11['message']= 'O e-mail digitado não está cadastrado em nosso banco de dados.';
                //  return redirect()->action(
                // 'ForgotpasswordController@index')->with('error','There is no account with the information that you provided.'); 
            }
             echo json_encode($request11);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }

    public function show($id)
    {

    }


    public function edit($id)
    {
  
    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
