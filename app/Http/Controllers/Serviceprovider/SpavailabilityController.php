<?php
namespace App\Http\Controllers\Serviceprovider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Models\Spavailability;

class SpavailabilityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::id();
        $spavailabilityInfo = Spavailability::where('user_id', $userid)->where('status', 1)->first();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/spavailablty')->where('is_active', '1')->where('is_deleted', '0')->first();
        return view('availablity.index', compact('spavailabilityInfo','menu_meta_details'));
    }

    public function updatedata(Request $request) {
          $userid = Auth::id();
          $openhrs = '1';
          $day_monday_chk = trim($request->day_monday);
          if($day_monday_chk == '1') {
            $day_monday = '1';
          } else {
            $day_monday = '0';
          }
          $monday_start = trim($request->monday_start);
          $monday_end = trim($request->monday_end);

          $day_tuesday_chk = trim($request->day_tuesday);
          if($day_tuesday_chk == '1') {
            $day_tuesday = '1';
          } else {
            $day_tuesday = '0';
          }
          $tuesday_start = trim($request->tuesday_start);
          $tuesday_end = trim($request->tuesday_end);

          $day_wednesday_chk = trim($request->day_wednesday);
          if($day_wednesday_chk == '1') {
            $day_wednesday = '1';
          } else {
            $day_wednesday = '0';
          }
          $wednesday_start = trim($request->wednesday_start);
          $wednesday_end = trim($request->wednesday_end);

          $day_thursday_chk = trim($request->day_thursday);
          if($day_thursday_chk == '1') {
            $day_thursday = '1';
          } else {
            $day_thursday = '0';
          }
          $thursday_start = trim($request->thursday_start);
          $thursday_end = trim($request->thursday_end);

          $day_friday_chk = trim($request->day_friday);
          if($day_friday_chk == '1') {
            $day_friday = '1';
          } else {
            $day_friday = '0';
          }
          $friday_start = trim($request->friday_start);
          $friday_end = trim($request->friday_end);

          $day_saturday_chk = trim($request->day_saturday);
          if($day_saturday_chk == '1') {
            $day_saturday = '1';
          } else {
            $day_saturday = '0';
          }
          $saturday_start = trim($request->saturday_start);
          $saturday_end = trim($request->saturday_end);

          $day_sunday_chk = trim($request->day_sunday);
          if($day_sunday_chk == '1') {
            $day_sunday = '1';
          } else {
            $day_sunday = '0';
          }
          $sunday_start = trim($request->sunday_start);
          $sunday_end = trim($request->sunday_end);
      
          $isCompleted = '1';
          //DB::enableQueryLog();
        $updateSpavailability = Spavailability::where('user_id', $userid)->update(['openhrs'=>$openhrs, 'day_monday'=>$day_monday, 'monday_start'=>$monday_start, 'monday_end'=>$monday_end, 'day_tuesday'=>$day_tuesday, 'tuesday_start'=>$tuesday_start, 'tuesday_end'=>$tuesday_end, 'day_wednesday'=>$day_wednesday, 'wednesday_start'=>$wednesday_start, 'wednesday_end'=>$wednesday_end, 'day_thursday'=>$day_thursday, 'thursday_start'=>$thursday_start, 'thursday_end'=>$thursday_end, 'day_friday'=>$day_friday, 'friday_start'=>$friday_start, 'friday_end'=>$friday_end, 'day_saturday'=>$day_saturday, 'saturday_start'=>$saturday_start, 'saturday_end'=>$saturday_end, 'day_sunday'=>$day_sunday, 'sunday_start'=>$sunday_start, 'sunday_end'=>$sunday_end, 'isCompleted'=>$isCompleted]);

        //dd(DB::getQueryLog());
        
        if($updateSpavailability) {
          echo '1';
        } else {
          echo '0';
        }
         
         //return redirect('/serviceprovider/spavailablty')->with('success', 'Data saved successfully!');
    }


    public function savedata(Request $request) {
      
        $userid = Auth::id();
        $openhrs = '0';
       
        $day_monday = '1';
        $monday_start = '00:00';
        $monday_end = '23:59';

        $day_tuesday = '1';
        $tuesday_start = '00:00';
        $tuesday_end = '23:59';

        $day_wednesday = '1';
        $wednesday_start = '00:00';
        $wednesday_end = '23:59';

        $day_thursday = '1';
        $thursday_start = '00:00';
        $thursday_end = '23:59';

        $day_friday = '1';
        $friday_start = '00:00';
        $friday_end = '23:59';

        $day_saturday = '1';
        $saturday_start = '00:00';
        $saturday_end = '23:59';

        $day_sunday = '1';
        $sunday_start = '00:00';
        $sunday_end = '23:59';
        $isCompleted = '1';
          
        // $saveSpavailability = Spavailability::where('user_id', $userid)->update(['openhrs'=>$openhrs, 'day_monday'=>$day_monday, 'monday_start'=>$monday_start, 'monday_end'=>$monday_end, 'day_tuesday'=>$day_tuesday, 'tuesday_start'=>$tuesday_start, 'tuesday_end'=>$tuesday_end, 'day_wednesday'=>$day_wednesday, 'wednesday_start'=>$wednesday_start, 'wednesday_end'=>$wednesday_end, 'day_thursday'=>$day_thursday, 'thursday_start'=>$thursday_start, 'thursday_end'=>$thursday_end, 'day_friday'=>$day_friday, 'friday_start'=>$friday_start, 'friday_end'=>$friday_end, 'day_saturday'=>$day_saturday, 'saturday_start'=>$saturday_start, 'saturday_end'=>$saturday_end, 'day_sunday'=>$day_sunday, 'sunday_start'=>$sunday_start, 'sunday_end'=>$sunday_end, 'isCompleted'=>$isCompleted]);
        //DB::enableQueryLog();
        $saveSpavailability = Spavailability::where('user_id', $userid)->update(['openhrs'=>$openhrs, 'isCompleted'=>$isCompleted]);
        //dd(DB::getQueryLog());
        if($saveSpavailability) {
          echo '1';
        } else {
          echo '0';
        }
         
         //return redirect('/serviceprovider/spavailablty')->with('success', 'Data saved successfully!');
    }


    

}
