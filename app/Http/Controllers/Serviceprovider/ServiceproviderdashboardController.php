<?php
namespace App\Http\Controllers\Serviceprovider;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Contact;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Spavailability;
use App\Models\Notification;
use IntlDateFormatter;
use DateTime;
// use App\Models\Spavailability;

class ServiceproviderdashboardController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index(Request $request)
	{
		try{
			$id = Auth::id();
			$ip         = $request->ip();
			$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
			if(empty($check_savailablitiy)){
				$spavailability = new Spavailability();
				$spavailability->user_id = Auth::user()->id;
				$spavailability->save();
			}
			$current_date = date("Y-m-d H:i:s");
			$user_info = DB::table('users')
			->where('id', $id)
			->where('is_active', '1')
			->where('is_deleted', '0')
			->where('approved_by_admin', '1')
			->first();
            
            if($user_info->latitude != '' && $user_info->longitude != '') {
                $latitude_center = $user_info->latitude;
                $longitude_center = $user_info->longitude;
            } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $latitude_center = $getips['lat'];
                  $longitude_center = $getips['lng'];
                } else {
                  $latitude_center = '-23.60332035036982';
                  $longitude_center = '-46.723359508984345';
                }
            }
            
			$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();

			$provider_price_lists = DB::table('provider_price_list')
			->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
			->where('provider_price_list.user_id', $id)
			->select('price_queries.*', 'provider_price_list.id', 'provider_price_list.amount')
			->get()->toArray();
			$main_category_detail = DB::table('main_category')->where('id', Auth::user()->cat_id)->first();
			$sub_category_detail = DB::table('category')->where('cat_id', Auth::user()->sub_id)->first();
			$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/dashboard')->where('is_active', '1')->where('is_deleted', '0')->first();

			return view('serviceprovider.dashboard', compact('user_info','spavailabilityInfo', 'provider_price_lists','main_category_detail','sub_category_detail','menu_meta_details','latitude_center','longitude_center'));
		}
		catch(\Illuminate\Database\QueryException $e){
			$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

			$errorlog                   = new Errorlogs;
			$errorlog->error_message    = $e->getMessage();
			$errorlog->line_number      = $e->getLine();
			$errorlog->file_name        = $e->getFile();
			$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
			$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
			if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
			$errorlog->ip_address       = "";
			$errorlog->save();
			return view('errors.custom',compact('customerror')); 
		}catch(\Exception $e){ 
			$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

			$errorlog                   = new Errorlogs;
			$errorlog->error_message    = $e->getMessage();
			$errorlog->line_number      = $e->getLine();
			$errorlog->file_name        = $e->getFile();
			$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
			$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
			$errorlog->loggedin_id      = Auth::user()->id;
			$errorlog->ip_address       = "";
			$errorlog->save();
			return view('errors.custom',compact('customerror')); 
		}
	}


  function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    return $data;
   } 
   
/* function name  : Booking and messages 
 work :display all provider bookings
parms:request 
*/
public function bookings_messages(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();
		$booking_payment_confirm = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '1')
		->orderBy('booking_id', 'desc')
		->paginate(20);

		$booking_payment_confirm_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '1')
		->count();
		$booking_payment_pending_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '0')
		->count();
		$booking_payment_past_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '2')
		->count();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/bookings')->where('is_active', '1')->where('is_deleted', '0')->first();
		return view('serviceprovider.dashboard-content.confirm_booking', compact('user_info', 'booking_payment_confirm','role', 'booking_payment_confirm_count','spavailabilityInfo','booking_payment_pending_count','booking_payment_past_count','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}


/* function name  : Booking and messages 
 work :display all provider bookings
parms:request 
*/
public function bookings_pending(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();

		$booking_payment_pending = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '0')
		->orderBy('booking_id', 'desc')
		->paginate(20);
		$booking_payment_confirm_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '1')
		->count();
		$booking_payment_pending_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '0')
		->count();
		$booking_payment_past_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '2')
		->count();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/booking/pending')->where('is_active', '1')->where('is_deleted', '0')->first();


		return view('serviceprovider.dashboard-content.dashboard-booking-content.pending_booking', compact('user_info','role', 'booking_payment_pending', 'booking_payment_pending_count','spavailabilityInfo','booking_payment_confirm_count','booking_payment_past_count','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}


/* function name  : Booking and messages 
 work :display all provider bookings
parms:request 
*/
public function bookings_declined(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();
		
		$booking_payment_past = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '2')
		->orderBy('booking_id', 'desc')
		->paginate(20);
		$booking_payment_confirm_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '1')
		->count();
		$booking_payment_pending_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '0')
		->count();
		$booking_payment_past_count = DB::table('booking')
		->where('profile_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('booking_verified', '2')
		->count();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/booking/declined')->where('is_active', '1')->where('is_deleted', '0')->first();

		return view('serviceprovider.dashboard-content.dashboard-booking-content.decline_booking', compact('user_info', 'role',  'booking_payment_past', 'booking_payment_past_count','spavailabilityInfo','booking_payment_pending_count','booking_payment_confirm_count','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}


/* function name  : Account 
 work :display dashboard_profile_account
parms:request 
*/
public function dashboard_profile_account(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
		$category = DB::table('category')->get();
		$user_documents = DB::table('user_documents')
		->where('user_id', $id)
		->where('is_active', '1')
		->where('document_status','1')
		->where('is_deleted', '0')
		->get();
		$user_image = DB::table('user_gallery')
		->where('user_id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->get();

		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();

		$listresult = Notification::leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
		->select('notifications.*','booking.*','notifications.id as notify_id')
		->where('notifications.is_deleted','0')
		->where('notifications.notification_role', 'Taker')
		->where('booking.profile_id', $id)
		->orderBy('notifications.id', 'desc')
		->where('booking.booking_verified', '0')
		->get();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/account/details')->where('is_active', '1')->where('is_deleted', '0')->first();

		return view('serviceprovider.dashboard-content.profile_account', compact('countries','category','user_info', 'user_image','spavailabilityInfo','listresult','user_documents','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}

/* function name  : dashboard_profile_card_deatils 
 work :display dashboard_profile_account
parms:request 
*/
public function dashboard_profile_card_deatils(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$category = DB::table('category')->get();
		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/carddetails')->where('is_active', '1')->where('is_deleted', '0')->first();
		return view('serviceprovider.dashboard-content.dashboard-account-content.card_details', compact('category','user_info','spavailabilityInfo','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}

/* function name  : dashboard_notifications 
 work :display dashboard_profile_account
parms:request 
*/
public function dashboard_notifications(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$category = DB::table('category')->get();

		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();

	
        
	   $is_deleted = array('0','1');
        $read_status = array('3');
        $notificaton_type = array('booking','document');
          
        $listresult = Notification::select('notifications.*','notifications.id as notify_id','notifications.created_at as notify_time','users.*','users.address_verified')
          ->leftjoin('users', 'notifications.user_id', '=', 'users.id')
          ->whereIn('notifications.is_deleted',$is_deleted)
          ->whereIn('notifications.notificaton_type',$notificaton_type)
          ->where('notifications.user_id', $id)
          ->where('notifications.notification_role', 'Taker')
          ->whereNotIn('notifications.read_status',$read_status)
          ->orderBy('notifications.created_at', 'desc')
          ->Paginate(5);
        // dd(DB::getQueryLog());
         // exit();
          $url= url('/');
          $i=0;
        if($listresult){
            foreach($listresult as $notification){
                $fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                );
                $dt = new DateTime($notification->notify_time. "+1 days");
                $d = $dt->format('d');
                $m = $dt->format('m');
                $y = $dt->format('Y');
                $time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                if($notification->notificaton_type ='booking'){
                
                $booking_info = DB::table('booking')->where('booking_id', '=', $notification->type_id)->first();
                if($booking_info->profile_id == $id){
		            $takerid = $booking_info->user_id;
		        } else {
		            $takerid = $booking_info->profile_id;
		        }
		        $userinfo = DB::table('users')->where('id', '=', $takerid)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->first();
		        $name = $userinfo->name;
		          if($userinfo->profile_image =='') {
		            $imUrl =  $url.'/public/admin/images/gravtar.jpeg'; } else {
		            $imUrl = $url.'public/images/profileimage/'.$userinfo->profile_image;
		          }
		        }
		        $hyperLink = $url.'serviceprovider/booking/pending';
		        $msg = "Voce recebeu solicitação para um novo serviço." ;
                 
                if($notification->notificaton_type =='document'){
			        if($userinfo->profile_image =='') {
			          $imUrl =  $url.'/public/admin/images/gravtar.jpeg'; } else {
			          $imUrl = $url.'public/images/profileimage/'.$userinfo->profile_image;
			        }
			        if($userinfo->document_status == '1') {
			        $hyperLink = $url.'serviceprovider/profile';
			        $msg = "Parabéns!seu Documento é aprovado.";
			        } else {
			        $hyperLink = $url.'serviceprovider/profile';
			        $msg = "Desculpa! seu documento não foi aprovado.";
			        }
			        $name = $userinfo->name;    
			    }
               
              
                $listresult[$i]->provider_name = $name;
                $listresult[$i]->provider_image = $userinfo->profile_image;
                $listresult[$i]->notify_time = $time;
                $listresult[$i]->notify_message = $msg;
                $listresult[$i]->link =$hyperLink;
                
                 $i++;
            }
        }
       /* echo "<pre>";
          print_r($listresult);
          echo "</pre>";
          die();*/
         // print_r($listresult);die;
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/notifications/details')->where('is_active', '1')->where('is_deleted', '0')->first();

		return view('serviceprovider.dashboard-content.dashboard-account-content.notifications', compact('countries','category','user_info', 'user_image','spavailabilityInfo','listresult','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}

/* function name  : dashboard_close_request 
 work :display dashboard_profile_account
parms:request 
*/

public function account_close_request(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',0)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();
		$category = DB::table('category')->get();
		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();
		$menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/closeaccount')->where('is_active', '1')->where('is_deleted', '0')->first();
		return view('serviceprovider.dashboard-content.dashboard-account-content.close_account', compact('category','user_info','spavailabilityInfo','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}


/* function name  : Account 
 work :display dashboard_profile_account
parms:request 
*/
public function user_swicth_role(Request $request)
{
	try{
		$id = Auth::id();
		$check_savailablitiy =Spavailability::where('user_id',Auth::user()->id)->where('isCompleted',1)->first();
		if(empty($check_savailablitiy)){
			$spavailability = new Spavailability();
			$spavailability->user_id = Auth::user()->id;
			$spavailability->openhrs = '0';
			$spavailability->isCompleted = '1';
			$spavailability->save();
		}
		$current_date = date("Y-m-d H:i:s");
		$user_info = DB::table('users')
		->where('id', $id)
		->where('is_active', '1')
		->where('is_deleted', '0')
		->where('approved_by_admin', '1')
		->first();

		$spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/switch/role')->where('is_active', '1')->where('is_deleted', '0')->first();
		

		return view('serviceprovider.dashboard-content.switch_role', compact('user_info','spavailabilityInfo','menu_meta_details'));
	}
	catch(\Illuminate\Database\QueryException $e){
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}catch(\Exception $e){ 
		$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

		$errorlog                   = new Errorlogs;
		$errorlog->error_message    = $e->getMessage();
		$errorlog->line_number      = $e->getLine();
		$errorlog->file_name        = $e->getFile();
		$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
		$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
		$errorlog->loggedin_id      = Auth::user()->id;
		$errorlog->ip_address       = "";
		$errorlog->save();
		return view('errors.custom',compact('customerror')); 
	}
}

public function save_card(Request $request) {
	$card_num = $request->card_num;
	$paymentname = $request->paymentname;
	$exp_month = $request->exp_month;
	$exp_year = $request->exp_year;
	$cvv = $request->cvv;
	$id = Auth::id();
	$user_info = DB::table('users')
	->where('id', $id)
	->update(['cardnumber' => $card_num, 'cardname' => $paymentname, 'exp_month' => $exp_month, 'exp_year'=> $exp_year, 'cvv' => $cvv]);
	echo 'true';
}

public function switchdashboard(Request $request) {
	$current_userId = $request->current_userId;
	$role_switch = $request->role_switch;
	$role_update = DB::table('users')
	->where('id', $current_userId)
	->update(['user_role' => $role_switch]);

	if($role_update) {
		return redirect('/logout');
	}
}

public function closeaccount(Request $request)
{
	$current_userId = $request->current_userId;
	$close_reason = $request->close_reason;
	$close_account = DB::table('close_account_request')->insertGetId(
		['user_id' => $current_userId, 'close_reason' => $close_reason]
	);
	$close_accounts = DB::table('users')
	->where('id', $current_userId)
	->update(['is_active' => '0', 'close_request_action' => '1']);
	if($close_account!=''){
		$notifie = DB::table('notifications')->insert(
			['user_id' => $current_userId, 'notificaton_type' => 'closerequest','type_id' => $close_account]
		);
		$response['success']="true";
		$response['message']="suceesss";
	}
	else{
		$response['success']="false";
		$response['message']="somthing went wrong";
	}
	echo json_encode($response);

}
public function approved_status(Request $request)
{
	$result =array();
	$html = '';
	$booking_status = '';
	$booking_id = $request->booking_id;
	$status = $request->approve_status_val;
	$booking_updated = DB::table('booking')
	->where('booking_id', $booking_id)
	->update(['booking_verified' =>$request->approve_status_val]);
	if($status =='1')
	{
		$booking_msg1 = "Sua solicitação de serviço foi aceita";
	}
	else{
		$booking_msg1 = "Something!invalid.";
	}

	if($booking_updated)
	{
		$booking = DB::table('booking')
		->where('booking_id', $booking_id)
		->select('*')
		->first();
		$booking_status = $booking->booking_verified;
		$user_book_by = $booking->user_id;
		$booking_message = $booking->booking_message;
		$booking_amount = $booking->booking_amount;
		$book_date = $booking->booking_start_time;
		$booking_category = $booking->category;
		$booking_subcategory = $booking->sub_category;
		$booking_start_time = date('F j, Y ', strtotime($booking->booking_start_time));
		$booking_end_time = date('F j, Y ', strtotime($booking->booking_end_time));
		$user_info = DB::table('users')
		->where('id', $user_book_by)
		->first();
		if($booking_amount){
              $amount = $booking_amount;
            } else{
              $amount = 'Não definido pelo(a) contratante';
            } 
        if($book_date){
        	$fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                );
                $dt = new DateTime($book_date. "+1 days");
                $d = $dt->format('d');
                $m = $dt->format('m');
                $y = $dt->format('Y');
                $booking_date = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
            } else{
            	$booking_date = " ";
            }

		$notifie = DB::table('notifications')->insert(
            ['user_id' => $user_book_by, 'notificaton_type' => 'booking','type_id' => $booking_id,'notification_role' => Auth::user()->user_role]
          );
		$user_data = DB::table('users')->where('id',$user_book_by)->where('is_active','=',1)->where('is_deleted','=',0)->first();
		//$this->taker_notification($user_data->token);
		$name = Auth::user()->name;
		$booking_msg = "Novo status enviado para o contratante.";
		if($booking_message){ 
			$display_message = $booking_message; 
		} else {
           $display_message = "Provider is available for your booking.";
		}

		$email = Auth::user()->email;
		$to = $user_info->email;
		$phone = Auth::user()->phone;
		$logo_url = url('/public').'/images/logo.png';
		$mail_bg_img = url('/public').'/images/bg.jpg';
		$email_icon = url('/public').'/images/email_icon_mail.png';
		$LI_png = url('/public').'/images/LI.png';
		$fb_png = url('/public').'/images/fb.png';
		$insta_png = url('/public').'/images/insta.png';
		$messageBody = '';
		$messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns:v="urn:schemas-microsoft-com:vml">
          <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
              <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
              <!--[if !mso]><!-- -->
              <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
              <!-- <![endif]-->
              <title>Email Template</title>
              <style type="text/css">
              * {font-family: "Lato", sans-serif;}
              .ii a[href] { text-decoration: none!important; }
              </style>
          </head>
        <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;" width="690">
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                <tr>
                    <td align="center">
                        <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                        <tr>
                                            <td align="center" height="70" style="height:70px;">
                                                <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <hr>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                <tr>
                    <td align="center">
                        <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                            <tr>
                                <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                    <div style="line-height: 35px">
                                        <span style="color: #1497d5;">'.$booking_msg1.'</span>
                                    </div>
                                    <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" class="container590">
                                        <tr>
                                            <td align="center" style="color: #4a4545; font-size: 16px; line-height: 24px;">
                                                <div style="line-height: 24px">'.$display_message.'
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="hide">
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
            </table>
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                <tr>
                    <td align="center">
                        <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table border="0" width="450" align="center" cellpadding="0" cellspacing="0" class="container580">
                                        <tr>
                                            <td width="30">&nbsp;</td>
                                            <td align="left" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                                              <h2 style="text-align: center;">Dados do prestador de serviço</h2>
                                                <div  style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;">
                                                    <b style="color: #1497d5;
                                                    padding-right: 5px;">Nome:</b>' . $name .'
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">E-mail:</b>  ' . $email .'
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Celular:</b>  ' . $phone .'
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Serviço:</b>  ' . $booking_subcategory .' ('.$booking_category.')
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Data de preferência para o serviço:</b>'.$booking_date.'
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Preço estimado pelo serviço, conforme selecionado por você dentre as opções disponíveis no nosso site:</b>
                                                   '.$amount.'
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="30">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                </tr>
                <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;"> 
                    <tr>
                        <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                <tr>
                                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                        <div style="line-height: 26px;">
                                            <a href="https://www.navizinhanca.com/servicetaker/booking/confirmed" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;max-width:350px;display:inline-block;">Ver mais informações e avaliar o prestador de serviço</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="70">&nbsp;</td>  
                                </tr>
                                    </table>
                                </td>
                            </tr>
                </table>
            </table>
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="f4f4f4" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                            <tr> 
                                <td>
                                    <div class="social-icons" style="text-align: center;">
                                        <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                            <img src="'.$insta_png.'">
                                        </a>
                                        <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                            <img src="'.$LI_png.'">
                                        </a>
                                        <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                            <img src="'.$fb_png.'">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
            </table>
            </table>
          </body>
        </html>';
		$subject ="NaVizinhança: Solicitação de serviço aceita";
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$headers .= "From: " .$email. "\r\n";
		//$headers .= "CC: dev2.bdpl@gmail.com\r\n";


		$mailsend = mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com');

	}else{
		$booking_msg = "Sorry!!Somthing wrong.";
	}
	$result['status'] = $booking_status;
	$result['msg'] = $booking_msg;
	echo json_encode($result);
}
/*  fucntion name :denied _status*/

public function denied_status(Request $request)
{
	$result =array();
	$html = '';
	$booking_id = $request->booking_id;
	$status = $request->status_denied;
	$booking_updated = DB::table('booking')
	->where('booking_id', $booking_id)
	->update(['booking_verified' =>'2']);
	if($status =='2')
	{
		$booking_msg1 = "Sua solicitação de serviço foi negada";
	}else{
		$booking_msg1 = "Sorry!!Something Wrong.";
	}

	if($booking_updated)
	{
		$booking = DB::table('booking')
		->where('booking_id', $booking_id)
		->select('*')
		->first();
		$booking_status = $booking->booking_verified;
		$user_book_by = $booking->user_id;
		$booking_message = $booking->booking_message;
		$booking_amount = $booking->booking_amount;
		$book_date = $booking->booking_start_time;
		$booking_category = $booking->category;
		$booking_subcategory = $booking->sub_category;
		$booking_start_time = date('F j, Y ', strtotime($booking->booking_start_time));
		$booking_end_time = date('F j, Y ', strtotime($booking->booking_end_time));
		$user_info = DB::table('users')
		->where('id', $user_book_by)
		->first();
		$name = Auth::user()->name;
		$phone = Auth::user()->phone;
		if($booking_amount){
              $amount = $booking_amount;
        } else{
              $amount = 'Não definido pelo(a) contratante';
        }
        if($book_date){
        	$fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                );
                $dt = new DateTime($book_date. "+1 days");
                $d = $dt->format('d');
                $m = $dt->format('m');
                $y = $dt->format('Y');
                $booking_date = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
            } else{
            	$booking_date = " ";
            }
		
		$notifie = DB::table('notifications')->insert(
            ['user_id' => $user_book_by, 'notificaton_type' => 'booking','type_id' => $booking_id,'notification_role' => Auth::user()->user_role]
          );

		$booking_msg = "Novo status enviado para o contratante.";

		$email = Auth::user()->email;
		$to = $user_info->email;
		$userphone = $user_info->phone;
		$logo_url = url('/public').'/images/logo.png';
	    $mail_bg_img = url('/public').'/images/bg.jpg';
	    $email_icon = url('/public').'/images/email_icon_mail.png';
	    $LI_png = url('/public').'/images/LI.png';
	    $fb_png = url('/public').'/images/fb.png';
	    $insta_png = url('/public').'/images/insta.png';
	    $messageBody = '';
	    $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	    <html xmlns:v="urn:schemas-microsoft-com:vml">
	      <head>
	          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	          <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
	          <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
	          <!--[if !mso]><!-- -->
	          <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
	          <!-- <![endif]-->
	          <title>Email Template</title>
	          <style type="text/css">
	          * {font-family: "Lato", sans-serif;}
	          .ii a[href] { text-decoration: none!important; }
	          </style>
	      </head>
	    <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	    <table style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;" width="690">
	        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
	            <tr>
	                <td align="center">
	                    <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
	                        <tr>
	                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	                        </tr>
	                        <tr>
	                            <td align="center">
	                                <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
	                                    <tr>
	                                        <td align="center" height="70" style="height:70px;">
	                                            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
	                                        </td>
	                                    </tr>
	                                    <tr>
	                                        <td align="center">
	                                            <hr>
	                                        </td>
	                                    </tr>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	        </table>
	        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
	            <tr>
	                <td align="center">
	                    <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
	                        <tr>
	                            <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
	                                <div style="line-height: 35px">
	                                    <span style="color: #1497d5;line-height:35px;">'.$booking_msg1.'</span>
	                                </div>
	                                <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
	                        </tr>
	                        <tr>
	                            <td align="center">
	                                <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" class="container590">
	                                    <tr>
	                                        <td align="center" style="color: #4a4545; font-size: 16px; line-height: 24px;">
	                                            <div style="line-height: 24px">'.$booking_message.'
	                                            </div>
	                                        </td>
	                                    </tr>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	            <tr class="hide">
	                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	            </tr>
	        </table>
	        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
	            <tr>
	                <td align="center">
	                    <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
	                        <tr>
	                            <td height="25">&nbsp;</td>
	                        </tr>
	                        <tr>
	                            <td align="left">
	                                <table border="0" width="450" align="center" cellpadding="0" cellspacing="0" class="container580">
	                                    <tr>
	                                        <td width="30">&nbsp;</td>
	                                        <td align="left" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
	                                          <h2 style="text-align: center;">Dados do prestador de serviço</h2>
	                                            <div  style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;">
	                                                <b style="color: #1497d5;
	                                                padding-right: 5px;">Nome:</b>' . $name .'
	                                            </div>
	                                            <div style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;"><b style="color: #1497d5;
	                                                padding-right: 5px;">E-mail:</b>  ' . $email .'
	                                            </div>
	                                            <div style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;"><b style="color: #1497d5;
	                                                padding-right: 5px;">Celular:</b>  ' . $phone .'
	                                            </div>
	                                            <div style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;"><b style="color: #1497d5;
	                                                padding-right: 5px;">Serviço:</b>  ' . $booking_subcategory .' ('.$booking_category.') 
	                                            </div>
	                                            <div style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;"><b style="color: #1497d5;
	                                                padding-right: 5px;">Data de preferência para o serviço:</b> '.$booking_date.'
	                                            </div>
	                                            <div style="color: #0c3e64;
	                                                font-size: 19px;
	                                                font-weight: 700;">
                                                   <p style="color: #1497d5;padding-right: 5px;line-height: 10px;line-height:25px;">Preço estimado pelo serviço, conforme selecionado por você dentre as opções disponíveis no nosso site:</p>'.$amount.'
	                                            </div>
	                                        </td>
	                                    </tr>
	                                </table>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td height="30">&nbsp;</td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	            <tr>
	                <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
	            </tr>
	            <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;"> 
	                <tr>
	                    <td align="center">
	                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
	                            <tr>
	                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
	                            </tr>
	                            <tr>
	                                <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
	                                    <div style="line-height: 26px;">
	                                        <a href="https://www.navizinhanca.com/servicetaker/booking/denied" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Ir para o NaVizinhança</a>
	                                    </div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td height="70">&nbsp;</td>  
	                            </tr>
	                                </table>
	                            </td>
	                        </tr>
	            </table>
	        </table>
	        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="f4f4f4" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
	            <tr>
	                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	            </tr>
	            <tr>
	                <td align="center">
	                    <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
	                        <tr> 
	                            <td>
	                                <div class="social-icons" style="text-align: center;">
	                                    <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
	                                        <img src="'.$insta_png.'">
	                                    </a>
	                                    <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
	                                        <img src="'.$LI_png.'">
	                                    </a>
	                                    <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
	                                        <img src="'.$fb_png.'">
	                                    </a>
	                                </div>
	                            </td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	            <tr>
	                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
	            </tr>
	        </table>
	        </table>
	      </body>
	    </html>';
		$subject  = "NaVizinhança: Solicitação de serviço negada";
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$headers .= "From: " .$email. "\r\n";
		$mailsend = mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com');

	}else{
		$booking_msg = "Sorry!!Somthing wrong.";
	}
	$result['status'] = $booking_status;
	$result['msg'] = $booking_msg;
	echo json_encode($result);
}

public function create()
{

}

public function store(Request $request)
{
	$email = $request->email;
	$user_info = DB::table('users')->where('email', $email)->first();
	$exist = DB::table('users')->where('email', $email)->count();

	if($exist>0){
		$name = $user_info->name;
		$password = $user_info->hd_password;
		$to = $email;
		$logo_url = url('/public').'/images/yellow_logo_new.png';
		$mail_bg_img = url('/public').'/images/bg.png';
		$email_icon = url('/public').'/images/email_icon_mail.png';
		$mail_icon = url('/public').'/images/mail.png';
		$phone_img = url('/public').'/images/phone1.png';
		$thankyouimg = url('/public').'/images/thankyou.png';
		$subject = "Forgot Password";
		$message    = '';
		$message = '<html>
		<head>
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		</head>
		<body>
		<div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
		<div class="container">
		<div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
		<div class="" style="width: 500px;margin: 0 auto;">
		<div class="row">
		<div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
		<img style="width:150px" src="'.$logo_url.'" alt="Logo">
		</div>
		<div class="" style="text-align: center;padding-bottom: 0px;">
		<img style="width:60px" src='.$email_icon.'>
		</div>
		<div class="" style="text-align: center;">
		<h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span>'.$name.', </h6>
		</div>
		<div class="" style="text-align: center;padding: 15px 0px;">
		<img style="width: 230px;" src='.$thankyouimg.'>
		</div>
		<div class="" style="text-align: center;padding-bottom: 20px;">
		<h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">Please note your email and password.</h5>
		<p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">You need this every time you want to log on to the website to keep it safe and do not share it with anyone.</p>
		<ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
		<li><strong>Name: ' . $name  .'</li>
		<li><strong>Email: ' . $email  .'</li>
		<li><strong>Password: ' . $password  .'</li>
		</ul>
		</div>
		<div class="" style="text-align: center;padding-bottom: 10px">
		<a href="http://navizinhanca.com/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
		</div>
		<div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
		<p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
		</div>
		<div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
		<div class="col-sm-6">
		<div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
		<div style="float: left;width: 15%;margin-right: 20px;">
		<img style="width: 100%;" src='.$phone_img.'>
		</div>
		<div style="float: left;width: 70%;">
		<a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
		<a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
		</div>
		</div>
		</div>
		<div class="col-sm-6">
		<div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
		<div style="float: left;width:15%;margin-right:20px;">
		<img style="width:100%" src='.$mail_icon.'>
		</div>
		<div style="float: left;width: 70%;">
		<a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
		<a  style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		<div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
		<p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
		<p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
		</div>
		</div>
		</div>
		</body>
		</html>';

		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <NA VIZINHANCA>' . "\r\n";
		mail($to,$subject,$message,$headers,'-fg35.websitewelcome.com');


		$message = '';
		return redirect()->action(
			'ForgotpasswordController@index')->with('success','Thank You , please check your mail.');

	}
	else {
		return redirect()->action(
			'ForgotpasswordController@index')->with('error','There is no Google account with the info that you provided.');
	}
}

public function show($id)
{

}


public function edit($id)
{

}

public function update(Request $request, $id)
{

}

public function destroy($id)
{

}

public function update_account(Request $request) {

	$phone = $request->phone;
	$user_role = $request->user_role;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$user_id = $request->user_id;
	$address_str = $request->address;
	$address = urlencode($address_str);
	$url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=$address";
	$filestring = file_get_contents($url);
	$filearray = explode("\n", $filestring);
	$data = json_decode($filearray[0]);
	foreach($data as $k=>$v) {
		$view = $v->View;
		$view_count = count($view);
		if($view_count>0){
			foreach($view as $result_k=>$result_v) {
				$Result = $result_v->Result;
				$lat =  $Result[0]->Location->DisplayPosition->Latitude;
				$long =  $Result[0]->Location->DisplayPosition->Longitude;
				$PostalCode =  $Result[0]->Location->Address->PostalCode;
				$countryname =  $Result[0]->Location->Address->AdditionalData[0]->value;
				$cityname =  $Result[0]->Location->Address->AdditionalData[2]->value;
				if($lat!='' && $long!=''){
					$user_updated = DB::table('users')
					->where('id', $user_id)
					->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'country' => $countryname, 'city' => $cityname]);
				}
			}
		}
		else {
			return redirect('/dashboard')->with('error', 'Enter correct Address!');
		}
	}
	if($request->hasfile('user_images')) {
		foreach($request->file('user_images') as $images ) {
			$names = time() . '.' . $images->getClientOriginalName();
			$image_resizes = Image::make($images->getRealPath());
			$image_resizes->resize(480, 400);
			$image_resizes->save(public_path('images/userimages/' .$names));
			DB::table('user_gallery')->insert(
				['image_name' => $names, 'user_id' => $user_id]
			);
		}
	}
	if($request->hasfile('profileimage')){
		$image = $request->file('profileimage');
		$name = time() . '.' . $image->getClientOriginalName();
		$image_resize = Image::make($image->getRealPath());
		$image_resize->resize(480, 400);
		$image_resize->save(public_path('images/profileimage/' .$name));
		$user_updated = DB::table('users')
		->where('id', $user_id)
		->update(['profile_image' => $name]);
	}
	$user_updated = DB::table('users')
	->where('id', $user_id)
	->update(['phone' => $phone, 'address' => $address_str]);
	if($user_updated!=''){
		return redirect('/dashboard')->with('success', 'Profile updated!');
	}
	else {
		return redirect('/dashboard')->with('error', 'Profile Not updated!');
	}
}
public function destroyNotification(Request $request)
{
	$id=$request->id;

	$remove =  Notification::where('id', $id)
	->update(['is_deleted' => '1']);
	if($remove!='')
	{
		echo 1;
	}
}

/*public function taker_notification($token)
  {
    $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
    $token= $token;

    $notification = [
    	'type' => '1',
    	'title' => 'accept booking service',
        'message' => 'this is test',
        'sound' => true
    ];

    $fcmNotification = [
        //'registration_ids' => $tokenList, //multple token array
        'to'        => 'dQuU74PkQc4:APA91bHB1XI2zg3jb-l1P3_0lmOmeFsYvx0x11up4aiK6AT99uj6bvD_RbZk_bWWPpqrgudXDaF-sna7BhLq22Y_EMCKkGBqLxs6estbgrth3SpSBo_DyM7RlCyfeaquj4FiRhQcrQtI', //single token
        //'notification' => $notification,
        'data' => array(
        	'type' => '1',
    		'title' => 'accept booking service',
        	'message' => 'this is test',
        	'sound' => true
        )
    ];

    $headers = [
        'Authorization: key=AAAAmUDOXp0:APA91bEWX3JGuybnzJr4ctuF90QFCdGqBnlAN9fFM0uEBkvEwb9AKvSmHgNfgSeWr45xtsXrRg__itiaogOC6pLzg77xv7qqjqzIaQjw5ruCfwq8RHMCRIEF6SCUd0K7X8xEx3SOerrG',
        'Content-Type: application/json'
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = curl_exec($ch);
    curl_close($ch);
    print_r($result);

} */


}
