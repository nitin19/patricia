<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Feedback;
class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //$this->middleware('auth');
  }
  public function index(Request $request)
  {
    try{

      $ip = $request->ip();

       if(!empty(Auth::user()) && !empty(Auth::user()->user_role == 'Provider') && empty(Auth::user()->cat_id)) { 
            return redirect('/serviceprovider/completeprofile'); 
        } elseif(!empty(Auth::user()) && !empty(Auth::user()->user_role == 'Taker') && empty(Auth::user()->address)){     
             return redirect('/servicetaker/completeprofile'); 
        } else {

            $incomprofile_provider_users_Arr = $this->getIncompleteprofileproviders();

            $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT(user_id SEPARATOR ',')) as user_id"))->distinct()->first();
            $provideusers = $providers->user_id;
          //DB::enableQueryLog();
            $query = DB::table('users')->select('users.name as username','users.longitude','users.latitude','users.profile_image','users.profile_rating','users.id as p_id','users.description','users.price','users.price_type','users.city','users.sub_id','users.email','users.category','users.neighborhood','main_category.name as name','main_category.cat_image as main_cat_image','category.cat_name','category.cat_image');
            $query->join('main_category','users.cat_id','=','main_category.id');   
            $query->join('category','users.sub_id','=','category.cat_id');
            $query->where('users.user_role', 'Provider');
            $query->where('users.is_active', '1');
            $query->where('users.is_deleted', '0');
            $query->where('users.close_request_action', '0');
 
            if(Auth::user()){
                $query->where('users.id', '!=', Auth::user()->id);
            }
            
            if( count($incomprofile_provider_users_Arr) > 0 ) {
              $query->whereNotIn('users.id',$incomprofile_provider_users_Arr);
            }

           /*$query->where(
           function($querys) use ($provideusers){
                $querys->whereIn('users.id', explode(',', $provideusers));
                $querys->orWhere('users.user_role', 'Provider');
            });*/
            
            $query->distinct();
            $users = $query->get();
            
            /*echo "<pre>";
            print_r($users);die;*/
            $average_rating = Feedback::select(DB::raw('provider_id,AVG(rating) AS avg_rating ,rating,users.id as u_id,users.name as user_name'))->join('users','users.id','=','feedback.provider_id')->groupBy('feedback.provider_id')->where('users.is_active', '1')
            ->where('users.is_deleted', '0')->get();
            $users_count = count($users);
            $category = DB::table('main_category')->where('is_active', '1')->where('is_deleted', '0')->get();
            $main_category = DB::table('main_category')->leftJoin('category','main_category.id','=','category.main_cat_id')->where('main_category.is_active', '1')->where('main_category.is_deleted', '0')->get();
            
           // dd(DB::getQueryLog()); exit();

          if (Auth::user()) {   
              $aulatitude = Auth::user()->latitude;
              $aulongitude = Auth::user()->longitude;
              if($aulatitude != '' && $aulongitude != '') {
                $latitude_center = Auth::user()->latitude;
                $longitude_center = Auth::user()->longitude;
                $latitudeCurrent = Auth::user()->latitude;
                $longitudeCurrent = Auth::user()->longitude;
              } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $latitude_center = $getips['lat'];
                  $longitude_center = $getips['lng'];
                  $latitudeCurrent = $getips['lat'];
                  $longitudeCurrent = $getips['lng'];
                } else {
                  $latitude_center = '-23.60332035036982';
                  $longitude_center = '-46.723359508984345';
                  $latitudeCurrent = '-23.60332035036982';
                  $longitudeCurrent = '-46.723359508984345';
                }
              }
            } else {
              $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $latitude_center = $getips['lat'];
                  $longitude_center = $getips['lng'];
                  $latitudeCurrent = $getips['lat'];
                  $longitudeCurrent = $getips['lng'];
                } else {
                  $latitude_center = '-23.60332035036982';
                  $longitude_center = '-46.723359508984345';
                  $latitudeCurrent = '-23.60332035036982';
                  $longitudeCurrent = '-46.723359508984345';
                }
          }
            $testimonial = DB::table('testimonials')->where('is_active', '1')->where('is_deleted', '0')->get();
            $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','home')->where('is_active', '1')->where('is_deleted', '0')->first();
            return view('pages.home', compact('category','users','ip','users_count','main_category','average_rating','testimonial','menu_meta_details','latitude_center','longitude_center','latitudeCurrent','longitudeCurrent'));
        }
    }
    catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
  }
  
    public function getIncompleteprofileproviders(){

        $incomprofile_provider_users = DB::table('users')
                                      ->where('user_role', '=', 'Provider')
                                      ->where('is_active', '=', '1')
                                      ->where('is_deleted', '=', '0')
                                      ->where('close_request_action', '0')
                                      ->orderBy('id', 'DESC')
                                      ->get();

          $incomprofile_provider_users_Arr = array();  

          if( count($incomprofile_provider_users) > 0 ) {
           
            foreach($incomprofile_provider_users as $incomprofile_provider_user) {

            $provider_price_list_count = DB::table('provider_price_list')->where('user_id', '=', $incomprofile_provider_user->id)->count();

              if($incomprofile_provider_user->cpf_id =='' || $incomprofile_provider_user->phone =='' || $incomprofile_provider_user->cat_id =='' || $incomprofile_provider_user->sub_id =='' || $incomprofile_provider_user->zipcode =='' || $incomprofile_provider_user->address =='' || $incomprofile_provider_user->city =='' || $incomprofile_provider_user->neighborhood =='' || $incomprofile_provider_user->state =='' || $incomprofile_provider_user->country =='') {

                $incomprofile_provider_users_Arr[] = $incomprofile_provider_user->id;

              }

            }

          } 

        return $incomprofile_provider_users_Arr;                           
    }

  function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    return $data;
   }  

  public function image(Request $request) {
    if($request->hasfile('image_upload'))
    {
      $image = $request->file('image_upload');
      $name = time() . '.' . $image->getClientOriginalName();
      $image->move(public_path('images/profileimage/' , $name));

    }
  }
  public function coming_soon(){
    return view('pages.coming_soon');
  }


  public function subscribe_newsletter(Request $request){
            $token = $request->_token;
            $from_email = $request->email;
            $to_email = 'contato@navizinhanca.com';
            $logo_url = url('/public').'/images/yellow_logo_new.png';
            $mail_bg_img = url('/public').'/images/bg.png';
            $email_icon = url('/public').'/images/email_icon_mail.png';
            $mail_icon = url('/public').'/images/mail.png';
            $phone_img = url('/public').'/images/phone1.png';
            $thankyouimg = url('/public').'/images/thankyou.png';
            // $to = 'bob@example.com';

            $subject = 'Newsletter Subscription Request Accepted';

            $headers = "From: " .$to_email  . "\r\n";
            // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
           // $headers .= "CC: susan@example.com\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

            // $message = '<p><strong>Congrats</strong> You are sucessfully subscribed.</p>';
            $message = '<html>
            <head>
            <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            </head>
            <body>
            <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
            <div class="container">
            <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
            <div class="" style="width: 500px;margin: 0 auto;">
            <div class="row">
            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
            <img style="width:150px" src="'.$logo_url.'" alt="Logo">
            </div>
            <div class="" style="text-align: center;padding-bottom: 0px;">
            <img style="width:60px" src='.$email_icon.'>
            </div>
            <div class="" style="text-align: center;">
            <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span> </h6>
            </div>
            <div class="" style="text-align: center;padding: 15px 0px;">
            <img style="width: 230px;" src='.$thankyouimg.'>
            </div>
            <div class="" style="text-align: center;padding-bottom: 20px;">
            <h5 style="font-size:18px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">Congrats ! You are Successfully subscribed</h5>
            <p style="font-size:12px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">Now You Got All Updates Regarding Our site </p>
            <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">


            </ul>
            </div>
            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
            <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
            </div>
            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
            <div class="col-sm-6">
            <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
            <div style="float: left;width: 15%;margin-right: 20px;">
            <img style="width: 100%;" src='.$phone_img.'>
            </div>
            <div style="float: left;width: 70%;">
            <a href="tel:1800â€“419â€“7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
            <a href="tel:1800â€“419â€“7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800â€“419â€“7713</a>
            </div>
            </div>
            </div>
            <div class="col-sm-6">
            <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
            <div style="float: left;width:15%;margin-right:20px;">
            <img style="width:100%" src='.$mail_icon.'>
            </div>
            <div style="float: left;width: 70%;">
            <a style="display: block;font-size:10px;" href="mailto:contato@navizinhanca.com" target="_top">Email us at</a>
            <a  style="display: block;font-size:12px;" href="mailto:contato@navizinhanca.com" target="_top">contato@navizinhanca.com</a>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
            <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
            <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
            </div>
            </div>
            </div>
            </body>
            </html>';
            // echo $message;die;
            if(mail($from_email, $subject, $message, $headers,'-faaaa@abc.com')){
              $check_exist = DB::table('subscribers')->where('subscriber_email',$from_email)->first();
              if(empty($check_exist)){
                $subscribe = DB::table('subscribers')->insert(['subscriber_email'=>$from_email,'status'=>1]);
                if($subscribe){
                  return redirect('/home');
                }
              }
              else{
                echo "You are already subscribed";
              }
            }
            else{
              echo "something went wrong  EMail Cannot sent";
            }

  }

  public function chklginprofilecomplete(Request $request)
  {
     $user_id = trim($request->userid);

     if($user_id!='') {

        $user = DB::table('users')->where('id', '=', $user_id)->first();
        $provider_price_list_count = DB::table('provider_price_list')->where('user_id', '=', $user_id)->count();

        if($user->user_role == 'Provider') {

            if($user->cpf_id =='' || $user->phone =='' || $user->cat_id =='' || $user->sub_id =='' || $user->zipcode =='' || $user->address =='' || $user->city =='' || $user->neighborhood =='' || $user->state =='' || $user->country =='' || $provider_price_list_count == 0 ) {

                $pStatus = 'Profilenotcompleted';

            } else {
                $pStatus = 'Profilecompleted';
            }

        } else {

            if($user->cpf_id =='' || $user->phone =='' || $user->zipcode =='' || $user->address =='' || $user->city =='' || $user->neighborhood =='' || $user->state =='' || $user->country =='') {

                $pStatus = 'Profilenotcompleted';

            } else {
                $pStatus = 'Profilecompleted';
            }
            
        }

        echo json_encode(array('pStatus'=>$pStatus,'urole'=>$user->user_role,'ucpf_id'=>$user->cpf_id,'uphone'=>$user->phone,'ucat_id'=>$user->cat_id,'usub_id'=>$user->sub_id,'uzipcode'=>$user->zipcode,'uaddress'=>$user->address,'ucity'=>$user->city,'uneighborhood'=>$user->neighborhood,'ustate'=>$user->state,'ucountry'=>$user->country,'uprice'=>$provider_price_list_count));

     } else {
        echo json_encode(array('pStatus'=>'','urole'=>''));
     }

  }

}
