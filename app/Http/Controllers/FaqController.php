<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Contact;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Spavailability;
use App\Models\Notification;
use IntlDateFormatter;
use DateTime;
// use App\Models\Spavailability;

class FaqController extends Controller
{
	public function index()
	{
		try{
			$menu_data = DB::table('menu_meta_details')
					->where('menu_slug','=','faq')
					->where('is_active', '1')
					->where('is_deleted', '0')
					->first();

			return view('pages.faq', compact('menu_data'));
		}
		catch(\Illuminate\Database\QueryException $e){
			$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

			$errorlog                   = new Errorlogs;
			$errorlog->error_message    = $e->getMessage();
			$errorlog->line_number      = $e->getLine();
			$errorlog->file_name        = $e->getFile();
			$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
			$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
			if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
			$errorlog->ip_address       = "";
			$errorlog->save();
			return view('errors.custom',compact('customerror')); 
		}catch(\Exception $e){ 
			$customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

			$errorlog                   = new Errorlogs;
			$errorlog->error_message    = $e->getMessage();
			$errorlog->line_number      = $e->getLine();
			$errorlog->file_name        = $e->getFile();
			$errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
			$errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
			if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
			$errorlog->ip_address       = "";
			$errorlog->save();
			return view('errors.custom',compact('customerror')); 
		}
	}
}
