<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\User;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Notification;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }
    public function show(Request $request, $id) {
        try{

         $user = User::where('id', $id)
         ->update(['is_active' => 1]);
         if($user){
            $latest_user_id = $id;
            $notificaton_type  = 'register';
            $type_id = $id;
            $check_notify = Notification::where('user_id',$id)->where('notificaton_type','register')->where('is_active',1)->count();
            if($check_notify > 0){
              if(Auth::check()){
            	 return redirect()->intended('/logout')->with('message','Your Account Has Been Activated Successfully');	
              } else {
               return redirect()->intended('/login')->with('message','Your Account Has Been Activated Successfully');
              }
            } else {
            	$notifications = Notification::insert(['user_id'=>$id,'notificaton_type'=>$notificaton_type,'type_id'=>$type_id]);
                // if($notifications){
                if(Auth::check()){
                  return redirect()->intended('/logout')->with('message','Your Account Has Been Activated Successfully');
                } else {
                  return redirect()->intended('/login')->with('message','Your Account Has Been Activated Successfully');
                }
                // } 
                
            }
              if(Auth::check()){
                return redirect()->intended('/logout')->with('message','Your Account Has Been Activated Successfully');
              } else {
                return redirect()->intended('/login')->with('message','Your Account Has Been Activated Successfully');
              }

            } else {
              if(Auth::check()){
                return redirect()->intended('/logout')->with('message','sorry Account Is not activated');
              } else {
                return redirect()->intended('/login')->with('message','sorry Account Is not activated');
              }
            }
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
  }
    public function update(Request $request) {
       $id = $request->id;
    }
}
