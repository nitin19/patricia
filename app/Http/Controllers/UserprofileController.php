<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Feedback;
use App\Errorlogs;
use App\User;
use App\Models\Spavailability;
use App\Models\Notification;
use App\Models\BlackUser;
use IntlDateFormatter;
use DateTime;


class UserprofileController extends Controller
{

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function userprofiledata(Request $request, $id) {
    try{
       // $ip = $_SERVER['REMOTE_ADDR'];
       /*$userlatlng = DB::table('users')->where('id','=',$id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $getips= $this->getIpLatLong($ip);
            $sys_latitude=$getips['lat'];
            $sys_longitude=$getips['lng'];
          }*/
      /*$getips= $this->getIpLatLong($ip);
      $sys_latitude=$getips['lat'];
      $sys_longitude=$getips['lng'];*/
      
      /*$user_info = User::select('users.*','category.*','main_category.name as main_cat_name',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
      ->where('users.id', $id)
      ->join('main_category','main_category.id','=','users.cat_id')
      ->join('category','category.cat_id','=','users.sub_id')
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      ->first();*/

      $ip = $request->ip(); 
      $unit = "K";
      
      if(!empty(Auth::user())) {
          $userlatlng = DB::table('users')->where('id','=',Auth::user()->id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $getips= $this->getIpLatLong($ip);
            $sys_latitude=$getips['lat'];
            $sys_longitude=$getips['lng'];
          }
        } else {
          $getips= $this->getIpLatLong($ip);
          $sys_latitude=$getips['lat'];
          $sys_longitude=$getips['lng'];
        }

      $user_info = User::select('users.*','category.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
      ->where('users.id', $id)
      ->join('main_category','main_category.id','=','users.cat_id')
      ->join('category','category.cat_id','=','users.sub_id')
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      ->first();

      $get_admin_info  = User::where('user_role','admin')->where('users.is_active', '1')
      ->where('users.is_deleted', '0')->first();

      /*$similar_category_users = User::select('users.*','price_queries.queries as price_des','category.cat_name as subcat_name','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))   
      ->leftjoin('provider_price_list','provider_price_list.user_id','=','users.id')
      ->leftjoin('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
      ->leftjoin('category', 'category.cat_id', '=', 'users.sub_id')
      ->where('users.cat_id',$user_info->main_cat_id)
      ->where('users.is_active', '1')
      ->where('price_queries.priority', '1')
      ->where('users.is_deleted', '0')
      ->where('users.id','!=', $get_admin_info->id)
      ->where('users.id','!=',$id)
      ->where('user_role', 'Provider')->limit('5')->get();*/

      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->having('distances', '<', '25');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.sub_id',$user_info->subcat_id);
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit('5');
      $similar_subcategory_users = $query->get();

      if( count($similar_subcategory_users) < 5) {
        $similar_main_category_users = $this->getSimilarMaincatUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users);
        if( count($similar_main_category_users) > 0 ) {
          $merged = $similar_main_category_users->merge($similar_subcategory_users);
          $merged_category_users = $merged->all();
          if( count($merged_category_users) < 5 ) {
          $merged_cat_withnearbydist_users = $this->getMergedCatWithNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$merged_category_users);
            if( count($merged_cat_withnearbydist_users) > 0 ) {
              $mergedAll = $merged_cat_withnearbydist_users->merge($merged_category_users);
              $similar_category_users = $mergedAll->all();
            } else {
            $randomUsers = $this->getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info);
            $similar_category_users = $randomUsers;
            }
          } else {
             $similar_category_users = $merged_category_users;
          }
        } else {
          $subcat_withnearbydist_users = $this->getNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users);
          if( count($subcat_withnearbydist_users) > 0 ) {
              $mergedAll = $subcat_withnearbydist_users->merge($similar_subcategory_users);
              $similar_category_users = $mergedAll->all();
            } else {
            $randomUsers = $this->getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info);
            $similar_category_users = $randomUsers;
            }
        }
      } else {
        $similar_category_users = $similar_subcategory_users;
      }

      $feedbacks = Feedback::select(DB::raw('AVG(rating) AS avg_rating ,provider_id,user_id,message,users.*'))->join('users','users.id','=','feedback.user_id')->where('feedback.provider_id',$id)->where('feedback.role','taker')->where('approve_status',1)->paginate(5);

//DB::enableQueryLog();

      $userreview = DB::table('feedback')
            ->join('users', 'users.id', '=', 'feedback.user_id')
            ->select('users.*', 'feedback.*')
            ->where('feedback.provider_id',$id)
            ->where('approve_status',1)
            ->where('role','taker')
            //->get()->toArray();
            ->paginate(5);
       //    dd(DB::getQueryLog());


      $reviewrating = Feedback::select(DB::raw('AVG(rating) AS avg_rating'))->where('feedback.provider_id',$id)->where('approve_status',1)->first()->toArray();

      /*$booking_status = '';
      $check_existing_booking = Booking::where('profile_id',$id)->where('user_id',Auth::user()->id)->first();
      if(empty($check_existing_booking)){
        $check_existing_booking = new Booking();
      }
      else{
        $booking_status =   $check_existing_booking->booking_verfied;
      }*/
      /*$check_feedback = Feedback::where('user_id',$id)
      ->where('provider_id',$user_info->id)
      ->where('booking_id',$check_existing_booking->booking_id)
      ->count();*/
      $review_count = DB::table('review')
      ->where('review_reciever_id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->count();
      $user_gallery = DB::table('user_gallery')
      ->where('user_id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->get();
      $user_lat = $user_info->latitude;
      $user_lon = $user_info->longitude;
      $query = DB::table('users')
      ->where('is_active', '1')
      ->where('is_deleted', '0');
     // $distance =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
      $category = DB::table('users')
      ->select('category')
      ->where('id', '=', $id)
      ->value('id');
      $similar_category = DB::table('users')
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->orWhere('category', 'like', '%' . $category . '%')
      ->limit('5')
      ->get();
      $get_user_availablity =  DB::table('spavailability')
      ->join('users', 'users.id', '=', 'spavailability.user_id')
      ->select('spavailability.*', 'users.name')
      ->where('users.id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      //->where('user_role', 'Provider')
      ->first();
      $user_documents = DB::table('user_documents')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('document_status', '1')
                      ->where('is_deleted', '0')
                      ->get();
      $user_id = $user_info->id;
      //$current_userid = Auth::id();
      $wish_list_exist = DB::table('wish_list')
      ->where('wish_profile_id', $user_id)
      ->where('status', '=', 1)
      ->count();
      $complete_booking=DB::table('booking')
      ->where('profile_id',$user_id)
      ->where('booking_verified','1')
      ->count(); 
      $repeat_clients = Booking::select(DB::raw('COUNT(*)'))
      ->where('profile_id',$user_id)  
      ->groupby('profile_id','user_id')
      ->havingRaw('COUNT(*) > 1')
      ->get()
      ->count();
      $average_rating = Feedback::select(DB::raw('provider_id,AVG(rating) AS avg_rating ,rating,users.id as u_id,users.name as user_name'))->join('users','users.id','=','feedback.provider_id')->groupBy('feedback.provider_id')->where('users.is_active', '1')
      ->where('users.is_deleted', '0')->get();
      $provider_price_lists = DB::table('provider_price_list')
      ->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
      ->where('provider_price_list.user_id', $id)
      ->select('price_queries.*', 'provider_price_list.id', 'provider_price_list.amount', 'provider_price_list.query_id')
      ->orderBy('price_queries.id')
      ->get()->toArray();
      $get_feedbacks = [];
      if(count($similar_category_users) > 0){
        foreach($similar_category_users as $cat_info)  {
          $get_user_feedbacks = Feedback::where('provider_id',$cat_info->id)->where('approve_status',1)->where('role','taker')->count();
          $get_feedbacks[$cat_info->id]= $get_user_feedbacks;
          $user_lat = $cat_info->latitude;
          $user_lon = $cat_info->longitude;
        //  $distance1  =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
        //  $distance_array[$cat_info->name] = $distance1;
        }
      }
      //$checkexist_user_blklist = BlackUser::where('from_id','=',Auth::user()->id)->where('to_id','=',$id)->first();  
      $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','profile')->where('is_active', '1')->where('is_deleted', '0')->first();
      return view('profile.userprofile', compact('user_info','distance','category', 'similar_category','complete_booking','repeat_clients','wish_list_exist', 'user_gallery', 'review_count','get_user_availablity','get_categories','feedbacks','similar_category_users','id','average_rating', 'provider_price_lists','get_feedbacks','check_feedback', 'userreview', 'reviewrating','user_documents','menu_meta_details'));
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }catch(\Exception $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }
  }

  function getSimilarMaincatUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users) {
      $rowCount = count($similar_subcategory_users);
      $limit = (5 - $rowCount);
      $similar_subcat_user_Arr = array();
      if($rowCount > 0) {
        foreach($similar_subcategory_users as $similar_subcat_user) {
          $similar_subcat_user_Arr[] = $similar_subcat_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->having('distances', '<', '25000');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.cat_id',$user_info->main_cat_id);
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($similar_subcat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$similar_subcat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $similar_main_category_users = $query->get();
    //  dd(DB::getQueryLog());
      return $similar_main_category_users;
  }
  
  function getMergedCatWithNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$merged_category_users) {

      $rowCount = count($merged_category_users);
      $limit = (5 - $rowCount);
      $merged_cat_user_Arr = array();
      if($rowCount > 0) {
        foreach($merged_category_users as $merged_category_user) {
          $merged_cat_user_Arr[] = $merged_category_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
    //  $query->having('distances', '<', '25000');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($merged_cat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$merged_cat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $merged_cat_withnearbydist_users = $query->get();
    //  dd(DB::getQueryLog());
      return $merged_cat_withnearbydist_users;
  }

  function getNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users) {

      $rowCount = count($similar_subcategory_users);
      $limit = (5 - $rowCount);
      $similar_subcat_user_Arr = array();
      if($rowCount > 0) {
        foreach($similar_subcategory_users as $similar_subcat_user) {
          $similar_subcat_user_Arr[] = $similar_subcat_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($similar_subcat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$similar_subcat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $subcat_withnearbydist_users = $query->get();
    //  dd(DB::getQueryLog());
      return $subcat_withnearbydist_users;
  }

  function getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info) {
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit(5);
      $randomUsers = $query->get();
    //  dd(DB::getQueryLog());
      return $randomUsers;
  }

  // get lat and long with ip address

   function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    return $data;
   }

  /*function getIpLatLong($ipaddress){
    $data = array();
    $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=08f1bc91f71c45880fefc03eb7f5e695");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];
    return $data;
  }*/

  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
    return 0;
  }
  else {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }
 }
  
}