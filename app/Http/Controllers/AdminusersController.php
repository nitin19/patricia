<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use Session;
use DB;
use Hash;
use Image;
use App\Models\Notification;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminusersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
      try{

        /* Count */
        $users_count = DB::table('users')
                  ->where('is_active', '1')
                  ->where('is_deleted', '0')
                  ->where('user_role', '!=', 'admin')
                  ->count();
           /* Count */
           /* Listing */ 
         $users = DB::table('users')
                  ->where('is_active', '1')
                  ->where('is_deleted', '0')
                  ->where('user_role', '!=', 'admin')
                  ->where('phone','!=','')
                  ->where('address','!=','')
                  ->orderBy('id', 'DESC')
                 ->get();
          // $provider = DB::table('users')
          //             ->where('is_active','1')
          //             ->where('is_deleted','0')
          //             ->where('user_role','Provider')
          //             ->where('cat_id','!=','')
          //             ->where('sub_id','!=','');
          /* Listing */
         return view('admin.users.index', compact('users_count', 'users'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
       
    }
      public function create(Request $request, $id){
      try {
        
      } catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function provider(Request $request){
      try {
           $cat_id = \Request::segment(2);
           $sub_id = \Request::segment(3);
          
          /*$query = DB::table('users')->join('category','users.sub_id','=','category.cat_id');
                 $query->join('main_category','users.cat_id','=','main_category.id');
                 $query->where('users.is_active', '1');
                 $query->where('users.is_deleted', '0');
                 $query->where('users.user_role','=','Provider');
                 if(!empty($cat_id) && !empty($sub_id)){
                  $query->where('users.cat_id', $cat_id);
                  $query->where('users.sub_id', $sub_id);
                 }
                 $query->select('users.*','category.cat_name','main_category.name as mainCatName');

              $query->orderBy('users.id', 'DESC');
              $provider_users = $query->get();*/

          $provider_users = DB::table('users')
                          ->where('user_role', '=', 'Provider')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->orderBy('id', 'DESC')
                          ->get();
                         
          return view('admin.provider.index', compact('provider_users'));
        
      } 
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

    public function getIncompleteprofileproviders(Request $request){
          
          $status = $request->p_status;
         if($status == "Incompleted") {
             $inc_provider_users = DB::table('users')
                          ->where('user_role', '=', 'Provider')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          /*->where('name', '=', '')
                          ->orWhereNull('name') 
                          ->where('phone', '=', '')
                          ->orWhereNull('phone')
                          ->where('city', '=', '')
                          ->orWhereNull('city')
                          ->where('rg', '=', '')
                          ->orWhereNull('rg')
                          ->where('state', '=', '')
                          ->orWhereNull('state')
                          ->where('country', '=', '')
                          ->orWhereNull('country')
                          ->where('profile_image', '=', '')
                          ->orWhereNull('profile_image')
                          ->where('zipcode', '=', '')
                          ->orWhereNull('zipcode')*/
                          ->orderBy('id', 'DESC')
                          ->get();
          $provider_users = array(); 
          foreach($inc_provider_users as $p_user)               
          if($p_user->name == "" || $p_user->phone == "" || $p_user->city == "" || $p_user->state == "" || $p_user->country == "" || $p_user->profile_image == "" || $p_user->zipcode == "" || $p_user->rg == "") {
            array_push($provider_users,$p_user);
          }
           
           // return view('admin.provider.ajaxresponse', compact('provider_users'));
          return $provider_users;



          } else if($status == "Completed"){
            $c_provider_users = DB::table('users')
                          ->where('user_role', '=', 'Provider')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->whereNotNull('name')
                          ->whereNotNull('profile_image')
                          ->whereNotNull('phone')
                          ->whereNotNull('city')
                          ->whereNotNull('state')
                          ->whereNotNull('rg')
                          ->whereNotNull('country')
                          ->whereNotNull('zipcode')
                          ->orderBy('id', 'DESC')
                          ->get();
          $provider_users = array(); 
          foreach($c_provider_users as $p_user)               
          if($p_user->name != "" || $p_user->phone != "" || $p_user->city != "" || $p_user->state != "" || $p_user->country != "" || $p_user->profile_image != "" || $p_user->zipcode != "" || $p_user->rg != "") {
            array_push($provider_users,$p_user);
          }

         return $provider_users;

          } else {
            return redirect('/admin-provider');
          }
         

            //print_r($provider_users);  
                 
          //return view('admin.provider.incomplete', compact('provider_users'));
        
    
     
    }

    public function sendcompleteprofilemsg(Request $request){
      //$recipients = ltrim($request->recipients,",");
      //$recipients = "dev1.bdpl@gmail.com,dev1.bdpl@gmail.com";

      $inc_provider_users = DB::table('users')
                          ->where('user_role', '=', 'Provider')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->orderBy('id', 'DESC')
                          ->get();
          $reciepientarr = array(); 
          foreach($inc_provider_users as $p_user)               
          if($p_user->name == "" || $p_user->phone == "" || $p_user->city == "" || $p_user->state == "" || $p_user->country == "" || $p_user->profile_image == "" || $p_user->zipcode == "" || $p_user->rg == "") {
            array_push($reciepientarr,$p_user->email);
          }
         if($reciepientarr!=''){
           //$reciepientarr = explode(',', $recipients);
          foreach($reciepientarr as $recipient){
          $provider_users = DB::table('users')->where('email','=',$recipient);
          $empty_fields = array();
          if(empty($provider_users->name)){
            array_push($empty_fields,'name');
          }
          if(empty($provider_users->phone)){
            array_push($empty_fields,'phone');
          }
          if(empty($provider_users->profile_image)){
            array_push($empty_fields,'Profile Image');
          }

          if(empty($provider_users->city)){
            array_push($empty_fields,'city');
          }

          if(empty($provider_users->rg)){
            array_push($empty_fields,'rg');
          }

          if(empty($provider_users->state)){
            array_push($empty_fields,'state');
          }

          if(empty($provider_users->country)){
            array_push($empty_fields,'country');
          }

          if(empty($provider_users->zipcode)){
            array_push($empty_fields,'Zipcode');
          }

          $message = '<p>Hi</p>
          <p>Please complete your profile. Please fill the following details.</p>';
          foreach($empty_fields as $emp_field) {
            $message .= '<input type="checkbox" checked="checked" disabled="disabled">'.$emp_field.'';
          }
          $message .= '<p>Click Here to Complete your profile.</p>';
          $message .= '<a href="https://www.navizinhanca.com/serviceprovider/completeprofile">Profile Link</a>';
          $message .= '<p>Thanks.</p>';

          $subject = "Complete Your Profile";
          

          $admindetail = DB::table('business_settings')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->first();

          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";

          mail($recipient,$subject,$message,$headers);
        }
          return redirect('/admin-provider')
                 ->with('success','Message send successfully'); 
      } else {
          return redirect('/admin-provider')
                ->with('error','Message not send'); 
      }




      
      /*if($recipients!=''){
        $reciepientarr = explode(',', $recipients);
        foreach($reciepientarr as $recipient){
          $provider_users = DB::table('users')->where('email','=',$recipient);
          $empty_fields = array();
          if(empty($provider_users->name)){
            array_push($empty_fields,'name');
          }
          if(empty($provider_users->phone)){
            array_push($empty_fields,'phone');
          }
          if(empty($provider_users->profile_image)){
            array_push($empty_fields,'Profile Image');
          }

          if(empty($provider_users->city)){
            array_push($empty_fields,'city');
          }

          if(empty($provider_users->rg)){
            array_push($empty_fields,'rg');
          }

          if(empty($provider_users->state)){
            array_push($empty_fields,'state');
          }

          if(empty($provider_users->country)){
            array_push($empty_fields,'country');
          }

          if(empty($provider_users->zipcode)){
            array_push($empty_fields,'Zipcode');
          }

          $message = '<p>Hi '.$provider_users->name.'</p>
          <p>Please complete your profile. Please fill the following details.</p>';
          foreach($empty_fields as $emp_field) {
            $message .= '<input type="checkbox" checked="checked" disabled="disabled">'.$emp_field.'';
          }
          $message .= '<p>Click Here to Complete yout profile.</p>';
          $message .= '<a href="">Profile Link</a>';
          $message .= '<p>Thanks.</p>';

          $subject = "Complete Your Profile";
          

          $admindetail = DB::table('business_settings')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->first();

          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";

          mail($recipient,$subject,$message,$headers);
        }
          return redirect('/admin-provider/incomplete-profile')
                 ->with('success','Message send successfully'); 
      } else {
          return redirect('/admin-provider/incomplete-profile')
                ->with('error','Message not send'); 
      }*/


      /*$subject = "Complete Your Profile";
      //$message = "Hi,Please complete your profile. Please fill the following details";
        $message = '<p>Hi, User</p>
          <p>Please complete your profile. Please fill the following details.</p>';
         
            $message .= '<input type="checkbox" checked="checked" disabled="disabled">email';
        
          $message .= '<p>Click Here to Complete your profile.</p>';
          $message .= '<a href="https://www.navizinhanca.com">Profile Link</a>';
          $message .= '<p>Thanks.</p>';

      $admindetail = DB::table('business_settings')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->first();

      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";
      if($recipients!=''){
        $reciepientarr = explode(',', $recipients);
        foreach($reciepientarr as $recipient){

            mail($recipient,$subject,$message,$headers);
        }
          return redirect('/admin-provider/incomplete-profile')
                 ->with('success','Message send successfully'); 
      } else {
          return redirect('/admin-provider/incomplete-profile')
                ->with('error','Message not send'); 
      }
*/
    }


    public function taker(Request $request){
      try {

          $taker_users = DB::table('users')
                          ->where('user_role', '=', 'Taker')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->orderBy('id', 'DESC')
                          ->get();            
          return view('admin.taker.index', compact('taker_users'));
        
      } 
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    
     public function getIncompleteprofiletakers(Request $request){
          
        $status = $request->p_status;
         if($status == "Incompleted") {
             $inc_provider_users = DB::table('users')
                          ->where('user_role', '=', 'Taker')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->orderBy('id', 'DESC')
                          ->get();
          $provider_users = array(); 
          foreach($inc_provider_users as $p_user)               
          if($p_user->name == "" || $p_user->phone == "" || $p_user->city == "" || $p_user->state == "" || $p_user->country == "" || $p_user->profile_image == "" || $p_user->zipcode == "" || $p_user->rg == "") {
            array_push($provider_users,$p_user);
          }
           
           // return view('admin.provider.ajaxresponse', compact('provider_users'));
          return $provider_users;



          } else if($status == "Completed"){
            $c_provider_users = DB::table('users')
                          ->where('user_role', '=', 'Taker')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->whereNotNull('name')
                          ->whereNotNull('profile_image')
                          ->whereNotNull('phone')
                          ->whereNotNull('city')
                          ->whereNotNull('state')
                          ->whereNotNull('rg')
                          ->whereNotNull('country')
                          ->whereNotNull('zipcode')
                          ->orderBy('id', 'DESC')
                          ->get();
          $provider_users = array(); 
          foreach($c_provider_users as $p_user)               
          if($p_user->name != "" || $p_user->phone != "" || $p_user->city != "" || $p_user->state != "" || $p_user->country != "" || $p_user->profile_image != "" || $p_user->zipcode != "" || $p_user->rg != "") {
            array_push($provider_users,$p_user);
          }

         return $provider_users;

          } else {
            return redirect('/admin-taker');
          }
        
    
     
    }

    public function sendcompleteprofiletakermsg(Request $request){
          //$recipients = ltrim($request->recipients,",");
          //$recipients = "dev1.bdpl@gmail.com,dev1.bdpl@gmail.com";

          $inc_provider_users = DB::table('users')
                              ->where('user_role', '=', 'Taker')
                              ->where('is_active', '=', '1')
                              ->where('is_deleted', '=', '0')
                              ->orderBy('id', 'DESC')
                              ->get();
              $reciepientarr = array(); 
              foreach($inc_provider_users as $p_user)               
              if($p_user->name == "" || $p_user->phone == "" || $p_user->city == "" || $p_user->state == "" || $p_user->country == "" || $p_user->profile_image == "" || $p_user->zipcode == "" || $p_user->rg == "") {
                array_push($reciepientarr,$p_user->email);
              }
             if($reciepientarr!=''){
               //$reciepientarr = explode(',', $recipients);
              foreach($reciepientarr as $recipient){
              $provider_users = DB::table('users')->where('email','=',$recipient);
              $empty_fields = array();
              if(empty($provider_users->name)){
                array_push($empty_fields,'name');
              }
              if(empty($provider_users->phone)){
                array_push($empty_fields,'phone');
              }
              if(empty($provider_users->profile_image)){
                array_push($empty_fields,'Profile Image');
              }

              if(empty($provider_users->city)){
                array_push($empty_fields,'city');
              }

              if(empty($provider_users->rg)){
                array_push($empty_fields,'rg');
              }

              if(empty($provider_users->state)){
                array_push($empty_fields,'state');
              }

              if(empty($provider_users->country)){
                array_push($empty_fields,'country');
              }

              if(empty($provider_users->zipcode)){
                array_push($empty_fields,'Zipcode');
              }

              $message = '<p>Hi</p>
              <p>Please complete your profile. Please fill the following details.</p>';
              foreach($empty_fields as $emp_field) {
                $message .= '<input type="checkbox" checked="checked" disabled="disabled">'.$emp_field.'';
              }
              $message .= '<p>Click Here to Complete your profile.</p>';
              $message .= '<a href="https://www.navizinhanca.com/serviceprovider/completeprofile">Profile Link</a>';
              $message .= '<p>Thanks.</p>';

              $subject = "Complete Your Profile";
              

              $admindetail = DB::table('business_settings')
                         ->where('is_active', '1')
                         ->where('is_deleted', '0')
                         ->first();

              $headers = "MIME-Version: 1.0" . "\r\n";
              $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
              $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";

              mail($recipient,$subject,$message,$headers);
            }
              return redirect('/admin-taker')
                     ->with('success','Message send successfully'); 
          } else {
              return redirect('/admin-taker')
                    ->with('error','Message not send'); 
          }


          /*$subject = "Complete Your Profile";
          //$message = "Hi,Please complete your profile. Please fill the following details";
            $message = '<p>Hi, User</p>
              <p>Please complete your profile. Please fill the following details.</p>';
             
                $message .= '<input type="checkbox" checked="checked" disabled="disabled">email';
            
              $message .= '<p>Click Here to Complete your profile.</p>';
              $message .= '<a href="https://www.navizinhanca.com">Profile Link</a>';
              $message .= '<p>Thanks.</p>';

          $admindetail = DB::table('business_settings')
                         ->where('is_active', '1')
                         ->where('is_deleted', '0')
                         ->first();

          $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
          $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";
          if($recipients!=''){
            $reciepientarr = explode(',', $recipients);
            foreach($reciepientarr as $recipient){

                mail($recipient,$subject,$message,$headers);
            }
              return redirect('/admin-provider/incomplete-profile')
                     ->with('success','Message send successfully'); 
          } else {
              return redirect('/admin-provider/incomplete-profile')
                    ->with('error','Message not send'); 
          }
    */
    }


    public function store(Request $request){
    }
    public function edit($id){
      try{
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('user_role', '!=', 'admin')
              ->first();            
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        $main_category = DB::table('main_category')
                        ->join('category','main_category.id','=','category.main_cat_id')
                        ->where('main_category.is_deleted', '0')
                        ->pluck('name','id');
        $subid=$user_info->sub_id;
        $category = DB::table('category')
                     ->where('cat_id',$subid)
                     ->first();              
        return view('admin.users.edit', compact('countries','user_info', 'user_image', 'category','main_category'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
               
    }
    public function update(Request $request){
      try{
         //print_r($request->all());die;
         $phone = $request->phone;
         $user_role = $request->user_role;
         $country = $request->country;
         $zipcode = $request->zipcode;
         $user_id = $request->user_id;
         $address_str = $request->address;
         $address = urlencode($address_str);
         $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=$address";
         $filestring = file_get_contents($url);
         $filearray = explode("\n", $filestring);
         $data = json_decode($filearray[0]);
         foreach($data as $k=>$v) {
            $view = $v->View;
            $view_count = count($view);
            if($view_count>0){
              foreach($view as $result_k=>$result_v) {
                $Result = $result_v->Result;
                $lat =  $Result[0]->Location->DisplayPosition->Latitude;
                $long =  $Result[0]->Location->DisplayPosition->Longitude;
                $PostalCode =  $Result[0]->Location->Address->PostalCode;
                $countryname =  $Result[0]->Location->Address->AdditionalData[0]->value;
                $cityname =  $Result[0]->Location->Address->AdditionalData[2]->value;
                if($lat!='' && $long!=''){
                  $user_updated = DB::table('users')
                      ->where('id', $user_id)
                      ->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'country' => $countryname, 'city' => $cityname]);
                }
              }
            } else {
              if($user_role == 'Provider'){
                return redirect('/admin-provider')->with('error', 'Enter correct Address!');
              } else {
                return redirect('/admin-taker')->with('error', 'Enter correct Address!');
              }
            }
          }
          if($request->hasfile('user_images')) {
            foreach($request->file('user_images') as $images ) {
                $names = time() . '.' . $images->getClientOriginalName();
                $image_resizes = Image::make($images->getRealPath());
                $image_resizes->resize(480, 400);
                $image_resizes->save(public_path('images/userimages/' .$names));
                DB::table('user_gallery')->insert(
                    ['image_name' => $names, 'user_id' => $user_id]
                );
            }
          }
          if($request->hasfile('profileimage')){
                $image = $request->file('profileimage');
                $name = time() . '.' . $image->getClientOriginalName(); 
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(480, 400);
                $image_resize->save(public_path('images/profileimage/' .$name));
                $user_updated = DB::table('users')
                      ->where('id', $user_id)
                      ->update(['profile_image' => $name]);
          }
          $update_service = DB::table('users')
                          ->where('id',$user_id)
                          ->update(['cat_id' => $request->main_categroy_id,'sub_id'=>$request->sub_categroy_id]);
          
           
        $user_updated = DB::table('users')
                ->where('id', $user_id)
                ->update(['phone' => $phone, 'address' => $address_str]);
          
              if($user_role == 'Provider'){
                return redirect('/admin-provider')->with('success', 'Profile updated!');
              } else {
                return redirect('/admin-taker')->with('success', 'Profile updated!');
              }
                   
           
       // else {
       //   if($user_role == 'Provider'){
       //      return redirect('/admin-provider')->with('error', ' No updation found!');
       //    } else {
       //      return redirect('/admin-taker')->with('error', 'No updation found!');
       //    }
       // }  
    }
    catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
   }

     public function show(Request $request, $id){
      try {
       //print_r($id);
        $user_info = DB::table('users')
                    ->leftjoin('main_category','users.cat_id','=','main_category.id')
                    ->leftjoin('category','users.sub_id','=','category.cat_id')
                    ->select('users.*','category.cat_name','main_category.name as mainCatName')
                    ->where('users.id', $id)
                    ->where('user_role', '!=', 'admin')
                    ->first();

        $user_gallery = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        $user_documents = DB::table('user_documents')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
                   
        // $category_info = DB::table('users')
        //                 ->join('main_category','users.cat_id','=','main_category.id')
        //                 ->join('category','users.sub_id','=','category.cat_id')
        //                 ->select('users.*','category.cat_name','main_category.name as mainCatName')
        //                 ->where('users.user_role','Provider');
        return view('admin.users.show', compact('user_info', 'user_gallery','user_documents'));
        
      } catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
     }
     /* approved address by admin */
    public function approved_address(Request $request){
      try{
          $user_id = $request->user_id;
          $address_status = $request->approve_status;
          $user_info = DB::table('users')
                      ->where('id',$user_id)->first();  
       
          $user_updated = DB::table('users')
            ->where('id', $user_id)
            ->update(['address_verified' =>$request->approve_status]);
          if($user_updated!=''){
            $notificaton_type  = 'address_approved';
            if($user_info->user_role =="Provider"){
              $notify_role ="Taker";
            } else{
              $notify_role ="Provider";
            }

            $notify = Notification::insert(['user_id' => $user_id, 'notificaton_type' => $notificaton_type,'type_id' => $user_id,'notification_role' => $notify_role]);
            
                if($user_info->user_role == 'Provider'){
                  return redirect('/admin-provider')->with('success', 'Address Status updated!');
                }
                else
                {
                  return redirect('/admin-taker')->with('success', 'Address Status updated!');
                }
       
          } else {
            if($user_info->user_role == 'Provider'){
               return redirect('/admin-provider')->with('error', 'Address Status Not updated!');
             }
             else
             {
                return redirect('/admin-taker')->with('error', 'Address Status Not updated!');
             } 
          }    
      } 
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    /*public function destroy(Request $request)
    {
        $id=$request->id;
        $remove_user =  DB::table('users')
            ->where('id', $id)
            ->update(['is_deleted' => 1,'is_active' => 0]);
        if($remove_user!='')
        {
        echo "User deleted successfully.";
        }
    }*/
    
    public function destroy(Request $request)
    {
        $id=$request->id;
        $user = User::find($id);
        $email1 = $user->email.'1';
        $remove_user =  DB::table('users')
            ->where('id', $id)
            ->update(['email' => $email1,'is_deleted' => 1,'is_active' => 0]);
        if($remove_user!='')
        {
        echo "User deleted successfully.";
        }

        /*$user = User::find($id);    
        $user->delete();
        $user = User::findOrFail($id); 
        $deleted = $user->delete();
        if($deleted) {
           echo "User deleted successfully.";
        } else {
           echo "Sorry fail to delete this user. Please try again."; 
        }*/
    }

    public function check_subcat(Request $request){
        $main_cat = DB::table('category')
                    ->where('main_cat_id',$request->main_cat_id)
                    ->pluck('cat_name','cat_id');
        // print_r($main_cat);die;
        // echo "<pre>";
        if(!empty($main_cat)){
            $response['success'] = "true";
            $response['message'] = "success";
            $response= $main_cat;
        }
        else{
            $response['success'] = "false";
            $response['message'] = "failure";

        }
        // $response['success'] = "true";
        // $response['message'] = "success";
        echo json_encode($response);
    }
    public function approved_document($user_id,$doc_id)
    {

      $update = DB::table('user_documents')
            ->where('id',$doc_id)
            ->update(['document_status' => '1']);
      $user_info = DB::table('users')
                      ->where('id',$user_id)->first();

       if($update) {
        $notificaton_type  = 'document';
        if($user_info->user_role =="Provider"){
              $notify_role ="Taker";
            } else{
              $notify_role ="Provider";
            }
          $notify = Notification::insert(['user_id' => $user_id, 'notificaton_type' => $notificaton_type,'type_id' => $doc_id,'notification_role' => $notify_role]);
         //echo "yes";die;
         return redirect('/admin-users/show/'.$user_id)->with('success', 'Document approved successfully!');
       } else {
         //echo "no";die;
        return redirect('/admin-users/show/'.$user_id)->with('danger', 'Something wrong!');
      }
    }
    public function disapproved_document($user_id,$doc_id)
    {
      
      $update = DB::table('user_documents')
            ->where('id', $doc_id)
            ->update(['document_status' => '2']);
      $user_info = DB::table('users')
                      ->where('id',$user_id)->first();
      if($update) {
         $notificaton_type  = 'document';
          $notify = Notification::insert(['user_id' => $user_id, 'notificaton_type' => $notificaton_type,'type_id' => $doc_id,'notification_role' => $user_info->user_role]);
        //echo "yes";die;
        return redirect('/admin-users/show/'.$user_id)->with('success', 'Document disapproved successfully!');
    } else {
        //echo "no";die;
        return redirect('/admin-users/show/'.$user_id)->with('danger', 'Something wrong!');
    }
  }
   public function excel_export_provider(Request $request)
    {

      /*$provider_users = DB::table('users')->join('category','users.sub_id','=','category.cat_id')
                 ->join('main_category','users.cat_id','=','main_category.id')
                 ->where('users.is_active', '1')
                 ->where('users.is_deleted', '0')
                 ->where('users.user_role','=','Provider')
                 ->select('users.name as Name','users.email as Email','users.phone as Phone','users.address as Address','users.cpf_id as CPF','users.rg as RG','category.cat_name as Subcategory','main_category.name as Category')
                 ->orderBy('users.id', 'DESC')
                 ->get();*/

                  $provider_users = DB::table('users')
                          ->where('user_role', '=', 'Provider')
                          ->where('is_active', '=', '1')
                          ->where('is_deleted', '=', '0')
                          ->orderBy('id', 'DESC')
                          ->get();

          $exceldata = array();
          if(count($provider_users) > 0){
              foreach($provider_users as $provider_info){

                  $mainCatInfo = DB::table('main_category')->where('id', '=', $provider_info->cat_id)->pluck('name');
                  $subCatInfo = DB::table('category')->where('cat_id', '=', $provider_info->sub_id)->pluck('cat_name');

                  if(count($mainCatInfo) > 0 ) { 
                         foreach($mainCatInfo as $mainCat) {
                            $maincatgry = $mainCat;
                   } 
                  }

                  if(count($subCatInfo) > 0 ) { 
                         foreach($subCatInfo as $subCat) {
                            $subcatgry = $subCat;
                   } 
                  } 
                 
                 $users['Name'] = $provider_info->name;
                 $users['Email'] = $provider_info->email;
                 $users['Phone'] = $provider_info->phone;
                 $users['CPF'] = $provider_info->cpf_id;
                 $users['RG'] = $provider_info->rg;
                 $users['Category'] = $maincatgry;
                 $users['Subcategory'] = $subcatgry;
                 $users['Address'] = str_replace(',', '', $provider_info->address);
                 $users['CEP'] = str_replace(',', '', $provider_info->zipcode);
                 $users['Registration Date'] = $provider_info->created_at;
                 //$users['ID'] = $provider_info->id;
                 array_push( $exceldata,  $users);
              }
          }        

      /* if(count($provider_users) > 0){
            $provider_users = json_decode(json_encode($provider_users), true );
            foreach($provider_users as $users){
               
               $users['Address'] = str_replace(',', '', $users['Address']);
               array_push( $exceldata,  $users);
            }
        }
*/
    
        //echo "<pre>"; print_r($provider_users);die;
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
             
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                     //print_r($row);die;
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
    public function excel_export_taker(Request $request)
    {

       $taker_users = DB::table('users')
                      ->select('name as Name','email as Email','phone as Phone','address as Address','created_at as Registration_Date')
                      ->where('is_active','1')
                      ->where('is_deleted', '0')
                      ->where('user_role','Taker')
                      ->orderBy('id', 'DESC')
                      ->get();
        $exceldata = array();
            if(count($taker_users) > 0){
                $taker_users = json_decode(json_encode($taker_users), true );
                foreach($taker_users as $users){
                   
                   //$users['Address_verified'] = $status;
                   $users['Address'] = str_replace(',', '', $users['Address']);
                   array_push( $exceldata,  $users);
                }
            }
       
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
              
              /*echo'<pre>';
              print_r($taker_users);
              echo'</pre>';
              die;*/

                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }

   public function resendVerificationEmail(Request $request, $id){
      try {
          $user = DB::table('users')->where('id', '=', $id)->first();
          $from_email = "NaVizinhança  <contato@navizinhanca.com>";
          // $from_email="contato@navizinhanca.com";
          $insertedId = $user->id;
          $name = $user->name;
          $email = $user->email;

          $to = $user->email;
          $message    = '';
          $logo_url = url('/public').'/images/logo.png';
          $mail_bg_img = url('/public').'/images/bg.jpg';
          $email_icon = url('/public').'/images/email_icon_mail.png';
          $LI_png = url('/public').'/images/LI.png';
          $fb_png = url('/public').'/images/fb.png';
          $insta_png = url('/public').'/images/insta.png';
          $messageBody = '';
          $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                <!--[if !mso]><!-- -->
                <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                <!-- <![endif]-->
                <title>Email Template</title>
                <style type="text/css">
                * {font-family: "Lato", sans-serif;}
                </style>
            </head>
          <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
          <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" >
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                          <tr>
                                              <td align="center" height="70" style="height:70px;">
                                                  <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center">
                                                  <hr>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                    <b style="color: #1497d5;
                                                      padding-right: 5px;">Olá </b>' . $name .',
                                      <div style="line-height: 35px">
                                          <span style="color: #1497d5;margin:10px auto;line-height:normal;">seja bem vindo(a) ao NaVizinhança.</span>
                                      </div>
                                      <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 45px;margin-top:5px;">Clique no link abaixo para validar seu cadastro junto ao NaVizinhança.</p>
                                   
                                  </td>
                              </tr>
                              <tr>
                                  <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr class="hide">
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                  <tr>
                      <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                  </tr>
                  <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" > 
                      <tr>
                          <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                  <tr>
                                      <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                          <div style="line-height: 26px;">
                                              <a href="https://www.navizinhanca.com/user/'.$insertedId.'" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Comece sua jornada</a>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td height="70">&nbsp;</td>  
                                  </tr>
                                      </table>
                                  </td>
                              </tr>
                  </table>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td align="center">
                          <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr> 
                                  <td>
                                      <div class="social-icons" style="text-align: center;">
                                          <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$insta_png.'">
                                          </a>
                                          <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$LI_png.'">
                                          </a>
                                          <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                              <img src="'.$fb_png.'">
                                          </a>
                                      </div>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              </table>
            </body>
          </html>';
          $subject ="NaVizinhança: Ative sua conta";
          $headers = "From: " .$from_email  . "\r\n".'X-Mailer: PHP/' . phpversion();
          // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          //$headers .= "CC: dev2.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $messageBody, $headers,'-faaa@gmail.com');
          if($mailsend) {

            $logedUser = Auth::user();
            $lastId = DB::table('resendactivationemailhistory')->insertGetId(
              ['user_id' => $id, 'send_by' => $logedUser->id]
            );
            
            $raehInfo = DB::table('resendactivationemailhistory')->where('id', '=', $lastId)->first();

            $resendacemailCount = $user->resend_ac_email + 1;
            
            User::where('id', $id)
                  ->update(['resend_ac_email' => $resendacemailCount, 'resend_ac_email_date' => $raehInfo->resend_date]);

            if($user->user_role=='Taker') {
              return redirect('/admin-taker')->with('success', 'Account activation email sent successfully to '. $name);
            } else {
              return redirect('/admin-provider')->with('success', 'Account activation email sent successfully to '. $name);
            }
            
          } else {
            if($user->user_role=='Taker') {
              return redirect('/admin-taker')->with('danger', 'Soory fail to send account activation email to '. $name);
            } else {
              return redirect('/admin-provider')->with('danger', 'Soory fail to send account activation email to '. $name);
            }
          }
          
      } catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    } 

  public function Inactiveusers(Request $request){
      try {
          $inactiveusers = DB::table('users')
                  ->where('user_role', '!=', 'admin')
                  ->where('close_request_action', '=', '0')
                  ->where('is_active', '=', '0')
                  ->where('is_deleted', '=', '0')
                  ->orderBy('id', 'DESC')
                 ->get();  
          return view('admin.inactiveusers.index', compact('inactiveusers'));
      } 
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

  public function resendVerificationEmailInactive(Request $request, $id){
      try {
          $user = DB::table('users')->where('id', '=', $id)->first();
          $from_email = "NaVizinhança  <contato@navizinhanca.com>";
          // $from_email="contato@navizinhanca.com";
          $insertedId = $user->id;
          $name = $user->name;
          $email = $user->email;

          $to = $user->email;
          $message    = '';
          $logo_url = url('/public').'/images/logo.png';
          $mail_bg_img = url('/public').'/images/bg.jpg';
          $email_icon = url('/public').'/images/email_icon_mail.png';
          $LI_png = url('/public').'/images/LI.png';
          $fb_png = url('/public').'/images/fb.png';
          $insta_png = url('/public').'/images/insta.png';
          $messageBody = '';
          $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                <!--[if !mso]><!-- -->
                <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                <!-- <![endif]-->
                <title>Email Template</title>
                <style type="text/css">
                * {font-family: "Lato", sans-serif;}
                </style>
            </head>
          <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
          <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" >
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                          <tr>
                                              <td align="center" height="70" style="height:70px;">
                                                  <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center">
                                                  <hr>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                    <b style="color: #1497d5;
                                                      padding-right: 5px;">Olá </b>' . $name .',
                                      <div style="line-height: 35px">
                                          <span style="color: #1497d5;margin:10px auto;line-height:normal;">seja bem vindo(a) ao NaVizinhança.</span>
                                      </div>
                                      <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 45px;margin-top:5px;">Clique no link abaixo para validar seu cadastro junto ao NaVizinhança.</p>
                                   
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                  <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" > 
                      <tr>
                          <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                  <tr>
                                      <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                          <div style="line-height: 26px;">
                                              <a href="https://www.navizinhanca.com/user/'.$insertedId.'" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Comece sua jornada</a>
                                          </div>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                      </tr>
                  </table>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td align="center">
                          <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr> 
                                  <td>
                                      <div class="social-icons" style="text-align: center;">
                                          <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$insta_png.'">
                                          </a>
                                          <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$LI_png.'">
                                          </a>
                                          <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                              <img src="'.$fb_png.'">
                                          </a>
                                      </div>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              </table>
            </body>
          </html>';
          $subject ="NaVizinhança: Ative sua conta";
          $headers = "From: " .$from_email  . "\r\n".'X-Mailer: PHP/' . phpversion();
          // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          //$headers .= "CC: dev2.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $messageBody, $headers,'-faaa@gmail.com');
          if($mailsend) {

            $logedUser = Auth::user();
            $lastId = DB::table('resendactivationemailhistory')->insertGetId(
              ['user_id' => $id, 'send_by' => $logedUser->id]
            );
            
            $raehInfo = DB::table('resendactivationemailhistory')->where('id', '=', $lastId)->first();

            $resendacemailCount = $user->resend_ac_email + 1;
            User::where('id', $id)
                  ->update(['resend_ac_email' => $resendacemailCount, 'resend_ac_email_date' => $raehInfo->resend_date]);

            return redirect('/admin-inactiveusers')->with('success', 'Account activation email sent successfully to '. $name);
          } else {
            return redirect('/admin-inactiveusers')->with('danger', 'Soory fail to send account activation email to '. $name);
          }
          
      } catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    } 

  public function ActivateUserAc(Request $request, $id){
      try {
        
          $user = User::where('id', $id)
                      ->update(['is_active' => 1]);

          $user_info = DB::table('users')
                          ->where('id', $id)
                          ->first();
                           
          if($user_info->is_active==1) {
            return redirect('/admin-inactiveusers')->with('success', 'Account activated successfully');
          } else {
            return redirect('/admin-inactiveusers')->with('danger', 'Soory fail to activate user account. Please try again');
          }  
          
      } 
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }   

    public function excel_export_inactiveusers(Request $request)
    {

       $inactive_users = DB::table('users')
                      ->select('name as Name','email as Email','user_role as Type','phone as Phone','address as Address', 'resend_ac_email_date as Date')
                      ->where('is_active','0')
                      ->where('is_deleted', '0')
                      ->orderBy('id', 'DESC')
                      ->get();
        $exceldata = array();
            if(count($inactive_users) > 0){
                $inactive_users = json_decode(json_encode($inactive_users), true );
                foreach($inactive_users as $users){
                   
                   //$users['Address_verified'] = $status;
                   $users['Address'] = str_replace(',', '', $users['Address']);
                   array_push( $exceldata,  $users);
                }
            }
       
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
              
              /*echo'<pre>';
              print_r($taker_users);
              echo'</pre>';
              die;*/

                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }       
     
}