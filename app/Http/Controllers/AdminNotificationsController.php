<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Notification;

class AdminNotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
      try{
        $listresult = Notification::leftjoin('users', 'notifications.user_id', '=','users.id' )
               ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
              ->leftjoin('close_account_request', 'notifications.type_id', '=', 'close_account_request.id')
              ->select('notifications.*','booking.booking_message','close_account_request.close_reason','users.profile_image','users.user_role','users.name','notifications.id as notify_id')
              ->where('notifications.is_deleted','0')
              ->orderBy('notifications.id', 'desc')
              ->paginate('10')
               
        ;
         
                    // echo "<pre>"; print_r($listresult);die;
       return view('admin.notifications',compact('listresult'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function store(Request $request)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request,$id)
    {
    }
    public function destroy(Request $request)
    {
         $id=$request->id;
       
       $remove =  Notification::where('id', $id)
            ->update(['is_deleted' => '1']);
        if($remove!='')
        {
        echo 1;
      }
    }
}
