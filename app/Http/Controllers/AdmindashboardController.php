<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Booking;
use App\Models\CloseAccountRequest;
use App\Models\Feedback;

class AdmindashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        try{
            /* COunt */
           $users_count = DB::table('users')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('user_role', '!=', 'admin')   
                     ->where('phone','!=','')
                     ->where('address','!=','')              
                     ->count();
            $order_count = DB::table('booking')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->count();
            $payment_count = DB::table('booking')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('payment_type', '!=', '')
                     ->count();
                     
            /*$feedback_count = DB::table('feedback')
                     ->count();*/
                     
            $feedback_count = Feedback::select('users.name','users.email','feedback.id as feedback_id','feedback.message','feedback.booking_id','feedback.rating','users.sub_id','feedback.provider_id','feedback.approve_status','category.*','feedback.user_id','feedback.*')
            ->leftjoin('users','users.id','=','feedback.provider_id')
            ->leftjoin('category','category.cat_id','=','users.sub_id')
            ->where('users.is_deleted',0)
            ->where('users.is_active',1)
            ->orderBy('feedback.id', 'desc')->count();
        
              /* Count */
              /* Listing */ 
            $users = DB::table('users')
                     ->where('users.is_active', '1')
                     ->where('users.is_deleted', '0')
                     ->where('users.user_role', '!=', 'admin')                 
                     ->limit('6')
                     ->orderBy('users.id', 'DESC')
                     ->get();  
             $provider_users = DB::table('users')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('user_role', '=', 'Provider')   
                     ->where('cat_id','!=','')              
                     ->limit('6')
                     ->orderBy('id', 'DESC')
                     ->get();
            $provider_users_count = DB::table('users')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('user_role', '=', 'Provider')   
                     ->count();  
            $taker_users_count = DB::table('users')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('user_role', '=', 'Taker')->count();
             $taker_users = DB::table('users')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->where('user_role', '=', 'Taker')                 
                     ->limit('6')
                     ->orderBy('id', 'DESC')
                     ->get();
            $booking = Booking::Join('users', 'booking.user_id', '=', 'users.id')
                        ->where('booking.is_active', '1')
                         ->where('booking.is_deleted', '0')
                         ->orderBy('booking.booking_id','DESC')
                         ->take('5')
                         ->get();
           $providerUsers = DB::table('users')->select('users.name as username','users.id as u_id','users.longitude','users.latitude','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.email','main_category.name as name','main_category.cat_image as main_cat_image','category.cat_name','category.cat_image','main_category.name')
                    // ->leftjoin('feedback','feedback.provider_id','=','users.id')
                    ->join('main_category','users.cat_id','=','main_category.id')
                    ->join('category','users.sub_id','=','category.cat_id')
                    ->where('users.user_role', 'Provider')
                    ->where('users.is_active', '1')
                    ->where('users.is_deleted', '0')
                    ->where('users.close_request_action', '0')
                    ->get();
            $closeaccountrequests=CloseAccountRequest::Join('users', 'close_account_request.user_id', '=', 'users.id')
                ->orderBy('close_account_request.id','desc')
                ->take('5')
                ->get();
            $average_rating = Feedback::select(DB::raw('provider_id,AVG(rating) AS avg_rating ,rating,users.id as u_id,users.name as user_name'))->join('users','users.id','=','feedback.provider_id')->groupBy('feedback.provider_id')->where('users.is_active', '1')
                ->where('users.is_deleted', '0')->get();
                     
          $category = DB::table('main_category')->where('is_active', '1')->where('is_deleted', '0')->get();
          $main_category = DB::table('main_category')->leftJoin('category','main_category.id','=','category.main_cat_id')->where('main_category.is_active', '1')->where('main_category.is_deleted', '0')->get();
            $ip = $request->ip(); 
            $getips= $this->getIpLatLong($ip);
            $latitude_center=$getips['lat'];
            $longitude_center=$getips['lng'];                  
            return view('admin.dashboard',compact('users_count', 'order_count', 'payment_count', 'users', 'booking','providerUsers','closeaccountrequests','ip','main_category','category','provider_users','taker_users','average_rating','provider_users_count','taker_users_count', 'feedback_count','longitude_center','latitude_center'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function notificationList(Request $request)
    {
        try{
            //echo "result";die;
          $listresult=   DB::table('notifications')
                         ->leftjoin('users', 'notifications.user_id', '=','users.id' )
                         ->leftjoin('close_account_request', 'notifications.type_id', '=', 'close_account_request.id')
                         ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
                         ->where('notifications.read_status', '0')
                         ->select('notifications.*', 'booking.booking_message','close_account_request.close_reason','users.*')

                         ->orderBy('notifications.created_at', 'desc')
                         ->limit(5)
                         ->get();
           return view('layouts.header-admin',compact('listresult'));
       }
       catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function unlockScreen(Request $request)
    {
        try{
            $id=Auth::user()->id;
            $name=Auth::user()->name;
            $e_password=$request->e_password;
            $e_user=$request->e_user;
            $hd_password = DB::table('users')
                          ->where('id', '=', $id)
                          ->value('password'); 
                          $check=Hash::check($e_password, $hd_password); 
                         if($e_user==$name && $check) 
                          {
                                return redirect()->action('AdmindashboardController@index');
                           }
                            else
                                {
                                    return redirect()->back()->withErrors(['Please fill the correct password.', 'Password not match.']);
                            //return redirect()->action('admin.lockscreen')->with('error', 'Password not matched');
                                } 
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }                 
    }
    function getIpLatLong($ipaddress){
    $data = array();
    $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=08f1bc91f71c45880fefc03eb7f5e695");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];
    return $data;
   }
}