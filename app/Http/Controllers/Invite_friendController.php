<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\User;

class Invite_friendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'delete']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $id = Auth::id();
            $user_info = DB::table('users')->where('id', $id)->first();
            $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','invite-friend')->where('is_active', '1')->where('is_deleted', '0')->first();
            return view('invite.index',compact('user_info','menu_meta_details'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function send(Request $request){
        try{
            $curent_id = Auth::id();
            $request_email = $request->email;
            $referal_link = $request->referal_link;
            $logo_url = url('/public').'/images/logo.png';
            $mail_bg_img = url('/public').'/images/bg.jpg';
            $email_icon = url('/public').'/images/email_icon_mail.png';
            $LI_png = url('/public').'/images/LI.png';
            $fb_png = url('/public').'/images/fb.png';
            $insta_png = url('/public').'/images/insta.png';
            $register_url = url('/').'/register';
            $request_email_arr = explode(",", $request_email);
            $from = 'NaVizinhança  <contato@navizinhanca.com>';
            foreach($request_email_arr as $email) {
            $inviteid = DB::table('invite_friend')->insert(
                    ['invite_by_id' => $curent_id, 'invite_to_email' => $email]
                );        
            $message    = '';
            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns:v="urn:schemas-microsoft-com:vml">
              <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                  <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                  <!--[if !mso]><!-- -->
                  <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                  <!-- <![endif]-->
                  <title>Email Template</title>
                  <style type="text/css">
                  * {font-family: "Lato", sans-serif;}
                  .ii a[href] { text-decoration: none!important; }
                  </style>
              </head>
            <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
            <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                            <tr>
                                                <td align="center" height="70" style="height:70px;">
                                                    <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <hr>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                    <tr>
                        <td align="center">
                            <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                                <tr>
                                    <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                        <div style="line-height: 35px">
                                            <span style="color: #1497d5;line-height: 41px;">Se cadastre agora mesmo no NaVizinhança.</span>
                                        </div>
                                        <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">'.Auth::user()->name.' gostou do nosso site e indicou seu e-mail para que você possa também se cadastrar.</p><p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">O NaVizinhança é uma plataforma de prestação de serviço que visa auxiliar tanto quem precisa da execução de um serviço quanto quem tem conhecimento para executá-lo.<br> 
                                        Simples e rápido! Bom para quem precisa de serviço! :)</p>

                                        <a href="'.$register_url.'" style="background-color: #1497d5;color:#fff;font-size: 20px;text-decoration: none;padding: 10px 20px 10px 20px;border-radius: 5px;">Cadastre-se</a>

                                    </td>
                                </tr>
                                <tr>
                                    <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="hide">
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                </table>
                <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="f4f4f4"  style="margin: 0 auto;overflow: hidden;background-image: url(https://navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                                <tr> 
                                    <td>
                                        <div class="social-icons" style="text-align: center;">
                                            <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                                <img src="'.$insta_png.'">
                                            </a>
                                            <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                                <img src="'.$LI_png.'">
                                            </a>
                                            <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                                <img src="'.$fb_png.'">
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>
                </table>
                </table>
              </body>
            </html>';
            $headers = "From: " .$from  . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $subject = "NaVizinhança: ".Auth::user()->name." te recomenda esse site de prestação de serviço!";
            $check_users = DB::table('users')->where('email',$request_email)->first();
            
            if(!empty($check_users)){
            return redirect()->action('Invite_friendController@index')->with('info','Esse usuário já está cadastrado. O e-mail não foi enviado. ');
            }
            else{
                // echo "nhi h";die;
                 $mailsend = mail($email,$subject,$message,$headers,'-faaaa@abc.com');
                if($mailsend){
                return redirect()->action(
                    'Invite_friendController@index')->with('success','Email enviado!');
                }
                else {
                  return redirect()->action(
                    'Invite_friendController@index')->with('error','Favor corrigir o e-mail digitado.');
                }
            }
            }
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
       
    }
        public function show(Request $request, $id) {
            $curent_id = Auth::user();
            if($curent_id){
               return redirect('/home');
            }else{ 
            // return redirect('/register');
              return redirect('/register');
           // return view('invite.show');
            }
        }
        }
