<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class WishlistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      try{
        $ip = $_SERVER['REMOTE_ADDR'];
         if(!empty(Auth::user())) {
          $userlatlng = DB::table('users')->where('id','=',Auth::user()->id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $getips= $this->getIpLatLong($ip);
            $sys_latitude=$getips['lat'];
            $sys_longitude=$getips['lng'];
          }
        }
        $unit = "K";
        $login_userId = Auth::id();
        $wish_listusers = DB::table('wish_list')
        ->select('users.*','wish_list.*',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
        ->join('users','users.id','=','wish_list.wish_profile_id')
        ->where('user_id', $login_userId)
        ->where('status',1)
        ->paginate(9);
        $user_category_name  = [];
        foreach($wish_listusers as $wishlist ){
          
          $user_category = DB::table('category')->where('cat_id',$wishlist->sub_id)->first();
          $user_category_name[$wishlist->name] = $user_category->cat_name;
          // $providerpricelist = DB::table('provider_price_list')
          //         ->leftjoin('price_queries','provider_price_list.query_id','=','price_queries.id')
          //         ->where('provider_price_list.user_id', '=', $wishlist->wish_profile_id)
          //         ->where('price_queries.priority', '1')
          //         ->first();
          // $usertotalreview = DB::table('feedback')
          //         ->join('users', 'users.id', '=', 'feedback.user_id')
          //         ->where('feedback.provider_id',$wishlist->wish_profile_id)
          //         ->where('approve_status',1)
          //         ->where('role','taker')
          //         ->count();
        }
        $c_ip = $request->ip();
        $wish_listip = DB::table('wish_list_ip')
         ->select('users.*','wish_list_ip.*',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
        ->join('users','users.id','=','wish_list_ip.wish_profile_id')->where('ip_address', $c_ip)->where('wish_profile_id', $login_userId)->get();
        foreach($wish_listip as $wishlistip ){
          $user_category = DB::table('category')->where('cat_id',$wishlistip->sub_id)->first();
          $user_category_name[$wishlistip->name] = $user_category->cat_name;
          // $providerpricelist = DB::table('provider_price_list')
          //         ->leftjoin('price_queries','provider_price_list.query_id','=','price_queries.id')
          //         ->where('provider_price_list.user_id', '=', $wishlistip->wish_profile_id)
          //         ->where('price_queries.priority', '1')
          //         ->first();
          // $usertotalreview = DB::table('feedback')
          //         ->join('users', 'users.id', '=', 'feedback.user_id')
          //         ->where('feedback.provider_id',$wishlistip->wish_profile_id)
          //         ->where('approve_status',1)
          //         ->where('role','taker')
          //         ->count();
        }
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','wishlist')->where('is_active', '1')->where('is_deleted', '0')->first();

      return view('pages.wishlist', compact('menu_meta_details','wish_listusers', 'wish_listipusers','provider_price_lists','user_category_name','wish_listip','c_ip'))->with('i', ($request->input('page', 1) - 1) * 6);;
      }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();
            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }
    }
    public function addwhish(Request $request){
    $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->userid)->where('user_id',Auth::user()->id)->count();
    if( $check_exist > 0 ) {
       echo '0';
    } else {
      $login_userId = Auth::id();
       $userid = $request->userid;
       $status = 1;
       $insert_whish = DB::table('wish_list')->insertGetId(
                       ['user_id' => $login_userId, 'wish_profile_id' => $userid,'status'=>$status]
                       );
       if($insert_whish) {
          echo '1';
        } else {
          echo '2';
       }
    }
  }

  public function removewhish(Request $request){
        $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->userid)->where('user_id',Auth::user()->id)->count();
          if( $check_exist > 0 ) {
            $login_userId = Auth::id();
            $userid = $request->userid;
            $remove_whish = DB::table('wish_list')
                            ->where('user_id', $login_userId)
                            ->where('wish_profile_id', $userid)
                            ->delete();
            if($remove_whish) {
                  echo '1';
            } else {
                  echo '2';
            }
          }
          else
          {
            echo '0';
          }
          
        }


    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
      if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
      }
      else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
          return $miles;
        }
      }
}
function getIpLatLong($ipaddress){
    $data = array();
    $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=08f1bc91f71c45880fefc03eb7f5e695");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];
    return $data;
   }

}
