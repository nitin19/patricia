<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Booking;
use App\Models\CloseAccountRequest;
use App\Models\Feedback;

class AdminFeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* function for displaying all feedbacks */

    public function index(Request $request){
        $feedbacks = Feedback::select('users.name','users.email','feedback.id as feedback_id','feedback.message','feedback.booking_id','feedback.rating','users.sub_id','feedback.provider_id','feedback.approve_status','category.*','feedback.user_id','feedback.*')
        ->leftjoin('users','users.id','=','feedback.provider_id')
        ->leftjoin('category','category.cat_id','=','users.sub_id')
        ->where('users.is_deleted',0)
        ->where('users.is_active',1)
        ->orderBy('feedback.id', 'desc')->get();
     
        return view('admin.feedback.index',compact('feedbacks'));
    }

    /*function name approve feedback */
    public function approve_feedback(Request $request){

        $feedback = Feedback::where('id',$request->feedback_id)->first();
        $feedback->approve_status = '1';
        if($feedback->update()){
            $provider_rate = Feedback::where('provider_id', $feedback->provider_id)->where('approve_status', '1')->where('role', 'taker')->avg('rating');
            $taker_rate = Feedback::where('user_id', $feedback->user_id)->where('approve_status', '1')->where('role', 'provider')->avg('rating');
            if($feedback->role == 'taker'){
              if($provider_rate){
                $rate_val = round($provider_rate,2);
                $user_rating = DB::table('users')
                         ->where('id', $feedback->provider_id)
                         ->update(['profile_rating' => $rate_val]);
              }
            }else{
              if($taker_rate){
                $rate_val = round($taker_rate,2);
                $user_rating = DB::table('users')
                         ->where('id', $feedback->user_id)
                         ->update(['profile_rating' => $rate_val]);
              }

            }
            $response['success'] = "true";
            $response['message'] = "Feedback approved successfully";
        }
        else{
            $response['success'] = "false";
            $response['message'] = "Feedback Not Approved";
        }
        echo json_encode($response);
    }

    /*function name denied  feedback */
    public function denied_feedback(Request $request){
        $feedback = Feedback::where('id',$request->feedback_id)->first();
        $feedback->approve_status = '2';
        if($feedback->update()){
            $response['success'] = "true";
            $response['message'] = "Feedback denied successfully";
        }
        else{
            $response['success'] = "false";
            $response['message'] = "Feedback Not declined";
        }
        echo json_encode($response);
    }
    public function excel_export(Request $request)
    {

        $feedbacks = Feedback::select('category.cat_name as Order_name','users.name as Provider_name','feedback.user_id as Taker_name','feedback.message as Feedback','feedback.rating as Rating','feedback.approve_status as Status')
        ->join('users','users.id','=','feedback.provider_id')
        ->leftjoin('category','category.cat_id','=','users.sub_id')->where('users.is_deleted',0)->where('users.is_active',1)->where('users.approved_by_admin',1)->orderBy('feedback.id', 'desc')->get()->toArray();
        

        $exceldata = array();
        if(count($feedbacks) > 0){
            foreach($feedbacks as $feedback){
                $takername = DB::table('users')
                    ->select('name as Takername')
                    ->where('users.id', '=', $feedback['Taker_name'])->first();
                if($feedback['Status']=='1'){
                    $status = 'Approved';
                }elseif($feedback['Status']=='2') {
                    $status = 'Denied';
                } else {
                    $status = 'Pending';
                }
               

               $feedback['Taker_name'] = $takername->Takername; 
               $feedback['Status'] = $status;
               $feedback['Feedback'] = preg_replace('/[^A-Za-z0-9\-]/', "", $feedback['Feedback']);
               array_push( $exceldata,  $feedback);
            }
        }

     // echo "<pre>";print_r($feedbacks);die;
     // foreach ($feedbacks as $row) {
     //     $takername = DB::table('users')
     //             ->select('name as Takername ');
     //             ->where('users.is_active', 1)
     //             ->where('users.is_deleted', 0)
     //             ->where('users.id', '==', $row['Taker_name']);
     // }
       
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {

                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                     //print_r($row);die;
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
    
}