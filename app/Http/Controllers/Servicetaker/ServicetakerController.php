<?php
namespace App\Http\Controllers\Servicetaker;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Notification;
class ServicetakerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      try{
        $id = Auth::id();

          /*$user_info = DB::table('users')
                ->where('id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->where('approved_by_admin', '1')
                ->first();*/

                $user_info = Auth::user();

                $current_ip = $request->ip();
                $currentuser_ip = DB::table('wish_list_ip')
                ->where('ip_address', $current_ip)
                ->get();
                $currentuser_ip_count = DB::table('wish_list_ip')
                ->where('ip_address', $current_ip)
                ->count();
                $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil","Brasil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
                $sub_category = DB::table('category')->pluck('cat_name','cat_id');

                $user_image = DB::table('user_gallery')
                ->where('user_id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->get();
                $user_documents = DB::table('user_documents')
                ->where('user_id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->get();
                $main_category = DB::table('main_category')->pluck('name','id');
                $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/profile')->where('is_active', '1')->where('is_deleted', '0')->first();
                return view('servicetaker.index', compact('countries','category','user_info', 'user_image', 'currentuser_ip', 'currentuser_ip_count','sub_category','main_category','user_documents','menu_meta_details'));
              }
              catch(\Illuminate\Database\QueryException $e){
                $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

                $errorlog                   = new Errorlogs;
                $errorlog->error_message    = $e->getMessage();
                $errorlog->line_number      = $e->getLine();
                $errorlog->file_name        = $e->getFile();
                $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
                $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
                $errorlog->loggedin_id      = Auth::user()->id;
                $errorlog->ip_address       = "";
                $errorlog->save();
                return view('errors.custom',compact('customerror')); 
              }catch(\Exception $e){ 
                $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

                $errorlog                   = new Errorlogs;
                $errorlog->error_message    = $e->getMessage();
                $errorlog->line_number      = $e->getLine();
                $errorlog->file_name        = $e->getFile();
                $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
                $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
                $errorlog->loggedin_id      = Auth::user()->id;
                $errorlog->ip_address       = "";
                $errorlog->save();
                return view('errors.custom',compact('customerror')); 
              }
            }

    public function incomplete_profile(Request $request)
    {
      try{
        $id = Auth::id();

          /*$user_info = DB::table('users')
                ->where('id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->where('approved_by_admin', '1')
                ->first();*/

                $user_info = Auth::user();

                $current_ip = $request->ip();
                $currentuser_ip = DB::table('wish_list_ip')
                ->where('ip_address', $current_ip)
                ->get();
                $currentuser_ip_count = DB::table('wish_list_ip')
                ->where('ip_address', $current_ip)
                ->count();
                $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil","Brasil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
                $sub_category = DB::table('category')->pluck('cat_name','cat_id');

                $user_image = DB::table('user_gallery')
                ->where('user_id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->get();
                $user_documents = DB::table('user_documents')
                ->where('user_id', $id)
                ->where('is_active', '1')
                ->where('is_deleted', '0')
                ->get();
                $main_category = DB::table('main_category')->pluck('name','id');
                $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/profile')->where('is_active', '1')->where('is_deleted', '0')->first();
                return view('servicetaker.complete_profile', compact('countries','category','user_info', 'user_image', 'currentuser_ip', 'currentuser_ip_count','sub_category','main_category','user_documents','menu_meta_details'));
              }
              catch(\Illuminate\Database\QueryException $e){
                $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

                $errorlog                   = new Errorlogs;
                $errorlog->error_message    = $e->getMessage();
                $errorlog->line_number      = $e->getLine();
                $errorlog->file_name        = $e->getFile();
                $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
                $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
                $errorlog->loggedin_id      = Auth::user()->id;
                $errorlog->ip_address       = "";
                $errorlog->save();
                return view('errors.custom',compact('customerror')); 
              }catch(\Exception $e){ 
                $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

                $errorlog                   = new Errorlogs;
                $errorlog->error_message    = $e->getMessage();
                $errorlog->line_number      = $e->getLine();
                $errorlog->file_name        = $e->getFile();
                $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
                $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
                $errorlog->loggedin_id      = Auth::user()->id;
                $errorlog->ip_address       = "";
                $errorlog->save();
                return view('errors.custom',compact('customerror')); 
              }
            }

    public function update(Request $request) {
              try{

               $phone = trim($request->phone);
               $user_role = trim($request->user_role);
               $user_id = trim($request->user_id);
               $cpf_id = trim($request->cpf_id);
               $rg = trim($request->rg);
               $newdob = str_replace('/', '-', trim($request->dob));
               $dob = date('Y-m-d', strtotime($newdob));
               $address_str = str_replace("'", "", trim($request->address));
               $address = urlencode($address_str);
               $bio = trim($request->bio);
               $additional_details = trim($request->additional_details);
               $price = 10;

             $PostalCode = trim($request->zipcode);
             $country_name = str_replace("'", "", trim($request->hdcountry));
              if($country_name == 'Brazil' || $country_name == 'brazil'){
                  $countryname = 'Brasil';
                } else {
                  $countryname  = $country_name;
                }
             $statename = str_replace("'", "", trim($request->hdstate));
             $neighborhoodname = str_replace("'", "", trim($request->hdneighborhood));
             //$strnumber = $request->hdstrnumber;
             $cityname = str_replace("'", "", trim($request->hdcity));
             $hdaddress = str_replace("'", "", trim($request->hdaddress));
             $complemento = trim($request->complemento);
             $strnumber = trim($request->strnumber);
             $lat = trim($request->hdlat);
             $long = trim($request->hdlng);
             $location_type = trim($request->iscorrect);

    $user_updated = DB::table('users')
       ->where('id', $user_id)
       ->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'state'=> $statename, 'country' => $countryname, 'city' => $cityname, 'neighborhood'=>$neighborhoodname, 'street_number'=>$strnumber, 'address'=>$hdaddress, 'complemento'=>$complemento, 'location_type'=>$location_type]); 

           if($request->hasfile('user_images')) {
            foreach($request->file('user_images') as $images ) {
             $names = time() . '.' . $images->getClientOriginalName();
             $image_resizes = Image::make($images->getRealPath());
             $image_resizes->resize(480, 400);
             $image_resizes->save(public_path('images/userimages/' .$names));
             DB::table('user_gallery')->insert(
              ['image_name' => $names, 'user_id' => $user_id]
            );
           }
         }
        
         if($request->hasfile('cpf_documents')){
           $file = $request->file('cpf_documents');
           foreach($request->file('cpf_documents') as $images ) {
             $names = time() . '-' . $images->getClientOriginalName();
             $destinationPath = public_path('images/user_document/');
             $images->move($destinationPath,$names);
             
             DB::table('user_documents')->insert(
              ['document' => $names, 'user_id' => $user_id]
            );
           }
            
          }
        
        if($request->profileimage!='') {

                $originalPath = public_path().'/images/profileimage/';
                $image_parts = explode(";base64,", $request->profileimage);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $filename = time().'.png';
                $file = $originalPath.$filename;
                file_put_contents($file, $image_base64);
                $user_updated = DB::table('users')
                  ->where('id', $user_id)
                  ->update(['profile_image' => $filename]);
          }
        $location_type = $request->iscorrect;
        $user_updated = DB::table('users')
        ->where('id', $user_id)
        ->update(['phone' => $phone, 'address' => $address_str,'bio'=>$bio,'additional_details'=>$additional_details,'price'=>$price,'name'=>$request->name, 'cpf_id' => $cpf_id, 'rg' => $rg, 'dob' => $dob, 'location_type'=>$location_type]);
        return redirect('/servicetaker/dashboard')->with('success', 'Dados cadastrados com sucesso.');
        //return redirect('/servicetaker/dashboard')->with('success', 'Atualizado com sucesso!');
      }
      catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror')); 
      }
    }

    public function profile_update(Request $request) {
              try{

               $phone = trim($request->phone);
               $user_role = trim($request->user_role);
               $user_id = trim($request->user_id);
               $cpf_id = trim($request->cpf_id);
               $rg = trim($request->rg);
               $newdob = str_replace('/', '-', trim($request->dob));
               $dob = date('Y-m-d', strtotime($newdob));
               $address_str = str_replace("'", "", trim($request->address));
               $address = urlencode($address_str);
               $bio = trim($request->bio);
               $additional_details = trim($request->additional_details);
               $price = 10;

             $PostalCode = trim($request->zipcode);
             $country_name = str_replace("'", "", trim($request->hdcountry));
              if($country_name == 'Brazil' || $country_name == 'brazil'){
                  $countryname = 'Brasil';
                } else {
                  $countryname  = $country_name;
                }
             $statename = str_replace("'", "", trim($request->hdstate));
             $neighborhoodname = str_replace("'", "", trim($request->hdneighborhood));
             //$strnumber = $request->hdstrnumber;
             $cityname = str_replace("'", "", trim($request->hdcity));
             $hdaddress = str_replace("'", "", trim($request->hdaddress));
             $complemento = trim($request->complemento);
             $strnumber = trim($request->strnumber);
             $lat = trim($request->hdlat);
             $long = trim($request->hdlng);
             $location_type = trim($request->iscorrect);

    $user_updated = DB::table('users')
       ->where('id', $user_id)
       ->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'state'=> $statename, 'country' => $countryname, 'city' => $cityname, 'neighborhood'=>$neighborhoodname, 'street_number'=>$strnumber, 'address'=>$hdaddress, 'complemento'=>$complemento, 'location_type'=>$location_type]); 


           if($request->hasfile('user_images')) {
            foreach($request->file('user_images') as $images ) {
             $names = time() . '.' . $images->getClientOriginalName();
             $image_resizes = Image::make($images->getRealPath());
             $image_resizes->resize(480, 400);
             $image_resizes->save(public_path('images/userimages/' .$names));
             DB::table('user_gallery')->insert(
              ['image_name' => $names, 'user_id' => $user_id]
            );
           }
         }

         if($request->hasfile('cpf_documents')){
           $file = $request->file('cpf_documents');
           foreach($request->file('cpf_documents') as $images ) {
             $names = time() . '-' . $images->getClientOriginalName();
             $destinationPath = public_path('images/user_document/');
             $images->move($destinationPath,$names);
             
             DB::table('user_documents')->insert(
              ['document' => $names, 'user_id' => $user_id]
            );
           }
            
          }

        if($request->profileimage!='') {

                $originalPath = public_path().'/images/profileimage/';
                $image_parts = explode(";base64,", $request->profileimage);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $filename = time().'.png';
                $file = $originalPath.$filename;
                file_put_contents($file, $image_base64);
                $user_updated = DB::table('users')
                  ->where('id', $user_id)
                  ->update(['profile_image' => $filename]);
          }
        $location_type = $request->iscorrect;
        $user_updated = DB::table('users')
        ->where('id', $user_id)
        ->update(['phone' => $phone, 'address' => $address_str,'bio'=>$bio,'additional_details'=>$additional_details,'price'=>$price,'name'=>$request->name, 'cpf_id' => $cpf_id, 'rg' => $rg, 'dob' => $dob, 'location_type'=>$location_type]);
        return redirect('/servicetaker/dashboard')->with('success', 'Atualizado com sucesso!');
      }
      catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror')); 
      }
    }

    public function show(Request $request) {

      $imageid = $request->imageid;

      $delquery = DB::table('user_gallery')
      ->where('image_id', $imageid)
      ->delete();
      if($delquery) {
        echo 'Success';
      } else {
        echo '1233';
      }
    }
    public function delete_image(Request $request) {


    }

    public function wishlistip(Request $request)
    {

     $current_ip = $request->ip();
     $login_userid = Auth::id();
     $currentuser_ip = DB::table('wish_list_ip')
     ->where('ip_address', $current_ip)
     ->get();
     $currentuser_ip_count = DB::table('wish_list_ip')
     ->where('ip_address', $current_ip)
     ->count();
     foreach($currentuser_ip as $currentuser_info) {
       $wish_profile_id = $currentuser_info->wish_profile_id;
       $currentuser_count = DB::table('wish_list')
       ->where('user_id', $login_userid)
       ->where('wish_profile_id', $wish_profile_id)
       ->count();
       if($currentuser_count==0 || $currentuser_count==''){
         $insert_whish = DB::table('wish_list')->insert(
           ['user_id' => $login_userId, 'wish_profile_id' => $userid]
         );
       }
     }

   }



   public function convert_address($address){
     $address_str = $address ;
     $address = urlencode($address_str);
    //  echo $address;die;
     $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".$address."&country=Brazil&language=en-US&additionaldata=&maxresults=20&gen=8";
     $filestring = file_get_contents($url);
     $filearray = explode("\n", $filestring);
     $data = json_decode($filearray[0]);
       	// echo"<pre>";
       	// print_r($data);die;
     $array = json_decode(json_encode($data), True);
     foreach ($array as $response => $res_value) {
       foreach($res_value['View'] as $view_key=>$view_value){
				// print_r($view_value['Result']);
         for($i=0;$i<count($view_value['Result']);$i++){
           $zipcode =	$view_value['Result'][0]['Location']['Address']['PostalCode'];
           $countryname = $view_value['Result'][0]['Location']['Address']['AdditionalData'][0]['value'];
          if($countryname == 'Brazil' || $countryname == 'brazil'){
            $country_name = 'Brasil';
          } else {
            $country_name  = $countryname;
          }
           $state_name = $view_value['Result'][0]['Location']['Address']['AdditionalData'][1]['value'];

         }
       }
     }
     $response = [];
     if(!empty($data)){
      $response['success']="true";
      $response['message']="address converted successfully";
      $response['res'] = ['zipcode'=>$zipcode,'country_name' => $country_name,'state_name' => $state_name] ;
    }
    else{
      $response['success']="false";
      $response['message']="address could not be converted";
    }
    echo json_encode($response);
  }

  public function unlockScreen(Request $request)
  {
    try{
      $id=Auth::user()->id;
      $name=Auth::user()->name;
      $e_password=$request->e_password;
      $e_user=$request->e_user;
      $hd_password = DB::table('users')
      ->where('id', '=', $id)
      ->value('password'); 
      $check=Hash::check($e_password, $hd_password); 
      if($e_user==$name && $check) 
      {
        return redirect('/servicetaker/profile');
      }
      else
      {
        return redirect()->back()->withErrors(['Please fill the correct password.', 'Password not match.']);
                            //return redirect()->action('admin.lockscreen')->with('error', 'Password not matched');
      } 
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }catch(\Exception $e){ 
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }                 
  }
  public function destroyNotification(Request $request)
  {
   $id=$request->id;
   
   $remove =  Notification::where('id', $id)
   ->update(['is_deleted' => '2']);
   if($remove!='')
   {
    echo "Notification Deleted Successfully.";
  }
}
public function checkCpfExist(Request $request) {
      // print_R($request->All());die;
  $avail =  DB::table('users')->where('cpf_id', $request->cpf_id )->where('id','!=',$request->user_id)->where('is_active', '=', 1)->where('is_deleted', '=', 0)->first();
  
  if($avail){
    echo 'Exists';
  }
  else{
       echo '0';
  } 
}
function getZipLatLong($zipcode){     //latlong according to zipcode
    $data = array();
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&key=AIzaSyAtVchOE56ZJyqA-K9hM1WAevCeOhnsi30&sensor=false";
    $details=file_get_contents($url);
    $result = json_decode($details,true);
    $data['lat']=@$result['results'][0]['geometry']['location']['lat'];
    $data['lng']=@$result['results'][0]['geometry']['location']['lng'];
    return $data;
}

public function profile_phone_update(Request $request) {
  $phone = $request->phone;
  $user_id = $request->user_id;
  $update_phone = DB::table('users')
            ->where('id', $user_id)
            ->update(['phone' => $phone]);
  if($update_phone) {
    echo 'true';
  } else {
    echo 'false';
  }
}
public function profile_name_update(Request $request) {
  $name = $request->name;
  $user_id = $request->user_id;
  $update_name = DB::table('users')
            ->where('id', $user_id)
            ->update(['name' => $name]);
  if($update_name) {
    echo 'true';
  } else {
    echo 'false';
  }
}
public function profile_cpf_update(Request $request) {
  $cpf = $request->cpf;
  $user_id = $request->user_id;
  $update_cpf = DB::table('users')
            ->where('id', $user_id)
            ->update(['cpf_id' => $cpf]);
  if($update_cpf) {
    echo 'true';
  } else {
    echo 'false';
  }
}
public function profile_rg_update(Request $request) {
  $rg = $request->rg;
  $user_id = $request->user_id;
  $update_rg = DB::table('users')
            ->where('id', $user_id)
            ->update(['rg' => $rg]);
  if($update_rg) {
    echo 'true';
  } else {
    echo 'false';
  }
}


}
