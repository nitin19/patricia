<?php
namespace App\Http\Controllers\Servicetaker;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Contact;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Spavailability;
use App\Models\Notification;
use IntlDateFormatter;
use DateTime;
// use App\Models\Spavailability;

class ServicetakerdashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
              
        if($user_info->latitude != '' && $user_info->longitude != '') {
                $latitude_center = $user_info->latitude;
                $longitude_center = $user_info->longitude;
            } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $latitude_center = $getips['lat'];
                  $longitude_center = $getips['lng'];
                } else {
                  $latitude_center = '-23.60332035036982';
                  $longitude_center = '-46.723359508984345';
                }
            }
            
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/dashboard')->where('is_active', '1')->where('is_deleted', '0')->first();
        return view('servicetaker.dashboard.dashboard', compact('countries','category','user_info', 'user_image','role','menu_meta_details','menu_meta_details','latitude_center','longitude_center'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    
    function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    return $data;
   } 
   
    public function booking_message(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        
        $booking_payment_pending = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->paginate('20');
        $booking_payment_past_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
        $booking_payment_confirm_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
        $booking_payment_pending_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/bookings')->where('is_active', '1')->where('is_deleted', '0')->first();
       
        return view('servicetaker.dashboard.booking.pending_booking', compact('countries','category','user_info', 'user_image','role', 'booking_payment_pending', 'booking_payment_pending_count','booking_payment_past_count','booking_payment_confirm_count','menu_meta_details'));
        //return view('servicetaker.dashboard.booking.pending_booking',compact('user_info','booking_payment_pending','booking_payment_pending_count','booking_payment_past_count','booking_payment_confirm_count','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function taker_account(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        $user_documents = DB::table('user_documents')
        ->where('user_id', $id)
        ->where('is_active', '1')
        ->where('document_status','1')
        ->where('is_deleted', '0')
        ->get();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/account/details')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.account_section.account_profile', compact('countries','category','user_info', 'user_image','role','user_documents','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function role_switch(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
         $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/switch/role')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.switch', compact('countries','category','user_info', 'user_image','role','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function booking_confirmed(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        $booking_payment_confirm = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->orderBy('booking_id', 'desc')
                      ->paginate(20);
        
       $booking_payment_past_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
        $booking_payment_confirm_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
        $booking_payment_pending_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/booking/confirmed')->where('is_active', '1')->where('is_deleted', '0')->first();
        
                   
        return view('servicetaker.dashboard.booking.confirm_booking', compact('countries','category','user_info', 'user_image', 'booking_payment_confirm','role', 'booking_payment_confirm_count','booking_payment_past_count','booking_payment_pending_count','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function booking_denied(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
        
        $booking_payment_past = DB::table('booking')
                      ->join('users', 'users.id', '=', 'booking.profile_id')
                      ->where('booking.user_id', $id)
                      ->where('booking.is_active', '1')
                      ->where('booking.is_deleted', '0')
                      ->where('booking.booking_verified', '2')
                      ->select('users.name', 'users.profile_image', 'booking.*')
                      ->orderBy('booking.booking_id','DESC')
                      ->paginate(20);

         $booking_payment_past_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();

        $booking_payment_confirm_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
        $booking_payment_pending_count = DB::table('booking')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
         $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/booking/denied')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.booking.decline_booking', compact('countries','category','user_info', 'user_image', 'booking_payment_past', 'booking_payment_past_count','booking_payment_confirm_count','booking_payment_pending_count','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
   public function account_card_details(Request $request)
    {
      try{
        $id = Auth::id();
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();
         $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/carddetails')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.account_section.add_card_details', compact('countries','category','user_info', 'user_image','role','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function account_notifications(Request $request)
    {
      try{
        $id = Auth::id();
        $i=0;
        $current_date = date("Y-m-d H:i:s");
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;       
        $category = DB::table('category')->get();
        $user_image = DB::table('user_gallery')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->get();


        $is_deleted = array('0','1');
        $read_status = array('0');

        $notificaton_type = array('booking','document');
     $listresult = Notification::select('notifications.*','notifications.id as notify_id','notifications.created_at as notify_time','booking.booking_message','booking.booking_verified','booking.profile_id','booking.booking_time','user_documents.document_status')
       
          ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
          ->leftjoin('user_documents', 'notifications.type_id', '=','user_documents.id')
          ->whereIn('notifications.is_deleted',$is_deleted)
          ->whereIn('notifications.notificaton_type',$notificaton_type)
          ->where('notifications.user_id', $id)
          ->where('notifications.notification_role', 'Provider')
          ->whereNotIn('notifications.read_status',$read_status)
          ->orderBy('notifications.created_at', 'desc')
          ->simplePaginate(5);
         /*dd(DB::getQueryLog());
         exit(); */

           if($listresult){
             foreach($listresult as $notification){
                $fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                );
                $dt = new DateTime($notification->created_at. "+1 days");
                $d = $dt->format('d');
                $m = $dt->format('m');
                $y = $dt->format('Y');
                $time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                if($notification->notificaton_type ='booking'){
                  //echo $notification->profile_id;

                $userinfo = DB::table('users')->select('name as provider_name','profile_image as provider_image')->where('id', '=', $notification->profile_id)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->get()->first();
                //$userinfo = DB::table('users')->where('id', '=', $notification->profile_id)->get();
              if ($userinfo){
              if(!empty($userinfo->provider_name)){
                if($notification->booking_verified == '1'){
                   $msg = "Parabéns! Solicitação de serviço confirmada.";
                 } else {
                   $msg = "Solicitação de serviço negada.";
                 }
              }
            }
                          
              }else{
                $userinfo = DB::table('users')->select('name as provider_name','profile_image as provider_image')->where('id', '=',$notification->user_id)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->get()->first();
                if ($userinfo) 
                 if(!empty($userinfo->provider_name)){
                    if($notification->document_status == '1') {
                    $msg = "Parabéns! Documento é aprovado.";
                  } else {
                    $msg = "Desculpa! seu documento não foi aprovado.";
                  }
                  }
                }
            
               if ($userinfo) {
                  if(!empty($userinfo->provider_name)){                
                $listresult[$i]->provider_name = $userinfo->provider_name;
                $listresult[$i]->provider_image = $userinfo->provider_image;
                $listresult[$i]->notify_time = $time;
                $listresult[$i]->notify_message = $msg;
                  }
                } 
                 $i++;
             }
          }

         $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/account/notifications')->where('is_active', '1')->where('is_deleted', '0')->first();
          
        return view('servicetaker.dashboard.account_section.notification_section', compact('category','user_info', 'user_image','role','listresult','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function account_close(Request $request)
    {
      try{
        $id = Auth::id();
        $user_info = DB::table('users')
              ->where('id', $id)
              ->where('is_active', '1')
              ->where('is_deleted', '0')
              ->where('approved_by_admin', '1')
              ->first();
        $role = $user_info->user_role;
      
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/account/close')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.account_section.close_account', compact('user_info','role','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function review(Request $request)
    {
      try{
        $id = Auth::id();
        
        $userreview  = DB::table('feedback')
                        ->leftjoin('users', 'users.id', '=', 'feedback.provider_id')
                        ->join('booking', 'feedback.booking_id', '=', 'booking.booking_id')
                        ->select('feedback.*','users.name','users.email','users.profile_image','booking.category','booking.sub_category')
                        ->where('feedback.role', 'provider')
                        ->where('feedback.approve_status', '1')
                        ->where('feedback.user_id', $id)
                        ->simplePaginate(5);
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','servicetaker/account/review')->where('is_active', '1')->where('is_deleted', '0')->first();
                   
        return view('servicetaker.dashboard.account_section.review', compact('userreview','menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    
    public function save_card(Request $request) {
        $card_num = $request->card_num;
        $paymentname = $request->paymentname;
        $exp_month = $request->exp_month;
        $exp_year = $request->exp_year;
        $cvv = $request->cvv;
        $id = Auth::id();
        $user_info = DB::table('users')
                     ->where('id', $id)
                     ->update(['cardnumber' => $card_num, 'cardname' => $paymentname, 'exp_month' => $exp_month, 'exp_year'=> $exp_year, 'cvv' => $cvv]);
                     echo 'true';
    }

    public function switchdashboard(Request $request) {
        $current_userId = $request->current_userId;
        $role_switch = $request->role_switch;
        //print_R($current_userId);die;
        $role_update = DB::table('users')
                       ->where('id', $current_userId)
                       ->update(['user_role' => $role_switch]);

        if($role_update) {
          return redirect('/logout');
       }
    }

      public function closeaccount(Request $request)
       {
         //print_r($request->all());die();
        $current_userId = $request->current_userId;
        $close_reason = $request->close_reason;
        $check_exist = DB::table('close_account_request')->where('user_id',$current_userId)->count();
        if($check_exist > 0){
          $response['success']="false";
          $response['message']="Close request already generated by you.";
        } else {
          $close_account = DB::table('close_account_request')->insertGetId(
                      ['user_id' => $current_userId, 'close_reason' => $close_reason]
                  );
          $close_accounts = DB::table('users')
                          ->where('id', $current_userId)
                          ->update(['is_active' => '0', 'close_request_action' => '1']);
          if($close_account!=''){
             $notifie = DB::table('notifications')->insert(
                      ['user_id' => $current_userId, 'notificaton_type' => 'closerequest','type_id' => $close_account]
                  );
            $response['success']="true";
            $response['message']="suceesss";
          }
          else{
            $response['success']="false";
            $response['message']="somthing went wrong";
          }
        }
      echo json_encode($response);

      }
      public function approved_status(Request $request)
       {
         $result =array();
          $html = '';
         $booking_id = $request->booking_id;
         $status = $request->approve_status;
         $booking_updated = DB::table('booking')
            ->where('booking_id', $booking_id)
            ->update(['booking_verified' =>$request->approve_status]);
        if($status =='1')
        {
          $booking_msg1 = "Congratulations!!Your request for booking service was confirmed.";
        }else{
           $booking_msg1 = "Sorry!!Your request for booking service was Rejected.";
        }
        
        if($booking_updated)
        {
          $booking = DB::table('booking')
            ->where('booking_id', $booking_id)
           ->select('*')
          ->first();
          $booking_status = $booking->booking_verified;
          $user_book_by = $booking->user_id;
          $booking_message = $booking->booking_message;
          $booking_amount = $booking->booking_amount;
          $category = $booking->category;
          $booking_start_time = date('F j, Y ', strtotime($booking->booking_start_time));
          $booking_end_time = date('F j, Y ', strtotime($booking->booking_end_time));
          $user_info = DB::table('users')
            ->where('id', $user_book_by)
            ->first();
          $name = $user_info->name;
          $profile_image = $user_info->profile_image;
          if($profile_image==''){
            $img_src= url('/public').'/images/gravtar.jpeg';
            }else {
            $img_src= url('/public').'/images/profileimage/'.$profile_image;
           }
         
          $html .='<tr class="tabledata confirmedtable past_'.$booking_id.'"><td class="username column4"><h4><img src="'.$img_src.'">
                    <span>'.ucfirst($name).'</span></h4></td><td class="userdesc column5"><p>'.mb_substr($booking_message, 0, 50).'</p></td><td class="column3">'.$category.'</td><td class="column3">'. $booking_start_time.'</td><td class="column2">'.$booking_end_time.'</td><td class="column3">$.'.$booking_amount.'</td></tr>';

          $booking_msg = "Booking status updated successfully.";
          
                
          //$to = 'dev2.bdpl@gmail.com';
             $email = Auth::user()->email;
             $to = $user_info->email;
             $logo_url = url('/public').'/images/yellow_logo_new.png';
             $mail_bg_img = url('/public').'/images/bg.png';
             $email_icon = url('/public').'/images/email_icon_mail.png';
             $mail_icon = url('/public').'/images/mail.png';
             $phone_img = url('/public').'/images/phone1.png';
             $thankyouimg = url('/public').'/images/thankyou.png';
             $messageBody = '';
               $messageBody = '<html>
               <head>
               <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
               <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
               </head>
               <body>
               <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
               <div class="container">
               <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
               <div class="" style="width: 500px;margin: 0 auto;">
               <div class="row">
               <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
               <img style="width:150px" src="'.$logo_url.'" alt="Logo">
               </div>
               <div class="" style="text-align: center;padding-bottom: 0px;">
               <img style="width:60px" src='.$email_icon.'>
               </div>
               <div class="" style="text-align: center;">
               <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0"></span>, </h6>
               </div>
               <div class="" style="text-align: center;padding: 15px 0px;">
               <img style="width: 230px;" src='.$thankyouimg.'>
               </div>
               <div class="" style="text-align: center;padding-bottom: 20px;">
               <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">'.$booking_msg1.' .</h5>
               <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">Please look below detail: </p>
               <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
               <li><strong>User Name: </strong>' . $name .'</li>
               <li><strong>Email: </strong>' . $to .'</li>
               <li><strong>Message: </strong>' . $booking->booking_message .'</li>
               </ul>
               </div>
               <div class="" style="text-align: center;padding-bottom: 10px">
               <a href="http://www.iebasketball.com/demo/yellow/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
               </div>
               <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
               <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
               </div>
               <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
               <div class="col-sm-6">
               <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
               <div style="float: left;width: 15%;margin-right: 20px;">
               <img style="width: 100%;" src='.$phone_img.'>
               </div>
               <div style="float: left;width: 70%;">
               <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
               <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
               </div>
               </div>
               </div>
               <div class="col-sm-6">
               <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
               <div style="float: left;width:15%;margin-right:20px;">
               <img style="width:100%" src='.$mail_icon.'>
               </div>
               <div style="float: left;width: 70%;">
               <a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
               <a style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
               </div>
               </div>
               </div>
               </div>
               </div>
               </div>
               </div>
               <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
               <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
               <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
               </div>
               </div>
               </div>
               </body>
               </html>';
               $subject ="Premission of Booking Service ";
               $headers = "From: " .$email . "\r\n";
               // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
               $headers .= "CC: dev2.bdpl@gmail.com\r\n";
               $headers .= "MIME-Version: 1.0\r\n";
               $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

               
               $mailsend = mail($to, $subject, $messageBody, $headers);
          
        }else{
           $booking_msg = "Sorry!!Somthing wrong.";
        }
        $result['status'] = $booking_status;
        $result['msg'] = $booking_msg;
        $result['html'] = $html;
        echo json_encode($result);
      }

    public function create()
    {

    }

    public function store(Request $request)
    {
    $email = $request->email;
    $user_info = DB::table('users')->where('email', $email)->first();
    $exist = DB::table('users')->where('email', $email)->count();

    if($exist>0){
    $name = $user_info->name;
    $password = $user_info->hd_password;
        $to = $email;
    $logo_url = url('/public').'/images/yellow_logo_new.png';
    $mail_bg_img = url('/public').'/images/bg.png';
    $email_icon = url('/public').'/images/email_icon_mail.png';
    $mail_icon = url('/public').'/images/mail.png';
    $phone_img = url('/public').'/images/phone1.png';
    $thankyouimg = url('/public').'/images/thankyou.png';
    $subject = "Forgot Password";
    $message    = '';
    $message = '<html>
    <head>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
    <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
    <div class="container">
    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
    <div class="" style="width: 500px;margin: 0 auto;">
    <div class="row">
    <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
    <img style="width:150px" src="'.$logo_url.'" alt="Logo">
    </div>
    <div class="" style="text-align: center;padding-bottom: 0px;">
    <img style="width:60px" src='.$email_icon.'>
    </div>
    <div class="" style="text-align: center;">
    <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span>'.$name.', </h6>
    </div>
    <div class="" style="text-align: center;padding: 15px 0px;">
    <img style="width: 230px;" src='.$thankyouimg.'>
    </div>
    <div class="" style="text-align: center;padding-bottom: 20px;">
    <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">Please note your email and password.</h5>
    <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">You need this every time you want to log on to the website to keep it safe and do not share it with anyone.</p>
    <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
    <li><strong>Name: ' . $name  .'</li>
    <li><strong>Email: ' . $email  .'</li>
    <li><strong>Password: ' . $password  .'</li>
    </ul>
    </div>
    <div class="" style="text-align: center;padding-bottom: 10px">
    <a href="http://www.iebasketball.com/demo/yellow/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
      </div>
    <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
    <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
    </div>
    <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
    <div class="col-sm-6">
    <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
    <div style="float: left;width: 15%;margin-right: 20px;">
    <img style="width: 100%;" src='.$phone_img.'>
    </div>
    <div style="float: left;width: 70%;">
    <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
    <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
    </div>
    </div>
    </div>
    <div class="col-sm-6">
    <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
    <div style="float: left;width:15%;margin-right:20px;">
    <img style="width:100%" src='.$mail_icon.'>
    </div>
    <div style="float: left;width: 70%;">
    <a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
    <a  style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
    <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
    <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
    </div>
    </div>
    </div>
    </body>
    </html>';

    $headers  = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: <NA VIZINHANCA>' . "\r\n";
    mail($to,$subject,$message,$headers,'-fg35.websitewelcome.com');


    $message = '';
    return redirect()->action(
    'ForgotpasswordController@index')->with('success','Thank You , please check your mail.');

}
else {
   return redirect()->action(
    'ForgotpasswordController@index')->with('error','There is no Google account with the info that you provided.');
}
}

    public function show($id)
    {

    }


    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }

public function update_account(Request $request) {

       $phone = $request->phone;
       $user_role = $request->user_role;
       $country = $request->country;
       $zipcode = $request->zipcode;
       $user_id = $request->user_id;
       $address_str = $request->address;
       $address = urlencode($address_str);
       $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=$address";
       $filestring = file_get_contents($url);
       $filearray = explode("\n", $filestring);
       $data = json_decode($filearray[0]);
       foreach($data as $k=>$v) {
       $view = $v->View;
       $view_count = count($view);
        if($view_count>0){
        foreach($view as $result_k=>$result_v) {
        $Result = $result_v->Result;
        $lat =  $Result[0]->Location->DisplayPosition->Latitude;
        $long =  $Result[0]->Location->DisplayPosition->Longitude;
        $PostalCode =  $Result[0]->Location->Address->PostalCode;
        $countryname =  $Result[0]->Location->Address->AdditionalData[0]->value;
        $cityname =  $Result[0]->Location->Address->AdditionalData[2]->value;
        if($lat!='' && $long!=''){
        $user_updated = DB::table('users')
                    ->where('id', $user_id)
                    ->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'country' => $countryname, 'city' => $cityname]);
        }
        }
        }
        else {
        return redirect('/dashboard')->with('error', 'Enter correct Address!');
        }
        }
        if($request->hasfile('user_images')) {
        foreach($request->file('user_images') as $images ) {
                $names = time() . '.' . $images->getClientOriginalName();
                $image_resizes = Image::make($images->getRealPath());
                $image_resizes->resize(480, 400);
                $image_resizes->save(public_path('images/userimages/' .$names));
                DB::table('user_gallery')->insert(
                    ['image_name' => $names, 'user_id' => $user_id]
                );
            }
         }
if($request->hasfile('profileimage')){
      $image = $request->file('profileimage');
      $name = time() . '.' . $image->getClientOriginalName();
      $image_resize = Image::make($image->getRealPath());
      $image_resize->resize(480, 400);
      $image_resize->save(public_path('images/profileimage/' .$name));
      $user_updated = DB::table('users')
            ->where('id', $user_id)
            ->update(['profile_image' => $name]);
}
     $user_updated = DB::table('users')
            ->where('id', $user_id)
            ->update(['phone' => $phone, 'address' => $address_str]);
       if($user_updated!=''){
               return redirect('/dashboard')->with('success', 'Profile updated!');
       }
       else {
        return redirect('/dashboard')->with('error', 'Profile Not updated!');
       }
}
 public function destroyNotification(Request $request)
    {
         $id=$request->id;
       
       $remove =  Notification::where('id', $id)
            ->update(['is_deleted' => '1']);
        if($remove!='')
        {
        echo 1;
      }
    }


}
