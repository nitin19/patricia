<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\Feedback;
use App\Models\Spavailability;
use App\Models\BlockProvider;

class FeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
   public function index()
    {
      try{
          if(!empty(Auth::user())){
            $get_user_bookings = DB::table('users')->leftjoin('booking', 'users.id','booking.profile_id')
            ->leftjoin('category','users.sub_id','=','category.cat_id')
            ->where('booking.user_id',Auth::user()->id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->orderBy('booking.booking_id','DESC')->paginate(5);

           $availablities = Spavailability::join('booking','booking.profile_id','=','spavailability.user_id')->where('booking.user_id',Auth::user()->id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->first();
             $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','feedback')->where('is_active', '1')->where('is_deleted', '0')->first();
          //$userblock = BlockProvider::where('taker_id','=',Auth::user()->id)->where('provider_id','=',$request->provider_id)->first();
           // return view('pages.single_users_feedback_list',compact('get_user_bookings','availablities'));
           return view('pages.feedback_list',compact('get_user_bookings','availablities','menu_meta_details'));

           
           }
           else{
             $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','contactus')->where('is_active', '1')->where('is_deleted', '0')->first();
             return view('pages.feedback',compact('menu_meta_details'));
           }
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
   }
   public function feedback()
    {
      try{ 
        $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','contactus')->where('is_active', '1')->where('is_deleted', '0')->first();
          return view('pages.feedback',compact('menu_meta_details'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
   }
   public function acc_to_users_feedback_list(Request $request)
    {
      try{
          if(!empty(Auth::user())){
            $get_user_bookings = DB::table('users')->leftjoin('booking', 'users.id','booking.profile_id')
            ->leftjoin('category','users.sub_id','=','category.cat_id')
            ->where('booking.user_id',Auth::user()->id)
            ->where('booking.profile_id',$request->provider_id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->paginate(5);
           $availablities = Spavailability::join('booking','booking.profile_id','=','spavailability.user_id')->where('booking.user_id',Auth::user()->id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->first();
          //$userblock = BlockProvider::where('taker_id','=',Auth::user()->id)->where('provider_id','=',$request->provider_id)->first();
           return view('pages.feedback_list',compact('get_user_bookings','availablities'));
           }
          
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
   }
   //distinct provider list  for booking
   public function distinct_provider_list() //not in use
   {
    if(!empty(Auth::user())){
            $get_user_bookings = DB::table('users')->leftjoin('booking', 'users.id','booking.profile_id')
            ->leftjoin('category','users.sub_id','=','category.cat_id')
            ->where('booking.user_id',Auth::user()->id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->groupBy('booking.profile_id')->paginate(5);

           $availablities = Spavailability::join('booking','booking.profile_id','=','spavailability.user_id')->where('booking.user_id',Auth::user()->id)
            ->where('booking.payment_confirm',1) ->where('booking.booking_verified',1)->first();
          //$userblock = BlockProvider::where('taker_id','=',Auth::user()->id)->where('provider_id','=',$request->provider_id)->first();
           // return view('pages.single_users_feedback_list',compact('get_user_bookings','availablities'));
           return view('pages.feedback_list',compact('get_user_bookings','availablities'));
           
           }
   }
   public function send(Request $request){
    try{

        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $phonenumber = $request->phonenumber;
        $comment = $request->comment;
        $fullname = $first_name .' '. $last_name;
        $comment_save = DB::table('conatct_admin')->insert(
          ['fullname' => $fullname, 'email' => $email, 'phone_number' => $phonenumber, 'message' => $comment, 'created_date' => date('Y-m-d H:i:s')]
        );
        $to = 'contato@navizinhanca.com';
        $logo_url = url('/public').'/images/yellow_logo_new.png';
        $mail_bg_img = url('/public').'/images/bg.png';
        $email_icon = url('/public').'/images/email_icon_mail.png';
        $mail_icon = url('/public').'/images/mail.png';
        $phone_img = url('/public').'/images/phone1.png';
        $thankyouimg = url('/public').'/images/thankyou.png';
        $message    = '';
        $message = '<html>
        <head>
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        </head>
        <body>
        <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
        <div class="container">
        <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
        <div class="" style="width: 500px;margin: 0 auto;">
        <div class="row">
        <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
        <img style="width:150px" src="'.$logo_url.'" alt="Logo">
        </div>
        <div class="" style="text-align: center;padding-bottom: 0px;">
        <img style="width:60px" src='.$email_icon.'>
        </div>
        <div class="" style="text-align: center;">
        <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey Admin</span>, </h6>
        </div>
        <div class="" style="text-align: center;padding: 15px 0px;">
        <img style="width: 230px;" src='.$thankyouimg.'>
        </div>
        <div class="" style="text-align: center;padding-bottom: 20px;">
        <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">You have received new user query.</h5>
        <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">Please look below detail: </p>
        <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
        <li><strong>First Name: </strong>' . $first_name  .'</li>
        <li><strong>Last Name: </strong>' . $last_name  .'</li>
        <li><strong>Email: </strong>' . $email  .'</li>
        <li><strong>Phone Number: </strong>' . $phonenumber  .'</li>
        <li><strong>Message: </strong>' . $comment  .'</li>
        </ul>
        </div>
        <div class="" style="text-align: center;padding-bottom: 10px">
        <a href="http://navizinhanca.com/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
        </div>
        <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
        <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
        </div>
        <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
        <div class="col-sm-6">
        <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
        <div style="float: left;width: 15%;margin-right: 20px;">
        <img style="width: 100%;" src='.$phone_img.'>
        </div>
        <div style="float: left;width: 70%;">
        <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
        <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
        </div>
        </div>
        </div>
        <div class="col-sm-6">
        <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
        <div style="float: left;width:15%;margin-right:20px;">
        <img style="width:100%" src='.$mail_icon.'>
        </div>
        <div style="float: left;width: 70%;">
        <a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
        <a  style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
        <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
        <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
        </div>
        </div>
        </div>
        </body>
        </html>';
            // echo $message;die;
            // $msg ="hiiiiii how are you";
        $subject ="contact";
        $headers = "From: " .$email  . "\r\n";
            // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
        $headers .= "CC: dev2.bdpl@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $mailsend = mail($to, $subject, $message, $headers);
            // $mailsend = mail($to,$subject,$message,$headers,'-fg35.websitewelcome.com');
        if($mailsend){
                // echo "hiii";die;
          return redirect()->action(
            'FeedbackController@index')->with('success','Recebemos sua mensagem. Muito obrigado pelo contato!');
        }
        else {
          return redirect()->action(
            'FeedbackController@index')->with('failure','Message Not, Please try again!');
        }
    }
    catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
  }
  public function feedback_list(){
    return view('pages.feedback_list');
  }
  public function create(Request $request){
    //echo "hello";die;
   // print_r($request->all());
    $request11 = array();

    $data = array('user_id'    => $request->user_id,
                  'provider_id'=> $request->provider_id,
                  'booking_id' => $request->booking_id,
                  'rating'     => $request->selected_rating,
                  'role'       => $request->role,
                  'message'    => $request->message );

    if(trim($request->role == 'provider')) {
      $rating = Feedback::where('provider_id','=',trim($request->provider_id))->where('booking_id','=',trim($request->booking_id))->where('user_id','=',trim($request->user_id))->where('role','=',trim($request->role))->count();
    } else {
      $rating = Feedback::where('user_id','=',trim($request->user_id))->where('booking_id','=',trim($request->booking_id))->where('provider_id','=',trim($request->provider_id))->where('role','=',trim($request->role))->count();
    }

    // $rating = Feedback::select('*')->where('user_id','=',$request->user_id)->where('booking_id','=',$request->booking_id)->first();

    //  echo"<pre>";
    // print_r($rating);die;

    if( $rating > 0 ){
         $request11['success'] = False;
         $request11['msg'] = 'Prestador de serviço já avaliado.';
        }else {

        $result = Feedback::insert($data);

        if($result){

            $rate = Feedback::where('provider_id', $request->provider_id)->where('approve_status', '1')->avg('rating');
            if($rate){
              $rate_val = round($rate,2);
              $user_rating = DB::table('users')
                       ->where('id', $request->provider_id)
                       ->update(['profile_rating' => $rate_val]);
            }
            $request11['success']= True;
            //$request11['msg'] = 'Contratante já avaliado.';
            $request11['msg'] = 'Prestador de serviço avaliado com sucesso. Aguarde análise para publicação.';
        } else {
            $request11['success']= False;
            $request11['msg']= 'something wrong. ';
        }
    }
    echo json_encode($request11);
  }


  /* fucntion name :load more $feedbacks
  parameter :id;
  work: load more feedback */
  public function load_more_feedbacks($id){
      $feedbacks = Feedback::join('users','users.id','=','feedback.user_id')->where('feedback.provider_id',$id)->where('feedback.approve_status',1)->paginate(5);

      $response = [];
      if(!empty($feedbacks)){
        $response['success'] = "true";
        $response['message']="feedback Fetched successfully";
        $response['info'] = $feedbacks;
        $response['count'] = count($feedbacks);

      }
      else{
        $response['success'] = "false";
        $response['message']="feedback not  Fetched ";
      }
      echo json_encode($response);
  }

  public function check_avability(Request $request){
      $profile_id = $request->profile_id;
      $datedayname = $request->datedayname;
      $get_avability = DB::table('spavailability')->where('user_id',$profile_id)->where('isCompleted','1')->where('status','1')->first();

      if($datedayname == 'day_monday' && $get_avability->day_monday == '0' && $get_avability->monday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_tuesday' && $get_avability->day_tuesday == '0' && $get_avability->tuesday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_wednesday' && $get_avability->day_wednesday == '0' && $get_avability->wednesday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_thursday' && $get_avability->day_thursday == '0' && $get_avability->thursday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_friday' && $get_avability->day_friday == '0' && $get_avability->friday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_saturday' && $get_avability->day_saturday == '0' && $get_avability->saturday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_sunday' && $get_avability->day_sunday == '0' && $get_avability->sunday_start == ' '){
        echo '0';
      } 
      elseif($datedayname == 'day_monday' && $get_avability->monday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_tuesday' && $get_avability->tuesday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_wednesday' && $get_avability->wednesday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_thursday' && $get_avability->thursday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_friday' && $get_avability->friday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_saturday' && $get_avability->saturday_start == NULL){
        echo '1';
      } 
      elseif($datedayname == 'day_sunday' && $get_avability->sunday_start == NULL){
        echo '1';
      } 
      else {
        echo '1';
      }

  }
}
