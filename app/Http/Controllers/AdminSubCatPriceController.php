<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
class AdminSubCatPriceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($subcat_id, Request $request)
    {

      $price_queries = DB::table('price_queries')->where('sub_id', $subcat_id)->get()->toArray();
      $categoryDetail = DB::table('category')->where('cat_id', '=', $subcat_id)->first();
      $mainCategoryDetail = DB::table('main_category')->where('id', '=', $categoryDetail->main_cat_id)->first();
      return view('admin.category_price.index', compact('price_queries', 'categoryDetail','mainCategoryDetail'));
    }

    public function save(Request $request)
    {
      $queries = trim($request->queries);
      $cat_id = trim($request->cat_id);
      $sub_id = trim($request->sub_id);
      $query_id = trim($request->query_id);

      if($query_id!=''){

        $chkUpQuery = DB::table('price_queries')->where('sub_id', '=', $sub_id)->where('id', '!=', $query_id)->where('queries', 'LIKE', '%'.$queries.'%')->count();

         if( $chkUpQuery > 0 ) {
          
          return redirect('admin/subcategory_price/'.$sub_id)->with('error','This Subcategory query is already exists.');
         
          } else {

            $newprice_queries = DB::table('price_queries')
                             ->where('id', $query_id)
                             ->update(['queries' => $queries]);
            $message = 'Category query updated successfully';

            return redirect('admin/subcategory_price/'.$sub_id)->with('success', $message);
         }

      } else {

        $count_queries = DB::table('price_queries')->where('cat_id',$cat_id)->where('sub_id',$sub_id)->get();
        if(count($count_queries) > 0){
            $priority = 0;
        } else {
            $priority = 1;
        }

        $chkInsQuery = DB::table('price_queries')->where('sub_id', '=', $sub_id)->where('queries', 'LIKE', '%'.$queries.'%')->count();

        if( $chkInsQuery > 0 ) {

           return redirect('admin/subcategory_price/'.$sub_id)->with('error','This Subcategory query is already exists.');
        
        } else {

          $newprice_queries = DB::table('price_queries')->insertGetId(['cat_id' => $cat_id, 'sub_id' => $sub_id, 'queries' => $queries, 'priority' => $priority]);
            $message = 'Subcategory query added successfully';

             if($newprice_queries){
                 return redirect('admin/subcategory_price/'.$sub_id)->with('success', $message);
              } else {
                return redirect('admin/subcategory_price/'.$sub_id)->with('error','Some problem occured. Please try again.');
              }

        }
        
      }

     
    }

    public function changepriority(Request $request){
      $price_queryid = $request->price_queryid;
      $price_query = DB::table('price_queries')->where('id', $price_queryid)->first();

      $updateusercards = DB::table('price_queries')
       ->where('cat_id', '=', $price_query->cat_id)
       ->where('sub_id', '=', $price_query->sub_id)
       ->where('id', '!=', $price_queryid)
       ->update(['priority' => '0']);

       $updateusercards = DB::table('price_queries')
       ->where('id', $price_queryid)
       ->update(['priority' => '1']);

    }

    public function deletesubcat(Request $request){
      $price_queryid = $request->price_queryid;
      $chkQuery = DB::table('provider_price_list')->where('query_id', '=', $price_queryid)->count();
      if( $chkQuery > 0 ) {
        echo '0';
      } else {
         DB::table("price_queries")->delete($price_queryid);
         echo '1';
      }

    }
    
}