<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminorderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
      try{
          /* Count */
              $order_count = DB::table('booking')
                             ->where('is_active', '1')
                             ->where('is_deleted', '0')
                             ->count();
            /* Count */
            /* Listing */ 
             $booking = DB::table('booking')
                       ->where('is_active', '1')
                       ->where('is_deleted', '0')
                       ->orderBy('booking_id', 'DESC')
                       ->get();  
            /* Listing */
          return view('admin.orders', compact('order_count', 'booking'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     /* approved address by admin */
     public function approved_address(Request $request){
        try{
           $booking_id = $request->booking_id;
           $status = $request->approve_status;
           $booking_updated = DB::table('booking')
              ->where('booking_id', $booking_id)
              ->update(['booking_verified' =>$request->approve_status]);
          $booking = DB::table('booking')
              ->where('booking_id', $booking_id)
              ->select('*')
              ->first();
          $bookingUser = DB::table('users')
              ->where('id', $booking->user_id)
              ->select('*')
              ->first();
          $bookingProvider = DB::table('users')
              ->where('id', $booking->profile_id)
              ->select('email')
              ->first();
          if($status =='1')
          {
            $booking_msg = "Congratulations!!Your request for booking service was confirmed.";
          }else{
             $booking_msg = "Sorry!!Your request for booking service was Rejected.";
          }
           
            if($booking_updated!=''){
                 $fullname = $bookingUser->name;
                 $email = $bookingUser->email;
                 //$to = 'dev2.bdpl@gmail.com';
                 $to = $bookingUser->email;
                 $logo_url = url('/public').'/images/yellow_logo_new.png';
                 $mail_bg_img = url('/public').'/images/bg.png';
                 $email_icon = url('/public').'/images/email_icon_mail.png';
                 $mail_icon = url('/public').'/images/mail.png';
                 $phone_img = url('/public').'/images/phone1.png';
                 $thankyouimg = url('/public').'/images/thankyou.png';
                 $messageBody = '';
                 $messageBody = '<html>
                 <head>
                 <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                 </head>
                 <body>
                 <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                 <div class="container">
                 <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                 <div class="" style="width: 500px;margin: 0 auto;">
                 <div class="row">
                 <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                 <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                 </div>
                 <div class="" style="text-align: center;padding-bottom: 0px;">
                 <img style="width:60px" src='.$email_icon.'>
                 </div>
                 <div class="" style="text-align: center;">
                 <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0"></span>, </h6>
                 </div>
                 <div class="" style="text-align: center;padding: 15px 0px;">
                 <img style="width: 230px;" src='.$thankyouimg.'>
                 </div>
                 <div class="" style="text-align: center;padding-bottom: 20px;">
                 <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">'.$booking_msg.' .</h5>
                 <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">Please look below detail: </p>
                 <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
                 <li><strong>User Name: </strong>' . $fullname .'</li>
                 <li><strong>Email: </strong>' . $email .'</li>
                 <li><strong>Message: </strong>' . $booking->booking_message .'</li>
                 </ul>
                 </div>
                 <div class="" style="text-align: center;padding-bottom: 10px">
                 <a href="http://www.iebasketball.com/demo/yellow/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                 </div>
                 <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                 <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                 </div>
                 <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                 <div class="col-sm-6">
                 <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                 <div style="float: left;width: 15%;margin-right: 20px;">
                 <img style="width: 100%;" src='.$phone_img.'>
                 </div>
                 <div style="float: left;width: 70%;">
                 <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
                 <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                 </div>
                 </div>
                 </div>
                 <div class="col-sm-6">
                 <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                 <div style="float: left;width:15%;margin-right:20px;">
                 <img style="width:100%" src='.$mail_icon.'>
                 </div>
                 <div style="float: left;width: 70%;">
                 <a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
                 <a style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
                 <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
                 <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                 </div>
                 </div>
                 </div>
                 </body>
                 </html>';
                 $subject ="Premission of Booking Service ";
                 $headers = "From: " .$email . "\r\n";
                 // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
                 $headers .= "CC: ".$bookingProvider->email."\r\n";
                 $headers .= "MIME-Version: 1.0\r\n";
                 $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

                 
                 $mailsend = mail($to, $subject, $messageBody, $headers);
                 return redirect('/admin-orders')->with('success', 'Booking Status updated!');
            }
            else {
           return redirect('/admin-orders')->with('error', 'Booking Status Not updated!');
         } 
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        } 
     }
    public function excel_export(Request $request)
    {

       $booking = DB::table('booking')
                       ->select('category as Service_name','user_id as Username','user_id as Email','booking_start_time as Start_Date','booking_amount as Amount','booking_verified as Status')
                       ->where('is_active', '1')
                       ->where('is_deleted', '0')
                       ->orderBy('booking_id', 'DESC')
                       ->get()->toArray();

        $exceldata = array();
        if(count($booking) > 0){
            $booking = json_decode(json_encode($booking), true );
            foreach($booking as $books){
                $username = DB::table('users')
                    ->select('name','email')
                    ->where('users.id', '=', $books['Username'])->first();
                if($books['Status']=='1'){
                    $status = 'Approved';
                }elseif($books['Status']=='2') {
                    $status = 'Denied';
                } else {
                    $status = 'Pending';
                }
               

               $books['Username'] = $username->name;
               $books['Email'] = $username->email; 
               $books['Status'] = $status;
               $books['Amount'] = str_replace(',', '.', $books['Amount']);
               array_push( $exceldata,  $books);
            }
        }
    
        //echo "<pre>"; print_r($booking);die;
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
              
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                     //print_r($row);die;
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
}