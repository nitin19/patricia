<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorfiveController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('pages.500error');
    }
}
?>