<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Support\Facades\Route;
class AdminMainCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     *display all Data of the subcateory table
     *
     * @return void
     */
    
    public function index(Request $request)
    {
        try{
            //     /* Count */
            //   $allRoutes = Route::getRoutes()->get();
            //     print_r($allRoutes);die;
                $main = DB::table('main_category')->where('is_deleted','=','0')->get();
                // echo "<pre>";
                // print_r($main);die;

               // $main = DB::table('main_category')->where('is_deleted','=','0')->get();
                 return view('admin.main_category.index',compact('main'));    
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }          
    }
      /**
     *create new subcategory
     *
     * @return void
     */
    public function create(Request $request){
        try{
            return view('admin.main_category.create');
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
      /**
     *stores new subcategory
     *parms:request
     * @return void
     */
    public function store(Request $request)
    {
        try{
          $name=$request->service_name;
          $main_cat_data = DB::table('main_category')->where('name',$name)->where('is_deleted','0')->first();
          if($main_cat_data) { 
              return redirect('/admin/main-category/create')->with('error', 'Category Already Exists!');
          } else {
              if($request->hasfile('service_img')){
              $image = $request->file('service_img');
              $cat_img = time() . '.' . $image->getClientOriginalName(); 
              $image_resize = Image::make($image->getRealPath());
              $image_resize->resize(40, 40);
              $image_resize->save(public_path('images/main_category/' .$cat_img));
              $category = DB::table('main_category')->insert(['name'=>$name,'cat_image' => $cat_img,'created_by'=>Auth::user()->id,'updated_by'=>Auth::user()->id,'is_active'=>1,'is_deleted'=>0]);
                 } else{
                   $category= DB::table('main_category')->insert(['name' => $name,'created_by'=>Auth::user()->id,'updated_by'=>Auth::user()->id,'is_active'=>1,'is_deleted'=>0]);
                 }
           
                //$category = DB::table('main_category')->insert(['name'=>$request->service_name,'cat_image' => $request->name,'created_by'=>Auth::user()->id,'updated_by'=>Auth::user()->id,'is_active'=>1,'is_deleted'=>0]);
                if($category){
                   return redirect('/admin-services/create')->with('success', 'Category saved successfully.Now Please Add Subcategories.');
                }
                 else{
                   return redirect('/admin/main-category')->with('error', 'Category not  saved ');
                }
          }
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }
      /**
     *edit existing subcategory
     *parms:id
     * @return void
     */
    public function edit($id){
        try{
            $main_cat = DB::table('main_category')->where('id',$id)->first();
           // print_r($main_cat);
             return view('admin.main_category.edit',compact('main_cat','id'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
      /**
     *update edited subcategory
     *
     * @return void
     */
    public function update(Request $request)
    {
        try{
            $id= $request->id;
            $cat_img = $request->service_img;
            $name = $request->service_name;
            if($request->hasfile('service_img'))
            {
                $image = $request->file('service_img');
                $cat_image = time() . '.' . $image->getClientOriginalName(); 
                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(40, 40);
                $image_resize->save(public_path('images/main_category/' .$cat_image));
                $update_category = DB::table('main_category')->where('id',$id)->update(['name'=>$name,'cat_image'=>$cat_image]);
            }
            else
            {
                $update_category = DB::table('main_category')
                ->where('id', $id)
                ->update(['name' => $name]);
            }
        //print_r($update_category);die;
           if($update_category){
               
               return redirect('/admin/main-category/')->with('success', 'Category updated successfully');
            }
             else{
              
               return redirect('/admin/main-category/')->with('error', 'Category not updated ');
            }
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
      /**
     *show  subcategory by id
     *
     * @return void
     */

    public function show(Request $request)
    {
        try{
            $main = DB::table('main_category')->get();
            return view('admin.main_category.index',compact('main'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     /**
     *show  subcategory by id
     *
     * @return void
     */
    public function destroy(Request $request)
    {
        // echo "fyfgh";die;
        $id=$request->id;
        // $remove_cat = DB::table('main_category')->where('id',$id)->delete();
        $checkCategory = DB::table('users')->where('cat_id', '=', $id)->count();
      if( $checkCategory > 0 ) {
        echo '0';
            // $response['success'] = "false";
            // $response['message'] = "failure";
            // $response= "Category Not deleted";
      }
      else
      {
        $remove_cat =  DB::table('main_category')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);
        if($remove_cat!='')
        {
            $remove_subcat =  DB::table('category')
            ->where('main_cat_id', $id)
            ->update(['is_deleted' => 1]);
            if($remove_subcat){
                echo '1';
            } else {
                echo '0';
            }
          
        }
      }
    }
}

