<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\Chat;
use Image;
use Validator;
use DB;
use Hash;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Cache;

class ChatController extends Controller
{
    public function userchat(Request $request, $id)
    { 
        try{
            $receiver_id = $id;
            $current_userid = Auth::user()->id;
            $frndInfodata = User::where('user_role', '=', null)->where('id','=', $current_userid)->where('is_active','=','1')->where('is_deleted','=','0')->first();
               if(!empty($frndInfodata)){
                $frndInfo_id = $frndInfodata->id;
               }
            $recentchat = DB::table('chathistory')->where('deleted', '=', 0)->where(
                function($query) use ($current_userid){
                        $query->where('sender_id', '=', $current_userid);
                        $query->orWhere('receiver_id', '=', $current_userid);
                    })->orderby('id', 'desc')->first();

            /*$get_friend_list = DB::table('chathistory')
                        ->join('users','users.id','=','chathistory.receiver_id')
                        ->where('users.is_active','=','1')
                        ->where('users.is_deleted','=','0')
                        ->where('chathistory.sender_id','=',$current_userid)
                        ->groupBy('chathistory.receiver_id')
                        ->orderBy('chathistory.receiver_id','desc')
                        ->get()->toArray();*/

            $get_friend_list = DB::table('chathistory')
                        //->select('usender.name as senderName', 'ureceiver.name as revicerName')
                        ->join('users as usender','usender.id','=','chathistory.sender_id')
                        ->join('users as ureceiver','ureceiver.id','=','chathistory.receiver_id')
                        ->where('usender.is_active','=','1')
                        ->where('ureceiver.is_active','=','1')
                        ->where('usender.is_deleted','=','0')
                        ->where('ureceiver.is_deleted','=','0')
                        ->where(function($query) use ($current_userid){
                            $query->where('chathistory.sender_id', '=', $current_userid);
                            $query->orWhere('chathistory.receiver_id', '=', $current_userid);
                        })
                        ->groupBy('chathistory.receiver_id')
                        ->orderBy('chathistory.receiver_id','desc')
                        ->get()->toArray();

            /*$get_friend_list = DB::table('chathistory')->join('users','users.id','=','chathistory.receiver_id')->where('users.is_deleted',0)->where('users.is_active',1)->where(
                function($query) use ($current_userid){
                        $query->where('receiver_id', '=', $current_userid);
                        $query->orWhere('sender_id', '=', $current_userid);
                    })->groupBy('chathistory.receiver_id')->orderby('chathistory.receiver_id', 'desc')->get()->toArray();*/

            
            return view('chat.userchat',compact('current_userid', 'frndInfo_id', 'recentchat', 'receiver_id', 'get_friend_list'));
        } catch(\Illuminate\Database\QueryException $e) {
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        } catch(\Exception $e) { 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

    public function saveuserChat(Request $request)
    { 
        try{
            $recentchat = DB::table('chathistory')->where('deleted', '=', 0)->orderby('id', 'desc')->first();
            $friend_request_id = trim($request->friend_request_id);
            if( $request->sender_id == 1 ){
                $id_to_update = $request->receiver_id;
            }else{
                $id_to_update = $request->sender_id;
            }
            $updatechats = DB::table('users')->where('id', '=', trim($id_to_update))
                 ->update(['last_message_to_admin' => date('Y-m-d h:i:s')]);
            $senderdata = DB::table('users')->where('id', '=', trim($id_to_update))->where('is_active','=','1')->where('is_deleted','=','0')->first();
            $chat = new Chat;
            $chat->sender_id = trim($request->sender_id);
            $chat->receiver_id = trim($request->receiver_id);
            $chat->message = trim($request->message);
            $chat->save();
            $insertedId = $chat->id;
            $sender_profile_image = $senderdata->profile_image;
            $sender_name = $senderdata->name;
            $sender_profile_image_url = url('/public')."/images/profileimage/".$sender_profile_image;
            $sender_profile_url = url('/')."userchat/".$request->receiver_id;
            
            if($insertedId) {
                if(Cache::has('user-is-online-' .$request->receiver_id)){
                    
                } else {
                    $to = 'dev1.bdpl@gmail.com';
                    $from = 'patricia@email.com';
                    $subject = 'Chat message conformation';
                    $message = '<html><body>';
                    $message .= '<img src="'.$sender_profile_image_url.'" alt="" />';
                    $message .= '<p style="color:#080;font-size:14px;">'.$sender_name.'</p>';
                    $message .= '</body></html>';
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: '.$from."\r\n" . 'Reply-To: '.$from."\r\n" . 'X-Mailer: PHP/' . phpversion();
                    mail($to,$subject,$message,$headers);
                }
                if($request->hasfile('attachments')){
                    foreach($request->file('attachments') as $file){
                        $name=rand(1, 999).$file->getClientOriginalName();
                        $file->move(public_path().'/chat/', $name);  
                        $newmedia = DB::table('attachments')->insert(['chat_id'=> $insertedId,'name' => $name]);
                    }
                 }
                echo json_encode(array('statusCode'=>"1",'msg'=>$request->message));
            } else {
                echo json_encode(array('statusCode'=>"0",'msg'=>'Error occured!'));
            }
        } catch(\Illuminate\Database\QueryException $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        } catch(\Exception $e) { 
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        } 
    }

    public function getusersChat($senderid,$receiverid)
    {
        $current_userId = Auth::id();
        $chatData = Chat::where('sender_id','=',$senderid)->where('receiver_id','=',$receiverid)->orWhere('sender_id','=',$receiverid)->where('receiver_id','=',$senderid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();
        $Html  = '';
        if (count($chatData) > 0) {
            foreach ($chatData as $chat) {
                $chatattachments = DB::table('attachments')
                           ->where('chat_id', '=', $chat['id'])
                           ->where('deleted', 0)
                           ->get()->toArray();
                $time = strtotime($chat['created_at']);
              if ($current_userId == $chat['sender_id']) {
                $senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();
                
                $Html.= '<li class="self">';
                if($senderInfo->profile_image!='') {
                 $Html.= '<div class="avatar"><img src="'.url("/public").'/images/profileimage/'.$senderInfo->profile_image.'" draggable="false"/></div>';
                } else {
                 $Html.= '<div class="avatar"><img src="'.url("/public").'/images/profileimage/1542707849.images.png" draggable="false"/></div>';
                }
                $Html.= '<div class="msg">';
                $Html.=  '<p>'.$chat['message'].'</p>';
                if(count($chatattachments) > 0){
                    foreach($chatattachments as $chatattachment){
                        $ext = pathinfo($chatattachment->name, PATHINFO_EXTENSION);
                        if($ext=='doc' || $ext=='docx'){
                          $attachmenturl = url('/public').'/images/doc_word_img.jpg';
                          $file_format = '<img class="doc_file_image" src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false">';
                        } elseif($ext=='zip'){
                          $attachmenturl = url('/public').'/images/zipped_file.jpeg';
                          $file_format = '<img class="zip_file_image" src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false">';  
                        } elseif($ext=='docs'){
                          $attachmenturl = url('/public').'/images/docs_file_img.jpg';   
                          $file_format = '<img class="docs_file_image" src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false">'; 
                        } elseif($ext=='xlsx' || $ext=='xls') {
                          $attachmenturl = url('/public').'/images/xlsx.png';
                          $file_format = '<img class="xlsx_file_image" src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false">';  
                        } else {
                          $attachmenturl = url('/public').'/chat/'.$chatattachment->name;
                          $file_format = '<img src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false">';
                        }
                $Html.= '<a href="'.$attachmenturl.'" title="'.$chatattachment->name.'" style="margin:0px 5px;" target="_blank">'.$file_format.'</a>';
                    }
                }
                $Html.=  '<time>'.$this->humanTiming($time).' ago</time>';
                $Html.= '</div>';
                $Html.= '</li>';
              } else {

                $receiverInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="other">';
                if($receiverInfo->profile_image!='') {
                    $Html.= '<div class="avatar"><img src="'.url("/public").'/images/profileimage/'.$receiverInfo->profile_image.'" draggable="false" alt="User Avatar"/></div>';
                } else {
                    $Html.= '<div class="avatar"><img src="'.url("/public").'/images/profileimage/1542707849.images.png" draggable="false" alt="User Avatar"/></div>';
                }
                $Html.= '<div class="msg">';
                $Html.='<p>'.$chat['message'].'</p>';
                if(count($chatattachments) > 0){
                    foreach($chatattachments as $chatattachment){
                        $attachmenturl = url('public').'/chat/'.$chatattachment->name;
                        $Html.= '<a href="'.$attachmenturl.'" title="'.$chatattachment->name.'" style="margin:0px 5px;" target="_blank"><img src="'.$attachmenturl.'" alt="'.$chatattachment->name.'" draggable="false"></a>';
                    }
                }
                $Html.=  '<time>'.$this->humanTiming($time).' ago</time>';
                $Html.= '</div>';
                $Html.= '</li>';
              }
            }

            echo $Html;

        }  else {
            echo '<li class="left clearfix">
                    <div class="chat-body clearfix">
                        <p>
                            Escreva no campo abaixo para iniciar a conversa.
                        </p>
                    </div>
                </li>';
        }
        echo '<input type="hidden" class="current_user_message_count_'.$receiverid.'" value="'. count($chatData) .'" />';
    }
    public function getusersChatCount()
    {
        $current_userId = Auth::id();
        $chatData = Chat::where('receiver_id','=',$current_userId)
                    ->where(['status'=>'1','is_read'=>'0'])
                    ->get()->toArray();
        if (count($chatData) > 0) {
           echo'<i class="far fa-envelope"></i><span class="top-bud yellow" id="msgid">'.count($chatData).'</span>';
        }  else {
            echo'<i class="far fa-envelope"></i>';
        }
    }
    function humanTiming ($time) {
        $time = time() - $time; 
        $time = ($time<1)? 1 : $time;
        $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
        );
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }
    }
    public function updateunreaduser(Request $request){
        $sender_id = $request->sender_id;
        $receiver_id = $request->receiver_id;
        $updatechats = DB::table('chathistory')->where('receiver_id', '=', $sender_id)
                 ->where('sender_id', '=', $receiver_id)
                 ->update(['is_read' => '1', 'updated_at' => date('Y-m-d h:i:s')]);
    }
    
    public function user_status_check($receiverid){
        if(Cache::has('user-is-online-' .$receiverid)){
            echo 'online';
        } else {
            echo 'offline';
        }
    }
    
    public function friend_list_user_status_check($receiverid){
        if(Cache::has('user-is-online-' .$receiverid)){
            echo 'online';
        } else {
            echo 'offline';
        }
    }

    public function ajaxfriendlist(Request $request){
        $current_userid = Auth::user()->id;
        $unreadchats = DB::table('chathistory')->groupBy('sender_id')
                        ->selectRaw('count(is_read) as unreadcount, GROUP_CONCAT(sender_id) as sender_id')
                        ->where('receiver_id', '=', $current_userid)
                        ->where('is_read', '=', 0)
                        ->where('deleted', '=', 0)
                        ->get()->toArray();

        if(count($unreadchats) > 0){
            $newarray = array();
            foreach($unreadchats as $unreadchat){
                    $senderid = current(explode(',', $unreadchat->sender_id));
                    $senderdata = DB::table('users')->where('id', '=', $senderid)->first();
                    array_push($newarray ,array('chat' => $unreadchat->unreadcount, 'sender_id' => $senderid, 'name' => $senderdata->name));
            }
        } else {
            $newarray = array();
        }
        $response = [
            'unreadchats'    => $newarray,
           ];
        return response()->json($response);
    } 

    public function ajaxfriendlistrefresh(){
        $current_userid = Auth::user()->id;
        $get_friend_list = DB::table('chathistory')
                        ->join('users','users.id','=','chathistory.receiver_id')
                        ->where('users.is_active','=','1')
                        ->where('users.is_deleted','=','0')
                        ->where('chathistory.sender_id','=',$current_userid)
                        ->groupBy('chathistory.receiver_id')
                        ->orderBy('chathistory.receiver_id','desc')
                        ->get()->toArray();
       
        if(count($get_friend_list) > 0){
            foreach($get_friend_list as $get_friend_lists){
                $get_friend_last_msg = DB::table('chathistory')
                    ->where('sender_id', $get_friend_lists->receiver_id)
                    ->limit(1)
                    ->orderBy('id','desc')
                    ->get()->toArray();
                //$time = $get_friend_last_msg[0]->updated_at;
                $newtime = date("H:i",strtotime($get_friend_last_msg[0]->updated_at));
                /*$get_unread_message = DB::table('chathistory')
                    ->where('receiver_id','=', Auth::user()->id)
                    ->where('is_read', '=', '0')
                    ->get()->toArray();*/
                $get_unread_message = DB::table('chathistory')
                        ->where('receiver_id', '=', Auth::user()->id)
                        ->where('sender_id', '=', $get_friend_lists->receiver_id)
                        ->where('is_read', '=', 0)
                        ->where('deleted', '=', 0)
                        ->count();
                            
            ?>
                <a href="<?php echo url('/');?>/userchat/<?php echo $get_friend_lists->receiver_id;?>">
                    <li class="single_friend_list img_cont">
                        <?php if($get_friend_lists->profile_image!=''){ ?>
                            <img id="profile-img" src="<?php echo url('/public');?>/images/profileimage/<?php echo $get_friend_lists->profile_image;?>" class="online profilpic img-circle user_img" alt="" draggable="false">
                            <?php if(Cache::has('user-is-online-' .$get_friend_lists->receiver_id)){ ?>
                            <span class="online_icon"></span>
                            <?php } else { ?>
                                <span></span>
                            <?php } ?>
                            <span class="last_message_time"><?php echo $newtime; ?></span>
                        <?php } else { ?>
                            <img id="profile-img" src="<?php echo url('/public');?>/images/profileimage/1542707849.images.png" class="online profilpic img-circle" alt="">
                            <?php if(Cache::has('user-is-online-' .$get_friend_lists->receiver_id)){ ?>
                            <span class="online_icon"></span>
                            <?php } else { ?>
                                <span></span>
                            <?php } ?>
                            <span class="last_message_time"><?php echo $newtime; ?></span>
                        <?php } ?>
                        <span class="friend_name"><?php echo $get_friend_lists->name;?></span>
                        <span class="last_msg"><?php echo $get_friend_last_msg[0]->message;?></span>
                        <span class="unreadmsgcount" id="countunread{{$get_friend_lists->id}}">
                            <?php if(count($get_unread_message) > 0){ ?><span class="badge badge-secondary unreadcount">
                                <?php echo count($get_unread_message);?>
                            </span>
                            <?php } ?>
                        </span>
                    </li>
                </a>  
            <?php 
            } 
        } 
    }
}
