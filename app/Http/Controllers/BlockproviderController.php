<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\BlockProvider;

class BlockproviderController extends Controller
{
    public function create(Request $request){
    $request11 = array();

    $data = array('taker_id'    => $request->taker_id,
                  'provider_id'=> $request->provider_id,
                  'block_status' => $request->block_status,
                   );
     $userblock = BlockProvider::where('taker_id','=',$request->taker_id)->where('provider_id','=',$request->provider_id)->first();
    if($userblock != ''){  
      if($request->block_status == '1')
      {
        $ublock = '2';
      } else {
        $ublock = '1';
      }
      $unblocked =   BlockProvider::where('taker_id','=',$request->taker_id)->where('provider_id','=',$request->provider_id)->update(['block_status'=>$ublock]);
      if($unblocked)
      {
        if($request->block_status == '2') {
          $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',Auth::user()->id)->count();
          if( $check_exist > 0 ) {
            $remove_whish = DB::table('wish_list')
                          ->where('user_id', Auth::user()->id)
                          ->where('wish_profile_id', $request->provider_id)
                          ->update(['is_active' => '1', 'is_deleted' => '0']);
          } 
          $request11['success'] = True;
          $request11['blkstatus'] = '1';
          $request11['msg'] = 'O prestador de servico foi bloqueado e não aparecerá mais no seu mapa';
        }
        else{
          $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',Auth::user()->id)->count();
          if( $check_exist > 0 ) {
            $remove_whish = DB::table('wish_list')
                          ->where('user_id', Auth::user()->id)
                          ->where('wish_profile_id', $request->provider_id)
                          ->update(['is_active' => '0', 'is_deleted' => '1']);
          }  
          $request11['success'] = True;
          $request11['blkstatus'] = '2';
          $request11['msg'] = 'O prestador de servico foi desbloqueado e voltará a aparecer no seu mapa';
        }
      } else {
        $request11['success']= False;
        $request11['msg']= 'something wrong. ';
      }
    }else {
      $result = BlockProvider::insert($data);
      if($result){
        $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',Auth::user()->id)->count();
        if( $check_exist > 0 ) {
          $remove_whish = DB::table('wish_list')
                        ->where('user_id', Auth::user()->id)
                        ->where('wish_profile_id', $request->provider_id)
                        ->update(['is_active' => '0', 'is_deleted' => '1']);
        }    
        $request11['success']= True;
        $request11['blkstatus'] = '1';
        $request11['msg'] = 'Prestador de serviço bloqueado.';
      } else {
        $request11['success']= False;
        $request11['msg']= 'something wrong. ';
        }
    }
    echo json_encode($request11);
  }
}
