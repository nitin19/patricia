<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Feedback;
use App\Models\Spavailability;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use IntlDateFormatter;
use DateTime;
use Carbon\Carbon; 
use App\Errorlogs;
use Session;
use DB;
use Image;
use URL;
use Log;

class NotificationController extends Controller {

 
  public function taker_notifications(Request $request) {
    //echo "hello";die;
    $i=0;
    $rules = [
       'user_id' => 'required',
    ];

   $messages = [
       'user_id.required'   =>  'User id is missing.',
    ];

  $validator = Validator::make($request->all(), $rules);

  if($validator->fails()){

        return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
        }
     
       $profile_id = trim($request->user_id);
       if($profile_id){
       
        $is_deleted = array('0','1');
        $read_status = array('0');
        $notificaton_type = array('booking','document');
        $notificationlist = Notification::select('notifications.*','notifications.id as notify_id','notifications.created_at as notify_time','booking.booking_message','booking.booking_verified','booking.profile_id','booking.booking_time','user_documents.document_status')
       
          ->leftjoin('booking', 'notifications.type_id', '=', 'booking.booking_id')
          ->leftjoin('user_documents', 'notifications.type_id', '=','user_documents.id')
          ->whereIn('notifications.is_deleted',$is_deleted)
          ->whereIn('notifications.notificaton_type',$notificaton_type)
          ->where('notifications.user_id', $profile_id)
          ->where('notifications.notification_role', 'Provider')
          ->whereNotIn('notifications.read_status',$read_status)
          ->orderBy('notifications.created_at', 'desc')
          //->get()->toArray();
          ->simplePaginate(5);
          if($notificationlist){
             foreach($notificationlist as $notification){
                $fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                );
                $dt = new DateTime($notification->created_at. "+1 days");
                $d = $dt->format('d');
                $m = $dt->format('m');
                $y = $dt->format('Y');
                $time = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                if($notification->notificaton_type ='booking'){
                $userinfo = DB::table('users')->select('name as provider_name','profile_image as provider_image')->where('id', '=', $notification->profile_id)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->first();
                if($notification->booking_verified == '1'){
                   $msg = "Parabéns! Solicitação de serviço confirmada.";
                 } else {
                   $msg = "Solicitação de serviço negada.";
                 }
               
              }else{
                $userinfo = DB::table('users')->select('name as provider_name','profile_image as provider_image')->where('id', '=',$notification->user_id)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->first();
                if($notification->document_status == '1') {
                  $msg = "Parabéns! Documento é aprovado.";
                } else {
                  $msg = "Desculpa! seu documento não foi aprovado.";
                }
                
               }
                $notificationlist[$i]->provider_name = $userinfo->provider_name;
                $notificationlist[$i]->provider_image = $userinfo->provider_image;
                $notificationlist[$i]->notify_time = $time;
                $notificationlist[$i]->notify_message = $msg;
                
                 $i++;
             }
          }
      
          return response()->json([
                'status' => true,
                'message' => 'Notification details.',
                'data' => $notificationlist,
            ], 200);
        } else {
          return response()->json([
                'status' => false,
                'message' => 'Notification list not found.',
                'data' => '',
            ], 404);
        }
  }
  
}