<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use URL;
use Log;
use DB;
use Session;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

// DB::enableQueryLog();  
// dd(DB::getQueryLog()); exit;

class ContactController extends Controller {

  public function sendEmail(Request $request) {

        $Input = $request->all();
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $phonenumber = $request->phonenumber;
        $comment = $request->comment;
        $fullname = $first_name .' '. $last_name;
        $comment_save = DB::table('conatct_admin')->insert(
        ['fullname' => $fullname, 'email' => $email, 'phone_number' => $phonenumber, 'message' => $comment]
        );
        $to = 'contato@navizinhanca.com';
        $logo_url = url('/public').'/images/logo.png';
        $mail_bg_img = url('/public').'/images/bg.jpg';
        $email_icon = url('/public').'/images/email_icon_mail.png';
        $LI_png = url('/public').'/images/LI.png';
        $fb_png = url('/public').'/images/fb.png';
        $insta_png = url('/public').'/images/insta.png';
      //  $message = '';
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns:v="urn:schemas-microsoft-com:vml">
          <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
              <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
              <!--[if !mso]><!-- -->
              <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
              <!-- <![endif]-->
              <title>Email Template</title>
              <style type="text/css">
              * {font-family: "Lato", sans-serif;}
              .ii a[href] { text-decoration: none!important; }
              </style>
          </head>
        <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" >
                <tr>
                    <td align="center">
                        <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                        <tr>
                                            <td align="center" height="70" style="height:70px;">
                                                <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <hr>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                <tr>
                    <td align="center">
                        <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                            <tr>
                                <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                    <div style="line-height: 35px">
                                        <span style="color: #1497d5;">Nova mensagem recebida:</span>
                                    </div>
                                    <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
                                 
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="hide">
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
            </table>
            <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                <tr>
                    <td align="center">
                        <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                            <tr>
                                <td height="25">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" width="450" align="center" cellpadding="0" cellspacing="0" class="container580">
                                        <tr>
                                            <td width="30">&nbsp;</td>
                                            <td align="center" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                                             
                                                <div  style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;">
                                                    <b style="color: #1497d5;
                                                    padding-right: 5px;">Nome:</b>' . $first_name .'
                                                </div>
                                                <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Sobrenome:</b>  ' . $last_name .'
                                                </div>
                                                 <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">E-mail:</b>  ' . $email .'
                                                </div>
                                                 <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Celular:</b>  ' . $phonenumber .'
                                                </div>
                                                 <div style="color: #0c3e64;
                                                    font-size: 19px;
                                                    font-weight: 700;"><b style="color: #1497d5;
                                                    padding-right: 5px;">Ver mensagem:</b>  ' . $comment .'
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="30">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                </tr>
                <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" > 
                    <tr>
                        <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                <tr>
                                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                        <div style="line-height: 26px;">
                                            <a href="https://www.navizinhanca.com" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Comece sua jornada</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="70">&nbsp;</td>  
                                </tr>
                                    </table>
                                </td>
                            </tr>
                </table>
            </table>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                            <tr> 
                                <td>
                                    <div class="social-icons" style="text-align: center;">
                                        <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                            <img src="'.$insta_png.'">
                                        </a>
                                        <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                            <img src="'.$LI_png.'">
                                        </a>
                                        <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                            <img src="'.$fb_png.'">
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
            </table>
            </table>
          </body>
        </html>';
         
         
          $subject ="NaVizinhança: Formulário de contato";
          $headers = "From: " .$email  . "\r\n";
          // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
         // $headers .= "CC: dev1.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $message, $headers,'-faaaa@abc.com');
          // $mailsend = mail($to,$subject,$message,$headers,'-fg35.websitewelcome.com');
          if($mailsend){
            
            return response()->json([
                'status' => true,
                'message' => 'Message sent successfully.',
                'data' => $Input,
            ], 200);

          } else {

            return response()->json([
                'status' => false,
                'message' => 'Message Not, Please try again!',
                'data' => $Input,
            ], 404);

          }

  }

}