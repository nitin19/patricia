<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\MainCategory;
use App\Models\Category;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use Validator;
use URL;
use Log;
use DB;
// DB::enableQueryLog();  
// dd(DB::getQueryLog()); exit;

class HomeController extends Controller {

  public function getLists(Request $request) {

        $lat = trim($request->lat);
        $lng = trim($request->lng);
        $login_id = trim($request->user_id);
        $cat_id =  trim($request->cat_id);
        $sub_id = trim($request->sub_id);
        $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT(user_id SEPARATOR ',')) as user_id"))->distinct()->first();
        $provideusers = $providers->user_id;
        $mapquery = DB::table('users');
        $sys_latitude = $lat;
        $sys_longitude = $lng;

        $mapquery->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.category','users.user_role','main_category.id as main_category_id','main_category.name as main_category_name','main_category.cat_image as main_category_image','category.cat_id as sub_category_id','category.cat_name as sub_category_name','category.cat_image as sub_category_image','users.address','users.profile_image','users.profile_rating','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority'
                ,DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $mapquery->join('main_category','users.cat_id','=','main_category.id');   
      $mapquery->join('category','users.sub_id','=','category.cat_id');
      $mapquery->join('provider_price_list','users.id','=','provider_price_list.user_id');
      $mapquery->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
      $mapquery->where('users.is_active', 1);
      $mapquery->where('users.is_deleted', 0);
      $mapquery->where('price_queries.priority', '1');
      if($cat_id) {
        $mapquery->where('users.cat_id',$cat_id);
      }
      if($sub_id) {
        $mapquery->where('users.sub_id',$sub_id);
      }
      if($login_id != ''){
        $mapquery->where('users.id', '!=', $login_id);
      }
      $mapquery->where(
        function($querys) use ($provideusers){
          $querys->whereIn('users.id', explode(',', $provideusers));
          $querys->orWhere('users.user_role', 'Provider');
      });
    $mapquery->distinct();
    $mapusers = $mapquery->get();

        return response()->json([
            'status' => true,
            'message' => 'Map user details.',
            'data' => $mapusers,
            ], 200);
       
  }

  public function getCategoryList(Request $request) {

    $category = DB::table('main_category')->where('is_active', '1')->where('is_deleted', '0')->get();
        if($category) {
          return response()->json([
                'status' => true,
                'message' => 'Category list.',
                'firstelement' => 'Selecione a Categoria',
                'data' => $category,
            ], 200);
        } else {
          return response()->json([
                'status' => false,
                'message' => 'No category found.',
                'data' => '',
            ], 404);
        }
  }

    public function getSuggestionsList(Request $request) {

      $q = trim($request->term);

      $url = "http://places.demo.api.here.com/places/v1/suggest?at=23.5505,46.6333&q='".$q."'+Sao+Paulo+Brazil&address=Brazil&recd=true&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);
      curl_close($ch);
      $data = json_decode($response,true);
      $suggestions = $data['suggestions'];
      $suggestions = str_replace('Brazil', 'Brasil', $suggestions);
     // echo json_encode((object)$suggestions);

        if($suggestions) {
          return response()->json([
                'status' => true,
                'message' => 'Suggestions list.',
                'data' => $suggestions,
            ], 200);
        } else {
          return response()->json([
                'status' => false,
                'message' => 'No result found.',
                'data' => '',
            ], 404);
        }
  }

  public function getSearchList(Request $request){
  
    $price      = array();
    $ip         = $request->ip(); // $_SERVER['REMOTE_ADDR'];
    $cat_id     = trim($request->get('cat'));
    $zipcode    = trim($request->get('zipcode'));
    $area       = trim(urldecode($request->get('area')));
    $rating     = trim($request->get('rating'));  
    $sub_id     = trim($request->sub_id);
    $sortby     = trim($request->get('sortby')); 
    $cat        = trim($request->cat);
    $amount     = trim($request->amount);
    $login_id   = trim($request->user_id);
    $lat   = trim($request->lat);
    $lng   = trim($request->lng);
    $frwdLat    = '';
    $frwdLng    = '';
    $latzip     = '';
    $lngzip     = '';
    $i= 0;
    
    if($zipcode!='' && $area!='') {
      $ZiplatLong = $this->getZipLatLong($zipcode);
      $AddrlatLong = $this->getAddrLatLong($area);
      $getips= $this->getIpLatLong($ip);
      if($ZiplatLong['lat']!='' && $ZiplatLong['lng']!='') {
        $frwdLat = $ZiplatLong['lat'];
        $frwdLng = $ZiplatLong['lng'];
      } elseif($AddrlatLong['lat']!='' && $AddrlatLong['lng']!='')   {
          $frwdLat = $AddrlatLong['lat'];
          $frwdLng = $AddrlatLong['lng'];
      } else {
        $frwdLat = '';
        $frwdLng = '';
      }
    } elseif($zipcode!='') {
      $ZiplatLong = $this->getZipLatLong($zipcode);
      if($ZiplatLong['lat']!='' && $ZiplatLong['lng']!='') {
        $frwdLat=$ZiplatLong['lat'];
        $frwdLng=$ZiplatLong['lng'];
      }
      
    } elseif($area!='') {
      $AddrlatLong = $this->getAddrLatLong($area);
      if($AddrlatLong['lat']!='' && $AddrlatLong['lng']!='') {
        $frwdLat=$AddrlatLong['lat'];
        $frwdLng=$AddrlatLong['lng'];
      }  
    } else {
        if($login_id  != '') {
          $userlatlng = DB::table('users')->where('id','=',$login_id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $frwdLat=$userlatlng->latitude;
            $frwdLng=$userlatlng->longitude;
          } else {
            $frwdLat=$lat;
            $frwdLng=$lng;
          }
        } else {
          $frwdLat=$lat;
          $frwdLng=$lng;
        }
    }

    $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT( DISTINCT user_id SEPARATOR ',')) as user_id"))->distinct()->first();
    $provideusers = $providers->user_id;
    //print_r($provideusers); 
    $unit = "K";
    $sys_latitude ="";
    $sys_longitude="";
    if($zipcode) {
      $ZiplatLong = $this->getZipLatLong($zipcode);
      if($ZiplatLong['lat']!='' && $ZiplatLong['lng']!='') {
        $latzip=$ZiplatLong['lat'];
        $lngzip=$ZiplatLong['lng'];
      }
      
    } else {
        if($login_id != '') {
          $userlatlng = DB::table('users')->where('id','=',$login_id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $sys_latitude=$lat;
            $sys_longitude=$lng;
          }
        } else {
          $sys_latitude=$lat;
          $sys_longitude=$lng;
        }
    } 
    //DB::enableQueryLog(); 
    $query = DB::table('users');
      if($zipcode != '' || $area != '' || ($zipcode!='' && $area!='')) {
       $query->select('users.id','users.id as user_id','users.name as username','users.email','users.description','users.address','users.city','users.zipcode','users.latitude','users.longitude','users.profile_image','users.profile_rating','users.price','users.price_type','users.cat_id as user_main_cat_id','users.sub_id','users.user_role','main_category.id as main_category_id','main_category.name as main_category_name','main_category.cat_image as main_category_image','category.cat_id as sub_category_id','category.cat_name as sub_category_name','category.cat_image as sub_category_image','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority'
                ,DB::raw("(6371 * acos (cos ( radians('$frwdLat') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$frwdLng') ) + sin ( radians('$frwdLat') ) * sin( radians( latitude ) ) ) ) AS distances"));
       $query->having('distances', '<', '60');

      } else {

        $query->select('users.id','users.id as user_id','users.name as username','users.email','users.description','users.address','users.city','users.zipcode','users.latitude','users.longitude','users.profile_image','users.profile_rating','users.price','users.price_type','users.cat_id as user_main_cat_id','users.sub_id','users.user_role','main_category.id as main_category_id','main_category.name as main_category_name','main_category.cat_image as main_category_image','category.cat_id as sub_category_id','category.cat_name as sub_category_name','category.cat_image as sub_category_image','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority'
                ,DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      }

    $query->join('main_category','users.cat_id','=','main_category.id');   
    $query->join('category','users.sub_id','=','category.cat_id');
    $query->join('provider_price_list','users.id','=','provider_price_list.user_id');
    $query->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
    if ($cat_id) {
            $query->where('users.cat_id',$cat_id);
    }

    if ($sub_id) {
            $query->where('users.sub_id',$sub_id);
    }

    if($rating!=''){
      $query->where('users.profile_rating','>=',$rating);
    }

    if($amount!=''){
      $minamount = $amount-30;
      $maxamount = $amount+30;
      $query->whereBetween('provider_price_list.amount', array($minamount, $maxamount));    
    }

    $query->where('users.is_active', 1);
    $query->where('users.is_deleted', 0);
    $query->where('price_queries.priority', '1');
    if($login_id != ''){
      $query->where('users.id', '!=', $login_id);
    }

    $query->where(
      function($querys) use ($provideusers){
        $querys->whereIn('users.id', explode(',', $provideusers));
        $querys->orWhere('users.user_role', 'Provider');
    });

    if($sortby == 'rating'){
      $query->orderBy('users.profile_rating','asc');
    }else if($sortby == 'price'){
    //$query->orderBy('distances', 'desc');
    $query->orderBy('provider_price_list.amount','asc');
    }else {
      $query->orderBy('distances','asc');
    }

    $query->distinct();

    $users = $query->simplePaginate(9);
    //$users = $query->get()->count();
    //return $users;

    foreach ($users as $user_info) {
      if($login_id !=''){
        $wishlist_count = DB::table('wish_list')
                    ->where('wish_profile_id',$user_info->user_id)
                    ->where('user_id',$login_id)
                    ->count(); 
        $users[$i]->wishlist_status = $wishlist_count ;        
      } else {
        $users[$i]->wishlist_status = 2;
      }
      if($user_info->distances !=''){
        $dist = number_format($user_info->distances,2,",",".");
        $users[$i]->distances = $dist;
      }
       
       $i++;
    }

    //dd(DB::getQueryLog()); exit;
    
    $responseArr =  [];

    $responseArr['users'] = $users;
    $responseArr['ip'] = $ip;
    $responseArr['cat_id'] = $cat_id;
    $responseArr['zipcode'] = $zipcode;
    $responseArr['area'] = $area;
    $responseArr['rating'] = $rating;
    $responseArr['cat'] = $cat;
    $responseArr['sortby'] = $sortby;
    $responseArr['sub_id'] = $sub_id;
    $responseArr['amount'] = $amount;
    $responseArr['frwdLat'] = $frwdLat;
    $responseArr['frwdLng'] = $frwdLng;

     return response()->json([
                'status' => true,
                'message' => 'Suggestions list.',
                'data' => $responseArr,
            ], 200);
    }
    /*return view('pages.search', compact('mainCatName', 'category', 'users', 'ip', 'current_ip', 'get_categories', 'get_category1', 'users_sub_list', 'main_category_data', 'mainCatdata', 'cat_id', 'zipcode', 'area', 'rating', 'cat', 'sortby', 'sub_id', 'amount', 'userblocks','distance','frwdLat','frwdLng'));*/
     



  function getAddrLatLong($address){  
    $mylat = '';
    $mylng = '';
    $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".urlencode($address)."&language=en-US&additionaldata=&maxresults=20&gen=8";  
    $filestring = file_get_contents($url);
    $filearray = explode("\n", $filestring);
    $data = json_decode($filearray[0]);
    foreach($data as $k=>$v) {
      $view = $v->View;
      $view_count = count($view);
      if($view_count>0){
        foreach($view as $result_k=>$result_v) {
          $Result = $result_v->Result;
          $mylat = $Result[0]->Location->DisplayPosition->Latitude;
          $mylng = $Result[0]->Location->DisplayPosition->Longitude;
        }
      }
    } 
    $data = array('lat'=>$mylat, 'lng'=>$mylng);
    return $data;
   }

  function getZipLatLong($zipcode){     //latlong according to zipcode
    $mylat = '';
    $mylng = '';
    $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".urlencode($zipcode)."&language=en-US&additionaldata=&maxresults=20&gen=8";
    $filestring = file_get_contents($url);
    $filearray = explode("\n", $filestring);
    $data = json_decode($filearray[0]);
    foreach($data as $k=>$v) {
      $view = $v->View;
      $view_count = count($view);
      if($view_count>0){
        foreach($view as $result_k=>$result_v) {
          $Result = $result_v->Result;
          $mylat = $Result[0]->Location->DisplayPosition->Latitude;
          $mylng = $Result[0]->Location->DisplayPosition->Longitude;
        }
      }
    } 
    $data = array('lat'=>$mylat, 'lng'=>$mylng);
    return $data;
   }

  function getIpLatLong($ipaddress){
    $data = array();
    $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=a3d9e92c7201bb32d8115142ae661d91");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];
    return $data;
   }

  public function getAllCategoryList(Request $request) {
    
    $data =[];
    $get_main_categories = DB::table('main_category')->select('id as main_category_id','name as main_category_name','cat_image as main_category_image')->where('is_active', '=', '1')->where('is_deleted', '=', '0')->get();

    foreach($get_main_categories as $main_categories) {
      $get_sub_categories = DB::table('category')->select('cat_id as sub_category_id','cat_name as sub_category_name','cat_image as sub_category_image', 'main_cat_id')->where('main_cat_id', '=', $main_categories->main_category_id)->where('is_active', '=', '1')->where('is_deleted', '=', '0')->get();

       $main_categories->subcategory_list = $get_sub_categories;
    }
    $data = $get_main_categories;
      
        if($data) {
           return response()->json([
                 'status' => true,
                 'message' => 'All category list.',
                 'data' => $data,
             ], 200);
         } else {
           return response()->json([
                 'status' => false,
                 'message' => 'No category found.',
                 'data' => '',
             ], 404);
         }
  }

  public function getotherservices(Request $request) {
      $category_info = DB::table('other-services')->where('is_active', '1')->where('is_deleted', '0')->orderBy('service_position', 'asc')->paginate(10);

       if($category_info) {
           return response()->json([
                 'status' => true,
                 'message' => 'All services list.',
                 'data' => $category_info,
             ], 200);
         } else {
           return response()->json([
                 'status' => false,
                 'message' => 'No service found.',
                 'data' => '',
             ], 404);
         }
  }

  public function getsingleservicedetails(Request $request) {
      $id = $request->service_id;
      $category_info = DB::table('other-services')->where('id',$id)->where('is_active', '1')->where('is_deleted', '0')->first();

       if($category_info) {
           return response()->json([
                 'status' => true,
                 'message' => 'Single service data.',
                 'data' => $category_info,
             ], 200);
         } else {
           return response()->json([
                 'status' => false,
                 'message' => 'No service found.',
                 'data' => '',
             ], 404);
         }
  }
   

}