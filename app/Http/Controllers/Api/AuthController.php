<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Spavailability;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use Validator;
use URL;
use Log;
use DB;

class AuthController extends Controller
{   
  public function __construct()
  {
   // $this->middleware('auth:api', ['except' => ['login', 'register', 'logout']]);
  }
  
  public function register(Request $request) {
    
        $data =  array();

        $rules = [
             'name' => 'required|string|max:255',
             'email' => 'required|string|email|max:255|unique:users',
             'user_role' => 'required',
             'password' => 'required|string|min:6|confirmed',
             'password_confirmation' => 'required|min:6',
         ];

         $messages = [
             'name.required'   =>  'Please enter your name.',
             'email.required'   =>  'Please enter your email.',
             'email.unique'   =>  'Email address is already in use.',
             'user_role.required'   =>  'Please select user role.',
             'password.required'   =>  'Please enter your password.',
             'password_confirmation.required' => 'Please enter confirm password.',
         ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){

        return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
        } 

        $userData = array();

        $data['name'] = trim($request->name);

        $data['email'] = trim($request->email);

        $data['password'] = Hash::make(trim($request->password));

        $data['hd_password'] = trim($request->password);

        $data['user_role'] = trim($request->user_role);

        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $referal = "navizinhanca";
        for ($i = 0; $i < 5; $i++) {
          $referal .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        $data['referal_code'] = $referal;

        $data['token'] = trim($request->fcm_token);

        $user = User::create($data);

        $insertedId = $user->id;

    if($user) {

          if($user->user_role == 'Provider') {
            $data = new Spavailability;
            $data->user_id = $user->id;
            $data->openhrs = '0';
            $data->isCompleted = '1';  
            $data->save();
          }

          $from_email = "NaVizinhanca  <contato@navizinhanca.com>";

          $name = trim($user->name);
          $email = trim($user->email);

          $to = $email;
          
          $logo_url = url('/public').'/images/logo.png';
          $mail_bg_img = url('/public').'/images/bg.jpg';
          $email_icon = url('/public').'/images/email_icon_mail.png';
          $LI_png = url('/public').'/images/LI.png';
          $fb_png = url('/public').'/images/fb.png';
          $insta_png = url('/public').'/images/insta.png';
          
          $messageBody = '';
          $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                <!--[if !mso]><!-- -->
                <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                <!-- <![endif]-->
                <title>Email Template</title>
                <style type="text/css">
                * {font-family: "Lato", sans-serif;}
                </style>
            </head>
          <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
          <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" >
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                          <tr>
                                              <td align="center" height="70" style="height:70px;">
                                                  <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center">
                                                  <hr>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                      <div style="line-height: 35px">
                                          <span style="color: #1497d5;">Seja bem vindo(a) ao NaVizinhança.</span>
                                      </div>
                                      <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:5px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
                                   
                                  </td>
                              </tr>
                              <tr>
                                  <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr class="hide">
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                  <tr>
                      <td align="center">
                          <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                              <tr>
                                  <td height="25">&nbsp;</td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <table border="0" width="450" align="center" cellpadding="0" cellspacing="0" class="container580">
                                          <tr>
                                              <td width="30">&nbsp;</td>
                                              <td align="center" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                                               
                                                  <div  style="color: #0c3e64;
                                                      font-size: 19px;
                                                      font-weight: 700;">
                                                      <b style="color: #1497d5;
                                                      padding-right: 5px;">Olá </b>' . $name .'
                                                     
                                                  </div>
                                                  
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td height="30">&nbsp;</td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                  </tr>
                  <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" > 
                      <tr>
                          <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                  <tr>
                                      <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                          <div style="line-height: 26px;">
                                              <a href="https://www.navizinhanca.com/user/'.$insertedId.'" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Comece sua jornada</a>
                                          </div>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td height="70">&nbsp;</td>  
                                  </tr>
                                      </table>
                                  </td>
                              </tr>
                  </table>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td align="center">
                          <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr> 
                                  <td>
                                      <div class="social-icons" style="text-align: center;">
                                          <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$insta_png.'">
                                          </a>
                                          <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$LI_png.'">
                                          </a>
                                          <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                              <img src="'.$fb_png.'">
                                          </a>
                                      </div>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              </table>
            </body>
          </html>';
          $subject ="Ative sua conta: NaVizinhanca";
          $headers = "From: " .$from_email  . "\r\n".'X-Mailer: PHP/' . phpversion();
          // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          //$headers .= "CC: dev2.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $messageBody, $headers,'-faaa@gmail.com');


            $userData['user_id'] = $user->id;
            $userData['name'] = trim($user->name);
            $userData['email'] = trim($user->email);
            $userData['user_role'] = trim($user->user_role);

      return response()->json([
                'status' => true,
                'message' => 'Thank you A new link has been sent to your email. Click it to activate your account.',
                'data' => $userData,
            ], 200);

    } else {

      return response()->json([
                'status' => false,
                'message' => 'Sorry fail to register, Please try again',
                'data' => '',
            ], 500);
    }
}

public function login(Request $request) { 

    $rules = [
       'email' => 'required|email',
       'password' => 'required',
       'fcm_token' => 'required',
   ];

   $messages = [
       'email.required'   =>  'Please enter your email.',
       'password.required'   =>  'Please enter your password.',
       'fcm_token.required'   =>  'fcm token is missing.',
   ];

  $validator = Validator::make($request->all(), $rules);

  if($validator->fails()){

        return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
        }        

if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

if(Auth::user()->is_deleted == '1' && Auth::user()->close_request_action == '1') {

    return response()->json([
                'status' => false,
                'message' => 'Your account is closed by admin on your close account request.',
                'data' => '',
            ], 200);

    } elseif(Auth::user()->is_deleted == '1' && Auth::user()->close_request_action == '0') { 

      return response()->json([
                'status' => false,
                'message' => 'Your account is deleted!.',
                'data' => '',
            ], 200);

    } else {

        if(Auth::user()->is_active == '1') {
                $user = Auth::user();
                if($user) {
                    $user_id = Auth::user()->id;
                    $token = trim($request->fcm_token);
                    $updateToken = DB::table('users')
                        ->where('id', $user_id)
                        ->update(['token' => $token]);
                    if($updateToken) {
                      return response()->json([
                            'status' => true,
                            'message' => 'User login successfully.',
                            'data' => $user,
                        ], 200);
                    } else {
                      return response()->json([
                            'status' => true,
                            'message' => 'Sorry fail to login.',
                            'data' => '',
                        ], 500);
                    }
                  } else {
                        return response()->json([
                            'status' => false,
                            'message' => 'User not found.',
                            'data' => '',
                        ], 404);
                  }
        } else {
            return response()->json([
                    'status' => false,
                    'message' => 'Your account is no active. Please check your registred email to activate your account',
                    'data' => '',
                ], 200); 
        }
    }

  } else {
            return response()->json([
                'status' => false,
                'message' => 'Invalid credentials.',
                'data' => '',
            ], 200);
    }
    
}


public function refreshtoken(Request $request)
{  
    $rules = [
       'user_id' => 'required',
       'fcm_token' => 'required',
    ];

   $messages = [
       'user_id.required'   =>  'User id is missing.',
       'fcm_token.required'   =>  'fcm token is missing.',
    ];

  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
        return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
        }

   $user_id = trim($request->user_id);
   $token = trim($request->fcm_token);
   $user = User::find($user_id);

   if($user) {
    $updateToken = DB::table('users')
        ->where('id', $user_id)
        ->update(['token' => $token]);
    if($updateToken) {
      return response()->json([
                'status' => true,
                'message' => 'fcm token refreshed successfully.',
                'data' => '',
            ], 200);
    } else {
      return response()->json([
                'status' => false,
                'message' => 'Sorry fail to refresh fcm token.',
                'data' => '',
            ], 500);
      }     
   } else {
    return response()->json([
                'status' => false,
                'message' => 'User not found.',
                'data' => '',
            ], 404);
    }
}

public function logout(Request $request)
{  
    $rules = [
       'user_id' => 'required',
    ];

   $messages = [
       'user_id.required'   =>  'User id is missing.',
    ];

  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
        return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
        }

   $user_id = trim($request->user_id);
   $user = User::find($user_id);

   if($user) {
    $updateToken = DB::table('users')
        ->where('id', $user_id)
        ->update(['token' => '']);
    if($updateToken) {
      return response()->json([
                'status' => true,
                'message' => 'Successfully logged out.',
            ], 200);
    } else {
      return response()->json([
                'status' => false,
                'message' => 'Sorry fail to logged out.',
            ], 500);
      }     
   } else {
    return response()->json([
                'status' => false,
                'message' => 'User not found.',
            ], 404);
    }
}

}