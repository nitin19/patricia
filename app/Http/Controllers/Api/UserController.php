<?php
namespace App\Http\Controllers\Api;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Feedback;
use App\Models\Spavailability;
use App\Models\Notification;
use App\Models\BlockProvider;
use App\Models\BlackUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Errorlogs;
use Session;
use DB;
use Image;
use URL;
use Log;
class UserController extends Controller {
  public function forgotpassword(Request $request) {
    $rules = [
        'email' => 'required|email',
    ];
    $messages = [
      'email.required'   =>  'Please enter your email.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $email = $request->email;
    $email_exist = DB::table('users')->where('email', $email)->count();
    if( $email_exist > 0 ){
      $user_info = DB::table('users')->where('email', $email)->first();
      $newpaswd = 'navi'.rand(10000,99999);
      $descrypt_pswd = Hash::make(trim($newpaswd));
      $update_pswd = DB::table('users')
      ->where('email', $email)
      ->update(['password' => $descrypt_pswd, 'hd_password' => $newpaswd]);
    if($update_pswd){
      $name = $user_info->name;
      $to = $email;
      $subject = "Forgot Password";
      $headers  = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: <navizinhanca.com>' . "\r\n";
        $message = '';
        $message .= 'Hi '. $name.','. '<br>';
        $message .= 'Here is your <b>Password: </b> '. $newpaswd . '<br>';
        $message .= 'Thanks';
        $a = mail($to,$subject,$message,$headers);
        return response()->json([
        'status' => true,
        'message' => 'Your password has been sent to your email',
        'data' => $email,
        ], 200);
      } else {
        return response()->json([
        'status' => false,
        'message' => 'Sorry fail to complete your request, Please try again',
        'data' => '',
        ], 500);
      }
      } else {
        return response()->json([
        'status' => false,
        'message' => 'Sorry your email did not matched with registered email',
        'data' => '',
        ], 404);
      }
  }
  public function changepassword(Request $request) {
    $rules = [
      'user_id' => 'required',
      'old_password' => 'required',
      'new_password' => 'required',
    ];
    $messages = [
      'user_id.required'   =>  'User id is missing.',
      'old_password.required'   =>  'Please enter your old password.',
      'new_password.required'   =>  'Please enter your new password.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => $validator->errors(),
      'data' => 'Validation error.',
      ], 401);
    }
    
    $user_id = trim($request->user_id);
    $old_pswd = trim($request->old_password);
    $new_pswd = trim($request->new_password);
    $descrypt_pswd = Hash::make(trim($new_pswd));
    $pswd_count = DB::table('users')
    ->where('id', $user_id)
    ->where('hd_password', $old_pswd)
    ->count();
    
    if($pswd_count>0){
      $update_pswd = DB::table('users')
      ->where('id', $user_id)
      ->update(['password'=>$descrypt_pswd,'hd_password'=>$new_pswd]);
      if($update_pswd) {
        return response()->json([
        'status' => true,
        'message' => 'Senha atualizada com sucesso.',
        'data' => $user_id,
        ], 200);
      } else {
        return response()->json([
        'status' => false,
        'message' => 'Failed to update your Password, Try again',
        'data' => '',
        ], 400);
      }
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Your old Password not matched with current password',
      'data' => '',
      ], 400);
    }
  }
  public function details(Request $request) {
  $rules = [
  'user_id' => 'required',
  ];
  $messages = [
  'user_id.required'   =>  'User id is missing.',
  ];
  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
  return response()->json([
  'status' => false,
  'message' => 'Validation error.',
  'data' => $validator->errors(),
  ], 401);
  }
  $id = trim($request->user_id);
  $login_id = trim($request->login_id);
  $user = User::find($id);
  if($user) {
  if($user->cat_id) {
  $main_category = DB::table('main_category')->select('id as main_category_id','name as main_category_name','cat_image as main_cat_image')->where('id', '=', $user->cat_id)->where('is_deleted', '=', 0)->where('is_active', '=', 1)->first();
  $main_category_id = $main_category->main_category_id;
  $main_category_name = $main_category->main_category_name;
  $main_cat_image = $main_category->main_cat_image;
  } else {
  $main_category_id = '';
  $main_category_name = '';
  $main_cat_image = '';
  }
  if($user->sub_id) {
  $category = DB::table('category')->select('cat_id as sub_category_id','cat_name as sub_cat_name','cat_image as sub_cat_image')->where('cat_id', '=', $user->sub_id)->where('is_deleted', '=', 0)->where('is_active', '=', 1)->first();
  $sub_category_id = $category->sub_category_id;
  $sub_cat_name = $category->sub_cat_name;
  $sub_cat_image = $category->sub_cat_image;
  } else {
  $sub_category_id = '';
  $sub_cat_name = '';
  $sub_cat_image = '';
  }
  $user_documents = DB::table('user_documents')->select('id as document_id', 'document','document_status')->where('user_id', '=', $id)->where('is_deleted', '=', 0)->where('is_active', '=', 1)->get();
  $user_gallery = DB::table('user_gallery')->select('image_id', 'image_name')->where('user_id', '=', $id)->where('is_deleted', '=', 0)->where('is_active', '=', 1)->get();
  $user_availability = DB::table('spavailability')->select('id as availability_id', 'openhrs', 'day_monday', 'monday_start', 'monday_end', 'day_tuesday', 'tuesday_start', 'tuesday_end', 'day_wednesday', 'wednesday_start', 'wednesday_end', 'day_thursday', 'thursday_start', 'thursday_end', 'day_friday', 'friday_start', 'friday_end', 'day_saturday', 'saturday_start', 'saturday_end', 'day_sunday', 'sunday_start', 'sunday_end')->where('user_id', '=', $id)->where('status', '=', 1)->get();
  foreach($user_availability  as $availability){
    if($availability->openhrs == '0'){
      $availability->monday_start = '00:00';
      $availability->monday_end = '23:59';
      $availability->tuesday_start = '00:00';
      $availability->tuesday_end = '23:59';
      $availability->wednesday_start= '00:00';
      $availability->wednesday_end = '23:59';
      $availability->thursday_start = '00:00';
      $availability->thursday_end = '23:59';
      $availability->friday_start = '00:00';
      $availability->friday_end = '23:59';
      $availability->saturday_start = '00:00';
      $availability->saturday_end = '23:59';
      $availability->sunday_start = '00:00';
      $availability->sunday_end = '23:59';
    } else {
      $availability =$availability;
    }
  }
  $provider_price_lists = DB::table('provider_price_list')
  ->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
  ->where('provider_price_list.user_id', $id)
  ->select('price_queries.*', 'provider_price_list.id', 'provider_price_list.amount', 'provider_price_list.query_id')
  ->orderBy('price_queries.id')
  ->get()->toArray();
  $complete_booking=DB::table('booking')
  ->where('profile_id', '=', $id)
  ->where('booking_verified', '=', '1')
  ->count();
  
  $repeat_clients = Booking::select(DB::raw('COUNT(*)'))
  ->where('profile_id', '=', $id)
  ->groupby('profile_id','user_id')
  ->havingRaw('COUNT(*) > 1')
  ->get()
  ->count();
  $review_count = DB::table('feedback')
  ->join('users', 'users.id', '=', 'feedback.user_id')
  ->select('users.*', 'feedback.*')
  ->where('feedback.provider_id', '=', $id)
  ->where('approve_status', '=', '1')
  ->where('role', '=', 'taker')
  ->count();
  if($login_id != ''){
  $wishlist_count = DB::table('wish_list')
  ->where('wish_profile_id',$id)
  ->where('user_id',$login_id)
  ->count();
  
  } else{
  $wishlist_count = 2;
  }
  $ip = $_SERVER['REMOTE_ADDR'];
  if(!empty($login_id)) {
    $userlatlng = DB::table('users')->where('id','=',$login_id)->where('is_active','=','1')->first();
    if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
      $sys_latitude=$userlatlng->latitude;
      $sys_longitude=$userlatlng->longitude;
    } else {
      $sys_latitude='';
      $sys_longitude='';
    }
  }else{
    $sys_latitude='';
      $sys_longitude='';
  }
  $unit = "K";
  $user_lat = $user->latitude;
  $user_lon = $user->longitude;
  
  $distance =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
   $distances = number_format($distance,2,",",".");
  /*$userData = DB::table('users')->select('users.id as user_id','users.name','user_documents.id as document_id','user_documents.document','user_gallery.image_id','user_gallery.image_name')->leftjoin('user_documents','users.id','=','user_documents.user_id')->leftjoin('user_gallery','users.id','=','user_gallery.user_id')->where('user_documents.is_active',1)->where('user_documents.is_deleted',0)->where('user_gallery.is_active',1)->where('user_gallery.is_deleted',0)->get();
  $userDataArr = [];
  foreach($userData as $user){
  $userDataArr[$user->user_id][] = ['user_id'=>$user->user_id,'name'=>$user->name,'document_id'=>$user->document_id,'document'=>$user->document,'image_id'=>$user->image_id,'image_name'=>$user->image_name];
  }*/
  $user['complete_booking'] = $complete_booking;
  $user['repeat_clients'] = $repeat_clients;
  $user['main_category_id'] = $main_category_id;
  $user['main_category_name'] = $main_category_name;
  $user['main_cat_image'] = $main_cat_image;
  $user['sub_category_id'] = $sub_category_id;
  $user['sub_cat_name'] = $sub_cat_name;
  $user['sub_cat_image'] = $sub_cat_image;
  $user['wishlist_status'] = $wishlist_count;
  $user['review_count'] = $review_count;
  $user['user_documents'] = $user_documents;
  $user['user_gallery'] = $user_gallery;
  $user['user_availability'] = $user_availability;
  $user['provider_price_lists'] = $provider_price_lists;
  $user['distance'] = $distances;
  return response()->json([
  'status' => true,
  'message' => 'User details.',
  'data' => $user,
  ], 200);
  } else {
  return response()->json([
  'status' => false,
  'message' => 'User not found.',
  'data' => '',
  ], 404);
  }
  }
  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
  return 0;
  } else {
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);
  if ($unit == "K") {
  return ($miles * 1.609344);
  } else if ($unit == "N") {
  return ($miles * 0.8684);
  } else {
  return $miles;
  }
  }
  }
  function getIpLatLong($ipaddress){
  $data = array();
  $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=a3d9e92c7201bb32d8115142ae661d91");
  $ipstackData  =  json_decode($ipstackResult ,true);
  $data['lat']=@$ipstackData['latitude'];
  $data['lng']=@$ipstackData['longitude'];
  return $data;
  }
  public function add_review(Request $request){
  $Input = $request->all();
  $rules = [
  'user_id'    => 'required',
  'provider_id'=> 'required',
  'booking_id'=> 'required',
  'rating'=> 'required',
  'user_role'  => 'required',
  'message'=> 'required',
  ];
  $messages = [
  'user_id.required'     =>  'User id is missing.',
  'provider_id.required'   =>  'User provider id is missing.',
  'booking_id.required'   =>  'User booking_id is missing.',
  'rating.required'   =>  'User rating is missing.',
  'user_role.required'   =>  'User role is missing.',
  'message.required'   =>  'User message is missing.',
  ];
  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
  return response()->json([
  'status' => false,
  'message' => 'Validation error.',
  'data' => $validator->errors(),
  ], 401);
  }
  
  $user_id = trim($request->user_id);
  $provider_id = trim($request->provider_id);
  $booking_id = trim($request->booking_id);
  $rating = trim($request->rating);
  $role = trim($request->user_role);
  $message = trim($request->message);
  $rate_user = Feedback::select('*')->where('user_id','=',$request->user_id)->where('booking_id','=',$request->booking_id)->first();
  if($rate_user != ''){
  return response()->json([
  'status' => false,
  'message' => 'Prestador de serviço já avaliado.',
  'data' => '',
  ], 400);
  } else {
  $insertedId = Feedback::insertGetId(
  ['user_id' => $user_id,
  'provider_id' => $provider_id,
  'booking_id' => $booking_id,
  'rating' => $rating,
  'role' => $role,
  'message' => $message
  ]
  );
  if($insertedId) {
  return response()->json([
  'status' => true,
  'message' => 'Avaliação feita com sucesso.',
  'data' => $Input,
  ], 200);
  } else {
  return response()->json([
  'status' => false,
  'message' => 'something wrong.',
  'data' => $Input,
  ], 404);
  
  }
  }
  }
  public function reviews(Request $request) {
  $rules = [
  'user_id' => 'required',
  'role'    =>'required',
  ];
  $messages = [
  'user_id.required'   =>  'User id is missing.',
  'role.required'   =>  'User role is missing.',
  ];
  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
  return response()->json([
  'status' => false,
  'message' => $validator->errors(),
  'data' => 'Validation error.',
  ], 401);
  }
  $id = trim($request->user_id);
  $role = trim($request->role);
  
  $user = User::find($id);
  if($user){
  $reviews = DB::table('feedback')
  ->join('users', 'users.id', '=', 'feedback.user_id')
  ->select('users.id as reviewer_user_id','users.name as username','users.email','users.profile_image', 'feedback.id as feedback_id','feedback.user_id','feedback.provider_id','feedback.booking_id','feedback.rating','feedback.message','feedback.role','feedback.approve_status','feedback.created_at','feedback.updated_at')
  ->where('feedback.provider_id', '=', $id)
  ->where('feedback.role', '=', $role)
  ->where('feedback.approve_status', '=', '1')
  ->paginate(5);
  
  
  return response()->json([
  'status' => true,
  'message' => 'User details.',
  'data' => $reviews,
  ], 200);
  } else {
  return response()->json([
  'status' => false,
  'message' => 'User not found.',
  'data' => '',
  ], 404);
  }
  }
  public function provider_booking(Request $request){
  $Input = $request->all();
  
  $startdate = trim($request->startdate);
  $cat_name = trim($request->cat_name);
  $sub_cat_name = trim($request->sub_cat_name);
  $booking_msg = trim($request->booking_msg);
  $profile_id = trim($request->profile_id);
  $user_id = trim($request->user_id);
  $booking_amount = trim($request->total_amount);
  $spavailabilityInfo = Spavailability::where('user_id', $profile_id)->where('status', 1)->first();
  $fromDate = date("Y-m-d", strtotime($startdate) );
  $getBooking = DB::table('booking')
  ->where('profile_id',$profile_id)
  ->where(function ($query) use ($fromDate) {
  $query->where('booking_start_time',$fromDate);
  })->count();
  
  $insertedId = DB::table('booking')->insertGetId(
  ['profile_id' => $profile_id,
  'user_id' => $user_id,
  'category' => $cat_name,
  'sub_category' => $sub_cat_name,
  'booking_message' => $booking_msg,
  'booking_amount' => $booking_amount,
  'payment_type' => 'Paypal',
  'booking_start_time' => $fromDate,
  //'booking_end_time' => Null,
  'transaction_id'=>'0',
  'payment_confirm'=>'1',
  'is_active'=>'1',
  'is_deleted'=>'0'
  ]
  );
  if($insertedId) {
  $providerEmail = DB::table('users')
  ->select('email','name','phone')
  ->where('id', '=', $profile_id)
  ->first();
  $takerInfo = DB::table('users')
  ->select('name','email')
  ->where('id', '=', $user_id)
  ->first();
  $notifie = DB::table('notifications')->insert(
  ['user_id' => $profile_id, 'notificaton_type' => 'booking','type_id' => $insertedId]
  );
  $fullname  = $takerInfo->name;
  $email = $takerInfo->email;
  $to = $providerEmail->email;
  $provider_name = $providerEmail->name;
  $provider_phone = $providerEmail->phone;
  $logo_url = url('/public').'/images/logo.png';
  $mail_bg_img = url('/public').'/images/bg.jpg';
  $email_icon = url('/public').'/images/email_icon_mail.png';
  $LI_png = url('/public').'/images/LI.png';
  $fb_png = url('/public').'/images/fb.png';
  $insta_png = url('/public').'/images/insta.png';
  //  $messageBody = '';
  $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns:v="urn:schemas-microsoft-com:vml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
      <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
      <!--[if !mso]><!-- -->
      <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
      <!-- <![endif]-->
      <title>Email Template</title>
      <style type="text/css">
      * {font-family: "Lato", sans-serif;}
      </style>
    </head>
    <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="690"  style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
          <tr>
            <td align="center">
              <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                      <tr>
                        <td align="center" height="70" style="height:70px;">
                          <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                        </td>
                      </tr>
                      <tr>
                        <td align="center"><hr></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
          <tr>
            <td align="center">
              <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                <tr>
                  <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                    <div style="line-height: 35px">
                      <span style="color: #1497d5;">Você tem uma <br>solicitação de serviço</span>
                    </div>
                    <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
                  </td>
                </tr>
                <tr>
                  <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                      <tr>
                        <td align="center" style="color: #4a4545; font-size: 16px; line-height: 24px;">
                          <div style="line-height: 24px">'.$booking_msg.'</div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="hide">
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
          </tr>
        </table>
        <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
          <tr>
            <td align="center">
              <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                <tr>
                  <td height="25">&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" class="container580" >
                      <tr>
                        <td width="30">&nbsp;</td>
                        <td align="center" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                          <div  style="color: #0c3e64;font-size: 19px;font-weight: 700;">
                            <b style="color: #1497d5;padding-right: 5px;">Nome:</b>' . $fullname .'
                          </div>
                          <div style="color: #0c3e64;font-size: 19px;font-weight: 700;">
                            <b style="color: #1497d5;padding-right: 5px;">E-mail:</b>
                            ' . $email .'
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td height="30">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
          </tr>
          <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
            <tr>
              <td align="center">
                <table border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                  <tr>
                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                      <div style="line-height: 26px;">
                        <a href="https://www.navizinhanca.com/serviceprovider/booking/pending" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Ver mais informações e avaliar o prestador de serviço</a>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td height="70">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </table>
        <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
          <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">
              <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                <tr>
                  <td>
                    <div class="social-icons" style="text-align: center;">
                      <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                        <img src="'.$insta_png.'">
                      </a>
                      <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                        <img src="'.$LI_png.'">
                      </a>
                      <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                        <img src="'.$fb_png.'">
                      </a>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
          </tr>
        </table>
      </table>
    </body>
  </html>';
  $subject ="NaVizinhança: Você tem uma solicitação de serviço";
  $headers = "From: " .$email. "\r\n";
  // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
  // $headers .= "CC: dev2.bdpl@gmail.com\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  // $headers .='X-Mailer: PHP/' . phpversion();
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
  $mailsend = mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com');
  if($provider_name != '' && $provider_phone != ''){
  $Input['provider_name'] = $provider_name;
  $Input['provider_phone'] = $provider_phone;
  }
  return response()->json([
  'status' => true,
  'message' => 'Mensagem enviada por e-mail com sucesso!',
  'data' => $Input,
  ], 200);
  } else {
  return response()->json([
  'status' => false,
  'message' => 'Sorry fail to save your booking request!',
  'data' => $Input,
  ], 404);
  
  }
  
  }
  public function provider_availability(Request $request) {
  $rules = [
  'user_id' => 'required',
  ];
  $messages = [
  'user_id.required'   =>  'User id is missing.',
  ];
  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
  return response()->json([
  'status' => false,
  'message' => 'Validation error.',
  'data' => $validator->errors(),
  ], 401);
  }
  $id = trim($request->user_id);
  $user = User::find($id);
  if($user) {
  $user_availability = Spavailability::select('id as availability_id', 'openhrs', 'day_monday', 'monday_start', 'monday_end', 'day_tuesday', 'tuesday_start', 'tuesday_end', 'day_wednesday', 'wednesday_start', 'wednesday_end', 'day_thursday', 'thursday_start', 'thursday_end', 'day_friday', 'friday_start', 'friday_end', 'day_saturday', 'saturday_start', 'saturday_end', 'day_sunday', 'sunday_start', 'sunday_end')->where('user_id', '=', $id)->where('status', '=', 1)->first();
  return response()->json([
  'status' => true,
  'message' => 'User availability details.',
  'data' => $user_availability,
  ], 200);
  } else {
  return response()->json([
  'status' => false,
  'message' => 'User availability details not found.',
  'data' => '',
  ], 404);
  }
  }
  public function profile_update(Request $request){
    $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $phone = $request->phone;
    $user_role = $request->user_role;
    $country = $request->country;
    $zipcode = $request->zipcode;
    $state = $request->state;
    $user_id = $request->user_id;
    $address_str = $request->address;
    $cpf_id = $request->cpf_id;
    $rg = $request->rg;
    $address = urlencode($address_str);
    $bio = $request->bio;
    $additional_details = $request->additional_details;
    $price = 10;
    if($cpf_id !=" "){
       $avail =  DB::table('users')->where('cpf_id', $request->cpf_id )->where('id','!=',$request->user_id)->first();
      if($avail){
      return response()->json([
      'status' => false,
      'message' => 'This cpf number already exists',
      'data' => '',
      ], 404);
      }
    }
    $url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".$address."&country=Brazil&language=en-US&additionaldata=&maxresults=20&gen=8";
    $filestring = file_get_contents($url);
    $filearray = explode("\n", $filestring);
    $data = json_decode($filearray[0]);
    //echo"<pre>";print_r($data);echo "</pre>";die;
    foreach($data as $k=>$v) {
      $view = $v->View;
      $view_count = count($view);
      if($view_count>0){
        foreach($view as $result_k=>$result_v) {
          $Result = $result_v->Result;
          $PostalCode1 =  array_key_exists('PostalCode',$Result[0]->Location->Address);
          if($PostalCode1 == 1){
            $PostalCode =  $Result[0]->Location->Address->PostalCode;
            $lat =  $Result[0]->Location->DisplayPosition->Latitude;
            $long =  $Result[0]->Location->DisplayPosition->Longitude;
          } else {
            return response()->json([
            'status' => false,
            'message' => 'Enter correct Address!',
            'data' => '',
            ], 404);
          }
          $countryname =  $Result[0]->Location->Address->AdditionalData[0]->value;
          $statename =  $Result[0]->Location->Address->AdditionalData[1]->value;
          $check_city  = array_key_exists('District',$Result[0]->Location->Address);
          $check_additonal = array_key_exists('City', $Result[0]->Location->Address);
          if($check_city) {
            $cityname =  $Result[0]->Location->Address->District;
          } else {
            if($check_additonal){
              $cityname =  $Result[0]->Location->Address->City;
            } else{
              return response()->json([
              'status' => false,
              'message' => 'Enter correct Address!',
              'data' => '',
              ], 404);
            }
          }
          if($lat!='' && $long!=''){
             $user_updated = DB::table('users')
             ->where('id', $user_id)
             ->update(['latitude' => $lat, 'longitude' => $long, 'zipcode' => $PostalCode, 'country' => $countryname,'state' => $statename ,'city' => $cityname]);
          }
        }
      } else {
        return response()->json([
        'status' => false,
        'message' => 'Enter correct Address!',
        'data' => '',
        ], 404);
      }
    }
    /***************profile image******************/
    //profile image normal
    if($request->hasfile('profile_image')){
    $image = $request->file('profile_image');
    $name = time() . '.' . $image->getClientOriginalName();
    $image_resize = Image::make($image->getRealPath());
    $image_resize->resize(480, 400);
    $image_resize->save(public_path('images/profileimage/' .$name));
    $user_updated = DB::table('users')
    ->where('id', $user_id)
    ->update(['profile_image' => $name]);
    }
    //profile image with base64 decode after image cropping
    // if($request->profileimage!='') {
    //     $originalPath = public_path().'/images/profileimage/';
    //     $image_parts = explode(";base64,", $request->profileimage);
    //     $image_type_aux = explode("image/", $image_parts[0]);
    //     $image_type = $image_type_aux[1];
    //     $image_base64 = base64_decode($image_parts[1]);
    //     $filename = time().'.png';
    //     $file = $originalPath.$filename;
    //     file_put_contents($file, $image_base64);
    //     $user_updated = DB::table('users')
    //       ->where('id', $user_id)
    //       ->update(['profile_image' => $filename]);
    // }
    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['phone' => $phone, 'address' => $address_str,'bio'=>$bio,'additional_details'=>$additional_details,'price'=>$price,'name'=>$request->name, 'cpf_id' => $cpf_id, 'rg' => $rg]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();
    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }
  }
  public function user_documents_upload(Request $request){
    $rules = [
      'user_id' => 'required',
      'cpf_documents' => 'required',
    ];
    $messages = [
      'user_id.required'   =>  'User id is missing.',
      'cpf_documents.required'   =>  'User cpf_documents is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $user_id = $request->user_id;
    //document image
    if($request->hasfile('cpf_documents')){
      $file = $request->file('cpf_documents');
      foreach($request->file('cpf_documents') as $images ) {
        $names = time() . '-' . $images->getClientOriginalName();
        $destinationPath = public_path('images/user_document/');
        $images->move($destinationPath,$names);
        DB::table('user_documents')->insert(
        ['document' => $names, 'user_id' => $user_id]
        );
      }
    }
    $userdocumentData = DB::table('user_documents')
    ->where('user_id', $user_id)
    ->where('is_active',1)
    ->where('is_deleted',0)
    ->orderBy('id','desc')
    ->get();
    if($userdocumentData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userdocumentData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your document, Try Again',
      'data' => '',
      ], 500);
    }
  }
  public function user_gallery_upload(Request $request){
   
    $rules = [
    'user_id' => 'required',
    'user_images' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    'user_images.required'   =>  'User images is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
    return response()->json([
    'status' => false,
    'message' => 'Validation error.',
    'data' => $validator->errors(),
    ], 401);
    }
    $user_id = $request->user_id;

    if($request->hasfile('user_images')) {
     $images = $request->file('user_images');
     foreach($request->file('user_images') as $images ) {
      $names = time() . '.' . $images->getClientOriginalName();
      $image_resizes = Image::make($images->getRealPath());
      $image_resizes->resize(480, 400);
      $image_resizes->save(public_path('images/userimages/' .$names));
      DB::table('user_gallery')->insert(
      ['image_name' => $names, 'user_id' => $user_id]
      );
      }
    }

    $userGalleryData = DB::table('user_gallery')
    ->where('user_id', $user_id)
    ->where('is_active',1)
    ->where('is_deleted',0)
    ->orderBy('image_id','desc')
    ->get();
    if($userGalleryData) {
    return response()->json([
    'status' => true,
    'message' => 'Atualizado com sucesso!',
    'data' => $userGalleryData
    ], 200);
    } else {
    return response()->json([
    'status' => false,
    'message' => 'Failed to update your gallery, Try Again',
    'data' => '',
    ], 500);
    }
  }
public function convert_address(Request $request){
$rules = [
'address' => 'required',

];
$messages = [
'address.required'   =>  'User address is missing.',

];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => 'Validation error.',
'data' => $validator->errors(),
], 401);
}
$address_str = $request->address;
$zipcode ='';
$country_name ='';
$state_name ='';
$address = urlencode($address_str);
//  echo $address;die;
$url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".$address."&country=Brazil&language=en-US&additionaldata=&maxresults=20&gen=8";
$filestring = file_get_contents($url);
$filearray = explode("\n", $filestring);
$data = json_decode($filearray[0]);
//print_r($data);exit;
$array = json_decode(json_encode($data), True);
foreach ($array as $response => $res_value) {
foreach($res_value['View'] as $view_key=>$view_value){
for($i=0;$i<count($view_value['Result']);$i++){
$zipcode =$view_value['Result'][0]['Location']['Address']['PostalCode'];
$countryname = $view_value['Result'][0]['Location']['Address']['AdditionalData'][0]['value'];
if($countryname == 'Brazil' || $countryname == 'brazil'){
$country_name = 'Brasil';
} else {
$country_name  = $countryname;
}
$state_name = $view_value['Result'][0]['Location']['Address']['AdditionalData'][1]['value'];
}
}
}
$response = [];
if(!empty($data)){
$response = ['zipcode'=>$zipcode,'country_name' => $country_name,'state_name' => $state_name] ;
return response()->json([
'status' => true,
'message' => 'address converted successfully',
'data' => $response
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'address could not be converted',
'data' => '',
], 500);
}
}
public function delete_user_images(Request $request) {
$rules = [
'user_id' => 'required',
'image_id' => 'required',
'image_type' => 'required',
];
$messages = [
'user_id.required'   =>  'User id is missing.',
'image_id.required'   =>  'User image id is missing.',
'image_id.required'   =>  'User image type is missing.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => 'Validation error.',
'data' => $validator->errors(),
], 401);
}
$imageid = $request->image_id;
$user_id = $request->user_id;
$image_type = $request->image_type;
if($image_type == 'gallery'){
if($imageid){
$user_updated = DB::table('user_gallery')
->where('image_id', $imageid)
->update(['is_active' => '0', 'is_deleted' => '1']);
return response()->json([
'status' => true,
'message' => 'Gallery image removed successfully',
'data' => ''
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'Gallery image could not be removed',
'data' => '',
], 500);
}
} else {
if($imageid){
$user_updated = DB::table('user_documents')
->where('id', $imageid)
->update(['is_active' => '0', 'is_deleted' => '1']);
return response()->json([
'status' => true,
'message' => 'Document removed successfully',
'data' => ''
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'Document could not be removed',
'data' => '',
], 500);
}
}
}
  public function block_provider(Request $request){
    $rules = [
      'taker_id' => 'required',
      'provider_id' => 'required',
      'block_status' => 'required',
    ];
    $messages = [
      'taker_id.required'   =>  'Taker id is missing.',
      'provider_id.required'   =>  'Provider id is missing.',
      'block_status.required'   =>  'User block status is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $data = array('taker_id'     => $request->taker_id,
      'provider_id'  => $request->provider_id,
      'block_status' => $request->block_status,
      );
    $userblock = BlockProvider::where('taker_id','=',$request->taker_id)->where('provider_id','=',$request->provider_id)->first();

    if($userblock != ''){
      if($request->block_status == '1')
      {
      $ublock = '2';
      } else {
      $ublock = '1';
      }
      $unblocked =   BlockProvider::where('taker_id','=',$request->taker_id)->where('provider_id','=',$request->provider_id)->update(['block_status'=>$ublock]);
      if($unblocked)
      {
        if($request->block_status == '2') {
          $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',$request->taker_id)->count();
          if( $check_exist > 0 ) {
            $remove_whish = DB::table('wish_list')
            ->where('user_id',$request->taker_id)
            ->where('wish_profile_id', $request->provider_id)
            ->update(['is_active' => '1', 'is_deleted' => '0']);
          }
          return response()->json([
          'status' => true,
          'block_status' => '1',
          'message' => 'Cadastro do prestador de serviço enviado para verificação.',
          'data' => ''
          ], 200);
        } else {
          $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',$request->taker_id)->count();
          if( $check_exist > 0 ) {
            $remove_whish = DB::table('wish_list')
            ->where('user_id',$request->taker_id)
            ->where('wish_profile_id', $request->provider_id)
            ->update(['is_active' => '0', 'is_deleted' => '1']);
          }
          return response()->json([
          'status' => true,
          'block_status' => '2',
          'message' => 'Prestador de serviço desbloqueado.',
          'data' => ''
          ], 200);
        }
      } else {
        return response()->json([
        'status' => false,
        'message' => 'something wrong',
        'data' => '',
        ], 500);
      }
    } else {
      $result = BlockProvider::insert($data);
      if($result){
        $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',$request->taker_id)->count();
        if( $check_exist > 0 ) {
          $remove_whish = DB::table('wish_list')
          ->where('user_id', Auth::user()->id)
          ->where('wish_profile_id', $request->provider_id)
          ->update(['is_active' => '0', 'is_deleted' => '1']);
        }
        return response()->json([
        'status' => true,
        'message' => 'Prestador de serviço bloqueado.',
        'data' => ''
        ], 200);
      } else {
        return response()->json([
        'status' => false,
        'message' => 'something wrong',
        'data' => '',
        ], 500);
      }
    }
  }
public function user_close_account_request(Request $request)
{
  $rules = [
    'user_id' => 'required',
    'close_reason' => 'required',
  ];
  $messages = [
    'user_id.required'   =>  'User id is missing.',
    'close_reason.required'   =>  'User reason is missing.',
  ];
  $validator = Validator::make($request->all(), $rules);
  if($validator->fails()){
    return response()->json([
    'status' => false,
    'message' => $validator->errors(),
    'data' => 'Validation error.',
    ], 401);
  }
  $current_userId = $request->user_id;
  $close_reason   = $request->close_reason;
  $check_exist = DB::table('close_account_request')->where('user_id',$current_userId)->count();
  if($check_exist > 0){
    return response()->json([
        'status' => false,
        'message' => 'User close account request  already generated.',
        'data' => '',
        ], 400);

  } else {
    $close_account  = DB::table('close_account_request')->insertGetId(
            ['user_id' => $current_userId, 'close_reason' => $close_reason]
                  );
    if($close_account!=''){
      $close_accounts = DB::table('users')
            ->where('id', $current_userId)
            ->update(['is_active' => '0', 'close_request_action' => '1']);
      $notifie = DB::table('notifications')->insert(
        ['user_id' => $current_userId, 'notificaton_type' => 'closerequest','type_id' => $close_account]
      );
      return response()->json([
        'status' => true,
        'message' => 'User close account request generated successfully.',
        'data' => $user_id,
        ], 200);
    } else {
      return response()->json([
        'status' => false,
        'message' => 'User close account request  not generated',
        'data' => '',
        ], 500);
    }
  }
}
public function deactivate_user(Request $request){
$rules = [
'user_id' => 'required',
];
$messages = [
'user_id.required'   =>  'User id is missing.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => $validator->errors(),
'data' => 'Validation error.',
], 401);
}
$user_id = trim($request->user_id);
$user_update = DB::table('users')
->where('id', $user_id)
->update(['is_active' => '0']);
if($user_update) {
return response()->json([
'status' => true,
'message' => 'User deactivated successfully',
'data' => $user_id,
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'User not deactivated',
'data' => '',
], 500);
}
}
public function activate_user(Request $request){
$rules = [
'user_id' => 'required',
];
$messages = [
'user_id.required'   =>  'User id is missing.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => $validator->errors(),
'data' => 'Validation error.',
], 401);
}

$user_id = trim($request->user_id);
$user_update = DB::table('users')
->where('id', $user_id)
->update(['is_active' => '1']);
if($user_update) {
return response()->json([
'status' => true,
'message' => 'User activated successfully',
'data' => $user_id,
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'User not activated',
'data' => '',
], 500);
}
}
public function search_user(Request $request) {
$normal_userid = $request->user_id;
$user = $request->user;
if($user!='' && $normal_userid!='') {
$count = 0;
$pagination = 10;
$page = $request->page;
$lastpage = $page-1;
$user_show = $page*$pagination;
$lastusershow = $lastpage*$pagination;
$userdata = array();
$count = 0;
$user_info = DB::table('users')
->where('username','like', '%'. $user . '%')
->where('is_active', '1')
->where('user_type', 'bc')
->limit($pagination)
->offset($lastusershow)
->get();
foreach($user_info as $user){
$broadcaster_user_id = $user->id;
$blocked_usercount = DB::table('block_user')
->where('user_id', $broadcaster_user_id)
->where('block_user_id', $normal_userid)
->count();
if($blocked_usercount==''){
$golive_count = DB::table('golive')
->where('user_id', $broadcaster_user_id)
->where('live', '1')
->count();
$goliveinfo = DB::table('golive')
->where('user_id', $broadcaster_user_id)
->where('live', '1')
->first();
if($golive_count>0){
$goliveid = $goliveinfo->id;
$groupname = $goliveinfo->group_name;
}
else {
$goliveid = '';
$groupname = '';
}
$userdata[$count]['id'] = $user->id;
$userdata[$count]['name'] = trim($user->name);
$userdata[$count]['username'] = trim($user->username);
$userdata[$count]['email'] = trim($user->email);
$userdata[$count]['user_type'] = trim($user->user_type);
$userdata[$count]['profile_image'] = trim($user->profile_image);
$userdata[$count]['golive_id'] = $goliveid;
$userdata[$count]['group_name'] = $groupname;
$likecount = DB::table('broadcater_user_like')
->where('broadcaster_userid', $broadcaster_user_id)
->count();
$userdata[$count]['likecount'] = $likecount;
if($likecount>0){ $userdata[$count]['likeuser'] = '1'; }
else { $userdata[$count]['likeuser'] = '0'; }
++$count;
}
}
if($user_info){
return response()->json([
'status' => true,
'message' => 'User list',
'data' => $userdata,
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'User not list',
'data' => '',
], 200);
}
}
else {
return response()->json([
'status' => false,
'message' => 'User not list',
'data' => '',
], 200);
}
}
public function add_user_in_blacklist(Request $request){
$rules = [
'from_id'      => 'required',
'to_id'        => 'required',
'black_reason' => 'required',
'to_name'      => 'required',
'from_name'    => 'required',
'user_type'    => 'required',
];
$messages = [
'from_id.required'       =>  'Please enter your id.',
'to_id.required'         =>  'Please enter which user want to block.',
'black_reason.required'  =>  'Please enter your reason.',
'to_name.required'       =>  'Please enter blocker name.',
'from_name.required'     =>  'Please enter your name.',
'user_type.required'     =>  'Please enter your role.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => $validator->errors(),
'data' => 'Validation error.',
], 401);
}
$data = array('from_id'    => $request->from_id,
'to_id'=> $request->to_id,
'black_reason' => $request->black_reason,
'to_name' => $request->to_name,
'from_name' => $request->from_name,
'user_type' => $request->user_type,
);
$userblock = BlackUser::where('from_id','=',$request->from_id)->where('to_id','=',$request->to_id)->first();
if($userblock != ''){
if($userblock['black_status'] == '0') {
return response()->json([
'status' => false,
'black_status' => '0',
'message' => 'Solicitação de bloqueio já submetida.',
'data' => '',
], 400);
} elseif($userblock['black_status'] == '1') {
return response()->json([
'status' => false,
'black_status' => '1',
'message' => 'Solicitação de bloqueio já  accepted.',
'data' => '',
], 400);
} else {
return response()->json([
'status' => false,
'black_status' => '2',
'message' => 'Solicitação de bloqueio já rejected.',
'data' => '',
], 400);
}
} else {
$result = BlackUser::insert($data);
if($result){
return response()->json([
'status' => true,
'message' => 'Solicitação de bloqueio enviada para avaliação da nossa equipe.',
'data' => '',
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'something wrong.',
'data' => '',
], 400);
}
}
}
public function user_wishlist_response(Request $request){
$rules = [
'user_id'      => 'required',
'provider_id'  => 'required',
];
$messages = [
'user_id.required'       =>  'Please enter your id.',
'provider_id.required'   =>  'Please enter provider id.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => $validator->errors(),
'data' => 'Validation error.',
], 401);
}
$providerId = $request->provider_id;
$userId = $request->user_id;
$check_exist = DB::table('wish_list')->where('wish_profile_id',$request->provider_id)->where('user_id',$request->user_id)->count();
if( $check_exist > 0 ) {
$remove_whish = DB::table('wish_list')
->where('user_id', $userId)
->where('wish_profile_id', $providerId)
->delete();
if($remove_whish) {
return response()->json([
'status' => true,
'wish_response' => '0',
'message' => 'Prestador de serviço excluído da sua lista de seleção.',
'data' => '',
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'Something wrong.',
'data' => '',
], 400);
}
} else {
$status = 1;
$insert_whish = DB::table('wish_list')->insertGetId(
['user_id' => $userId, 'wish_profile_id' => $providerId,'status'=>$status]
);
if($insert_whish) {
return response()->json([
'status' => true,
'wish_response' => '1',
'message' => 'Prestadores de serviço adicionado na sua lista de seleção.',
'data' => '',
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'Something wrong.',
'data' => '',
], 400);
}
}
}

function getZipLatLong(Request $request){     //latlong according to zipcode
    $zipcode = $request->zipcode;
    $data = array();
    $myaddress ="";
    $city="";
    $state="";
    $country="";
    $neighbourhood="";
    $street_number="";
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&key=AIzaSyAtVchOE56ZJyqA-K9hM1WAevCeOhnsi30&sensor=false";
    $details=file_get_contents($url);
    $result = json_decode($details,true);
    $lat = @$result['results'][0]['geometry']['location']['lat'];
    $lang = @$result['results'][0]['geometry']['location']['lng'];

    if($result['status']=='ZERO_RESULTS') {
      return response()->json(['status'=>false,'data'=>'','error'=>'CEP não reconhecido. Favor mover o ponto vermelho no mapa abaixo até o seu endereço.'],400);
    }

    $newurl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lang."&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&country=BR&types=(cities)&language=pt_BR";
    $details_new=file_get_contents($newurl);
    $result_data = json_decode($details_new,true);
    $address = $result_data['results'][0]['address_components'];
          foreach($address as $adrs) {

              $address_type = $adrs['types'][0];

              
              
              switch ($address_type) {
                  case 'route':
                      $myaddress = $adrs['long_name'];
                      break;
                  case 'administrative_area_level_2':
                      $city = $adrs['long_name'];
                      break;
                  case 'administrative_area_level_1':
                      $state = $adrs['long_name'];
                      break;
                  case 'country':
                      $country = $adrs['long_name'];
                      break;
                  case 'political':
                      $neighbourhood = $adrs['long_name'];
                      break;
                  case 'street_number':
                      $street_number = $adrs['long_name'];
                      break;
              }
            }
             
          $all_data = array(
            'myaddress' => $myaddress,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'neighbourhood' => $neighbourhood,
            'street_number' => $street_number,
            'lat' => $lat,
            'lang' => $lang,
          );

          return response()->json(['status'=>true,'data'=>$all_data],200);
}



public function getUserWhislistDetails(Request $request)
{
$rules = [
'user_id'      => 'required',
//'lat'  => 'required',
//'lng'  => 'required',
];
$messages = [
'user_id.required' =>  'Please enter your id.',
//'lat.required'     =>  'Please enter your latitude.',
//'lng.required'     =>  'Please enter your longitude.',
];
$validator = Validator::make($request->all(), $rules);
if($validator->fails()){
return response()->json([
'status' => false,
'message' => $validator->errors(),
'data' => 'Validation error.',
], 401);
}
$userId = $request->user_id;
$lat = $request->lat;
$lng = $request->lng;
$userlatlng = User::where('id','=',$userId)->where('is_active','=','1')->first();
if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
$sys_latitude=$userlatlng->latitude;
$sys_longitude=$userlatlng->longitude;
} else {
$sys_latitude=$lat;
$sys_longitude=$lng;
}
$unit = "K";
$wish_listusers = DB::table('wish_list')
->select('users.id as providerId','users.name','users.profile_image','users.profile_rating','wish_list.*','category.cat_id','category.cat_name','provider_price_list.amount','price_queries.queries',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
->join('users','users.id','=','wish_list.wish_profile_id')
->leftjoin('category','category.cat_id','=','users.sub_id')
->leftjoin('provider_price_list','provider_price_list.user_id','=','users.id')
->leftjoin('price_queries','provider_price_list.query_id','=','price_queries.id')
->where('price_queries.priority', '1')
->where('wish_list.user_id', $userId)
->where('wish_list.status',1)
->orderBy('wish_list.wish_list_id')
->paginate(9);
if($wish_listusers) {
foreach ($wish_listusers as $wishuser) {
$wishuser->wishlist_status  = '1';
if($wishuser->distances !=''){
$wishuser->distances = number_format($wishuser->distances,2,",",".");
}
if($wishuser->amount !=''){
$wishuser->amount = number_format($wishuser->amount,2,",",".");
}
}
return response()->json([
'status'    => true,
'message'   => 'User wishlist details',
'data'      => $wish_listusers,
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'User no whislist detail founds.',
'data' => '',
], 400);
}
}
public function logout(Request $request){
$user_id = trim($request->user_id);
$user_count = DB::table('users')
->where('id', $user_id)
->count();
if($user_count>0) {
$user = DB::table('users')
->where('id', $user_id)
->update(['token' => '']);
if($user) {
return response()->json([
'status' => true,
'message' => 'User logout successfully!',
'data' => $user_id,
], 200);
} else {
return response()->json([
'status' => false,
'message' => 'User not logout successfully!',
'data' => $user_id,
], 200);
}
}
else {
return response()->json([
'status' => false,
'message' => 'User not logout successfully!',
'data' => $user_id,
], 200);
}
}

public function switchuserrole(Request $request)
{
  $user_id = $request->user_id;
  try{

    $userdetail = DB::table('users')->where('id','=',$user_id)->where('is_active','=','1')->first();
    $email = $userdetail->email;
    $password = trim($userdetail->hd_password);

    if($userdetail->user_role == 'Provider') {
      $Input =  [];
      $Input['user_role'] = 'Taker';
      $user_data = User::where('id', $user_id)->update($Input);
      $user_d = User::where('id', $user_id)->first();
      //$request->session()->put('success', 'Role Changed Successfully!');
      //return redirect('/logout');
      
     /* Auth::logout();
      if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
          // The user is active, not suspended, and exists.
        return redirect('/home');
      }*/
      return response()->json([
                  'status' => true,
                  'message' => 'Role Changed successfully!',
                  'data' => $user_d,
                  ], 200);

    } else if($userdetail->user_role == 'Taker'){
      $Input =  [];
      $Input['user_role'] = 'Provider';
      $user_data = User::where('id', $user_id)->update($Input);
      $user_d = User::where('id', $user_id)->first();
      //$request->session()->put('success', 'Role Changed Successfully!');
      //return redirect('/logout');
      /*Auth::logout();
      if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
          // The user is active, not suspended, and exists.
        return redirect('/home');
      }*/
      return response()->json([
                  'status' => true,
                  'message' => 'Role Changed successfully!',
                  'data' => $user_d,
                  ], 200);

    }
  
  }
  catch(\Illuminate\Database\QueryException $e){
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
    $errorlog->loggedin_id      = Auth::user()->id;
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror'));
  }catch(\Exception $e){
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
    $errorlog->loggedin_id      = Auth::user()->id;
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror'));
  }
}

public function updateprofiledata(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $phone = $request->phone;
    $user_id = $request->user_id;
    $cpf_id = $request->cpf_id;
    $rg = $request->rg;
    $name = $request->name;

    if(!empty($cpf_id)){
       $avail =  DB::table('users')->where('cpf_id', $request->cpf_id )->where('id','!=',$request->user_id)->first();
      if($avail){
      return response()->json([
      'status' => false,
      'message' => 'This cpf number already exists',
      'data' => '',
      ], 404);
      }
    }

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['phone' => $phone,'name'=>$request->name,'cpf_id' => $cpf_id,'rg' => $rg]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

public function updateprofilephone(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $phone = $request->phone;
    $user_id = $request->user_id;

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['phone' => $phone]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

public function updateprofilename(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $name = $request->name;
    $user_id = $request->user_id;

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['name' => $name]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

public function updateprofilecpf(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $cpf_id = $request->cpf_id;
    $user_id = $request->user_id;

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['cpf_id' => $cpf_id]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

public function updateprofilerg(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $rg = $request->rg;
    $user_id = $request->user_id;

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['rg' => $rg]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

public function updateprofiledob(Request $request)
{
  $rules = [
    'user_id' => 'required',
    ];
    $messages = [
    'user_id.required'   =>  'User id is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
      'status' => false,
      'message' => 'Validation error.',
      'data' => $validator->errors(),
      ], 401);
    }
    $dob = $request->dob;
    $user_id = $request->user_id;

    $user_updated = DB::table('users')
    ->where('id', $user_id)->update(['dob' => $dob]);
    $userData = DB::table('users')
    ->where('id', $user_id)
    ->first();

    if($userData) {
      return response()->json([
      'status' => true,
      'message' => 'Atualizado com sucesso!',
      'data' => $userData,
      ], 200);
    } else {
      return response()->json([
      'status' => false,
      'message' => 'Failed to update your profile, Try Again',
      'data' => '',
      ], 500);
    }

}

}