<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\MainCategory;
use App\Models\Category;
use App\Models\Booking;
use App\Models\Feedback;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use Validator;
use URL;
use Log;
use DB;
// DB::enableQueryLog();  
// dd(DB::getQueryLog()); exit;

class BookingController extends Controller {

  public function get_pending_booking(Request $request) {
    $rules = [
       'user_id'   => 'required',
       'user_role' => 'required',
    ];
    $messages = [
      'user_id.required'     =>  'User id is missing.',
      'user_role.required'   =>  'User role is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
    }
    $user_id = trim($request->user_id);
    $user_role = trim($request->user_role);
    if($user_role =='Provider'){
      $pending_booking = Booking::select('booking.*','users.name as taker_name','users.profile_image as taker_image')->leftjoin('users', 'users.id', '=', 'booking.user_id')->where('booking.profile_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '0')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_confirm_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
      $booking_denied_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
    } else {
      $pending_booking = Booking::select('booking.*','users.name as provider_name','users.profile_image as provider_image')->leftjoin('users', 'users.id', '=', 'booking.profile_id')->where('booking.user_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '0')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_confirm_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
      $booking_denied_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
    }
    if($pending_booking) {
      return response()->json([
                  'status'  => true,
                  'message' => 'Pending booking list.',
                  'data'    => $pending_booking,
                  'booking_denied_count' => $booking_denied_count,
                  'booking_confirm_count' => $booking_confirm_count,
              ], 200);
    } else {
      return response()->json([
                  'status' => false,
                  'message' => 'No pending booking found.',
                  'data' => '',
              ], 404);
          }
  }
  public function get_confirmed_bookings(Request $request) {
    $rules = [
       'user_id'   => 'required',
       'user_role' => 'required',
    ];
    $messages = [
      'user_id.required'     =>  'User id is missing.',
      'user_role.required'   =>  'User role is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
    }
    $user_id = trim($request->user_id);
    $user_role = trim($request->user_role);
    if($user_role =='Provider'){
      $confirmed_booking = Booking::select('booking.*','users.name as taker_name','users.profile_image as taker_image','category.cat_image')->leftjoin('users', 'users.id', '=', 'booking.user_id')->leftjoin('category', 'users.sub_id', '=', 'category.cat_id')->where('booking.profile_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '1')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_pending_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
      $booking_denied_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
    } else{
      $confirmed_booking = Booking::select('booking.*','users.name as provider_name','users.profile_image as provider_image','category.cat_image')->leftjoin('users', 'users.id', '=', 'booking.profile_id')->leftjoin('category', 'users.sub_id', '=', 'category.cat_id')->where('booking.user_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '1')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_pending_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
      $booking_denied_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '2')
                      ->count();
      if(count($confirmed_booking) > 0){
        foreach($confirmed_booking as $confirmedData){
          $feedbacks = DB::table('feedback')->where('user_id',$confirmedData->user_id)->where('booking_id',$confirmedData->booking_id)->where('provider_id',$confirmedData->profile_id)->first();
          if($feedbacks){
            $confirmedData['feedback_status'] = $feedbacks->approve_status;
          } else{
             $confirmedData['feedback_status'] = '';
          }
          $userblock = DB::table('block_providers')->where('taker_id','=',$confirmedData->user_id)->where('provider_id','=',$confirmedData->profile_id)->first();
          if($userblock){
            $confirmedData['block_status'] = $userblock->block_status;
          } else{
             $confirmedData['block_status'] = '0';
          }
          
        }
      }
    }
    if($confirmed_booking) {
      return response()->json([
                  'status'  => true,
                  'message' => 'Confirmed booking list.',
                  'data'    => $confirmed_booking,
                  'booking_denied_count' => $booking_denied_count,
                  'booking_pending_count' => $booking_pending_count,
              ], 200);
    } else {
      return response()->json([
                  'status' => false,
                  'message' => 'No confirmed booking found.',
                  'data' => '',
              ], 404);
          }
  }
  public function get_denied_bookings(Request $request) {
    $rules = [
       'user_id'   => 'required',
       'user_role' => 'required',
    ];
    $messages = [
      'user_id.required'     =>  'User id is missing.',
      'user_role.required'   =>  'User role is missing.',
    ];
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return response()->json([
                'status' => false,
                'message' => 'Validation error.',
                'data' => $validator->errors(),
            ], 401);
    }
    $user_id = trim($request->user_id);
    $user_role = trim($request->user_role);
    if($user_role =='Provider'){
      $denied_booking = Booking::select('booking.*','users.name as taker_name','users.profile_image as taker_image')->leftjoin('users', 'users.id', '=', 'booking.user_id')->where('booking.profile_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '2')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_confirm_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
      $booking_pending_count = DB::table('booking')
                      ->where('profile_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
    } else{
      $denied_booking = Booking::select('booking.*','users.name as provider_name','users.profile_image as provider_image')->leftjoin('users', 'users.id', '=', 'booking.profile_id')->where('booking.user_id', $user_id)->where('booking.is_active', '1')->where('booking.is_deleted', '0')->where('booking.booking_verified', '2')->orderBy('booking.booking_id','DESC')->paginate('5');
      $booking_confirm_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '1')
                      ->count();
      $booking_pending_count = DB::table('booking')
                      ->where('user_id', $user_id)
                      ->where('is_active', '1')
                      ->where('is_deleted', '0')
                      ->where('booking_verified', '0')
                      ->count();
    }
    if($denied_booking) {
      return response()->json([
                  'status'  => true,
                  'message' => 'Denied booking list.',
                  'data'    => $denied_booking,
                  'booking_pending_count' => $booking_pending_count,
                  'booking_confirm_count' => $booking_confirm_count,
              ], 200);
    } else {
      return response()->json([
                  'status' => false,
                  'message' => 'No denied booking found.',
                  'data' => '',
              ], 404);
          }
  }
}