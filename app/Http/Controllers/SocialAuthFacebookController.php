<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Redirect;
use Auth;
use Exception;
use Session;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ErrorsClass;

class SocialAuthFacebookController extends Controller
{
    public function logindb(Request $request)
    {
      //print_r($request->all());die;
     
        $authUser = User::where('email', $request->email)->first();
        //$authUser = User::where('facebook_id', $request->facebook_id)->first();

        if ($authUser){
           if($request->profile_image){
            $fbUrl = $request->profile_image;
            //echo ltrim($fbUrl,"&height=50&width=50");die;
          } else {
            $fbUrl = 'http://graph.facebook.com/'.$request->facebook_id.'/picture?height=350&width=250';
          }
          //$g= '&height=50&width=50';
          //$fbUrl = str_replace($g,'',$fbUrl);
         
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $fbUrl);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          $data = curl_exec($ch);
          curl_close($ch);
          $fileName = time().'_fb_profilepic.jpg';
          $path = 'public/images/profileimage/'.$fileName;
          $file = fopen($path, 'w+');
          fputs($file, $data);
          fclose($file);
           //file_put_contents($path, $fbUrl);
          $user_info = User::where('facebook_id', $request->facebook_id)
          ->update(['profile_image' => $fileName]);
            
           Auth::loginUsingId($authUser->id);
            //$request->session->put('id', $dd);
            if($authUser->is_active==1){
              if($authUser->user_role == 'Provider'){
                  if(!empty($authUser->phone) && !empty($authUser->address)&&!empty($authUser->cat_id)){
                    echo '/serviceprovider/dashboard';
                  }
                  else{
                    echo '/serviceprovider/completeprofile';
                  }
              }
              else if($authUser->user_role == 'Taker'){
                if(!empty($authUser->phone) && !empty($authUser->address)){
                  echo '/servicetaker/dashboard';
                }
                else{
                  echo '/servicetaker/profile';
                }
              }
            }
        } else {
          if($request->profile_image){
            $fbUrl = $request->profile_image;
          } else {
            $fbUrl = 'http://graph.facebook.com/'.$request->facebook_id.'/picture';
          }
          // $g= '&height=50&width=50';
          // $gs= '&height=100&width=100';
          // $fbUrl = str_replace($g,$gs,$fbUrl);
          
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $fbUrl);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          $data = curl_exec($ch);
          curl_close($ch);
          $fileName = time().'_fb_profilepic.jpg';
          $path = 'public/images/profileimage/'.$fileName;
          $file = fopen($path, 'w+');
          fputs($file, $data);
          fclose($file);

          

          $authsave =  User::insertGetId([
            'name' => $request->name,
            'email' => $request->email,
            'facebook_id' => $request->facebook_id,
            'profile_image' => $fileName,
            'user_role' => 'Taker',
            'is_active' => '1',
             ]);
          if($authsave){
            
            Auth::loginUsingId($authsave);
            //$request->session->put('id',$dd);
            echo '/servicetaker/profile';

          }
        }
    }
    public function facebookredirect()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

     public function facebookcallback()
    {
        try {
            
        $facebookUser = Socialite::with ('facebook')->stateless()->user ();
         $existUser = User::where('email',$facebookUser->email)->first();
            if($existUser) {
               echo  Auth::loginUsingId($user->id);
            }
            else {
                $user = new User;
                $user->name = $facebookUser->name;
                $user->email = $facebookUser->email;
                $user->facebook_id = $facebookUser->id;
                $user->user_role = 'Taker';
                $user->password = md5(rand(1,10000));
                $user->save();
                Auth::loginUsingId($user->id);
            }
           /* return redirect()->to('/home');*/
           return redirect( '/home' )->withDetails ( $user )->withService ('facebook');
        
        }  
        catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       } 
    }
}
