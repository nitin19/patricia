<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class WishlistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      try{
        $ip = $_SERVER['REMOTE_ADDR'];
        $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
        $sys_latitude = $new_arr[0]['geoplugin_latitude'];
        $sys_longitude = $new_arr[0]['geoplugin_longitude'];
        $unit = "K";
        $login_userId = Auth::id();
        $wish_listusers = DB::table('wish_list')->join('users','users.id','=','wish_list.wish_profile_id')->where('user_id', $login_userId)->where('status',1)
        ->paginate(6);
        $distance_array = [];
        foreach($wish_listusers as $wishlist ){
          $user_lat = $wishlist->latitude;
          $user_lon = $wishlist->longitude;
          $distance  =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
          // echo $distance;
          $distance_array[$wishlist->name] = $distance;
        };
        // echo "<pre>";
        // print_r($distance_array['provider work']);die;
          return view('pages.wishlist', compact('wish_listusers', 'wish_listipusers','distance_array'))->with('i', ($request->input('page', 1) - 1) * 6);;
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();
            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        }
    }
    public function addwhish(Request $request){
    // echo "<pre>";
    // print_r($request->all());die;
    $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->userid)->where('user_id',Auth::user()->id)->first();
    if(empty($check_exist)){
       $login_userId = Auth::id();
       $userid = $request->userid;
       $status = 1;
       $insert_whish = DB::table('wish_list')->insert(
                       ['user_id' => $login_userId, 'wish_profile_id' => $userid,'status'=>$status]
                       );
       if($insert_whish!=''){
        echo 'true';
       }
    }
    else{
       if($check_exist->status == 0){
           $login_userId = Auth::id();
           $userid = $request->userid;
           $status = 1;
           // $update = DB::table('wish_list')->update(
           //             ['user_id' => $login_userId, 'wish_profile_id' => $userid,'status'=>$status]
           //             );
           $update = DB::table('wish_list')
                    ->where('user_id', $login_userId)
                    ->where('wish_profile_id', $userid)
                    ->update(['status'=>$status]);
           if($update!='')
           {
            echo 'true';
           }
       }
    }

    }

    public function removewhish(Request $request){
        $check_exist = DB::table('wish_list')->where('wish_profile_id',$request->userid)->where('user_id',Auth::user()->id)->first();
         $response = [];
          $check_exist_active_status = DB::table('wish_list')->where('wish_profile_id',$request->userid)->where('user_id',Auth::user()->id)->where('status',1)->count();
            if(!empty($check_exist)){
                 $login_userId = Auth::id();
                 $userid = $request->userid;
                 if($check_exist->status == 1){
                     $status = 0;
                  }
                 $remove_whish = DB::table('wish_list')
                                  ->where('user_id', $login_userId)
                                  ->where('wish_profile_id', $userid)
                                  ->update(['status'=>$status]);

                if($remove_whish!=''){
                    $response['success'] = "true";
                    $response['count'] =$check_exist_active_status;
                }
                else{
                  $response['success'] = "false";
                   $response['count'] =$check_exist_active_status;
                }
                echo json_encode($response);
            }
        }


    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
      if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
      }
      else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
          return $miles;
        }
      }
}

}
