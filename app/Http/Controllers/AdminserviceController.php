<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
class AdminserviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
      try{
          /* Count */
         $category_count = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   ->count();
            /* Count */
            /* Listing */ 

          $category = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   ->orderBy('category.cat_id','desc')
                   ->get();  
                  
          return view('admin.services.index',compact('category_count', 'category'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function create(Request $request){
      try{
        $main_category =  DB::table('main_category')->where('is_active', '1')
                   ->where('is_deleted', '0')->pluck('name','id');
        // echo "<pre>";
        // print_r($sub_category);die;
        return view('admin.services.create',compact('main_category'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function store(Request $request){
      try{
        // echo "<pre>";
        // print_r($request->all());die;
        $main_category_id = $request->main_category_id;
        $cat_name = $request->service_name;
        $cat_description = $request->service_description;
        if($request->hasfile('service_img')){
          $image = $request->file('service_img');
          $name = time() . '.' . $image->getClientOriginalName(); 
          $image_resize = Image::make($image->getRealPath());
          $image_resize->resize(40, 40);
          $image_resize->save(public_path('images/categoryimage/' .$name));
          $image_resize_100 = Image::make($image->getRealPath());
          $image_resize_100->resize(100, 100);
          $image_resize_100->save(public_path('images/category_home/' .$name)); 
          if(!empty($cat_description))
          {     
            $category_insert = DB::table('category')->insert(
            ['cat_name' => $cat_name, 'cat_description' => $cat_description, 'cat_image' => $name,'main_cat_id'=>$main_category_id]);
          }
          else
          {
            $category_insert = DB::table('category')->insert(
            ['cat_name' => $cat_name,'cat_image' => $name,'main_cat_id'=>$main_category_id]);
          }
         }
        else {
          if(!empty($cat_description))
          {
              $category_insert = DB::table('category')->insert(
                ['cat_name' => $cat_name, 'cat_description' => $cat_description,'main_cat_id'=>$main_category_id]);
          }
          else
          {
              $category_insert = DB::table('category')->insert(
                ['cat_name' => $cat_name,'main_cat_id'=>$main_category_id]);
          }
        }
        return Redirect('/admin-services')->with('success', 'Service Added Successfully!');
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function edit($id){
      try{
          $category = DB::table('category')
                 ->where('is_active', '1')
                 ->where('is_deleted', '0')
                 ->where('cat_id', $id)->first(); 
          $main_category =  DB::table('main_category')->where('is_active', '1')
                   ->where('is_deleted', '0')->pluck('name','id'); 
        return view('admin.services.edit', compact('category','main_category'));  
      }     
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }  
    }
    public function update(Request $request){
      try{
             $service_name = $request->service_name;
             $service_img = $request->service_img;
             $service_id = $request->service_id;
             $main_catid = $request->main_cat_id;
             $service_description = $request->service_description;
            
            if($request->hasfile('service_img')){
            $image = $request->file('service_img');
            $name = time() . '.' . $image->getClientOriginalName(); 
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(40, 40);
            $image_resize->save(public_path('images/categoryimage/' .$name));
                  $image_resize_100 = Image::make($image->getRealPath());
            $image_resize_100->resize(100, 100);
            $image_resize_100->save(public_path('images/category_home/' .$name));
              $category_update = DB::table('category')
                  ->where('cat_id', $service_id)
                  ->update(['cat_name' => $service_name,'main_cat_id' => $main_catid, 'cat_description' => $service_description, 'cat_image' => $name]);
           }
           else {
            $category_update = DB::table('category')
                  ->where('cat_id', $service_id)
                  ->update(['cat_name' => $service_name,'main_cat_id' => $main_catid, 'cat_description' => $service_description]);
           }
           return Redirect('/admin-services')->with('success', 'Service Updated  Successfully!');
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     public function show(Request $request){
      try {

        
      } catch (\Illuminate\Database\QueryException $e) {

                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {

                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
        }   
     }
     public function category_remove(Request $request){
      $catid = $request->catid;
      $checkCategory = DB::table('users')->where('sub_id', '=', $catid)->count();
      if( $checkCategory > 0 ) {
        echo '0';
      }
      else
      {
        $remove_cat = DB::table('category')->where('cat_id', $catid)->delete();
        if($remove_cat!=''){
          echo '1';
        }
      }
     }

     public function showallcategories(Request $request)
    {
      try{
          /* Count */
         $category_count = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   ->count();
            /* Count */
            /* Listing */ 

          $category = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   //->orderBy('category.cat_id','desc')
                   ->orderBy('main_category.name','ASC')
                   ->get();  
                  
          return view('showcategories',compact('category_count', 'category'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
}