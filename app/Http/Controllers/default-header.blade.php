<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>@yield('title')</title>
  <meta name="description" content="@yield('description')">
  <meta name="keywords" content="@yield('keywords')">
  <meta name="google-site-verification" content="kLCDUJcsae0_90l4V3JispjYD4DzQldTXpoPmX66_ww" />
  <link rel="shortcut icon" type="image/x-icon" href="{{ url('/public') }}/images/icon.png" />
  <!-- Bootstrap -->
  <link href="{{ url('/public')}}/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('/public')}}/css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ url('/public')}}/css/mapsjs-ui.css"/>
  <link href="{{url('/public')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ url('/public') }}/css/jquery-ui.css">
  <link href="{{ url('/public')}}/css/dropdown.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap-multiselect.css" type="text/css">
  <link href="{{ url('/public')}}/css/change.css" rel="stylesheet">
  <link href="{{ url('/public')}}/css/new.css" rel="stylesheet">
  <link href="{{ url('/public')}}/css/table-css.css" rel="stylesheet"> 
  <link href="{{ url('/public')}}/css/lightbox.min.css" rel="stylesheet"> 
  <link href="{{ url('/public') }}/css/animate.css" rel="stylesheet">
  
  <link href="{{ url('/public/') }}/css/bootstrap-datetimepicker.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ url('/public')}}/css/other.css" />
  <link href="{{ url('/public') }}/css/jquery.raty.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.min.js"></script>
  <script src="{{ url('/public') }}/js/jquery-ui.min.js"></script>
  <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-core.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-service.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-ui.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/mapsjs-mapevents.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/bootstrap-multiselect.js"></script>
 

  
  <script src="{{ url('/public') }}/js/bootstrap-notify.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
  <script src="{{ url('/public') }}/js/additional-methods.min.js"></script>
  <script type="text/javascript" src="{{ url('/public') }}/js/wow.min.js"></script>  
   <script type="text/javascript" src="{{ url('/public') }}/js/lightbox.min.js"></script>  
  <script type="text/javascript" src="{{ url('/public/') }}/js/moment.js"></script>
  <script type="text/javascript" src="{{ url('/public/') }}/js/bootstrap-datetimepicker.js"></script>
  <script src="{{ url('/public/') }}/js/jquery.raty.js"></script> 
  <script src="{{ url('/public/') }}/js/jquery-confirm.min.js"></script>
  
 
 
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135930218-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135930218-1');
  </script>
  <style type="text/css">
  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
</style>
</head>
<body>
  <header class="site-header" id="myHeader">
    <div class="top_header">
      <div class="container-fluid">
        <div class="col-sm-6 leftheader">
          @if(empty(Auth::user()))
          <p style="">    
            <span>Você é um prestador de serviço?</span><a class="" href="{{ url('/register?v=sp') }}" >Cadastre-se!</a>
          </p> 
          @endif
        </div>
        <div class="col-sm-6 rightheader">
          <?php
            $business_info = DB::table('business_settings')
                ->join('users','business_settings.user_id','=','users.id')
                ->where('users.user_role','=','admin')
                ->select('business_settings.*','users.user_role','users.id')->first();
          ?>   
          <ul class="navbar-right">
            <!-- <span>Follow Us</span> -->
            <li class="tg-facebook"><a href="{{ $business_info->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li class="tg-instagram"><a href="{{ $business_info->instagram_link }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li class="tg-linkedin"><a href="{{ $business_info->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <nav class="navbar navbar-default navigationbar">
        @if (Auth::check())
        @if(Auth::user()->user_role == 'Provider')
        @include('layouts.provider_header')
        @elseif(Auth::user()->user_role == 'Taker')
        @include('layouts.taker_header')
        @else
        <input type="hidden" value="{{Auth::user()->user_role}}" name="admin_role_check" id="check_role">
        @endif
        @else
        @include('layouts.login_header')
        @endif
      </nav>
    </header>
    <main> @yield('content') </main>

    <footer> @include('layouts.footer') </footer>

    <script>
      $(document).ready( function() {
        if($('#check_role').val() == 'admin'){
         window.location.href="{{ url('/admin-dashboard') }}";
       }
       $('.success_show_ip').hide();
       $('.success_show').hide();
       $('.success_role').hide();
       $('.success_request').hide();
       function removewishlist(userid){
          var home_url = '{{url("/home")}}'
          $.ajax({
            url: "{{ url('/wishlist/removewhish') }}",
            type: 'POST',
            data: { "_token": "{{ csrf_token() }}",'userid': userid },
            success: function(response) {
              if(response =='1') {
                $.notify({
                  message: 'Prestador de serviço removido da sua lista de seleção.' 
                  },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                })
                $('#unwish_'+userid).remove(); 
                
                var pageno = '<?php echo Request::getQueryString(); ?>'.match(/\d+/);
                if(pageno[0]>1)
                {
                  if($('.searchresultdiv').length == 0){
                    var prepage = pageno[0]-1;
                    var current_location = "https://www.navizinhanca.com/wishlist?page=";
                    var newlocation_url = current_location.concat(prepage);
                    window.location.replace(newlocation_url);
                  }
                }
                else(pageno[0]==1)
                {
                  if($('.searchresultdiv').length == 0){
                    location.reload();
                  }
                }

                }
              }
            });
       }
       $('.removewhishlist').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.confirm({
        title: '',
        content: 'Você deseja remover o prestador de serviço da sua lista de desejos?',
        buttons: {
            confirme: function () {
                removewishlist(userid);
            },
            Cancelar: function () {
                //return false;
              }
          }
        });
      });
       $('.removewhishlist_profile').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/removewhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') { 
              $.notify({
                message: 'Prestador de serviço excluído da sua lista de seleção.',
                },{
                  type: 'success',
                  delay: 7000,
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              });
              window.setTimeout(function(){location.reload()},3000)
            }
          }
        });
      });
       $('.addwhishlists').click(function(){
        var parent_id = $("#whishlist_id").attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') {
              window.setTimeout(function(){location.reload()},3000)
              $.notify({
                message: 'Prestadores de serviço adicionado na sua lista de seleção',
                },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  delay: 7000,
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              })
            }
          }
        });
      });
       $('.addwhishlist').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        <?php if(!empty(Auth::user())) { if(Auth::user()->user_role =='Provider') {?>
          $.notify({
                message: 'Se você quiser adicionar o provedor em uma lista de desejos, por favor, faça o login como comprador por mudar o seu papel.'               
                },{
                  type: 'info',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  }
                  
              });

        <?php } else{?>
        $.ajax({
          url: "{{ url('/wishlist/addwhish') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid },
          success: function(response) {
            if(response=='1') {
              $('.success_show').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist");
              $("#"+parent_id+' i').addClass("alreadyadd");
              $.notify({
                message: 'Prestadores de serviço adicionado na sua lista de seleção. Clique aqui para ver sua lista completa.',
                url: '{{ url("/wishlist") }}',
                },{
                  type: 'success',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                  url_target: '_self',
              });
            } else if(response=='2') {
            } else {
              $.notify({
                message: 'Provider Already Added To Your Wishlist. Click Here To Check',
                url: '{{ url("/wishlist") }}',
                },{
                  type: 'danger',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
                  url_target: '_self',
              });
            }
          }
        });
      <?php } } else { ?>
         window.location.href="{{ url('/login') }}";
      <?php } ?>
      });
       $('.addwhishlist_ip').click(function(){
        var parent_id = $(this).closest('div').attr('id');
        var userid = jQuery('#'+parent_id+' #userid').val();
        var userip = jQuery('#'+parent_id+' #userip').val();
        <?php if(!empty(Auth::user())) { if(Auth::user()->user_role =='Provider') {?>
          $.notify({
                message: 'Se você quiser adicionar o provedor em uma lista de desejos, por favor, faça o login como comprador por mudar o seu papel.'               
                },{
                  type: 'info',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  }
                  
              });

        <?php } else{?>
        $.ajax({
          url: "{{ url('/wishlistip/addwhish_ip') }}",
          type: 'POST',
          data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
          success: function(response) {
            if(response=='1') {
              $('.success_show').show();
              $('.success_show_ip').show();
              $("#"+parent_id+' i').removeClass("fa-heart-o");
              $("#"+parent_id+' i').addClass("fa-heart");
              $("#"+parent_id+' i').removeClass("addwhishlist_ip");
                $.notify({
                message: 'Provider Added To Your Wishlist. To Check Wishlist Please Create Account or Login Into Your Account.',
                },{
                  type: 'info',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              });
              }
              else if(response=='2') {
              }
              else
              {
                $.notify({
                message: 'Already Added To Your Wishlist. To Check Wishlist Please Create Account or Login Into Your Account.',
                },{
                  type: 'danger',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  },
              });
              }
            }
          });
        <?php } } else { ?>
         window.location.href="{{ url('/login?pId=') }}"+userid;
      <?php } ?>

      });
       $('.removewhishlist_ip').click(function(){
        <?php if(!empty(Auth::user())) { if(Auth::user()->user_role =='Provider') {?>
          $.notify({
                message: 'Se você quiser adicionar o provedor em uma lista de desejos, por favor, faça o login como comprador por mudar o seu papel.'               
                },{
                  type: 'info',
                  offset: 
                  {
                    x: 10,
                    y: 130
                  },
                  animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                  }
                  
              });

        <?php } else{?>
        if(confirm("Are you sure?")){
              var parent_id = $(this).closest('div').attr('id');
              var userid = jQuery('#'+parent_id+' #userid').val();
              var userip = jQuery('#'+parent_id+' #userip').val();
              $.ajax({
                url: "{{ url('/wishlistip/removewhish_ip') }}",
                type: 'POST',
                data: { "_token": "{{ csrf_token() }}",'userid': userid,'userip': userip },
                success: function(response) {
                  if(response=='1') {
                    $.notify({
                      message: 'User Removed From Wish List' 
                      },{
                      type: 'success',
                      offset: 
                      {
                        x: 10,
                        y: 130
                      },
                      animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                      },
                    })
                    $('#unwish_'+userid).hide();
                  }
                }
              });
        }
        <?php } } else { ?>
         window.location.href="{{ url('/login') }}";
      <?php } ?>
      });
       $.validator.addMethod("alphaLetterNumber", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
      });
       $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
        return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
      });
       $.validator.addMethod("alphaLetter", function(value, element) {
        return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
      });
       $("#loginform").validate({
        rules: {
          email: {
            required: true,
            email: true,
            maxlength: 120
          },
          password: {
            required: true
          }
        },
        messages: {
          email: {
           
            required: "Favor preencher seu e-mail.",
            email: "Favor digitar um endereço de email válido.",
            maxlength: "Máximo de 120 caracteres permitidos."
            },
            password: {
              required: "Favor preencher o campo acima."
            }
          },
          submitHandler: function(form) {
            form.submit();
          }
        });
       $("#contactform").validate({
        rules: {
          first_name: {
            required: true
          },
          last_name: {
            required: true
          },
          email: {
            required: true,
            email: true
          },
          phonenumber: {
            required: true,
            minlength:13,
            maxlength: 14
          },
          comment: {
            required: true
          }
        },
        messages: {
          first_name: {
            required: " Favor preencher seu nome (sem sobrenome)."
          },
          last_name: {
            required: "Favor preencher seu sobrenome."
          },
          email: {
            required: "Favor preencher seu e-mail.",
            email: "E-mail incorreto"
          },
          phonenumber: {
            required: " Favor preencher o número de celular.",
            minlength: "Telefone celular com 11 digitos e obrigatorio.",
            maxlength: "Maximum 11 Number allowed."
          },
          comment: {
            required: "Favor preencher seu mensagem."
          }
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
       $("#carddetailform").validate({
        rules: {
          paymentname: {
            required: true
          },
          card_num: {
            required:true,
            minlength:16,
            maxlength:20
          },
          exp_month:{
            required: true
          },
          exp_year:{
            required : true
          },
          cvv:{
            required:true,
            minlength: 3,
            maxlength: 4
          }
        },
        messages: {
          paymentname: {
            required: "Favor preencher seu nome."
          },
          card_num: {
            required: "Please enter card Number",
            minlength: "Telefone celular com 16 digitos e obrigatorio.",
            maxlength: "Maximum 20-digits are required."
          },
          exp_month: {
            required: "Please select expiry month of card."
          },
          exp_year: {
            required: "Please select expiry year of card."
          },
          cvv: {
            required:"Please enter your cvv",
            minlength: "Telefone celular com 3 digitos e obrigatorio.",
            maxlength: "Maximum 4-digits are required."
          }
        },
        submitHandler: function(form) {
          $.ajax({
            url: "{{ url('/dashboard/save_card') }}",
            type: 'POST',
            data: $(form).serialize(),
            success: function(response) {
              if(response == 'true')
              {
                    $.notify({
                    message: 'Account Info Saved Successfully!',
                    },{
                    type: 'success',
                    offset: 
                    {
                      x: 10,
                      y: 130
                    },
                    animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
                    },
                  });
              }
            }
          });
        }
      });
       wow = new WOW(
       {
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
      );
       wow.init();
      (function(){
        $('#serviceslider').carousel({ interval: 5000 });
      }());
      (function(){
        $('.carousel-showmanymoveone .item').each(function(){
          var itemToClone = $(this);
          for (var i=1;i<16;i++) {
            itemToClone = itemToClone.next();
            // wrap around if at end of item collection
            if (!itemToClone.length) {
              itemToClone = $(this).siblings(':first');
            }
            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
            .addClass("cloneditem-"+(i))
            .appendTo($(this));
          }
        });
      }());
      jQuery('#uploadbtn').on('click', function() {
        jQuery('#brwsebtn').click();
      });

      jQuery("#brwsebtn").change(function() {
        var file = this.files[0];
        // console.log(file);
        var imagefile = file.type;
        var imageTypes= ["image/jpeg","image/png","image/jpg"];

        if(imageTypes.indexOf(imagefile) == -1)
        {
          jQuery(".image123_alert").html("<div class='alert alert-danger' role='alert'><span class='msg-error'>Please Select A valid Image File Only jpeg, jpg and png Images type allowed</span></div>");
          $('.image123_alert').show();
          $('.image123_alert').css("opacity", "1");
          $('.next_1').prop('disabled','true');
          return false;
        }
        else
        {
          var reader = new FileReader();
          reader.onload = function(e){
            jQuery("#previewdiv").html('<img src="' + e.target.result + '" style="width:100px;height:100px;"/>');
          };
          reader.readAsDataURL(this.files[0]);
          $('.next_1').removeAttr('disabled','false');
        }
      });

      $(".list-group a").click(function() {
        $('.list-group a').removeClass('selected');
        $(this).addClass('selected');
      });

      window.onscroll = function() {myFunction()};
      var header = document.getElementById("myHeader");
      var sticky = header.offsetTop;
      function myFunction() {
        if (window.pageYOffset > sticky) {
          header.classList.add("sticky");
        } else {
          header.classList.remove("sticky");
        }
      }
      function myFunction_copy() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        document.execCommand("copy");
        alert("Copied the URL: " + copyText.value);
      }
      // Compete Profile js
      jQuery('#browsebtn').on('click', function() {
        jQuery('#user_images').click();
      });
      $(function() {
        // Multiple images preview in browser
        $('.image_alert').hide();
        var imagesPreview = function(input, placeToInsertImagePreview) {
          if (input.files) {
            var filesAmount = input.files.length;
            if(filesAmount <= '15'){
             for (i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
              reader.onload = function(event) {
                $($.parseHTML('<img style="margin:10px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
              }
              reader.readAsDataURL(input.files[i]);
            }
            $('.img_alert').hide();
            $('.next_1').removeAttr('disabled','false');
          }
          else{
            var error_html = '<p align="left" id=""> Sorry !you Cannot Add More Than 15 Images</p>';
            $('.img_alert').html('');
            $('.img_alert').append(error_html);
            $('.img_alert').show();
            $('.next_1').prop('disabled','true');
            return false;
          }
        }
      };
      $('#user_images').on('change', function() {
        jQuery('#nofile').empty();
        jQuery('.demoGallaryPic').hide();
        imagesPreview(this, '#browse_img');
      });
    });
       // user document upload code js
      jQuery('#documentbtn').on('click', function() {
        jQuery('#cpf_documents').click();
      });
      $(function() {
        var filesize = 0;
        // Multiple images preview in browser
        $('.doc_img_alert').hide();
        var imagesPreview = function(input, placeToInsertImagePreview) {
          if (input.files) {
              var docfilename = jQuery('#cpf_documents').val().split(".");
              var filesAmount = input.files.length;
              console.log(docfilename[1]);
              if(docfilename[1] != 'jpg' && docfilename[1] != 'jpeg' && docfilename[1] != 'png'  && docfilename[1] != 'svg' && docfilename[1] != 'ico' && docfilename[1] !='gif' && docfilename[1] != 'pdf' && docfilename[1] != 'doc'  && docfilename[1] != 'docx'){
                  alert("Please Select A valid  File Only  pdf,doc,docx and images type allowed");
                  // jQuery(".doc_img_alert").html("<div class='alert alert-danger' role='alert'><span class='msg-error'>Please Select A valid Image File Only jpeg, jpg and png Images type allowed</span></div>");
                  $('.next_1').prop('disabled','true');
              } else {
                var ff = [];  
                if(filesAmount <= '10'){
                  for (i = 0; i < filesAmount; i++) 
                  {
                    
                    var filesize =  input.files[i].size;
                    console.log('h1'+filesize);
                    if(filesize  > '2097152'){
                       alert("Please upload file less then 2 MB");
                      $('.next_1').prop('disabled','true');
                    } else {
                      var reader = new FileReader();
                      reader.onload = function(event) 
                      {
                        var docfilename = jQuery('#cpf_documents').val().split(".");
                        if(docfilename[1] == 'jpg' || docfilename[1] == 'jpeg' ||docfilename[1] =='png'  || docfilename[1] =='svg' || docfilename[1] =='ico' || docfilename[1] =='gif'){
                          $($.parseHTML('<img style="margin:10px;width:100px;height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        } else{
                           $($.parseHTML('<img style="margin:10px;width:100px;height:100px;">')).attr('src', '/public/images/32329.jpg').appendTo(placeToInsertImagePreview);
                        }
                      }
                       reader.readAsDataURL(input.files[i]);
                        $('.doc_img_alert').hide();
                        //$('.next_1').removeAttr('disabled','false');
                    }
                    var check = ff.push(filesize);
                  }
                  function checkAdult(fsize) {
                   return fsize > 2097152;
                  }
                   check.find(checkAdult);
                  if(check){
                       alert("Please upload file less then 2 MB");
                      $('.next_1').prop('disabled','true');
                  } else{
                    $('.next_1').removeAttr('disabled','false');
                  }

                } else {
                  var error_html = '<p align="left" id=""> Sorry !you Cannot Add More Than 10 Files</p>';
                  $('.doc_img_alert').html('');
                  $('.doc_img_alert').append(error_html);
                  $('.doc_img_alert').show();
                  $('.next_1').prop('disabled','true');
                }
              }
          }
      };
      $('#cpf_documents').on('change', function() {
        jQuery('#nofile').empty();
        jQuery('#document_img').empty();
        imagesPreview(this, '#document_img');
      });
    });
      /* end user doc upload code*/
      var phone = jQuery('#phone').val();
      var zipcode = jQuery('#zipcode').val();
      var address = jQuery('#address').val();
      var state = jQuery('#state').val();
      jQuery('.phone').show();
      jQuery('.zipcode').show();
      jQuery('.address').show();
      jQuery('.state').show();
      if(phone==''){
        jQuery('.phone').hide();
      }
      if(zipcode==''){
        jQuery('.zipcode').hide();
      }
      if(address==''){
        jQuery('.address').hide();
      }
      if(state==''){
        jQuery('.state').hide();
      }
      jQuery("#phone").keypress(function(){
        var phone_val = jQuery('#phone').val();
        if(phone_val==''){
          jQuery('.phone').hide();
        }
        else {
          jQuery('.phone').show();
        }
      });
      jQuery("#zipcode").keypress(function(){
        var zipcode_val = jQuery('#zipcode').val();
        if(zipcode_val==''){
          jQuery('.zipcode').hide();
        }
        else {
          jQuery('.zipcode').show();
        }
      });
      jQuery("#address").keypress(function(){
        var address_val = jQuery('#address').val();
        var docu = jQuery('#cpf_documents').val();
        console.log(docu);
        if(address_val==''){
          jQuery('.address').hide();
        }
        else {
          jQuery('.address').show();
        }
      });
      jQuery("#state").keypress(function(){
        var state_val = jQuery('#state').val();
        if(state_val==''){
          jQuery('.state').hide();
        }
        else {
          jQuery('.state').show();
        }
      });
      //profile submit ajax
      $('body').click(function() {

        var empeml = $('.cpfNumbers').val();
        if(empeml == '') {
        $('#inputId-error').hide();
        $('.cpf_format_error').hide();
         }
         });
      var hostname = " {{ url('/') }}"; 
      jQuery("#completionform").validate({
        rules: {
          phone: {
            required: true,
            minlength: 13,
            maxlength: 14
          },
          zipcode: {
            required: true,
            minlength: 4,
            maxlength: 8
          },
          address: {
            required: true
          },
          cpf_id: {
            required: true,
            maxlength: 14,
            //  remote: {
            //   url: hostname+"/profile/cpfcheck",
            //   type: "post",
            //   data: { cpf_id: function(){ return $('input[name=cpf_id]').val(); },
            //           _token: function(){ return $('input[name=_token]').val(); },
            //           user_id: function(){ return $('input[name=user_id]').val(); }
            //         },
            //   dataFilter: function(data) {
            //     console.log(data);
            //     if(data == 'Exists'){

            //       return false;
            //     }else{
            //       return true;
            //     }
            //   }
            // }
          },
          rg: {
            required: true
          },
          profileimage:{accept: "png|jpe?g|gif", filesize: 1048576  },
          cpf_documents:{ accept: "doc, application/pdf, image/*" ,filesize: 2097152  
          },
        },
        messages: {
          phone: {
            required: "Favor preencher o campo acima.",
            minlength: "Telefone celular com 11 digitos e obrigatorio.",
            maxlength: "Maximum 11 Number allowed."
           
          },
          zipcode: {
            required: "Favor preencher o campo acima.",
            minlength: "Telefone celular com 4 digitos e obrigatorio.",
            maxlength: "Maximum 8 Number allowed."
          },
          address: {
            required: "Favor preencher o campo acima."
          },
          cpf_id: {
            required: "Favor preencher o campo acima.",
            maxlength: "Maximum 11 Number allowed",
            //remote:"This cpf number already exists."
           
          },
          rg: {
            required: "Favor preencher o campo acima."
          },
          profileimage:{
           accept: "Apenas arquivos em JPG, GIF ou PNG e menores que 1MG." ,
          },
          cpf_documents:{
           accept: "Apenas arquivos em JPG, GIF ou PNG e menores que 2MG." ,
          }
        },
        submitHandler: function(form) {
         form.submit(); 
       }
     });
      /* phone number max number validation*/
      $('#phone').on('keydown',function(event){
       var key = event.keyCode || event.charCode;
       if($(this).val().length == 14){
        if(event.keyCode !== 8) {
          event.preventDefault();
        }
      }
    });
    jQuery("#complete_service").validate({
      rules: {
          'cat_name[]': {
            required: true
      }
    },
    messages: {
          'cat_name[]': {
            required: "This field is required"
          },
    },
    submitHandler: function(form) {
          form.submit();
      }
    });
      $(".delete_btn").on('click', function(e){
        if(confirm("Are you sure you want to delete?")){
          var imageid = jQuery(this).attr('id');
          // alert('#prv_'+imageid);
          if(imageid!='') {
            jQuery.ajax({
              type : 'GET',
              url : "{{ url('/accountcomplete') }}/"+imageid,
              data:{ 'imageid': imageid, '_token': "{{ csrf_token() }}"},
              success :  function(resp) {
                if(resp.success="true"){
                 $('#prv_'+imageid).remove();
                 $('#'+imageid).remove();
               }
             }
           });
          }
        } else {
          return false;
        }
      });
      /*delete documents js*/
      $(".document_delete_btn").on('click', function(e){
        if(confirm("Are you sure you want to delete?")){
          var imageid = jQuery(this).attr('data-imgId');
          //alert(imageid);
          if(imageid!='') {
            jQuery.ajax({
               type : 'POST',
              url : "{{ url('/user_document/delete') }}",
              data:{ 'imageid': imageid, '_token': "{{ csrf_token() }}"},
              success :  function(resp) {
                if(resp="success"){
                 $('#prvdoc_'+imageid).remove();
                 $('#doc_'+imageid).remove();
               }
             }
           });
          }
        } else {
          return false;
        }
      });
      /* end delete document js*/
    });
  </script>
  <!-- Compete Profile js */ -->
</body>
</html>
