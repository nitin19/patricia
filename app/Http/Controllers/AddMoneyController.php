<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use Auth;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
class AddMoneyController extends HomeController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->user = new User;
    }
    
    /**
     * Show the application paywith stripe.
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithStripe()
    {
        return view('paywithstripe');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithStripe(Request $request)
    {
        $payment_id = $request->payment_id;
        $error_hide = $request->error_hide;
        $total_amount = $request->total_amount;
        \Session::put('error_hide', $error_hide);
        \Session::put('booking_id', $payment_id);
        \Session::put('total_amounts', $total_amount);
        $validator = Validator::make($request->all(), [
            'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            'amount' => 'required',
        ]);
        
        $input = $request->all();  
        $paymentid_stripe = $request->get('paymentid_stripe');       
            $input = array_except($input,array('_token'));            
            $stripe = Stripe::make('sk_test_AvC4K4j7Ti4iNK2SSlQGz36K');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year'  => $request->get('ccExpiryYear'),
                        'cvc'       => $request->get('cvvNumber'),
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error','The Stripe Token was not generated correctly');
                    return view('paywithstripe');
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount'   => $request->get('amount'),
                    'description' => 'Add in wallet',
                ]);

                if($charge['status'] == 'succeeded') {
                    /**
                    * Write Here Your Database insert logic.
                    */
                     $paymentid_stripe;

                 $transaction_id = $charge['id'];
                 
                  \Session::put('success','Thank you! Your payment was processed successfully.');
                    return view('paywithstripe', compact('transaction_id', 'paymentid_stripe'));
                } else {
                    \Session::put('error','Sorry! Your payment was processed failure. Please try again!');
                    return view('paywithstripe');
                }
            } catch (Exception $e) {
                \Session::put('error',$e->getMessage());
                return view('paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error',$e->getMessage());
                return view('paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error',$e->getMessage());
                return view('paywithstripe');
            }
        
    }    
}
?>