<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\ErrorLogs;
use App\Classes\ErrorsClass;

class AdminblackusersController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request){
    try{
      $blackuserlist = DB::table('black_users')
                       ->where('is_deleted','0')
            
            ->get()->toarray();
         
      // $blackuserlist = DB::table('black_users')
      //       ->join('users', 'users.id', '=', 'black_users.from_id')
      //       ->select('users.*', 'users.id as from_id', 'black_users.*')
      //       ->get()->toarray();
          /* Listing */
        return view('admin.blackuserlist', compact('blackuserlist'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System /home/pankaj/patricia/apr17/controller/work4/: " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
      }
       public function requestaction(Request $request){
        $action = $request->action;
        $userid = $request->userid;
        $request_id = $request->request_id;
        if($action=='1'){
        DB::table('users')
            ->where('id', $userid)
            ->update(['is_deleted' => '1', 'is_active' => '0']);

        $update_user = DB::table('black_users')
            ->where('id', $request_id)
            ->update(['black_status' => '1']);

        } else {
        DB::table('users')
            ->where('id', $userid)
            ->update(['is_deleted' => '0', 'is_active' => '1']);

        $update_user = DB::table('black_users')
            ->where('id', $request_id)
            ->update(['black_status' => '2']);
        }
        if($update_user!=''){
                echo 'true';
        }
    }
    public function excel_export(Request $request)
    {

       $blackuserlist = DB::table('black_users')
                       ->where('is_deleted','0')
                        ->select('from_name as Black_By','to_name as Black_To','black_reason as Reason','black_status as Black_status','user_type as Request_by')
                       ->get();
       $exceldata = array();
        if(count($blackuserlist) > 0){
            $blackuserlist = json_decode(json_encode($blackuserlist), true );
            foreach($blackuserlist as $users){
                
                if($users['Black_status']=='1'){
                    $status = 'Accepted';
                } elseif($users['Black_status']=='2') {
                    $status = 'Rejected';
                } else {
                    $status = 'Pending';
                }
               $users['Black_status'] = $status;
               //$users['Address'] = str_replace(',', '', $users['Address']);
               array_push( $exceldata,  $users);
            }
        }
    
        //echo "<pre>"; print_r($provider_users);die;
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
             
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                     //print_r($row);die;
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
}
