<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\ErrorLogs;
use App\Classes\ErrorsClass;

class AdmincloserequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
      try{
          /* Listing */
       /*$close_account_request = DB::table('close_account_request')
                                ->orderBy('id', 'desc')
                                ->get();*/

        $close_account_request = DB::table('close_account_request')
            ->join('users', 'users.id', '=', 'close_account_request.user_id')
            ->select('users.*', 'users.id as userid', 'close_account_request.close_reason', 'close_account_request.admin_activity', 'close_account_request.id as requestid')
            ->get()->toarray();
          /* Listing */
        return view('admin.closerequest', compact('close_account_request'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System /home/pankaj/patricia/apr17/controller/work4/: " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function requestaction(Request $request){
        $action = $request->action;
        $userid = $request->userid;
        $request_id = $request->request_id;
        if($action=='1'){
        DB::table('users')
            ->where('id', $userid)
            ->update(['approved_by_admin' => '0', 'is_active' => '0', 'close_request_action' => '1']);

        $update_user = DB::table('close_account_request')
            ->where('id', $request_id)
            ->update(['admin_activity' => '1', 'updated_at' => date('Y-m-d H:i:s')]);

        } else {
        DB::table('users')
            ->where('id', $userid)
            ->update(['approved_by_admin' => '1', 'is_active' => '1', 'close_request_action' => '2']);

        $update_user = DB::table('close_account_request')
            ->where('id', $request_id)
            ->update(['admin_activity' => '2', 'updated_at' => date('Y-m-d H:i:s')]);
        }
        if($update_user!=''){
                echo 'true';
        }
    }
}