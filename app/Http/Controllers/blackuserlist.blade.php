@extends('layouts.default-header-admin')

@section('title', 'Blackuserlist')

@section('content')

<div class="lock_box">
	<div class="yelloback">
		<div class="col-sm-12">
			<div class="service_box tablediv tablesection">
			<!-- <div class="alert alert-success alert-block success_show">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>Request submitted successfully!</strong>
			</div> -->
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-sm-12">
					<div class="media payment_box">
						<!-- <div class="col-sm-10"> -->
							<div class="media-body button_side">
								<h3>Black User List<span class="list_clr"> ({{ count($blackuserlist) }})</span>
								 <a href="{{ url('/admin/blacklist-export') }}" class="btn btn-primary excl_export">Excel</a></h3>
							</div>
						<!-- </div> -->
					</div>
				</div>
			</div>
			<div class="row"> 
			<div class="col-sm-12">
			<table id="blackusers_list" class="table table-borderless heading-name">
			    <thead>
			      <tr>
			        <th class="">Sr No.</th>
			        <th class="">Report By</th>
			        <th class="">Report To</th>
			        <th class="">Reason</th>
			        <th class="">Status</th>
			      </tr>
			    </thead> 
			    <tbody>
				    @if(count($blackuserlist)> 0)
				       	<?php $count = 0; ?>
					    @foreach($blackuserlist as $black_user_list_info)
					   
				    <tr class="closereq_table tabledata">
				        <td class="">{{ ++$count }}</td>
				        <td class="">{{ ucfirst($black_user_list_info->from_name) }}({{$black_user_list_info->user_type}})</td>
				        <td class="">{{ ucfirst($black_user_list_info->to_name) }}</td>
				        <td class="">{{ $black_user_list_info->black_reason }}</td>
				        <td  class="" id="{{$black_user_list_info->to_id}}" data-value="{{$black_user_list_info->id}}">
				        	@if($black_user_list_info->black_status=='0')
				          <select name="action_request" id="action_request_<?php echo $black_user_list_info->id; ?>" class="select">
				            <option value="0">Pending</option>
				            <option value="1">Accepted</option>
				            <option value="2">Rejected</option>
				            </select>
				            @else
				            <input name="action_request" type="text" readonly="" disabled="true" value="@if($black_user_list_info->black_status=='2'){{ 'Rejected'}} @else {{'Accepted'}} @endif" style="width: 80px;text-align: center;">
				            @endif
				        </td>
				    </tr>
			      	@endforeach
			      	@else
			      	<tr><td align="center" colspan="7">No Record Found</td></tr>
			      	@endif
			    </tbody> 
			</table>
			</div> 
			</div> 
				<script type="text/javascript">
				    // jQuery('.success_show').hide();
				    jQuery('select').on('change', function() {
				    var action = jQuery(this).val();
				    var parent_id = $(this).closest('td').attr('id');
				    var request_id = $(this).closest('td').attr('data-value');
				    jQuery.ajax({
				          url: 'admin-blackusers/status',
				          type: 'POST',
				          data: {'action': action,'userid': parent_id, "request_id":request_id, "_token": "{{ csrf_token() }}" },
				          success: function(response) {
				          	$.notify({
						      message: 'Request submitted successfully!',
						      },{
						      type: 'success',
						      offset: 
						      {
						        x: 10,
						        y: 130
						      },
						      animate: {
						        enter: 'animated fadeInRight',
						        exit: 'animated fadeOutRight'
						      },
						    });

				          }            
				          });
				    });
				</script>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#blackusers_list').DataTable();
} );
</script>
@stop
