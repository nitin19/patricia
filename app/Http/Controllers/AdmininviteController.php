<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdmininviteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        try{
            /* COunt */
           $invite_friend_count = DB::table('invite_friend')
                                 ->where('is_active', '1')
                                 ->where('is_deleted', '0')
                                 ->count();

            $invitecount = DB::table('users')
                    ->join('invite_friend', 'users.id', '=', 'invite_friend.invite_by_id')
                     ->where('users.is_active', '1')
                     ->where('users.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('user_role', '!=', 'admin')
                     ->count();
            /* Count */
            /* Listing */ 
          $users = DB::table('users')
                    ->join('invite_friend', 'users.id', '=', 'invite_friend.invite_by_id')
                     ->where('users.is_active', '1')
                     ->where('users.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('user_role', '!=', 'admin')
                     ->select('users.*', DB::raw("count(invite_friend.invite_by_id) as countinvite"))
                     ->groupBy('invite_friend.invite_by_id')
                     ->get();                    
            return view('admin.invitelist', compact('invite_friend_count', 'users', 'invitecount'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function export_excel(Request $request)
    {

       $inviteusers = DB::table('users')
                    ->join('invite_friend', 'users.id', '=', 'invite_friend.invite_by_id')
                     ->where('users.is_active', '1')
                     ->where('users.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('invite_friend.is_deleted', '0')
                     ->where('user_role', '!=', 'admin')
                     ->select('users.name as Name','users.email as Email', DB::raw("count(invite_friend.invite_by_id) as Total_invitation_send"),'users.phone as Phone','users.address as Location','users.is_active as Status')
                     ->groupBy('invite_friend.invite_by_id')
                     ->get();
        $exceldata = array();
        if(count($inviteusers) > 0){
            $inviteusers = json_decode(json_encode($inviteusers), true );
            foreach($inviteusers as $users){
                
                if($users['Status']=='1'){
                    $status = 'Active';
                }else {
                    $status = 'Inactive';
                }
               

               
               $users['Status'] = $status;
               $users['Location'] = str_replace(',', '.', $users['Location']);
               array_push( $exceldata,  $users);
            }
        }
        
       
        $filename = "Export_excel.xls";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $isPrintHeader = false;
            if (!empty($exceldata)) {
                foreach ($exceldata as $row) {
                    if (! $isPrintHeader) {
                        echo implode("\t", array_keys($row)) . "\n";
                        $isPrintHeader = true;
                     }
                  
                    echo implode("\t", array_values($row)) . "\n";
                }
            }
        exit();
      
    }
}
?>