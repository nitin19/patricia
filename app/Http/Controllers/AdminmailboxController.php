<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminmailboxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        try{
            /* Count */
           $conatct_admin_count = DB::table('conatct_admin')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->count();
              /* Count */
              /* Listing */ 
            $conatct_admin = DB::table('conatct_admin')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->orderBy('conatct_id', 'DESC')
                     ->paginate('10');
              /* Listing */
            return view('admin.mails', compact('conatct_admin_count', 'conatct_admin'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function destroy(Request $request){
        $contact_id = $request->contact_id;
        $conatct_admin = DB::table('conatct_admin')->where('conatct_id', $contact_id)->delete();
        if($conatct_admin!=''){
            echo 'true';
        }
        else{
            echo 'false';
        }
        
    }

    public function sendadminmsg(Request $request){
      $recipients = $request->recipients;
      $subject = $request->subject;
      $message = $request->message;

      $admindetail = DB::table('business_settings')
                     ->where('is_active', '1')
                     ->where('is_deleted', '0')
                     ->first();

      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= 'From: '.$admindetail->name.'<'.$admindetail->email.'>' . "\r\n";
      if($recipients!=''){
        $reciepientarr = explode(',', $recipients);
        foreach($reciepientarr as $recipient){
            mail($recipient,$subject,$message,$headers);
        }
          return redirect('/admin-mails')
                 ->with('success','Message send successfully'); 
      } else {
          return redirect('/admin-mails')
                ->with('error','Message not send'); 
      }

    }

}