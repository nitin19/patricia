<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Errorlogs;
use App\Models\MainCategory;
use App\Models\Spavailability;
use Redirect;
class ServiceController extends Controller
{
    //
  public function incomplete_service(Request $request){
    try{
      $main_category = MainCategory::join('category','main_category.id','=','category.main_cat_id')
      ->where('main_category.is_deleted', '0')->pluck('name','id');
      $id = Auth::id();
      $user_info = User::select('users.*','category.cat_id as sub_cat_id','category.cat_image','category.cat_name','category.main_cat_id')
      ->join('category','category.cat_id','=','users.sub_id')
      ->where('users.id', $id)
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      ->where('users.approved_by_admin', '1')
      ->first();
                // echo "<pre>";
                // print_r($user_info);die;
      $current_ip = $request->ip();
      $currentuser_ip = DB::table('wish_list_ip')
      ->where('ip_address', $current_ip)
      ->get();
      $currentuser_ip_count = DB::table('wish_list_ip')
      ->where('ip_address', $current_ip)
      ->count();
      $sub_category = DB::table('category')->pluck('cat_name','cat_id');

      $user_image = DB::table('user_gallery')
      ->where('user_id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->get();

      $spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first(); 
      $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/completeservice')->where('is_active', '1')->where('is_deleted', '0')->first();
            
      return view('serviceprovider.completeservices',compact('id','main_category','user_info','currentuser_ip','current_ip','currentuser_ip_count','sub_category','spavailabilityInfo','menu_meta_details'));
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }catch(\Exception $e){ 
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
     if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }
  }

  public function store(Request $request){
    try{
      $id = Auth::id();
      $update_service = DB::table('users')
      ->where('id', $id)
      ->update(['cat_id' => $request->main_categroy_id,'sub_id'=>$request->sub_categroy_id]);
      if($update_service!=''){
        return redirect('/serviceprovider/dashboard')->with('message', 'Dados cadastrados com sucesso.');
      }
      else {
        return redirect('/serviceprovider/dashboard')->with('message', 'Services Not updated!');
      }
      echo json_encode($response);
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
     if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }catch(\Exception $e){ 
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
     if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror')); 
    }
  }

  public function check_subcategory(Request $request){
    $main_category = DB::table('category')->where('main_cat_id',$request->main_categroy_id)->pluck('cat_name','cat_id');
        // // echo "<pre>";
        // print_r($main_category);die;
    if(!empty($main_category)){
      $response['success'] = "true";
      $response['message'] = "success";
      $response= $main_category;
    }
    else{
      $response['success'] = "false";
      $response['message'] = "failure";

    }
        // $response['success'] = "true";
        // $response['message'] = "success";
    echo json_encode($response);
  }

  public function subcategory_queries(Request $request){
    $price_queries = DB::table('price_queries')->where('cat_id', $request->main_category)->where('sub_id', $request->sub_category)->get()->toArray();
    if(!empty(Auth::user()->address)&&!empty(Auth::user()->cat_id)){ $profile_url =  url('/serviceprovider/profile'); } else{ $profile_url=url('/serviceprovider/completeprofile'); }
    $Html = '';
    if(count($price_queries) > 0){
      $queryids = array();
      $Html .= '<div class="form-group col-md-12"><h3>Preços</h3><hr></div>';
      foreach($price_queries as $price_query){
        $Html .= '<table class="table pricing_table edit_price">
                <tbody><tr><th><h4>'.$price_query->queries.'</h4></th><th>&#82;&#36;
        <input data-type="currency" type="text" value="0" name="pricequery_'.$price_query->id.'"'; 
        /*if($price_query->priority==1){*/
         $Html .= 'required'; 
       /*}*/
       //$Html .= ' id="query_amount" >
       $Html .= ' id="query_amount_'.$price_query->id.'">
       </th>
       </tr></tbody></table>';
       array_push($queryids, $price_query->id);
     }
     $Html .= '<input type="hidden" name="queryids" value="'.implode(',', $queryids).'">';
     $Html .= '<div class="col-sm-12 nextbtn">
     <input class="form-control save " value="Salvar" type="submit">&nbsp;
     <a href="'.$profile_url.'" class="btn btn-default pull-right cancelcancel">Cancelar</a>&nbsp;
     </div>';
   } 
   else{
   	 $Html = '<div class="col-sm-12 nextbtn">
     <input type="hidden" name="queryids" value="">
     <input class="form-control save " value="Salvar" type="submit">
     &nbsp;<a href="'.$profile_url.'" class="btn btn-default pull-right cancelcancel">Cancelar</a>&nbsp;
     </div>';
   }
   echo $Html;
 }

 public function savequeryprice(Request $request){
  $queryids = trim($request->queryids);
  $sub_categroy_id = $request->sub_categroy_id;
  $main_categroy_id = $request->main_categroy_id;
  $userid = Auth::id();
  $user = User::where('id', $userid)->first();

  DB::table('provider_price_list')->where('user_id', $userid)->delete();
  if($queryids!=''){
      $queryidsArr = explode(',', $queryids);
  } else {
    $queryidsArr = array();
  }
  
  if(count($queryidsArr) > 0){
    $insertrow = 0;
    foreach($queryidsArr as $queryprice){
      $fieldname = 'pricequery_'.$queryprice;
      if($request->$fieldname!=''){
      $ser_price =  $request->$fieldname;
      $price_dec = explode(',',$ser_price);
      $finalSrcPrc = preg_replace('/[^A-Za-z0-9\-]/', '', $price_dec[0]);
      $newprice_queries = DB::table('provider_price_list')->insertGetId(['user_id' => $userid, 'query_id' => $queryprice, 'amount' => $finalSrcPrc]);
        if($newprice_queries){
          $insertrow++;
          $user_updated = DB::table('users')
          ->where('id', $userid)
          ->update(['cat_id' => $main_categroy_id, 'sub_id' => $sub_categroy_id]);
        }
      } else{
      	 $user_updated = DB::table('users')
          ->where('id', $userid)
          ->update(['cat_id' => $main_categroy_id, 'sub_id' => $sub_categroy_id]);
      }
    }

    return  redirect('/serviceprovider/dashboard')->with('success', 'Dados cadastrados com sucesso.');
  } else {
    $newprice_queries = DB::table('provider_price_list')->insertGetId(['user_id' => $userid, 'query_id' => '0', 'amount' => '0.00']);
    $user_updated = DB::table('users')
          ->where('id', $userid)
          ->update(['cat_id' => $main_categroy_id, 'sub_id' => $sub_categroy_id]);

    if($newprice_queries){
        return  redirect('/serviceprovider/dashboard')->with('success', 'Dados alterados com sucesso.');
    } else {
      return Redirect::back()->with('error','Some problem occured. Please try again.');
    }
  }
}

public function updatequeryprice(Request $request){
  
  $userid = Auth::id();
  $user = User::where('id', $userid)->first();

  $queryids = $request->queryids;
  $queryidsArr = explode(',', $queryids);
  if(count($queryidsArr) > 0){
    $updaterow = 0;
    foreach($queryidsArr as $queryprice){
      $fieldname = 'pricequery_'.$queryprice;
      if($request->$fieldname!=''){
        $ser_price =  $request->$fieldname;
        $price_dec = explode(',',$ser_price);
        $finalSrcPrc = preg_replace('/[^A-Za-z0-9\-]/', '', $price_dec[0]); 

        $provide_query_price = DB::table('provider_price_list')->where('query_id', $queryprice)->where('user_id', Auth::user()->id)->first();
        if($provide_query_price){
          
          $newprice_queries = DB::table('provider_price_list')
          ->where('user_id', Auth::user()->id)->where('query_id', $queryprice)
          ->update(['amount' => $finalSrcPrc]);
        } else {
          $newprice_queries = DB::table('provider_price_list')->insertGetId(['user_id' => Auth::user()->id, 'query_id' => $queryprice, 'amount' => $finalSrcPrc]);
        }
        if($newprice_queries){
          $updaterow++;
        }
      }
    }

    return redirect('/serviceprovider/dashboard')->with('success', 'Dados cadastrados com sucesso.');
  } else {
    return Redirect::back()->with('message','Some problem occured. Please try again.');
  }


}

public function user_services(Request $request){
  try{
    $main_category = MainCategory::join('category','main_category.id','=','category.main_cat_id')
    ->where('main_category.is_deleted', '0')->pluck('name','id');
    $id = Auth::id();
    $user_info = User::select('users.*','category.cat_id as sub_cat_id','category.cat_image','category.cat_name','category.main_cat_id')
    ->join('category','category.cat_id','=','users.sub_id')
    ->where('users.id', $id)
    ->where('users.is_active', '1')
    ->where('users.is_deleted', '0')
    ->where('users.approved_by_admin', '1')
    ->first();

    $current_ip = $request->ip();
    $currentuser_ip = DB::table('wish_list_ip')
    ->where('ip_address', $current_ip)
    ->get();

    $currentuser_ip_count = DB::table('wish_list_ip')
    ->where('ip_address', $current_ip)
    ->count();
    $sub_category = DB::table('category')->pluck('cat_name','cat_id');

    $user_image = DB::table('user_gallery')
    ->where('user_id', $id)
    ->where('is_active', '1')
    ->where('is_deleted', '0')
    ->get();

    $provider_price_lists = DB::table('provider_price_list')
    ->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
    ->where('provider_price_list.user_id', $id)
    ->select('price_queries.*', 'provider_price_list.id', 'provider_price_list.amount', 'provider_price_list.query_id')
    ->orderBy('provider_price_list.id', 'asc')
    ->get()
    ->toArray();
    
    $spavailabilityInfo = Spavailability::where('user_id', $id)->where('status', 1)->first();     
    $sub_cat_info = DB::table('category')->where('cat_id',Auth::user()->sub_id)->first();   
    $querieslists = DB::table('price_queries')->where('cat_id', Auth::user()->cat_id)->where('sub_id', Auth::user()->sub_id)->get()->toArray(); 
    $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','serviceprovider/services')->where('is_active', '1')->where('is_deleted', '0')->first();
    
    return view('serviceprovider.user_services',compact('id','main_category','user_info','currentuser_ip','current_ip','currentuser_ip_count','sub_category','spavailabilityInfo','provider_price_lists','sub_cat_info','querieslists','menu_meta_details'));
  }
  catch(\Illuminate\Database\QueryException $e){
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
    if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror')); 
  }catch(\Exception $e){ 
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
   if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror')); 
  }
}


}

