<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;

class LockScreenController extends Controller
{
    public function __construct()
    {
      
    }
    public function get(){
            
    // only if user is logged in
        if(Auth::check()){
            Session::put('locked', true );
           // echo Session::get('locked');
            return view('lockscreen');
        
        }
        else
        {
            return redirect('/login');
        }

        
    }
    public function unlock(Request $request)
    {
        
            if(!Auth::check())
            {
            return redirect('/login');
            }
          $id=Auth::user()->id;
          $name=Auth::user()->name;
          $e_password=$request->e_password;
          $e_user=$request->e_user;
          $hd_password = DB::table('users')
                        ->where('id', '=', $id)
                        ->value('password'); 
          $check=Hash::check($e_password, $hd_password);
          // echo $check;die;
          if(!empty($e_password))
          {
                         if($e_user == $name && $check) 
                          {
                                // return redirect('/servicetaker/profile');
                            if(Auth::user()->user_role =='admin')
                                {
                                    Session::put('locked', false );
                                    return redirect('/admin-dashboard');
                                }
                                else if(Auth::user()->user_role == 'Taker')
                                {
                                    Session::put('locked', false );
                                    return redirect('/servicetaker/dashboard');
                                }
                                else
                                {
                                    Session::put('locked', false );
                                    return redirect('/serviceprovider/dashboard');
                                }
                           }
                            else
                                {
                                    return redirect()->back()->withErrors(['Please fill the correct password.']);
                            //return redirect()->action('admin.lockscreen')->with('error', 'Password not matched');
                                } 
              }
              else
              {
                return redirect()->back()->withErrors(['Please fill the password.']);
              }
        
                       
    }

    
}