<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use User;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
class FrontserviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('auth');
    }

     public function showallcategories(Request $request)
    {
      try{
          /* Count */
         $category_count = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   ->count();
            /* Count */
            /* Listing */ 

          $category = DB::table('category')
                   ->join('main_category', 'main_category.id', '=', 'category.main_cat_id')
                   ->where('category.is_active', '1')
                   ->where('category.is_deleted', '0')
                   ->select('category.*', 'main_category.name')
                   //->orderBy('category.cat_id','desc')
                   ->orderBy('main_category.name','ASC')
                   ->get();  
                  
          return view('showcategories',compact('category_count', 'category'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
}