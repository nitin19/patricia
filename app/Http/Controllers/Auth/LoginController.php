<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Session;
class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
  * Where to redirect users after login.
  *
  * @var string
  */
  protected $redirectTo = '/';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  protected function authenticated(Request $request, $user) 
  {
   $remember_me = $request->has('remember_me') ? true : false;
    if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $remember_me))
    {     
            $ua=$this->getBrowser();
            $u_device = '';
            if($this->isMobile()){
               $u_device = 'mobile';
            }
            else {
                $u_device = 'desktop';
            }
            Auth::user()->last_login = Carbon::now()->toDateTimeString();
            Auth::user()->last_login_ip = $this->get_client_ip();
            Auth::user()->login_count = $user->login_count+1;
            Auth::user()->login_browser = "browser: " . $ua['name'] . " version " . $ua['version'];
            Auth::user()->login_device = $u_device;
            Auth::user()->save();
            $rem_email = "rem_email";
            $rem_email_val = $request->email;
            $rem_pwd = "rem_pwd";
            $rem_pwd_val = $request->password;
            $rem_chk = "rem_chk";
            $rem_chk_val = "1";
        if($request->remeber_me==1) {
            setcookie($rem_email, $rem_email_val, time() + (86400 * 90), "/"); 
            setcookie($rem_pwd, $rem_pwd_val, time() + (86400 * 90), "/"); 
            setcookie($rem_chk, $rem_chk_val, time() + (86400 * 90), "/");
        } else {
          setcookie($rem_email, "", time() - 3600);
          setcookie($rem_pwd, "", time() - 3600);
          setcookie($rem_chk, "", time() - 3600);
        }
        $user = auth()->user();
        if($user->user_role=='admin'){
      return redirect()->intended('admin-dashboard');
    }
     else {
       if($user->is_active=='0' && $user->close_request_action!='0' && $user->approved_by_admin=='0'){
           Auth::logout();
           $request->session()->put('error', 'Entre em contato com o administrador para ativar sua conta!');
          // $request->session()->flash('error', 'Please contact to admin for your account activation!');
              return redirect()->intended('/login');
      }
     else if($user->is_active=='0' && $user->close_request_action!='0' && $user->approved_by_admin=='1'){
           Auth::logout();
           $request->session()->put('error', 'Você já enviou uma solicitação de desativação de conta!');
          // $request->session()->flash('error', 'You have already submitted request for account deactivation!');
              return redirect()->intended('/login');
      }
      else if($user->is_deleted  == '1'){
          Auth::logout();
          $request->session()->put('error', 'Usuário não encontrado. Tente novamente ou verifique o e-mail digitado.');
         // $request->session()->flash('error', 'Sorry! User does not exist now ');
              return redirect()->intended('/login');
     }
    else if($user->is_active==1 && $user->approved_by_admin=='1'){
      if($user->user_role == 'Provider'){

        $provider_price_list_count = DB::table('provider_price_list')->where('user_id', '=', $user->id)->count();
        
        if( $user->cpf_id !='' && $user->phone !='' && $user->cat_id !='' && $user->sub_id !='' && $user->zipcode !='' && $user->address !='' && $user->city !='' && $user->neighborhood !='' && $user->state !='' && $user->country !='' && $provider_price_list_count != 0 )
        {
          return redirect(Session::get('backUrl') ? Session::get('backUrl') :   '/home');
            //return redirect()->intended('/home');
        }
        else{

          if($user->cpf_id !='' && $user->phone !='' && $user->cat_id !='' && $user->sub_id !='' && $user->zipcode !='' && $user->address !='' && $user->city !='' && $user->neighborhood !='' && $user->state !='' && $user->country !='') {
            return redirect()->intended('/serviceprovider/completeservice');
          } else {
            return redirect()->intended('/serviceprovider/completeprofile');
          }
        }
      }
       else if($user->user_role == 'Taker'){
        if(!empty($user->phone) && !empty($user->address)){
          $wish_provider_id = Session::get('addwish');
          if($wish_provider_id){
             $check_exist = DB::table('wish_list')->where('wish_profile_id',$wish_provider_id)->where('user_id',Auth::user()->id)->count();
             if($check_exist == '0'){
              $insert_whish = DB::table('wish_list')->insertGetId(
                       ['user_id' => Auth::user()->id, 'wish_profile_id' => $wish_provider_id,'status'=> '1']
                       );
             }
          }
          return redirect(Session::get('backUrl') ? Session::get('backUrl') :   '/home');
          //return redirect()->intended('/home');
        }
        else{
          return redirect()->intended('/servicetaker/completeprofile');
        }
      }
      // else{
      //     return redirect('dashboard');
      // }
     }

     else {
      Auth::logout();
          $request->session()->put('error', 'Por favor verifique a sua caixa de email e clique no link recebido para ativar sua conta!');
          // $request->session()->flash('error', 'Please activate your account!');
              return redirect()->intended('/login');
          }
     }
       
    }else{
        return back()->with('error','your username and password are wrong.');
    }


    
    }
    public function logout(Request $request)
    {
      Auth::logout();
      $request->session()->flush();
      return redirect()->intended('/login'); 
    }
    public function getBrowser(){
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
   
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
   
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
   
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }
   
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
       
        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }
    public function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
  }
