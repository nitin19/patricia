<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use DB;
use App\Models\Spavailability;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
  * Where to redirect users after registration.
  *
  * @var string
  */
  protected $redirectTo = '/thankumsg';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
  * Get a validator for an incoming registration request.
  *
  * @param  array  $data
  * @return \Illuminate\Contracts\Validation\Validator
  */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'name' => 'required|string|max:255',
     // 'email' => 'required|string|email|max:255|unique:users',
      'email' => 'required|string|email|max:255',
      'password' => 'required|string|min:6|confirmed',
      'password_confirmation' => 'required|min:6',
    ]);
  }

  /**
  * Create a new user instance after a valid registration.
  *
  * @param  array  $data
  * @return \App\User
  */
  protected function create(array $data)
  {
    $from_email = "NaVizinhança  <contato@navizinhanca.com>";
    // $from_email="contato@navizinhanca.com";
    $name = $data['name'];
    $email = $data['email'];
    $password = $data['password'];
    $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $referal = "navizinhanca";
    for ($i = 0; $i < 5; $i++) {
      $referal .= $chars[mt_rand(0, strlen($chars)-1)];
    }
    $user_create = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'hd_password' => $data['password'],
          'user_role' => $data['user_role'],
          'referal_code' => $referal,
        ]);
        $insertedId = $user_create->id;
        if( $insertedId > 0 ) {
          $user = User::where('id',$user_create->id)->first();
          if($user->user_role == 'Provider') {
            $data = new Spavailability;
            $data->user_id = $user_create->id;
            $data->openhrs = '0';
            $data->isCompleted = '1';  
            $data->save();
          }
          $to = $email;
          $message    = '';
          $logo_url = url('/public').'/images/logo.png';
          $mail_bg_img = url('/public').'/images/bg.jpg';
          $email_icon = url('/public').'/images/email_icon_mail.png';
          $LI_png = url('/public').'/images/LI.png';
          $fb_png = url('/public').'/images/fb.png';
          $insta_png = url('/public').'/images/insta.png';
          
          //https://www.navizinhanca.com/user/'.$insertedId.';
          $messageBody = '';
          $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns:v="urn:schemas-microsoft-com:vml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                <!--[if !mso]><!-- -->
                <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                <!-- <![endif]-->
                <title>Email Template</title>
                <style type="text/css">
                * {font-family: "Lato", sans-serif;}
                </style>
            </head>
          <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
          <table style="background-image:url('.$mail_bg_img.');" style="width:600px;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" >
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                             
                              <tr>
                                  <td align="center">
                                      <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                                          <tr>
                                               <td align="center" height="70" style="height:70px;">
                                                  <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="center">
                                                  <hr>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                             
                          </table>
                      </td>
                  </tr>
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
                  <tr>
                      <td align="center">
                          <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                  <td align="center" style="color: #1497d5; font-size: 35px; font-weight:700; line-height: 35px;" class="main-header">
                                    <b style="color: #1497d5;
                                                      padding-right: 5px;">Olá </b>' . $name .',
                                      <div style="line-height: 35px">
                                          <span style="color: #1497d5;margin:10px auto;line-height:normal;">seja bem vindo(a) ao NaVizinhança.</span>
                                      </div>
                                      <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 45px;margin-top:5px;">Clique no link abaixo para validar seu cadastro junto ao NaVizinhança.</p>
                                   
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr> 
              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                 
                  <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" > 
                      <tr>
                          <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                 
                                  <tr>
                                      <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                          <div style="line-height: 26px;">
                                              <a href="https://www.navizinhanca.com/user/'.$insertedId.'" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Comece sua jornada</a>
                                          </div>
                                      </td>
                                  </tr>
                                   <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
                                 
                                      </table>
                                  </td>
                              </tr>
                  </table>

              </table>
              <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">
                   <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
                  <tr>
                      <td align="center">
                          <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr> 
                                  <td>
                                      <div class="social-icons" style="text-align: center;">
                                          <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$insta_png.'">
                                          </a>
                                          <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                              <img src="'.$LI_png.'">
                                          </a>
                                          <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                              <img src="'.$fb_png.'">
                                          </a>
                                      </div>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                  </tr>
              </table>
              </table>
            </body>
          </html>';
          $subject ="NaVizinhança: Ative sua conta";
          $headers = "From: " .$from_email  . "\r\n".'X-Mailer: PHP/' . phpversion();
          // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          //$headers .= "CC: dev2.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $messageBody, $headers,'-faaa@gmail.com');
          return redirect('/thankumsg');
       // } }
      } else {
          if($data['ispvrgfrm']=='yes') {
            return redirect('/patriciaservice/register')->with('error', 'Sorry fail to create account. Please try again');
          } else {
            return redirect('/register')->with('error', 'Sorry fail to create account. Please try again');
          }
      }
   

    // Validate reCAPTCHA box 
    // if(($_REQUEST['g-recaptcha-response']) && !empty($_REQUEST['g-recaptcha-response'])){ 
    //   // Google reCAPTCHA API secret key 
    //   $secretKey = '6LdLBKgUAAAAAFlezdBI4lPuV4MuJx5YyIxdljD8'; 
               
    //     // Verify the reCAPTCHA response 
    //   $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_REQUEST['g-recaptcha-response']); 
         
    //   // Decode json data 
    //   $responseData = json_decode($verifyResponse); 
       
    //   // If reCAPTCHA response is valid 
    //   if($responseData->success){ 
    //     echo "validate";
    //   }else{
    //     echo "no validate";
    //   }
    // }
        
  }
  public function checkEmailEx(Request $request) {
      // print_R($request->All());die;
      $avail =  DB::table('users')->where('email', $request->email )->where('is_deleted', '0')->first();
      
      if($avail){
        echo 'Exists';
      }
      else{
           echo '0';
      } 
    }
}

