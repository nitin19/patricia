<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;

use Session;
class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
  * Where to redirect users after login.
  *
  * @var string
  */
  protected $redirectTo = '/';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  protected function authenticated(Request $request, $user) 
  {
    if($user->user_role=='admin'){
      return redirect()->intended('admin-dashboard');
    }
     else {
       if($user->is_active=='0' && $user->close_request_action!='0' && $user->approved_by_admin=='0'){
           Auth::logout();
           $request->session()->put('error', 'Entre em contato com o administrador para ativar sua conta!');
          // $request->session()->flash('error', 'Please contact to admin for your account activation!');
              return redirect()->intended('/login');
      }
     else if($user->is_active=='0' && $user->close_request_action!='0' && $user->approved_by_admin=='1'){
           Auth::logout();
           $request->session()->put('error', 'Você já enviou uma solicitação de desativação de conta!');
          // $request->session()->flash('error', 'You have already submitted request for account deactivation!');
              return redirect()->intended('/login');
      }
      else if($user->is_deleted  == '1'){
          Auth::logout();
          $request->session()->put('error', 'Usuário não encontrado. Tente novamente ou verifique o e-mail digitado.');
         // $request->session()->flash('error', 'Sorry! User does not exist now ');
              return redirect()->intended('/login');
     }
    else if($user->is_active==1 && $user->approved_by_admin=='1'){
      if($user->user_role == 'Provider'){
        if(!empty($user->phone) && !empty($user->address)&&!empty($user->cat_id))
        {
          return redirect(Session::get('backUrl') ? Session::get('backUrl') :   '/home');
            //return redirect()->intended('/home');
        }
        else{
          return redirect()->intended('/serviceprovider/completeprofile');
        }
      }
       else if($user->user_role == 'Taker'){
        if(!empty($user->phone) && !empty($user->address)){
          $wish_provider_id = Session::get('addwish');
          if($wish_provider_id){
             $check_exist = DB::table('wish_list')->where('wish_profile_id',$wish_provider_id)->where('user_id',Auth::user()->id)->count();
             if($check_exist == '0'){
              $insert_whish = DB::table('wish_list')->insertGetId(
                       ['user_id' => Auth::user()->id, 'wish_profile_id' => $wish_provider_id,'status'=> '1']
                       );
             }
          }
          return redirect(Session::get('backUrl') ? Session::get('backUrl') :   '/home');
          //return redirect()->intended('/home');
        }
        else{
          return redirect()->intended('/servicetaker/profile');
        }
      }
      // else{
      //     return redirect('dashboard');
      // }
     }

     else {
      Auth::logout();
          $request->session()->put('error', 'Por favor verifique a sua caixa de email e clique no link recebido para ativar sua conta!');
          // $request->session()->flash('error', 'Please activate your account!');
              return redirect()->intended('/login');
          }
     }
    }
    public function logout(Request $request)
    {
      Auth::logout();
      $request->session()->flush();
      return redirect()->intended('/login'); 
    }
  }
