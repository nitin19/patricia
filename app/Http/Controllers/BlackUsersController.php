<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Models\BlackUser;

class BlackUsersController extends Controller
{
    public function create(Request $request){
    $request11 = array();

    $data = array('from_id'    => $request->from_id,
                  'to_id'=> $request->to_id,
                  'black_reason' => $request->black_reason,
                  'to_name' => $request->to_name,
                  'from_name' => $request->from_name,
                  'user_type' => $request->user_type,

                   );
     $userblock = BlackUser::where('from_id','=',$request->from_id)->where('to_id','=',$request->to_id)->first();
    if($userblock != ''){ 
        if($userblock != ''){  
      
        $request11['success']= False;
        $request11['msgcode']= '0';
        $request11['message']= 'Solicitação de bloqueio já submetida.';
      } 
     
    }else {
      $result = BlackUser::insert($data);
      if($result){
          
        $request11['success']= True;
        $request11['msgcode']= '1';
        $request11['msgcode'] = '1';
        $request11['message'] = 'Solicitação de bloqueio enviada para avaliação da nossa equipe.';
      } else {
        $request11['success']= False;
        $request11['msgcode']= '0';
        $request11['message']= 'something wrong. ';
        }
    }
    echo json_encode($request11);
  }
}
