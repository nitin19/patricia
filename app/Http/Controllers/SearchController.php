<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Models\MainCategory;
use App\Models\Category;
use App\User;
use App\Errorlogs;

class SearchController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }
  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    try{ 
    $price      = array();
    $current_ip = $request->ip();
    $ip         = $request->ip(); // $_SERVER['REMOTE_ADDR'];
    $cat_id     = trim($request->get('cat'));
    $zipcode    = trim($request->get('zipcode'));
    $area       = trim(urldecode($request->get('area')));
    $rating     = trim($request->get('rating'));  
    $sub_id     = trim($request->sub_id);
    $sortby     = trim($request->get('sortby')); 
    $cat        = trim($request->cat);
    $amount     = trim($request->amount);

    $name       = trim($request->name);
    $bio        = trim($request->bio);
    $certificate = trim($request->certificate);

    $frwdLat    = '';
    $frwdLng    = '';
    $latzip     = '';
    $lngzip     = '';
    $sys_latitude = '';
    $sys_longitude = '';

    if($area!='') {
      $AddrlatLong = $this->getAddrLatLong($area);
      if($AddrlatLong['lat']!='' && $AddrlatLong['lng']!='') {
        $frwdLat=$AddrlatLong['lat'];
        $frwdLng=$AddrlatLong['lng'];
        $sys_latitude=$AddrlatLong['lat'];
        $sys_longitude=$AddrlatLong['lng'];
      } else {
        if (Auth::user()) {   
              $aulatitude = Auth::user()->latitude;
              $aulongitude = Auth::user()->longitude;
              if($aulatitude != '' && $aulongitude != '') {
                $frwdLat = Auth::user()->latitude;
                $frwdLng = Auth::user()->longitude;
                $sys_latitude=Auth::user()->latitude;
                $sys_longitude=Auth::user()->longitude;
              } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
              }
            } else {
              $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
          }
      }
    } else {
        if (Auth::user()) {   
              $aulatitude = Auth::user()->latitude;
              $aulongitude = Auth::user()->longitude;
              if($aulatitude != '' && $aulongitude != '') {
                $frwdLat = Auth::user()->latitude;
                $frwdLng = Auth::user()->longitude;
                $sys_latitude=Auth::user()->latitude;
                $sys_longitude=Auth::user()->longitude;
              } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
              }
            } else {
              $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
          }
    }

    $incomprofile_provider_users_Arr = $this->getIncompleteprofileproviders();

    $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT( DISTINCT user_id SEPARATOR ',')) as user_id"))->distinct()->first();
    $provideusers = $providers->user_id;
    //print_r($provideusers); 
    $unit = "K";
    
    //DB::enableQueryLog(); 
    $query = DB::table('users');
      if($area!='') {
       $query->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
       $query->having('distances', '<', '60');
      } else {
        $query->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      }
    $query->join('main_category','users.cat_id','=','main_category.id');   
    $query->join('category','users.sub_id','=','category.cat_id');
    $query->join('provider_price_list','users.id','=','provider_price_list.user_id');
    $query->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
    if ($cat_id) {
            $query->where('users.cat_id',$cat_id);
    }
    if ($sub_id) {
            $query->where('users.sub_id',$sub_id);
    }

    if ($name) {
          $query->where('users.name', 'like', '%'.$name.'%');
    }

    if ($bio) {
          $query->where('users.bio', 'like', '%'.$bio.'%');
    }

    if ($certificate) {
          $query->where('users.additional_details', 'like', '%'.$certificate.'%');
    }

    if($rating!=''){
      $query->where('users.profile_rating','>=',$rating);
    }
    if($amount!=''){
      $minamount = $amount-($amount*(30/100));
      $maxamount = $amount+($amount*(30/100));
      $query->whereBetween('provider_price_list.amount', array($minamount, $maxamount));    
    }
    $query->where('users.user_role', 'Provider');
    $query->where('users.is_active', '1');
    $query->where('users.is_deleted', '0');
    $query->where('users.close_request_action', '0');
    $query->where('price_queries.priority', '1');
    if(Auth::user()){
      $query->where('users.id', '!=', Auth::user()->id);
    }

    if( count($incomprofile_provider_users_Arr) > 0 ) {
      $query->whereNotIn('users.id',$incomprofile_provider_users_Arr);
    }

    /*$query->where(
      function($querys) use ($provideusers){
        $querys->whereIn('users.id', explode(',', $provideusers));
        $querys->orWhere('users.user_role', 'Provider');
    });*/

    if($sortby == 'rating'){
      $query->orderBy('users.profile_rating','desc');
    }else if($sortby == 'price'){
    //$query->orderBy('distances', 'desc');
    $query->orderBy('provider_price_list.amount','asc');
    }else {
      $query->orderBy('distances','asc');
    }
    $query->distinct();
    $users = $query->simplePaginate(9);
    
    //$totalusers = $users->toArray();
    //if($sortby == 'price'){
      //if($users) {
      //   $usersData = $users->toArray();
      //   $resArr = json_decode( json_encode($usersData), true);
      //   foreach($users as $resArr){
      //      $byPriceSort = $this->aasort($resArr,"amount");
      //   }
      //   $users  = $byPriceSort;
      //}
    //}
    //dd(DB::getQueryLog()); exit;
    
    $mapquery = DB::table('users');
      if($area!='') {
       $mapquery->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
       $mapquery->having('distances', '<', '60');
      } else {
        $mapquery->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      }
    $mapquery->join('main_category','users.cat_id','=','main_category.id');   
    $mapquery->join('category','users.sub_id','=','category.cat_id');
    $mapquery->join('provider_price_list','users.id','=','provider_price_list.user_id');
    $mapquery->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
    if ($cat_id) {
            $mapquery->where('users.cat_id',$cat_id);
    }
    if ($sub_id) {
            $mapquery->where('users.sub_id',$sub_id);
    }

    if ($name) {
          $mapquery->where('users.name', 'like', '%'.$name.'%');
    }

    if ($bio) {
          $mapquery->where('users.bio', 'like', '%'.$bio.'%');
    }

    if ($certificate) {
          $mapquery->where('users.additional_details', 'like', '%'.$certificate.'%');
    }
    if($rating!=''){
      $mapquery->where('users.profile_rating','>=',$rating);
    }
    if($amount!=''){
      $minamount = $amount-($amount*(30/100));
      $maxamount = $amount+($amount*(30/100));
      $mapquery->whereBetween('provider_price_list.amount', array($minamount, $maxamount));    
    }
    $mapquery->where('users.user_role', 'Provider');
    $mapquery->where('users.is_active', '1');
    $mapquery->where('users.is_deleted', '0');
    $mapquery->where('users.close_request_action', '0');
    $mapquery->where('price_queries.priority', '1');
    if(Auth::user()){
      $mapquery->where('users.id', '!=', Auth::user()->id);
    }
    if( count($incomprofile_provider_users_Arr) > 0 ) {
      $mapquery->whereNotIn('users.id',$incomprofile_provider_users_Arr);
    }
    if($sortby == 'rating'){
      $mapquery->orderBy('users.profile_rating','desc');
    }else if($sortby == 'price'){
    $mapquery->orderBy('provider_price_list.amount','asc');
    }else {
      $mapquery->orderBy('distances','asc');
    }
    $mapquery->distinct();
    $mapusers = $mapquery->get();

    if(!empty(Auth::user())) {
      $final_data = array();
      foreach($users as $userblock){
        $provider_ids = $userblock->id;

        $pro_id = explode(',', $provider_ids);
        //print_r($pro_id);
        //DB::enableQueryLog();
       $alluserblocks = DB::table('block_providers')->where('taker_id','=',Auth::user()->id)->where('provider_id',$pro_id)->where('block_status','=','1')->count();
       array_push($final_data, $alluserblocks);
      } 
    } 
    if($cat_id!=''){  
       $mainCatName = MainCategory::select('name')->where('id',$cat_id)->first();
    } else {
      $mainCatName='';
    }
    $mainCatdata= MainCategory::where('is_active',1)->where('is_deleted',0)->get();
    if($mainCatName!=''){
      $main_category_data = $mainCatName->name;
    } else {
      $main_category_data = '';
    }
    $category = DB::table('category')->get();
    $get_categories = DB::table('main_category')->leftjoin('category','category.main_cat_id','=','main_category.id')->where('main_category.is_active',1)->where('main_category.is_deleted',0)->orderBy('category.cat_name')->get();
    $get_category1 = [];
    foreach($get_categories as $category){
      $get_category1[$category->name][] = ['id'=>$category->id,'cat_name'=>$category->cat_name,'cat_image'=>$category->cat_image,'cat_id'=>$category->cat_id];
    }
    if(!empty($request->sub_id)){
      $users_sub_list =  DB::table('users')->select('users.name as username','users.email','users.latitude','users.longitude','users.neighborhood','users.id as user_id','users.city','users.profile_image','category.cat_name','category.cat_image','longitude','latitude','profile_image','profile_rating','users.id','users.description','price','price_type','city','sub_id')
      ->Join('category','category.cat_id','=','users.sub_id')
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      ->where('sub_id',$request->sub_id)
      ->get();
    }
    $distance ='';
    $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','search')->where('is_active', '1')->where('is_deleted', '0')->first();

    return view('pages.search', compact('mainCatName', 'category', 'users', 'ip', 'current_ip', 'get_categories', 'get_category1', 'users_sub_list', 'main_category_data', 'mainCatdata', 'cat_id', 'zipcode', 'area', 'rating', 'cat', 'sortby', 'sub_id', 'amount', 'final_data', 'distance', 'frwdLat', 'frwdLng', 'mapusers', 'menu_meta_details', 'name', 'bio', 'certificate'));
   }
   catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
           if(Auth::user()){
              $errorlog->loggedin_id      = Auth::user()->id;
            }
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

  }

  public function getIncompleteprofileproviders(){

        $incomprofile_provider_users = DB::table('users')
                                      ->where('user_role', '=', 'Provider')
                                      ->where('is_active', '=', '1')
                                      ->where('is_deleted', '=', '0')
                                      ->where('close_request_action', '0')
                                      ->orderBy('id', 'DESC')
                                      ->get();

          $incomprofile_provider_users_Arr = array();  

          if( count($incomprofile_provider_users) > 0 ) {
           
            foreach($incomprofile_provider_users as $incomprofile_provider_user) {

            $provider_price_list_count = DB::table('provider_price_list')->where('user_id', '=', $incomprofile_provider_user->id)->count();

              if($incomprofile_provider_user->cpf_id =='' || $incomprofile_provider_user->phone =='' || $incomprofile_provider_user->cat_id =='' || $incomprofile_provider_user->sub_id =='' || $incomprofile_provider_user->zipcode =='' || $incomprofile_provider_user->address =='' || $incomprofile_provider_user->city =='' || $incomprofile_provider_user->neighborhood =='' || $incomprofile_provider_user->state =='' || $incomprofile_provider_user->country =='') {

                $incomprofile_provider_users_Arr[] = $incomprofile_provider_user->id;

              }

            }

          } 

        return $incomprofile_provider_users_Arr;                           
    }

  public function infoexist(Request $request) {
    $cat_name = $request->cat;
    $zipcode = $request->zipcode;
    $area =  $request->area;
    $rating     = trim($request->rating); 
    $ip         = $_SERVER['REMOTE_ADDR'];
    $frwdLat    = '';
    $frwdLng    = '';
    $latzip     = '';
    $lngzip     = '';
    $sys_latitude = '';
    $sys_longitude = '';
    $users      = '';
    
    if($area!='') {
      $AddrlatLong = $this->getAddrLatLong($area);
      if($AddrlatLong['lat']!='' && $AddrlatLong['lng']!='') {
        $frwdLat=$AddrlatLong['lat'];
        $frwdLng=$AddrlatLong['lng'];
        $sys_latitude=$AddrlatLong['lat'];
        $sys_longitude=$AddrlatLong['lng'];
      } else {
        if (Auth::user()) {   
              $aulatitude = Auth::user()->latitude;
              $aulongitude = Auth::user()->longitude;
              if($aulatitude != '' && $aulongitude != '') {
                $frwdLat = Auth::user()->latitude;
                $frwdLng = Auth::user()->longitude;
                $sys_latitude=Auth::user()->latitude;
                $sys_longitude=Auth::user()->longitude;
              } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
              }
            } else {
              $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
          }
      }
    } else {
        if (Auth::user()) {   
              $aulatitude = Auth::user()->latitude;
              $aulongitude = Auth::user()->longitude;
              if($aulatitude != '' && $aulongitude != '') {
                $frwdLat = Auth::user()->latitude;
                $frwdLng = Auth::user()->longitude;
                $sys_latitude=Auth::user()->latitude;
                $sys_longitude=Auth::user()->longitude;
              } else {
                $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
              }
            } else {
              $getips = $this->getIpLatLong($ip);
                if($getips['lat'] !='' && $getips['lng'] !='') {
                  $frwdLat = $getips['lat'];
                  $frwdLng = $getips['lng'];
                  $sys_latitude=$getips['lat'];
                  $sys_longitude=$getips['lng'];
                } else {
                  $frwdLat = '-23.60332035036982';
                  $frwdLng = '-46.723359508984345';
                  $sys_latitude='-23.60332035036982';
                  $sys_longitude='-46.723359508984345';
                }
          }
    }

    $incomprofile_provider_users_Arr = $this->getIncompleteprofileproviders();

    $providers = DB::table('provider_price_list')->select('user_id', DB::raw("(GROUP_CONCAT( DISTINCT user_id SEPARATOR ',')) as user_id"))->distinct()->first();
    $provideusers = $providers->user_id;
      //print_r($provideusers);
    $unit = "K";
      
    //  DB::enableQueryLog(); 
    $query = DB::table('users');
      if($area!='') {
       $query->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
       $query->having('distances', '<', '60');
      } else {
        $query->select('users.name as username','users.id as user_id','users.cat_id as user_main_cat_id','users.email','users.latitude','users.longitude','users.neighborhood','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.address','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode','provider_price_list.amount','provider_price_list.query_id','price_queries.queries as pvquery','price_queries.priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      }
      $query->join('main_category','users.cat_id','=','main_category.id');   
      $query->join('category','users.sub_id','=','category.cat_id');
      $query->join('provider_price_list','users.id','=','provider_price_list.user_id');
      $query->join('price_queries','provider_price_list.query_id','=','price_queries.id');   
      if ($cat_name) {
        $query->where('users.cat_id',$cat_name);
      }
      if($rating!=''){
        $query->where('users.profile_rating','>=',$rating);
      }

      $query->where('users.user_role', 'Provider');
      $query->where('users.is_active', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.close_request_action', '0');
      $query->where('price_queries.priority', '1');
      
      if(Auth::user()){
        $query->where('users.id', '!=', Auth::user()->id);
      }

      if( count($incomprofile_provider_users_Arr) > 0 ) {
        $query->whereNotIn('users.id',$incomprofile_provider_users_Arr);
      }

      /*$query->where(
        function($querys) use ($provideusers){
          $querys->whereIn('users.id', explode(',', $provideusers));
          $querys->orWhere('users.user_role', 'Provider');
      });*/

      $query->orderBy('distances','asc');
      $query->distinct();
      $users = $query->get();

    if(count($users) ==0){
      echo '0';
    }

    $category_exist = User::select('users.name as username','users.email','main_category.name as name','category.cat_name','category.cat_image','main_category.cat_image as maincat_image','users.longitude','users.address','users.latitude','users.neighborhood','users.profile_image','users.profile_rating','users.id','users.description','users.price','users.price_type','users.city','users.sub_id','users.zipcode')
    ->join('main_category', 'main_category.id','=','users.cat_id')
    ->join('category','category.main_cat_id','=','main_category.id')
    ->where('users.is_active', '1')
    ->where('users.is_deleted', '0')
    ->where('users.close_request_action', '0')
    ->where('user_role', 'Provider')
    ->Where('users.cat_id','=',$cat_name)
    ->count();

    if($category_exist>0){
      echo '1';
    }
    elseif($category_exist == 0){
      echo '0';
    }
    else {
      echo '0';
    }
  }
  
  public function subcategory_filter($id){
    $users_list =  DB::table('users')->select('cat_name','sub_id')
    ->Join('category','category.main_cat_id','=','users.cat_id')
    ->Join('main_category', 'main_category.id','=','category.main_cat_id')
    ->where('users.is_active', '1')
    ->where('users.is_deleted', '0')
    ->where('users.close_request_action', '0')
    ->where('sub_id',$id)
    ->get();
    if(!empty($users_list)){
      $response['success']="true";
      $response['message']="data found";
      $response= $users_list;
    }
    else{
      $response['success']="false";
      $response['message']="data not  found";
    }
    echo json_encode($response);
  }
  
  function getAddrLatLong($address){  
    $mylat = '';
    $mylng = '';

    /*$url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".urlencode($address)."&language=en-US&additionaldata=&maxresults=20&gen=8";*/
    $neightborhood_data = DB::table('users')->Where('neighborhood','LIKE','%'.$address.'%')->first();
    if($neightborhood_data!=''){
      $search_address = $neightborhood_data->zipcode;
    } else{
      $search_address = $address;
    }

    
    $url ="https://maps.googleapis.com/maps/api/geocode/json?address='".urlencode($search_address)."'&sensor=true&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&language=pt_BR"; 
    $filestring = file_get_contents($url);
    $data = json_decode($filestring);
    $mylat = $data->results[0]->geometry->location->lat;
    $mylng = $data->results[0]->geometry->location->lng;
    /*$filestring = file_get_contents($url);
    $filearray = explode("\n", $filestring);
    $data = json_decode($filearray[0]);
    foreach($data as $k=>$v) {
      $view = $v->View;
      $view_count = count($view);
      if($view_count>0){
        foreach($view as $result_k=>$result_v) {
          $Result = $result_v->Result;
          $mylat = $Result[0]->Location->DisplayPosition->Latitude;
          $mylng = $Result[0]->Location->DisplayPosition->Longitude;
        }
      }
    }*/ 
    $data = array('lat'=>$mylat, 'lng'=>$mylng);
    return $data;
   }
   
  function getZipLatLong($zipcode){     //latlong according to zipcode
    $mylat = '';
    $mylng = '';
    //$url ="https://geocoder.api.here.com/6.2/geocode.json?app_id=QSZdkzJQd9swd9TIXUFQ&app_code=3N7kVDNeS3qmzNhvtDIUUQ&searchtext=".urlencode($zipcode)."&language=en-US&additionaldata=&maxresults=20&gen=8";
    $url ="https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zipcode)."&sensor=true&key=AIzaSyDz9e6HlQAGcHdng8J5zRzw3sH2m-fVk_Y&language=pt_BR"; 

    $filestring = file_get_contents($url);
    $data = json_decode($filestring);
    $mylat = $data->results[0]->geometry->location->lat;
    $mylng = $data->results[0]->geometry->location->lng;

    /*$filestring = file_get_contents($url);
    $filearray = explode("\n", $filestring);
    $data = json_decode($filearray[0]);
    foreach($data as $k=>$v) {
      $view = $v->View;
      $view_count = count($view);
      if($view_count>0){
        foreach($view as $result_k=>$result_v) {
          $Result = $result_v->Result;
          $mylat = $Result[0]->Location->DisplayPosition->Latitude;
          $mylng = $Result[0]->Location->DisplayPosition->Longitude;
        }
      }
    } */
    $data = array('lat'=>$mylat, 'lng'=>$mylng);
    return $data;
   }
   
  function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    /*$ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=a3d9e92c7201bb32d8115142ae661d91");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];*/
    return $data;
   }
   
  function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
  
  function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
  }
}

