<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Feedback;
use App\Errorlogs;
use App\user;
use App\Models\Spavailability;
use App\Models\Notification;
use App\Models\BlackUser;
use IntlDateFormatter;
use DateTime;


class ProfileController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    try{
      $user_info = DB::table('users')
      ->where('id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->first();
      $user_documents = DB::table('user_documents')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('document_status', '1')
                      ->where('is_deleted', '0')
                      ->get();
      return view('profile.index', compact('user_info','user_documents'));
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }catch(\Exception $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }
  }
  public function show(Request $request, $id) {
    try{
          //$ip = $_SERVER['REMOTE_ADDR'];
          //$userlatlng = DB::table('users')->where('id','=',Auth::user()->id)->where('is_active','=','1')->first();
          /*$userlatlng = DB::table('users')->where('id','=',$id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $getips= $this->getIpLatLong($ip);
            $sys_latitude=$getips['lat'];
            $sys_longitude=$getips['lng'];
          }*/

      /*$user_info = User::select('users.*','category.*','main_category.name as main_cat_name',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
      ->where('users.id', $id)
      ->join('main_category','main_category.id','=','users.cat_id')
      ->join('category','category.cat_id','=','users.sub_id')
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      //->where('user_role', 'Provider')
      ->first();
      */

      $ip = $request->ip(); 
      $unit = "K";

      if(!empty(Auth::user())) {
          $userlatlng = DB::table('users')->where('id','=',Auth::user()->id)->where('is_active','=','1')->first();
          if($userlatlng->latitude != '' && $userlatlng->longitude != '') {
            $sys_latitude=$userlatlng->latitude;
            $sys_longitude=$userlatlng->longitude;
          } else {
            $getips= $this->getIpLatLong($ip);
            $sys_latitude=$getips['lat'];
            $sys_longitude=$getips['lng'];
          }
        } else {
          $getips= $this->getIpLatLong($ip);
          $sys_latitude=$getips['lat'];
          $sys_longitude=$getips['lng'];
        }

      $user_info = User::select('users.*','category.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))
      ->where('users.id', $id)
      ->join('main_category','main_category.id','=','users.cat_id')
      ->join('category','category.cat_id','=','users.sub_id')
      ->where('users.is_active', '1')
      ->where('users.is_deleted', '0')
      ->first();

      $get_admin_info  = User::where('user_role','admin')->where('users.is_active', '1')
      ->where('users.is_deleted', '0')->first();

      /*$similar_category_users = User::select('users.*','price_queries.queries as price_des','category.cat_name as subcat_name','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"))   
      ->leftjoin('provider_price_list','provider_price_list.user_id','=','users.id')
      ->leftjoin('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
      ->leftjoin('category', 'category.cat_id', '=', 'users.sub_id')
      ->where('users.cat_id',$user_info->main_cat_id)
      ->where('users.is_active', '1')
      ->where('price_queries.priority', '1')
      ->where('users.is_deleted', '0')
      ->where('users.id','!=', $get_admin_info->id)
      ->where('users.id','!=',$id)
      ->where('user_role', 'Provider')->limit('5')->get();*/

      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->having('distances', '<', '25');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.sub_id',$user_info->subcat_id);
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit('5');
      $similar_subcategory_users = $query->get();

       if( count($similar_subcategory_users) < 5) {
        $similar_main_category_users = $this->getSimilarMaincatUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users);
        if( count($similar_main_category_users) > 0 ) {
          $merged = $similar_main_category_users->merge($similar_subcategory_users);
          $merged_category_users = $merged->all();
          if( count($merged_category_users) < 5 ) {
          $merged_cat_withnearbydist_users = $this->getMergedCatWithNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$merged_category_users);
            if( count($merged_cat_withnearbydist_users) > 0 ) {
              $mergedAll = $merged_cat_withnearbydist_users->merge($merged_category_users);
              $similar_category_users = $mergedAll->all();
            } else {
            $randomUsers = $this->getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info);
            $similar_category_users = $randomUsers;
            }
          } else {
             $similar_category_users = $merged_category_users;
          }
        } else {
          $subcat_withnearbydist_users = $this->getNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users);
          if( count($subcat_withnearbydist_users) > 0 ) {
              $mergedAll = $subcat_withnearbydist_users->merge($similar_subcategory_users);
              $similar_category_users = $mergedAll->all();
            } else {
            $randomUsers = $this->getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info);
            $similar_category_users = $randomUsers;
            }
        }
      } else {
        $similar_category_users = $similar_subcategory_users;
      }

      $feedbacks = Feedback::select(DB::raw('AVG(rating) AS avg_rating ,provider_id,user_id,message,users.*'))->join('users','users.id','=','feedback.user_id')->where('feedback.provider_id',$id)->where('feedback.role','taker')->where('approve_status',1)->paginate(5);

//DB::enableQueryLog();

      $userreview = DB::table('feedback')
            ->join('users', 'users.id', '=', 'feedback.user_id')
            ->select('users.*', 'feedback.*')
            ->where('feedback.provider_id',$id)
            ->where('approve_status',1)
            ->where('role','taker')
            //->get()->toArray();
            ->paginate(5);
       //    dd(DB::getQueryLog());


      $reviewrating = Feedback::select(DB::raw('AVG(rating) AS avg_rating'))->where('feedback.provider_id',$id)->where('approve_status',1)->first()->toArray();

      $booking_status = '';
      $check_existing_booking = Booking::where('profile_id',$id)->where('user_id',Auth::user()->id)->first();
      if(empty($check_existing_booking)){
        $check_existing_booking = new Booking();
      }
      else{
        $booking_status =   $check_existing_booking->booking_verfied;
      }
      $check_feedback = Feedback::where('user_id',Auth::id())
      ->where('provider_id',$user_info->id)
      ->where('booking_id',$check_existing_booking->booking_id)
      ->count();
      $review_count = DB::table('review')
      ->where('review_reciever_id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->count();
      $user_gallery = DB::table('user_gallery')
      ->where('user_id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->get();
      $user_lat = $user_info->latitude;
      $user_lon = $user_info->longitude;
      $query = DB::table('users')
      ->where('is_active', '1')
      ->where('is_deleted', '0');
      $distance =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
      $category = DB::table('users')
      ->select('category')
      ->where('id', '=', $id)
      ->value('id');
      $similar_category = DB::table('users')
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      ->orWhere('category', 'like', '%' . $category . '%')
      ->limit('5')
      ->get();
      $get_user_availablity =  DB::table('spavailability')
      ->join('users', 'users.id', '=', 'spavailability.user_id')
      ->select('spavailability.*', 'users.name')
      ->where('users.id', $id)
      ->where('is_active', '1')
      ->where('is_deleted', '0')
      //->where('user_role', 'Provider')
      ->first();
      $user_documents = DB::table('user_documents')
                      ->where('user_id', $id)
                      ->where('is_active', '1')
                      ->where('document_status', '1')
                      ->where('is_deleted', '0')
                      ->get();
      $user_id = $user_info->id;
      $current_userid = Auth::id();
      $wish_list_exist = DB::table('wish_list')
      ->where('wish_profile_id', $user_id)
      ->where('user_id', $current_userid)
      ->where('status', '=', 1)
      ->count();
      $complete_booking=DB::table('booking')
      ->where('profile_id',$user_id)
      ->where('booking_verified','1')
      ->count(); 
      $repeat_clients = Booking::select(DB::raw('COUNT(*)'))
      ->where('profile_id',$user_id)  
      ->groupby('profile_id','user_id')
      ->havingRaw('COUNT(*) > 1')
      ->get()
      ->count();
      $average_rating = Feedback::select(DB::raw('provider_id,AVG(rating) AS avg_rating ,rating,users.id as u_id,users.name as user_name'))->join('users','users.id','=','feedback.provider_id')->groupBy('feedback.provider_id')->where('users.is_active', '1')
      ->where('users.is_deleted', '0')->get();
      $provider_price_lists = DB::table('provider_price_list')
      ->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id')
      ->where('provider_price_list.user_id', $id)
      ->select('price_queries.*', 'provider_price_list.id', 'provider_price_list.amount', 'provider_price_list.query_id')
      ->orderBy('price_queries.id')
      ->get()->toArray();
      $get_feedbacks = [];
      if(count($similar_category_users) > 0){
        foreach($similar_category_users as $cat_info)  {
          $get_user_feedbacks = Feedback::where('provider_id',$cat_info->id)->where('approve_status',1)->where('role','taker')->count();
          $get_feedbacks[$cat_info->id]= $get_user_feedbacks;
          $user_lat = $cat_info->latitude;
          $user_lon = $cat_info->longitude;
          $distance1  =$this->distance($sys_latitude, $sys_longitude, $user_lat, $user_lon, $unit);
          $distance_array[$cat_info->name] = $distance1;
        }
      }
      $checkexist_user_blklist = BlackUser::where('from_id','=',Auth::user()->id)->where('to_id','=',$id)->first();  
      $menu_meta_details = DB::table('menu_meta_details')->where('menu_slug','profile')->where('is_active', '1')->where('is_deleted', '0')->first();
      return view('profile.show', compact('user_info','distance','category', 'similar_category','complete_booking','repeat_clients','wish_list_exist', 'user_gallery', 'review_count','get_user_availablity','get_categories','check_existing_booking','feedbacks','similar_category_users','id','average_rating', 'provider_price_lists','get_feedbacks','distance_array','check_feedback', 'userreview', 'reviewrating','user_documents','menu_meta_details','checkexist_user_blklist'));
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }catch(\Exception $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }
  }

  function getSimilarMaincatUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users) {
      $rowCount = count($similar_subcategory_users);
      $limit = (5 - $rowCount);
      $similar_subcat_user_Arr = array();
      if($rowCount > 0) {
        foreach($similar_subcategory_users as $similar_subcat_user) {
          $similar_subcat_user_Arr[] = $similar_subcat_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->having('distances', '<', '25000');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.cat_id',$user_info->main_cat_id);
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($similar_subcat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$similar_subcat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $similar_main_category_users = $query->get();
    //  dd(DB::getQueryLog());
      return $similar_main_category_users;
  }
  
  function getMergedCatWithNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$merged_category_users) {

      $rowCount = count($merged_category_users);
      $limit = (5 - $rowCount);
      $merged_cat_user_Arr = array();
      if($rowCount > 0) {
        foreach($merged_category_users as $merged_category_user) {
          $merged_cat_user_Arr[] = $merged_category_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
    //  $query->having('distances', '<', '25000');
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($merged_cat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$merged_cat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $merged_cat_withnearbydist_users = $query->get();
    //  dd(DB::getQueryLog());
      return $merged_cat_withnearbydist_users;
  }

  function getNearbyDistUsers($id,$sys_latitude,$sys_longitude,$user_info,$similar_subcategory_users) {

      $rowCount = count($similar_subcategory_users);
      $limit = (5 - $rowCount);
      $similar_subcat_user_Arr = array();
      if($rowCount > 0) {
        foreach($similar_subcategory_users as $similar_subcat_user) {
          $similar_subcat_user_Arr[] = $similar_subcat_user->id;
        }
      }
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      if( count($similar_subcat_user_Arr) > 0 ) {
        $query->whereNotIn('users.id',$similar_subcat_user_Arr);
      }
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit($limit);
      $subcat_withnearbydist_users = $query->get();
    //  dd(DB::getQueryLog());
      return $subcat_withnearbydist_users;
  }

  function getRandomUsers($id,$sys_latitude,$sys_longitude,$user_info) {
      
    //  DB::enableQueryLog();
      $query = DB::table('users');
      $query->select('users.*','main_category.name as main_cat_name','main_category.id as main_cat_id','category.cat_name as subcat_name','category.cat_id as subcat_id','price_queries.queries as price_des','provider_price_list.amount as amount','provider_price_list.query_id as query_id','price_queries.priority as priority',DB::raw("(6371 * acos (cos ( radians('$sys_latitude') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('$sys_longitude') ) + sin ( radians('$sys_latitude') ) * sin( radians( latitude ) ) ) ) AS distances"));
      $query->join('provider_price_list','provider_price_list.user_id','=','users.id');
      $query->join('price_queries', 'provider_price_list.query_id', '=', 'price_queries.id');
      $query->join('main_category','main_category.id','=','users.cat_id');
      $query->join('category', 'category.cat_id', '=', 'users.sub_id');
      $query->where('users.is_active', '1');
      $query->where('price_queries.priority', '1');
      $query->where('users.is_deleted', '0');
      $query->where('users.id','!=',$id);
      $query->where('user_role', 'Provider');
      $query->orderBy('distances','asc');
      $query->distinct();
      $query->limit(5);
      $randomUsers = $query->get();
    //  dd(DB::getQueryLog());
      return $randomUsers;
  }
  
  public function payment(Request $request){
    $startdate = date("Y-m-d H:i:s", strtotime($request->startdate));
    $enddate = date("Y-m-d H:i:s", strtotime($request->enddate));
    $cat_name = $request->cat_name;
    $booking_msg = $request->booking_msg;
    $payment_type = $request->payment_type;
    $total_amount = $request->total_amount;
    $payment_type = $request->payment_type;
    $profile_id = $request->profile_id;
    $user_id = $request->user_id;
    $current_date = date("Y/m/d h:i:sa");
    $insertedId = DB::table('booking')->insertGetId(
      ['profile_id' => $profile_id,
      'user_id' => $user_id,
      'category' => $cat_name,
      'booking_message' => $booking_msg,
      'booking_amount' => $total_amount,
      'payment_type' => $payment_type,
      'booking_start_time' => $startdate,
      'booking_end_time' => Null ]
    );
    echo $insertedId;
  }
  public function provider_booking(Request $request){
    try{
           
        $startdate = trim($request->startdate);
        $cat_name = trim($request->cat_name);
        $sub_cat_name = trim($request->sub_cat_name);
        $booking_msg = trim($request->booking_msg);
        $profile_id = trim($request->profile_id);
        $user_id = trim($request->user_id);
        $booking_amount = trim($request->total_amount);
        $spavailabilityInfo = Spavailability::where('user_id', $profile_id)->where('status', 1)->first();
        $fromDate = date("Y-m-d", strtotime($startdate) );
        $getBooking = DB::table('booking')
               ->where('profile_id',$profile_id)
               ->where(function ($query) use ($fromDate) {
                 $query->where('booking_start_time',$fromDate);
              })->count();
        $providerEmail = DB::table('users')
                  ->select('email')
                  ->where('id', '=', $profile_id)
                  ->first();
        $to = $providerEmail->email;
        $insertedId = DB::table('booking')->insertGetId(
                  ['profile_id' => $profile_id,
                  'user_id' => $user_id,
                  'category' => $cat_name,
                  'sub_category' => $sub_cat_name,
                  'booking_message' => $booking_msg,
                  'booking_amount' => $booking_amount,
                  'payment_type' => 'Paypal',
                  'booking_start_time' => $fromDate,
                  //'booking_end_time' => Null,
                  'transaction_id'=>'0',
                  'payment_confirm'=>'1',
                  'is_active'=>'1',
                  'is_deleted'=>'0'
                ]
                );
        if($insertedId) {
            $providerEmail = DB::table('users')
                  ->select('email')
                  ->where('id', '=', $profile_id)
                  ->first();

            $notifie = DB::table('notifications')->insert(
                    ['user_id' => $profile_id, 'notificaton_type' => 'booking','type_id' => $insertedId,'notification_role' => Auth::user()->user_role]
            );
            $fullname  = Auth::user()->name;
            $email = Auth::user()->email;
            $to = $providerEmail->email;
            $book_date = $startdate;
            $booking_category = $cat_name;
            $booking_subcategory = $sub_cat_name;
            if($booking_amount){
              $amount = $booking_amount;
            } else{
              $amount = 'Não definido pelo(a) contratante';
            }
            if($book_date){
              $fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
              );
              $dt = new DateTime($book_date. "+1 days");
              $d = $dt->format('d');
              $m = $dt->format('m');
              $y = $dt->format('Y');
              $booking_date = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
            } else{
              $booking_date = " ";
            }
            
            $logo_url = url('/public').'/images/logo.png';
            $mail_bg_img = url('/public').'/images/bg.jpg';
            $email_icon = url('/public').'/images/email_icon_mail.png';
            $LI_png = url('/public').'/images/LI.png';
            $fb_png = url('/public').'/images/fb.png';
            $insta_png = url('/public').'/images/insta.png';
            $messageBody = '';
            $messageBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                  <html xmlns:v="urn:schemas-microsoft-com:vml">
                  <head>
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
                      <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
                      <!--[if !mso]><!-- -->
                      <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
                      <!-- <![endif]-->
                      <title>Email Template</title>
                      <style type="text/css">
                      * {font-family: "Lato", sans-serif;}
                      </style>
                  </head>
                  <body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                  <table width="690"  style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                      <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                        <tr>
                          <td align="center">
                            <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590">
                              <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <table border="0" align="center" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                                      <tr>
                                        <td align="center" height="70" style="height:70px;">
                                          <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="100" border="0" style="display: block; width: 150px;" src="'.$logo_url.'" alt="" /></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="center"><hr></td>
                                      </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                          <tr>
                            <td align="center">
                              <table border="0" align="center" width="" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                                <tr>
                                  <td align="center" style="color: #1497d5; font-size: 44px; font-weight:700; line-height: 35px;" class="main-header">
                                    <div style="line-height: 35px">
                                      <span style="color: #1497d5;line-height: 45px;max-width: 500px;display: block;">Você tem uma solicitação de serviço</span>
                                    </div>
                                    <p style="color: #333333; font-size: 19px;font-weight:400; line-height: 35px;margin-top:15px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço:</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                                      <tr>
                                        <td align="center" style="color: #4a4545; font-size: 16px; line-height: 24px;font-weight:700;">
                                            <div style="line-height: 24px">'.$booking_msg.'</div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr class="hide">
                            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                          </tr>
                      </table>
                      <table border="0" width="690" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                        <tr>
                          <td align="center">
                            <table border="0" width="490" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590" style="border: 1px solid #92a1ab;border-radius: 25px; ">
                                <tr>
                                  <td height="25">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="container580" >
                                      <tr>
                                        <td width="30">&nbsp;</td>
                                        <td align="left" style="color: #333333; font-size: 18px; font-family: "Work Sans", Calibri, sans-serif; line-height: 32px;">
                                          <div  style="color: #0c3e64;font-size: 19px;">
                                            <b style="color: #1497d5;padding-right: 5px; font-weight: 100;">Nome:</b>' . $fullname .'
                                          </div>
                                          <div style="color: #0c3e64;font-size: 19px;">
                                            <b style="color: #1497d5;padding-right: 5px; font-weight: 100;">E-mail:</b> 
                                            ' . $email .'
                                          </div>
                                          <div style="color: #0c3e64;font-size: 19px;">
                                            <b style="color: #1497d5;padding-right: 5px; font-weight: 100;">Serviço:</b>' 
                                             . $booking_subcategory .' ('.$booking_category.')
                                          </div>
                                          <div style="color: #0c3e64;font-size: 19px;">
                                            <b style="color: #1497d5;padding-right: 5px; font-weight: 100;">Data de preferência para o serviço:</b> '.$booking_date.'
                                          </div>
                                          <div style="color: #0c3e64;font-size: 19px;line-height:25px;">
                                             <b style="color: #1497d5;padding-right: 5px; font-weight: 100;">O contratante selecionou essa opção como a mais apropriada referente ao preço estimado pelo serviço, conforme informado por você no nosso site:</b>'.$amount.'
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="30">&nbsp;</td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
                          </tr>
                          <table border="0" width="690" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;"> 
                            <tr>
                              <td align="center">
                                <table border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;overflow: hidden;background-size: cover;background-position: center;">
                                  <tr>
                                    <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" style="color: #ffffff; font-size: 14px; font-family: "Work Sans", Calibri, sans-serif; line-height: 26px;">
                                      <div style="line-height: 26px;">
                                        <a href="https://www.navizinhanca.com/serviceprovider/booking/pending" style="background-color: #325c7c;border: none; border-radius: 30px; padding: 10px 40px; margin-top: 40px;  color: #fff;   font-size: 24px;    cursor: pointer; text-decoration: none;">Analisar e responder</a>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="70">&nbsp;</td>  
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                      </table>
                      <table border="0" width="590" cellpadding="0" cellspacing="0" bgcolor="f4f4f4" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                        <tr>
                          <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">
                            <table border="0" width="690" cellpadding="0" cellspacing="0" class="container590" style="margin: 0 auto;overflow: hidden;background-image: url(https://www.navizinhanca.com/public/images/bg.jpg);background-size: cover;background-position: center;">
                              <tr> 
                                <td>
                                  <div class="social-icons" style="text-align: center;">
                                    <a href="https://www.instagram.com/navizinhanca/"  style="display: inline-block;padding-right: 5px;">
                                        <img src="'.$insta_png.'">
                                    </a>
                                    <a href="https://www.linkedin.com/company/navizinhanca/" style="display: inline-block;padding-right: 5px;">
                                      <img src="'.$LI_png.'">
                                    </a>
                                    <a href="https://www.facebook.com/navizinhanca/" style="display: inline-block;padding-right: 5px;"> 
                                        <img src="'.$fb_png.'">
                                    </a>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                        </tr>
                      </table>
                    </table>
                  </body>
                  </html>';
                  $subject ="NaVizinhança: Você tem uma solicitação de serviço";
                  $headers = "From: " .$email. "\r\n";
                  // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
                  // $headers .= "CC: dev2.bdpl@gmail.com\r\n";
                  $headers .= "MIME-Version: 1.0\r\n";
                 // $headers .='X-Mailer: PHP/' . phpversion();
                  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                  $mailsend = mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com');
                  $rtnMsg = 'Mensagem enviada por e-mail com sucesso!';
                  echo json_encode(array('msgCode'=>"1",'message'=>$rtnMsg));

                } else {
                  $rtnMsg = 'Sorry fail to save your booking request';
                  echo json_encode(array('msgCode'=>"0",'message'=>$rtnMsg));
                }
      }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }catch(\Exception $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }
  }
  public function checkbookingavailability(Request $request){
    try{

    $startdate = trim($request->startdate);
    //$enddate = trim($request->enddate);
    $profile_id = trim($request->profile_id);
    $user_id = trim($request->user_id);
    $spavailabilityInfo = Spavailability::where('user_id', $profile_id)->where('status', 1)->first();
    if( $spavailabilityInfo->isCompleted == '1' ) {
        $availabledaysArr = array();
        $calculatedaysArr = array();
        $from_date=strtotime($startdate);
        // $to_date=strtotime($enddate);
        $current=$from_date;
        // while($current<=$to_date) {
        //   $days[]=date('l', $current);
        //   $current=$current+86400;
        // }
        foreach($days as $key=> $day) {
          $calculatedaysArr[] = $day;
        }
        $uniqueArr = array_unique($calculatedaysArr);
        if($spavailabilityInfo->openhrs=='1') {
          if($spavailabilityInfo->day_monday=='1') {
            $availabledaysArr[] = 'Monday';
          }
          if($spavailabilityInfo->day_tuesday=='1') {
            $availabledaysArr[] = 'Tuesday';
          }
          if($spavailabilityInfo->day_wednesday=='1') {
            $availabledaysArr[] = 'Wednesday';
          }
          if($spavailabilityInfo->day_thursday=='1') {
            $availabledaysArr[] = 'Thursday';
          }
          if($spavailabilityInfo->day_friday=='1') {
            $availabledaysArr[] = 'Friday';
          }
          if($spavailabilityInfo->day_saturday=='1') {
            $availabledaysArr[] = 'Saturday';
          }
          if($spavailabilityInfo->day_sunday=='1') {
            $availabledaysArr[] = 'Sunday';
          }
        } else {
          $availabledaysArr = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
        }
        $diffArr = array_diff($uniqueArr, $availabledaysArr);
         $fromDate = date("Y-m-d", strtotime($startdate) );
         //$toDate = date("Y-m-d", strtotime($enddate) );
         $getBooking = DB::table('booking')
         ->where('profile_id',$profile_id)
         ->where(function ($query) use ($fromDate) {
           $query->where('booking_start_time',$fromDate);
           // $query->orwhereBetween('booking_end_time',[$fromDate, $toDate]);
         })->count();
          $rtnMsg = 'Disponível';
          echo json_encode(array('msgCode'=>"1",'message'=>$rtnMsg));
        }
  }
  catch(\Illuminate\Database\QueryException $e){
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
    $errorlog->loggedin_id      = Auth::user()->id;
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror'));
  }catch(\Exception $e){
    $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

    $errorlog                   = new Errorlogs;
    $errorlog->error_message    = $e->getMessage();
    $errorlog->line_number      = $e->getLine();
    $errorlog->file_name        = $e->getFile();
    $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
    $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
    $errorlog->loggedin_id      = Auth::user()->id;
    $errorlog->ip_address       = "";
    $errorlog->save();
    return view('errors.custom',compact('customerror'));
  }
}
public function getmybooking() {
  $profile_id = '8';
  $fromDate = '2019-02-19';
  $toDate = '2019-02-25';
         // DB::enableQueryLog();
  $getBooking = DB::table('booking')->where('profile_id', '=', $profile_id)->whereBetween('booking_start_time',[$fromDate, $toDate])->count();
    echo '>>'.$getBooking;
         // dd(DB::getQueryLog());
         // dd($getBooking);

}
public function addwhish(Request $request){
  $userid = $request->userid;
  $insert_whish = DB::table('wish_list')->insert(
    ['user_id' => $userid, 'wish_profile_id' => '1']
  );
  if($insert_whish!=''){
    echo 'true';
  }
}
/* function calculates ditance between user sytem lat long and profile userlat longs in miles*/
public function removewhish(Request $request){
  $userid = $request->userid;
  $remove_whish = DB::table('wish_list')->where('user_id', $userid)->delete();
  if($remove_whish!=''){
    echo 'true';
  }
}
function distance($lat1, $lon1, $lat2, $lon2, $unit) {
  if (($lat1 == $lat2) && ($lon1 == $lon2)) {
    return 0;
  }
  else {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }
}
// get lat and long with ip address

 function getIpLatLong($ipaddress){
    $data = array();
    $ip_server = $ipaddress; 
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ip_server}/json"));
    $each_data = explode(',', $details->loc);
    $latitude_center  = $each_data[0];
    $longitude_center = $each_data[1];
    $data['lat']=$latitude_center;
    $data['lng']=$longitude_center ;
    return $data;
   }

/*function getIpLatLong($ipaddress){
    $data = array();
    $ipstackResult  = file_get_contents("http://api.ipstack.com/".$ipaddress."?access_key=08f1bc91f71c45880fefc03eb7f5e695");
    $ipstackData  =  json_decode($ipstackResult ,true);
    $data['lat']=@$ipstackData['latitude'];
    $data['lng']=@$ipstackData['longitude'];
    return $data;
   }*/

// fucntion to create booking enteries in db with  payment
  public function provider_booking_old(Request $request){// fucntion to create booking enteries in db with out payment
    try{
        //echo "<pre>";print_r($request->all());die;
      $startdate = date("Y-m-d H:i:s", strtotime($request->startdate));
      $enddate = date("Y-m-d H:i:s", strtotime($request->enddate));
      $cat_name = $request->cat_name;
      $booking_msg = $request->booking_msg;
      $payment_type = $request->payment_type;
      $total_amount = $request->total_amount;
      $payment_type = $request->payment_type;
      $profile_id = $request->profile_id;
        $user_id = Auth::user()->id;// user id for auth user saving booking in table wthout payment

        $check_existing_booking = Booking::where('profile_id',$profile_id)->where('user_id',Auth::user()->id)->where('booking_verified',0)->first();

        if(!empty($check_existing_booking)){
          return redirect()->action('ProfileController@show', $profile_id)->with('error','your previous booking is not confirmed yet');
        }
        else{
          $check_existing_booking = [];
        }
        // $user_id = $request->user_id;
        $providerEmail = DB::table('users')
        ->select('email')
        ->where('id', '=', $profile_id)
        ->first();
        //print_r($providerEmail);die;
        $current_date = date("Y/m/d h:i:sa");
        $insertedId = DB::table('booking')->insertGetId(
          ['profile_id' => $profile_id,
          'user_id' => $user_id,
          'category' => $cat_name,
          'booking_message' => $booking_msg,
          'booking_amount' => $total_amount,
          'payment_type' => 'Paypal',
          'booking_start_time' => $startdate,
          'booking_end_time' => Null ,
          'transaction_id'=>'0',
          'payment_confirm'=>'1',
          'is_active'=>'1',
          'is_deleted'=>'0'
        ]
      );
        if($insertedId){
          /*add booking  data in notification table */
          $notifie = DB::table('notifications')->insert(
            ['user_id' => $user_id, 'notificaton_type' => 'booking','type_id' => $insertedId,'notification_role' => Auth::user()->user_role]
          );

          $fullname  = Auth::user()->name;
          $email = Auth::user()->email;
        //$to = 'dev2.bdpl@gmail.com';
          $to = $providerEmail->email;
          $logo_url = url('/public').'/images/yellow_logo_new.png';
          $mail_bg_img = url('/public').'/images/bg.png';
          $email_icon = url('/public').'/images/email_icon_mail.png';
          $mail_icon = url('/public').'/images/mail.png';
          $phone_img = url('/public').'/images/phone1.png';
          $thankyouimg = url('/public').'/images/thankyou.png';
          $messageBody    = '';
          $messageBody = '<html>
          <head>
          <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
          </head>
          <body>
          <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
          <div class="container">
          <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
          <div class="" style="width: 500px;margin: 0 auto;">
          <div class="row">
          <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
          <img style="width:150px" src="'.$logo_url.'" alt="Logo">
          </div>
          <div class="" style="text-align: center;padding-bottom: 0px;">
          <img style="width:60px" src='.$email_icon.'>
          </div>
          <div class="" style="text-align: center;">
          <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0"></span>, </h6>
          </div>
          <div class="" style="text-align: center;padding: 15px 0px;">
          <img style="width: 230px;" src='.$thankyouimg.'>
          </div>
          <div class="" style="text-align: center;padding-bottom: 20px;">
          <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">You have received new booking service .</h5>
          <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">Veja a mensagem enviada inicialmente referente a essa solicitação de serviço: </p>
          <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
          <li><strong>User Name: </strong>' . $fullname  .'</li>
          <li><strong>Email: </strong>' . $email  .'</li>
          <li><strong>Message: </strong>' . $booking_msg  .'</li>
          </ul>
          </div>
          <div class="" style="text-align: center;padding-bottom: 10px">
          <a href="http://navizinhanca.com/login" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
          </div>
          <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
          <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
          </div>
          <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
          <div class="col-sm-6">
          <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
          <div style="float: left;width: 15%;margin-right: 20px;">
          <img style="width: 100%;" src='.$phone_img.'>
          </div>
          <div style="float: left;width: 70%;">
          <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a>
          <a href="tel:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
          </div>
          </div>
          </div>
          <div class="col-sm-6">
          <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
          <div style="float: left;width:15%;margin-right:20px;">
          <img style="width:100%" src='.$mail_icon.'>
          </div>
          <div style="float: left;width: 70%;">
          <a style="display: block;font-size:10px;" href="mailto:info@yellow.com" target="_top">Email us at</a>
          <a  style="display: block;font-size:12px;" href="mailto:info@yellow.com" target="_top">info@yellow.com</a>
          </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:100%;margin: -6px auto 0px;">
          <p style="font-size:11px;font-weight:bold;margin:0px;">Team NA VIZINHANCA</p>
          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
          </div>
          </div>
          </div>
          </body>
          </html>';

        //echo $message;die;
        // $msg ="hiiiiii how are you";
          $subject ="Booking  Service";
          $headers = "From: " .$email  . "\r\n";
        // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
          //$headers .= "CC: dev2.bdpl@gmail.com\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
          $mailsend = mail($to, $subject, $messageBody, $headers,'-faaaa@abc.com');
          return redirect()->action('ProfileController@show', $profile_id)->with('message','Your Booking Request sended successfully');
        }
      }
      catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror'));
      }catch(\Exception $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

        $errorlog                   = new Errorlogs;
        $errorlog->error_message    = $e->getMessage();
        $errorlog->line_number      = $e->getLine();
        $errorlog->file_name        = $e->getFile();
        $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
        $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
        $errorlog->loggedin_id      = Auth::user()->id;
        $errorlog->ip_address       = "";
        $errorlog->save();
        return view('errors.custom',compact('customerror'));
      }
    }

    public function switchuserrole(Request $request)
  {
    try{

      $userdetail = DB::table('users')->where('id','=',Auth::user()->id)->where('is_active','=','1')->first();
      $email = $userdetail->email;
      $password = trim($userdetail->hd_password);

      if($userdetail->user_role == 'Provider') {
        $Input =  [];
        $Input['user_role'] = 'Taker';
        $user_data = User::where('id', Auth::user()->id)->update($Input);
        //$request->session()->put('success', 'Role Changed Successfully!');
        //return redirect('/logout');
        
        Auth::logout();
        if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
            // The user is active, not suspended, and exists.
          return redirect('/home');
        }

      } else if($userdetail->user_role == 'Taker'){
        $Input =  [];
        $Input['user_role'] = 'Provider';
        $user_data = User::where('id', Auth::user()->id)->update($Input);
        //$request->session()->put('success', 'Role Changed Successfully!');
        //return redirect('/logout');
        Auth::logout();
        if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
            // The user is active, not suspended, and exists.
          return redirect('/home');
        }

      }
    
    }
    catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }catch(\Exception $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

      $errorlog                   = new Errorlogs;
      $errorlog->error_message    = $e->getMessage();
      $errorlog->line_number      = $e->getLine();
      $errorlog->file_name        = $e->getFile();
      $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
      $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
      $errorlog->loggedin_id      = Auth::user()->id;
      $errorlog->ip_address       = "";
      $errorlog->save();
      return view('errors.custom',compact('customerror'));
    }
  }
}
