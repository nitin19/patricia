<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{   
    protected $table = 'chathistory';

    protected $fillable = [
        'sender_id', 'receiver_id', 'message', 'created_at', 'updated_at', 'status', 'deleted',
    ];

    protected $hidden = [
        
    ]; 

    

}
