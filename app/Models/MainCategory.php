<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
		protected $table='main_category';
    protected $fillable = ['id','cat_image','name','created_by','updated_by','is_active','is_deleted','created_at','updated_at'];
		/* relation with  category*/
		public function Category()
		{
				return $this->hasMany('App\Models\Category','cat_id','main_cat_id');
		}

}
