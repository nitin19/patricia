<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	protected $table='booking';
    protected $fillable = ['booking_id','profile_id','user_id','category','booking_message','booking_amount','payment_type','booking_time','payment_confirm','transaction_id','booking_start_time','booking_end_time','booking_verified','is_active','is_deleted'];
}
