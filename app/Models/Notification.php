<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $table = 'notifications';
    protected $fillable = ['id', 'user_id', 'notificaton_type', 'notification_role', 'type_id', 'read_status', 'is_active', 'is_deleted', 'created_at', 'updated_at'];
   
}
