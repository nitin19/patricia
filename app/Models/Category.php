<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
		protected $table='category';
    protected $fillable = ['cat_id','cat_name','cat_description','main_cat_id','cat_image','is_active','is_deleted','created_at','updated_at'];
		/* relation with main category*/
		public function MainCategory()
    {
        return $this->belongsTo('App\Models\MainCategory','main_cat_id');
    }


}
