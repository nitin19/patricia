<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spavailability extends Model
{
	protected $table = 'spavailability';
    protected $fillable = [
    	'id', 'user_id', 'openhrs', 'day_monday', 'monday_start', 'monday_end', 'day_tuesday', 'tuesday_start', 'tuesday_end', 'day_wednesday', 'wednesday_start', 'wednesday_end', 'day_thursday', 'thursday_start', 'thursday_end', 'day_friday', 'friday_start', 'friday_end', 'day_saturday', 'saturday_start', 'saturday_end', 'day_sunday', 'sunday_start', 'sunday_end', 'status', 'isCompleted', 'created_at', 'updated_at'
    ];
   
}
