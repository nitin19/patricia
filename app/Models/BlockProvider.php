<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockProvider extends Model
{
    protected $table='block_providers';
    protected $fillable = ['provider_id','taker_id','block_status','created_at','updated_at','is_deleted'];
}
