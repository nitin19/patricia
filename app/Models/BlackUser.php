<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlackUser extends Model
{
    protected $table='black_users';
    protected $fillable = ['from_id','to_id','black_reason','to_name','from_name','user_type','black_status','created_at','updated_at','is_deleted'];
}
