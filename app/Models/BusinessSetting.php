<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessSetting extends Model
{
    //
    protected $fillable = ['id','name','email','phone','city','state','country','address','zipcode','twitter_link','google_link','linkedin','header_logo','footer_logo','admin_profile_picture','is_active','is_deletd','created_at','updated_at'];
}
