<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CloseAccountRequest extends Model
{
	protected $table='close_account_request';
   protected $fillable = ['id','user_id','close_reason','created_at'];
}
