<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider_price extends Model
{
    protected $table = 'provider_price_list';
    protected $fillable = ['id','user_id','query_id','amount','created_at','updated_at'];
    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
