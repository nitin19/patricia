<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';
    protected $fillable = ['id','user_id','provider_id','booking_id','rating','message','approve_status','role','created_at','updated_at'];
}
