@extends('layouts.default-header')
@section('title',$menu_meta_details->meta_title)
@section('description', $menu_meta_details->meta_description)
@section('keywords',$menu_meta_details->meta_keywords )
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>/* calender events customized css*/
.calendar {width: 650px !important;height: 319px;position: absolute;z-index: 9999;}
.prev, .next, .today{background-color:red;}
/* Main carousel style */
.carousel {width: 600px;}
/* Indicators list style */
.article-slide .carousel-indicators {bottom: 0;left: 0;margin-left: 5px;width: 100%;}
/* Indicators list style */
.article-slide .carousel-indicators li {border: medium none;border-radius: 0;float: left;height: 54px;margin-bottom: 5px;margin-left: 0;margin-right: 5px !important;margin-top: 0;width: 100px;}
/* Indicators images style */
.article-slide .carousel-indicators img {border: 2px solid #FFFFFF;float: left;height: 54px;left: 0;width: 100px;}
/* Indicators active image style */
.article-slide .carousel-indicators .active img {border: 2px solid #428BCA;opacity: 0.7;}
</style>
@if(session()->has('message'))
<script>
  $.notify({ message: { text: "{{ session()->get('message') }}" }
  },{
    type: 'success',offset:{ x: 10,y: 130 },animate: { enter: 'animated fadeInRight',
    exit: 'animated fadeOutRight'
    },
  });
</script>
@endif
@if(session()->has('error'))
<script>
  $.notify({
    message: { text: "{{ session()->get('error') }}" }
  },{
    type: 'danger',offset: { x: 10, y: 130 },
    animate: {
      enter: 'animated fadeInRight',exit: 'animated fadeOutRight' },
   });
</script>
@endif
<section class="maincontent searchbarbg">
  <div class="col-sm-12 pagecontent whitebgdiv nopadding">
    <div class="message"></div>
    <div class="bannerimg">
      <div class="container">
        <div class="col-sm-2 profile_img">
          <?php if($user_info->profile_image!=''){ ?>
            <img src="{{ url('/public/images/profileimage/') }}/<?php echo $user_info->profile_image; ?>">
            <?php } else { ?>
            <img src="{{ url('/public/') }}/images/profileimage/1542707849.images.png">
            <?php }
            $category_name = explode(",", $user_info->category);
            $current_userid = Auth::id();
            $current_username = Auth::user()->name;
            $user_id = $user_info->id;
            $current_user_role = Auth::user()->user_role;
            $uaddrs = $user_info->address;
            $pieces = explode(",", $uaddrs);
            ?>
        </div>
        <div class="col-sm-6 profileinfo">
          <h2>{{ $user_info->category }}</h2>
          <p><b>{{ ucfirst($user_info->name) }}</b> <span>
            <?php 
            if(count($userreview) > 0) {
            if($user_info->profile_rating != ''){
              $number = number_format($user_info->profile_rating,1);
              $integer_part = floor($number);
              $fraction_part = $number-$integer_part;
              for($x=1;$x<=$number;$x++) {?>
                <i class="fa fa-star"></i>
                <?php } if (strpos($number,'.')) { if($fraction_part != 0) { ?>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                  <?php  $x++; } }
              while($x<=5){?>
               <i class="fa fa-star-o" aria-hidden="true"></i>
               <?php $x++; } } }?>
           </span><span class="reviews">@if(count($userreview) > 0) ({{count($userreview)}} @if(count($userreview) == '1' )review @else reviews @endif) @else (No Review) @endif</span></p>
           <p class="prflinfo"><span>Categoria: </span>{{$user_info->main_cat_name}}</p>
           <p class="prflinfo"><span>Serviço: </span>{{$user_info->cat_name}}</p>
           <p class="prflinfo"><span>Distância: </span>{{number_format($user_info->distances,2,',','.')}} Km</p>
           <p class="prflinfo"><span>Bairro: </span> {{ucfirst($user_info->city) }}</p>
          <div class="profilebtns">
            @if($user_id!=$current_userid && $current_user_role=='Taker')
            <input type="hidden" name="booking_status" id="booking_status" value="@if(!empty($check_existing_booking)){{$check_existing_booking->booking_verified}}@endif">
            <a href="#" class="yellowbtn prflbtn marbot" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-paper-plane"></i>Contatar</a>
            <div class="prflbtn <?php if($wish_list_exist>0){ echo 'removewhishlist_profile'; } else { echo 'addwhishlists'; } ?>" value="{{ $user_info->id }}" id="whishlist_id" >
              <input type="hidden" name="userid" id="userid" value="{{ $user_info->id }}">
              <i class="fa <?php if($wish_list_exist>0){ echo 'fa-heart'; } else { echo 'fa-heart-o'; } ?>" value="{{ $user_info->id }}"></i>@if($wish_list_exist>0) Adicionado na sua lista de seleção @else Adicionar na lista de seleção @endif 
            </div>
            <a href="#" class="yellowbtn prflbtn marbot" data-toggle="modal" data-target="#blackuserModal" data-whatever="@mdoblk"><i class="fa fa-bug" ></i>Solicitar bloqueio</a>
            @else
            <a href="#" class="yellowbtn prflbtn " data-toggle="modal" data-target="#providerAlertModal" data-whatever="@mdopro"><i class="fa fa-paper-plane"></i>Adicionar na lista de seleção</a> 
            @endif
            <?php
              $paypal_email = 'ravi@49webstreet.com';
              $base_url = url('/');
              $return_url =   $base_url.'/success';
              $cancel_url =  $base_url.'/failure';
            ?>
          </div>
          <div class="modalsection">
            <div class="modal fade booknow_popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <h1 class="text-center">Contate {{ ucfirst($user_info->name) }}</h1>
                  <h4 class="text-center">Preencha os campos abaixo e ao clicar em “Contate agora”, o prestador receberá um E-mail e você terá acesso ao telefone de contato desse prestador.</h4>
                  <div class="modal-body">
                    <div class="alert alert-success" id="bokngSuccess"></div>
                    <div class="alert alert-danger" id="bokngError"></div>
                      <form id="booking_form">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group datesec">
                              <input type="text" class="form-control" id="startdate" name="startdate" placeholder="Data de preferência para o serviço" required>
                              <input type="hidden" id="profile_id" name="profile_id" value="{{ $user_id }}">
                              <input type="hidden" id="user_id" name="user_id" value="{{$current_userid}}">
                            </div>
                          </div>    
                        </div>
                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <input value="{{$user_info->main_cat_name}}" name="cat_name" id="cat_name" required readonly class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <input name ="sub_cat_name" id="sub_cat_name" value="{{$user_info->cat_name}}" required readonly class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 datesec">
                            <div class="form-group">
                              <select name="total_amount" class="form-control" id="total_amount" required>
                               <option value="" selected disabled>Selecione a opção mais apropriada</option>
                               @if(count($provider_price_lists) > 0)
                                 @foreach($provider_price_lists as $provider_price_list)
                                 <?php $serPrc = number_format($provider_price_list->amount,2,",","."); ?>
                                 <option value="{{'&#82;&#36;'.$serPrc.'/'. $provider_price_list->queries}}">{{'&#82;&#36;'.number_format($provider_price_list->amount,2,",",".").'/'. $provider_price_list->queries}}</option>
                                 @endforeach
                               @endif
                             </select>
                           </div>
                         </div>
                       </div>
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group datesec">
                            <textarea rows="6" name="booking_msg" id="booking_msg" class="" placeholder="Descreva em detalhes qual serviço você busca..." required></textarea>
                          </div>
                        </div>
                      </div>               
                      <div class="booknowbtn">
                        <input type="submit" class="btn btn-primary" id="booknowBtn" value="Contate agora">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- black user popup -->
        <div class="modalsection">
          <div class="modal fade blacknow_popup" id="blackuserModal" tabindex="-1" role="dialog" aria-labelledby="blackuserModalModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <h2 class="text-center" style="color: #fdfdfd;background-color: #1b9bd8;border-color: #1b9bd8;padding:20px;font-size: 18px;">Ajude-nos a entender o motivo de solicitar o bloqueio desse prestador de serviço no nosso site.</h2>
                <div class="modal-body">
                  <div class="alert alert-success" id="blackSuccess" style="display:none;"></div>
                    <div class="alert alert-danger" id="blackError"style="display:none;"></div>
                      <form id="blackForm" method="post">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group datesec">
                              <h4>Selecione o motivo:</h4>
                              <label class="radio_btn_label">Prestador não responde nos contatos disponibilizados
                                <input type="radio"  name="black_reason" value="Prestador não responde nos contatos disponibilizados" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason == 'Prestador não responde nos contatos disponibilizados') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <label class="radio_btn_label">Prestador não entregou o serviço no tempo previsto 
                                <input type="radio"  name="black_reason" value="Prestador não entregou o serviço no tempo previsto" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason == 'Prestador não entregou o serviço no tempo previsto') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <label class="radio_btn_label">Prestador não completou o serviço
                                <input type="radio" name="black_reason" value="Prestador não completou o serviço" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason == 'Prestador não completou o serviço') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <label class="radio_btn_label">Prestador se demonstrou inapto para o tipo de serviço contratado
                                <input type="radio"  name="black_reason" value="Prestador se demonstrou inapto para o tipo de serviço contratado" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason == 'Prestador se demonstrou inapto para o tipo de serviço contratado') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <label class="radio_btn_label">Prestador com comportamento inadequado
                                <input type="radio" name="black_reason" value="Prestador com comportamento inadequado" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason== 'Prestador com comportamento inadequado') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <label class="radio_btn_label">Prestador cadastrado no site não é o mesmo que o que efetua os serviços
                                <input type="radio"  name="black_reason" value="Prestador cadastrado no site não é o mesmo que o que efetua os serviços" @if(!empty($checkexist_user_blklist) && $checkexist_user_blklist->black_reason == 'Prestador cadastrado no site não é o mesmo que o que efetua os serviços') checked="checked" @endif>
                                <span class="checkmark"></span>
                              </label>
                              <input type="hidden" id="from_id" name="from_id" value="{{ $current_userid }}">
                              <input type="hidden" id="to_id" name="to_id" value="{{ $user_id }}">
                              <input type="hidden" id="to_name" name="to_name" value="{{ $user_info->name }}">
                              <input type="hidden" id="from_name" name="from_name" value="{{ $current_username }}">
                              <input type="hidden" id="user_type" name="user_type" value="{{ $current_user_role }}">
                            </div>
                          </div>
                        </div>       
                      <div class="blackBtn">
                        <input type="submit" class="btn btn-primary" id="blackBtn" value="Enviar">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end black users -->
          <!-- /*provider phone modal */ -->
          <div class="modalsection">
            <div class="modal fade provider_phn_popup" id="proPhoneModal" tabindex="-1" role="dialog" aria-labelledby="proPhoneModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="alert alert-success" id="bokngSuccessmesgs">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  </div>
                  <h1 class="text-center">Contate "{{ ucfirst($user_info->name) }}"</h1>
                  <h4 class="text-center">Se preferir ou estiver com pressa,<br> contate o prestador de serviço também por telefone: </h4>
                  <div class="modal-body">
                    <span class="provider_price" ><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+{{ $user_info->phone}}" target="_blank"> {{ $user_info->phone}}</a></span>
                    <div class="crossbtn">
                      <button type="button" class=" btn btn-primary pull-right" data-dismiss="modal">&times;</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end phone modal -->
          <!-- /*provider alert modal */ -->
          <div class="modalsection">
            <div class="modal fade provider_phn_popup" id="providerAlertModal" tabindex="-1" role="dialog" aria-labelledby="providerAlertModalLabel" style="top: 40px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <h1 class="text-center">Olá "{{ ucfirst($user_info->name) }}"</h1>
                  <div class="modal-body" style="padding: 15px 12px 45px;">
                    <span class="provider_price" >Para adicionar um prestador de serviço em na sua lista de seleção, você precisa sair da função de prestador de serviço e entrar com a função de contratante. Se deseja prosseguir, clique aqui..<a href="{{ URL::current() }}">clique aqui</a></span>
                    <div class="crossbtn">
                        <button type="button" class=" btn btn-primary pull-right" data-dismiss="modal">&times;</button>
                    </div>
                  </div>
                  </div>
              </div>
            </div>
          </div>
          <!-- end alert modal -->
          <div class="col-sm-4 durationdiv">
            @if(count($provider_price_lists) > 0)
            @foreach($provider_price_lists as $provider_price_list)
            <h5 class="profile-detail">&#82;&#36;{{ number_format($provider_price_list->amount,2,",",".").'/'.$provider_price_list->queries }}</h5>
            @endforeach
            @endif
          </div>
        </div>
      </div>
      <div class="singlepagecontent">
        <div class="container">
          <div class="col-sm-8 singlepageleft">
            <h2>Sobre {{ ucfirst($user_info->name) }}</h2>
            <p>{{$user_info->bio}}</p>
            <?php $count_user_gallery = count($user_gallery); $active_count = 1;
            if($count_user_gallery>0) { ?>
            <div class="carousel slide article-slide carousel slide singlepageslider" id="article-photo-carousel" >
              <!-- Wrapper for slides -->
              <div class="carousel-inner cont-slider">
                <?php foreach($user_gallery as $user_gallery_img){ ?>
                <div class="item <?php if($active_count==1) { echo 'active'; } ?>">
                  <a href="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" data-lightbox="user-gallery"><img src="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" alt="..."></a>
                </div>
                <?php ++$active_count; } ?>
              </div>
              <!-- Indicators -->
              <ol class="carousel-indicators user_glry_slider">
                <?php $count_user_gallery = count($user_gallery); $active_count = 1;
                  $i=-1; if($count_user_gallery>0){ foreach($user_gallery as $user_gallery_img){ ?>
                <li class="active" data-slide-to="<?php echo $i=$i+1;?>" data-target="#article-photo-carousel">
                  <div class="item <?php if($active_count==1) { echo 'active'; } ?>">
                    <img src="{{ url('/public/') }}/images/userimages/{{ $user_gallery_img->image_name }}" alt="...">
                  </div>
                </li>
                <?php ++$active_count;} } ?>
              </ol>
              <?php if($count_user_gallery>1){ ?>
              <a class="left carousel-control" href="#article-photo-carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#article-photo-carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <?php  } ?>
            </div>
          <?php } ?>  
          @if(count($user_documents) > 0)
          <div class="singleprofile_listing">
            <h3>Anexar documento</h3>
            @foreach ($user_documents as $documents)
            <?php   $exploded=explode(".",$documents->document);$cpfname = $exploded[0];
            $cpfExt = $exploded[1];
            if($cpfExt == 'pdf'){ ?>
              <img src="{{ url('/public/images/') }}/cpfpdf.png" class="img-circle" style="width: 120px;height:120px;margin-top: 10px;"> 
            <?php } elseif($cpfExt == 'doc') { ?>
              <img src="{{ url('/public/images/') }}/cpfdoc.png" class="img-circle" style="width: 120px;height:120px;margin-top: 10px;">
            <?php } else { ?>
              <img src="{{ url('/public') }}/images/user_document/{{ $documents->document }}"  style="width: 120px;height:120px;margin-top: 10px;">
            <?php } ?>
            @endforeach
          </div>
          @endif
          @if(!empty($user_info->additional_details))
          <div class="singleprofile_listing">
            <h3>Cursos e certificados na área (informados pelo prestador de serviço)</h3>
            <ul class="listingitem1">
              <li><i class="fa fa-check-circle"></i>{{$user_info->additional_details}}</li>
            </ul>
          </div>
          @endif
          <div class="singleprofile_listing">
            <h3>Dados gerais</h3>
            <ul class="listingitem2">
              <li>Clientes repetidos: <span>{{ $repeat_clients }}</span></li>
              <li># de serviço(s) confirmados: <span>{{ $complete_booking }}</span></li>
              <li>Taxa de resposta: <span>100%</span></li>
              <li>Tempo de resposta: <span>em poucas horas</span></li>
            </ul>
          </div>
          <div class="singleprofile_listing">
            <h3>Horário</h3>
            <a href="javascript:void(0);"><h4 class="view_cal"></h4></a>
            <div class="event-calendar"></div>
              <ul class="listingitem1">
                @if($get_user_availablity->isCompleted=='1')
                  @if($get_user_availablity->openhrs=='1')
                    <li> @if($get_user_availablity->day_monday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Segunda-feira:
                      @if($get_user_availablity->day_monday=='1')
                      {{$get_user_availablity->monday_start}} - {{$get_user_availablity->monday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_tuesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Terça-feira:
                      @if($get_user_availablity->day_tuesday=='1')
                      {{$get_user_availablity->tuesday_start}} - {{$get_user_availablity->tuesday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_wednesday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quarta-feira:
                      @if($get_user_availablity->day_wednesday=='1')
                      {{$get_user_availablity->wednesday_start}} - {{$get_user_availablity->wednesday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_thursday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Quinta-feira:
                      @if($get_user_availablity->day_thursday=='1')
                      {{$get_user_availablity->thursday_start}} - {{$get_user_availablity->thursday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_friday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sexta-feira:
                      @if($get_user_availablity->day_friday=='1')
                      {{$get_user_availablity->friday_start}} - {{$get_user_availablity->friday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_saturday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Sábado:
                      @if($get_user_availablity->day_saturday=='1')
                      {{$get_user_availablity->saturday_start}} - {{$get_user_availablity->saturday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                    <li>@if($get_user_availablity->day_sunday=='1') <i class="fa fa-check-circle"></i> @else <i class="fa fa-times-circle"></i> @endif Domingo:
                      @if($get_user_availablity->day_sunday=='1')
                      {{$get_user_availablity->sunday_start}} - {{$get_user_availablity->sunday_end}}
                      @else
                      Fechado
                      @endif
                    </li>
                  @else
                    <li><i class="fa fa-check-circle"></i>Segunda-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Terça-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Quarta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Quinta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Sexta-feira 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Sábado 00:00 - 23:59</li>
                    <li><i class="fa fa-check-circle"></i>Domingo 00:00 - 23:59</li>
                  @endif
                @else
                  <li><i class="fa fa-times-circle"></i>No availability set by {{$get_user_availablity->name}}</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-sm-4 singlepageright">
            @if(count($similar_category_users)>0)
          <!--  <?php echo "<pre>";print_r($similar_category_users);echo "</pre>";?> -->
            <div class="singlepagesidebar">
              <h3>Similar {{ lcfirst($user_info->main_cat_name) }} </h3>
               
              @foreach($similar_category_users as $cat_info)

                @if($cat_info->id != $id)

                  <div class="babysitter_div">
                    <div class="babysitter_img">
                      <?php if($cat_info->profile_image!=''){ ?>
                        <img src="{{ url('/public/images/profileimage/') }}/<?php echo $cat_info->profile_image; ?>">
                        <?php } else { ?>
                          <img src="{{ url('/public/images/profileimage/1542707849.images.png') }}">
                      <?php } ?>
                    </div>
                    <div class="babysitter_text">
                      <h4><a href="{{url('/profile/'.$cat_info->id)}}">{{ucfirst($cat_info->name) }}</a></h4>
                      <?php  $interger_part = ''; $fraction_part= '';
                      $uaddrs1 = $cat_info->address;
                      $pieces1 = explode(",", $uaddrs1);
                      if(!empty($cat_info->profile_rating)){
                        $number = number_format($cat_info->profile_rating,1);
                        $integer_part = floor($number);
                        $fraction_part = $number-$integer_part;
                        for($x=1;$x<=$number;$x++) {?>
                          <i class="fa fa-star"></i>
                        <?php }
                        if (strpos($number,'.')) {
                          if($fraction_part != 0) {?>
                           <i class="fa fa-star-half-o" aria-hidden="true"></i>
                           <?php 
                           $x++;
                         }
                       }
                        while ($x<=5) {?>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                          <?php
                         $x++;
                        }
                      }
                      else{
                        for($x=1;$x<5;$x++){?>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <?php  }
                      }
                      if(!empty($get_feedbacks)){
                        foreach($get_feedbacks as $u=>$f){
                          if($cat_info->id == $u)
                            echo "(".$f.")";
                        }
                      } ?>
                      <h5>{{ucfirst($cat_info->city) }} ({{number_format($cat_info->distances,2,",",".")}} km)</h5>
                      <h5>&#82;&#36;{{number_format($cat_info->amount,2,",",".")}}  @if($cat_info->price_type!='') {{ $cat_info->price_type }} @endif </h5>
                    </div>
                  </div>
                @endif
              @endforeach
            </div>
            @endif
          </div>
          <div class="singleprofile_listing">
            <h3>Comentário dos contratantes (@if(count($userreview) > 1){{count($userreview).' comentários' }} @elseif(count($userreview) == 1) {{ '1 comentário' }}@else 0 comentário @endif )</h3>
            <div class="review_section">
              @if(count($userreview) > 0)
              @foreach($userreview as $feedback)
                <?php 
                 $fmt = datefmt_create(
                  'pt-BR',
                  IntlDateFormatter::FULL,
                  IntlDateFormatter::FULL,
                  'Brazil/East',
                  IntlDateFormatter::GREGORIAN,
                  "dd/MMM/YYYY"  
                   );
                    //notification time
                  $dt = new DateTime($feedback->created_at. "+1 days");
                  $d = $dt->format('d');
                  $m = $dt->format('m');
                  $y = $dt->format('Y');
                  $startdate = datefmt_format($fmt, mktime(0,0,0,$m,$d,$y));
                   ?>
              <div class="reviewblk">
                <div class="row">
                  <div class="clientimg">
                    <img class="user_image" src="@if(!empty($feedback->profile_image)) {{ url('/public/images/profileimage/'.$feedback->profile_image) }}@else {{url('/public/images/gravtar.jpeg')}} @endif">
                  </div>
                  <div class="client_text nopadding">
                    <h4 class="user_name">{{$feedback->name}}<span>
                      <?php 
                        for($i=0; $i<$feedback->rating; $i++){
                          echo '<i class="fa fa-star" aria-hidden="true"></i>';
                        }
                      ?>
                     </span></h4>
                      <h5 class="feedback_date">{{$startdate}}</h5>
                      <p class="feedback_message" id="msgrow{{$feedback->id}}"><?php if(strlen($feedback->message) > 200){ echo substr($feedback->message, 0, 200). '<span class="dots" id="dotmsg'.$feedback->id.'">...</span><span class="readmore" id="moremsg'.$feedback->id.'" style="display:none;">'.substr($feedback->message, 200, strlen($feedback->message)).'</span><span class="btnmore">Veja mais</span><span class="btnless" style="display:none;">Veja menos</span>';}else{ echo $feedback->message;} ?></p>  
                    <input type="hidden" name="page" value="2" id="page_number">
                    
                 </div>
               </div>
             </div>
            @endforeach
            @else
              <h2 align="center"> Nenhum comentário feito pelos contratantes para esse prestador de serviço.</h2>
            @endif
          </div>
          <div class="loadbtn">
            <?php if(count($feedbacks)==5){?>
              <a data-id="{{$user_info->id}}" class="load_more">Load More</a>
            <?php }?>
          </div>
        </div>
      </div>
    </div>
    <div id="map" style="width: 100%; height: 400px; background: #5497b7" />
    <style>
    #proPhoneModal .modal-content { border: 12px solid #d5d7d8;border-radius: 5px;padding: 30px 0;}
    .provider_price { display: block; width: 100%;text-align: center;font-weight: bold;font-size: 20px;}
    .fa-phone {-webkit-animation: wrench 2s infinite;animation: wrench 2s infinite;color: #1B9BD8;font-size: 18px;}
    @-webkit-keyframes wrench {
        0% { -webkit-transform: rotate(-12deg);transform: rotate(-12deg);}
        8% {-webkit-transform:  rotate(12deg);transform: rotate(12deg);}
      10% { -webkit-transform: rotate(24deg);transform: rotate(24deg);}
      18% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg); }
      20% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
      28% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
      30% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
      38% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
      40% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
      48% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
      50% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
      58% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
      60% {-webkit-transform: rotate(-24deg);transform: rotate(-24deg);}
      68% {-webkit-transform: rotate(24deg);transform: rotate(24deg);}
      75% {-webkit-transform: rotate(0deg);transform: rotate(0deg);}
    }
    </style>
    <script  type="text/javascript" charset="UTF-8" >
    /* carousel script for user gallery*/
      $('.carousel').carousel({
        interval: 2000,
        cycle:true
      });

      var lat = '<?php echo $user_info->latitude; ?>';
      var lan = '<?php echo $user_info->longitude; ?>';
      /**
      * Adds a circle over New Delhi with a radius of 1000 metres onto the map
      *
      * @param  {H.Map} map      A HERE Map instance within the application
      */
      function addCircleToMap(map){
        map.addObject(new H.map.Circle(
          // The central point of the circle
          {lat: lat, lng: lan},
          // The radius of the circle in meters
          1000,
          {
            style: {
              strokeColor: 'rgba(55, 85, 170, 0.6)', // Color of the perimeter
              lineWidth: 2,
              fillColor: 'rgba(0, 128, 0, 0.7)'  // Color of the circle
            }
          }
          ));
      }
      /**
      * Boilerplate map initialization code starts below:
      */
      //Step 1: initialize communication with the platform
      var platform = new H.service.Platform({
        app_id: 'devportal-demo-20180625',
        app_code: '9v2BkviRwi9Ot26kp2IysQ',
        useHTTPS: true
      });
      var pixelRatio = window.devicePixelRatio || 1;
      var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320
      });
      //Step 2: initialize a map - this map is centered over New Delhi
      var map = new H.Map(document.getElementById('map'),
        defaultLayers.normal.map.setMax(15),{
          center: {lat: lat, lng: lan},
          zoom: 15,
          pixelRatio: pixelRatio
        });
        //Step 3: make the map interactive
        // MapEvents enables the event system
        // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
        var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
        // Create the default UI components
        var ui = H.ui.UI.createDefault(map, defaultLayers);
        // Now use the map as required...
        addCircleToMap(map);
    </script>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){

    $(".btnmore").click(function(){
      var tdid = $(this).closest('p').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'inline');
      $('#'+tdid).find('.btnless').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'none');
      $(this).css('display', 'none');
  });

  $(".btnless").click(function(){
      var tdid = $(this).closest('p').attr('id');
      $('#'+tdid).find('.readmore').css('display', 'none');
      $('#'+tdid).find('.btnmore').css('display', 'block');
      $('#'+tdid).find('.dots').css('display', 'inline');
      $(this).css('display', 'none');
  }); 

    $('#bokngSuccess').hide();
    $('#bokngError').hide();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#booknowBtn').click(function(e) {
      e.preventDefault();
      var startdate = $('#startdate').val();
     // var enddate   = $('#enddate').val();
      var cat_name = $('#cat_name').val();
      var sub_cat_name = $('#sub_cat_name').val();
      var booking_msg = $('#booking_msg').val();
      var profile_id = $('#profile_id').val();
      var user_id = $('#user_id').val();
      var total_amount = $('#total_amount').val();
      var baseUrl  = '<?php echo url('/');?>';
      if(startdate=='') {
       $('#startdate').addClass('vlderror');
       return false;
     }
     if(booking_msg=='') {
      $('#booking_msg').addClass('vlderror');
      return false;
     }
     $.ajax({
       type:'POST',
       url:baseUrl+'/booking',
       data:{ startdate:startdate, cat_name:cat_name, sub_cat_name:sub_cat_name, booking_msg:booking_msg, profile_id:profile_id, user_id:user_id, total_amount:total_amount },
       success:function(data){
        data=jQuery.parseJSON(data);
        if(data.msgCode == 1) {
          $('.provider_price').show();
          $("#booking_form")[0].reset();
          $('#exampleModal').modal('hide');
          $('#proPhoneModal').modal('toggle');
          $('#proPhoneModal').animate({
            scrollTop: 0
          }, 'slow');
          $('#bokngSuccessmesgs').html(data.message);
          $('#bokngSuccessmesgs').show();
        } else {
          $('#bokngError').html(data.message);
          $('#bokngError').show();
          $('#exampleModal').animate({
            scrollTop: 0
          }, 'slow');
          window.setTimeout(function () {
            $("#bokngError").fadeTo(500, 0).slideUp(500, function () {
              $(this).hide();
            });
          }, 3000);
        }
      }
    });
   });
    // ajax black list
    $('#blackBtn').click(function(e) {
      e.preventDefault();
      var black_reason = $('#black_reason').val();
      var Url  = '<?php echo url('/');?>';
      if(black_reason=='') {
       $('#black_reason').addClass('vlderror');
       return false;
       }
      $.ajax({
           type:'POST',
           url:Url+'/addblackuser',
           data:$('#blackForm').serialize(),
           success:function(data){
            data=jQuery.parseJSON(data);
            if(data.msgcode == 1) {
              $("#blackForm")[0].reset();
              $('#blackSuccess').html(data.message);
              $('#blackSuccess').show();
              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');
              
              window.setTimeout(function () {
                $("#blackSuccess").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  window.location.href="<?php echo url('/');?>";
                });
              }, 3000);

            } else {
              $('#blackError').show();
              $('#blackError').html(data.message);
              $('#blackError').css('opacity',1);

              $('#blackuserModal').animate({
                scrollTop: 0
              }, 'slow');

              window.setTimeout(function () {
                $("#blackError").fadeTo(500, 0).slideUp(500, function () {
                  $(this).hide();
                  //$('#blackuserModal').modal('hide');
                });
              }, 3000);

            }
          }
        });
       });
    //end ajax black list
    $("#startdate").focusout(function(){
      $(this).removeClass('vlderror');
    });
    $("#booking_msg").focusout(function(){
      $(this).removeClass('vlderror');
    });
    jQuery(function () {
    $('#startdate').datetimepicker({
      useCurrent: false,
      format: 'DD-MM-YYYY',
      minDate: moment()
    });
    $('#startdate').datetimepicker().on('dp.change', function (e) {
      var incrementDay = moment(new Date(e.date));
      incrementDay.add(0, 'days');
      //$('#enddate').data('DateTimePicker').minDate(incrementDay);
      $(this).data("DateTimePicker").hide();
    });
  });
  /* load more feedback to on click load more button*/
  $('.load_more').on('click',function(){
    var page = $('#page_number').val();
    var id=$(this).data('id');``
    var base_url = '<?php echo url('/');?>';
    var url = base_url+'/load_more_feedbacks/'+id+'?page='+page;
    var profile_url ='';
    $.ajax({
      type:'GET',
      url:url,
      dataType:'json',
      success:function(data){
        // alert(page);
        page = ++page;
        $('#page_number').val(page);
        for(var i=0;i<=data.count;i++){
          $.each(data.info, function() {
            $.each(this, function(k, v) {
              if(v.profile_image ==null){
               profile_url = base_url+'/public/images/gravtar.jpeg';
             }
             else{
              profile_url = base_url+'/public/images/profileimage'+v.profile_image;
            }
            var feedback_html = '<div class="reviewblk"><div class="row"> <div class="clientimg"> <img class="user_image" src='+profile_url
            +' > </div><div class="client_text nopadding"><h5>'+v.name+'</h5><p>'+v.message+'</p>';

            feedback_html+='<h5>';
            for(var j=0;j< v.rating;j++){
              feedback_html+='<i class="fa fa-star"></i>';
            }
            feedback_html+='</h5></div></div></div>';
            if(k < data.count){
              $('.review_section').append(feedback_html);
            }
          });
          });

        }
      },
    });
  })
  /* end ajax code*/
});
</script>
<style type="text/css">
.greybtn_anchr {background: #696969 !important;color: #fff;background: red none repeat scroll 0 0;border-radius: 5px;box-shadow: 1px 3px 12px 0 #919191;font-family: Lato;font-size: 16px;font-weight: bold;margin-right: 10px;padding: 10px 25px;}
.prflbtn { float: left; cursor: pointer; }
.payment_type { color: #898989; font-size: 20px; }
.vlderror { border: 2px solid red !important; }
#bokngSuccess { opacity: 1 !important; }
#bokngError { opacity: 1 !important; }
.article-slide .carousel-indicators {
    overflow: -webkit-paged-x;
    display: flex;
}
.article-slide .carousel-indicators li {
    height: 100px;
    width: 100px;
}
.article-slide .carousel-indicators img {
    height: 100px;
    width: 100px;
}
@media screen and (max-width:991px) {
  .greybtn_anchr {padding: 10px 15px;font-size:14px;}
  .profilebtns a {padding: 10px 15px;font-size: 14px;}
  .profilebtns a i {font-size: 14px;}
}
label.radio_btn_label {
    font-weight: 100;
}


</style>
@stop
